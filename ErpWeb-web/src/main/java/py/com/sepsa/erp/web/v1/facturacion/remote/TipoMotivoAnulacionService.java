/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAnulacionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TipoMotivoAnulacionFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivoAnulacion;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para Tipo Motivo Anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoMotivoAnulacionService extends APIErpFacturacion {

    /**
     * Obtiene la lista de objetos
     *
     * @param tipoMotivoAnulacion Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoMotivoAnulacionAdapter getTipoMotivoAnulacionList(TipoMotivoAnulacion tipoMotivoAnulacion, Integer page,
            Integer pageSize) {

        TipoMotivoAnulacionAdapter lista = new TipoMotivoAnulacionAdapter();

        Map params = TipoMotivoAnulacionFilter.build(tipoMotivoAnulacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_MOTIVO_ANULACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoMotivoAnulacionAdapter.class);

            lista = (TipoMotivoAnulacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de clase
     */
    public TipoMotivoAnulacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_MOTIVO_ANULACION("Tipo Motivo Anulacion", "tipo-motivo-anulacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
