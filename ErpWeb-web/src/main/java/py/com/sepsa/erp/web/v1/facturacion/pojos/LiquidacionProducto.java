/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * Pojo para liquidacion producto
 *
 * @author Sergio D. Riveros Vazquez
 */
public class LiquidacionProducto {

    /**
     * Identificador del liquidacion producto
     */
    private Integer id;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Cliente
     */
    private String Cliente;
    /**
     * Plazo
     */
    private Integer plazo;
    /**
     * Extension
     */
    private Integer extension;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }

    public Integer getPlazo() {
        return plazo;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public Integer getExtension() {
        return extension;
    }

    public void setExtension(Integer extension) {
        this.extension = extension;
    }

    //</editor-fold>
    /**
     * Constructor
     */
    public LiquidacionProducto() {

    }

    /**
     * Constructor con id
     *
     * @param id
     */
    public LiquidacionProducto(Integer id) {
        this.id = id;
    }

}
