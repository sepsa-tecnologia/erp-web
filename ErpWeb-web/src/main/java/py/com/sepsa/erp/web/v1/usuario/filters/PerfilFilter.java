package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;

/**
 * Filtro utilizado para perfil
 *
 * @author Cristina Insfrán
 */
public class PerfilFilter extends Filter {

    /**
     * Agrega el filtro de identificador del perfil
     *
     * @param id Identificador del perfil
     * @return
     */
    public PerfilFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la empresa
     *
     * @param idEmpresa Identificador de la empresa
     * @return
     */
    public PerfilFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por código
     *
     * @param codigo codgio del perfil
     * @return
     */
    public PerfilFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de identicador del software
     *
     * @param idSoftware Identificador del software
     * @return
     */
    public PerfilFilter idSoftware(Integer idSoftware) {
        if (idSoftware != null) {
            params.put("idSoftware", idSoftware);
        }
        return this;
    }

    /**
     * Agrega un filtro por software
     *
     * @param software software
     * @return
     */
    public PerfilFilter software(String software) {
        if (software != null && !software.trim().isEmpty()) {
            params.put("software", software);
        }
        return this;
    }

    /**
     * Agrega un filtro por codigo de software
     *
     * @param codigoSoftware codigo de software
     * @return
     */
    public PerfilFilter codigoSoftware(String codigoSoftware) {
        if (codigoSoftware != null && !codigoSoftware.trim().isEmpty()) {
            params.put("codigoSoftware", codigoSoftware);
        }
        return this;
    }

    /**
     * Agrega el filtro para la descripción
     *
     * @param descripcion descripción del software
     * @return
     */
    public PerfilFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro por empresa
     *
     * @param empresa
     * @return
     */
    public PerfilFilter empresa(Empresa empresa) {
        if (empresa != null) {
            params.put("empresa", empresa);
        }
        return this;
    }

    /**
     * Filtro por campo activo
     *
     * @param activo
     * @return
     */
    public PerfilFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param perfil
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Perfil perfil, Integer page, Integer pageSize) {

        PerfilFilter filter = new PerfilFilter();

        filter
                .id(perfil.getId())
                .idEmpresa(perfil.getIdEmpresa())
                .descripcion(perfil.getDescripcion())
                .codigo(perfil.getCodigo())
                .empresa(perfil.getEmpresa())
                .activo(perfil.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PerfilFilter
     */
    public PerfilFilter() {
        super();
    }
}
