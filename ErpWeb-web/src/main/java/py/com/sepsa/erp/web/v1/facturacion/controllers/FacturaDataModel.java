/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.util.List;
import java.util.Map;
import org.primefaces.model.SortOrder;
import py.com.sepsa.erp.web.v1.adapters.AbstractDataModel;
import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;


/**
 *
 * @author Williams Vera
 */
public class FacturaDataModel extends AbstractDataModel<Factura, Factura> {
 
    private final FacturacionService service;
    
    @Override
    public List<Factura> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        DataListAdapter<Factura> result = callService(filtro, first, pageSize);
        
        setRowCount(Integer.parseInt(result.getTotalSize() + ""));
        
        return result.getData();
    }
    
        @Override
    public Factura getRowData(String rowKey) {
        if (rowKey != null){
            Factura fac = new Factura();
            fac.setId(Integer.valueOf(rowKey));
            return service.getFacturaMontoList(fac, 0, 10).getData().get(0);
        }

        return null;
    }

    @Override
    public String getRowKey(Factura cob) {
        return String.valueOf(cob.getId());
    }
    
    public FacturaDataModel() {
        this.service = new FacturacionService();
        this.filtro = new Factura();
    }
    
    @Override
    public DataListAdapter<Factura> callService(Factura filtro, Integer first, Integer pageSize) {
        return service.getFacturaMontoList(filtro, first, pageSize);
    }
}
