package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaRolAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProveedorCompradorAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.RolListAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;
import py.com.sepsa.erp.web.v1.info.pojos.ProveedorComprador;
import py.com.sepsa.erp.web.v1.info.pojos.Rol;
import py.com.sepsa.erp.web.v1.comercial.remote.ProductoService;
import py.com.sepsa.erp.web.v1.info.remote.ProveedorCompradorService;

/**
 * Controlador para ProveedorComprador
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("proveedorComprador")
public class ProveedorCompradorController implements Serializable {

    /**
     * Cantidad de filas de la tabla
     */
    private Integer rows = 10;
    /**
     * Variable para control interno
     */
    private boolean showProveedor;
    /**
     * Variable para control interno
     */
    private boolean showComprador;
    /**
     * Dato del Cliente
     */
    private String idCliente;
    /**
     * Adaptador de producto
     */
    private ProductoAdapter adapterProductoProveedor;
    /**
     * Objeto Producto para filtro
     */
    private Producto productoFilter;
    /**
     * Adaptador de producto
     */
    private ProductoAdapter adapterProductoComprador;
    /**
     * Adaptador de producto
     */
    private ProductoAdapter adapterProductoFilter;
    /**
     * Adaptador de producto
     */
    private ProductoAdapter adapterProductoDescripcion;
    /**
     * Objeto Producto para filtro
     */
    private Producto productoFilterComprador;
    /**
     * Objeto Producto para filtro
     */
    private Producto producto;
    /**
     * Adaptador para persona rol
     */
    private PersonaRolAdapter adapterPersonaRol;

    /**
     * Adaptador para persona rol
     */
    private PersonaRolAdapter adapterPersonaRolCreate;
    /**
     * Objeto Persona Rol
     */
    private PersonaRol personaRolFilter;
    /**
     * Objeto Persona Rol
     */
    private PersonaRol rolPersonaCreate;
    /**
     * Servicio de Producto
     */
    private ProductoService serviceProducto;
    /**
     * Adaptador de Roles
     */
    private RolListAdapter adapterRol;
    /**
     * Lista para persona proveedor seleccionados
     */
    private List<PersonaRol> listSelectedPersonaProveedor = new ArrayList();
    /**
     * Lista para persona comprador seleccionado
     */
    private List<PersonaRol> listSelectedPersonaComprador = new ArrayList();
    /**
     * Lista para productos seleccionados
     */
    private List<Producto> listSelectedProductoComprador = new ArrayList();
    /**
     * Lista para productos seleccionados
     */
    private List<Producto> listSelectedProductoProveedor = new ArrayList();
    /**
     * Adaptador para la lista de personas
     */
    private PersonaListAdapter adapterPersona;
    /**
     * Servicio de proveedor comprador
     */
    private ProveedorCompradorService proComService;
    /**
     * Objeto proveedorComprador
     */
    private ProveedorComprador proveedorComprador;
    /**
     * Objeto proveedorComprador
     */
    private ProveedorComprador proveedorCompradorEdit;
    /**
     * Adaptador para la lista de proveedor comprador
     */
    private ProveedorCompradorAdapter adapterProveedorComprador;
    /**
     * Objeto proveedorComprador
     */
    private ProveedorComprador proveedorCompradorFilter;
    /**
     * Objeto producto
     */
    private Producto proFilter;
    /**
     * POJO PersonaRol
     */
    private PersonaRol personaShow;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setAdapterPersonaRolCreate(PersonaRolAdapter adapterPersonaRolCreate) {
        this.adapterPersonaRolCreate = adapterPersonaRolCreate;
    }

    public void setPersonaShow(PersonaRol personaShow) {
        this.personaShow = personaShow;
    }

    public PersonaRol getPersonaShow() {
        return personaShow;
    }

    public void setProveedorCompradorFilter(ProveedorComprador proveedorCompradorFilter) {
        this.proveedorCompradorFilter = proveedorCompradorFilter;
    }

    public void setProFilter(Producto proFilter) {
        this.proFilter = proFilter;
    }

    public Producto getProFilter() {
        return proFilter;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getRows() {
        return rows;
    }

    public void setProveedorCompradorEdit(ProveedorComprador proveedorCompradorEdit) {
        this.proveedorCompradorEdit = proveedorCompradorEdit;
    }

    public ProveedorComprador getProveedorCompradorEdit() {
        return proveedorCompradorEdit;
    }

    public ProveedorComprador getProveedorCompradorFilter() {
        return proveedorCompradorFilter;
    }

    public void setAdapterProveedorComprador(ProveedorCompradorAdapter adapterProveedorComprador) {
        this.adapterProveedorComprador = adapterProveedorComprador;
    }

    public void setAdapterProductoDescripcion(ProductoAdapter adapterProductoDescripcion) {
        this.adapterProductoDescripcion = adapterProductoDescripcion;
    }

    public void setAdapterProductoFilter(ProductoAdapter adapterProductoFilter) {
        this.adapterProductoFilter = adapterProductoFilter;
    }

    public ProductoAdapter getAdapterProductoFilter() {
        return adapterProductoFilter;
    }

    public ProductoAdapter getAdapterProductoDescripcion() {
        return adapterProductoDescripcion;
    }

    public ProveedorCompradorAdapter getAdapterProveedorComprador() {
        return adapterProveedorComprador;
    }

    public PersonaRolAdapter getAdapterPersonaRolCreate() {
        return adapterPersonaRolCreate;
    }

    public void setProductoFilterComprador(Producto productoFilterComprador) {
        this.productoFilterComprador = productoFilterComprador;
    }

    public void setAdapterProductoComprador(ProductoAdapter adapterProductoComprador) {
        this.adapterProductoComprador = adapterProductoComprador;
    }

    public Producto getProductoFilterComprador() {
        return productoFilterComprador;
    }

    public ProductoAdapter getAdapterProductoComprador() {
        return adapterProductoComprador;
    }

    public void setProveedorComprador(ProveedorComprador proveedorComprador) {
        this.proveedorComprador = proveedorComprador;
    }

    public ProveedorComprador getProveedorComprador() {
        return proveedorComprador;
    }

    public void setListSelectedProductoProveedor(List<Producto> listSelectedProductoProveedor) {
        this.listSelectedProductoProveedor = listSelectedProductoProveedor;
    }

    public void setListSelectedProductoComprador(List<Producto> listSelectedProductoComprador) {
        this.listSelectedProductoComprador = listSelectedProductoComprador;
    }

    public List<Producto> getListSelectedProductoProveedor() {
        return listSelectedProductoProveedor;
    }

    public List<Producto> getListSelectedProductoComprador() {
        return listSelectedProductoComprador;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public List<PersonaRol> getListSelectedPersonaProveedor() {
        return listSelectedPersonaProveedor;
    }

    public void setListSelectedPersonaProveedor(List<PersonaRol> listSelectedPersonaProveedor) {
        this.listSelectedPersonaProveedor = listSelectedPersonaProveedor;
    }

    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }

    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }

    public List<PersonaRol> getListSelectedPersonaComprador() {
        return listSelectedPersonaComprador;
    }

    public void setListSelectedPersonaComprador(List<PersonaRol> listSelectedPersonaComprador) {
        this.listSelectedPersonaComprador = listSelectedPersonaComprador;
    }

    public void setProductoFilter(Producto productoFilter) {
        this.productoFilter = productoFilter;
    }

    public PersonaRol getPersonaRolFilter() {
        return personaRolFilter;
    }

    public void setRolPersonaCreate(PersonaRol rolPersonaCreate) {
        this.rolPersonaCreate = rolPersonaCreate;
    }

    public PersonaRol getRolPersonaCreate() {
        return rolPersonaCreate;
    }

    public void setAdapterRol(RolListAdapter adapterRol) {
        this.adapterRol = adapterRol;
    }

    public void setShowProveedor(boolean showProveedor) {
        this.showProveedor = showProveedor;
    }

    public void setShowComprador(boolean showComprador) {
        this.showComprador = showComprador;
    }

    public boolean isShowProveedor() {
        return showProveedor;
    }

    public boolean isShowComprador() {
        return showComprador;
    }

    public RolListAdapter getAdapterRol() {
        return adapterRol;
    }

    public void setPersonaRolFilter(PersonaRol personaRolFilter) {
        this.personaRolFilter = personaRolFilter;
    }

    public void setAdapterPersonaRol(PersonaRolAdapter adapterPersonaRol) {
        this.adapterPersonaRol = adapterPersonaRol;
    }

    public PersonaRolAdapter getAdapterPersonaRol() {
        return adapterPersonaRol;
    }

    public void setServiceProducto(ProductoService serviceProducto) {
        this.serviceProducto = serviceProducto;
    }

    public ProductoService getServiceProducto() {
        return serviceProducto;
    }

    public Producto getProductoFilter() {
        return productoFilter;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public void setAdapterProductoProveedor(ProductoAdapter adapterProductoProveedor) {
        this.adapterProductoProveedor = adapterProductoProveedor;
    }

    public ProductoAdapter getAdapterProductoProveedor() {
        return adapterProductoProveedor;
    }

    public void setProComService(ProveedorCompradorService proComService) {
        this.proComService = proComService;
    }

    public ProveedorCompradorService getProComService() {
        return proComService;
    }

    public String getIdCliente() {
        return idCliente;
    }
//</editor-fold>

    /**
     * Método para seleccionar el proveedor
     *
     * @param event
     */
    public void onItemSelectProveedor(SelectEvent event) {
        proveedorCompradorFilter.setIdProveedor(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Método para seleccionar el comprador
     *
     * @param event
     */
    public void onItemSelectComprador(SelectEvent event) {
        proveedorCompradorFilter.setIdComprador(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Actualiza la lista de documentos
     */
    public void loadProveedorComprador() {
        adapterProveedorComprador.setPageSize(rows);
        adapterProveedorComprador = adapterProveedorComprador.fillData(proveedorCompradorFilter);
    }

    /**
     * Método para obtener la descripción del rol
     *
     * @param idRol
     * @return
     */
    public String getDescripcion(Integer idRol) {
        Rol rolID = new Rol();
        rolID.setId(idRol);
        adapterRol = adapterRol.fillData(rolID);

        return adapterRol.getData() == null
                || adapterRol.getData().isEmpty()
                ? "N/A"
                : adapterRol.getData().get(0).getDescripcion();
    }

    /**
     * Método para obtener la descripción del producto
     *
     * @param idRol
     * @return
     */
    public String getDescripcionProducto(Integer idProducto) {
        Producto productoID = new Producto();
        productoID.setId(idProducto);

        adapterProductoDescripcion = adapterProductoDescripcion.fillData(productoID);

        return adapterProductoDescripcion.getData() == null
                || adapterProductoDescripcion.getData().isEmpty()
                ? "N/A"
                : adapterProductoDescripcion.getData().get(0).getDescripcion();
    }

    /**
     * Método para obtener la razón social según el ID
     *
     * @param idPersona
     * @return
     */
   /* public String getPersona(Integer idPersona) {
        Persona personaID = new Persona();
        personaID.setId(idPersona);
        adapterPersona = adapterPersona.fillData(personaID);

        return adapterPersona.getData() == null
                || adapterPersona.getData().isEmpty()
                ? "N/A"
                : adapterPersona.getData().get(0).getRazonSocial();
    }*/

    /**
     * Método para mostrar panel
     */
    public void showPanel() {
         personaShow = new PersonaRol();

        if (rolPersonaCreate.getIdRol() == 1) {
            showComprador = false;
            showProveedor = true;
            personaShow.setIdRol(2);
            personaShow.setEstado("A");
            adapterPersonaRolCreate = adapterPersonaRolCreate.fillData(personaShow);
        } else {
            showProveedor = false;
            showComprador = true;
            personaShow.setIdRol(1);
            personaShow.setEstado("A");
            adapterPersonaRolCreate = adapterPersonaRolCreate.fillData(personaShow);
        }
    }

    /**
     * Método para cambiar el estado de la asociación
     *
     * @param proveedorComprador
     */
    public void changeStatus(ProveedorComprador proveedorComprador) {
        proveedorCompradorEdit.setIdComprador(proveedorComprador.getIdComprador());
        proveedorCompradorEdit.setIdProveedor(proveedorComprador.getIdProveedor());
        proveedorCompradorEdit.setIdProducto(proveedorComprador.getIdProducto());
        proveedorCompradorEdit.setEstado("I");

        proveedorCompradorEdit = proComService.createProveedorComprador(proveedorCompradorEdit);
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociación eliminada!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        filter();
    }

    /**
     * Método para crear asociación
     */
    public void createProveedorCompradorAsociacion() {

        boolean save = false;

        if (rolPersonaCreate.getIdRol() == 1) {
            for (int i = 0; i < listSelectedPersonaProveedor.size(); i++) {
                for (int j = 0; j < listSelectedProductoProveedor.size(); j++) {
                    proveedorComprador.setIdComprador(Integer.parseInt(idCliente));
                    proveedorComprador.setIdProveedor(listSelectedPersonaProveedor.get(i).getIdPersona());
                    proveedorComprador.setEstado("A");
                    proveedorComprador.setIdProducto(listSelectedProductoProveedor.get(j).getId());

                    proveedorComprador = proComService.createProveedorComprador(proveedorComprador);

                    if (proveedorComprador != null) {
                        save = true;
                    }
                }
            }
        } else {
            for (int i = 0; i < listSelectedPersonaComprador.size(); i++) {
                for (int j = 0; j < listSelectedProductoComprador.size(); j++) {
                    proveedorComprador.setIdComprador(listSelectedPersonaComprador.get(i).getIdPersona());
                    proveedorComprador.setIdProveedor(Integer.parseInt(idCliente));
                    proveedorComprador.setEstado("A");
                    proveedorComprador.setIdProducto(listSelectedProductoComprador.get(j).getId());

                    proveedorComprador = proComService.createProveedorComprador(proveedorComprador);

                    if (proveedorComprador != null) {
                        save = true;
                    }
                }
            }
        }

        if (save != false) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociación creada correctamente!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al crear la asociación");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }

        clearView();
    }

    public void clearView() {
        listSelectedPersonaComprador = new ArrayList<>();
        listSelectedPersonaProveedor = new ArrayList<>();
        listSelectedProductoComprador = new ArrayList<>();
        listSelectedProductoProveedor = new ArrayList<>();
    }

    public void filter() {
        adapterProveedorComprador.setFirstResult(0);
        this.adapterProveedorComprador = adapterProveedorComprador.fillData(proveedorCompradorFilter);

    }

    public void findRol() {
        adapterPersonaRol = adapterPersonaRol.fillData(personaRolFilter);
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clearFilter() {
        proveedorCompradorFilter = new ProveedorComprador();
        proveedorCompradorFilter.setEstado("A");
        filter();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {

            this.proveedorCompradorFilter = new ProveedorComprador();
            this.adapterProveedorComprador = new ProveedorCompradorAdapter();
            adapterProveedorComprador.setPageSize(rows);
            proveedorCompradorFilter.setEstado("A");
            filter();

            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();

            idCliente = params.get("idCliente");
            this.productoFilter = new Producto();
            this.proFilter = new Producto();
            this.adapterProductoProveedor = new ProductoAdapter();
            this.adapterProductoFilter = new ProductoAdapter();
            adapterProductoFilter = adapterProductoFilter.fillData(proFilter);
            adapterProductoProveedor = adapterProductoProveedor.fillData(productoFilter);
            this.adapterProductoDescripcion = new ProductoAdapter();
            this.adapterProductoComprador = new ProductoAdapter();
            this.productoFilterComprador = new Producto();
            adapterProductoComprador = adapterProductoComprador.fillData(productoFilterComprador);

            this.personaRolFilter = new PersonaRol();
            this.adapterPersonaRol = new PersonaRolAdapter();

            personaRolFilter.setIdPersona(idCliente == null ? null : Integer.parseInt(idCliente));
            findRol();

            rolPersonaCreate = new PersonaRol();
            this.adapterPersonaRolCreate = new PersonaRolAdapter();

            this.adapterRol = new RolListAdapter();
            showComprador = false;
            showProveedor = false;

            this.adapterPersona = new PersonaListAdapter();
            this.producto = new Producto();

            this.proComService = new ProveedorCompradorService();
            this.proveedorComprador = new ProveedorComprador();
            this.proveedorCompradorEdit = new ProveedorComprador();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
}
