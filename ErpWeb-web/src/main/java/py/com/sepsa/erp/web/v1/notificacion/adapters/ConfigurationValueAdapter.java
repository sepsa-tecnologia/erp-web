/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ConfigurationValue;
import py.com.sepsa.erp.web.v1.notificacion.remote.ConfigurationValueService;

/**
 * Adapter para configurationValue
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ConfigurationValueAdapter extends DataListAdapter<ConfigurationValue> {

    /**
     * Cliente para los servicios de Configuration value
     */
    private final ConfigurationValueService ConfigurationValueClient;

    @Override
    public ConfigurationValueAdapter fillData(ConfigurationValue searchData) {
        return ConfigurationValueClient.getConfigurationValueList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de configuration value
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConfigurationValueAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.ConfigurationValueClient = new ConfigurationValueService();
    }

    /**
     * Constructor de ConfigurationValueAdapter
     */
    public ConfigurationValueAdapter() {
        super();
        this.ConfigurationValueClient = new ConfigurationValueService();
    }

}
