
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmailNotificacion;
import py.com.sepsa.erp.web.v1.info.remote.ContactoEmailNotificacionService;

/**
 * Adaptador de la lista de contacto email
 * @author Cristina Insfrán
 */
public class ContactoEmailNotificacionAdapter extends DataListAdapter<ContactoEmailNotificacion> {
    
    /**
     * Cliente para el servicio de contacto email
     */
    private final ContactoEmailNotificacionService contactoEmailNotificacionService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoEmailNotificacionAdapter fillData(ContactoEmailNotificacion searchData) {
     
        return contactoEmailNotificacionService.getContactoEmailNotificacionList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ContactoEmailNotificacionAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoEmailNotificacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoEmailNotificacionService = new ContactoEmailNotificacionService();
    }

    /**
     * Constructor de ContactoEmailNotificacionAdapter
     */
    public ContactoEmailNotificacionAdapter() {
        super();
        this.contactoEmailNotificacionService = new ContactoEmailNotificacionService();
    }
}
