package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.Date;

/**
 * Pojo de liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionesPeriodo {

    /**
     * Identificador de hisórico de liquidación.
     */
    private Integer idHistoricoLiquidacion;
      /**
     * Identificador de hisórico de liquidación.
     */
    private Integer idContrato;
    /**
     * Fecha de proceso de liquidacion.
     */
    private Date fechaProceso;
    /**
     * Monto total excedente por liquidación.
     */
    private Integer montoTotalExcedente;
    /**
     * Monto total liquidado.
     */
    private Integer montoTotalLiquidado;
    /**
     * Monto mínimo.
     */
    private Character montoMinimo;
    /**
     * Estado de la liquidación.
     */
    private Character estado;
    /**
     * Identificador de la liquidación.
     */
    private Integer idLiquidacion;
    /**
     * Cantidad de documentos recibidos.
     */
    private Integer cantDocRecibidos;
    /**
     * Cantidad de documentos enviados.
     */
    private Integer cantDocEnviados;
    /**
     * Fecha desde.
     */
    private Date fechaDesde;
    /**
     * Fecha hasta.
     */
    private Date fechaHasta;
    /**
     * Identificador de producto.
     */
    private Integer idProducto;
    /**
     * Producto.
     */
    private String producto;
    /**
     * Identificador de servicio.
     */
    private Integer idServicio;
    /**
     * Servicio.
     */
    private String servicio;
    /**
     * Identificador del cliente.
     */
    private Integer idCliente;
    /**
     * Cliente.
     */
    private String cliente;
    /**
     * Ejecutivo Comercial.
     */
    private String comercial;
    /**
     * Cantidad de líneas.
     */
    private Integer cantLineas;
    /**
     * Año
     */
    private String ano;
    /**
     * Mes
     */
    private String mes;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public Integer getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(Integer montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public Integer getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(Integer montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }

    public Character getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Character montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public Integer getCantLineas() {
        return cantLineas;
    }

    public void setCantLineas(Integer cantLineas) {
        this.cantLineas = cantLineas;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    //</editor-fold>

    /**
     * Constructor.
     */
    public LiquidacionesPeriodo() {

    }

    public LiquidacionesPeriodo(String ano, String mes) {
        this.ano = ano;
        this.mes = mes;
    }

    /**
     * Constructor de parámetros.
     *
     * @param idHistoricoLiquidacion
     * @param fechaProceso
     * @param montoTotalExcedente
     * @param montoTotalLiquidado
     * @param montoMinimo
     * @param estado
     * @param idLiquidacion
     * @param cantDocRecibidos
     * @param cantDocEnviados
     * @param fechaDesde
     * @param fechaHasta
     * @param idProducto
     * @param producto
     * @param idServicio
     * @param servicio
     * @param idCliente
     * @param cliente
     * @param comercial
     * @param cantLineas
     * @param ano
     * @param mes
     */
    public LiquidacionesPeriodo(Integer idHistoricoLiquidacion, Date fechaProceso, Integer montoTotalExcedente,
            Integer montoTotalLiquidado, Character montoMinimo, Character estado, Integer idLiquidacion, Integer cantDocRecibidos,
            Integer cantDocEnviados, Date fechaDesde, Date fechaHasta, Integer idProducto, String producto, Integer idServicio,
            String servicio, Integer idCliente, String cliente, String comercial, Integer cantLineas, String ano,
            String mes) {

        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.fechaProceso = fechaProceso;
        this.montoTotalExcedente = montoTotalExcedente;
        this.montoTotalLiquidado = montoTotalLiquidado;
        this.montoMinimo = montoMinimo;
        this.estado = estado;
        this.idLiquidacion = idLiquidacion;
        this.cantDocRecibidos = cantDocRecibidos;
        this.cantDocEnviados = cantDocEnviados;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.comercial = comercial;
        this.cantLineas = cantLineas;
        this.ano = ano;
        this.mes = mes;
    }

}
