/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public MotivoEmisionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public MotivoEmisionFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro descripcion
     *
     * @param descripcion
     * @return
     */
    public MotivoEmisionFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param motivoEmision datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(MotivoEmision motivoEmision, Integer page, Integer pageSize) {
        MotivoEmisionFilter filter = new MotivoEmisionFilter();

        filter
                .id(motivoEmision.getId())
                .codigo(motivoEmision.getCodigo())
                .descripcion(motivoEmision.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public MotivoEmisionFilter() {
        super();
    }

}
