package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteServiceClient;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para reporte de venta
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("reporteVenta")
public class ReporteVentaController implements Serializable {

    /**
     * Objeto
     */
    private ReporteParam parametro;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private ReporteServiceClient reporteCliente;
    /**
     * Adaptador para la lista de local
     */
    private LocalListAdapter localAdapterList;
    
    private Cliente client;
    
    private ClientListAdapter adapterCliente;
    /**
     * POJO de local
     */
    private Local localFilter;
    private Integer diasFiltro;
    @Inject
    private SessionData session;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public ReporteParam getParametro() {
        return parametro;
    }

    public void setParametro(ReporteParam parametro) {
        this.parametro = parametro;
    }

    public ReporteServiceClient getReporteCliente() {
        return reporteCliente;
    }

    public void setReporteCliente(ReporteServiceClient reporteCliente) {
        this.reporteCliente = reporteCliente;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public Integer getDiasFiltro() {
        return diasFiltro;
    }

    public void setDiasFiltro(Integer diasFiltro) {
        this.diasFiltro = diasFiltro;
    }

    //</editor-fold>
    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {
        if (validacionesDownloadReporte()) {
            String contentType = ("application/vnd.ms-excel");
            String fileName = "libro-venta-factura.xlsx";
            byte[] data;
            data = reporteCliente.getReporteVenta(parametro);
            if (data != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(data),
                        contentType, fileName);               
            }
        }
        return null;
    }
    
    public void clear(){
        client = new Cliente();
        parametro = new ReporteParam();
        Calendar today = Calendar.getInstance();
        parametro.setFechaDesde(today.getTime());
        parametro.setFechaHasta(today.getTime());
        localFilter = new Local();
    }
    
    public boolean validacionesDownloadReporte() {
        boolean save = true;
        if (parametro.getIdCliente() == null && parametro.getFechaDesde() == null && parametro.getFechaHasta() == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe elegir al menos un cliente o un rango de fechas"));
            save = false;
        } else {
            if (parametro.getFechaDesde() != null && parametro.getFechaHasta() == null) {
                FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha hasta"));
                save = false;
            }else{
                if (parametro.getFechaDesde() == null && parametro.getFechaHasta() != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha desde"));
                    save = false;
                }
            }
            
            if (parametro.getFechaDesde() != null && parametro.getFechaHasta() != null) {
                Instant from = parametro.getFechaDesde().toInstant();
                Instant to = parametro.getFechaHasta().toInstant();

                long days = ChronoUnit.DAYS.between(from, to);
                Integer maxDias = 31;
                if (diasFiltro != null) {
                    maxDias = diasFiltro;
                }
                if (days > maxDias) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Fecha Incorrecta", "El rango de fecha máximo es de 31 dias"));
                    save = false;
                }
            } 
        }
        return save;
    }

    /**
     * Método para obtener local
     */
    public void getLocales() {
        this.localFilter.setLocalExterno("N");
        this.setLocalAdapterList(getLocalAdapterList().fillData(getLocalFilter()));
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setLocalExterno("N");
        local.setDescripcion(query);
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }
    
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        parametro.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocal(SelectEvent event) {
        parametro.setIdLocalTalonario(((Local) event.getObject()).getId());
    }
    
    /**
     * Método para obtener la configuración de Cantidad maxima de dias para los filtros.
     */
    public void obtenerConfiguracionDiasFiltro() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("CANTIDAD_MAXIMA_DIAS_FILTRO");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.diasFiltro = null;
            } else {
                try {
                    this.diasFiltro = Integer.parseInt(adapterConfigValor.getData().get(0).getValor());
                } catch (NumberFormatException e) {
                    this.diasFiltro = null;
                }
                
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteParam();
        this.reporteCliente = new ReporteServiceClient();
        this.localAdapterList = new LocalListAdapter();
        this.localFilter = new Local();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        Calendar today = Calendar.getInstance();
        parametro.setFechaDesde(today.getTime());
        parametro.setFechaHasta(today.getTime());
        getLocales();
        obtenerConfiguracionDiasFiltro();
    }

}
