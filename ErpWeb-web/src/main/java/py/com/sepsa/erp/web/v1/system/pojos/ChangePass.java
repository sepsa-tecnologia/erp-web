
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 * POJO para cambiar contraseña
 * @author Daniel F. Escauriza Arza
 */
public class ChangePass {
    
    /**
     * Nombre del usuario
     */
    private String usuario;
    
    /**
     * Contraseña actual
     */
    private String contrasena;
    
    /**
     * Nueva contraseña
     */
    private String nuevaContrasena;
    
    /**
     * Confirmación de la nueva contraseña
     */
    private String confirmationPass;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }


    /**
     * Obtiene la confirmación de contraseña
     * @return Confirmación de contraseña
     */
    public String getConfirmationPass() {
        return confirmationPass;
    }

    /**
     * Setea la confirmación de la contraseña
     * @param confirmationPass Confirmación de la contraseña
     */
    public void setConfirmationPass(String confirmationPass) {
        this.confirmationPass = confirmationPass;
    }

    /**
     * Constructor de ChangePass
     * @param userName Nombre del usuario
     * @param password Contraseña actual
     * @param newPassword Contraseña nueva
     */
    public ChangePass(String usuario, String contrasena, String nuevaContrasena
            ) {
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.nuevaContrasena = nuevaContrasena;
    }
    
    /**
     * Constructor de ChangePass
     * @param userName Nombre del Usuario
     * @param password Nueva contraseña
     */
    public ChangePass(String usuario,String contrasena){
        this.usuario = usuario;
        this.contrasena = contrasena;
    }
    
    /**
     * Constructor de ChangePass
     */
    public ChangePass() {}
    
}
