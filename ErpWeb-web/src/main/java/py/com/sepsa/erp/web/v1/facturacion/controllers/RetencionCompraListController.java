package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionCompraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionCompraService;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Controlador para listar de retención de compra
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("retencionCompra")
public class RetencionCompraListController implements Serializable {

    /**
     * Adaptador de la lista de retención de compra
     */
    private RetencionCompraListAdapter adapter;
    /**
     * Pojo de retención
     */
    private RetencionCompra searchData;
    /**
     * Servicio para retencion de compra
     */
    private RetencionCompraService retencionCompraService;
    /**
     * Pojo de cliente
     */
    private Persona persona;
    /**
     * Adaptador de la lista de cliente
     */
    private PersonaListAdapter personaAdapter;
    /**
     * Adaptador de estado.
     */
    private EstadoAdapter estadoAdapter;
    /**
     * Objeto estado.
     */
    private Estado estadoFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public RetencionCompraListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RetencionCompraListAdapter adapter) {
        this.adapter = adapter;
    }

    public RetencionCompra getSearchData() {
        return searchData;
    }

    public void setSearchData(RetencionCompra searchData) {
        this.searchData = searchData;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public RetencionCompraService getRetencionCompraService() {
        return retencionCompraService;
    }

    public void setRetencionCompraService(RetencionCompraService retencionCompraService) {
        this.retencionCompraService = retencionCompraService;
    }

    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    public Estado getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(Estado estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

    //</editor-fold>
    /**
     * Método para filtrar retención
     */
    public void filterRetencionCompra() {
        getAdapter().setFirstResult(0);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        searchData = new RetencionCompra();
        filterRetencionCompra();
    }

    /* Método para obtener personas filtradas
     *
     * @param query
     * @return
     */
    public List<Persona> completeQuery(String query) {
        Persona persona = new Persona();
        persona.setNombre(query);
        personaAdapter = personaAdapter.fillData(persona);
        return personaAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectPersonaFilter(SelectEvent event) {
        persona.setId(((Persona) event.getObject()).getId());
        this.searchData.setIdPersona(persona.getId());
    }

    /**
     * Método para eliminar la Retención de Compra.
     *
     * @param id
     */
    public void eliminarRetencionCompra(Integer id) {
        boolean respuesta = retencionCompraService.deleteRetencionCompra(id);
        if (respuesta == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Retención de Compra eliminada!"));
            searchData = new RetencionCompra();
            filterRetencionCompra();
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al eliminar la Retención de Compra!"));
        }
    }

    /**
     * Método para filtrar estados de retencion.
     */
    public void listarEstados() {
        this.estadoAdapter = estadoAdapter.fillData(estadoFilter);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapter = new RetencionCompraListAdapter();
        this.searchData = new RetencionCompra();
        this.retencionCompraService = new RetencionCompraService();
        this.persona = new Persona();
        this.personaAdapter = new PersonaListAdapter();
        this.estadoAdapter = new EstadoAdapter();
        this.estadoFilter = new Estado();
        this.estadoFilter.setCodigoTipoEstado("RETENCION");
        listarEstados();
        filterRetencionCompra();
    }

}
