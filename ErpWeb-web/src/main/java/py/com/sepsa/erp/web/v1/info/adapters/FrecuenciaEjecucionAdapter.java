/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.info.remote.FrecuenciaEjecucionService;

/**
 * Adapter para frecuencia ejecucion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class FrecuenciaEjecucionAdapter extends DataListAdapter<FrecuenciaEjecucion> {
    
    /**
     * Cliente para el servicio proceso
     */
    private final FrecuenciaEjecucionService frecuenciaEjecucionService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public FrecuenciaEjecucionAdapter fillData(FrecuenciaEjecucion searchData) {

        return frecuenciaEjecucionService.getFrecuenciaEjecucionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FrecuenciaEjecucionAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FrecuenciaEjecucionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.frecuenciaEjecucionService = new FrecuenciaEjecucionService();
    }

    /**
     * Constructor de FrecuenciaEjecucionAdapter
     */
    public FrecuenciaEjecucionAdapter() {
        super();
        this.frecuenciaEjecucionService = new FrecuenciaEjecucionService();
    }
}
