/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.repositorio.adapters.TipoRepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;
import py.com.sepsa.erp.web.v1.repositorio.remote.RepositorioService;

/**
 * Controlador para Crear Repositorio
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("crearRepositorio")
public class RepositorioCreateController implements Serializable {

    /**
     * Cliente para el servicio de repositorio.
     */
    private final RepositorioService service;

    /**
     * Datos del repositorio
     */
    private Repositorio repositorio;

    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;

    /**
     * Datos del cliente
     */
    private Cliente cliente;

    /**
     * Adaptador para la lista de Tipos de Repositorios
     */
    private TipoRepositorioAdapter tipoRepositorioListAdapter;

    /**
     * Pojo de tipo Repositorio
     */
    private TipoRepositorio tipoRepositorio;

    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;

    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the repositorio
     */
    public Repositorio getRepositorio() {
        return repositorio;
    }

    /**
     * @param repositorio the repositorio to set
     */
    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    /**
     * @return the clientAdapter
     */
    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    /**
     * @param clientAdapter the clientAdapter to set
     */
    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoRepositorioAdapter getTipoRepositorioListAdapter() {
        return tipoRepositorioListAdapter;
    }

    public void setTipoRepositorioListAdapter(TipoRepositorioAdapter tipoRepositorioListAdapter) {
        this.tipoRepositorioListAdapter = tipoRepositorioListAdapter;
    }

    public TipoRepositorio getTipoRepositorio() {
        return tipoRepositorio;
    }

    public void setTipoRepositorio(TipoRepositorio tipoRepositorio) {
        this.tipoRepositorio = tipoRepositorio;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    //</editor-fold>
    /**
     * Método para crear un repositorio
     */
    public void create() {

        BodyResponse<Repositorio> respuestaDeRepositorio
                = service.setRepositorio(this.repositorio);

        if (respuestaDeRepositorio.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Repositorio creado correctamente!"));
            init();
        }
    }
    
    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        repositorio.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.repositorio.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Método para obtener los Tipo de Repositorios
     */
    public void getTipoRepositorios() {
        this.setTipoRepositorioListAdapter(getTipoRepositorioListAdapter().fillData(getTipoRepositorio()));
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.repositorio = new Repositorio();
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        this.tipoRepositorioListAdapter = new TipoRepositorioAdapter();
        this.tipoRepositorio = new TipoRepositorio();
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        getTipoRepositorios();
    }

    /**
     * Constructor de BookNewController
     */
    public RepositorioCreateController() {
        init();
        this.service = new RepositorioService();
    }

}
