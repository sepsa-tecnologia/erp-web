/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionNrService;


/**
 * Adapter para motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionNrAdapter extends DataListAdapter<MotivoEmision> {

    /**
     * Cliente remoto para tipo motivoEmision
     */
    private final MotivoEmisionNrService motivoEmisionClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MotivoEmisionNrAdapter fillData(MotivoEmision searchData) {

        return motivoEmisionClient.getMotivoEmisionNr(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoEmisionNrAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.motivoEmisionClient = new MotivoEmisionNrService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public MotivoEmisionNrAdapter() {
        super();
        this.motivoEmisionClient = new MotivoEmisionNrService();
    }
}
