/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.task;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Jonathan
 */
@Startup
@Singleton
public class StartupTask {

    @EJB
    private TaskEjecutor taskEjecutor;

    @PostConstruct
    public void init() {
        taskEjecutor.asyncExecute();
    }

}