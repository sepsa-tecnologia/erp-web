package py.com.sepsa.erp.web.v1.comercial.filters;

import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.pojos.MotivoDescuento;

/**
 * Filtro utilizado para el servicio de motivo descuento
 *
 * @author Romina E. Núñez Rojas
 */
public class MotivoDescuentoFilter extends Filter {

    /**
     * Agrega el filtro de identificador 
     *
     * @param id
     * @return
     */
    public MotivoDescuentoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param descripcion
     * @return
     */
    public MotivoDescuentoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de rango desde
     *
     * @param rangoDesde 
     * @return
     */
    public MotivoDescuentoFilter rangoDesde(Double rangoDesde) {
        if (rangoDesde != null) {
            params.put("rangoDesde", rangoDesde);
        }
        return this;
    }
    
        /**
     * Agrega el filtro de rango hasta
     *
     * @param rangoHasta
     * @return
     */
    public MotivoDescuentoFilter rangoHasta(Double rangoHasta) {
        if (rangoHasta != null) {
            params.put("rangoHasta", rangoHasta);
        }
        return this;
    }

    /**
     * Agrega el filtro de porcentual
     *
     * @param porcentual
     * @return
     */
    public MotivoDescuentoFilter porcentual(String porcentual) {
        if (porcentual != null && !porcentual.trim().isEmpty()) {
            params.put("porcentual", porcentual);
        }
        return this;
    }


    /**
     * Agrega el filtro de estado
     *
     * @param estado 
     * @return
     */
    public MotivoDescuentoFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de tipo etiqueta
     *
     * @param idTipoEtiqueta
     * @return
     */
    public MotivoDescuentoFilter idTipoEtiqueta(Integer idTipoEtiqueta) {
        if (idTipoEtiqueta != null) {
            params.put("idTipoEtiqueta", idTipoEtiqueta);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipo etiqueta
     *
     * @param tipoEtiqueta
     * @return
     */
    public MotivoDescuentoFilter tipoEtiqueta(String tipoEtiqueta) {
        if (tipoEtiqueta != null && !tipoEtiqueta.trim().isEmpty()) {
            params.put("tipoEtiqueta", tipoEtiqueta);
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde 
     * @return
     */
    public MotivoDescuentoFilter fechaDesde(Date fechaDesde) {

        if (fechaDesde != null) {
            try {
                params.put("fechaDesde", URLEncoder.encode(fechaDesde.toString(), "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta 
     * @return
     */
    public MotivoDescuentoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                params.put("fechaHasta", URLEncoder.encode(fechaHasta.toString(), "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param motivoDescuento datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(MotivoDescuento motivoDescuento, Integer page, Integer pageSize) {
        MotivoDescuentoFilter filter = new MotivoDescuentoFilter();

        filter
                .id(motivoDescuento.getId())
                .descripcion(motivoDescuento.getDescripcion())
                .rangoDesde(motivoDescuento.getRangoDesde())
                .rangoHasta(motivoDescuento.getRangoHasta())
                .porcentual(motivoDescuento.getPorcentual())
                .estado(motivoDescuento.getEstado())
                .idTipoEtiqueta(motivoDescuento.getIdTipoEtiqueta())
                .tipoEtiqueta(motivoDescuento.getTipoEtiqueta())
                .fechaDesde(motivoDescuento.getFechaDesde())
                .fechaHasta(motivoDescuento.getFechaHasta())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DescuentoFilter
     */
    public MotivoDescuentoFilter() {
        super();
    }
}
