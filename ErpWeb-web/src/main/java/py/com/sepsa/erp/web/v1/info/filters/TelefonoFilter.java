package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;

/**
 * Filtro utilizado para el servicio de Telefono
 *
 * @author Williams Vera
 */
public class TelefonoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del telefono
     *
     * @param id Identificador del telefono
     * @return
     */
    public TelefonoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para idTipoTelefono
     *
     * @param idTipoTelefono
     * @return
     */
    public TelefonoFilter idTipoTelefono(Integer idTipoTelefono) {
        if (idTipoTelefono != null ) {
            params.put("idTipoTelefono", idTipoTelefono);
        }
        return this;
    }

    /**
     * Agrega el filtro de principal
     *
     * @param principal
     * @return
     */
    public TelefonoFilter principal(String principal) {
        if (principal != null && !principal.trim().isEmpty()) {
            params.put("principal", principal);
        }
        return this;
    }
    /**
     * Agrega el filtro del numero
     * @param numero Estado de local
     * @return
     */
    public TelefonoFilter numero(String numero) {
        if (numero != null && !numero.trim().isEmpty()) {
            params.put("numero", numero);
        }
        return this;
    }

    /**
     * Agrega el filtro del prefijo
     *
     * @param prefijo Estado de local
     * @return
     */
    public TelefonoFilter prefijo(String prefijo) {
        if (prefijo != null && !prefijo.trim().isEmpty()) {
            params.put("prefijo", prefijo);
        }
        return this;
    }
    
    /**
     * Agrega el filtro del activo
     *
     * @param activo Estado de local
     * @return
     */
    public TelefonoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }





    /**
     * Construye el mapa de parametros
     *
     * @param telefono datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Telefono telefono, Integer page, Integer pageSize) {
        TelefonoFilter filter = new TelefonoFilter();

        filter
                .id(telefono.getId())
                .idTipoTelefono(telefono.getIdTipoTelefono())
                .principal(telefono.getPrincipal())
                .activo(telefono.getActivo())
                .numero(telefono.getNumero())
                .prefijo(telefono.getPrefijo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TelefonoFilter
     */
    public TelefonoFilter() {
        super();
    }
}
