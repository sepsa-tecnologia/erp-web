package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Detalles
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("notaRemisionDetalle")
public class NotaRemisionDetalleController implements Serializable {

    /**
     * Servicio Nota de Remision
     */
    private NotaRemisionService serviceNotaRemision;
    /**
     * POJO Nota de Crédito
     */
    private NotaRemisionPojo notaRemisionDetalle;
    /**
     * Dato de Nota de Crédito
     */
    private String idNotaRemision;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private FileServiceClient fileServiceClient;
    private NotaRemisionDetalleListPojoAdapter adapterDetalle;
    private NotaRemisionDetallePojo detallePojoFilter;
    private Map parametros;
    private Boolean siediApiKude = false;
    private ConfiguracionValorListAdapter adapterConfigValor;
    private ConfiguracionValor configuracionValorFilter;
    
    @Inject
    private SessionData session;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Boolean getSiediApiKude() {
        return siediApiKude;
    }

    public void setSiediApiKude(Boolean siediApiKude) {
        this.siediApiKude = siediApiKude;
    }

    public Map getParametros() {
        return parametros;
    }

    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public NotaRemisionService getServiceNotaRemision() {
        return serviceNotaRemision;
    }

    public void setServiceNotaRemision(NotaRemisionService serviceNotaRemision) {
        this.serviceNotaRemision = serviceNotaRemision;
    }

    public NotaRemisionPojo getNotaRemisionDetalle() {
        return notaRemisionDetalle;
    }

    public void setNotaRemisionDetalle(NotaRemisionPojo notaRemisionDetalle) {
        this.notaRemisionDetalle = notaRemisionDetalle;
    }

    public String getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(String idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public NotaRemisionDetalleListPojoAdapter getAdapterDetalle() {
        return adapterDetalle;
    }

    public void setAdapterDetalle(NotaRemisionDetalleListPojoAdapter adapterDetalle) {
        this.adapterDetalle = adapterDetalle;
    }

    public NotaRemisionDetallePojo getDetallePojoFilter() {
        return detallePojoFilter;
    }

    public void setDetallePojoFilter(NotaRemisionDetallePojo detallePojoFilter) {
        this.detallePojoFilter = detallePojoFilter;
    }

//</editor-fold>
    /**
     * Método para obtener el detalle de la nota de remisión
     */
    public void obtenerNotaRemisionDetalle() {
        notaRemisionDetalle.setId(Integer.parseInt(idNotaRemision));
        notaRemisionDetalle.setListadoPojo(Boolean.TRUE);

        notaRemisionDetalle = serviceNotaRemision.getNotaRemisionDetalle(notaRemisionDetalle);
        
        detallePojoFilter.setListadoPojo(Boolean.TRUE);
        detallePojoFilter.setIdNotaRemision(notaRemisionDetalle.getId());
        adapterDetalle = adapterDetalle.fillData(detallePojoFilter);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String notaCreditoURL() {
        return String.format("nota-credito-list"
                + "?faces-redirect=true");
    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        parametros = new HashMap();
        String contentType = ("application/pdf");
        String fileName = notaRemisionDetalle.getNroNotaRemision() + ".pdf";
        String url = "nota-remision/consulta/" + idNotaRemision;
        parametros.put("ticket", false);
        parametros.put("siediApi", siediApiKude);
        
        byte[] data = fileServiceClient.downloadFactura(url, parametros);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }
    
    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download(Boolean ticket, Boolean xml) {
        parametros = new HashMap();
        String contentType = ("application/pdf");
        String fileName = notaRemisionDetalle.getNroNotaRemision() + ".pdf";
        String url = "nota-remision/consulta/" + idNotaRemision;
        if (notaRemisionDetalle.getDigital().equalsIgnoreCase("S")){
            parametros.put("siediApi", siediApiKude);
            if (xml){
                fileName = notaRemisionDetalle.getNroNotaRemision() + ".xml";
                url = "nota-remision/consulta-xml/" + idNotaRemision;
            } else {
                 parametros.put("ticket", ticket);
            } 
        }
        byte[] data = fileServiceClient.downloadFactura(url, parametros);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "nota-remision/consulta/" + idNotaRemision;
        if (notaRemisionDetalle.getDigital().equalsIgnoreCase("S")){
            parametros.put("siediApi", siediApiKude);
            parametros.put("ticket", false);
        }
        byte[] data = fileServiceClient.downloadFactura(url, parametros);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try ( FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    public void obtenerConfigSiediApi() {
        configuracionValorFilter = new ConfiguracionValor();
        adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("ARCHIVOS_DTE_SIEDI_API");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            siediApiKude = false;
        } else {
            siediApiKude = adapterConfigValor.getData().get(0).getValor().equalsIgnoreCase("S"); 
        }
    }
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idNotaRemision = params.get("id");

        this.serviceNotaRemision = new NotaRemisionService();
        this.notaRemisionDetalle = new NotaRemisionPojo();
        this.fileServiceClient = new FileServiceClient();
        
        this.adapterDetalle = new NotaRemisionDetalleListPojoAdapter();
        this.detallePojoFilter = new NotaRemisionDetallePojo();
        this.parametros = new HashMap();
        obtenerNotaRemisionDetalle();
        obtenerConfigSiediApi();
    }

}
