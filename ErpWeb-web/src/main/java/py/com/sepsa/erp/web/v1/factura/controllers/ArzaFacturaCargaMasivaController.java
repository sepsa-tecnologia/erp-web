package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Archivo;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.remote.LoteService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("arzaMasiva")
public class ArzaFacturaCargaMasivaController implements Serializable {
    /**
     * Archivo
     */
    private UploadedFile file;
    
    private String email;
    
    private LoteService serviceLote;
    
    @Inject
    private SessionData session;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LoteService getServiceLote() {
        return serviceLote;
    }

    public void setServiceLote(LoteService serviceLote) {
        this.serviceLote = serviceLote;
    }
    
    /**
     * Método para subir y enviar el archivo
     *
     * @throws IOException
     */
    public void upload() throws IOException {
        if (validarArchivos()) {
            Archivo archivoCreate = new Archivo();
            archivoCreate.setNombreArchivo(file.getFileName());
            String contentBase64 = Base64.getEncoder().encodeToString(file.getContents());
            archivoCreate.setContenido(contentBase64);
            archivoCreate.setCodigo("FACTURAS");

            ArrayList<Archivo> archivos = new ArrayList<>(Arrays.asList(archivoCreate));
            Lote loteCreate = new Lote();
            loteCreate.setIdEmpresa(session.getUser().getIdEmpresa());
            loteCreate.setFechaInsercion(Date.from(Instant.now()));
            loteCreate.setNombreArchivo(file.getFileName());
            loteCreate.setEmail(email);
            loteCreate.setIdUsuario(session.getUser().getId());
            loteCreate.setArchivos(archivos);
            loteCreate.setCodigoTipoLote("CARGA_MASIVA_FACTURAS_ARZA");
            BodyResponse<Lote> respuesta = serviceLote.createLote(loteCreate);
            if (respuesta.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se han enviado los archivos correctamente!"));
            }

        }
    }
        
    public boolean validarArchivos(){
        boolean valid = true;
        
        if (file == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de facturas con el formato correcto: (xlsx)"));
        }
        if (email == null || email.trim().isEmpty()) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el correo para enviar los resultados"));
        }else{
            if(!validarFormatoCorreo(email)){
                valid = false;
            }
        }
        
        return valid;
    }
    
    public boolean validarFormatoCorreo(String email) {
        boolean addEmail = false;
         
        email = email.trim().replace(" ", "");
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z]{2,})?$");

        // El email a validar
        String correo = email;

        Matcher matcher = pattern.matcher(correo);

        if (matcher.find() == true) {
            addEmail = true;

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            String msg = String.format("%s no es un email válido", email);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        return addEmail;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.file = null;
        this.email = null;
        this.serviceLote = new LoteService();
    }
}
