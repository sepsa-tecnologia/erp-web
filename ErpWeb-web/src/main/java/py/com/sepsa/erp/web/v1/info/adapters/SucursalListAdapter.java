
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.SucursalService;

/**
 * Adaptador para la lista de local
 * @author Alex T.
 */
public class SucursalListAdapter extends DataListAdapter<Local> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final SucursalService localClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public SucursalListAdapter fillData(Local searchData) {
     
        return localClient.getLocalList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public SucursalListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.localClient = new SucursalService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public SucursalListAdapter() {
        super();
        this.localClient = new SucursalService();
    }
}
