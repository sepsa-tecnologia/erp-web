package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutofacturaProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.remote.AutoFacturaService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.SepsaSiediMonitorService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ArchivoAdjunto;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.notificacion.remote.NotificacionService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;

/**
 * Controlador para listar Autofacturas
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("listarAutofactura")
public class AutoFacturaListarController implements Serializable {

    /**
     * Adaptador para la lista de facturas
     */
    private AutoFacturaPojoAdapter adapterFactura;
        /**
     * Adaptador para la lista de facturas
     */
    private AutoFacturaPojoAdapter adapterAutoFactura;
    /**
     * POJO para Factura
     */
    private AutoFacturaPojo facturaFilter;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * Servicio Factura
     */
    private AutoFacturaService serviceFactura;
    /**
     * Servicio AutoFactura
     */
    private AutoFacturaService serviceAutoFactura;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * POJO moneda
     */
    private Moneda moneda;
    /**
     * Dato de Liquidacion
     */
    private String idHistoricoLiquidacion;
    /**
     * Email Reenvio
     */
    private String emailReenvio;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    /**
     * POJO para Factura
     */
    private Factura facturaReenvio;
    private AutoFactura autoFactura;
    private Retencion retencion;
    private RetencionAdapter retencionAsociadosAdapter;
    private NotaCredito notaCredito;
    private NotaCreditoAdapter notaCreditoAsociadosAdapter;
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Pojo de procesamiento de archivo
     */
    private DocumentoProcesamiento procesamientoFilter;
    /**
     * Adaptador de la lista de procesamiento de archivo
     */
    private AutofacturaProcesamientoAdapter adapterProcesamiento;
    private Boolean addEmail;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public String getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setAddEmail(Boolean addEmail) {
        this.addEmail = addEmail;
    }

    public Boolean getAddEmail() {
        return addEmail;
    }

    public AutoFacturaPojoAdapter getAdapterAutoFactura() {
        return adapterAutoFactura;
    }

    public void setAdapterAutoFactura(AutoFacturaPojoAdapter adapterAutoFactura) {
        this.adapterAutoFactura = adapterAutoFactura;
    }

    public AutoFacturaPojo getFacturaFilter() {
        return facturaFilter;
    }

    public void setFacturaFilter(AutoFacturaPojo facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public AutoFactura getAutoFactura() {
        return autoFactura;
    }

    public void setAutoFactura(AutoFactura autoFactura) {
        this.autoFactura = autoFactura;
    }

    public AutoFacturaPojoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setAdapterFactura(AutoFacturaPojoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public AutoFacturaService getServiceAutoFactura() {
        return serviceAutoFactura;
    }

    public void setServiceAutoFactura(AutoFacturaService serviceAutoFactura) {
        this.serviceAutoFactura = serviceAutoFactura;
    }

    

    public void setProcesamientoFilter(DocumentoProcesamiento procesamientoFilter) {
        this.procesamientoFilter = procesamientoFilter;
    }


    public DocumentoProcesamiento getProcesamientoFilter() {
        return procesamientoFilter;
    }

    public AutofacturaProcesamientoAdapter getAdapterProcesamiento() {
        return adapterProcesamiento;
    }

    public void setAdapterProcesamiento(AutofacturaProcesamientoAdapter adapterProcesamiento) {
        this.adapterProcesamiento = adapterProcesamiento;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setFacturaReenvio(Factura facturaReenvio) {
        this.facturaReenvio = facturaReenvio;
    }

    public Factura getFacturaReenvio() {
        return facturaReenvio;
    }

    public void setEmailReenvio(String emailReenvio) {
        this.emailReenvio = emailReenvio;
    }

    public String getEmailReenvio() {
        return emailReenvio;
    }

    public void setIdHistoricoLiquidacion(String idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public AutoFacturaService getServiceFactura() {
        return serviceFactura;
    }

    public void setServiceFactura(AutoFacturaService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @return the adapterMotivoAnulacion
     */
    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    /**
     * @param adapterMotivoAnulacion the adapterMotivoAnulacion to set
     */
    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    /**
     * @return the motivoAnulacion
     */
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    /**
     * @param motivoAnulacion the motivoAnulacion to set
     */
    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public Retencion getRetencion() {
        return retencion;
    }

    public void setRetencion(Retencion retencion) {
        this.retencion = retencion;
    }

    public RetencionAdapter getRetencionAsociadosAdapter() {
        return retencionAsociadosAdapter;
    }

    public void setRetencionAsociadosAdapter(RetencionAdapter retencionAsociadosAdapter) {
        this.retencionAsociadosAdapter = retencionAsociadosAdapter;
    }

    public NotaCredito getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(NotaCredito notaCredito) {
        this.notaCredito = notaCredito;
    }

    public NotaCreditoAdapter getNotaCreditoAsociadosAdapter() {
        return notaCreditoAsociadosAdapter;
    }

    public void setNotaCreditoAsociadosAdapter(NotaCreditoAdapter notaCreditoAsociadosAdapter) {
        this.notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter;
    }

//</editor-fold>
    /**
     * Metodo para limpiar filtros
     */
    public void clear() {
        facturaFilter = new AutoFacturaPojo();
        asignarFiltrosFecha();
        filterFacturas();
    }

    /**
     * Método para filtrar las facturas
     */
    public void filterFacturas() {
        Instant from = facturaFilter.getFechaDesde().toInstant();
        Instant to = facturaFilter.getFechaHasta().toInstant();
        facturaFilter.setListadoPojo(true);

        long days = ChronoUnit.DAYS.between(from, to);

        if (days > 31) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fecha Incorrecta", "El rango de fecha no puede ser más de 1 mes"));
        } else {
            
            getAdapterFactura().setFirstResult(0);
            adapterFactura = adapterFactura.fillData(facturaFilter);
        }

    }

    /**
     * Método para filtrar las monedas
     */
    public void filterMoneda() {
        adapterMoneda = adapterMoneda.fillData(moneda);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleURL(Integer id) {
        return String.format("autofactura-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String notaCreditoURL(Integer id) {
        return String.format("nota-credito-create"
                + "?faces-redirect=true"
                + "&id=%d", id
        );
    }

    /**
     * Método para obtener bandera de redirección
     *
     * @param f
     * @return
     */
    public boolean allowRedirect(String cobrado, BigDecimal saldo) {
        boolean ar = false;
        if (cobrado.equals("S") || saldo.compareTo(BigDecimal.ZERO) == 0) {
            ar = false;
        } else {
            ar = true;
        }
        return ar;
    }

    /**
     * Método para mostrar el Mensaje
     */
    public void showMessage() {
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Advertencia", "No puede crearse NC relacionada a la Factura"));
    }

    /**
     * Método para anular factura
     *
     * @param factura
     */
    public void anularFactura(AutoFacturaPojo factura) {
        AutoFactura fact = new AutoFactura();
        fact.setId(factura.getId());
        fact.setIdMotivoAnulacion(factura.getIdMotivoAnulacion());
        fact.setObservacionAnulacion(factura.getObservacionAnulacion());
        fact.setIgnorarPeriodoAnulacion(ignorarPeriodoAnulacion);
        fact.setDigital(factura.getDigital());
        if (factura.getIdEstado() != null) {
            fact.setIdEstado(factura.getIdEstado());
        }
        BodyResponse<AutoFactura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.anularFactura(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura anulada correctamente!"));
            facturaFilter = new AutoFacturaPojo();
            asignarFiltrosFecha();
            filterFacturas();

        }
    }

    public void guardarFacturaAReenviar(Integer id, String nro, String email, String digital, String cdc, String cliente) {
        this.facturaReenvio = new Factura();
        this.facturaReenvio.setId(id);
        this.facturaReenvio.setNroFactura(nro);
        this.facturaReenvio.setDigital(digital);
        this.facturaReenvio.setCdc(cdc);
        this.facturaReenvio.setRazonSocial(cliente);
        this.emailReenvio = null;
        this.emailReenvio = email;

        if (emailReenvio != null) {
            addEmail = true;
        } else {
            addEmail = false;
        }
    }
    
    public String crearCobro(Integer idCliente, Integer nroFactura){
         return String.format("/app/cobro/cobro-direct-create.xhtml"
                + "?faces-redirect=true"
                + "&idCliente=%d"
                + "&nroFactura=%d",idCliente, nroFactura);
    }

    public void validarCorreo(String email) {
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void reenviar() {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String cliente = facturaReenvio.getRazonSocial();
        String mailTo = emailReenvio;
        String empresa = session.getNombreEmpresa();
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su Documento Tributario en formato PDF";
        String subjet = "Factura No. " + facturaReenvio.getNroFactura();
        String url = "factura/consulta/" + facturaReenvio.getId();
        byte[] data = fileServiceClient.download(url);
        String fileName = facturaReenvio.getNroFactura() + ".pdf";

        if (facturaReenvio.getDigital().equalsIgnoreCase("S")) {
            reenviarFactElectronica();
        } else if (facturaReenvio.getDigital().equalsIgnoreCase("N") && !mailSender.equalsIgnoreCase("N") && !passSender.equalsIgnoreCase("N")) {
            reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, facturaReenvio.getNroFactura(),host, port, empresa);
        } else {
            reenviarViaNotificacionSepsa(mensaje, subjet, mailTo, data, fileName);
        }

    }

    public void reenviarFactElectronica() {
        SepsaSiediMonitorService serviceSiediMonitor = new SepsaSiediMonitorService();
        BodyResponse<Factura> respuestaFact = new BodyResponse<>();
        respuestaFact = serviceSiediMonitor.reenviarNotificacion(facturaReenvio.getCdc());

        if (respuestaFact.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
        }

    }

    /**
     * Método para reenviar documento por correo
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     * @param from
     * @param passFrom
     * @param cliente
     * @param nroDoc
     */
    public void reenviarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa) {
        try {
            String mensaje = MailUtils.getMailHtml(nroDoc, cliente, empresa);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host, port);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para reenviar documento via sepsa notificación
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     */
    public void reenviarViaNotificacionSepsa(String msn, String subject, String to, byte[] file, String fileName) {
        Notificacion notificacion = new Notificacion();
        TipoNotificaciones tipoNotificacion = new TipoNotificaciones();
        ArchivoAdjunto archivo = new ArchivoAdjunto();
        NotificacionService servicioNoti = new NotificacionService();
        List<ArchivoAdjunto> notificationAttachments = new ArrayList<>();

        BodyResponse<Notificacion> respuestaNoti = new BodyResponse<>();

        try {
            tipoNotificacion.setCode("MAIL_ALERTAS");

            archivo.setContentType("application/pdf");
            archivo.setFileName(fileName);
            archivo.setData(file);
            notificationAttachments.add(archivo);

            notificacion.setSubject(subject);
            notificacion.setMessage(msn);
            notificacion.setTo(to);
            notificacion.setState("P");
            notificacion.setObservation("--");
            notificacion.setNotificationType(tipoNotificacion);
            notificacion.setNotificationAttachments(notificationAttachments);

            respuestaNoti = servicioNoti.createNotificacion(notificacion);

            if (respuestaNoti.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }

    /**
     *
     * @param factura
     */
    public void regenerarFactura(AutoFacturaPojo factura) {
        AutoFactura fact = new AutoFactura();
        fact.setId(factura.getId());
        fact.setEntregado(factura.getEntregado());
        fact.setImpreso(factura.getImpreso());
        fact.setArchivoEdi(factura.getArchivoEdi());
        fact.setArchivoSet(factura.getArchivoSet());
        fact.setGeneradoEdi(factura.getGeneradoEdi());
        fact.setGeneradoSet("N");
        BodyResponse<AutoFactura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.regenerarFactura(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura regenerada!"));
            facturaFilter = new AutoFacturaPojo();
            asignarFiltrosFecha();
            filterFacturas();
            PF.current().ajax().update(":list-factura-form:form-data:data-list-facturas ");
            PF.current().ajax().update(":message-form:message");

        }
    }

    /**
     * Método para obtener lista de retención
     *
     * @param idFactura
     */
    public void filterRetencion(Integer idFactura) {
        this.retencion.setIdFactura(idFactura);
        getRetencionAsociadosAdapter().setFirstResult(0);
        retencionAsociadosAdapter = retencionAsociadosAdapter.fillData(retencion);
    }

    /**
     * Método para obtener nota de credito
     *
     * @param idFactura
     */
    public void filterNotaCredito(Integer idFactura) {
        this.notaCredito.setIdFactura(idFactura);
        getNotaCreditoAsociadosAdapter().setFirstResult(0);
        notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter.fillData(notaCredito);
    }

    /**
     * Metodo para obtener los doc asociados
     *
     * @param idFactura
     */
    public void verDocAsociados(Integer idFactura) {
        filterRetencion(idFactura);
        filterNotaCredito(idFactura);
    }

    /**
     * Metodo para obtener los doc asociados
     *
     * @param idProcesamiento
     */
    public void verProcesamiento(Integer idProcesamiento) {
        WebLogger.get().debug("PROCESAMIENTO" + idProcesamiento);
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new AutofacturaProcesamientoAdapter();
        if (idProcesamiento != null) {
            this.procesamientoFilter.setIdProcesamiento(idProcesamiento);
            this.adapterProcesamiento = adapterProcesamiento.fillData(procesamientoFilter);
            this.procesamientoFilter = this.adapterProcesamiento.getData().get(0);
        }
    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionPeriodoAnulacionFactura() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("IGNORAR_PERIODO_ANULACION_FACTURA");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ignorarPeriodoAnulacion = false;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.ignorarPeriodoAnulacion = true;
        } else {
            this.ignorarPeriodoAnulacion = false;
        }

    }

    /**
     * Método para obtener fechas desde y hasta para filtro
     */
    public void asignarFiltrosFecha() {
        Calendar today = Calendar.getInstance();
        facturaFilter.setFechaHasta(today.getTime());

        today.add(Calendar.DATE, -10);
        facturaFilter.setFechaDesde(today.getTime());

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.serviceFactura = new AutoFacturaService();
        this.adapterFactura = new AutoFacturaPojoAdapter();
        this.serviceAutoFactura = new AutoFacturaService();
        this.adapterAutoFactura = new AutoFacturaPojoAdapter();
        this.facturaFilter = new AutoFacturaPojo();
        asignarFiltrosFecha();
        filterFacturas();
        this.moneda = new Moneda();
        this.adapterMoneda = new MonedaAdapter();
        filterMoneda();
        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();
        this.retencion = new Retencion();
        this.retencionAsociadosAdapter = new RetencionAdapter();
        this.notaCredito = new NotaCredito();
        this.notaCreditoAsociadosAdapter = new NotaCreditoAdapter();
        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("AUTOFACTURA");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
        this.emailReenvio = null;
        this.fileServiceClient = new FileServiceClient();
        this.facturaReenvio = new Factura();
        this.ignorarPeriodoAnulacion = false;
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new AutofacturaProcesamientoAdapter();
        obtenerConfiguracionPeriodoAnulacionFactura();
        this.addEmail = false;
        
    }
}
