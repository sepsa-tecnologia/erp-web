
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;

/**
 * Filtro para el servicio tipo email
 * @author Cristina Insfrán
 */
public class TipoEmailFilter extends Filter{
     /**
     * Agrega el filtro de identificador del tipo email
     *
     * @param id Identificador del rol
     * @return
     */
    public TipoEmailFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de la descripción
     * @param descripcion descripción del email
     * @return 
     */
    public TipoEmailFilter descripcion(String descripcion){
         if(descripcion!= null && !descripcion.trim().isEmpty()){
           params.put("descripcion", descripcion);
         }
         return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param tipoemail datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoEmail tipoemail, Integer page, Integer pageSize) {
        TipoEmailFilter filter = new TipoEmailFilter();

        filter
                .id(tipoemail.getId())
                .descripcion(tipoemail.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor TipoEmailFilter
     */
    public TipoEmailFilter() {
        super();
    }
    
}
