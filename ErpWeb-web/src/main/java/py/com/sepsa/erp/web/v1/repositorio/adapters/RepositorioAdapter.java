/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;
import py.com.sepsa.erp.web.v1.repositorio.remote.RepositorioService;

/**
 * Adaptador de Repositorio
 *
 * @author Romina Núñez
 */
public class RepositorioAdapter extends DataListAdapter<Repositorio> {

    /**
     * Cliente para los servicios de Repositorio
     */
    private final RepositorioService repositorioClient;

    @Override
    public RepositorioAdapter fillData(Repositorio searchData) {
        return repositorioClient.getRepositorioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de RepositorioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public RepositorioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.repositorioClient = new RepositorioService();
    }

    /**
     * Constructor de RetencionAdapter
     */
    public RepositorioAdapter() {
        super();
        this.repositorioClient = new RepositorioService();
    }

}
