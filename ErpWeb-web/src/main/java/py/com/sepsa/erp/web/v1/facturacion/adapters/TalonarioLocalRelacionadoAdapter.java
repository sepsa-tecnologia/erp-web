
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioLocal;
import py.com.sepsa.erp.web.v1.facturacion.remote.TalonarioService;

/**
 * Adaptador para la lista de talonario local relacionado
 * @author Romina Núñez
 */
public class TalonarioLocalRelacionadoAdapter extends DataListAdapter<TalonarioLocal>{
    
    /**
     * Cliente para los servicios de Talonario Local
     */
    private final TalonarioService talonarioLocalService;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return TalonarioLocalRelacionadoAdapter
     */
    @Override
    public TalonarioLocalRelacionadoAdapter fillData(TalonarioLocal searchData) {
     
        return talonarioLocalService.getTalonarioLocalRelacionadoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de TalonarioLocalRelacionadoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TalonarioLocalRelacionadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.talonarioLocalService = new TalonarioService();
    }

    /**
     * Constructor de TalonarioLocalRelacionadoAdapter
     */
    public TalonarioLocalRelacionadoAdapter() {
        super();
        this.talonarioLocalService= new TalonarioService();
    }
}
