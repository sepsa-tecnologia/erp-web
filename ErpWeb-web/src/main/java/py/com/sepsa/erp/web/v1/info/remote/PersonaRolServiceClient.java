
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaRolAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaRolAsociadoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaRolFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de persona rol
 * @author Cristina Insfrán
 */
public class PersonaRolServiceClient extends APIErpCore{
    
    
     /**
     * Método para crear persona rol
     *
     * @param personaRol
     * @return
     */
    public PersonaRol setPersonaRol(PersonaRol  personaRol) {

        PersonaRol prol = new PersonaRol();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PERSONA_ROL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaRol));

            response = BodyResponse.createInstance(conn, PersonaRol.class);
            
            prol = (PersonaRol) response.getPayload();
        
           
        }
        return prol;
    }

    /**
     * Obtiene la lista de personaRol
     *
     * @param personaRol 
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaRolAdapter getPersonaRol(PersonaRol personaRol, Integer page,
            Integer pageSize) {

        PersonaRolAdapter lista = new PersonaRolAdapter();

        Map params = PersonaRolFilter.build(personaRol, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_ROL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaRolAdapter.class);

            lista = (PersonaRolAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }    
    
       /**
     * Obtiene la lista de personaRol
     *
     * @param personaRol 
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaRolAsociadoAdapter getPersonaRolAsociado(PersonaRol personaRol, Integer page,
            Integer pageSize) {

        PersonaRolAsociadoAdapter lista = new PersonaRolAsociadoAdapter();

        Map params = PersonaRolFilter.build(personaRol, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_ROL_ASOC.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaRolAsociadoAdapter.class);

            lista = (PersonaRolAsociadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }    
    /**    
     /**
     * Constructor de PersonaRolServiceClient
     */
    public PersonaRolServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        PERSONA_ROL("Servicio Persona Rol", "persona-rol"),
        PERSONA_ROL_ASOC("Servicio Persona Rol", "persona-rol/asociado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
