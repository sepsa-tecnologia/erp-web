/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoDocumentoService;

/**
 * Adapter para Tipo Documento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoAdapter extends DataListAdapter<TipoDocumento> {

    /**
     * Cliente para el servicio de LiquidacionProducto
     */
    private final TipoDocumentoService serviceTipoDocumento;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoDocumentoAdapter fillData(TipoDocumento searchData) {

        return serviceTipoDocumento.getTipoDocumentoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoDocumentoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoDocumento = new TipoDocumentoService();
    }

    /**
     * Constructor de TalonarioAdapter
     */
    public TipoDocumentoAdapter() {
        super();
        this.serviceTipoDocumento = new TipoDocumentoService();
    }
}
