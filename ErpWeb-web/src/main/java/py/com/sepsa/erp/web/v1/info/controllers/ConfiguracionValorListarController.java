package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;

/**
 * Controlador para Listar Configuracion
 *
 * @author Crisitina Insfrán, Romina Nuñez, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarConfiguracionValor")
public class ConfiguracionValorListarController implements Serializable {

    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorEdit;
    /**
     * Adaptador para la lista de configuracion
     */
    private ConfiguracionListAdapter adapterConfig;
    /**
     * POJO Configuración
     */
    private Configuracion configuracionFilter;
    /**
     * Service configuracion
     */
    private ConfiguracionValorServiceClient serviceConfiguracionValor;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setServiceConfiguracionValor(ConfiguracionValorServiceClient serviceConfiguracionValor) {
        this.serviceConfiguracionValor = serviceConfiguracionValor;
    }

    public ConfiguracionValorServiceClient getServiceConfiguracionValor() {
        return serviceConfiguracionValor;
    }

    public void setConfiguracionValorEdit(ConfiguracionValor configuracionValorEdit) {
        this.configuracionValorEdit = configuracionValorEdit;
    }

    public ConfiguracionValor getConfiguracionValorEdit() {
        return configuracionValorEdit;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public ConfiguracionListAdapter getAdapterConfig() {
        return adapterConfig;
    }

    public void setAdapterConfig(ConfiguracionListAdapter adapterConfig) {
        this.adapterConfig = adapterConfig;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public Configuracion getConfiguracionFilter() {
        return configuracionFilter;
    }

    public void setConfiguracionFilter(Configuracion configuracionFilter) {
        this.configuracionFilter = configuracionFilter;
    }
    //</editor-fold>

    /**
     * Método para filtrar configuración valor
     */
    public void filter() {
        this.adapterConfigValor.setFirstResult(0);
        this.adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        configuracionFilter = new Configuracion();
        empresaAutocomplete = new Empresa();
        configuracionValorFilter = new ConfiguracionValor();
        filter();
    }

    /**
     * Método para filtrar configuración
     */
    public void filterConfiguracion() {
        this.adapterConfig = adapterConfig.fillData(configuracionFilter);
    }

    /**
     * Método para editar el cobro
     *
     * @param event
     */
    public void onRowEditConfig(RowEditEvent event) {
        configuracionValorEdit = new ConfiguracionValor();

        configuracionValorEdit.setId(((ConfiguracionValor) event.getObject()).getId());
        configuracionValorEdit.setIdConfiguracion(((ConfiguracionValor) event.getObject()).getIdConfiguracion());
        configuracionValorEdit.setValor(((ConfiguracionValor) event.getObject()).getValor());
        configuracionValorEdit.setActivo(((ConfiguracionValor) event.getObject()).getActivo());
        configuracionValorEdit.setIdEmpresa(((ConfiguracionValor) event.getObject()).getIdEmpresa());

        configuracionValorEdit = serviceConfiguracionValor.editConfiguracionValor(configuracionValorEdit);

        if (configuracionValorEdit != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Error al realizar el cambio!"));
        }
    }

    /**
     * Método para cambiar el estado del usuario
     *
     * @param user
     */
    public void onChangeStateConfig(ConfiguracionValor cv) {

        ConfiguracionValor configValorNuevo = new ConfiguracionValor();

        switch (cv.getActivo()) {
            case "S":
                configValorNuevo.setActivo("N");
                break;
            case "N":
                configValorNuevo.setActivo("S");
                break;
            default:
                break;
        }

        configValorNuevo.setId(cv.getId());
        configValorNuevo.setIdConfiguracion(cv.getIdConfiguracion());
        configValorNuevo.setValor(cv.getValor());
        configValorNuevo.setIdEmpresa(cv.getIdEmpresa());

        configValorNuevo = serviceConfiguracionValor.editConfiguracionValor(configValorNuevo);

        if (configValorNuevo != null) {
            filter();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Configuracion> completeQuery(String query) {

        Configuracion configuracion = new Configuracion();
        configuracion.setDescripcion(query);

        adapterConfig = adapterConfig.fillData(configuracion);

        return adapterConfig.getData();
    }

    /**
     * Selecciona configuracion
     *
     * @param event
     */
    public void onItemSelectConfiguracionFilter(SelectEvent event) {
        configuracionValorFilter.setIdConfiguracion(((Configuracion) event.getObject()).getId());
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        configuracionValorFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        filter();
        this.adapterConfig = new ConfiguracionListAdapter();
        this.configuracionFilter = new Configuracion();
        filterConfiguracion();
        this.serviceConfiguracionValor = new ConfiguracionValorServiceClient();
    }
}
