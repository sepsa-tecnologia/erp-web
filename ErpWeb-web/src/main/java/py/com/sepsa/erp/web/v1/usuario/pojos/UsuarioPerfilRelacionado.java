
package py.com.sepsa.erp.web.v1.usuario.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 *  POJO para usuario perfil relacionado
 * @author Romina Núñez
 */
public class UsuarioPerfilRelacionado {
       /**
     * Identificador de usuario perfil
     */
    private Integer id;
     /**
     * Identificador del Usuario
     */
    private Integer idUsuario;
    /**
     * Bandera para perfiles de sepsa
     */
    private String perfilesSepsa;
    /**
     * Identificador del perfil
     */
    private Integer idPerfil;
    /**
     * Descripción del perfil
     */
    private Perfil perfil;
    /**
     * Identificador del software
     */
    private Integer idSoftware;
    /**
     * Descripción del software
     */
    private String software;
        /**
     * Estado de la relación
     */
    private Estado estado;
    /**
     * Código Estado
     */
    private String codigoEstado;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPerfilesSepsa() {
        return perfilesSepsa;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {

        return estado;
    }

    public void setPerfilesSepsa(String perfilesSepsa) {
        this.perfilesSepsa = perfilesSepsa;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(Integer idSoftware) {
        this.idSoftware = idSoftware;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getSoftware() {
        return software;
    }
   
    
    /**
     * Constructor 
     */
    public UsuarioPerfilRelacionado(){
        
    }
   
}
