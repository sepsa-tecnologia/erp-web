package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO de Moneda
 *
 * @author Romina E. Núñez Rojas
 */
public class Moneda {
    /**
     * Identidicador del tipo tarifa
     */
    private Integer id;
    /**
     * Descripción del tipo tarifa
     */
    private String descripcion;
    /**
     * Código de la moneda
     */
    private String codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
    

    /**
     * Contructor de la clase
     */
    public Moneda() {

    }
    
    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion
     * @param codigo 
     */
    public Moneda(Integer id, String descripcion, String codigo) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
    }

    

    

}
