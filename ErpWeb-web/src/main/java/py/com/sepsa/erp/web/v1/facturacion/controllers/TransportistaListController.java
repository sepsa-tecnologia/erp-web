/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TransportistaListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.TransportistaService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("transportistaList")
public class TransportistaListController implements Serializable {
    
    private TransportistaListAdapter adapterTransportista;
    
    private Transportista transportistaFilter;
    
    private String nroDocumento;
    
    private TransportistaService transportistaService;

    
    public TransportistaService getTransportistaService() {
        return transportistaService;
    }

    public void setTransportistaService(TransportistaService transportistaService) {
        this.transportistaService = transportistaService;
    }
    
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }
    
    public TransportistaListAdapter getAdapterTransportista() {
        return adapterTransportista;
    }

    public void setAdapterTransportista(TransportistaListAdapter adapterTransportista) {
        this.adapterTransportista = adapterTransportista;
    }

    public Transportista getTransportistaFilter() {
        return transportistaFilter;
    }

    public void setTransportistaFilter(Transportista transportistaFilter) {
        this.transportistaFilter = transportistaFilter;
    }
    
    public void filterTransportista(){
        transportistaFilter.setIdEmpresa(Integer.BYTES);
    }
    
    public void buscarTransportista(){
        adapterTransportista = adapterTransportista.fillData(transportistaFilter);
    }
    
        /**
     * Método para filtrar productos.
     */
    public void buscar() {
        adapterTransportista.setFirstResult(0);
        this.adapterTransportista = adapterTransportista.fillData(transportistaFilter);
    }
    
    public void filtrarTransportista() {
        try{
            if (!nroDocumento.trim().replace(" ","").isEmpty()  && nroDocumento.trim().replace(" ","").contains("-")){
                String[] doc = nroDocumento.split("-");
                Integer nroVerificador = Integer.parseInt(doc[1]);
                transportistaFilter.setRuc(doc[0]);
                transportistaFilter.setVerificadorRuc(nroVerificador);
            } else {
                transportistaFilter.setNroDocumento(nroDocumento);
            }
            buscar();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error","Debe ingresar un número de documento válido"));
        }              
        
    }
    
    public void clear() {
        this.transportistaFilter = new Transportista();
        this.nroDocumento  = "";
        this.adapterTransportista = adapterTransportista.fillData(transportistaFilter);
    }
    
        /**
     * Método para redireccionar a la página de edición
     * @param id
     * @return 
     */
    public String editUrl(Integer id) {
        return String.format("transportista-edit?faces-redirect=true&id=%d", id);
    }
    
    
    /**
     * Método para cambiar el estado del vehículo
     *
     * @param vt
     */
    public void onChangeStateConfig(Transportista trans) {

        BodyResponse response = new BodyResponse();

        if (trans.getActivo() == null) {
            trans.setActivo("S");
        } else {
            switch (trans.getActivo()) {
                case "S":
                    trans.setActivo("N");
                    break;
                case "N":
                    trans.setActivo("S");
                    break;
                default:
                    break;
            }
        }
        
        response = transportistaService.editTransportista(trans);

        if (response != null) {
            buscar();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        }
    }
    
    public String getDocumentoTransportista(Transportista trans) {
        try {
        if (trans.getIdNaturalezaTransportista() != null && trans.getIdNaturalezaTransportista() == 1) {
            String rucT = String.format("%s-%s", trans.getRuc(),trans.getVerificadorRuc().toString());
            return rucT;
        } else if(trans.getIdNaturalezaTransportista() != null && trans.getIdNaturalezaTransportista() == 2) {
            return trans.getNroDocumentoChofer();
        }
        return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    @PostConstruct()
    public void init(){
        this.adapterTransportista = new TransportistaListAdapter();
        this.transportistaFilter = new Transportista();
        this.transportistaService = new TransportistaService();
        buscarTransportista();
    }
}
