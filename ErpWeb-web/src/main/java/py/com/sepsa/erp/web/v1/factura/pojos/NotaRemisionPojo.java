package py.com.sepsa.erp.web.v1.factura.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;

/**
 * POJO para Nota de Remision
 *
 * @author Romina Núñez
 */
public class NotaRemisionPojo {

    /**
     * Identificador del tipo de dato
     */
    private Integer id;
    /**
     * Identificador del cliente
     */
    private Integer idEmpresa;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de persona/cliente
     */
    private Integer idPersona;
    /**
     * Cod Moneda
     */
    private String codigoMoneda;
    /**
     * Número Nota de Remision
     */
    private String nroNotaRemision;
    /**
     * Número Nota de Crédito
     */
    private String cdc;
    /**
     * Número Nota de Crédito
     */
    private String timbrado;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Razón Social
     */
    private String razonSocial;
    /**
     * Razón Social
     */
    private String ruc;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Digital
     */
    private String digital;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * Impuesto
     */
    private String impreso;
    /**
     * Entregado
     */
    private String entregado;

    /**
     * Identificador del talonario
     */
    private Integer idTalonario;
    /**
     * Moneda
     */
    private String moneda;

    /**
     * Direccion
     */
    private Talonario talonario;
    /**
     * RUC
     */
    private Empresa empresa;

    /**
     * Identificador del tipo de dato
     */
    private Integer idMotivoAnulacion;
    /**
     * Descripción Motivo Emision Nr
     */
    private String motivoEmisionNr;
    /**
     * Motivo Anulación
     */
    private String observacionAnulacion;
    /**
     * Detalles
     */
    private List<NotaRemisionDetalle> notaRemisionDetalles = new ArrayList<>();
    /**
     * Identificador de emisión del motivo Nr
     */
    private Integer idMotivoEmisionNr;
    /**
     * Número Nota de Crédito
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Archivo Set
     */
    private String archivoSet;
    /**
     * Nro casa
     */
    private String nroCasa;
    /**
     * Punto de expedicion
     */
    private String puntoExpedicion;
    /**
     * Establecimiento
     */
    private String establecimiento;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * direccion
     */
    private String direccion;
    /**
     * Generado Set
     */
    private String generadoSet;
    /**
     * Codigo de Estado
     */
    private String codigoEstado;
    /**
     * Dato para filtro de listado
     */
    private Boolean listadoPojo;
            /**
     * Clave de pago
     */
    private Integer idNaturalezaCliente;
    /**
     * Datos Adicionales
     */
    private String observacion;
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public void setGeneradoSet(String generadoSet) {
        this.generadoSet = generadoSet;
    }

    public String getGeneradoSet() {
        return generadoSet;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    /**
     *
     * @return the archivo set
     */
    public String getArchivoSet() {
        return archivoSet;
    }

    /**
     *
     * @param archivoSet
     */
    public void setArchivoSet(String archivoSet) {
        this.archivoSet = archivoSet;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    /**
     * @return the idMotivoAnulacion
     */
    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    /**
     * @param idMotivoAnulacion the idMotivoAnulacion to set
     */
    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    /**
     * @return the observacionAnulacion
     */
    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    /**
     * @param observacionAnulacion the observacionAnulacion to set
     */
    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Integer getId() {
        return id;
    }

  
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getImpreso() {
        return impreso;
    }

    public void setImpreso(String impreso) {
        this.impreso = impreso;
    }

    public String getEntregado() {
        return entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public List<NotaRemisionDetalle> getNotaRemisionDetalles() {
        return notaRemisionDetalles;
    }

    public void setNotaRemisionDetalles(List<NotaRemisionDetalle> notaRemisionDetalles) {
        this.notaRemisionDetalles = notaRemisionDetalles;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getMotivoEmisionNr() {
        return motivoEmisionNr;
    }

    public void setMotivoEmisionNr(String motivoEmisionNr) {
        this.motivoEmisionNr = motivoEmisionNr;
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    //</editor-fold>
    /**
     * Constructor por defecto
     */
    public NotaRemisionPojo() {
    }

}
