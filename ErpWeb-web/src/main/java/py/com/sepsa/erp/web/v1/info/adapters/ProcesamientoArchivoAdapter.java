package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ProcesamientoArchivo;
import py.com.sepsa.erp.web.v1.info.remote.ProcesamientoArchivoService;

/**
 * Adaptador para lista de procesamiento de archivos
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class ProcesamientoArchivoAdapter extends DataListAdapter<ProcesamientoArchivo> {

    /**
     * Cliente remoto para procesamiento archivo
     */
    private final ProcesamientoArchivoService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProcesamientoArchivoAdapter fillData(ProcesamientoArchivo searchData) {
        return service.getProcesamientoArchivoList(searchData,
                getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ProcesamientoArchivoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProcesamientoArchivoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new ProcesamientoArchivoService();
    }

    /**
     * Constructor de ProcesamientoArchivoAdapter
     */
    public ProcesamientoArchivoAdapter() {
        super();
        this.service = new ProcesamientoArchivoService();
    }
}
