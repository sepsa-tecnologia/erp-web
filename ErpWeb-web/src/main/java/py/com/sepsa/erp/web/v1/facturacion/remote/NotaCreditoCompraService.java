package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.NotaCreditoCompraFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Nota de Credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotaCreditoCompraService extends APIErpFacturacion {

    /**
     * Obtiene la lista de nota de credito compra
     *
     * @param notaCreditoCompra Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoCompraAdapter getNotaCreditoCompraList(NotaCreditoCompra notaCreditoCompra, Integer page,
            Integer pageSize) {

        NotaCreditoCompraAdapter lista = new NotaCreditoCompraAdapter();

        Map params = NotaCreditoCompraFilter.build(notaCreditoCompra, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_COMPRA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoCompraAdapter.class);

            lista = (NotaCreditoCompraAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * 
     * @param idNotaCredito identificador de NC
     * @return 
     */
    public Boolean deleteNotaCreditoCompra(Integer idNotaCredito) {
        BodyResponse response = new BodyResponse();
        String URL = "nota-credito-compra/" + idNotaCredito;
        HttpURLConnection conn = DELETE(URL, ContentType.JSON);
        if (conn != null) {
           
            response = BodyResponse.createInstance(conn, NotaCreditoCompra.class);
        }
        return response.getSuccess();
    }

    /**
     * Constructor
     */
    public NotaCreditoCompraService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        NOTA_CREDITO_COMPRA("Nota Credito Compra", "nota-credito-compra"),
        NOTA_CREDITO_COMPRA_DETALLE("nota credito compra detalle", "nota-credito-compra-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
