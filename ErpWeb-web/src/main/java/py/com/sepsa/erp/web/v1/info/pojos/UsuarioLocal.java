package py.com.sepsa.erp.web.v1.info.pojos;

import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * POJO de usuario local
 *
 * @author Cristina Insfrán
 */
public class UsuarioLocal {
    
    /**
     * Identificador de Usuario Local
     */
    private Integer id;
    /**
     * Identificador del local
     */
    private Integer idLocal;
    /**
     * Identificador del usuario
     */
    private Integer idUsuario;
    /**
     * Pojo Usuario
     */
    private Usuario usuario;
    /**
     * Nombre del local
     */
    private Local local;
    /**
     * Identificador de Persona
     */
    private Integer idPersona;
    /**
     *  Dato de estado
     */
    private String activo;

    public Integer getId() {
        return id;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getActivo() {
        return activo;
    }

    /**
     * Constructor
     */
    public UsuarioLocal() {

    }

}
