package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.DocumentoProcesamientoFilter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para Factura Procesamiento
 *
 * @author Romina N
 */
public class NotaCreditoProcesamientoService extends APIErpFacturacion {

    /**
     * Método que obtiene el listado de menu
     *
     * @param fp  objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoProcesamientoAdapter getNCProcesamientoList(DocumentoProcesamiento fp, Integer page,
            Integer pageSize) {

        NotaCreditoProcesamientoAdapter lista = new NotaCreditoProcesamientoAdapter();

        Map params = DocumentoProcesamientoFilter.build(fp, page, pageSize);

        HttpURLConnection conn = GET(Resource.NC_PROCESAMIENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoProcesamientoAdapter.class);

            lista = (NotaCreditoProcesamientoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de la clase
     */
    public NotaCreditoProcesamientoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        NC_PROCESAMIENTO("Factura Procesamiento", "nota-credito-procesamiento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
