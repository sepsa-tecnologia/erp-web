
package py.com.sepsa.erp.web.v1.facturacion.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoTransaccion;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoTransaccionService;

/**
 * Adaptador para la lista de TipoTransaccion
 * @author Williams Vera
 */

public class TipoTransaccionAdapter extends DataListAdapter<TipoTransaccion> {
    
    /**
     * Cliente para los servicios de tipoTransaccion
     */
    private final TipoTransaccionService serviceTipoTransaccion;

    /**
     * Constructor de MonedaAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoTransaccionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoTransaccion = new TipoTransaccionService();
    }

    /**
     * Constructor de TipoTransaccionAdapter
     */
    public TipoTransaccionAdapter() {
        super();
        this.serviceTipoTransaccion= new TipoTransaccionService();
    }

    @Override
    public TipoTransaccionAdapter fillData(TipoTransaccion searchData) {
        return serviceTipoTransaccion.getTipoTransaccionList(searchData, getFirstResult(), getPageSize());
    }

}
