/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.ResguardoDocumentoFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.ResguardoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ResguardoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Resguardo Documento
 *
 * @author Romina Núñez
 */
public class ResguardoDocumentoService extends APIErpFacturacion {

    /**
     * Obtiene la lista
     *
     * @param resguardoDocumento Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public ResguardoDocumentoAdapter getResguardoDocumento(ResguardoDocumento resguardoDocumento, Integer page,
            Integer pageSize) {

        ResguardoDocumentoAdapter lista = new ResguardoDocumentoAdapter();

        Map params = ResguardoDocumentoFilter.build(resguardoDocumento, page, pageSize);

        HttpURLConnection conn = GET(Resource.RESGUARDO_DOCUMENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ResguardoDocumentoAdapter.class);

            lista = (ResguardoDocumentoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la url
     *
     * @param id  identificador
     * @return
     */
    public ResguardoDocumento getURLDescarga(Integer id) {

        ResguardoDocumento rd = new ResguardoDocumento();

        Map params = new HashMap();
        String url = "resguardo-documento/url-descarga/" + id;

        HttpURLConnection conn = GET(url,ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ResguardoDocumento.class);

            rd = (ResguardoDocumento) response.getPayload();

            conn.disconnect();
        }
        return rd;
    }

    /**
     * Constructor de clase
     */
    public ResguardoDocumentoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        RESGUARDO_DOCUMENTO("Resguardo Documento", "resguardo-documento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
