/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.pojos;

import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import java.util.Date;
import java.util.List;

/**
 * POJO de Notificaciones
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class Notificacion {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Tipo de notificacion
     */
    private TipoNotificaciones notificationType;
    /**
     * Fecha de insercion
     */
    private Date insertDate;
    /**
     * Fecha de envio
     */
    private Date sendDate;
    /**
     * Fecha desde envio
     */
    private Date fromSendDate;
    /**
     * Fecha hasta envio
     */
    private Date toSendDate;
    /**
     * Fecha desde insercion
     */
    private Date fromInsertDate;
    /**
     * Fecha hasta insercion
     */
    private Date toInsertDate;
    /**
     * Asunto de la notificacion
     */
    private String subject;
    /**
     * Destino (to)
     */
    private String to;
    /**
     * Estado de la notificacion
     */
    private String state;
    /**
     * Mensaje de la notificacion
     */
    private String message;
    private String observation;
    
    private ArchivoAdjunto notificationAttachment;
    private List <ArchivoAdjunto> notificationAttachments;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setNotificationAttachments(List<ArchivoAdjunto> notificationAttachments) {
        this.notificationAttachments = notificationAttachments;
    }

    public List<ArchivoAdjunto> getNotificationAttachments() {
        return notificationAttachments;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getObservation() {
        return observation;
    }

    public void setNotificationAttachment(ArchivoAdjunto notificationAttachment) {
        this.notificationAttachment = notificationAttachment;
    }

    public ArchivoAdjunto getNotificationAttachment() {
        return notificationAttachment;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoNotificaciones getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(TipoNotificaciones notificationType) {
        this.notificationType = notificationType;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getFromSendDate() {
        return fromSendDate;
    }

    public void setFromSendDate(Date fromSendDate) {
        this.fromSendDate = fromSendDate;
    }

    public Date getToSendDate() {
        return toSendDate;
    }

    public void setToSendDate(Date toSendDate) {
        this.toSendDate = toSendDate;
    }

    public Date getFromInsertDate() {
        return fromInsertDate;
    }

    public void setFromInsertDate(Date fromInsertDate) {
        this.fromInsertDate = fromInsertDate;
    }

    public Date getToInsertDate() {
        return toInsertDate;
    }

    public void setToInsertDate(Date toInsertDate) {
        this.toInsertDate = toInsertDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    //</editor-fold>
    
    /**
     * Contructor de la clase
     */
    public Notificacion() {

    }
}
