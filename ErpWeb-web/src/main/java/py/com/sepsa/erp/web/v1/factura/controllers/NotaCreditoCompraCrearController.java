package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("ncCompraCrear")
public class NotaCreditoCompraCrearController implements Serializable {

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean create;

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaCompraAdapter adapterFactura;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * POJO Datos Nota de Crédito
     */
    private DatosNotaCredito datoNotaCredito;
    /**
     * POJO Nota de Crédito
     */
    private NotaCredito notaCreditoCreate;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.C.
     */
    private List<NotaCreditoDetalle> listaDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato de factura
     */
    private String idFactura;
    /**
     *
     * Adaptador para la lista de motivoEmisionInterno
     *
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;

    /**
     *
     * POJO de MotivoEmisionInterno
     *
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Descripción de motivo emisión
     */
    private String motivoEmision;
    /**
     * Dato para digital
     */
    private String digital;
    private Integer idFacturaCompra;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public void setMotivoEmision(String motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public String getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {
        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;
    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {
        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;
    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {
        return motivoEmisionInternoFilter;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {
        return motivoEmisionInternoAdapterList;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListSelectedFactura(List<Factura> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public List<Factura> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListaDetalle(List<NotaCreditoDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<NotaCreditoDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setNotaCreditoCreate(NotaCredito notaCreditoCreate) {
        this.notaCreditoCreate = notaCreditoCreate;
    }

    public NotaCredito getNotaCreditoCreate() {
        return notaCreditoCreate;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

    public void setDatoNotaCredito(DatosNotaCredito datoNotaCredito) {
        this.datoNotaCredito = datoNotaCredito;
    }

    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    public DatosNotaCredito getDatoNotaCredito() {
        return datoNotaCredito;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaCompraAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaCompraAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

//</editor-fold>
    /**
     * Método para agregar detalle
     *
     * @param event
     */
    public void onCheckFactura(SelectEvent event) {
        if (show == false) {
            show = true;
        }

        List<FacturaDetalle> detallesDeFactura = new ArrayList<>();
        detallesDeFactura = ((Factura) event.getObject()).getFacturaDetalles();
        idFacturaCompra = ((Factura) event.getObject()).getId();
        notaCreditoCreate.setIdMoneda(((Factura) event.getObject()).getIdMoneda());
        notaCreditoCreate.setCodigoMoneda(((Factura) event.getObject()).getMoneda().getCodigo());
        NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

        if (listaDetalle.isEmpty()) {
            notaCreditoDetalle.setNroLinea(linea);
        } else {
            linea++;
            notaCreditoDetalle.setNroLinea(linea);
        }

        notaCreditoDetalle.setIdFactura(((Factura) event.getObject()).getId());
        String nroFactura = ((Factura) event.getObject()).getNroFactura();
        notaCreditoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);
        notaCreditoDetalle.setPorcentajeIva(10);

        notaCreditoDetalle.setIdFacturaCompra(idFacturaCompra);

        listaDetalle.add(notaCreditoDetalle);
    }

    /**
     * Método invocado para deseleccionar liquidacion
     *
     * @param event
     */
    public void onUncheckFactura(UnselectEvent event) {

        BigDecimal subTotal = new BigDecimal("0");
        NotaCreditoDetalle info = null;

        for (int i = 0; i < ((Factura) event.getObject()).getFacturaCompraDetalles().size(); i++) {
            for (NotaCreditoDetalle info0 : listaDetalle) {
                if (Objects.equals(info0.getIdFacturaCompra(), ((Factura) event.getObject()).getId())) {
                    info = info0;
                    subTotal = info.getMontoTotal();
                    listaDetalle.remove(info);
                    break;
                }
            }

            BigDecimal monto = notaCreditoCreate.getMontoTotalNotaCredito();
            BigDecimal totalNotaCredito = monto.subtract(subTotal);
            notaCreditoCreate.setMontoTotalNotaCredito(totalNotaCredito);

        }

        calcularTotales();

    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void cambiarDescripcion(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        String h = info.getDescripcion();
        WebLogger.get().debug(h);
    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontos(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotales();

    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaDetalle.get(i).getMontoTotal());
        }

        notaCreditoCreate.setMontoTotalNotaCredito(acumTotales);
        notaCreditoCreate.setMontoTotalGuaranies(acumTotales);

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        notaCreditoCreate.setMontoIva5(montoIva5Acumulador);
        notaCreditoCreate.setMontoImponible5(montoImponible5Acumulador);
        notaCreditoCreate.setMontoTotal5(montoTotal5Acumulador);
        notaCreditoCreate.setMontoIva10(montoIva10Acumulador);
        notaCreditoCreate.setMontoImponible10(montoImponible10Acumulador);
        notaCreditoCreate.setMontoTotal10(montoTotal10Acumulador);
        notaCreditoCreate.setMontoTotalExento(montoExcentoAcumulador);
        notaCreditoCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        notaCreditoCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(notaCreditoCreate.getIdMoneda());
        montoLetrasFilter.setCodigoMoneda("PYG");
        montoLetrasFilter.setMonto(notaCreditoCreate.getMontoTotalNotaCredito());

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelectVtoTimbrado(SelectEvent event) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            Date dateNC = notaCreditoCreate.getFecha();
            Date dateUtil = (Date) event.getObject();

            String dateToday = simpleDateFormat.format(dateNC);
            String dateUtilFormat = simpleDateFormat.format(dateUtil);

            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dateToday);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(dateUtilFormat);
            WebLogger.get().debug("FECHA 1:" + date1);
            WebLogger.get().debug("FECHA 2:" + date2);

            if (date1.compareTo(date2) < 0) {
                notaCreditoCreate.setFechaVencimientoTimbrado(date2);
            } else if (date1.compareTo(date2) > 0) {
                notaCreditoCreate.setFechaVencimientoTimbrado(null);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La Fecha de Vto Timbrado no puede ser inferior a la Fecha de NC!"));

            }
        } catch (ParseException ex) {
            WebLogger.get().fatal(ex);
        }

    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
        int id = ((Cliente) event.getObject()).getIdCliente();

        notaCreditoCreate = new NotaCredito();

        notaCreditoCreate.setRazonSocial(((Cliente) event.getObject()).getRazonSocial());
        notaCreditoCreate.setIdPersona(id);
        ruc = ((Cliente) event.getObject()).getNroDocumento();
        notaCreditoCreate.setRuc(ruc);

        filterFactura(id);
        showDatatable = true;

    }

    /**
     * Método para crear Nota de Crédito
     */
    public void createNotaCredito() {
        boolean saveNC = true;
        calcularTotalesGenerales();
        BigDecimal precioNoValido = new BigDecimal("0");

        notaCreditoCreate.setAnulado("N");
        notaCreditoCreate.setImpreso("N");
        notaCreditoCreate.setEntregado("N");
        notaCreditoCreate.setDigital(digital);

        for (NotaCreditoDetalle notaCreditoDetalle : listaDetalle) {
            if (precioNoValido.compareTo(notaCreditoDetalle.getPrecioUnitarioConIva()) == 0) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El precio unitario no puede ser 0!"));
            }
        }
        if (saveNC == true) {
            notaCreditoCreate.setNotaCreditoCompraDetalles(listaDetalle);

            BodyResponse<NotaCredito> respuestaDeNC = serviceNotaCredito.createNotaCreditoCompra(notaCreditoCreate);

            if (respuestaDeNC.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "NC creada correctamente!"));
                init();
            } else {
                for (Mensaje mensaje : respuestaDeNC.getStatus().getMensajes()) {

                }

            }
        }

    }

    /**
     * Método para filtrar la factura
     */
    public void filterFactura(Integer idPersona) {
        facturaFilter.setIdPersona(idPersona);
        facturaFilter.setCobrado("N");
        facturaFilter.setAnulado("N");

        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        direccion = "";
        notaCreditoCreate = new NotaCredito();
        listSelectedFactura = new ArrayList<>();
        listaDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        datoNotaCredito = new DatosNotaCredito();
    }

    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }

    /**
     * Método para obtener la factura en NC, cuando se redirecciona desde el
     * listado de facturas
     */
    public void obtenerFactura() {
        facturaFilter.setId(Integer.parseInt(idFactura));
        adapterFactura = adapterFactura.fillData(facturaFilter);
        listSelectedFactura.add(adapterFactura.getData().get(0));
        cliente.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());

      //  obtenerDatosNCDevolucionFactura();
        devolucionDetalleNC();

    }

    public void devolucionDetalleNC() {
        if (show == false) {
            show = true;
        }
        NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

        int nroBonificacion = 1;
        if (listaDetalle.isEmpty()) {
            notaCreditoDetalle.setNroLinea(linea);
        } else {
            linea++;
            notaCreditoDetalle.setNroLinea(linea);
        }

        notaCreditoDetalle.setIdFactura(adapterFactura.getData().get(0).getId());
        String nroFactura = adapterFactura.getData().get(0).getNroFactura();
        notaCreditoDetalle.setDescripcion("BONIFICACION " + nroBonificacion + " DE LA FACTURA NRO." + nroFactura);
        notaCreditoDetalle.setPorcentajeIva(10);
        notaCreditoDetalle.setMontoImponible(adapterFactura.getData().get(0).getMontoImponibleTotal());
        notaCreditoDetalle.setCantidad(new BigDecimal(1));
        notaCreditoDetalle.setPrecioUnitarioConIva(adapterFactura.getData().get(0).getMontoTotalFactura());
        notaCreditoDetalle.setPrecioUnitarioSinIva(adapterFactura.getData().get(0).getMontoImponibleTotal());
        notaCreditoDetalle.setMontoIva(adapterFactura.getData().get(0).getMontoIvaTotal());
        notaCreditoDetalle.setMontoTotal(adapterFactura.getData().get(0).getMontoTotalFactura());

        listaDetalle.add(notaCreditoDetalle);

        calcularTotales();
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void obtenerMotivoEmisionInterno() {

        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));

    }

    public void obtenerMotivoEmision() {
        MotivoEmisionInternoAdapter motivoEmisionInternoAdapter = new MotivoEmisionInternoAdapter();
        MotivoEmisionInterno motivoEmisionInterno = new MotivoEmisionInterno();
        if (notaCreditoCreate.getIdMotivoEmisionInterno() != null) {
            motivoEmisionInterno.setId(notaCreditoCreate.getIdMotivoEmisionInterno());
            motivoEmisionInternoAdapter = motivoEmisionInternoAdapter.fillData(motivoEmisionInterno);
            motivoEmision = motivoEmisionInternoAdapter.getData().get(0).getMotivoEmision().getDescripcion();
        } else {
            this.motivoEmision = "Ninguno";
        }

    }

    /**
     * Método para asignar la fecha del día
     */
    public void obtenerFechaNC() {

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.create = false;
        this.show = false;
        this.showDatatable = false;
        this.digital = "N";

        this.idFacturaCompra = null;
        this.adapterFactura = new FacturaCompraAdapter();
        this.facturaFilter = new Factura();
        this.serviceNotaCredito = new NotaCreditoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new Cliente();
        this.adapterCliente = new ClientListAdapter();

        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();

        this.notaCreditoCreate = new NotaCredito();
        Calendar today = Calendar.getInstance();
        this.notaCreditoCreate.setFecha(today.getTime());
        Date d = notaCreditoCreate.getFecha();
        WebLogger.get().debug("FECHA: " + d);
        obtenerFechaNC();

        this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
        this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());

        filterMoneda();
        obtenerMotivoEmisionInterno();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("id") != null) {
            idFactura = params.get("id");
            showDatatable = true;
            obtenerFactura();
        }

        this.motivoEmision = "Ninguno";

    }

}
