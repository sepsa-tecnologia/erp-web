package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;

/**
 * Adaptador de la lista de configuración valor
 *
 * @author Cristina Insfrán
 */
public class ConfiguracionListAdapter extends DataListAdapter<Configuracion> {

    /**
     * Cliente para el servicio de Configuracion Valor
     */
    private final ConfiguracionValorServiceClient confValorClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ConfiguracionListAdapter fillData(Configuracion searchData) {

        return confValorClient.getConfiguracionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ConfiguracionValorListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConfiguracionListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.confValorClient = new ConfiguracionValorServiceClient();
    }

    /**
     * Constructor de ConfiguracionValorListAdapter
     */
    public ConfiguracionListAdapter() {
        super();
        this.confValorClient = new ConfiguracionValorServiceClient();
    }

}
