package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoDatoPersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDatoPersona;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("proveedorList")
public class ProveedorListController implements Serializable {

    /**
     * Cliente Filter
     */
    private Cliente clienteFilter;

    /**
     * Pojo de canal de ventas
     */
    private CanalVenta canalVenta;

    /**
     * Adaptador de canal de ventas
     */
    private CanalVentaListAdapter canalVentaAdapter;

    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clienteAdapterList;
    /**
     * Adaptador para la lista de tipo Documento/ tipo dato persona
     */
    private TipoDatoPersonaListAdapter tipoDocumentoAdapterList;
    /**
     * POJO de tipo documento / tipoDato persona
     */
    private TipoDatoPersona tipoDocumentoFilter;

    /**
     * Adaptador para la lista de estados
     */
    private EstadoAdapter estadoAdapterList;
    /**
     * POJO de estado
     */
    private Estado estado;
    /**
     * Servicio Cliente
     */
    private ClientServiceClient serviceCliente;
    /**
     * Pojo de factura
     */
    private Factura facturaCliente;
    /**
     * Adaptador de la lista de facturas
     */
    private FacturaAdapter facturaAdapter;
    private Boolean show;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Boolean getShow() {
        return show;
    }

    public void setServiceCliente(ClientServiceClient serviceCliente) {
        this.serviceCliente = serviceCliente;
    }

    public ClientServiceClient getServiceCliente() {
        return serviceCliente;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public CanalVentaListAdapter getCanalVentaAdapter() {
        return canalVentaAdapter;
    }

    public void setCanalVentaAdapter(CanalVentaListAdapter canalVentaAdapter) {
        this.canalVentaAdapter = canalVentaAdapter;
    }

    public ClientListAdapter getClienteAdapterList() {
        return clienteAdapterList;
    }

    public void setClienteAdapterList(ClientListAdapter clienteAdapterList) {
        this.clienteAdapterList = clienteAdapterList;
    }

    public TipoDatoPersonaListAdapter getTipoDocumentoAdapterList() {
        return tipoDocumentoAdapterList;
    }

    public void setTipoDocumentoAdapterList(TipoDatoPersonaListAdapter tipoDocumentoAdapterList) {
        this.tipoDocumentoAdapterList = tipoDocumentoAdapterList;
    }

    public TipoDatoPersona getTipoDocumentoFilter() {
        return tipoDocumentoFilter;
    }

    public void setTipoDocumentoFilter(TipoDatoPersona tipoDocumentoFilter) {
        this.tipoDocumentoFilter = tipoDocumentoFilter;
    }

    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Factura getFacturaCliente() {
        return facturaCliente;
    }

    public void setFacturaCliente(Factura facturaCliente) {
        this.facturaCliente = facturaCliente;
    }

    public void setFacturaAdapter(FacturaAdapter facturaAdapter) {
        this.facturaAdapter = facturaAdapter;
    }

    public FacturaAdapter getFacturaAdapter() {
        return facturaAdapter;
    }

    //</editor-fold>
    /**
     * Metodo para obtener la lista de clientes
     */
    public void filterCliente() {
        getClienteAdapterList().setFirstResult(0);
        clienteFilter.setComprador("N");
        clienteFilter.setProveedor("S");
        this.clienteAdapterList = clienteAdapterList.fillData(clienteFilter);
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clearFilter() {
        canalVenta = new CanalVenta();
        clienteFilter = new Cliente();
        filterCliente();
    }

    /**
     * Método para traer la lista de facturas asociadas al cliente
     *
     * @param idCliente
     */
    public void filterFactura(Integer idCliente) {
        this.facturaAdapter = new FacturaAdapter();
        this.facturaCliente = new Factura();

        facturaCliente.setIdCliente(idCliente);
        facturaAdapter = facturaAdapter.fillData(facturaCliente);

    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoDocumentos() {
        this.setTipoDocumentoAdapterList(getTipoDocumentoAdapterList().fillData(getTipoDocumentoFilter()));
    }

    /**
     * Método para obtener estado
     */
    public void getEstados() {
        this.setEstadoAdapterList(getEstadoAdapterList().fillData(getEstado()));
    }

    /* Método para obtener canales de ventas filtrados
     *
     * @param query
     * @return
     */
    public List<CanalVenta> completeQuery(String query) {

        CanalVenta canalVenta = new CanalVenta();
        canalVenta.setDescripcion(query);

        canalVentaAdapter = canalVentaAdapter.fillData(canalVenta);

        return canalVentaAdapter.getData();
    }

    /**
     * Selecciona el canal de ventas
     *
     * @param event
     */
    public void onItemSelectCanalVentaFilter(SelectEvent event) {
        canalVenta.setId(((CanalVenta) event.getObject()).getId());
        this.clienteFilter.setIdCanalVenta(canalVenta.getId());
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idCliente
     * @return
     */
    public String contratoURL(Integer idCliente) {
        return String.format("contrato"
                + "?faces-redirect=true"
                + "&idCliente=%d", idCliente);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idCliente
     * @return
     */
    public String contactoURL(Integer idCliente) {
        return String.format("/app/client/client-contact-list.xhtml"
                + "?faces-redirect=true"
                + "&idCliente=%d", idCliente);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idCliente
     * @return
     */
    public String direccionURL(Integer idCliente, Integer idDireccion) {
        return String.format("/app/direccion/client-list-direccion.xhtml"
                + "?faces-redirect=true"
                + "&idCliente=%d"
                + "&idDireccion=%d", idCliente, idDireccion);
    }

    /**
     * Método para cambiar si es una entidad o no
     */
    public void onChangeCliente(Cliente cliente) {

        switch (cliente.getEstado().getCodigo()) {
            case "ACTIVO":
                cliente.setEstado(buscarEstadoCliente("INACTIVO"));
                cliente.setIdEstado(cliente.getEstado().getId());
                break;
            case "INACTIVO":
                cliente.setEstado(buscarEstadoCliente("ACTIVO"));
                cliente.setIdEstado(cliente.getEstado().getId());
                break;
            default:
                break;
        }
        Cliente cli = new Cliente();
        cli.setIdCliente(cliente.getIdCliente());
        cli.setEstado(buscarEstadoCliente("INACTIVO"));
        cli.setIdEstado(cliente.getEstado().getId());
        cli.setIdTipoDocumento(cliente.getIdTipoDocumento());
        cli.setFactElectronica(cliente.getFactElectronica());
        cli.setNroDocumento(cliente.getNroDocumento());
        cli.setRazonSocial(cliente.getRazonSocial());
        cli.setClavePago(cliente.getClavePago());
        BodyResponse<Cliente> respuestaCliente = serviceCliente.putCliente(cli);
        if (respuestaCliente.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Estado editado correctamente!"));

        }

    }

    public Estado buscarEstadoCliente(String codigo) {
        EstadoAdapter adapter = new EstadoAdapter();
        Estado estadoFilter = new Estado();

        estadoFilter.setCodigoTipoEstado("CLIENTE");
        estadoFilter.setCodigo(codigo);
        adapter = adapter.fillData(estadoFilter);
        return adapter.getData().get(0);
    }

    public String exportState(String estado) {

        return estado;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.show = false;
            this.clienteAdapterList = new ClientListAdapter();
            this.clienteFilter = new Cliente();
            this.canalVenta = new CanalVenta();
            this.canalVentaAdapter = new CanalVentaListAdapter();
            this.tipoDocumentoAdapterList = new TipoDatoPersonaListAdapter();
            this.tipoDocumentoFilter = new TipoDatoPersona();
            this.estadoAdapterList = new EstadoAdapter();
            this.estado = new Estado();
            this.estado.setCodigoTipoEstado("CLIENTE");
            this.serviceCliente = new ClientServiceClient();
            this.facturaAdapter = new FacturaAdapter();
            this.facturaCliente = new Factura();
            filterCliente();
            getTipoDocumentos();
            getEstados();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
