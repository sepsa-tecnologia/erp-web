
package py.com.sepsa.erp.web.v1.http.pojos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * POJO para la respuesta en formato String
 * @author Daniel F. Escauriza Arza
 */
public class HttpStringResponse extends HttpResponse<String> {
    
    /**
     * Crea una instancia de HttpStringResponse
     * @param conn Conexión HTTP
     * @return Instancia HttpStringResponse
     */
    public static HttpStringResponse createInstance(HttpURLConnection conn) {
        return new HttpStringResponse(conn);
    }
    
    /**
     * Constructor de HttpStringResponse
     * @param conn Conexión HTTP
     */
    public HttpStringResponse(HttpURLConnection conn) {
        
        try {
            int respCode = conn.getResponseCode();
            this.code = ResponseCode.OK;
            if(respCode == 200) {
                this.code = ResponseCode.OK;
            } else if(respCode == 400) {
                this.code = ResponseCode.BR;
            } else {
                this.code = ResponseCode.ERROR;
            }
            
            InputStream inputStream = respCode < 400 
                    ? conn.getInputStream() 
                    : conn.getErrorStream();
            
            if(inputStream != null) {

                BufferedReader responseBuffer = new BufferedReader(
                        new InputStreamReader(inputStream, "UTF-8"));

                String aux;
                String resp = "";

                while((aux = responseBuffer.readLine()) != null) {
                    resp += aux;
                }

                if(resp.length() > 2000) {
                    WebLogger.get().debug("La respuesta es muy larga para el log");
                } else {
                    WebLogger.get().debug(resp);
                }

                WebLogger.get().debug(""+respCode);
                
                this.input = resp;
            }
        
        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }
        
    }

    /**
     * Constructor de HttpStringResponse
     */
    public HttpStringResponse() {}
    
    
}
