/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para Editar MotivoEmision
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionEdit")
public class MotivoEmisionEditController implements Serializable {

    /**
     * Cliente para el servicio de motivoEmision.
     */
    private final MotivoEmisionService service;
    /**
     * Datos del motivoEmision
     */
    private MotivoEmision motivoEmision;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    //</editor-fold>
    /**
     * Método para editar MotivoEmision
     */
    public void edit() {
        BodyResponse<MotivoEmision> respuestaTal = service.editMotivoEmision(motivoEmision);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Motivo de Emision editado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.motivoEmision = service.get(id);
    }

    /**
     * Constructor
     */
    public MotivoEmisionEditController() {
        this.service = new MotivoEmisionService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
