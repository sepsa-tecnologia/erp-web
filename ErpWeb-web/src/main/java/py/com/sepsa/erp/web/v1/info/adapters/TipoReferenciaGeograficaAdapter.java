
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;

/**
 * Adaptador del tipo de referencia geografica
 * @author Cristina Insfrán
 */
public class TipoReferenciaGeograficaAdapter extends DataListAdapter<TipoReferenciaGeografica>{
    /**
     * Cliente remoto para Referencia Geografica
     */
    private final ReferenciaGeograficaService refGeoClient;
     /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoReferenciaGeograficaAdapter fillData(TipoReferenciaGeografica searchData) {

        return refGeoClient.getTipoRefGeograficaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoReferenciaGeograficaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoReferenciaGeograficaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.refGeoClient = new ReferenciaGeograficaService();
    }

    /**
     * Constructor de TipoReferenciaGeograficaAdapter
     */
    public TipoReferenciaGeograficaAdapter() {
        super();
        this.refGeoClient = new ReferenciaGeograficaService();
    } 
}
