package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoServicioAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.DescuentoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MotivoDescuentoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.pojos.MotivoDescuento;
import py.com.sepsa.erp.web.v1.comercial.remote.DescuentoService;

/**
 * Controlador para Contrato
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("contratoDetalle")
public class ContratoDetalleController implements Serializable {

    /**
     * Dato del Contrato
     */
    private String id;
    /**
     * Adaptador para la lista de contratos
     */
    private ContratoAdapter adapter;
    /**
     * Objeto Contrato
     */
    private Contrato contratoFilter;
    /**
     * Adaptador para la lista de descuentos
     */
    private DescuentoAdapter adapterDescuento;
    /**
     * Objeto Descuento
     */
    private Descuento descuentoFilter;
    /**
     * Objeto Motivo Descuento
     */
    private MotivoDescuento motivoDescuentoFilter;
    /**
     * Adaptador para la lista de motivo descuento
     */
    private MotivoDescuentoAdapter adapterMotivoDescuento;
    /**
     * Objeto Descuento a ser enviado
     */
    private Descuento descuentoCreate;
    /**
     * Servicio Descuento
     */
    private DescuentoService serviceDescuento;
    private ContratoServicioAdapter adapterContratoServicio;
    private ContratoServicio contratoServicioFilter;
    private ClientListAdapter adapterCliente;
    private Cliente clienteFilter;
    private String idCliente;

    public void setId(String id) {
        this.id = id;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setContratoServicioFilter(ContratoServicio contratoServicioFilter) {
        this.contratoServicioFilter = contratoServicioFilter;
    }

    public void setAdapterContratoServicio(ContratoServicioAdapter adapterContratoServicio) {
        this.adapterContratoServicio = adapterContratoServicio;
    }

    public ContratoServicio getContratoServicioFilter() {
        return contratoServicioFilter;
    }

    public ContratoServicioAdapter getAdapterContratoServicio() {
        return adapterContratoServicio;
    }

    public void setDescuentoCreate(Descuento descuentoCreate) {
        this.descuentoCreate = descuentoCreate;
    }

    public Descuento getDescuentoCreate() {
        return descuentoCreate;
    }

    public void setServiceDescuento(DescuentoService serviceDescuento) {
        this.serviceDescuento = serviceDescuento;
    }

    public DescuentoService getServiceDescuento() {
        return serviceDescuento;
    }

    public void setDescuentoFilter(Descuento descuentoFilter) {
        this.descuentoFilter = descuentoFilter;
    }

    public void setAdapterDescuento(DescuentoAdapter adapterDescuento) {
        this.adapterDescuento = adapterDescuento;
    }

    public Descuento getDescuentoFilter() {
        return descuentoFilter;
    }

    public DescuentoAdapter getAdapterDescuento() {
        return adapterDescuento;
    }

    public void setMotivoDescuentoFilter(MotivoDescuento motivoDescuentoFilter) {
        this.motivoDescuentoFilter = motivoDescuentoFilter;
    }

    public void setAdapterMotivoDescuento(MotivoDescuentoAdapter adapterMotivoDescuento) {
        this.adapterMotivoDescuento = adapterMotivoDescuento;
    }

    public MotivoDescuento getMotivoDescuentoFilter() {
        return motivoDescuentoFilter;
    }

    public MotivoDescuentoAdapter getAdapterMotivoDescuento() {
        return adapterMotivoDescuento;
    }

    public String getId() {
        return id;
    }

    public void setContratoFilter(Contrato contratoFilter) {
        this.contratoFilter = contratoFilter;
    }

    public Contrato getContratoFilter() {
        return contratoFilter;
    }

    public void setAdapter(ContratoAdapter adapter) {
        this.adapter = adapter;
    }

    public ContratoAdapter getAdapter() {
        return adapter;
    }

    public void filter() {
        contratoFilter.setId(Integer.parseInt(id));
        this.adapter = adapter.fillData(contratoFilter);

        contratoFilter = adapter.getData().get(0);
    }

    public void filterDescuento() {
        descuentoFilter.setIdContrato(Integer.parseInt(id));
        this.adapterDescuento = adapterDescuento.fillData(descuentoFilter);
    }

    public void filterMotivoDescuento() {
        this.adapterMotivoDescuento = adapterMotivoDescuento.fillData(motivoDescuentoFilter);
    }

    /**
     * Método para inactivar descuento
     *
     * @param id
     */
    public void inactivarDescuento(Integer id) {
        Descuento descuentoInactivado = new Descuento();

        descuentoInactivado.setId(id);

        descuentoInactivado = serviceDescuento.inactivarDescuento(descuentoInactivado);

        if (descuentoInactivado.getEstado().equals("I")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Descuento inactivado correctamente!"));
            filterDescuento();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al inactivar el descuento!"));
        }
    }

    /**
     * Filtro para servicios de contrato
     */
    public void filterServicios() {
        contratoServicioFilter.setIdContrato(Integer.parseInt(id));
        adapterContratoServicio = adapterContratoServicio.fillData(contratoServicioFilter);
    }

    /**
     * Método para filtrar el Cliente
     */
    public void filterCliente() {
        clienteFilter.setIdCliente(Integer.parseInt(idCliente));
        adapterCliente = adapterCliente.fillData(clienteFilter);

        clienteFilter.setNroDocumento(adapterCliente.getData().get(0).getNroDocumento());
        clienteFilter.setRazonSocial(adapterCliente.getData().get(0).getRazonSocial());
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        id = params.get("id");
        idCliente = params.get("idCliente");

        this.adapter = new ContratoAdapter();
        this.contratoFilter = new Contrato();
        this.adapterDescuento = new DescuentoAdapter();
        this.descuentoFilter = new Descuento();

        this.motivoDescuentoFilter = new MotivoDescuento();
        this.adapterMotivoDescuento = new MotivoDescuentoAdapter();

        this.descuentoCreate = new Descuento();
        this.serviceDescuento = new DescuentoService();

        this.adapterContratoServicio = new ContratoServicioAdapter();
        this.contratoServicioFilter = new ContratoServicio();

        this.adapterCliente = new ClientListAdapter();
        this.clienteFilter = new Cliente();

        filter();
        filterDescuento();
        filterMotivoDescuento();
        filterServicios();
        filterCliente();
    }
}
