/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para tipo documento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoDocumentoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoDocumentoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoDocumento datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoDocumento tipoDocumento, Integer page, Integer pageSize) {
        TipoDocumentoFilter filter = new TipoDocumentoFilter();

        filter
                .id(tipoDocumento.getId())
                .descripcion(tipoDocumento.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoDocumentoFilter() {
        super();
    }

}
