package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de Pais
 *
 * @author Williams Vera
 */
public class Pais {

    /**
     * Identificador de pais
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Código
     */
    private String codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Constructor
     */
    public Pais() {

    }
}
