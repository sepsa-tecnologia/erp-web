package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Filtro utilizado para usuario
 *
 * @author Cristina Insfrán
 * @author Sergio D. Riveros Vazquez
 */
public class UserFilter extends Filter {

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param id Identificador del usuario
     * @return
     */
    public UserFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public UserFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de empresa
     *
     * @param idEmpresa Identificador de la persona
     * @return
     */
    public UserFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de identicador estado
     *
     * @param idEstado Identificador del estado
     * @return
     */
    public UserFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agregar el filtro de identificador de grupo
     *
     * @param usuario Nombre de usuario
     * @return
     */
    public UserFilter usuario(String usuario) {
        if (usuario != null && !usuario.trim().isEmpty()) {
            params.put("usuario", usuario);
        }
        return this;
    }

    /**
     * Agrega el filtro para la contrasena
     *
     * @param contrasena
     * @return
     */
    public UserFilter contrasena(String contrasena) {
        if (contrasena != null && !contrasena.trim().isEmpty()) {
            params.put("contrasena", contrasena);
        }
        return this;
    }

    /**
     * Agrega el filtro para el estado
     *
     * @param estado estado del usuario
     * @return
     */
    public UserFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro para el email
     *
     * @param email
     * @return
     */
    public UserFilter email(String email) {
        if (email != null && !email.trim().isEmpty()) {
            params.put("email", email);
        }
        return this;
    }

    /**
     * Agrega el filtro para el número de teléfono
     *
     * @param telefono
     * @return
     */
    public UserFilter telefono(String telefono) {
        if (telefono != null && !telefono.trim().isEmpty()) {
            params.put("telefono", telefono);
        }
        return this;
    }

    /**
     * Agrega el filtro para la validación
     *
     * @param validacion
     * @return
     */
    public UserFilter validacion(String validacion) {
        if (validacion != null && !validacion.trim().isEmpty()) {
            params.put("validacion", validacion);
        }
        return this;
    }

    /**
     * Agrega el filtro comercial
     *
     * @param comercial
     * @return
     */
    public UserFilter comercial(String comercial) {
        if (comercial != null && !comercial.trim().isEmpty()) {
            params.put("comercial", comercial);
        }
        return this;
    }

    /**
     * Agrega el filtro comercial administrador
     *
     * @param comercialAdministrador
     * @return
     */
    public UserFilter comercialAdministrador(String comercialAdministrador) {
        if (comercialAdministrador != null && !comercialAdministrador.trim().isEmpty()) {
            params.put("comercialAdministrador", comercialAdministrador);
        }
        return this;
    }

    /**
     * Agrega el filtro nombre persona física
     *
     * @param nombrePersonaFisica
     * @return
     */
    public UserFilter nombrePersonaFisica(String nombrePersonaFisica) {
        if (nombrePersonaFisica != null && !nombrePersonaFisica.trim().isEmpty()) {
            params.put("nombrePersonaFisica", nombrePersonaFisica);
        }
        return this;
    }

    /**
     * Agrega el filtro tiene persona física
     *
     * @param tienePersonaFisica
     * @return
     */
    public UserFilter tienePersonaFisica(Boolean tienePersonaFisica) {
        if (tienePersonaFisica != null) {
            params.put("tienePersonaFisica", tienePersonaFisica);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identicador Persona Física
     *
     * @param idEstado Identificador de persona física
     * @return
     */
    public UserFilter idPersonaFisica(Integer idPersonaFisica) {
        if (idPersonaFisica != null) {
            params.put("idPersonaFisica", idPersonaFisica);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param user datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Usuario user, Integer page, Integer pageSize) {
        UserFilter filter = new UserFilter();

        filter
                .idPersona(user.getIdPersona())
                .id(user.getId())
                .idEmpresa(user.getIdEmpresa())
                .idEstado(user.getIdEstado())
                .usuario(user.getUsuario())
                .validacion(user.getValidacion())
                .comercial(user.getComercial())
                .comercialAdministrador(user.getComercialAdministrador())
                .tienePersonaFisica(user.getTienePersonaFisica())
                .nombrePersonaFisica(user.getNombrePersonaFisica())
                .idPersonaFisica(user.getIdPersonaFisica())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClientFilter
     */
    public UserFilter() {
        super();
    }

}
