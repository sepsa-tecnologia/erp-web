package py.com.sepsa.erp.web.v1.producto.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;
import sun.net.www.content.image.gif;

/**
 * Controlador para crear el precio producto
 *
 * @author Cristina Insfrán, Romina Núñez
 */
@ViewScoped
@Named("crearProductoPrecio")
public class ProductoPrecioCrearController implements Serializable {

    /**
     * Objeto producto precio
     */
    private ProductoPrecio productoPrecio;
    /**
     * Cliente remoto de producto cliente
     */
    private ProductoService productoClient;
    /**
     * Objeto producto
     */
    private Producto producto;
    /**
     * Adptador de la lista de productos
     */
    private ProductoAdapter adapterProducto;
    /**
     * Adaptador de la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * Objeto Moneda
     */
    private Moneda moneda;
    /**
     * Objeto canal venta
     */
    private CanalVenta canalVenta;
    /**
     * Adaptador de la lista de canal venta
     */
    private CanalVentaListAdapter adapterCanalVenta;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapter;
    /**
     * Objeto Cliente
     */
    private Cliente cliente;
    /**
     *
     * @return
     */
    @Inject
    private ProductoPrecioListController productoListController;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the productoListController
     */
    public ProductoPrecioListController getProductoListController() {
        return productoListController;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setAdapter(ClientListAdapter adapter) {
        this.adapter = adapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public ClientListAdapter getAdapter() {
        return adapter;
    }

    /**
     * @param productoListController the productoListController to set
     */
    public void setProductoListController(ProductoPrecioListController productoListController) {
        this.productoListController = productoListController;
    }

    /**
     * @return the adapterCanalVenta
     */
    public CanalVentaListAdapter getAdapterCanalVenta() {
        return adapterCanalVenta;
    }

    /**
     * @param adapterCanalVenta the adapterCanalVenta to set
     */
    public void setAdapterCanalVenta(CanalVentaListAdapter adapterCanalVenta) {
        this.adapterCanalVenta = adapterCanalVenta;
    }

    /**
     * @return the canalVenta
     */
    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    /**
     * @param canalVenta the canalVenta to set
     */
    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    /**
     * @return the moneda
     */
    public Moneda getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the adapterMoneda
     */
    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @param adapterMoneda the adapterMoneda to set
     */
    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    /**
     * @return the productoPrecio
     */
    public ProductoPrecio getProductoPrecio() {
        return productoPrecio;
    }

    /**
     * @param productoPrecio the productoPrecio to set
     */
    public void setProductoPrecio(ProductoPrecio productoPrecio) {
        this.productoPrecio = productoPrecio;
    }

    /**
     * @return the productoClient
     */
    public ProductoService getProductoClient() {
        return productoClient;
    }

    /**
     * @param productoClient the productoClient to set
     */
    public void setProductoClient(ProductoService productoClient) {
        this.productoClient = productoClient;
    }

    /**
     * @return the producto
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * @return the adapterProducto
     */
    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    /**
     * @param adapterProducto the adapterProducto to set
     */
    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }
//</editor-fold>

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        producto = new Producto();
        producto.setActivo("S");
        producto.setDescripcion(query);
        adapterProducto.setPageSize(50);
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto() {
        productoPrecio.setIdProducto(producto.getId());

    }

    /**
     * Método autocomplete Canal Venta
     *
     * @param query
     * @return
     */
    public List<CanalVenta> completeQueryCanalVenta(String query) {

        canalVenta = new CanalVenta();
        canalVenta.setDescripcion(query);
        adapterCanalVenta = adapterCanalVenta.fillData(canalVenta);

        return adapterCanalVenta.getData();
    }

    /**
     * Método para seleccionar el canal de venta
     */
    public void onItemSelectCanal() {
        productoPrecio.setIdCanalVenta(canalVenta.getId());
    }

    /**
     * Limpiar Formulario
     */
    public void clearForm() {
        productoPrecio = new ProductoPrecio();
        this.producto = new Producto();
        this.canalVenta = new CanalVenta();
        this.cliente = new Cliente();
    }

    /**
     * Método para crear producto precio
     */
    public void create() {

        BodyResponse<ProductoPrecio> respuestaProducto = productoClient.setProductoPrecio(productoPrecio);
        if (respuestaProducto.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Configuración precio agregado correctamente!"));
            clearForm();
            getProductoListController().init();

        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        if (productoPrecio.getIdCanalVenta() != null) {
            cliente.setIdCanalVenta(productoPrecio.getIdCanalVenta());
        }

        adapter = adapter.fillData(cliente);

        return adapter.getData();
    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        int id = ((Cliente) event.getObject()).getIdCliente();

        productoPrecio.setIdCliente(id);

    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        this.adapter = adapter.fillData(cliente);
    }

    public void filterMoneda() {
        adapterMoneda = adapterMoneda.fillData(moneda);
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        configuracionValorFilter.setCodigoConfiguracion("PRODUCTO_PRECIO_MONEDA");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        MonedaAdapter adapterMonedaDefecto = new MonedaAdapter();
        Moneda monedadefecto = new Moneda();
        monedadefecto.setCodigo(adapterConfigValor.getData().get(0).getValor());
        adapterMonedaDefecto = adapterMonedaDefecto.fillData(monedadefecto);
        if (!adapterMonedaDefecto.getData().isEmpty()) {
            productoPrecio.setIdMoneda(adapterMonedaDefecto.getData().get(0).getId());
        }
    }

    @PostConstruct
    public void init() {
        this.productoPrecio = new ProductoPrecio();
        this.productoClient = new ProductoService();
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.adapterMoneda = new MonedaAdapter();
        this.moneda = new Moneda();
        this.canalVenta = new CanalVenta();
        this.adapterCanalVenta = new CanalVentaListAdapter();
        this.adapter = new ClientListAdapter();
        this.cliente = new Cliente();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        filterCliente();
        filterMoneda();

    }
}
