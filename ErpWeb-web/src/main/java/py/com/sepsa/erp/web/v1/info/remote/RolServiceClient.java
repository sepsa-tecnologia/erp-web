
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.RolListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.RolFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Rol;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Rol
 * @author Cristina Insfrán
 */
public class RolServiceClient extends APIErpCore{
    
     /**
     * Obtiene la lista de roles
     *
     * @param rol Objeto local
     * @param page
     * @param pageSize
     * @return
     */
    public RolListAdapter getRolList(Rol rol, Integer page,
            Integer pageSize) {
       
        RolListAdapter lista = new RolListAdapter();

        Map params = RolFilter.build(rol, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.ROL.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    RolListAdapter.class);

            lista = (RolListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }
       /**
     * Constructor de RolServiceClient
     */
    public RolServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        ROL("Servicio para rol", "rol");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
