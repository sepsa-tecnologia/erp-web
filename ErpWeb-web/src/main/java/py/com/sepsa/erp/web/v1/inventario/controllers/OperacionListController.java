
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.OperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.OperacionInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.TipoOperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;

/**
 * Controlador para la vista listar operaciones
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("operacionList")
public class OperacionListController implements Serializable {

    /**
     * POJO de operacion
     */
    private Operacion operacionFilter;
    /**
     * Adaptador para la lista de operacion
     */
    private OperacionAdapter operacionAdapterList;
    /**
     * Pojo de detalle de operación de inventario
     */
    private OperacionInventarioDetalle operacionDetalle;
    /**
     * Adaptador de detalle de operación de inventario
     */
    private OperacionInventarioDetalleAdapter operacionDetalleAdapter;
    /**
     * POJO de tipo motivo
     */
    private TipoOperacion tipoOperacionFilter;
    /**
     * Adaptador para la lista de tipo operacion
     */
    private TipoOperacionAdapter tipoOperacionAdapterList;
    /**
     * POJO de Motivo
     */
    private Motivo motivoOperacionFilter;
    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoOperacionAdapterList;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Cliente
     */
    private Cliente proveedor;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterProveedor;
    /**
     * Adaptador para la lista de local origen
     */
    private LocalListAdapter localOrigenAdapterList;
    /**
     * POJO de local origen
     */
    private Local localOrigenFilter;
    /**
     * Adaptador para la lista de local Destino
     */
    private LocalListAdapter localDestinoAdapterList;
    /**
     * POJO de local destino
     */
    private Local localDestinoFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Operacion getoperacionFilter() {
        return operacionFilter;
    }

    public void setAdapterProveedor(ClientListAdapter adapterProveedor) {
        this.adapterProveedor = adapterProveedor;
    }

    public void setProveedor(Cliente proveedor) {
        this.proveedor = proveedor;
    }

    public Cliente getProveedor() {
        return proveedor;
    }

    public ClientListAdapter getAdapterProveedor() {
        return adapterProveedor;
    }

    public void setoperacionFilter(Operacion operacionFilter) {
        this.operacionFilter = operacionFilter;
    }

    public OperacionAdapter getoperacionAdapterList() {
        return operacionAdapterList;
    }

    public void setoperacionAdapterList(OperacionAdapter operacionAdapterList) {
        this.operacionAdapterList = operacionAdapterList;
    }

    public TipoOperacion getTipooperacionFilter() {
        return tipoOperacionFilter;
    }

    public void setTipooperacionFilter(TipoOperacion tipoOperacionFilter) {
        this.tipoOperacionFilter = tipoOperacionFilter;
    }

    public TipoOperacionAdapter getTipooperacionAdapterList() {
        return tipoOperacionAdapterList;
    }

    public void setTipooperacionAdapterList(TipoOperacionAdapter tipoOperacionAdapterList) {
        this.tipoOperacionAdapterList = tipoOperacionAdapterList;
    }

    public Motivo getMotivooperacionFilter() {
        return motivoOperacionFilter;
    }

    public void setMotivooperacionFilter(Motivo motivoOperacionFilter) {
        this.motivoOperacionFilter = motivoOperacionFilter;
    }

    public MotivoAdapter getMotivooperacionAdapterList() {
        return motivoOperacionAdapterList;
    }

    public void setMotivooperacionAdapterList(MotivoAdapter motivoOperacionAdapterList) {
        this.motivoOperacionAdapterList = motivoOperacionAdapterList;
    }

    public OperacionInventarioDetalle getOperacionDetalle() {
        return operacionDetalle;
    }

    public void setOperacionDetalle(OperacionInventarioDetalle operacionDetalle) {
        this.operacionDetalle = operacionDetalle;
    }

    public OperacionInventarioDetalleAdapter getOperacionDetalleAdapter() {
        return operacionDetalleAdapter;
    }

    public void setOperacionDetalleAdapter(OperacionInventarioDetalleAdapter operacionDetalleAdapter) {
        this.operacionDetalleAdapter = operacionDetalleAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public Operacion getOperacionFilter() {
        return operacionFilter;
    }

    public void setOperacionFilter(Operacion operacionFilter) {
        this.operacionFilter = operacionFilter;
    }

    public OperacionAdapter getOperacionAdapterList() {
        return operacionAdapterList;
    }

    public void setOperacionAdapterList(OperacionAdapter operacionAdapterList) {
        this.operacionAdapterList = operacionAdapterList;
    }

    public LocalListAdapter getLocalOrigenAdapterList() {
        return localOrigenAdapterList;
    }

    public void setLocalOrigenAdapterList(LocalListAdapter localOrigenAdapterList) {
        this.localOrigenAdapterList = localOrigenAdapterList;
    }

    public Local getLocalOrigenFilter() {
        return localOrigenFilter;
    }

    public void setLocalOrigenFilter(Local localOrigenFilter) {
        this.localOrigenFilter = localOrigenFilter;
    }

    public LocalListAdapter getLocalDestinoAdapterList() {
        return localDestinoAdapterList;
    }

    public void setLocalDestinoAdapterList(LocalListAdapter localDestinoAdapterList) {
        this.localDestinoAdapterList = localDestinoAdapterList;
    }

    public Local getLocalDestinoFilter() {
        return localDestinoFilter;
    }

    public void setLocalDestinoFilter(Local localDestinoFilter) {
        this.localDestinoFilter = localDestinoFilter;
    }

    public TipoOperacion getTipoOperacionFilter() {
        return tipoOperacionFilter;
    }

    public void setTipoOperacionFilter(TipoOperacion tipoOperacionFilter) {
        this.tipoOperacionFilter = tipoOperacionFilter;
    }

    public TipoOperacionAdapter getTipoOperacionAdapterList() {
        return tipoOperacionAdapterList;
    }

    public void setTipoOperacionAdapterList(TipoOperacionAdapter tipoOperacionAdapterList) {
        this.tipoOperacionAdapterList = tipoOperacionAdapterList;
    }

    public Motivo getMotivoOperacionFilter() {
        return motivoOperacionFilter;
    }

    public void setMotivoOperacionFilter(Motivo motivoOperacionFilter) {
        this.motivoOperacionFilter = motivoOperacionFilter;
    }

    public MotivoAdapter getMotivoOperacionAdapterList() {
        return motivoOperacionAdapterList;
    }

    public void setMotivoOperacionAdapterList(MotivoAdapter motivoOperacionAdapterList) {
        this.motivoOperacionAdapterList = motivoOperacionAdapterList;
    }

    //</editor-fold>
    /**
     * Método para obtener operaciones
     */
    public void getOperaciones() {
        getoperacionAdapterList().setFirstResult(0);
        this.operacionFilter.setListadoPojo("true");
        this.operacionAdapterList = this.operacionAdapterList.fillData(operacionFilter);
    }

    /**
     * Método para filtrar operación de inventario
     *
     * @param idOperacionInventario
     */
    public void filterOperacionInvetarioDetalle(Integer idOperacionInventario) {
        operacionDetalle.setListadoPojo("true");
        operacionDetalle.setIdOperacionInventario(idOperacionInventario);
        operacionDetalleAdapter = operacionDetalleAdapter.fillData(operacionDetalle);
    }

    /**
     * Método para obtener los motivos de operaciones filtradas
     *
     * @param query
     * @return
     */
    public List<Motivo> completeQueryMotivoOperacion(String query) {
        Motivo object = new Motivo();
        object.setDescripcion(query);
        motivoOperacionAdapterList = motivoOperacionAdapterList.fillData(object);
        return motivoOperacionAdapterList.getData();
    }

    /**
     * Selecciona el motivo de operacion
     *
     * @param event
     */
    public void onItemSelectMotivooperacionFilter(SelectEvent event) {
        motivoOperacionFilter.setId(((Motivo) event.getObject()).getId());
        this.operacionFilter.setIdMotivo(motivoOperacionFilter.getId());
    }

    /**
     * Método para obtener los tipos de operaciones filtradas
     *
     * @param query
     * @return
     */
    public List<TipoOperacion> completeQueryTipoOperacion(String query) {
        tipoOperacionFilter = new TipoOperacion();
        tipoOperacionFilter.setDescripcion(query);
        tipoOperacionAdapterList = tipoOperacionAdapterList.fillData(tipoOperacionFilter);
        return tipoOperacionAdapterList.getData();
    }

    /**
     * Selecciona el tipo de operacion
     *
     * @param event
     */
    public void onItemSelectTipoOperacionFilter(SelectEvent event) {
        tipoOperacionFilter.setId(((TipoOperacion) event.getObject()).getId());
        this.operacionFilter.setIdTipoOperacion(tipoOperacionFilter.getId());
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(cliente);
        return adapterCliente.getData();
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQueryProveedor(String query) {
        Cliente proveedor = new Cliente();
        proveedor.setRazonSocial(query);
        proveedor.setProveedor("S");
        adapterProveedor = adapterProveedor.fillData(proveedor);
        return adapterProveedor.getData();
    }

    /**
     * Selecciona cliente en Autocomplete y lo guarda en operacionFilter
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        this.operacionFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /* Método para obtener los locales origen filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocalOrigen(String query) {
        localOrigenFilter = new Local();
        localOrigenFilter.setLocalExterno("N");
        localOrigenFilter.setDescripcion(query);
        localOrigenAdapterList = localOrigenAdapterList.fillData(localOrigenFilter);
        return localOrigenAdapterList.getData();
    }

    /**
     * Selecciona local Origen
     *
     * @param event
     */
    public void onItemSelectLocalOrigen(SelectEvent event) {
        this.operacionFilter.setIdLocalOrigen(((Local) event.getObject()).getId());
    }

    /* Método para obtener los locales destinos filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocalDestino(String query) {
        localDestinoFilter = new Local();
        localDestinoFilter.setLocalExterno("N");
        localDestinoFilter.setDescripcion(query);
        localDestinoAdapterList = localDestinoAdapterList.fillData(localDestinoFilter);
        return localDestinoAdapterList.getData();
    }

    /**
     * Selecciona local Destino
     *
     * @param event
     */
    public void onItemSelectLocalDestino(SelectEvent event) {
        this.operacionFilter.setIdLocalDestino(((Local) event.getObject()).getId());
    }

    /**
     * Método para seleccionar el proveedor
     *
     * @param event
     */
    public void onItemSelectProveedor(SelectEvent event) {
        this.operacionFilter.setIdProveedor(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.operacionFilter = new Operacion();
        this.operacionFilter.setListadoPojo("true");
        this.tipoOperacionFilter = new TipoOperacion();
        this.tipoOperacionAdapterList = new TipoOperacionAdapter();
        this.motivoOperacionFilter = new Motivo();
        this.motivoOperacionAdapterList = new MotivoAdapter();
        this.localOrigenFilter = new Local();
        this.localOrigenAdapterList = new LocalListAdapter();
        this.localDestinoFilter = new Local();
        this.localDestinoAdapterList = new LocalListAdapter();
        getOperaciones();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("operacion-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.operacionFilter = new Operacion();
        this.operacionFilter.setListadoPojo("true");
        this.operacionAdapterList = new OperacionAdapter();
        this.cliente = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.tipoOperacionFilter = new TipoOperacion();
        this.tipoOperacionAdapterList = new TipoOperacionAdapter();
        this.motivoOperacionFilter = new Motivo();
        this.motivoOperacionAdapterList = new MotivoAdapter();
        this.operacionDetalle = new OperacionInventarioDetalle();
        this.operacionDetalle.setListadoPojo("true");
        this.operacionDetalleAdapter = new OperacionInventarioDetalleAdapter();
        this.localOrigenAdapterList = new LocalListAdapter();
        this.localOrigenFilter = new Local();
        this.localDestinoAdapterList = new LocalListAdapter();
        this.localDestinoFilter = new Local();
        this.adapterProveedor = new ClientListAdapter();
        this.proveedor = new Cliente();

        getOperaciones();
    }

    /**
     * Constructor
     */
    public OperacionListController() {
        init();
    }

}
