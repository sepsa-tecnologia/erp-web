package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Controlador Estado de Cuenta de Cliente
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("estadoCuenta")
public class EstadoCuentaController implements Serializable {

    /**
     * Objeto
     */
    private ReporteParam parametro;
    /**
     * Servicio
     */
    private FacturacionService serviceFacturacion;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapter;
    /**
     * Objeto Cliente
     */
    private Cliente cliente;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setServiceFacturacion(FacturacionService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setAdapter(ClientListAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientListAdapter getAdapter() {
        return adapter;
    }

    public void setParametro(ReporteParam parametro) {
        this.parametro = parametro;
    }

    public FacturacionService getServiceFacturacion() {
        return serviceFacturacion;
    }

    public ReporteParam getParametro() {
        return parametro;
    }

//</editor-fold>
    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {
        Instant from = parametro.getFechaDesde().toInstant();
        Instant to = parametro.getFechaHasta().toInstant();

        long days = ChronoUnit.DAYS.between(from, to);

        boolean download = true;

        if (days > 31) {
            download = false;
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fecha Incorrecta", "El rango de fecha máximo es de 31 dias"));
        }

        if (parametro.getIdPersona() == null) {
            download = false;
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Debe seleccionar un cliente"));
        }

        if (download == true) {
            String contentType = ("application/vnd.ms-excel");
            String fileName = "estado-cuenta.xlsx";
            byte[] data = serviceFacturacion.getEstadoCuenta(parametro);
            if (data != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(data),
                        contentType, fileName);
            }

        }
        return null;
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        adapter = adapter.fillData(cliente);

        return adapter.getData();
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        int id = ((Cliente) event.getObject()).getIdCliente();

        parametro.setIdPersona(id);

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteParam();
        this.serviceFacturacion = new FacturacionService();
        this.cliente = new Cliente();
        this.adapter = new ClientListAdapter();
        filterCliente();
    }

}
