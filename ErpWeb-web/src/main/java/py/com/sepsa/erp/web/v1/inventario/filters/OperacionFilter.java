/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para operacion de inventario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OperacionFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public OperacionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idUsuario
     *
     * @param idUsuario
     * @return
     */
    public OperacionFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

    /**
     * Agrega el filtro por idCliente
     *
     * @param idCliente
     * @return
     */
    public OperacionFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProveedor
     *
     * @param idProveedor
     * @return
     */
    public OperacionFilter idProveedor(Integer idProveedor) {
        if (idProveedor != null) {
            params.put("idProveedor", idProveedor);
        }
        return this;
    }

    /**
     * Agrega el filtro por idTipoOperacion
     *
     * @param idTipoOperacion
     * @return
     */
    public OperacionFilter idTipoOperacion(Integer idTipoOperacion) {
        if (idTipoOperacion != null) {
            params.put("idTipoOperacion", idTipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro por idMotivo
     *
     * @param idMotivo
     * @return
     */
    public OperacionFilter idMotivo(Integer idMotivo) {
        if (idMotivo != null) {
            params.put("idMotivo", idMotivo);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de estado
     *
     * @param idEstado
     * @return
     */
    public OperacionFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por idLocalOrigen
     *
     * @param idLocalOrigen
     * @return
     */
    public OperacionFilter idLocalOrigen(Integer idLocalOrigen) {
        if (idLocalOrigen != null) {
            params.put("idLocalOrigen", idLocalOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idLocalDestino
     *
     * @param idLocalDestino
     * @return
     */
    public OperacionFilter idLocalDestino(Integer idLocalDestino) {
        if (idLocalDestino != null) {
            params.put("idLocalDestino", idLocalDestino);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDepositoOrigen
     *
     * @param idDepositoOrigen
     * @return
     */
    public OperacionFilter idDepositoOrigen(Integer idDepositoOrigen) {
        if (idDepositoOrigen != null) {
            params.put("idDepositoOrigen", idDepositoOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDepositoDestino
     *
     * @param idDepositoDestino
     * @return
     */
    public OperacionFilter idDepositoDestino(Integer idDepositoDestino) {
        if (idDepositoDestino != null) {
            params.put("idDepositoDestino", idDepositoDestino);
        }
        return this;
    }

    /**
     * Agrega el filtro tipo de Operacion
     *
     * @param tipoOperacion
     * @return
     */
    public OperacionFilter tipoOperacion(String tipoOperacion) {
        if (tipoOperacion != null && !tipoOperacion.trim().isEmpty()) {
            params.put("tipoOperacion", tipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo tipo de Operacion
     *
     * @param codigoTipoOperacion
     * @return
     */
    public OperacionFilter codigoTipoOperacion(String codigoTipoOperacion) {
        if (codigoTipoOperacion != null && !codigoTipoOperacion.trim().isEmpty()) {
            params.put("codigoTipoOperacion", codigoTipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro motivo Operacion
     *
     * @param motivoOperacion
     * @return
     */
    public OperacionFilter motivoOperacion(String motivoOperacion) {
        if (motivoOperacion != null && !motivoOperacion.trim().isEmpty()) {
            params.put("motivoOperacion", motivoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo motivo Operacion
     *
     * @param codigoMotivo
     * @return
     */
    public OperacionFilter codigoMotivo(String codigoMotivo) {
        if (codigoMotivo != null && !codigoMotivo.trim().isEmpty()) {
            params.put("codigoMotivo", codigoMotivo);
        }
        return this;
    }

    /**
     * Agrega el filtro Estado
     *
     * @param estado
     * @return
     */
    public OperacionFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public OperacionFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public OperacionFilter listadoPojo(String listadoPojo) {
        if (listadoPojo != null && !listadoPojo.trim().isEmpty()) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaInsercion
     * @return
     */
    public OperacionFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro nroDocumentoAsociado
     *
     * @param nroDocumentoAsociado
     * @return
     */
    public OperacionFilter nroDocumentoAsociado(String nroDocumentoAsociado) {
        if (nroDocumentoAsociado != null && !nroDocumentoAsociado.trim().isEmpty()) {
            params.put("nroDocumentoAsociado", nroDocumentoAsociado);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoEstado
     *
     * @param localOrigen
     * @return
     */
    public OperacionFilter localOrigen(String localOrigen) {
        if (localOrigen != null && !localOrigen.trim().isEmpty()) {
            params.put("localOrigen", localOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idTipoOperacion
     *
     * @param localDestino
     * @return
     */
    public OperacionFilter localDestino(String localDestino) {
        if (localDestino != null && !localDestino.trim().isEmpty()) {
            params.put("localDestino", localDestino);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public OperacionFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);

                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionHasta
     * @return
     */
    public OperacionFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);

                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param operacionFilter datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Operacion operacionFilter, Integer page, Integer pageSize) {
        OperacionFilter filter = new OperacionFilter();

        filter
                .id(operacionFilter.getId())
                .idUsuario(operacionFilter.getIdUsuario())
                .idCliente(operacionFilter.getIdCliente())
                .idProveedor(operacionFilter.getIdProveedor())
                .idTipoOperacion(operacionFilter.getIdTipoOperacion())
                .idLocalOrigen(operacionFilter.getIdLocalOrigen())
                .idLocalDestino(operacionFilter.getIdLocalDestino())
                .idDepositoOrigen(operacionFilter.getIdDepositoOrigen())
                .idDepositoDestino(operacionFilter.getIdDepositoDestino())
                .tipoOperacion(operacionFilter.getTipoOperacion())
                .codigoTipoOperacion(operacionFilter.getCodigoTipoOperacion())
                .idMotivo(operacionFilter.getIdMotivo())
                .motivoOperacion(operacionFilter.getMotivo())
                .codigoMotivo(operacionFilter.getCodigoMotivo())
                .idEstado(operacionFilter.getIdEstado())
                .estado(operacionFilter.getEstado())
                .listadoPojo(operacionFilter.getListadoPojo())
                .codigoEstado(operacionFilter.getCodigoEstado())
                .nroDocumentoAsociado(operacionFilter.getNroDocumentoAsociado())
                .localOrigen(operacionFilter.getLocalOrigen())
                .localDestino(operacionFilter.getLocalDestino())
                .fechaInsercionDesde(operacionFilter.getFechaInsercionDesde())
                .fechaInsercionHasta(operacionFilter.getFechaInsercionHasta())
                .page(page)
                .pageSize(pageSize);
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public OperacionFilter() {
        super();
    }

}
