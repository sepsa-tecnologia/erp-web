
package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO de el Tipo Negocio Persona
 * @author Romina Núñez 
 */
public class ClienteTipoNegocio {

    /**
     * Identificador del tipo de negocio
     */
    private Integer idTipoNegocio;
    /**
     * Tipo Negocio
     */
    private String tipoNegocio;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Principal
     */
    private String principal;
    /**
     * Tiene
     */
    private String tiene;
    
    /**
     * @return the idTipoNegocio
     */
    public Integer getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public String getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(String tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    public void setTiene(String tiene) {
        this.tiene = tiene;
    }

    public String getTiene() {
        return tiene;
    }

    /**
     * @param idTipoNegocio the idTipoNegocio to set
     */
    public void setIdTipoNegocio(Integer idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    /**
     * @return the idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the principal
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    /**
     * Constructor 
     */
    public ClienteTipoNegocio(){
        
    }
    
    /**
     * Constructor con parametros
     * @param idTipoNegocio
     * @param idCliente
     * @param principal
     */
    public ClienteTipoNegocio(Integer idTipoNegocio,Integer idCliente, 
            String principal){
        this.idTipoNegocio = idTipoNegocio;
        this.idCliente = idCliente;
        this.principal = principal;
    }
    
    /**
     * Constructor con parametros
     * @param idTipoNegocio
     * @param tipoNegocio
     * @param idCliente
     * @param principal
     * @param tiene 
     */
    public ClienteTipoNegocio(Integer idTipoNegocio, String tipoNegocio, Integer idCliente, String principal, String tiene) {
        this.idTipoNegocio = idTipoNegocio;
        this.tipoNegocio = tipoNegocio;
        this.idCliente = idCliente;
        this.principal = principal;
        this.tiene = tiene;
    }
    
    
}
