/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.remote.DepositoLogisticoService;

/**
 * Controlador para Editar Motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("depositoEdit")
public class DepositoLogisticoEditController implements Serializable {

    /**
     * Cliente para el servicio de motivo.
     */
    private final DepositoLogisticoService service;
    /**
     * Datos del motivo
     */
    private DepositoLogistico deposito;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de tipo documento
     */
    private Local localFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public DepositoLogistico getDeposito() {
        return deposito;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public DepositoLogisticoService getService() {
        return service;
    }

    public void setDeposito(DepositoLogistico deposito) {
        this.deposito = deposito;
    }

    //</editor-fold>
    /**
     * Método para editar Motivo
     */
    public void edit() {
        BodyResponse<DepositoLogistico> respuestaDep = service.editDeposito(deposito);

        if (respuestaDep.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Deposito editado correctamente!"));
        }

    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        local.setLocalExterno("N");
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        deposito.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.deposito = service.get(id);

        this.localFilter = new Local();
        localFilter.setId(deposito.getIdLocal());
        this.localAdapterList = new LocalListAdapter();
        this.localAdapterList = localAdapterList.fillData(localFilter);
        this.localFilter = localAdapterList.getData().get(0);
    }

    /**
     * Constructor
     */
    public DepositoLogisticoEditController() {
        this.service = new DepositoLogisticoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
