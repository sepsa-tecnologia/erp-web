/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;

/**
 *
 * @author Cristina Insfrán
 */
@FacesConverter("clientePojoConverter")
public class ClientePojoConverter implements Converter{
    
      /**
     * Servicios para cliente
     */
    private ClientServiceClient serviceClient;
    
    @Override
    public Cliente getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new ClientServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            Cliente cli=new Cliente();
            cli.setIdCliente(val);
          
            List<Cliente> clientes = serviceClient.getClientList(cli, 0, 10).getData();
            if(clientes != null && !clientes.isEmpty()) {
                return clientes.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un cliente válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Cliente) {
                return String.valueOf(((Cliente)object).getIdCliente());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
