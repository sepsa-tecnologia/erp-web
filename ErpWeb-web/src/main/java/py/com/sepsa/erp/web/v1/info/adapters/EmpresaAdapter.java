/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.EmpresaService;

/**
 * Adapter para empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class EmpresaAdapter extends DataListAdapter<Empresa> {

    /**
     * Cliente para el servicio empresa
     */
    private final EmpresaService empresaService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public EmpresaAdapter fillData(Empresa searchData) {

        return empresaService.getEmpresaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de EmpresaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public EmpresaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.empresaService = new EmpresaService();
    }

    /**
     * Constructor de EmpresaAdapter
     */
    public EmpresaAdapter() {
        super();
        this.empresaService = new EmpresaService();
    }
}
