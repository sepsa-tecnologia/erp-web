
package py.com.sepsa.erp.web.v1.info.pojos;


/**
 * POJO para tipoDato
 * @author Romina Núñez
 */
public class TipoDato {

    /**
     * Identificador del tipo de dato
     */
    private Integer id;
    /**
     * Nombre de TipoDato
     */
    private String tipoDato;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id) {       
        this.id = id;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }
    /**
     * Constructor de la clase
     */
    public TipoDato() {
    }
    
    /**
     * Constructor con parametros
     * @param id
     * @param tipoDato
     */
    public TipoDato(Integer id,String tipoDato){
        this.id=id;
        this.tipoDato = tipoDato;
    }
    
}
