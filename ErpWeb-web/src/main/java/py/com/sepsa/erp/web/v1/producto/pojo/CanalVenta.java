
package py.com.sepsa.erp.web.v1.producto.pojo;

/**
 * POJO canal venta
 * @author Cristina Insfrán
 */
public class CanalVenta {


    /**
     * Identificador 
     */
    private Integer  id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Código
     */
    private String codigo;
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @return the activo
     */
    public Character getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(Character activo) {
        this.activo = activo;
    }
    
    public CanalVenta(){
        
    }
}
