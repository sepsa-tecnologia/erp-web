package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;

/**
 * Controlador para listado de referencia geografica
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarReferenciaGeografica")
public class ReferenciaGeograficaController implements Serializable {

    /**
     * Adaptador de la lista
     */
    private ReferenciaGeograficaAdapter adapter;
    /**
     * Pojo de referencia geografica
     */
    private ReferenciaGeografica searchData;
    /**
     * Pojo de Referencia Geeográfica Padre
     */
    private ReferenciaGeografica padre;
    /**
     * Adaptador de la lista para autocomplete
     */
    private ReferenciaGeograficaAdapter adapter2;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public ReferenciaGeograficaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ReferenciaGeograficaAdapter adapter) {
        this.adapter = adapter;
    }

    public ReferenciaGeografica getSearchData() {
        return searchData;
    }

    public void setSearchData(ReferenciaGeografica searchData) {
        this.searchData = searchData;
    }

    public ReferenciaGeografica getPadre() {
        return padre;
    }

    public void setPadre(ReferenciaGeografica padre) {
        this.padre = padre;
    }

    public ReferenciaGeograficaAdapter getAdapter2() {
        return adapter2;
    }

    public void setAdapter2(ReferenciaGeograficaAdapter adapter2) {
        this.adapter2 = adapter2;
    }

    //</editor-fold>
    /**
     * Método para filtrar referencias
     */
    public void filterReferenciaGeografica() {
        adapter.setFirstResult(0);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para obetener referencias geográficas filtradas
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQuery(String query) {

        ReferenciaGeografica padre = new ReferenciaGeografica();
        padre.setDescripcion(query);

        adapter2 = adapter2.fillData(padre);

        return adapter2.getData();
    }

    /**
     * Selecciona el padre
     *
     * @param event
     */
    public void onItemSelectReferenciaFilter(SelectEvent event) {
        searchData.setIdPadre(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Metodo para redirigir a la vista editar Menu
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("referencia-geografica-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        this.padre = new ReferenciaGeografica();
        this.searchData = new ReferenciaGeografica();
        filterReferenciaGeografica();
    }

    @PostConstruct
    public void init() {
        this.adapter2 = new ReferenciaGeograficaAdapter();
        this.adapter = new ReferenciaGeograficaAdapter();
        this.searchData = new ReferenciaGeografica();
        this.padre = new ReferenciaGeografica();
        filterReferenciaGeografica();
    }

}
