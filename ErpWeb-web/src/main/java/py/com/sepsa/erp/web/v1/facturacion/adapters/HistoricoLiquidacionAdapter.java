
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.facturacion.remote.HistoricoLiquidacionService;

/**
 * Adaptador de la lista de Liquidación
 * @author Romina Núñez
 */
public class HistoricoLiquidacionAdapter extends DataListAdapter<HistoricoLiquidacion> {
    
    /**
     * Cliente para el servicio de HistoricoLiquidacion
     */
    private final HistoricoLiquidacionService serviceHistoricoLiquidacion;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public HistoricoLiquidacionAdapter fillData(HistoricoLiquidacion searchData) {
     
        return serviceHistoricoLiquidacion.getHistoricoLiquidacionList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de HistoricoLiquidacionAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public HistoricoLiquidacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceHistoricoLiquidacion = new HistoricoLiquidacionService();
    }

    /**
     * Constructor de HistoricoLiquidacionAdapter
     */
    public HistoricoLiquidacionAdapter() {
        super();
        this.serviceHistoricoLiquidacion = new HistoricoLiquidacionService();
    }
}
