
package py.com.sepsa.erp.web.v1.usuario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.remote.PerfilServiceClient;

/**
 * Adaptador para la lista de perfil
 * @author Cristina Insfrán
 */
public class PerfilListAdapter extends DataListAdapter<Perfil>{
   
     /**
     * Cliente para los servicios de perfil
     */
    private final PerfilServiceClient perfilClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public PerfilListAdapter fillData(Perfil searchData) {
        return perfilClient.getPerfilList(searchData, getFirstResult(), getPageSize());

    }

    /**
     * Constructor de ClientListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PerfilListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.perfilClient = new PerfilServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public PerfilListAdapter() {
        super();
        this.perfilClient = new PerfilServiceClient();
    }
}
