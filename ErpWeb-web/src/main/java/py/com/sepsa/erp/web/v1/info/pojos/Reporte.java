/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class Reporte {

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Descripción
     */
    private String descripcion;

    /**
     * Código
     */
    private String codigo;

    /**
     * Activo
     */
    private Character activo;

    /**
     * Código de tipo de reporte
     */
    private String codigoTipoReporte;

    /**
     * Formato
     */
    private String formato;

    /**
     * Tipo estado
     */
    private List<Parametro> parametros;

    public static class Parametro {

        private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        private String stringValue;
        private Integer integerValue;
        private Boolean booleanValue;
        private Date dateValue;
        private String codigo;
        private String descripcion;
        private String tipoParametro;
        private String tipoResultado;
        private Boolean mandatorio;
        private String defecto;
        private List<Valor> valores;

        public Boolean hasValue() {
            switch (tipoResultado) {
                case "FECHA":
                    return dateValue != null;
                case "STRING":
                    return stringValue != null && !stringValue.trim().isEmpty();
                case "INTEGER":
                    return integerValue != null;
                case "BOOLEAN":
                    return booleanValue != null;
                default:
                    throw new IllegalArgumentException("Tipo de resultado no soportado");
            }
        }
        
        public JsonObject getJsonObject() {
            
            JsonObject result = new JsonObject();
            result.addProperty("codigo", codigo);
            result.addProperty("tipo", tipoResultado);

            switch (tipoResultado) {
                case "FECHA":
                    result.addProperty("valor", getStringDateValue());
                    break;
                case "INTEGER":
                    result.addProperty("valor", integerValue);
                    break;
                case "BOOLEAN":
                    result.addProperty("valor", booleanValue);
                    break;
                case "STRING":
                default:
                    result.addProperty("valor", stringValue.trim());
            }
            
            return result;
        }

        public Boolean getBooleanValue() {
            return booleanValue;
        }

        public void setBooleanValue(Boolean booleanValue) {
            this.booleanValue = booleanValue;
        }

        public String getStringValue() {
            return stringValue;
        }

        public void setStringValue(String stringValue) {
            this.stringValue = stringValue;
        }

        public Integer getIntegerValue() {
            return integerValue;
        }

        public void setIntegerValue(Integer integerValue) {
            this.integerValue = integerValue;
        }

        public Date getDateValue() {
            return dateValue;
        }

        public void setDateValue(Date dateValue) {
            this.dateValue = dateValue;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public void setTipoResultado(String tipoResultado) {
            this.tipoResultado = tipoResultado;
        }

        public String getTipoResultado() {
            return tipoResultado;
        }

        public void setTipoParametro(String tipoParametro) {
            this.tipoParametro = tipoParametro;
        }

        public String getTipoParametro() {
            return tipoParametro;
        }

        public Boolean getMandatorio() {
            return mandatorio;
        }

        public void setMandatorio(Boolean mandatorio) {
            this.mandatorio = mandatorio;
        }

        public String getDefecto() {
            return defecto;
        }

        public void setDefecto(String defecto) {
            this.defecto = defecto;
        }

        public List<Valor> getValores() {
            return valores;
        }

        public void setValores(List<Valor> valores) {
            this.valores = valores;
        }

        public String getStringDateValue() {
            if (dateValue != null) {
                return sdf1.format(dateValue);
            }
            return null;
        }
    }

    public class Valor {

        private String codigo;
        private String descripcion;

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setParametros(List<Parametro> parametros) {
        this.parametros = parametros;
    }

    public List<Parametro> getParametros() {
        return parametros;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getFormato() {
        return formato;
    }

    public void setCodigoTipoReporte(String codigoTipoReporte) {
        this.codigoTipoReporte = codigoTipoReporte;
    }

    public String getCodigoTipoReporte() {
        return codigoTipoReporte;
    }
}
