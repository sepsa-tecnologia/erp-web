
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefonoNotificacion;
import py.com.sepsa.erp.web.v1.info.remote.ContactoTelefonoNotificacionService;

/**
 * Adaptador de la lista de contacto telefono notificación
 * @author Romina Núñez
 */
public class ContactoTelefonoNotificacionAsociadoAdapter extends DataListAdapter<ContactoTelefonoNotificacion> {
    
    /**
     * Cliente para el servicio de contacto email
     */
    private final ContactoTelefonoNotificacionService contactoTelefonoNotificacionService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoTelefonoNotificacionAsociadoAdapter fillData(ContactoTelefonoNotificacion searchData) {
     
        return contactoTelefonoNotificacionService.getContactoTelefonoNotificacionAsociadoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ContactoTelefonoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoTelefonoNotificacionAsociadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoTelefonoNotificacionService = new ContactoTelefonoNotificacionService();
    }

    /**
     * Constructor de ContactoTelefonoAdapter
     */
    public ContactoTelefonoNotificacionAsociadoAdapter() {
        super();
        this.contactoTelefonoNotificacionService = new ContactoTelefonoNotificacionService();
    }
}
