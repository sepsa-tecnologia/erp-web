/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemision;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionNrAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("notaRemisionList")
public class NotaRemisionController implements Serializable {
    
    private NotaRemisionAdapter adapterNotaRemision;
    
    private NotaRemision notaRemisionFilter;
    
    private NotaRemisionService serviceNotaRemision;
    
    private Cliente cliente;
    
    private MotivoEmisionNrAdapter motivoEmisionNrAdapter;
    
    private MotivoAnulacion motivoAnulacion;
    
    private Boolean ignorarPeriodoAnulacion;
    
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    
    private DocumentoProcesamiento procesamientoFilter;
    
    private NotaRemisionProcesamientoAdapter adapterProcesamiento;

    @Inject
    private SessionData session;
    
    private Integer diasFiltro;
    
    public DocumentoProcesamiento getProcesamientoFilter() {
        return procesamientoFilter;
    }

    public void setProcesamientoFilter(DocumentoProcesamiento procesamientoFilter) {
        this.procesamientoFilter = procesamientoFilter;
    }

    public NotaRemisionProcesamientoAdapter getAdapterProcesamiento() {
        return adapterProcesamiento;
    }

    public void setAdapterProcesamiento(NotaRemisionProcesamientoAdapter adapterProcesamiento) {
        this.adapterProcesamiento = adapterProcesamiento;
    }
    
    public NotaRemisionAdapter getAdapterNotaRemision() {
        return adapterNotaRemision;
    }

    public void setAdapterNotaRemision(NotaRemisionAdapter adapterNotaRemision) {
        this.adapterNotaRemision = adapterNotaRemision;
    }

    public NotaRemision getNotaRemisionFilter() {
        return notaRemisionFilter;
    }

    public void setNotaRemisionFilter(NotaRemision notaRemisionFilter) {
        this.notaRemisionFilter = notaRemisionFilter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public MotivoEmisionNrAdapter getMotivoEmisionNrAdapter() {
        return motivoEmisionNrAdapter;
    }

    public void setMotivoEmisionNrAdapter(MotivoEmisionNrAdapter motivoEmisionNrAdapter) {
        this.motivoEmisionNrAdapter = motivoEmisionNrAdapter;
    }

    public NotaRemisionService getServiceNotaRemision() {
        return serviceNotaRemision;
    }

    public void setServiceNotaRemision(NotaRemisionService serviceNotaRemision) {
        this.serviceNotaRemision = serviceNotaRemision;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    public Integer getDiasFiltro() {
        return diasFiltro;
    }

    public void setDiasFiltro(Integer diasFiltro) {
        this.diasFiltro = diasFiltro;
    }
    
    
     /**
     * Método para anular nota de crédito
     *
     * @param notaRemision
     */
    public void anularNotaRemision(NotaRemision notaRemision) {
        NotaRemision nota = new NotaRemision();
        nota.setId(notaRemision.getId());
        nota.setIdMotivoAnulacion(notaRemision.getIdMotivoAnulacion());
        nota.setObservacionAnulacion(notaRemision.getObservacionAnulacion());
        nota.setIgnorarPeriodoAnulacion(ignorarPeriodoAnulacion);
        BodyResponse notaCredResp = serviceNotaRemision.anularNotaCredito(nota);

        if (notaCredResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de Remisión Anulada!"));
            notaRemisionFilter = new NotaRemision();
            filterNotaRemision();
        }

    }
    
    
    public void filterNotaRemision() {
        if (validacionesFiltrarNotaRemision()) {
            getAdapterNotaRemision().setFirstResult(0);
            this.adapterNotaRemision = adapterNotaRemision.fillData(notaRemisionFilter);
        }
    }
    
    public boolean validacionesFiltrarNotaRemision() {
        boolean save = true;
        if (notaRemisionFilter.getFechaDesde() != null && notaRemisionFilter.getFechaHasta() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error", "Se debe indicar la fecha hasta"));
            save = false;
        } else {
            if (notaRemisionFilter.getFechaDesde() == null && notaRemisionFilter.getFechaHasta() != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "Se debe indicar la fecha desde"));
                save = false;
            }
        }
        
        if (notaRemisionFilter.getFechaDesde() != null && notaRemisionFilter.getFechaHasta() != null) {
            Instant from = notaRemisionFilter.getFechaDesde().toInstant();
            Instant to = notaRemisionFilter.getFechaHasta().toInstant();
            notaRemisionFilter.setListadoPojo(true);

            long days = ChronoUnit.DAYS.between(from, to);

            Integer maxDias = 31;
            if (diasFiltro != null) {
                maxDias = diasFiltro;
            }

            if (days > maxDias) {

                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Fecha Incorrecta", String.format("El rango de fecha no puede ser más de %d días", maxDias)));
                save = false;
            }
        }
        return save;
    }
    
    /**
     * Método para obtener fechas desde y hasta para filtro
     */
    public void asignarFiltrosFecha() {
        Calendar today = Calendar.getInstance();
        notaRemisionFilter.setFechaHasta(today.getTime());

        today.add(Calendar.DATE, -10);
        notaRemisionFilter.setFechaDesde(today.getTime());

    }
    
    public void clear() {
        notaRemisionFilter = new NotaRemision();
        cliente = new Cliente();
        asignarFiltrosFecha();
        filterNotaRemision();
    }
    
    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleNotaRemisionURL(Integer id) {

        return String.format("nota-remision-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }
    
    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void getMotivoEmisionNr() {
        MotivoEmision menr = new MotivoEmision();
        motivoEmisionNrAdapter = motivoEmisionNrAdapter.fillData(menr);

    }
    
    
    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionPeriodoAnulacionNC() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("IGNORAR_PERIODO_ANULACION_NC");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ignorarPeriodoAnulacion = false;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.ignorarPeriodoAnulacion = true;
        } else {
            this.ignorarPeriodoAnulacion = false;
        }

    }
    
    /**
     *
     * @param notaRemision
     */
    public void regenerarNR(NotaRemision notaRemision) {
        NotaRemision nr = new NotaRemision();
        nr.setId(notaRemision.getId());
        nr.setArchivoSet(notaRemision.getArchivoSet());
        nr.setObservacion(notaRemision.getObservacion());
        nr.setGeneradoSet('N');

        BodyResponse notaRemiResp = serviceNotaRemision.regenerarNR(nr);

        if (notaRemiResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de remisión regenerada!"));
            notaRemisionFilter = new NotaRemision();
            filterNotaRemision();
        }

    }
    
    /**
     * Metodo para obtener los doc asociados
     *
     * @param idProcesamiento
     */
    public void verProcesamiento(Integer idProcesamiento) {
        WebLogger.get().debug("PROCESAMIENTO" + idProcesamiento);
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new NotaRemisionProcesamientoAdapter();
        if (idProcesamiento != null) {
            this.procesamientoFilter.setIdProcesamiento(idProcesamiento);
            this.adapterProcesamiento = adapterProcesamiento.fillData(procesamientoFilter);
            this.procesamientoFilter = this.adapterProcesamiento.getData().get(0);
        }
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.notaRemisionFilter = new NotaRemision();
        this.adapterNotaRemision = new NotaRemisionAdapter();
        asignarFiltrosFecha();
        filterNotaRemision();
        
        this.cliente = new Cliente();
        this.serviceNotaRemision = new NotaRemisionService();
        this.motivoEmisionNrAdapter = new MotivoEmisionNrAdapter();
        getMotivoEmisionNr();

        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();

        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("NOTA_REMISION");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
        this.ignorarPeriodoAnulacion = false;
        
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new NotaRemisionProcesamientoAdapter();
        
        obtenerConfiguracionPeriodoAnulacionNC();
        
        
        
    }
    
}
