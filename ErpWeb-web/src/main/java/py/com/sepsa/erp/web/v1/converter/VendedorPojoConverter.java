/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.info.remote.VendedorService;

/**
 * Convertidor para datos de autocomplete
 *
 * @author Gustavo Benitez
 */
@FacesConverter("vendedorPojoConverter")
public class VendedorPojoConverter implements Converter {

    /**
     * Servicios para login al sistema
     */
    private VendedorService vendedorService;

    @Override
    public VendedorPojo getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            vendedorService = new VendedorService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            VendedorPojo ver = new VendedorPojo();
            ver.setId(val);
            ver.setListadoPojo(Boolean.TRUE);

            List<VendedorPojo> vendedor
                    = vendedorService.list(ver, 0, 10).getData();
            if (vendedor != null && !vendedor.isEmpty()) {
                return vendedor.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es una persona válida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof VendedorPojo) {
                return String.valueOf(((VendedorPojo) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
