package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteVentaNotaDebito;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para reporte de venta nota debito
 *
 * @author Antonella Lucero
 */
public class ReporteVentaNotaDebitoFilter extends Filter {

    /**
     * Agrega el filtro de identificador local talonario
     *
     * @param idLocalTalonario
     * @return
     */
    public ReporteVentaNotaDebitoFilter idLocalTalonario(Integer idLocalTalonario) {
        if (idLocalTalonario != null) {
            params.put("idLocalTalonario", idLocalTalonario);
        }
        return this;
    }
    
    public ReporteVentaNotaDebitoFilter idCliente(Integer idCliente) {
        
        if (idCliente != null) {
            params.put("idCliente", idCliente); 
        }
        return this;  
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public ReporteVentaNotaDebitoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public ReporteVentaNotaDebitoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param anulado
     * @return
     */
    public ReporteVentaNotaDebitoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param venta
     * @return
     */
    public static Map build(ReporteVentaNotaDebito venta) {
        ReporteVentaNotaDebitoFilter filter = new ReporteVentaNotaDebitoFilter();

        filter
                .idCliente(venta.getIdCliente())
                .fechaDesde(venta.getFechaDesde())
                .fechaHasta(venta.getFechaHasta())
                .anulado(venta.getAnulado())
                .idLocalTalonario(venta.getIdLocalTalonario());

        return filter.getParams();

    }

    /**
     * Constructor de la clase
     */
    public ReporteVentaNotaDebitoFilter() {
        super();
    }

}
