package py.com.sepsa.erp.web.v1.montoMinimo.pojos;

import java.math.BigDecimal;

/**
 * POJO de monto mínimo
 *
 * @author Romina E. Núñez Rojas
 */
public class MontoMinimo {

    /**
     * Identificador del Monto Mínimo
     */
    private Integer id;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Descripción del producto
     */
    private String producto;
    /**
     * Identificador del servicio
     */
    private Integer idServicio;
    /**
     * Código del producto
     */
    private String servicio;
    /**
     * Estado del monto mínimo
     */
    private String estado;
    /**
     * Monto
     */
    private BigDecimal monto;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    
//</editor-fold>

    /**
     * Contructor de la clase
     */
    public MontoMinimo() {

    }

    
    /**
     * Constructor con parámetros
     * @param id
     * @param idProducto
     * @param producto
     * @param idServicio
     * @param servicio
     * @param estado
     * @param monto 
     */
    public MontoMinimo(Integer id, Integer idProducto, String producto, Integer idServicio, String servicio, String estado, BigDecimal monto) {
        this.id = id;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.estado = estado;
        this.monto = monto;
    }

    

}
