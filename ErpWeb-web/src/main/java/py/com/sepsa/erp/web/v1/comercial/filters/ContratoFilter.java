package py.com.sepsa.erp.web.v1.comercial.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;

/**
 * Filtro utilizado para el servicio de contrato
 *
 * @author Romina E. Núñez Rojas
 */
public class ContratoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del contrato
     *
     * @param id
     * @return
     */
    public ContratoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public ContratoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param producto
     * @return
     */
    public ContratoFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador concepto factura
     *
     * @param idConceptoFactura
     * @return
     */
    public ContratoFilter idConceptoFactura(Integer idConceptoFactura) {
        if (idConceptoFactura != null) {
            params.put("idConceptoFactura", idConceptoFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param descripcion
     * @return
     */
    public ContratoFilter conceptoFactura(String conceptoFactura) {
        if (conceptoFactura != null && !conceptoFactura.trim().isEmpty()) {
            params.put("conceptoFactura", conceptoFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param idCliente
     * @return
     */
    public ContratoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de razón social
     *
     * @param razonSocial
     * @return
     */
    public ContratoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto mínimo
     *
     * @param montoMinimo
     * @return
     */
    public ContratoFilter montoMinimo(String montoMinimo) {
        if (montoMinimo != null && !montoMinimo.trim().isEmpty()) {
            params.put("montoMinimo", montoMinimo);
        }
        return this;
    }

    /**
     * Agrega el filtro de nro contrato
     *
     * @param nroContrato
     * @return
     */
    public ContratoFilter nroContrato(String nroContrato) {
        if (nroContrato != null && !nroContrato.trim().isEmpty()) {
            params.put("nroContrato", nroContrato);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha inicio
     *
     * @param fechaInicio
     * @return
     */
    public ContratoFilter fechaInicio(Date fechaInicio) {

        if (fechaInicio != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInicio);
                params.put("fechaInicio", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha fin
     *
     * @param fechaFin
     * @return
     */
    public ContratoFilter fechaFin(Date fechaFin) {
        if (fechaFin != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaFin);
                params.put("fechaFin", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de firmado
     *
     * @param firmado
     * @return
     */
    public ContratoFilter firmado(String firmado) {
        if (firmado != null && !firmado.trim().isEmpty()) {
            params.put("firmado", firmado);
        }
        return this;
    }

    /**
     * Agrega el filtro de documento aval
     *
     * @param documentoAval
     * @return
     */
    public ContratoFilter documentoAval(String documentoAval) {
        if (documentoAval != null && !documentoAval.trim().isEmpty()) {
            params.put("documentoAval", documentoAval);
        }
        return this;
    }

    /**
     * Agrega el filtro de renovacion automática
     *
     * @param renovacionAutomatica
     * @return
     */
    public ContratoFilter renovacionAutomatica(String renovacionAutomatica) {
        if (renovacionAutomatica != null && !renovacionAutomatica.trim().isEmpty()) {
            params.put("renovacionAutomatica", renovacionAutomatica);
        }
        return this;
    }

    /**
     * Agrega el filtro de debito automático
     *
     * @param debitoAutomatico
     * @return
     */
    public ContratoFilter debitoAutomatico(String debitoAutomatico) {
        if (debitoAutomatico != null && !debitoAutomatico.trim().isEmpty()) {
            params.put("debitoAutomatico", debitoAutomatico);
        }
        return this;
    }

    /**
     * Agrega el filtro de observación
     *
     * @param observacion
     * @return
     */
    public ContratoFilter observacion(String observacion) {
        if (observacion != null && !observacion.trim().isEmpty()) {
            params.put("observacion", observacion);
        }
        return this;
    }

    /**
     * Agrega el filtro de plazoRescision
     *
     * @param plazoRescision
     * @return
     */
    public ContratoFilter plazoRescision(String plazoRescision) {
        if (plazoRescision != null && !plazoRescision.trim().isEmpty()) {
            params.put("plazoRescision", plazoRescision);
        }
        return this;
    }

    /**
     * Agrega el filtro de estado
     *
     * @param estado
     * @return
     */
    public ContratoFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro de cobro adelantado
     *
     * @param cobroAdelantado
     * @return
     */
    public ContratoFilter cobroAdelantado(String cobroAdelantado) {
        if (cobroAdelantado != null && !cobroAdelantado.trim().isEmpty()) {
            params.put("cobroAdelantado", cobroAdelantado);
        }
        return this;
    }

    /**
     * Agrega el filtro de observación
     *
     * @param observacion
     * @return
     */
    public ContratoFilter fechaVencimientoAcuerdo(String fechaVencimientoAcuerdo) {
        if (fechaVencimientoAcuerdo != null && !fechaVencimientoAcuerdo.trim().isEmpty()) {
            params.put("fechaVencimientoAcuerdo", fechaVencimientoAcuerdo);
        }
        return this;
    }

    /**
     * Agrega el filtro del identificador tipo tarifa
     *
     * @param idTipoTarifa
     * @return
     */
    public ContratoFilter idTipoTarifa(Integer idTipoTarifa) {
        if (idTipoTarifa != null) {
            params.put("idTipoTarifa", idTipoTarifa);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipo tarifa
     *
     * @param tipoTarifa
     * @return
     */
    public ContratoFilter tipoTarifa(String tipoTarifa) {
        if (tipoTarifa != null && !tipoTarifa.trim().isEmpty()) {
            params.put("tipoTarifa", tipoTarifa);
        }
        return this;
    }

    /**
     * Agrega el filtro del identificador comercial
     *
     * @param idComercial
     * @return
     */
    public ContratoFilter idComercial(Integer idComercial) {
        if (idComercial != null) {
            params.put("idComercial", idComercial);
        }
        return this;
    }

    /**
     * Agrega el filtro de comercial
     *
     * @param comercial
     * @return
     */
    public ContratoFilter comercial(String comercial) {
        if (comercial != null && !comercial.trim().isEmpty()) {
            params.put("comercial", comercial);
        }
        return this;
    }

    /**
     * Agrega el filtro de servicios
     *
     * @param servicios
     * @return
     */
    public ContratoFilter servicios(List<ContratoServicio> servicios) {
        if (servicios != null && !servicios.isEmpty()) {
            params.put("servicios", servicios);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contrato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Contrato contrato, Integer page, Integer pageSize) {
        ContratoFilter filter = new ContratoFilter();

        filter
                .id(contrato.getId())
                .idProducto(contrato.getIdProducto())
                .producto(contrato.getProducto())
                .idConceptoFactura(contrato.getIdConceptoFactura())
                .conceptoFactura(contrato.getConceptoFactura())
                .idCliente(contrato.getIdCliente())
                .razonSocial(contrato.getRazonSocial())
                .montoMinimo(contrato.getMontoMinimo())
                .nroContrato(contrato.getNroContrato())
                .fechaInicio(contrato.getFechaInicio())
                .fechaFin(contrato.getFechaFin())
                .firmado(contrato.getFirmado())
                .documentoAval(contrato.getDocumentoAval())
                .renovacionAutomatica(contrato.getRenovacionAutomatica())
                .debitoAutomatico(contrato.getDebitoAutomatico())
                .observacion(contrato.getObservacion())
                .plazoRescision(contrato.getPlazoRescision())
                .estado(contrato.getEstado())
                .cobroAdelantado(contrato.getCobroAdelantado())
                .fechaVencimientoAcuerdo(contrato.getFechaVencimientoAcuerdo())
                .idTipoTarifa(contrato.getIdTipoTarifa())
                .tipoTarifa(contrato.getTipoTarifa())
                .servicios(contrato.getServicios())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ContratoFilter
     */
    public ContratoFilter() {
        super();
    }
}
