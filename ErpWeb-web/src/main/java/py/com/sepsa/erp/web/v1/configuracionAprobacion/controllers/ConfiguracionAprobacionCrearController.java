package py.com.sepsa.erp.web.v1.configuracionAprobacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionAprobacionService;

@ViewScoped
@Named("crearConfiguracionAprobacion")
public class ConfiguracionAprobacionCrearController implements Serializable {

    /**
     * Cliente para el servicio de etiqueta.
     */
    private final ConfiguracionAprobacionService service;

    /**
     * Datos del talonario
     */
    private ConfiguracionAprobacion configuracionAprobacion;

    /**
     * Getter y Setters.
     */
    public ConfiguracionAprobacion getConfiguracionAprobacion() {
        return configuracionAprobacion;
    }

    public void setConfiguracionAprobacion(ConfiguracionAprobacion configuracionAprobacion) {
        this.configuracionAprobacion = configuracionAprobacion;
    }
    

    /**
     * Método para crear y validar nueva etiqueta.
     */
    public void create() {
        configuracionAprobacion = service.create(configuracionAprobacion);
        if (configuracionAprobacion != null && configuracionAprobacion.getId() != null) {
            init();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Configuracion creada con éxito!", "Configuracion creada con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear Configuracion!", "Error al crear Configuracion!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.configuracionAprobacion = new ConfiguracionAprobacion();
    }

    /**
     * Método para reiniciar el formulario
     *
     * public void clear() { init();
    }
     */
    /**
     * Constructor de BookNewController
     */
    public ConfiguracionAprobacionCrearController() {
        init();
        this.service = new ConfiguracionAprobacionService();
    }

}
