/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.task;

import py.com.sepsa.erp.syncdte.SyncEstadoNotificacion;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorUtils;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.utils.automation.AbstractTask;
import py.com.sepsa.utils.automation.Task;
import py.com.sepsa.utils.info.remote.LoginService;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Williams Vera
 */
public class ReenvioNotificacionPendiente extends AbstractTask{
     /**
     * Identificador de empresa
     */
    private final Integer idEmpresa;

    public ReenvioNotificacionPendiente(Proceso proceo, Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
        this.task = new Task(proceo.getId(),
                String.format("%s - IdEmpresa:%d", proceo.getCodigo(), idEmpresa),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getFrecuencia()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getHora()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getMinuto()));
    }

    @Override
    public void task() {
        try {
            
                String user = ConfiguracionValorUtils.getUser(idEmpresa);
                String pass = ConfiguracionValorUtils.getPass(idEmpresa);
                String token = LoginService.doLogin(user, pass);
                SyncEstadoNotificacion sen = new SyncEstadoNotificacion(token, user, pass, idEmpresa);
                sen.run();  
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
