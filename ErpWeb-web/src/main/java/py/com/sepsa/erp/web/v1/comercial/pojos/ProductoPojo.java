package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.Date;

/**
 * POJO de producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ProductoPojo {

    /**
     * Identificador del producto
     */
    private Integer id;
    /**
     * Identificador del producto
     */
    private Integer idEmpresa;
    /**
     * Descripción del producto
     */
    private String empresa;
    /**
     * Descripción del producto
     */
    private String descripcion;
    /**
     * Código del producto
     */
    private String codigoGtin;
    /**
     * Código del producto
     */
    private String codigoInterno;
    /**
     * Estado del producto
     */
    private String activo;
    /**
     * Fecha de Inserción
     */
    private Date fechaInsercion;
    /**
     * Identificador del producto
     */
    private Integer porcentajeImpuesto;
    /**
     * Cuenta Contable
     */
    private String cuentaContable;
    /**
     * Identificador del producto
     */
    private Integer idAuditoria;
    /**
     * Identificador del producto
     */
    private Integer idAuditoriaDetalle;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(Integer porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public Integer getIdAuditoria() {
        return idAuditoria;
    }

    public void setIdAuditoria(Integer idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public Integer getIdAuditoriaDetalle() {
        return idAuditoriaDetalle;
    }

    public void setIdAuditoriaDetalle(Integer idAuditoriaDetalle) {
        this.idAuditoriaDetalle = idAuditoriaDetalle;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }
    
    

    /**
     * Contructor de la clase
     */
    public ProductoPojo() {

    }
}
