/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEstadoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoEstadoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para tipo tipoEstado
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEstadoService extends APIErpCore {

    /**
     * Obtiene la lista de tipo Estados
     *
     * @param tipoEstado
     * @param page
     * @param pageSize
     * @return
     */
    public TipoEstadoAdapter getTipoEstadoList(TipoEstado tipoEstado, Integer page,
            Integer pageSize) {

        TipoEstadoAdapter lista = new TipoEstadoAdapter();

        Map params = TipoEstadoFilter.build(tipoEstado, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_ESTADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoEstadoAdapter.class);

            lista = (TipoEstadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de TipoEstadoService
     */
    public TipoEstadoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_ESTADO("Servicio Tipo Estado", "tipo-estado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
