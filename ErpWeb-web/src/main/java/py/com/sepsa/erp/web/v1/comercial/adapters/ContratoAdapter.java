
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.remote.ContratoService;

/**
 * Adaptador para la lista de Descuento
 * @author Romina Núñez
 */

public class ContratoAdapter extends DataListAdapter<Contrato> {
    
    /**
     * Cliente para los servicios de Documento
     */
    private final ContratoService serviceContrato;

    /**
     * Constructor de InstallationListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContratoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceContrato = new ContratoService();
    }

    /**
     * Constructor de DocumentAdapter
     */
        public ContratoAdapter() {
        super();
        this.serviceContrato= new ContratoService();
    }


    @Override
    public ContratoAdapter fillData(Contrato searchData) {
        return serviceContrato.getContratoList(searchData, getFirstResult(), getPageSize());
    }

    
}
