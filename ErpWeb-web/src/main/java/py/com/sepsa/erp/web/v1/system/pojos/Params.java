
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 *  Representa los parametros del sistema
 * @author Cristina Insfrán
 */
public class Params {
     /**
     * Usuario para los mails de alertas
     */
    private String mailAlert= "alertas@sepsa.com.py";
    /**
     * Contraseña para el usuario del mail de alerta
     */
    private String mailPass= "SEPSA12345";
    /**
     * Dirección del servidor de mail
     */
    private String mailServer= "mail.sepsa.com.py";
    /**
     * Puerto del servidor de mail
     */
    private int mailPort= 25;
    /**
     * Mail del área de operaciones
     */
    private String mailOper;
    /**
     * Mail del área de TI
     */
    private String mailTI;
    /**
     * Usuario samba
     */
    private String smbUser= "";
    /**
     * Contraseña del usuario samba
     */
    private String smbUserPass= "";
    
    /**
     * Obtiene la contraseña para el usuario de mails de alertas
     * @return Contraseña para el usuario de mails de alertas
     */
    public String getMailPass() {
        return mailPass;
    }
    /**
     * Setea la contraseña para el usuario de mails de alertas
     * @param mailPass Contraseña para el usuario de mails de alertas
     */
    public void setMailPass(String mailPass) {
        this.mailPass = mailPass;
    }
    /**
     * Obtiene el puerto del servidor de mail
     * @return Puerto del servidor de mail
     */
    public int getMailPort() {
        return mailPort;
    }
    /**
     * Setea el puerto del servidor de mail
     * @param mailPort Puerto del servidor de mail
     */
    public void setMailPort(int mailPort) {
        this.mailPort = mailPort;
    }
    /**
     * Obtiene el mail del área de operaciones
     * @return Mail del área de operaciones
     */
    public String getMailOper() {
        return mailOper;
    }
    /**
     * Setea el mail del área de operaciones
     * @param mailOper Mail del área de operaciones
     */
    public void setMailOper(String mailOper) {
        this.mailOper = mailOper;
    }
    /**
     * Obtiene la dirección del servidor de mail
     * @return Dirección del servidor de mail
     */
    public String getMailServer() {
        return mailServer;
    }
    /**
     * Setea la dirección del servidor de mail
     * @param mailServer Dirección del servidor de mail
     */
    public void setMailServer(String mailServer) {
        this.mailServer = mailServer;
    }
    /**
     * Obtiene el usuario del mail para las alertas
     * @return Usuario del mail para las alertas
     */
    public String getMailAlert() {
        return mailAlert;
    }
    /**
     * Setea el usuario del mail para las alertas
     * @param mailAlert Usuario del mail para las alertas
     */
    public void setMailAlert(String mailAlert) {
        this.mailAlert = mailAlert;
    }
    /**
     * Obtiene el usuario samba
     * @return Usuario samba
     */
    public String getSmbUser() {
        return smbUser;
    }
    /**
     * Setea el usuario samba
     * @param smbUser Usuario samba
     */
    public void setSmbUser(String smbUser) {
        this.smbUser = smbUser;
    }
    /**
     * Obtiene la contraseña del usuario samba
     * @return Contraseña del usuario samba
     */
    public String getSmbUserPass() {
        return smbUserPass;
    }
    /**
     * Setea la contraseña del usuario samba
     * @param smbUserPass Contraseña del usuario samba
     */
    public void setSmbUserPass(String smbUserPass) {
        this.smbUserPass = smbUserPass;
    }
    /**
     * Obtiene el mail del área de TI
     * @return Mail del área de TI
     */
    public String getMailTI() {
        return mailTI;
    }
    /**
     * Setea el mail del área de TI
     * @param mailTI Mail del área de TI
     */
    public void setMailTI(String mailTI) {
        this.mailTI = mailTI;
    }
}
