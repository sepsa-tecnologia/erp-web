package py.com.sepsa.erp.web.v1.system.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;

/**
 * Filtro para la lista de menu
 *
 * @author Cristina Insfrán
 */
public class MenuFilter extends Filter {

    /**
     * Identificador del menu
     *
     * @param id
     * @return
     */
    public MenuFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro del titulo
     *
     * @param titulo
     * @return
     */
    public MenuFilter titulo(String titulo) {
        if (titulo != null && !titulo.trim().isEmpty()) {
            params.put("titulo", titulo);
        }
        return this;
    }

    /**
     * Agrega el filtro idPadre
     *
     * @param idPadre
     * @return
     */
    public MenuFilter idPadre(Integer idPadre) {
        if (idPadre != null) {
            params.put("idPadre", idPadre);
        }
        return this;
    }

    /**
     * Agrega el filtro idEmpresa
     *
     * @param idEmpresa
     * @return
     */
    public MenuFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro idEmpresa
     *
     * @param activo
     * @return
     */
    public MenuFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro del url
     *
     * @param url
     * @return
     */
    public MenuFilter url(String url) {
        if (url != null && !url.trim().isEmpty()) {
            params.put("url", url);
        }
        return this;
    }

    /**
     * Agrega el filtro tiene padre
     *
     * @param tienePadre
     * @return
     */
    public MenuFilter tienePadre(String tienePadre) {
        if (tienePadre != null && !tienePadre.trim().isEmpty()) {
            params.put("tienePadre", tienePadre);
        }
        return this;
    }

    /**
     * Agrega el filtro tiene hijo
     *
     * @param tieneHijo
     * @return
     */
    public MenuFilter tieneHijo(String tieneHijo) {
        if (tieneHijo != null && !tieneHijo.trim().isEmpty()) {
            params.put("tieneHijo", tieneHijo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param menu
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Menu menu, Integer page, Integer pageSize) {
        MenuFilter filter = new MenuFilter();

        filter
                .id(menu.getId())
                .titulo(menu.getTitulo())
                .idPadre(menu.getIdPadre())
                .url(menu.getUrl())
                .tienePadre(menu.isTienePadre())
                .tieneHijo(menu.getTieneHijo())
                .idEmpresa(menu.getIdEmpresa())
                .activo(menu.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public MenuFilter() {
        super();
    }

}
