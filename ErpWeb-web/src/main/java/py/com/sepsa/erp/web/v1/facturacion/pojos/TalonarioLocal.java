/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Local;

/**
 * Pojo para Talonario Local
 *
 * @author Romina Núñez
 */
public class TalonarioLocal {

    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * Inicio
     */
    private Integer idLocal;
    /**
     * Activo
     */
    private String activo;
    /**
     * Talonario
     */
    private Talonario talonario;
    /**
     * Local
     */
    private Local local;
    

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public Local getLocal() {
        return local;
    }

    public String getActivo() {
        return activo;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }
    
    

    public TalonarioLocal() {
    }
}
