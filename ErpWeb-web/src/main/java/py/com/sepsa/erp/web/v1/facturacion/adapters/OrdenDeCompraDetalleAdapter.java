/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetallesPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;

/**
 * Adapter para orden de compra
 *
 * @author Romina Núñez
 */
public class OrdenDeCompraDetalleAdapter extends DataListAdapter<OrdenCompraDetallesPojo> {

    /**
     * Cliente para el servicio de OrdenDeCompra
     */
    private final OrdenDeCompraService serviceOrdenDeCompra;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public OrdenDeCompraDetalleAdapter fillData(OrdenCompraDetallesPojo searchData) {

        return serviceOrdenDeCompra.getOrdenDeCompraDetalleList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de OrdenDeCompraAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public OrdenDeCompraDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceOrdenDeCompra = new OrdenDeCompraService();
    }

    /**
     * Constructor de OrdenDeCompraAdapter
     */
    public OrdenDeCompraDetalleAdapter() {
        super();
        this.serviceOrdenDeCompra = new OrdenDeCompraService();
    }
}
