
package py.com.sepsa.erp.web.v1.usuario.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioPerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.UsuarioPerfilFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfil;

/**
 * Cliente para el servico de Usuario Perfil
 * @author Cristina Insfrán
 */
public class UsuarioPerfilServiceClient extends APIErpCore{
    
     /**
     * Método para setear userPerfil
     * @param userPerfil
     * @return 
     */
    public UsuarioPerfil setUserPerfil(UsuarioPerfil userPerfil){
       
        UsuarioPerfil up=new UsuarioPerfil();
        
         HttpURLConnection conn = PUT(Resource.USUARIO_PERFIL.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(userPerfil));
     
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioPerfil.class);
            
            up = (UsuarioPerfil) response.getPayload();
      
            conn.disconnect();
        }
        return up;
    }
    
    /**
     * Obtiene la lista de usuario perfiles
     *
     * @param up
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioPerfilListAdapter getUserPerfilList(UsuarioPerfil up, Integer page,
            Integer pageSize) {
       
        UsuarioPerfilListAdapter lista = new UsuarioPerfilListAdapter();
        
        Map params = UsuarioPerfilFilter.build(up, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_PERFIL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioPerfilListAdapter.class);

            lista = (UsuarioPerfilListAdapter) response.getPayload();
           
            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Constructor de UsuarioPerfilServiceClient
     */
    public UsuarioPerfilServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        USUARIO_PERFIL("Asociar Usuario Perfil", "usuario-perfil");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
