/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.ParametroAdicional;
import py.com.sepsa.erp.web.v1.facturacion.remote.ParametroAdicionalService;

/**
 *
 * @author Williams Vera
 */
public class ParametroAdicionalAdapter extends DataListAdapter<ParametroAdicional> {
    
     /**
     * Cliente para el servicio de facturacion
     */
    private final ParametroAdicionalService serviceParametroAdicional;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ParametroAdicionalAdapter fillData(ParametroAdicional searchData) {

        return serviceParametroAdicional.getParametroAdicionalList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ParametroAdicionalAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ParametroAdicionalAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceParametroAdicional = new ParametroAdicionalService();
    }

    /**
     * Constructor de ParametroAdicionalAdapter
     */
    public ParametroAdicionalAdapter() {
        super();
        this.serviceParametroAdicional = new ParametroAdicionalService();
    }
    
}
