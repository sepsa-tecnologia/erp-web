package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Filro utilizado para los clientes del servicio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ClientFilter extends Filter {

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public ClientFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de id Estado
     *
     * @param idEstado
     * @return
     */
    public ClientFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por idCanalVenta
     *
     * @param idCanalVenta
     * @return
     */
    public ClientFilter idCanalVenta(Integer idCanalVenta) {
        if (idCanalVenta != null) {
            params.put("idCanalVenta", idCanalVenta);
        }
        return this;
    }

    /**
     * Agrega el filtro para razon social
     *
     * @param razonSocial
     * @return
     */
    public ClientFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro para razon social
     *
     * @param proveedor
     * @return
     */
    public ClientFilter proveedor(String proveedor) {
        if (proveedor != null && !proveedor.trim().isEmpty()) {
            params.put("proveedor", proveedor);
        }
        return this;
    }

    /**
     * Agrega el filtro para razon social
     *
     * @param comprador
     * @return
     */
    public ClientFilter comprador(String comprador) {
        if (comprador != null && !comprador.trim().isEmpty()) {
            params.put("comprador", comprador);
        }
        return this;
    }

    /**
     * Agrega el filtro para canalVenta
     *
     * @param canalVenta
     * @return
     */
    public ClientFilter canalVenta(CanalVenta canalVenta) {
        if (canalVenta != null) {
            params.put("canalVenta", canalVenta);
        }
        return this;
    }

    /**
     * Agrega el filtro para clave Pago
     *
     * @param clavePago
     * @return
     */
    public ClientFilter clavePago(String clavePago) {
        if (clavePago != null && !clavePago.trim().isEmpty()) {
            params.put("clavePago", clavePago);
        }
        return this;
    }

    /**
     * Agrega el filtro para factura Electronica
     *
     * @param factElectronica
     * @return
     */
    public ClientFilter factElectronica(String factElectronica) {
        if (factElectronica != null && !factElectronica.trim().isEmpty()) {
            params.put("factElectronica", factElectronica);
        }
        return this;
    }

    /**
     * Agregar el filtro de id TipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public ClientFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro para el número de documento
     *
     * @param nroDocumento
     * @return
     */
    public ClientFilter nroDocumento(String nroDocumento) {
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            params.put("nroDocumento", nroDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro para tipo Documento
     *
     * @param tipoDocumento
     * @return
     */
    public ClientFilter tipoDocumento(TipoDocumento tipoDocumento) {
        if (tipoDocumento != null) {
            params.put("tipoDocumento", tipoDocumento);
        }
        return this;
    }

    /**
     * Agregar el filtro de id Persona
     *
     * @param persona
     * @return
     */
    public ClientFilter persona(Persona persona) {
        if (persona != null) {
            params.put("persona", persona);
        }
        return this;
    }

    /**
     * Agrega el filtro para el número de documento
     *
     * @param codigoEstado
     * @return
     */
    public ClientFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param client datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Cliente client, Integer page, Integer pageSize) {
        ClientFilter filter = new ClientFilter();

        filter
                .idCliente(client.getIdCliente())
                .idEstado(client.getIdEstado())
                .codigoEstado(client.getCodigoEstado())
                .idCanalVenta(client.getIdCanalVenta())
                .razonSocial(client.getRazonSocial())
                .canalVenta(client.getCanalVenta())
                .clavePago(client.getClavePago())
                .factElectronica(client.getFactElectronica())
                .idTipoDocumento(client.getIdTipoDocumento())
                .nroDocumento(client.getNroDocumento())
                .tipoDocumento(client.getTipoDocumento())
                .persona(client.getPersona())
                .proveedor(client.getProveedor())
                .comprador(client.getComprador())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClientFilter
     */
    public ClientFilter() {
        super();
    }
}
