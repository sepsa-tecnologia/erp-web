/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDocumentoProceso;
import py.com.sepsa.erp.web.v1.info.remote.TipoDocumentoProcesoService;

/**
 * Adapter para Tipo Documento proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoProcesoListAdapter extends DataListAdapter<TipoDocumentoProceso> {

    /**
     * Cliente para el servicio de tipo documento proceso
     */
    private final TipoDocumentoProcesoService serviceTipoDocumentoProceso;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoDocumentoProcesoListAdapter fillData(TipoDocumentoProceso searchData) {

        return serviceTipoDocumentoProceso.getTipoDocumentoProcesoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoProcesoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoDocumentoProcesoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoDocumentoProceso = new TipoDocumentoProcesoService();
    }

    /**
     * Constructor de TalonarioAdapter
     */
    public TipoDocumentoProcesoListAdapter() {
        super();
        this.serviceTipoDocumentoProceso = new TipoDocumentoProcesoService();
    }
}
