/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;
import py.com.sepsa.erp.web.v1.repositorio.remote.TipoRepositorioService;



/**
 * Adapter para Tipo Repositorio
 *
 * @author Romina Núñez
 */
public class TipoRepositorioAdapter extends DataListAdapter<TipoRepositorio> {

    /**
     * Cliente para los servicios de Tipo Retencion
     */
    private final TipoRepositorioService tipoRepositorioClient;

    @Override
    public TipoRepositorioAdapter fillData(TipoRepositorio searchData) {
        return tipoRepositorioClient.getTipoRepositorioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoRetencionAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoRepositorioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoRepositorioClient = new TipoRepositorioService();
    }

    /**
     * Constructor de TipoRepositorioAdapter
     */
    public TipoRepositorioAdapter() {
        super();
        this.tipoRepositorioClient = new TipoRepositorioService();
    }
}
