package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;

/**
 * POJO para Datos de Factura
 *
 * @author Romina Núñez
 */
public class DatosCobro {

    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * N° Factura
     */
    private String nroRecibo;
    /**
     * Timbrado
     */
    private Integer timbrado;
    /**
     * Fecha Vto Timbrado
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Fecha de Vencimiento de Factura
     */
    private Date fechaVencimiento;
    /**
     * Días de crédito
     */
    private Integer diasCredito;
    /**
     * telefono
     */
    private String telefono;
    /**
     * direccion
     */
    private String direccion;
    /**
     * email
     */
    private String email;
    /**
     * Nro de casa
     */
    private String nroCasa;
    /**
     * Punto de expedicion
     */
    private String puntoExpedicion;
    /**
     * Identificador de departamento 
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Establecimiento
     */
    private String establecimiento;

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public Integer getDiasCredito() {
        return diasCredito;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }
    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getTimbrado() {
        return timbrado;
    }

    /**
     * Constructor de la clase
     */
    public DatosCobro() {
    }


}
