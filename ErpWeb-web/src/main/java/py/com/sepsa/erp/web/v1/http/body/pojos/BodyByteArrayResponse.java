/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.http.body.pojos;

import com.google.gson.JsonSyntaxException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import org.json.JSONException;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.http.pojos.HttpStreamResponse;

/**
 * POJO para el cuerpo de las respuestas HTTP
 *
 * @author Daniel F. Escauriza Arza
 */
public class BodyByteArrayResponse {

    /**
     * Datos de respuesta
     */
    protected byte[] payload;

    /**
     * Obtiene los datos de la respuesta
     *
     * @return Datos de la respuesta
     */
    public byte[] getPayload() {
        return payload;
    }

    /**
     * Setea los datos de la respuesta
     *
     * @param payload Datos de la respuesta
     */
    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    /**
     * Parsea la respuesta en bruto a un objeto BodyResponse
     *
     * @param conn Conexión HTTP
     * @return Objecto BodyResponse con el payload formateado
     */
    public static BodyByteArrayResponse createInstance(HttpURLConnection conn, PrintWriter writer) {

        if (writer != null) {
            writer.append(LINE_FEED)
                    .append("----------------------------------------------------")
                    .append(LINE_FEED);
            writer.close();
        }

        BodyByteArrayResponse bodyResponse = new BodyByteArrayResponse();

        try {

            HttpStreamResponse response = HttpStreamResponse
                    .createInstance(conn);

            bodyResponse = new BodyByteArrayResponse(response.getInput());

        } catch (JsonSyntaxException | JSONException ex) {
            WebLogger.get().fatal(ex);
        }

        return bodyResponse;
    }

    /**
     * Parsea la respuesta en bruto a un objeto BodyResponse
     *
     * @param conn Conexión HTTP
     * @return Objecto BodyResponse con el payload formateado
     */
    public static BodyByteArrayResponse createInstance(HttpURLConnection conn) {


       return createInstance(conn, null);
    }

    /**
     * Constructor de BodyByteArrayResponse
     *
     * @param payload Datos de la respuesta
     */
    public BodyByteArrayResponse(byte[] payload) {
        this.payload = payload;
    }

    /**
     * Constructor de BodyByteArrayResponse
     */
    public BodyByteArrayResponse() {
    }

}
