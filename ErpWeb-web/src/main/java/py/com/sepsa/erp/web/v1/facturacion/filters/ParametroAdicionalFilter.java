package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ParametroAdicional;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para listado de Parametro Adicional
 *
 * @author Williams Vera
 */
public class ParametroAdicionalFilter extends Filter {

    /**
     * Agrega el filtro por identificador de parametro adicional
     *
     * @param id
     * @return
     */
    public ParametroAdicionalFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public ParametroAdicionalFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro codigo
     *
     * @param codigo
     * @return
     */
    public ParametroAdicionalFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro activo
     *
     * @param activo
     * @return
     */
    public ParametroAdicionalFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }
    
    /**
     * Agrega el filtro por parámetro mandatorio
     *
     * @param mandatorio
     * @return
     */
    public ParametroAdicionalFilter mandatorio(String mandatorio) {
        if ((mandatorio != null) && (!mandatorio.trim().isEmpty())){
            params.put("mandatorio", mandatorio);
        }
        return this;
    }
    
    
    /**
     * Agrega el filtro por parámetro visible
     *
     * @param visible
     * @return
     */
    public ParametroAdicionalFilter visible(Character visible) {
        if (visible != null) {
            params.put("visible", visible);
        }
        return this;
    }
    
        /**
     * Agrega el filtro por parámetro codigo de tipo de parametro
     *
     * @param codigoTipoParametroAdicional
     * @return
     */
    public ParametroAdicionalFilter codigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        if (codigoTipoParametroAdicional != null && !codigoTipoParametroAdicional.trim().isEmpty()) {
            params.put("codigoTipoParametroAdicional", codigoTipoParametroAdicional);
        }
        return this;
    }
    
    
    
    public static Map build(ParametroAdicional param, Integer page, Integer pageSize) {
        ParametroAdicionalFilter filter = new ParametroAdicionalFilter();
        
        filter
                .id(param.getId())
                .idEmpresa(param.getIdEmpresa())
                .codigo(param.getCodigo())
                .activo(param.getActivo())
                .visible(param.getVisible())
                .codigoTipoParametroAdicional(param.getCodigoTipoParametroAdicional())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public ParametroAdicionalFilter() {
        super();
    }
    
    
}
