package py.com.sepsa.erp.web.v1.montoMinimo.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.montoMinimo.pojos.MontoMinimo;

/**
 * Filtro utilizado para el monto mínimo
 *
 * @author Romina Núñez
 */
public class MontoMinimoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del monto mínimo
     *
     * @param id
     * @return
     */
    public MontoMinimoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del producto
     *
     * @param idProducto
     * @return
     */
    public MontoMinimoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del servicio
     *
     * @param idServicio
     * @return
     */
    public MontoMinimoFilter idServicio(Integer idServicio) {
        if (idServicio != null) {
            params.put("idServicio", idServicio);
        }
        return this;
    }

    /**
     * Agrega el filtro para la descripción
     *
     * @param producto
     * @return
     */
    public MontoMinimoFilter producto(String producto) {
        if (producto != null) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro para la descripción
     *
     * @param servicio
     * @return
     */
    public MontoMinimoFilter servicio(String servicio) {
        if (servicio != null) {
            params.put("servicio", servicio);
        }
        return this;
    }

    /**
     * Agrega el filtro para el estado
     *
     * @param estado
     * @return
     */
    public MontoMinimoFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro para el monto
     *
     * @param monto
     * @return
     */
    public MontoMinimoFilter monto(BigDecimal monto) {
        if (monto != null) {
            params.put("monto", monto);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param service datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(MontoMinimo montoMinimo, Integer page, Integer pageSize) {
        MontoMinimoFilter filter = new MontoMinimoFilter();

        filter
                .id(montoMinimo.getId())
                .idProducto(montoMinimo.getIdProducto())
                .producto(montoMinimo.getProducto())
                .idServicio(montoMinimo.getIdServicio())
                .servicio(montoMinimo.getServicio())
                .estado(montoMinimo.getEstado())
                .monto(montoMinimo.getMonto())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ServiceFilter
     */
    public MontoMinimoFilter() {
        super();
    }

}
