package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para el listado de Marca
 *
 * @author Sergio D. Riveros Vazquez & Alexander Triana
 */
public class Marca {

    /**
     * Identificador de Marca
     */
    private Integer id;
    /**
     * Identificador de Empresa
     */
    private Integer idEmpresa;
    /**
     * Descripción de Marca
     */
    private String descripcion;
    /**
     * Código de Marca
     */
    private String codigo;
    /**
     * Empresa relacionada con la Marca
     */
    private Empresa empresa;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public Marca() {
    }

    /**
     * Constructor de la clase con parámetros
     *
     * @param id
     */
    public Marca(Integer id) {
        this.id = id;
    }

}
