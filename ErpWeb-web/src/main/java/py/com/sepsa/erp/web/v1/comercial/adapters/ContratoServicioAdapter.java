
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;
import py.com.sepsa.erp.web.v1.comercial.remote.ContratoServicioService;

/**
 * Adaptador para la lista de Descuento
 * @author Romina Núñez
 */

public class ContratoServicioAdapter extends DataListAdapter<ContratoServicio> {
    
    /**
     * Cliente para los servicios de Documento
     */
    private final ContratoServicioService serviceContratoServicio;

    /**
     * Constructor de InstallationListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContratoServicioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceContratoServicio = new ContratoServicioService();
    }

    /**
     * Constructor de DocumentAdapter
     */
        public ContratoServicioAdapter() {
        super();
        this.serviceContratoServicio= new ContratoServicioService();
    }


    @Override
    public ContratoServicioAdapter fillData(ContratoServicio searchData) {
        return serviceContratoServicio.getContratoServicioList(searchData, getFirstResult(), getPageSize());
    }

    
}
