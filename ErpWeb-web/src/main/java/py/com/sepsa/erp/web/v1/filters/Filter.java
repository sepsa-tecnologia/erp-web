
package py.com.sepsa.erp.web.v1.filters;

import java.util.HashMap;
import java.util.Map;

/**
 * Filtro para consultas a servicios GET
 * @author Daniel F. Escauriza Arza
 */
public class Filter {
    
    /**
     * Mapa de parámetros para filtros
     */
    protected Map params;

    /**
     * Agrega al filtro el número de página
     * @param page Número de página
     * @return Filter
     */
    public Filter page(Integer page) {
        if(page != null) {
            params.put("firstResult", page);
        }
        
        return this;
    }
    
    /**
     * Agrega al filtro el tamaño de página resultado
     * @param pageSize Tamaño de la página resultado
     * @return Filter
     */
    public Filter pageSize(Integer pageSize) {
        if(pageSize != null) {
            params.put("pageSize", pageSize);
        }
        
        return this;
    }
    
    /**
     * Obtiene el mapa de parámetros
     * @return Mapa de parámetros
     */
    public Map getParams() {
        return params;
    }

    /**
     * Setea el mapa de parámetros
     * @param params Mapa de parámetros
     */
    public void setParams(Map params) {
        this.params = params;
    }

    /**
     * Constructor de Filter
     */
    public Filter() {
        this.params = new HashMap();
    }
    
}
