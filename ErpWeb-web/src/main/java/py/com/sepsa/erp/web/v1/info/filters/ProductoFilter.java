/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * Filter para producto info
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ProductoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public ProductoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de producto
     *
     * @param descripcion
     * @return
     */
    public ProductoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigoGtin del producto
     *
     * @param codigoGtin
     * @return
     */
    public ProductoFilter codigoGtin(String codigoGtin) {
        if (codigoGtin != null && !codigoGtin.trim().isEmpty()) {
            params.put("codigoGtin", codigoGtin);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigoInterno del producto
     *
     * @param codigoInterno
     * @return
     */
    public ProductoFilter codigoInterno(String codigoInterno) {
        if (codigoInterno != null && !codigoInterno.trim().isEmpty()) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo del producto
     *
     * @param activo
     * @return
     */
    public ProductoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public ProductoFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public ProductoFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro de porcentajeImpuesto de producto
     *
     * @param porcentajeImpuesto
     * @return
     */
    public ProductoFilter porcentajeImpuesto(Integer porcentajeImpuesto) {
        if (porcentajeImpuesto != null) {
            params.put("porcentajeImpuesto", porcentajeImpuesto);
        }
        return this;
    }

    /**
     * Agrega el filtro de idMarca de producto
     *
     * @param idMarca
     * @return
     */
    public ProductoFilter idMarca(Integer idMarca) {
        if (idMarca != null) {
            params.put("idMarca", idMarca);
        }
        return this;
    }

    /**
     * Agrega el filtro de idMarca de producto
     *
     * @param idMarca
     * @return
     */
    public ProductoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de marca de producto
     *
     * @param marca
     * @return
     */
    public ProductoFilter marca(Marca marca) {
        if (marca != null) {
            params.put("marca", marca);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Producto producto, Integer page, Integer pageSize) {
        ProductoFilter filter = new ProductoFilter();

        filter
                .id(producto.getId())
                .descripcion(producto.getDescripcion())
                .codigoGtin(producto.getCodigoGtin())
                .codigoInterno(producto.getCodigoInterno())
                .activo(producto.getActivo())
                .fechaInsercionDesde(producto.getFechaInsercionDesde())
                .fechaInsercionHasta(producto.getFechaInsercionHasta())
                .porcentajeImpuesto(producto.getPorcentajeImpuesto())
                .idMarca(producto.getIdMarca())
                .marca(producto.getMarca())
                .idEmpresa(producto.getIdEmpresa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public ProductoFilter() {
        super();
    }
}
