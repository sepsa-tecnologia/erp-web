/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAnulacionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para la vista crear motivo anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoAnulacionCreate")
public class MotivoAnulacionCreateController implements Serializable {

    /**
     * Cliente para el servicio
     */
    private FacturacionService service;
    /**
     * POJO del motivoAnulacion
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Adaptador para la lista de motivo anulacion
     */
    private TipoMotivoAnulacionAdapter tipoMotivoAnulacionAdapterList;
    /**
     * POJO de Tipo motivo anulacion
     */
    private TipoMotivoAnulacion tipoMotivoAnulacionFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public TipoMotivoAnulacionAdapter getTipoMotivoAnulacionAdapterList() {
        return tipoMotivoAnulacionAdapterList;
    }

    public void setTipoMotivoAnulacionAdapterList(TipoMotivoAnulacionAdapter tipoMotivoAnulacionAdapterList) {
        this.tipoMotivoAnulacionAdapterList = tipoMotivoAnulacionAdapterList;
    }

    public TipoMotivoAnulacion getTipoMotivoAnulacionFilter() {
        return tipoMotivoAnulacionFilter;
    }

    public void setTipoMotivoAnulacionFilter(TipoMotivoAnulacion tipoMotivoAnulacionFilter) {
        this.tipoMotivoAnulacionFilter = tipoMotivoAnulacionFilter;
    }

    public FacturacionService getService() {
        return service;
    }

    public void setService(FacturacionService service) {
        this.service = service;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {
        BodyResponse<MotivoAnulacion> respuestaTal = service.createMotivoAnulacion(motivoAnulacion);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Motivo de Anulacion creado correctamente!"));
            init();
        }

    }

    /**
     * Método para obtener tipoMotivoAnulacion
     */
    public void getTipoMotivoAnulaciones() {
        this.tipoMotivoAnulacionAdapterList.setFirstResult(0);
        this.tipoMotivoAnulacionFilter.setActivo("S");
        this.tipoMotivoAnulacionAdapterList = this.tipoMotivoAnulacionAdapterList.fillData(tipoMotivoAnulacionFilter);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.service = new FacturacionService();
        this.motivoAnulacion = new MotivoAnulacion();
        this.tipoMotivoAnulacionAdapterList = new TipoMotivoAnulacionAdapter();
        this.tipoMotivoAnulacionFilter = new TipoMotivoAnulacion();
        getTipoMotivoAnulaciones();
    }

    /**
     * Constructor
     */
    public MotivoAnulacionCreateController() {
        init();
        this.service = new FacturacionService();
    }

}
