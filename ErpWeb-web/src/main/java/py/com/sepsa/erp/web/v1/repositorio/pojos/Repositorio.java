/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * POJO para Repositorio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Repositorio {

    /**
     * Identificador del repositorio
     */
    private Integer id;
    /**
     * Identificador del tipo de repositorio
     */
    private Integer idTipoRepositorio;
    /**
     * Tipo de repositorio
     */
    private TipoRepositorio tipoRepositorio;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Cliente
     */
    private String cliente;
    /**
     * Clave de acceso del repositorio
     */
    private String claveAcceso;
    /**
     * Clave secreta del repositorio
     */
    private String claveSecreta;
    /**
     * Region del repositorio
     */
    private String region;
    /**
     * Bucket
     */
    private String bucket;
    /**
     * Estado del repositorio
     */
    private String activo;
    /**
     * Identificador de la empresa
     */
    private Integer idEmpresa;

    /**
     * Identificador de la empresa
     */
    private Empresa empresa;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id to set id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the idTipoRepositorio
     */
    public Integer getIdTipoRepositorio() {
        return idTipoRepositorio;
    }

    /**
     * @param idTipoRepositorio to set idTipoRepositorio
     */
    public void setIdTipoRepositorio(Integer idTipoRepositorio) {
        this.idTipoRepositorio = idTipoRepositorio;
    }

    public void setTipoRepositorio(TipoRepositorio tipoRepositorio) {
        this.tipoRepositorio = tipoRepositorio;
    }

    public TipoRepositorio getTipoRepositorio() {
        return tipoRepositorio;
    }

    /**
     * @return the idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente to set idCliente
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the claveAcceso
     */
    public String getClaveAcceso() {
        return claveAcceso;
    }

    /**
     * @param claveAcceso to set claveAcceso
     */
    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    /**
     * @return the claveSecreta
     */
    public String getClaveSecreta() {
        return claveSecreta;
    }

    /**
     * @param claveSecreta to set claveSecreta
     */
    public void setClaveSecreta(String claveSecreta) {
        this.claveSecreta = claveSecreta;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region to set region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the bucket
     */
    public String getBucket() {
        return bucket;
    }

    /**
     * @param bucket to set bucket
     */
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getActivo() {
        return activo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    //</editor-fold>
    /**
     *
     * @param id
     */
    public Repositorio(Integer id) {
        this.id = id;
    }

    /**
     * Constructor
     */
    public Repositorio() {

    }
}
