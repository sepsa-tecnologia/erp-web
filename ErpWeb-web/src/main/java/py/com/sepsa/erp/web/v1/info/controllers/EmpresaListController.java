/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmpresa;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar Empresas
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarEmpresas")
public class EmpresaListController implements Serializable {

    /**
     * Adaptador de empresa.
     */
    private EmpresaAdapter adapter;
    /**
     * Objeto empresa.
     */
    private Empresa empresaFilter;
    /**
     * Adaptador de la lista de empresas
     */
    private TipoEmpresaAdapter tipoEmpresaAdapter;
    /**
     * Datos del cliente
     */
    private TipoEmpresa tipoEmpresa;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    /**
     * @return the adapter
     */
    public EmpresaAdapter getAdapter() {
        return adapter;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(EmpresaAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * @return the empresaFilter
     */
    public Empresa getEmpresaFilter() {
        return empresaFilter;
    }

    /**
     * @param empresaFilter the empresaFilter to set
     */
    public void setEmpresaFilter(Empresa empresaFilter) {
        this.empresaFilter = empresaFilter;
    }

    public TipoEmpresaAdapter getTipoEmpresaAdapter() {
        return tipoEmpresaAdapter;
    }

    public void setTipoEmpresaAdapter(TipoEmpresaAdapter tipoEmpresaAdapter) {
        this.tipoEmpresaAdapter = tipoEmpresaAdapter;
    }

    public TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    //</editor-fold>
    /**
     * Método para filtrar empresas.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(empresaFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.empresaFilter = new Empresa();
        tipoEmpresa = new TipoEmpresa();
        getTipoEmpresas();
        buscar();
    }

    /**
     * Método para obtener las tipoEmpresas
     */
    public void getTipoEmpresas() {
        this.setTipoEmpresaAdapter(getTipoEmpresaAdapter().fillData(getTipoEmpresa()));
    }

    /**
     * Metodo para redirigir a la vista Editar Talonario
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("empresa-edit?faces-redirect=true&id=%d", id);
    }

    @PostConstruct
    public void init() {
        try {
            this.adapter = new EmpresaAdapter();
            this.empresaFilter = new Empresa();
            this.tipoEmpresaAdapter = new TipoEmpresaAdapter();
            this.tipoEmpresa = new TipoEmpresa();
            getTipoEmpresas();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
