/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;
import py.com.sepsa.erp.web.v1.info.remote.ProcesoService;

/**
 * Adapter para proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ProcesoAdapter extends DataListAdapter<Proceso> {

    /**
     * Cliente para el servicio proceso
     */
    private final ProcesoService procesoService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProcesoAdapter fillData(Proceso searchData) {

        return procesoService.getProcesoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ProcesoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProcesoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.procesoService = new ProcesoService();
    }

    /**
     * Constructor de ProcesoAdapter
     */
    public ProcesoAdapter() {
        super();
        this.procesoService = new ProcesoService();
    }
}
