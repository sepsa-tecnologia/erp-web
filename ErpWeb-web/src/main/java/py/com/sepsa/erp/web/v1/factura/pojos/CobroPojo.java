package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * POJO para Cobro
 *
 * @author Romina Núñez,Cristina Insfrán
 */
public class CobroPojo {
    /**
     * Identificador de cobro
     */
    private Integer id;
    /**
     * Identificador de la empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Razón social del cliente
     */
    private String razonSocial;
    /**
     * Nro. Documento del cliente
     */
    private String nroDocumento;
    /**
     * Identificador de lugar cobro
     */
    private Integer idLugarCobro;
    /**
     * Lugar cobro
     */
    private String lugarCobro;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Monto Cobro
     */
    private BigDecimal montoCobro;
    /**
     * Número recibo
     */
    private String nroRecibo;
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Descripcion de estado
     */
    private String estado;
    /**
     * Código de estado
     */
    private String codigoEstado;
    /**
     * Saldo cliente
     */
    private BigDecimal saldoCliente;
    /**
     * Fecha desde
     */
    private Date fechaDesde;
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    /**
     * Identificador de tipo de transaccion red
     */
    private Integer idTransaccionRed;
    /**
     * Fecha de envío
     */
    private Date fechaEnvio;
    /**
     * Enviado
     */
    private Character enviado;
    /**
     * Transaccion red
     */
    private TransaccionRed transaccionRed;
    /**
     * Detalle de cobro
     */
    private List<CobroDetalle> cobroDetalles;
    //Agregados segun necesidad

    /**
     * Identificador de motivo anulación
     */
    private Integer idMotivoAnulacion;
    
    /**
     * Observación de motivo de anulacion
     */
    private String observacionAnulacion;
    
    /**
     * Descripcion de motivoAnulacion
     */
    private String motivoAnulacion;
    
    /**
     * Campo digital
     */
    private String digital;
    /**
     * Bandera listadoPojo
     */
    private Boolean listadoPojo;
    /**
     * Moneda
     */
    private String moneda;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    
    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }
    
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getLugarCobro() {
        return lugarCobro;
    }

    public void setLugarCobro(String lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    public String getEstado() {
        return estado;
    }
    
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the talonario
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the idEmpresa
     */
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    } 

    /**
     * @return the cobroDetalles
     */
    public List<CobroDetalle> getCobroDetalles() {
        return cobroDetalles;
    }

    /**
     * @param cobroDetalles the cobroDetalles to set
     */
    public void setCobroDetalles(List<CobroDetalle> cobroDetalles) {
        this.cobroDetalles = cobroDetalles;
    }

    /**
     * @return the codigoEstado
     */
    public String getCodigoEstado() {
        return codigoEstado;
    }

    /**
     * @param codigoEstado the codigoEstado to set
     */
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    /**
     * @return the digital
     */
    public String getDigital() {
        return digital;
    }

    /**
     * @param digital the digital to set
     */
    public void setDigital(String digital) {
        this.digital = digital;
    }

    /**
     * @return the idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the transaccionRed
     */
    public TransaccionRed getTransaccionRed() {
        return transaccionRed;
    }

    /**
     * @param transaccionRed the transaccionRed to set
     */
    public void setTransaccionRed(TransaccionRed transaccionRed) {
        this.transaccionRed = transaccionRed;
    }

    /**
     * @return the enviado
     */
    public Character getEnviado() {
        return enviado;
    }

    /**
     * @param enviado the enviado to set
     */
    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    /**
     * @return the fechaEnvio
     */
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    /**
     * @param fechaEnvio the fechaEnvio to set
     */
    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    /**
     * @return the idTransaccionRed
     */
    public Integer getIdTransaccionRed() {
        return idTransaccionRed;
    }

    /**
     * @param idTransaccionRed the idTransaccionRed to set
     */
    public void setIdTransaccionRed(Integer idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }


    /**
     * @return the saldoCliente
     */
    public BigDecimal getSaldoCliente() {
        return saldoCliente;
    }

    /**
     * @param saldoCliente the saldoCliente to set
     */
    public void setSaldoCliente(BigDecimal saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
   
//</editor-fold>

    /**
     * Constructor de la clase
     */
    public CobroPojo() {
    }

    /**
     * Constructor con parametros
     *
     * @param id
     * @param idLugarCobro
     * @param lugarCobro
     * @param fecha
     * @param montoCobro
     * @param nroRecibo
     * @param idTalonario
     * @param estado
     */
    public CobroPojo(Integer id, Integer idLugarCobro, LugarCobro lugarCobro,
            Date fecha, BigDecimal montoCobro, String nroRecibo,
            Integer idTalonario, Estado estado) {
        this.id = id;
        this.idLugarCobro = idLugarCobro;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idTalonario = idTalonario;
    }

    /**
     * Constructor con parámetros
     * @param id
     * @param idCliente
     * @param cliente
     * @param idLugarCobro
     * @param lugarCobro
     * @param fecha
     * @param montoCobro
     * @param nroRecibo
     * @param idTalonario
     * @param estado
     * @param saldoCliente
     * @param fechaDesde
     * @param fechaHasta
     * @param idTransaccionRed
     * @param fechaEnvio
     * @param enviado
     * @param transaccionRed
     * @param cobroDetalles
     * @param nroFactura
     * @param totalFactura
     * @param idFactura
     * @param tieneRetention
     * @param fechaRetencion
     * @param nroRetencion
     * @param porcentajeRetencion
     * @param montoRetenidoTotal
     * @param digital 
     */
    public CobroPojo(Integer id, Integer idCliente, Cliente cliente, Integer idLugarCobro,
            LugarCobro lugarCobro, Date fecha, BigDecimal montoCobro, String nroRecibo, 
            Integer idTalonario, Estado estado, BigDecimal saldoCliente, Date fechaDesde, 
            Date fechaHasta, Integer idTransaccionRed, Date fechaEnvio, Character enviado,
            TransaccionRed transaccionRed, List<CobroDetalle> cobroDetalles, String nroFactura, 
            BigDecimal totalFactura, Integer idFactura, String tieneRetention, Date fechaRetencion,
            String nroRetencion, Integer porcentajeRetencion, BigDecimal montoRetenidoTotal, 
            String digital) {
        this.id = id;
        this.idCliente = idCliente;
        this.idLugarCobro = idLugarCobro;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idTalonario = idTalonario;
        this.saldoCliente = saldoCliente;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.idTransaccionRed = idTransaccionRed;
        this.fechaEnvio = fechaEnvio;
        this.enviado = enviado;
        this.transaccionRed = transaccionRed;
        this.cobroDetalles = cobroDetalles;
        this.digital = digital;
    }
    
    

}
