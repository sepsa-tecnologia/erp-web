/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.FrecuenciaEjecucionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar procesos
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarProcesos")
public class ProcesoListController implements Serializable {

    /**
     * Adaptador de proceso.
     */
    private ProcesoAdapter adapter;
    /**
     * Objeto proceso.
     */
    private Proceso procesoFilter;
    /**
     * Adaptador de la lista de tipo procesos
     */
    private TipoProcesoAdapter tipoProcesoAdapter;
    /**
     * Datos del cliente
     */
    private TipoProceso tipoProceso;
    /**
     * Adaptador de la lista de tipo procesos
     */
    private FrecuenciaEjecucionAdapter frecuenciaEjecucionAdapter;
    /**
     * Datos del cliente
     */
    private FrecuenciaEjecucion frecuenciaEjecucion;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    /**
     * @return the adapter
     */
    public ProcesoAdapter getAdapter() {
        return adapter;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(ProcesoAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * @return the procesoFilter
     */
    public Proceso getProcesoFilter() {
        return procesoFilter;
    }

    /**
     * @param procesoFilter the procesoFilter to set
     */
    public void setProcesoFilter(Proceso procesoFilter) {
        this.procesoFilter = procesoFilter;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public TipoProcesoAdapter getTipoProcesoAdapter() {
        return tipoProcesoAdapter;
    }

    public void setTipoProcesoAdapter(TipoProcesoAdapter tipoProcesoAdapter) {
        this.tipoProcesoAdapter = tipoProcesoAdapter;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(TipoProceso tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public FrecuenciaEjecucionAdapter getFrecuenciaEjecucionAdapter() {
        return frecuenciaEjecucionAdapter;
    }

    public void setFrecuenciaEjecucionAdapter(FrecuenciaEjecucionAdapter frecuenciaEjecucionAdapter) {
        this.frecuenciaEjecucionAdapter = frecuenciaEjecucionAdapter;
    }

    public FrecuenciaEjecucion getFrecuenciaEjecucion() {
        return frecuenciaEjecucion;
    }

    public void setFrecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        this.frecuenciaEjecucion = frecuenciaEjecucion;
    }

    //</editor-fold>
    /**
     * Método para filtrar procesos.
     */
    public void buscar() {
        this.adapter = adapter.fillData(procesoFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.empresaAutocomplete = new Empresa();
        this.procesoFilter = new Proceso();
        this.tipoProceso = new TipoProceso();
        this.frecuenciaEjecucion = new FrecuenciaEjecucion();
        getTipoProcesos();
        getFrecuenciaEjecucionList();
        buscar();
    }

    /**
     * Método para obtener las tipoProcesos
     */
    public void getTipoProcesos() {
        this.setTipoProcesoAdapter(getTipoProcesoAdapter().fillData(getTipoProceso()));
    }

    /**
     * Método para obtener Frecuencia Ejecucion List
     */
    public void getFrecuenciaEjecucionList() {
        this.setFrecuenciaEjecucionAdapter(getFrecuenciaEjecucionAdapter().fillData(getFrecuenciaEjecucion()));
    }

    /**
     * Metodo para redirigir a la vista Editar procesos
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("proceso-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        procesoFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    @PostConstruct
    public void init() {
        try {
            this.empresaAutocomplete = new Empresa();
            this.empresaAdapter = new EmpresaAdapter();
            this.adapter = new ProcesoAdapter();
            this.procesoFilter = new Proceso();
            this.tipoProcesoAdapter = new TipoProcesoAdapter();
            this.tipoProceso = new TipoProceso();
            this.frecuenciaEjecucionAdapter = new FrecuenciaEjecucionAdapter();
            this.frecuenciaEjecucion = new FrecuenciaEjecucion();
            getTipoProcesos();
            getFrecuenciaEjecucionList();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
