package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * POJO para Factura
 *
 * @author Romina Núñez
 */
public class FacturaDetallePojo {

    /**
     * Identificador de Factura Detalle
     */
    private Integer id;
    /**
     * Identificador de fcatura
     */
    private Integer idFactura;
    /**
     * Número de línea
     */
    private Integer nroLinea;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * Monto IVA
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Número de línea
     */
    private Integer idProducto;
    /**
     * Descripción
     */
    private String producto;
    /**
     * Descripción
     */
    private String codigoGtin;
    /**
     * Descripción
     */
    private String codigoInterno;
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    /**
     * Precio Unitario
     */
    private BigDecimal precioUnitarioConIva;
    /**
     * Precio Unitario
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoParticularUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoGlobalUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoParticular;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoGlobal;
    /**
     * Precio Unitario
     */
    private BigDecimal montoExentoGravado;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeGravada;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * Codigo metrica
     */
    private String codigoMetrica;
    /**
     * Descripcion metrica
     */
    private String metrica;
    
    private String nroLote;
    
    private Date fechaVencimientoLote;

    public Integer getId() {
        return id;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(Integer porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public String getMetrica() {
        return metrica;
    }

    public void setMetrica(String metrica) {
        this.metrica = metrica;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    } 

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }
    

    /**
     * Constructor de la clase
     */
    public FacturaDetallePojo() {
    }

}
