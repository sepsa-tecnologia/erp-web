package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ReferenciaGeograficaFilter;
import py.com.sepsa.erp.web.v1.info.filters.TipoRefGeograficaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente remoto para servicio de R
 *
 * @author Cristina Insfrán
 */
public class ReferenciaGeograficaService extends APIErpCore {

    /**
     * Obtiene la lista de referencia Geográfica
     *
     * @param tipoRef
     * @param page
     * @param pageSize
     * @return
     */
    public ReferenciaGeograficaAdapter getRefGeograficaList(ReferenciaGeografica tipoRef, Integer page,
            Integer pageSize) {

        ReferenciaGeograficaAdapter lista = new ReferenciaGeograficaAdapter();

        Map params = ReferenciaGeograficaFilter.build(tipoRef, page, pageSize);

        HttpURLConnection conn = GET(Resource.REF_GEOGRAFICA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ReferenciaGeograficaAdapter.class);

            lista = (ReferenciaGeograficaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear una nueva referencia geográfica
     *
     * @param referencia
     * @return
     */
    public BodyResponse<ReferenciaGeografica> setReferencia(ReferenciaGeografica referencia) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CREAR_REFERENCIA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(referencia));
            response = BodyResponse.createInstance(conn, ReferenciaGeografica.class);
        }
        return response;
    }

    /**
     * Método para editar una referencia geográfica
     *
     * @param referencia
     * @return
     */
    public BodyResponse<ReferenciaGeografica> editReferencia(ReferenciaGeografica referencia) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.EDITAR_REFERENCIA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(referencia));
            response = BodyResponse.createInstance(conn, ReferenciaGeografica.class);
        }
        return response;
    }

    /**
     * Obtiene la lista de tipo referencia Geográfica
     *
     * @param tipoRef
     * @param page
     * @param pageSize
     * @return
     */
    public TipoReferenciaGeograficaAdapter getTipoRefGeograficaList(TipoReferenciaGeografica tipoRef, Integer page,
            Integer pageSize) {

        TipoReferenciaGeograficaAdapter lista = new TipoReferenciaGeograficaAdapter();

        Map params = TipoRefGeograficaFilter.build(tipoRef, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_REF_GEO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoReferenciaGeograficaAdapter.class);

            lista = (TipoReferenciaGeograficaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para obtener detalles del objeto a editar.
     *
     * @param id
     * @return
     */
    public ReferenciaGeografica get(Integer id) {
        ReferenciaGeografica data = new ReferenciaGeografica(id);
        ReferenciaGeograficaAdapter list = getRefGeograficaList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de la clase
     */
    public ReferenciaGeograficaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_REF_GEO("Tipo Referencia Geografica", "tipo-referencia-geografica"),
        REF_GEOGRAFICA("Referencia Geográfica", "referencia-geografica"),
        CREAR_REFERENCIA("Crear Referencia Geográfica", "referencia-geografica"),
        EDITAR_REFERENCIA("Editar Referencia Geográfica", "referencia-geografica");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
