package py.com.sepsa.erp.web.task;

import java.io.Serializable;
import static java.util.Objects.isNull;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.concurrent.ManagedExecutorService;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorUtils;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.adapters.ProcesoAdapter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.utils.automation.AbstractTask;
import py.com.sepsa.utils.info.remote.LoginService;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
public class TaskEjecutor implements Serializable {
    
    /**
     * Manejador de tareas asincronas
     */
    @Resource
    protected ManagedExecutorService executor;

    public void ejecutar() {

        Empresa efilter = new Empresa();
        efilter.setActivo("S");
        EmpresaAdapter eadapter = new EmpresaAdapter();
        eadapter.setPageSize(1000);
        eadapter = eadapter.fillData(efilter);
        
        for (Empresa empresa : eadapter.getData()) {
            
            String user = ConfiguracionValorUtils.getUser(empresa.getId());
            String pass = ConfiguracionValorUtils.getPass(empresa.getId());

            String token = LoginService.doLogin(user, pass);

            token = token != null && !token.toLowerCase().startsWith("bearer ")
                    ? String.format("Bearer %s", token)
                    : token;

            Proceso filter = new Proceso();
            filter.setActivo('S');
            filter.setIdEmpresa(empresa.getId());
            filter.setCodigoTipoNotificacion("EJECUCION_PERIODICA");

            ProcesoAdapter adapter = new ProcesoAdapter(token, 0, 5000);
            adapter.fillData(filter);
            
            for (Proceso proceso : adapter.getData()) {
               
                AbstractTask abstractTask = null;
                switch (proceso.getCodigo()) {
                    case "ALERTA_TALONARIO_POR_VENCER":
                        abstractTask = new AlertaTalonarioPorVencer(proceso, empresa.getId());
                        break;

                    case "ALERTA_CANTIDAD_HOJAS_TALONARIO":
                        abstractTask = new AlertaCantidadHojasTalonario(proceso, empresa.getId());
                        break;
//
//                    case "GENERACION_DTE_FACTURA":
//                        abstractTask = new GeneracionDteFactura(proceso, empresa.getId());
//                        break;
//
                    case "GENERACION_DTE_NOTACREDITO":
                        abstractTask = new GeneracionDteNotaCredito(proceso, empresa.getId());
                        break;

                    case "IMPORTAR_ARCHIVOS_EDI":
                        abstractTask = new ImportarArchivosEDI(proceso, empresa.getId());
                        break;

                    case "GENERAR_ARCHIVOS_EDI":
                        abstractTask = new GenerarArchivosEDI(proceso, empresa.getId());
                        break;

                    case "RESGUARDO_DOCUMENTO":
                        abstractTask = new ResguardoDocumento(proceso, empresa.getId());
                        break;
                        
                    case "GENERACION_DTE_NOTADEBITO":
                        abstractTask = new GeneracionDteNotaDebito(proceso, empresa.getId());
                        break;
                    case "SINCRONIZACION_ESTADO_DTE":
                            abstractTask = new SyncEstadoDte(proceso, empresa.getId());
                        break;
                        
                    case "GENERACION_DTE_NOTAREMISION":
                        abstractTask = new GeneracionDteNotaRemision(proceso, empresa.getId());
                        break;
                        
                    case "GENERACION_DTE_AUTOFACTURA":
                        abstractTask = new GeneracionDteAutofactura(proceso, empresa.getId());
                        break;
                    
                    case "REENVIO_NOTIFICACION":
                        abstractTask = new ReenvioNotificacionPendiente(proceso, empresa.getId());
                        break;
                    
                    case "GENERACION_DTE_FACTURA_MULTIEMPRESA":
                        abstractTask = new GeneracionDteFacturaMultiempresa(proceso, empresa.getId());
                        break;
//                        
//                    case "REENVIO_NOTIFICACION_MULTIEMPRESA":
//                        abstractTask = new ReenvioNotificacionPendienteMultiempresa(proceso, empresa.getId());
//                        break;
//                        
                    case "SINCRONIZACION_ESTADO_DTE_MULTIEMPRESA":
                        abstractTask = new SyncEstadoDteMultiempresa(proceso, empresa.getId());
                        break;  
                                   
                    case "GENERACION_DTE_NOTACREDITO_MULTIEMPRESA":
                        abstractTask = new GeneracionDteNotaCreditoMultiempresa(proceso, empresa.getId());
                        break;
                        
                    case "GENERACION_DTE_NOTAREMISION_MULTIEMPRESA":
                        abstractTask = new GeneracionDteNotaRemisionMultiempresa(proceso, empresa.getId());
                        break;
                        
                    case "GENERACION_DTE_AUTOFACTURA_MULTIEMPRESA":
                        abstractTask = new GeneracionDteAutofacturaMultiempresa(proceso, empresa.getId());
                        break;
                    
                    case "GENERACION_DTE_NOTADEBITO_MULTIEMPRESA":
                        abstractTask = new GeneracionDteNotaDebitoMultiempresa(proceso, empresa.getId());
                        break;
                                   
                }
               
                if (!isNull(abstractTask)) {
                    executor.execute(abstractTask);
                }
            }
        }
    }

    @javax.ejb.Asynchronous
    public void asyncExecute() {
        
        Boolean continuarIntentando = Boolean.TRUE;
        
        while (continuarIntentando) {            
            
            try {
                
                ejecutar();
                continuarIntentando = Boolean.FALSE;
                
            } catch (Exception e) {
                
                try {
                    Thread.sleep(5000);
                } catch (Exception ex) {
                    WebLogger.get().fatal(ex);
                }
                
                WebLogger.get().fatal(e);
            }
        }
        
    }
}
