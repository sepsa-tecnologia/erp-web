
package py.com.sepsa.erp.web.v1.usuario.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 *  POJO para usuario perfil
 * @author Cristina Insfrán, Romina Núñez
 */
public class UsuarioPerfil {

    /**
     * Identificador de usuario perfil
     */
    private Integer id;
     /**
     * Identificador del Usuario
     */
    private Integer idUsuario;
    /**
     * Identificador del perfil
     */
    private Integer idPerfil;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Perfil
     */
    private Perfil perfil;
    /**
     * Estado
     */
    private Estado estado;
     /**
     * Codigo estado
     */
    private String codigoEstado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    /**
     * @return the codigoEstado
     */
    public String getCodigoEstado() {
        return codigoEstado;
    }

    /**
     * @param codigoEstado the codigoEstado to set
     */
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    
    /**
     * Constructor 
     */
    public UsuarioPerfil(){
        
    }
}
