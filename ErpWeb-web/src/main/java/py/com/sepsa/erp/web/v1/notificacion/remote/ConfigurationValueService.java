/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.notificacion.adapters.ConfigurationValueAdapter;
import py.com.sepsa.erp.web.v1.notificacion.filters.ConfigurationValueFilter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ConfigurationValue;
import py.com.sepsa.erp.web.v1.remote.APIErpNotificacion;

/**
 * Cliente para el servicio Configuration value
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ConfigurationValueService extends APIErpNotificacion {

    /**
     * Obtiene la lista de configurationValue.
     *
     * @param configurationValue
     * @param page
     * @param pageSize
     * @return
     */
    public ConfigurationValueAdapter getConfigurationValueList(ConfigurationValue configurationValue, Integer page,
            Integer pageSize) {

        ConfigurationValueAdapter lista = new ConfigurationValueAdapter();

        Map params = ConfigurationValueFilter.build(configurationValue, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfigurationValueAdapter.class);

            lista = (ConfigurationValueAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de ConfigurationValueService
     */
    public ConfigurationValueService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        LISTAR("Listado de Configuracion Valor", "v1/configurationValue");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
