package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de la entidad persona rol
 *
 * @author Cristina Insfrán
 * @author Romina Núñez
 */
public class PersonaRol {

    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del rol
     */
    private Integer idRol;
    /**
     * Persona
     */
    private String persona;
    /**
     * Rol
     */
    private String rol;
    /**
     * Estado
     */
    private String estado;
    /**
     * Tiene
     */
    private String tiene;

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the idRol
     */
    public Integer getIdRol() {
        return idRol;
    }

    /**
     * @param idRol the idRol to set
     */
    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setTiene(String tiene) {
        this.tiene = tiene;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getTiene() {
        return tiene;
    }

    public String getRol() {
        return rol;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    /**
     * Constructor
     */
    public PersonaRol() {

    }

    /**
     * Constructor con parametros
     *
     * @param idPersona
     * @param idRol
     */
    public PersonaRol(Integer idPersona, Integer idRol) {
        this.idPersona = idPersona;
        this.idRol = idRol;

    }

    /**
     * Constructor con parámetros.
     *
     * @param idPersona
     * @param idRol
     * @param persona
     * @param rol
     * @param estado
     * @param tiene
     */
    public PersonaRol(Integer idPersona, Integer idRol, String persona, String rol, String estado, String tiene) {
        this.idPersona = idPersona;
        this.idRol = idRol;
        this.persona = persona;
        this.rol = rol;
        this.estado = estado;
        this.tiene = tiene;
    }

}
