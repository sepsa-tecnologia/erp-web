
package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;

/**
 * Filtro para tipo cobro
 * @author Cristina Insfrán
 */
public class TipoCobroFilter extends Filter{
    
     /**
     * Agrega el filtro de identificador del tipo de documento
     *
     * @param id
     * @return
     */
    public TipoCobroFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public TipoCobroFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion.trim());
        }
        return this;
    }
    
     /**
     * Agrega el filtro para codigo
     *
     * @param codigo
     * @return
     */
    public TipoCobroFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo.trim());
        }
        return this;
    }
    
       /**
     * Agrega el filtro para estado
     *
     * @param estado
     * @return
     */
    public TipoCobroFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }
    
     /**
     * Agrega el filtro por parámetro de búsqueda.
     *
     * @param charToString Identificador de charToString.
     * @return
     */
    public TipoCobroFilter charToString(Boolean charToString) {
        if (charToString != null) {
            params.put("charToString", charToString);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     * 
     * @param tipo
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoCobro tipo, Integer page, Integer pageSize) {
        TipoCobroFilter filter = new TipoCobroFilter();

        filter
                .id(tipo.getId())
                .descripcion(tipo.getDescripcion())
                .codigo(tipo.getCodigo())
                .estado(tipo.getEstado())
                .charToString(tipo.getCharToString())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoCobroFilter
     */
    public TipoCobroFilter() {
        super();
    }
    
}
