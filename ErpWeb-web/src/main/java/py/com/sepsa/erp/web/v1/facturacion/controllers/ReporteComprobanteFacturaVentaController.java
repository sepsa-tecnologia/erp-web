package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteVentaParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteServiceClient;

/**
 * Controlador para reporte de comprobante de factura de venta
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("reporteComprobanteVenta")
public class ReporteComprobanteFacturaVentaController implements Serializable {

    /**
     * Objeto
     */
    private ReporteComprobanteVentaParam parametro;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private ReporteServiceClient reporteCliente;
    
    private Cliente client;
    
    private ClientListAdapter adapterCliente;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public ReporteServiceClient getReporteCliente() {
        return reporteCliente;
    }

    public void setParametro(ReporteComprobanteVentaParam parametro) {
        this.parametro = parametro;
    }

    public ReporteComprobanteVentaParam getParametro() {
        return parametro;
    }

    public void setReporteCliente(ReporteServiceClient reporteCliente) {
        this.reporteCliente = reporteCliente;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }
    
    //</editor-fold>
    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {

        String contentType = ("application/zip");
        String fileName = "reporte-comprobante-factura-venta.zip";
        byte[] data = reporteCliente.getReporteComprobanteFacturaVenta(parametro);
        if (data != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(data),
                    contentType, fileName);
        }

        return null;

    }
    
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Detalle de lista de facturas pendientes y canceladas del cliente
     * seleccionado
     */
    public void onItemSelectCliente() {
        if (client != null && client.getIdCliente() != null) {
            this.parametro.setIdCliente(client.getIdCliente());
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteComprobanteVentaParam();
        this.reporteCliente = new ReporteServiceClient();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        
        Calendar today = Calendar.getInstance();
        parametro.setFecha(today.getTime());
    }

}
