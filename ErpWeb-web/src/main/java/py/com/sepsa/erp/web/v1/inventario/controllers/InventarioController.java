package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioResumenAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.pojos.Inventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para lista de inventario
 *
 * @author Alexander Triana, Romina Núñez
 */
@ViewScoped
@Named("listarInventario")
public class InventarioController implements Serializable {

    /**
     * Adaptador para la lista de inventario
     */
    private InventarioAdapter inventarioAdapter;

    /**
     * Pojo de inventario
     */
    private Inventario inventario;

    /**
     * Objeto producto
     */
    private Producto producto;

    /**
     * Adptador de la lista de productos
     */
    private ProductoAdapter adapterProducto;

    /**
     * Objeto depositoLogistico
     */
    private DepositoLogistico deposito;

    /**
     * Adaptador de la lista de depositoLogistico
     */
    private DepositoLogisticoAdapter depositoAdapter;

    /**
     * Adaptador para la lista de estados
     */
    private EstadoAdapter estadoAdapterList;

    /**
     * POJO de estado
     */
    private Estado estado;
    /**
     * Dato para selección de listado
     */
    private Integer tipoInventario;

    /**
     * Adaptador para la lista de inventario
     */
    private InventarioResumenAdapter inventarioResumenAdapter;

    /**
     * Pojo de inventario
     */
    private InventarioResumen inventarioResumen;

    /**
     * Adaptador para la lista de inventario
     */
    private InventarioDetalleAdapter inventarioDetalleAdapter;

    /**
     * Pojo de inventario
     */
    private InventarioDetalle inventarioDetalle;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public InventarioAdapter getInventarioAdapter() {
        return inventarioAdapter;
    }

    public void setInventarioDetalleAdapter(InventarioDetalleAdapter inventarioDetalleAdapter) {
        this.inventarioDetalleAdapter = inventarioDetalleAdapter;
    }

    public void setInventarioDetalle(InventarioDetalle inventarioDetalle) {
        this.inventarioDetalle = inventarioDetalle;
    }

    public InventarioDetalleAdapter getInventarioDetalleAdapter() {
        return inventarioDetalleAdapter;
    }

    public InventarioDetalle getInventarioDetalle() {
        return inventarioDetalle;
    }

    public void setInventarioResumenAdapter(InventarioResumenAdapter inventarioResumenAdapter) {
        this.inventarioResumenAdapter = inventarioResumenAdapter;
    }

    public void setInventarioResumen(InventarioResumen inventarioResumen) {
        this.inventarioResumen = inventarioResumen;
    }

    public InventarioResumenAdapter getInventarioResumenAdapter() {
        return inventarioResumenAdapter;
    }

    public InventarioResumen getInventarioResumen() {
        return inventarioResumen;
    }

    public void setTipoInventario(Integer tipoInventario) {
        this.tipoInventario = tipoInventario;
    }

    public Integer getTipoInventario() {
        return tipoInventario;
    }

    public void setInventarioAdapter(InventarioAdapter inventarioAdapter) {
        this.inventarioAdapter = inventarioAdapter;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public DepositoLogistico getDeposito() {
        return deposito;
    }

    public void setDeposito(DepositoLogistico deposito) {
        this.deposito = deposito;
    }

    public DepositoLogisticoAdapter getDepositoAdapter() {
        return depositoAdapter;
    }

    public void setDepositoAdapter(DepositoLogisticoAdapter depositoAdapter) {
        this.depositoAdapter = depositoAdapter;
    }

    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    //</editor-fold>

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {
        Producto producto = new Producto();
        producto.setDescripcion(query);
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProductoResumen(SelectEvent event) {
        inventarioResumen.setIdProducto(((Producto) event.getObject()).getId());
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        inventario.setIdProducto(((Producto) event.getObject()).getId());
    }

    /**
     * Autocomplete para depositoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        DepositoLogistico deposito = new DepositoLogistico();
        deposito.setDescripcion(query);
        deposito.setListadoPojo(true);
        depositoAdapter = depositoAdapter.fillData(deposito);

        return depositoAdapter.getData();
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        inventario.setIdDepositoLogistico(((DepositoLogistico) event.getObject()).getId());
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDepositoResumen(SelectEvent event) {
        inventarioResumen.setIdDepositoLogistico(((DepositoLogistico) event.getObject()).getId());
    }

    /**
     * Método para obtener estado
     */
    public void getEstados() {
        this.setEstadoAdapterList(getEstadoAdapterList().fillData(getEstado()));
    }

    public void filter() {
        if (tipoInventario == 1) {
            filterResumenInventario();
        } else {
            getListaInventario();
        }
    }

    /**
     * Método para obetner lista de inventario
     */
    public void getListaInventario() {
        getInventarioAdapter().setFirstResult(0);
        getInventario().setListadoPojo(true);
        this.setInventarioAdapter(getInventarioAdapter().
                fillData(getInventario()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        if (tipoInventario == 1) {
            this.inventarioResumen = new InventarioResumen();
            filterResumenInventario();
        } else {
            this.inventario = new Inventario();
            getListaInventario();
        }

    }

    public void filterResumenInventario() {
        this.inventarioResumenAdapter.setFirstResult(0);
        this.inventarioResumen.setListadoPojo(Boolean.TRUE);
        this.inventarioResumenAdapter = inventarioResumenAdapter.fillData(inventarioResumen);
    }

    public void filtrarTipo() {
        if (tipoInventario == 1) {
            filterResumenInventario();
        } else if (tipoInventario == 2) {
            getListaInventario();
        }
    }

    /**
     * Método para filtrar el detalle de inventario
     *
     * @param idInventario
     */
    public void filterDetalleInventario(Integer idInventario) {
        this.inventarioDetalle = new InventarioDetalle();
        this.inventarioDetalleAdapter = new InventarioDetalleAdapter();

        this.inventarioDetalle.setIdInventario(idInventario);
        this.inventarioDetalle.setListadoPojo(Boolean.TRUE);

        this.inventarioDetalleAdapter = inventarioDetalleAdapter.fillData(inventarioDetalle);
    }

    /**
     * Método para encontrar la información del contacto
     *
     * @param event
     */
    public void onRowToggle(ToggleEvent event) {
        WebLogger.get().debug("entra");

        Integer id = ((Inventario) event.getData()).getId();

        filterDetalleInventario(id);
    }

    /**
     * Inicializa los datos del controlador
     */
    private void init() {
        this.inventarioAdapter = new InventarioAdapter();
        this.inventario = new Inventario();
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.deposito = new DepositoLogistico();
        this.deposito.setListadoPojo(true);
        this.depositoAdapter = new DepositoLogisticoAdapter();
        this.estado = new Estado();
        this.estadoAdapterList = new EstadoAdapter();
        this.tipoInventario = 1;
        this.inventarioResumen = new InventarioResumen();
        this.inventarioResumenAdapter = new InventarioResumenAdapter();

        this.inventarioDetalle = new InventarioDetalle();
        this.inventarioDetalleAdapter = new InventarioDetalleAdapter();

        filterResumenInventario();
        getEstados();
    }

    /**
     * Constructor de la clase
     */
    public InventarioController() {
        init();
    }

}
