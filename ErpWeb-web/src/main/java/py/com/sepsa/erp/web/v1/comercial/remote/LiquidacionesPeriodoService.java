package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionesPeriodoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.LiquidacionesPeriodoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesPeriodo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para el listado de liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionesPeriodoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de liquidaciones por periodo.
     *
     * @param searchData
     * @param page
     * @param pageSize
     * @return
     */
    public LiquidacionesPeriodoAdapter getLiquidacionesPeriodoList(LiquidacionesPeriodo searchData,
            Integer page, Integer pageSize) {

        LiquidacionesPeriodoAdapter lista = new LiquidacionesPeriodoAdapter();

        Map params = LiquidacionesPeriodoFilter.build(searchData, page, pageSize);

        HttpURLConnection conn = GET(Resource.LIQUIDACIONES_PERIODO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LiquidacionesPeriodoAdapter.class);

            lista = (LiquidacionesPeriodoAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    /**
     * Constructor.
     */
    public LiquidacionesPeriodoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaFacturación.
     */
    public enum Resource {

        //Servicios
        LIQUIDACIONES_PERIODO("Servicios de Liquidaciones por periodo", "historico-liquidacion/periodo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
