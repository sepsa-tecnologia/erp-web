/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.info.remote.EstadoService;

/**
 * controlador para crear estado
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("estadoCreate")
public class EstadoCreateController implements Serializable {

    /**
     * Cliente para el servicio estado.
     */
    private final EstadoService service;
    /**
     * POJO del Estado
     */
    private Estado estado;
    /**
     * Adaptador para la lista de TipoEstado
     */
    private TipoEstadoAdapter tipoEstadoAdapterList;
    /**
     * POJO de TipoEstado
     */
    private TipoEstado tipoEstadoFilter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public TipoEstadoAdapter getTipoEstadoAdapterList() {
        return tipoEstadoAdapterList;
    }

    public void setTipoEstadoAdapterList(TipoEstadoAdapter tipoEstadoAdapterList) {
        this.tipoEstadoAdapterList = tipoEstadoAdapterList;
    }

    public TipoEstado getTipoEstadoFilter() {
        return tipoEstadoFilter;
    }

    public void setTipoEstadoFilter(TipoEstado tipoEstadoFilter) {
        this.tipoEstadoFilter = tipoEstadoFilter;
    }

    //</editor-fold>
    /**
     * Método para crear Estado
     */
    public void create() {
        BodyResponse<Estado> respuestaTal = service.setEstado(this.estado);
        if (respuestaTal.getSuccess()) {
            this.estado = new Estado();
            this.tipoEstadoFilter = new TipoEstado();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Estado creado correctamente!"));
        }
    }

    /**
     * Método para obtener los tipos de estados filtradas
     *
     * @param query
     * @return
     */
    public List<TipoEstado> completeQueryTipoEstado(String query) {
        TipoEstado tipoEstadoObj = new TipoEstado();
        tipoEstadoObj.setDescripcion(query);
        tipoEstadoAdapterList = tipoEstadoAdapterList.fillData(tipoEstadoObj);
        return tipoEstadoAdapterList.getData();
    }

    /**
     * Selecciona el tipo de estado
     *
     * @param event
     */
    public void onItemSelectTipoEstadoFilter(SelectEvent event) {
        tipoEstadoFilter.setId(((TipoEstado) event.getObject()).getId());
        this.estado.setIdTipoEstado(tipoEstadoFilter.getId());
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.estado = new Estado();
        this.tipoEstadoAdapterList = new TipoEstadoAdapter();
        this.tipoEstadoFilter = new TipoEstado();
    }

    /**
     * Constructor
     */
    public EstadoCreateController() {
        init();
        this.service = new EstadoService();
    }
}
