/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import java.util.Date;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;

/**
 * pojo para canal venta cliente
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CanalVentaCliente {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Fecha insercion
     */
    private Date fechaInsercion;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador del canal de venta
     */
    private Integer idCanalVenta;
    /**
     * Cliente
     */
    private Cliente cliente;
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    //</editor-fold>

    public CanalVentaCliente() {
    }

}
