package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de Contacto
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class Contacto {

    /**
     * Identificador del contacto
     */
    private Integer idContacto;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador del cargo
     */
    private Integer idCargo;
    /**
     * Identificador del tipo contacto
     */
    private Integer idTipoContacto;
    /**
     * Cargo
     */
    private Cargo cargo;
    /**
     * Persona
     */
    private Persona persona;
    /**
     * tipo contacto
     */
    private TipoContacto tipoContacto;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public TipoContacto getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(TipoContacto tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    //</editor-fold>
    /**
     * Constructor
     */
    public Contacto() {

    }

    /**
     * Constructor con parametros
     *
     * @param idContacto
     * @param idTipoContacto
     * @param idCargo
     * @param estado
     */
    public Contacto(Integer idContacto, Integer idTipoContacto, Integer idCargo,
            String estado) {
        this.idContacto = idContacto;
        this.idTipoContacto = idTipoContacto;
        this.idCargo = idCargo;
    }

}
