/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import com.google.gson.JsonElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class ReporteEmpresa {

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;

    /**
     * Código de empresa
     */
    private String codigoEmpresa;
    
    /**
     * RUC de empresa
     */
    private String rucEmpresa;
    
    /**
     * Empresa
     */
    private Empresa empresa;

    /**
     * Activo
     */
    private Character activo;

    /**
     * Jasper
     */
    private String jasper;

    /**
     * Identificador de reporte
     */
    private Integer idReporte;

    /**
     * Código de reporte
     */
    private String codigoReporte;

    /**
     * Descripción del reporte
     */
    private String descripcionReporte;

    /**
     * Reporte
     */
    private Reporte reporte;
    
    /**
     * parametros
     */
    private JsonElement parametros;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getRucEmpresa() {
        return rucEmpresa;
    }

    public void setRucEmpresa(String rucEmpresa) {
        this.rucEmpresa = rucEmpresa;
    }

    public void setDescripcionReporte(String descripcionReporte) {
        this.descripcionReporte = descripcionReporte;
    }

    public String getDescripcionReporte() {
        return descripcionReporte;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public String getJasper() {
        return jasper;
    }

    public void setJasper(String jasper) {
        this.jasper = jasper;
    }

    public Integer getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public String getCodigoReporte() {
        return codigoReporte;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    public Reporte getReporte() {
        return reporte;
    }

    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setParametros(JsonElement parametros) {
        this.parametros = parametros;
    }

    public JsonElement getParametros() {
        return parametros;
    }
}
