package py.com.sepsa.erp.web.v1.remote;

/**
 * APIErpFacturacion para conexiones al servicio.
 * @author Romina Núñez
 */
public class APISepsaSiediMonitor extends API {

    //Parámetros de conexiones.
    private final static String BASE_API = "SiediMonitor/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;

    /**
     * Constructor de API
     */
    public APISepsaSiediMonitor() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);
        
        InfoPojo info = InfoPojo.createInstanceSiediMonitor("HOST_SIEDI_JSON");
        
        updateConnInfo(info);
    }
}
