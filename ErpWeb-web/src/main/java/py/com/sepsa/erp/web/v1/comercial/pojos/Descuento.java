package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * POJO de Descuento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Descuento {

    /**
     * Identificador del descuento
     */
    private Integer id;
    /**
     * Identificador del tipo de descuento
     */
    private Integer idTipoDescuento;
    /**
     * Id Cliente
     */
    private Integer idCliente;
    /**
     * Id Servicio
     */
    private Integer idServicio;
    /**
     * Id contrato
     */
    private Integer idContrato;
    /**
     * Integer id Estado
     */
    private Integer idEstado;
    /**
     * ID aprobacion
     */
    private Integer idAprobacion;
    /**
     * Fecha inserción
     */
    private Date fechaInsercion;
    /**
     * Fecha Desde del Descuento
     */
    private Date fechaDesde;
    /**
     * Fecha Desde del Descuento
     */
    private Date fechaDesdeDesde;
    /**
     * Fecha Desde del Descuento
     */
    private Date fechaDesdeHasta;
    /**
     * Fecha Hasta del Descuento
     */
    private Date fechaHasta;
    /**
     * Fecha Hasta del Descuento
     */
    private Date fechaHastaDesde;
    /**
     * Fecha Hasta del Descuento
     */
    private Date fechaHastaHasta;
    /**
     * Valor del Descuento
     */
    private BigDecimal valorDescuento;
    /**
     * Excedente
     */
    private String excedente;
    /**
     * Porcentual
     */
    private String porcentual;
    /**
     * Estado del descuento
     */
    private Estado estado;
    /**
     * Codigo tipo descuento
     */
    private TipoDescuento tipoDescuento;
    /**
     * aprobacion
     */
    private Aprobacion aprobacion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoDescuento() {
        return idTipoDescuento;
    }

    public void setIdTipoDescuento(Integer idTipoDescuento) {
        this.idTipoDescuento = idTipoDescuento;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public BigDecimal getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(BigDecimal valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public Integer getIdAprobacion() {
        return idAprobacion;
    }

    public void setIdAprobacion(Integer idAprobacion) {
        this.idAprobacion = idAprobacion;
    }

    public String getExcedente() {
        return excedente;
    }

    public void setExcedente(String excedente) {
        this.excedente = excedente;
    }

    public Date getFechaDesdeDesde() {
        return fechaDesdeDesde;
    }

    public void setFechaDesdeDesde(Date fechaDesdeDesde) {
        this.fechaDesdeDesde = fechaDesdeDesde;
    }

    public Date getFechaDesdeHasta() {
        return fechaDesdeHasta;
    }

    public void setFechaDesdeHasta(Date fechaDesdeHasta) {
        this.fechaDesdeHasta = fechaDesdeHasta;
    }

    public Date getFechaHastaDesde() {
        return fechaHastaDesde;
    }

    public void setFechaHastaDesde(Date fechaHastaDesde) {
        this.fechaHastaDesde = fechaHastaDesde;
    }

    public Date getFechaHastaHasta() {
        return fechaHastaHasta;
    }

    public void setFechaHastaHasta(Date fechaHastaHasta) {
        this.fechaHastaHasta = fechaHastaHasta;
    }

    public String getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(String porcentual) {
        this.porcentual = porcentual;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public TipoDescuento getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(TipoDescuento tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public Aprobacion getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(Aprobacion aprobacion) {
        this.aprobacion = aprobacion;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

//</editor-fold>
    /**
     * Contructor de la clase
     */
    public Descuento() {

    }
}
