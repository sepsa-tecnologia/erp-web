/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para reporte venta
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ReporteVentaFilter extends Filter {

    /**
     *
     * Agrega el filtro de identificador local talonario
     *
     *
     *
     * @param idLocalTalonario
     *
     * @return
     *
     */
    public ReporteVentaFilter idLocalTalonario(Integer idLocalTalonario) {
        
        if (idLocalTalonario != null) {
            
            params.put("idLocalTalonario", idLocalTalonario);
            
        }
        
        return this;
        
    }
    
    public ReporteVentaFilter idCliente(Integer idCliente) {
        
        if (idCliente != null) {
            params.put("idCliente", idCliente); 
        }
        return this;  
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public ReporteVentaFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public ReporteVentaFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public ReporteVentaFilter montoAnuladasCero(Boolean montoAnuladasCero) {
        if (montoAnuladasCero != null) {
            params.put("montoAnuladasCero", montoAnuladasCero);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public ReporteVentaFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     *
     * Construye el mapa de parámetros
     *
     *
     *
     * @param venta
     *
     * @return
     *
     */
    public static Map build(ReporteParam venta) {
        
        ReporteVentaFilter filter = new ReporteVentaFilter();
        
        filter
                .anulado(venta.getAnulado())
                .idCliente(venta.getIdCliente())
                .fechaDesde(venta.getFechaDesde())
                .fechaHasta(venta.getFechaHasta())
                .montoAnuladasCero(venta.getMontoAnuladasCero())
                .idLocalTalonario(venta.getIdLocalTalonario());
        
        return filter.getParams();
        
    }

    /**
     *
     * Constructor de la clase
     *
     */
    public ReporteVentaFilter() {
        
        super();
        
    }
    
}
