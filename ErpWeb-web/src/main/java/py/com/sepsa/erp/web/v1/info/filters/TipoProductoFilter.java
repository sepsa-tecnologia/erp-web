/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProducto;

/**
 * filter para tipo producto
 *
 * @author Williams Vera
 */
public class TipoProductoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoProductoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public TipoProductoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoProductoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoProducto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoProducto tipoProducto, Integer page, Integer pageSize) {
        TipoProductoFilter filter = new TipoProductoFilter();

        filter
                .id(tipoProducto.getId())
                .descripcion(tipoProducto.getDescripcion())
                .codigo(tipoProducto.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
}
