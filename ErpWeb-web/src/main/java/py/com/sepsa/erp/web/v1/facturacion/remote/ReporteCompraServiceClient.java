package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteCompraParam;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para reporte de compra
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class ReporteCompraServiceClient extends APIErpFacturacion {

    /**
     * Método para obtener reporte de compra
     *
     * @param compra
     * @return
     */
    public byte[] getReporteCompra(ReporteParam compra) {

        byte[] result = null;

        Date inicio = compra.getFechaDesde();
        Date fin = compra.getFechaHasta();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String inicio_ = format.format(inicio);
        String fin_ = format.format(fin);
        String service
                = String.format(Resource.REPORTE_COMPRA.url, inicio_, fin_);

        HttpURLConnection conn = GET(service, ContentType.JSON, null);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Método para obtener reporte de venta
     *
     * @param venta
     * @return
     */
    public byte[] getReporteComprobanteFacturaCompra(ReporteComprobanteCompraParam venta) {

        byte[] result = null;
        Map params = new HashMap<>();

        Date fecha = venta.getFecha();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String fecha_ = format.format(fecha);
        String anual_ = String.valueOf(venta.getAnual());
        String service
                = String.format(Resource.REPORTE_COMPROBANTE_FACTURA_COMPRA.url, fecha_, anual_);

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Método para obtener reporte de venta
     *
     * @param venta
     * @return
     */
    public byte[] getReporteComprobanteNotaCreditoCompra(ReporteComprobanteCompraParam venta) {

        byte[] result = null;
        Map params = new HashMap<>();

        Date fecha = venta.getFecha();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String fecha_ = format.format(fecha);
        String anual_ = String.valueOf(venta.getAnual());
        String service
                = String.format(Resource.REPORTE_COMPROBANTE_NOTA_CREDITO_COMPRA.url, fecha_, anual_);

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Recursos de conexión o servicios del APISepsaSet
     */
    public enum Resource {

        //Servicios
        REPORTE_COMPRA("Reporte de compra", "factura-compra/reporte-compra/%s/%s"),
        REPORTE_COMPROBANTE_FACTURA_COMPRA("Reporte de compra", "factura/comprobante-compra/%s/%s"),
        REPORTE_COMPROBANTE_NOTA_CREDITO_COMPRA("Reporte de Nota de credito compra", "nota-credito-compra/comprobante-compra/%s/%s");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
