package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;

/**
 * Filtro utilizado para el servicio de configuracion valor
 *
 * @author Cristina Insfrán
 */
public class ConfiguracionValorFilter extends Filter {

    /**
     * Agrega el filtro de identificador de la configuración
     *
     * @param id Identificador de la configuración
     * @return
     */
    public ConfiguracionValorFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la configuración
     *
     * @param idConfiguracion Identificador de la configuración
     * @return
     */
    public ConfiguracionValorFilter idConfiguracion(Integer idConfiguracion) {
        if (idConfiguracion != null) {
            params.put("idConfiguracion", idConfiguracion);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la empresa
     *
     * @param idEmpresa Identificador de la empresa
     * @return
     */
    public ConfiguracionValorFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agregar el filtro de activo
     *
     * @param activo
     * @return
     */
    public ConfiguracionValorFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agregar el filtro de codigoConfiguracion
     *
     * @param codigoConfiguracion
     * @return
     */
    public ConfiguracionValorFilter codigoConfiguracion(String codigoConfiguracion) {
        if (codigoConfiguracion != null && !codigoConfiguracion.trim().isEmpty()) {
            params.put("codigoConfiguracion", codigoConfiguracion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param conf datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ConfiguracionValor conf, Integer page, Integer pageSize) {
        ConfiguracionValorFilter filter = new ConfiguracionValorFilter();

        filter
                .id(conf.getId())
                .idConfiguracion(conf.getIdConfiguracion())
                .idEmpresa(conf.getIdEmpresa())
                .codigoConfiguracion(conf.getCodigoConfiguracion())
                .activo(conf.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ConfiguracionValorFilter
     */
    public ConfiguracionValorFilter() {
        super();
    }
}
