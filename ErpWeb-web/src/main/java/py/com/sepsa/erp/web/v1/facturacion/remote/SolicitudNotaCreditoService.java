package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionSncAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.SolicitudNotaCreditoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.MotivoSolicitudFilter;
import py.com.sepsa.erp.web.v1.facturacion.filters.SolicitudNotaCreditoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionSnc;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Solicitud Nota de Credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class SolicitudNotaCreditoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de nota de credito
     *
     * @param solicitudNotaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public SolicitudNotaCreditoListAdapter getSolicitudNotaCreditoList(SolicitudNotaCredito solicitudNotaCredito, Integer page,
            Integer pageSize) {

        SolicitudNotaCreditoListAdapter lista = new SolicitudNotaCreditoListAdapter();

        Map params = SolicitudNotaCreditoFilter.build(solicitudNotaCredito, page, pageSize);

        HttpURLConnection conn = GET(Resource.SOLICITUD_NOTA_CREDITO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    SolicitudNotaCreditoListAdapter.class);

            lista = (SolicitudNotaCreditoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Obtiene detalles de la solicitnota de crédito
     *
     * @param notaCredito Objeto
     * @return
     */
    public SolicitudNotaCredito getNotaCreditoSolicitudDetalle(SolicitudNotaCredito notaCredito) {

        SolicitudNotaCredito nc = new SolicitudNotaCredito();

        Map params = new HashMap<>();

        String url = "solicitud-nota-credito/id/" + notaCredito.getId();

        HttpURLConnection conn = GET(url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    SolicitudNotaCredito.class);

            nc = (SolicitudNotaCredito) response.getPayload();

            conn.disconnect();
        }
        return nc;
    }
    
     /**
     * Método para crear Nota de crédito
     *
     * @param notaCredito
     * @return
     */
    public BodyResponse<SolicitudNotaCredito> createNotaCredito(SolicitudNotaCredito notaCredito) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.SOLICITUD_NOTA_CREDITO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaCredito));

            response = BodyResponse.createInstance(conn, SolicitudNotaCredito.class);

        }
        return response;
    }
    
    /**
     * Obtiene la lista de motivo de solicitud snc
     *
     * @param motivo
     * @param page
     * @param pageSize
     * @return
     */
    public MotivoEmisionSncAdapter getMotivoEmisionSncList(MotivoEmisionSnc motivo, Integer page,
            Integer pageSize) {

        MotivoEmisionSncAdapter lista = new MotivoEmisionSncAdapter();

        Map params = MotivoSolicitudFilter.build(motivo, page, pageSize);

        HttpURLConnection conn = GET(Resource.MOTIVO_EMISION_SNC.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MotivoEmisionSncAdapter.class);

            lista = (MotivoEmisionSncAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    

    /**
     * Constructor
     */
    public SolicitudNotaCreditoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        SOLICITUD_NOTA_CREDITO("Nota Credito Compra", "solicitud-nota-credito"),
        MOTIVO_EMISION_SNC("Motivo Emisión Snc","motivo-emision-snc");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
