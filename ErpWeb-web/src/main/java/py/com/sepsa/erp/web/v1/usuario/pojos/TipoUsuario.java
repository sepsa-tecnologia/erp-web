
package py.com.sepsa.erp.web.v1.usuario.pojos;

import java.util.ArrayList;
import java.util.List;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;

/**
 * POJO de tipo de usuario
 * @author Cristina Insfrán
 */
public class TipoUsuario {

    /**
     * Identificador del tipo de usuario
     */
    private Integer id;
    /**
     * Descripción del producto
     */
    private String producto;
    /**
     * Perfiles para los productos
     */
    private List<Perfil> perfiles=new ArrayList<>();
    /**
     * Perfil seleccionado
     */
    private String perfilSel;
    
    
    /**
     * @return the perfilSel
     */
    public String getPerfilSel() {
        return perfilSel;
    }

    /**
     * @param perfilSel the perfilSel to set
     */
    public void setPerfilSel(String perfilSel) {
        this.perfilSel = perfilSel;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the perfiles
     */
    public List<Perfil> getPerfiles() {
        return perfiles;
    }

    /**
     * @param perfiles the perfiles to set
     */
    public void setPerfiles(List<Perfil> perfiles) {
        this.perfiles = perfiles;
    }
    
    public TipoUsuario(Integer id, String producto, List<Perfil> perfils){
        this.id = id;
        this.producto = producto;
        this.perfiles = perfils;
    }
    
}
