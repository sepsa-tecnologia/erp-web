package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO de ServicioContrato
 *
 * @author Romina E. Núñez Rojas
 */
public class ContratoServicio {

    /**
     * Identificador del contrato
     */
    private Integer idContrato;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Identificador del monto mínimo
     */
    private Integer idMontoMinimo;
    /**
     * Producto
     */
    private String producto;
    /**
     * Identificador del servicio
     */
    private Integer idServicio;
    /**
     * Servicio
     */
    private String servicio;
    /**
     * Estado del contrato
     */
    private String estado;
    /**
     * Monto Acuerdo
     */
    private BigDecimal montoAcuerdo;
    /**
     * Fecha Vto Acuerdo
     */
    private Date fechaVencimientoAcuerdo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getIdContrato() {
        return idContrato;
    }
    public BigDecimal getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(BigDecimal montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public Date getFechaVencimientoAcuerdo() {
        return fechaVencimientoAcuerdo;
    }

    public void setFechaVencimientoAcuerdo(Date fechaVencimientoAcuerdo) {
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }


    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
//</editor-fold>

    /**
     * Contructor de la clase
     */
    public ContratoServicio() {

    }

    
    /**
     * Constructor con parámetros
     * @param idContrato
     * @param idProducto
     * @param idMontoMinimo
     * @param producto
     * @param idServicio
     * @param servicio
     * @param estado
     * @param montoAcuerdo
     * @param fechaVencimientoAcuerdo 
     */
    public ContratoServicio(Integer idContrato, Integer idProducto, Integer idMontoMinimo, String producto, Integer idServicio, String servicio, String estado, BigDecimal montoAcuerdo, Date fechaVencimientoAcuerdo) {
        this.idContrato = idContrato;
        this.idProducto = idProducto;
        this.idMontoMinimo = idMontoMinimo;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.estado = estado;
        this.montoAcuerdo = montoAcuerdo;
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }

    

    

}
