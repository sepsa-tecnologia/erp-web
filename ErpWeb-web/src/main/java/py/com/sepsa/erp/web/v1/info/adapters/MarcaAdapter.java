/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.remote.MarcaService;

/**
 * Adapter para marca
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MarcaAdapter extends DataListAdapter<Marca> {

    /**
     * Cliente para el servicio marca
     */
    private final MarcaService marcaService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MarcaAdapter fillData(Marca searchData) {

        return marcaService.getMarcaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de MarcaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MarcaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.marcaService = new MarcaService();
    }

    /**
     * Constructor de MarcaAdapter
     */
    public MarcaAdapter() {
        super();
        this.marcaService = new MarcaService();
    }

}
