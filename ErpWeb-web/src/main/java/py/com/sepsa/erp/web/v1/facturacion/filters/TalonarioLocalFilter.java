/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioLocal;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para talonario local relacionado
 *
 * @author Romina Núñez
 */
public class TalonarioLocalFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TalonarioLocalFilter idTalonario(Integer idTalonario) {
        if (idTalonario != null) {
            params.put("idTalonario", idTalonario);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param idLocal
     * @return
     */
    public TalonarioLocalFilter idLocal(Integer idLocal) {
        if (idLocal != null) {
            params.put("idLocal", idLocal);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param talonario datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TalonarioLocal talonarioLocal, Integer page, Integer pageSize) {
        TalonarioLocalFilter filter = new TalonarioLocalFilter();

        filter
                .idTalonario(talonarioLocal.getIdTalonario())
                .idLocal(talonarioLocal.getIdLocal())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TalonarioLocalFilter() {
        super();
    }

}
