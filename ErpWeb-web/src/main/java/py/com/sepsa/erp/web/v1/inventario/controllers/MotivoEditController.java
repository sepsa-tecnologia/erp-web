/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.MotivoService;

/**
 * Controlador para Editar Motivo
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEdit")
public class MotivoEditController implements Serializable {

    /**
     * Cliente para el servicio de motivo.
     */
    private final MotivoService service;
    /**
     * Datos del motivo
     */
    private Motivo motivo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    //</editor-fold>
    /**
     * Método para editar Motivo
     */
    public void edit() {
        BodyResponse<Motivo> respuestaTal = service.editMotivo(motivo);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Motivo editado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.motivo = service.get(id);
    }

    /**
     * Constructor
     */
    public MotivoEditController() {
        this.service = new MotivoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
