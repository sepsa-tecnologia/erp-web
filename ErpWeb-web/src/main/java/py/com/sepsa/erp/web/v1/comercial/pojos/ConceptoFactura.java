package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.math.BigDecimal;

/**
 * POJO de ConceptoFactura
 *
 * @author Romina E. Núñez Rojas
 */
public class ConceptoFactura {
    /**
     * Identidicador del concepto factura
     */
    private Integer id;
    /**
     * Descripción del concepto factura
     */
    private String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    

    /**
     * Contructor de la clase
     */
    public ConceptoFactura() {

    }

    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion 
     */
    public ConceptoFactura(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    

}
