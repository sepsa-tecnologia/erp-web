/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO de metrica
 *
 * @author Williams Ver
 */
public class Metrica {
    /**
     * Identificador de metrica
     */
    private Integer id;
    /**
     * Descripción de métrica
     */
    private String descripcion;
    /**
     * Codigo de métrica
     */
    private String codigo;
    /**
     * Activo
     */
    private String activo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Metrica() {

    }

    /**
     * Constructor de la clase
     */
    public Metrica(int id) {
        this.id = id;
    }
}
