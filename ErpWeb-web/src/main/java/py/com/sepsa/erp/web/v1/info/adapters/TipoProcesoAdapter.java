/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;
import py.com.sepsa.erp.web.v1.info.remote.TipoProcesoService;

/**
 * Adapter para tipo tipoProceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoProcesoAdapter extends DataListAdapter<TipoProceso> {

    /**
     * Cliente para el servicio tipo proceso
     */
    private final TipoProcesoService tipoProcesoService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoProcesoAdapter fillData(TipoProceso searchData) {

        return tipoProcesoService.getTipoProcesoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoProcesoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoProcesoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoProcesoService = new TipoProcesoService();
    }

    /**
     * Constructor de TipoProcesoAdapter
     */
    public TipoProcesoAdapter() {
        super();
        this.tipoProcesoService = new TipoProcesoService();
    }
}
