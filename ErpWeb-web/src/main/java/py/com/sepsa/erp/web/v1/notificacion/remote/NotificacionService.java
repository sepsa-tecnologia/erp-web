/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.notificacion.adapters.NotificacionAdapter;
import py.com.sepsa.erp.web.v1.notificacion.filters.NotificacionFilter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.remote.APIErpNotificacion;

/**
 * Cliente para el servicio Notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotificacionService extends APIErpNotificacion {

    /**
     * Obtiene la lista de Notificaciones.
     *
     * @param notificacion
     * @param page
     * @param pageSize
     * @return
     */
    public NotificacionAdapter getNotificacionList(Notificacion notificacion, Integer page,
            Integer pageSize) {

        NotificacionAdapter lista = new NotificacionAdapter();

        Map params = NotificacionFilter.build(notificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotificacionAdapter.class);

            lista = (NotificacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param notificacion 
     * @return
     */
    public BodyResponse<Notificacion> createNotificacion(Notificacion notificacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CREAR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notificacion));
            response = BodyResponse.createInstance(conn, Notificacion.class);
        }
        return response;
    }

    /**
     * Constructor de NotificacionService
     */
    public NotificacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        LISTAR("Listado de Notificaciones", "v1/notification"),
        CREAR("Crear Notificacion", "v1/notification");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
