package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoFilter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaContactoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de contacto
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class ContactoServiceClient extends APIErpCore {

    /**
     * Método para setear ContactoTelefono
     *
     * @param contacto
     * @return
     */
    public ContactoEmail setContactoEmail(ContactoEmail contacto) {

        ContactoEmail ct = new ContactoEmail();

        HttpURLConnection conn = POST(Resource.CONTACTO_EMAIL.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(contacto));

            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoEmail.class);

            ct = (ContactoEmail) response.getPayload();

            conn.disconnect();
        }
        return ct;
    }

    /**
     * Método para setear contacto telefono
     *
     * @param contacto
     * @return
     */
    public ContactoTelefono setContactoTelefono(ContactoTelefono contacto) {

        ContactoTelefono ct = new ContactoTelefono();

        HttpURLConnection conn = POST(Resource.CONTACTO_TELEFONO.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(contacto));

            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoTelefono.class);

            ct = (ContactoTelefono) response.getPayload();

            conn.disconnect();
        }
        return ct;
    }

   /**
     * Método para crear
     *
     * @param contacto
     * @return
     */
    public BodyResponse<Contacto> setContacto(Contacto contacto) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CONTACTO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(contacto));

            response = BodyResponse.createInstance(conn, Contacto.class);


        }
        return response;
    }

    /**
     * Obtiene la lista de contacto
     *
     * @param contacto
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoListAdapter getContactoList(Contacto contacto, Integer page,
            Integer pageSize) {

        ContactoListAdapter lista = new ContactoListAdapter();

        Map params = ContactoFilter.build(contacto, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTACTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoListAdapter.class);

            lista = (ContactoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Obtiene la lista de persona contacto
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaContactoListAdapter getPersonaContactoList(PersonaContacto dato, Integer page,
            Integer pageSize) {

        PersonaContactoListAdapter lista = new PersonaContactoListAdapter();

        Map params = PersonaContactoFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_CONTACTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaContactoListAdapter.class);

            lista = (PersonaContactoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para editar contacto
     *
     * @param contacto
     * @return
     */
    public BodyResponse editContacto(Contacto contacto) {

       
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CONTACTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(contacto));

            response = BodyResponse.createInstance(conn, Contacto.class);

          

        }
        return response;
    }

    /**
     * Agrega o edita un contacto
     *
     * @param personaContacto
     * @return
     */
    public PersonaContacto createPersonaContacto(PersonaContacto personaContacto) {

        PersonaContacto personaContactoCreate = new PersonaContacto();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERSONA_CONTACTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaContacto));

            response = BodyResponse.createInstance(conn, PersonaContacto.class);

            personaContactoCreate = ((PersonaContacto) response.getPayload());

        }
        return personaContactoCreate;
    }

    /**
     * Constructor de ContactoServiceClient
     */
    public ContactoServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CONTACTO("Servicio Contacto", "contacto"),
        CONTACTO_TELEFONO("Servicio Contacto Telefono", "contacto-telefono"),
        CONTACTO_EMAIL("Servicio Contacto Email", "contacto-email"),
        PERSONA_CONTACTO("Servicio persona contacto", "persona-contacto");
       
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
