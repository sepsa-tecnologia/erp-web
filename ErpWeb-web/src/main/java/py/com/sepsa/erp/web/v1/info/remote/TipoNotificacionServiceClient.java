package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.TipoNotificacionAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoEmailFilter;
import py.com.sepsa.erp.web.v1.info.filters.TipoNotificacionFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.TipoNotificacion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de tipo notificación
 *
 * @author Romina Núñez
 */
public class TipoNotificacionServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de tipo notificacion
     *
     * @param tipoNotificacion 
     * @param page
     * @param pageSize
     * @return
     */
    public TipoNotificacionAdapter getTipoNotificacionList(TipoNotificacion tipoNotificacion, Integer page,
            Integer pageSize) {

        TipoNotificacionAdapter lista = new TipoNotificacionAdapter();

        Map params = TipoNotificacionFilter.build(tipoNotificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_NOTIFICACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoNotificacionAdapter.class);

            lista = (TipoNotificacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }


    /**
     * Constructor de TipoNotificacionServiceClient
     */
    public TipoNotificacionServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_NOTIFICACION("Servicio Tipo Notificacion", "tipo-notificacion");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
