package py.com.sepsa.erp.web.v1.proceso.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ProcesamientoArchivo;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para el listado de procesamiento de archivo
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class ProcesamientoArchivoFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public ProcesamientoArchivoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de tipo documento
     *
     * @param idTipoDocumento
     * @return
     */
    public ProcesamientoArchivoFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }
    
    /**
     * Agrega el filtro por identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public ProcesamientoArchivoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de local origen
     *
     * @param idLocalOrigen
     * @return
     */
    public ProcesamientoArchivoFilter idLocalOrigen(Integer idLocalOrigen) {
        if (idLocalOrigen != null) {
            params.put("idLocalOrigen", idLocalOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de local destino
     *
     * @param idLocalDestino
     * @return
     */
    public ProcesamientoArchivoFilter idLocalDestino(Integer idLocalDestino) {
        if (idLocalDestino != null) {
            params.put("idLocalDestino", idLocalDestino);
        }
        return this;
    }

    /**
     * Agrega el filtro por nombreArchivo
     *
     * @param nombreArchivo
     * @return
     */
    public ProcesamientoArchivoFilter nombreArchivo(String nombreArchivo) {
        if (nombreArchivo != null && !nombreArchivo.trim().isEmpty()) {
            params.put("nombreArchivo", nombreArchivo);
        }
        return this;
    }

    /**
     * Agrega el filtro por tamaño
     *
     * @param tamano
     * @return
     */
    public ProcesamientoArchivoFilter tamano(Integer tamano) {
        if (tamano != null) {
            params.put("tamano", tamano);
        }
        return this;
    }

    /**
     * Agrega el filtro por fechaInsercion
     *
     * @param fechaInsercion
     * @return
     */
    public ProcesamientoArchivoFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaDesde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public ProcesamientoArchivoFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);

                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaHasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public ProcesamientoArchivoFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por hash
     *
     * @param hash
     * @return
     */
    public ProcesamientoArchivoFilter hash(String hash) {
        if (hash != null && !hash.trim().isEmpty()) {
            params.put("hash", hash);
        }
        return this;
    }

    /**
     * Agrega el filtro por confirmado
     *
     * @param confirmado
     * @return
     */
    public ProcesamientoArchivoFilter confirmado(String confirmado) {
        if (confirmado != null && !confirmado.trim().isEmpty()) {
            params.put("confirmado", confirmado);
        }
        return this;
    }

    /**
     * Agrega el filtro por reenviado
     *
     * @param reenviado
     * @return
     */
    public ProcesamientoArchivoFilter reenviado(String reenviado) {
        if (reenviado != null && !reenviado.trim().isEmpty()) {
            params.put("reenviado", reenviado);
        }
        return this;
    }

    /**
     * Agrega el filtro de id tipo Archivo
     *
     * @param idTipoArchivo
     * @return
     */
    public ProcesamientoArchivoFilter idTipoArchivo(Integer idTipoArchivo) {
        if (idTipoArchivo != null) {
            params.put("idTipoArchivo", idTipoArchivo);
        }
        return this;
    }

    /**
     * Agrega el filtro Numero de Documento
     *
     * @param nroDocumento
     * @return
     */
    public ProcesamientoArchivoFilter nroDocumento(String nroDocumento) {
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            params.put("nroDocumento", nroDocumento);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param procesamiento datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ProcesamientoArchivo procesamiento, Integer page,
            Integer pageSize) {

        ProcesamientoArchivoFilter filter = new ProcesamientoArchivoFilter();

        filter
                .id(procesamiento.getId())
                .idEmpresa(procesamiento.getIdEmpresa())
                .nombreArchivo(procesamiento.getNombreArchivo())
                .fechaInsercion(procesamiento.getFechaInsercion())
                .fechaInsercionDesde(procesamiento.getFechaInsercionDesde())
                .fechaInsercionHasta(procesamiento.getFechaInsercionHasta())
                .hash(procesamiento.getHash())
                .confirmado(procesamiento.getConfirmado())
                .reenviado(procesamiento.getReenviado())
                .idTipoArchivo(procesamiento.getIdTipoArchivo())
                .idTipoDocumento(procesamiento.getIdTipoDocumento())
                .idLocalOrigen(procesamiento.getIdLocalOrigen())
                .idLocalDestino(procesamiento.getIdLocalDestino())
                .nroDocumento(procesamiento.getNroDocumento())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public ProcesamientoArchivoFilter() {
        super();
    }

}
