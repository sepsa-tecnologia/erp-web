/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.ControlInventarioService;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("listarControlInventario")
public class ControlInventarioListController implements Serializable {

    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoAdapterList;

    /**
     * POJO de MotivoEmisionInterno
     */
    private Motivo motivoFilter;
    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioAdapter adapterControlInventario;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioFilter;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterEncargado;
    /**
     * POJO Encargado
     */
    private Usuario encargado;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterSupervisor;
    /**
     * POJO Encargado
     */
    private Usuario supervisor;
    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioDetalleAdapter adapterControlInventarioDetalle;
    /**
     * Pojo Control Inventario
     */
    private ControlInventarioDetalle controlInventarioDetalleFilter;
    private ControlInventarioService serviceControlInventario;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoAdapter getMotivoAdapterList() {
        return motivoAdapterList;
    }

    public void setServiceControlInventario(ControlInventarioService serviceControlInventario) {
        this.serviceControlInventario = serviceControlInventario;
    }

    public ControlInventarioService getServiceControlInventario() {
        return serviceControlInventario;
    }

    public void setControlInventarioDetalleFilter(ControlInventarioDetalle controlInventarioDetalleFilter) {
        this.controlInventarioDetalleFilter = controlInventarioDetalleFilter;
    }

    public void setAdapterControlInventarioDetalle(ControlInventarioDetalleAdapter adapterControlInventarioDetalle) {
        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle;
    }

    public ControlInventarioDetalle getControlInventarioDetalleFilter() {
        return controlInventarioDetalleFilter;
    }

    public ControlInventarioDetalleAdapter getAdapterControlInventarioDetalle() {
        return adapterControlInventarioDetalle;
    }

    public void setSupervisor(Usuario supervisor) {
        this.supervisor = supervisor;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public void setAdapterSupervisor(UserListAdapter adapterSupervisor) {
        this.adapterSupervisor = adapterSupervisor;
    }

    public void setAdapterEncargado(UserListAdapter adapterEncargado) {
        this.adapterEncargado = adapterEncargado;
    }

    public Usuario getSupervisor() {
        return supervisor;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public UserListAdapter getAdapterSupervisor() {
        return adapterSupervisor;
    }

    public UserListAdapter getAdapterEncargado() {
        return adapterEncargado;
    }

    public void setControlInventarioFilter(ControlInventario controlInventarioFilter) {
        this.controlInventarioFilter = controlInventarioFilter;
    }

    public void setAdapterControlInventario(ControlInventarioAdapter adapterControlInventario) {
        this.adapterControlInventario = adapterControlInventario;
    }

    public ControlInventario getControlInventarioFilter() {
        return controlInventarioFilter;
    }

    public ControlInventarioAdapter getAdapterControlInventario() {
        return adapterControlInventario;
    }

    public void setMotivoAdapterList(MotivoAdapter motivoAdapterList) {
        this.motivoAdapterList = motivoAdapterList;
    }

    public Motivo getMotivoFilter() {
        return motivoFilter;
    }

    public void setMotivoFilter(Motivo motivoFilter) {
        this.motivoFilter = motivoFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener motivoEmisions
     */
    public void getMotivos() {
        getMotivoAdapterList().setFirstResult(0);
        motivoFilter.setCodigoTipoMotivo("CONTROL_INVENTARIO");
        this.motivoAdapterList = this.motivoAdapterList.fillData(motivoFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.controlInventarioFilter = new ControlInventario();
        filterControlInventario();
    }

    public void filterControlInventario() {
        getAdapterControlInventario().setFirstResult(0);
        this.controlInventarioFilter.setListadoPojo(true);

        this.adapterControlInventario = adapterControlInventario.fillData(controlInventarioFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioEncargado(String query) {

        Usuario encargado = new Usuario();
        encargado.setTienePersonaFisica(true);
        encargado.setNombrePersonaFisica(query);

        adapterEncargado = adapterEncargado.fillData(encargado);

        return adapterEncargado.getData();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioSupervisor(String query) {

        Usuario supervisor = new Usuario();
        supervisor.setTienePersonaFisica(true);
        supervisor.setNombrePersonaFisica(query);

        adapterSupervisor = adapterSupervisor.fillData(supervisor);

        return adapterSupervisor.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectEncargadoFilter(SelectEvent event) {
        encargado.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioFilter.setIdUsuarioEncargado(encargado.getId());
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectSupervisorFilter(SelectEvent event) {
        supervisor.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioFilter.setIdUsuarioSupervisor(supervisor.getId());
    }

    /**
     * Método para filtrar los detalles de control de inventario
     *
     * @param idControl
     */
    public void filterControlInventarioDetalle(Integer idControl) {
        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();

        this.controlInventarioDetalleFilter.setListadoPojo(Boolean.TRUE);
        this.controlInventarioDetalleFilter.setIdControlInventario(idControl);

        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle.fillData(controlInventarioDetalleFilter);
    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download(Integer idControlIventario, String codigo) {
        String contentType = ("application/pdf");
        String fileName = "CONTROL_INVENTARIO_" + codigo + ".pdf";
        String url = "control-inventario/consulta/" + idControlIventario;
        byte[] data = serviceControlInventario.download(url);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    /**
     * Método para imprimir control de inventario
     *
     * @param idControlIventario
     */
    public void print(Integer idControlIventario) {
        String url = "control-inventario/consulta/" + idControlIventario;
        byte[] data = serviceControlInventario.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivoFilter = new Motivo();
        this.motivoAdapterList = new MotivoAdapter();

        this.adapterControlInventario = new ControlInventarioAdapter();
        this.controlInventarioFilter = new ControlInventario();

        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();

        this.adapterEncargado = new UserListAdapter();
        this.adapterSupervisor = new UserListAdapter();
        this.encargado = new Usuario();
        this.supervisor = new Usuario();
        this.serviceControlInventario = new ControlInventarioService();

        filterControlInventario();
        getMotivos();
    }

    /**
     * Constructor
     */
    public ControlInventarioListController() {
        init();
    }

}
