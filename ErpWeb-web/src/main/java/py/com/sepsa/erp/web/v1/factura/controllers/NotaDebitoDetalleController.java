package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Detalles ND
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("notaDebitoDetalle")
public class NotaDebitoDetalleController implements Serializable {

    /**
     * Servicio Nota de Débito
     */
    private NotaDebitoService serviceNotaDebito;
    /**
     * POJO Nota de Débito
     */
    private NotaDebitoPojo notaDebitoDetalle;
    /**
     * Dato de Nota de Débito
     */
    private String idNotaDebito;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private FileServiceClient fileServiceClient;
    private NotaDebitoDetalleListPojoAdapter adapterDetalle;
    private NotaDebitoDetallePojo detallePojoFilter;
    private Map parametros;

    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    private Boolean siediApiKude = false;

    @Inject
    private SessionData session;
 //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">  
    public NotaDebitoService getServiceNotaDebito() {
        return serviceNotaDebito;
    }

    public void setServiceNotaDebito(NotaDebitoService serviceNotaDebito) {
        this.serviceNotaDebito = serviceNotaDebito;
    }

    public NotaDebitoPojo getNotaDebitoDetalle() {
        return notaDebitoDetalle;
    }

    public void setNotaDebitoDetalle(NotaDebitoPojo notaDebitoDetalle) {
        this.notaDebitoDetalle = notaDebitoDetalle;
    }

    public String getIdNotaDebito() {
        return idNotaDebito;
    }

    public void setIdNotaDebito(String idNotaDebito) {
        this.idNotaDebito = idNotaDebito;
    }

    public NotaDebitoDetalleListPojoAdapter getAdapterDetalle() {
        return adapterDetalle;
    }

    public void setAdapterDetalle(NotaDebitoDetalleListPojoAdapter adapterDetalle) {
        this.adapterDetalle = adapterDetalle;
    }

    public NotaDebitoDetallePojo getDetallePojoFilter() {
        return detallePojoFilter;
    }

    public void setDetallePojoFilter(NotaDebitoDetallePojo detallePojoFilter) {    
        this.detallePojoFilter = detallePojoFilter;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }
    

//</editor-fold>
    /**
     * Método para obtener el detalle de la nota de débito
     */
    public void obtenerNotaDebitoDetalle() {
        notaDebitoDetalle.setId(Integer.parseInt(idNotaDebito));
        notaDebitoDetalle.setListadoPojo(Boolean.TRUE);

        notaDebitoDetalle = serviceNotaDebito.getNotaDebitoDetalle(notaDebitoDetalle);

        detallePojoFilter.setListadoPojo(Boolean.TRUE);
        detallePojoFilter.setIdNotaDebito(notaDebitoDetalle.getId());
        adapterDetalle = adapterDetalle.fillData(detallePojoFilter);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String notaDebitoURL() {
        return String.format("nota-debito-list"
                + "?faces-redirect=true");
    }

    /**
     * Método para descargar el pdf de Nota de Débito
     */
    public StreamedContent download(Boolean ticket, Boolean xml) {
        String contentType = ("application/pdf");
        String fileName = notaDebitoDetalle.getNroNotaDebito() + ".pdf";
        String url = "nota-debito/consulta/" + idNotaDebito;
        if (notaDebitoDetalle.getDigital().equalsIgnoreCase("S")) {
            parametros.put("siediApi", siediApiKude);
            if (xml) {
                fileName = notaDebitoDetalle.getNroNotaDebito() + ".xml";
                url = "nota-debito/consulta-xml/" + idNotaDebito;
            } else {
                parametros.put("ticket", ticket);
            }
        }
        byte[] data = fileServiceClient.downloadNotaDebito(url, parametros);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "nota-debito/consulta/" + idNotaDebito;
        if (notaDebitoDetalle.getDigital().equalsIgnoreCase("S")) {
            parametros.put("siediApi", siediApiKude);
            parametros.put("ticket", false);
        }
        byte[] data = fileServiceClient.downloadNotaDebito(url, parametros);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     * Obtiene el valor de configuracion siediApi
     */
    public void obtenerConfigSiediApi() {
        configuracionValorFilter = new ConfiguracionValor();
        adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("ARCHIVOS_DTE_SIEDI_API");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            siediApiKude = false;
        } else {
            siediApiKude = adapterConfigValor.getData().get(0).getValor().equalsIgnoreCase("S");
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idNotaDebito = params.get("id");

        this.serviceNotaDebito = new NotaDebitoService();
        this.notaDebitoDetalle = new NotaDebitoPojo();
        this.fileServiceClient = new FileServiceClient();

        this.adapterDetalle = new NotaDebitoDetalleListPojoAdapter();
        this.detallePojoFilter = new NotaDebitoDetallePojo();
        this.parametros = new HashMap();
        obtenerNotaDebitoDetalle();
        obtenerConfigSiediApi();
    }

}
