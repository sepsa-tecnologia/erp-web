package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("productoCargaMasiva")
public class ProductoCargaMasivaController implements Serializable {

    /**
     * Archivo de instructivo para la carga de productos
     */
    private StreamedContent loadProductoCarga;
    /**
     * Objeto Attach
     */
    private Attach attach;
    /**
     * Producto Service
     */
    private ProductoService serviceProducto;
    /**
     * Archivo
     */
    private UploadedFile file;
    /**
     * Datos para descarga
     */
    private byte[] dataDownload;
    /**
     * Bandera para descargar
     */
    private boolean downloadBtn;

    public UploadedFile getFile() {
        return file;
    }

    public void setDownloadBtn(boolean downloadBtn) {
        this.downloadBtn = downloadBtn;
    }

    public boolean isDownloadBtn() {
        return downloadBtn;
    }

    public void setDataDownload(byte[] dataDownload) {
        this.dataDownload = dataDownload;
    }

    public byte[] getDataDownload() {
        return dataDownload;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public ProductoService getServiceProducto() {
        return serviceProducto;
    }

    public void setServiceProducto(ProductoService serviceProducto) {
        this.serviceProducto = serviceProducto;
    }

    public void setLoadProductoCarga(StreamedContent loadProductoCarga) {
        this.loadProductoCarga = loadProductoCarga;
    }

    /**
     * Método para descargar el archivo plantilla
     *
     * @return
     */
    public StreamedContent getLoadProductoCarga() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/app/producto-info/download/matriz-carga-productos.csv");
        loadProductoCarga = new DefaultStreamedContent(stream, "text/csv", "matriz_carga.csv");
        return loadProductoCarga;
    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     * @throws IOException
     */
    public void upload() throws IOException {
        if (file != null) {
            attach = new Attach(file.getFileName(), file.getInputstream(), file.getContentType());
            byte[] data = serviceProducto.sendFile(attach);
            dataDownload = data;
            if (data != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado el archivo correctamente!"));
                this.downloadBtn = true;

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Archivo sin adjuntar, favor seleccionar archivo!"));
        }

    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     */
    public StreamedContent download() {
        String contentType = file.getContentType();
        String fileName = file.getFileName();

        return new DefaultStreamedContent(new ByteArrayInputStream(dataDownload),
                contentType, fileName);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.attach = new Attach();
        this.serviceProducto = new ProductoService();
        this.downloadBtn = false;

    }
}
