
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.remote.PersonaEmailClient;

/**
 * Adaptador para persona email
 * @author Romina Núñez
 */
public class PersonaEmailAdapter extends DataListAdapter<PersonaEmail> {
    
    /**
     * Cliente para los servicios de persona email
     */
    private final PersonaEmailClient servicePersonaEmail;
   
    
    
    /**
     * Constructor de PersonaEmailAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PersonaEmailAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.servicePersonaEmail = new PersonaEmailClient();
    }

    /**
     * Constructor de PersonaEmailAdapter
     */
    public PersonaEmailAdapter() {
        super();
        this.servicePersonaEmail = new PersonaEmailClient();
    }

    @Override
    public PersonaEmailAdapter fillData(PersonaEmail searchData) {
        return servicePersonaEmail.getPersonaEmail(searchData,getFirstResult(),getPageSize());
    }
}



