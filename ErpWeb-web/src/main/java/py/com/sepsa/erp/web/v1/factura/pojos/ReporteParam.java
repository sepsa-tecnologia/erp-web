package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;

/**
 * POJO oc vs factura
 *
 * @author Cristina Insfrán
 */
public class ReporteParam {

    /**
     * Identificador de persona
     */
    private Integer idCliente;
    /**
     * Identificador de persona
     */
    private Integer idPersona;

    /**
     * Fecha desde
     */
    private Date fechaDesde;

    /**
     * Fecha Hasta
     */
    private Date fechaHasta;
    /**
     * Identificador de local
     */
    private Integer idLocalTalonario;
    /**
     * Parámetro anulado
     */
    private String anulado;
        /**
     * Parámetro anulado
     */
    private Boolean montoAnuladasCero;

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    public void setMontoAnuladasCero(Boolean montoAnuladasCero) {
        this.montoAnuladasCero = montoAnuladasCero;
    }

    public Boolean getMontoAnuladasCero() {
        return montoAnuladasCero;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdLocalTalonario() {
        return idLocalTalonario;
    }

    public void setIdLocalTalonario(Integer idLocalTalonario) {
        this.idLocalTalonario = idLocalTalonario;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Constructor
     */
    public ReporteParam() {

    }
}
