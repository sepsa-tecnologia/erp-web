package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.AutoFacturaService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class AutoFacturaDetallePojoAdapter extends DataListAdapter<AutoFacturaDetallePojo> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final AutoFacturaService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public AutoFacturaDetallePojoAdapter fillData(AutoFacturaDetallePojo searchData) {

        return serviceFactura.getFacturaDetallePojoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public AutoFacturaDetallePojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new AutoFacturaService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public AutoFacturaDetallePojoAdapter() {
        super();
        this.serviceFactura = new AutoFacturaService();
    }
}
