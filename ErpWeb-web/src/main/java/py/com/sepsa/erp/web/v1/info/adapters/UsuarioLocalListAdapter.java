
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;
import py.com.sepsa.erp.web.v1.info.remote.UsuarioLocalServiceClient;

/**
 * Adaptador para la lista de usuario local
 * @author Cristina Insfrán
 */
public class UsuarioLocalListAdapter extends DataListAdapter<UsuarioLocal>{
    
    /**
     * Cliente para los servicios de dato persona
     */
    private final UsuarioLocalServiceClient usuarioLocalClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioLocalListAdapter fillData(UsuarioLocal searchData) {
     
        return usuarioLocalClient.getUsuarioLocalList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de DatoPersonaListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioLocalListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.usuarioLocalClient = new UsuarioLocalServiceClient();
    }

    /**
     * Constructor de UsuarioLocalListAdapter
     */
    public UsuarioLocalListAdapter() {
        super();
        this.usuarioLocalClient= new UsuarioLocalServiceClient();
    }
}
