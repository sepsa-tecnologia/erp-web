
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;

/**
 * Filtro para el tipo de referencia geográfica
 * @author Cristina Insfrán
 */
public class TipoRefGeograficaFilter extends Filter{
     /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoRefGeograficaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
     /**
     * Agrega el filtro del código
     *
     * @param codigo
     * @return
     */
    public TipoRefGeograficaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo.trim());
        }
        return this;
    }
    
     /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public TipoRefGeograficaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion.trim());
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     * 
     * @param tipo
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoReferenciaGeografica tipo, Integer page, Integer pageSize) {
        TipoRefGeograficaFilter filter = new TipoRefGeograficaFilter();

        filter
                .id(tipo.getId())
                .codigo(tipo.getCodigo())
                .descripcion(tipo.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoRefGeograficaFilter
     */
    public TipoRefGeograficaFilter() {
        super();
    }
}
