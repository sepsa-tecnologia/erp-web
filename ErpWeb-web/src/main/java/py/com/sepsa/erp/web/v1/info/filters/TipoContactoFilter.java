
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;

/**
 * Filtro utilizado para tipo contacto
 * @author Cristina Insfrán
 */
public class TipoContactoFilter extends Filter {
    
      /**
     * Agrega el filtro de identificación del tipo contacto
     *
     * @param id Identificador del tipo contacto
     * @return
     */
    public TipoContactoFilter id(Integer id) {
        if (id!= null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro para la descripción del tipo contacto
     * @param descripcion
     * @return 
     */
    public TipoContactoFilter descripcion(String descripcion) {
        if (descripcion!= null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
      /**
     * Construye el mapa de parametros
     *
     * @param tipo
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoContacto tipo, Integer page, Integer pageSize) {
        TipoContactoFilter filter = new TipoContactoFilter();

        filter
                .id(tipo.getId())
                .descripcion(tipo.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoContactoFilter
     */
    public TipoContactoFilter() {
        super();
    }
}
