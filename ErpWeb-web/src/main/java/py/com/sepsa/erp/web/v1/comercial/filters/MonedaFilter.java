package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;

/**
 * Filtro utilizado para el servicio de Moneda
 *
 * @author Romina E. Núñez Rojas
 */
public class MonedaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param id
     * @return
     */
    public MonedaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de la moneda
     *
     * @param descripcion
     * @return
     */
    public MonedaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public MonedaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param moneda 
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Moneda moneda, Integer page, Integer pageSize) {
        MonedaFilter filter = new MonedaFilter();

        filter
                .id(moneda.getId())
                .descripcion(moneda.getDescripcion())
                .codigo(moneda.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de MonedaFilter
     */
    public MonedaFilter() {
        super();
    }
}
