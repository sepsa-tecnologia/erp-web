/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.remote.DepositoLogisticoService;

/**
 * Adapter para depósito logístico
 *
 * @author Romina Núñez
 */
public class DepositoLogisticoAdapter extends DataListAdapter<DepositoLogistico> {

    /**
     * Cliente remoto para tipo motivoEmision
     */
    private final DepositoLogisticoService depositoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public DepositoLogisticoAdapter fillData(DepositoLogistico searchData) {

        return depositoClient.getDepositoMotivoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public DepositoLogisticoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.depositoClient = new DepositoLogisticoService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public DepositoLogisticoAdapter() {
        super();
        this.depositoClient = new DepositoLogisticoService();
    }
}
