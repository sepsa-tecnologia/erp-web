package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;

/**
 * Controlador para la creación de referencia geográfica
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("referenciaCreate")
public class ReferenciaGeograficaCrearController implements Serializable {

    /**
     * Cliente para el servicio de referencia geográfica
     */
    private final ReferenciaGeograficaService service;
    /**
     * Pojo de Referencia Geográfica
     */
    private ReferenciaGeografica referencia;
    /**
     * Pojo de Referencia Geográfica
     */
    private ReferenciaGeografica referenciaAutoComplete;
    /**
     * Adaptador de referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapter;
    /**
     * Adaptador de la lista de tipo de referencia geográfica
     */
    private TipoReferenciaGeograficaAdapter adapterTipoReferencia;
    /**
     * Pojo de tipo de referencia geográfica
     */
    private TipoReferenciaGeografica tipoReferenciaGeografica;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public ReferenciaGeografica getReferencia() {
        return referencia;
    }

    public void setReferencia(ReferenciaGeografica referencia) {
        this.referencia = referencia;
    }

    public ReferenciaGeografica getReferenciaAutoComplete() {
        return referenciaAutoComplete;
    }

    public void setReferenciaAutoComplete(ReferenciaGeografica referenciaAutoComplete) {
        this.referenciaAutoComplete = referenciaAutoComplete;
    }

    public ReferenciaGeograficaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ReferenciaGeograficaAdapter adapter) {
        this.adapter = adapter;
    }

    public TipoReferenciaGeograficaAdapter getAdapterTipoReferencia() {
        return adapterTipoReferencia;
    }

    public void setAdapterTipoReferencia(TipoReferenciaGeograficaAdapter adapterTipoReferencia) {
        this.adapterTipoReferencia = adapterTipoReferencia;
    }

    public TipoReferenciaGeografica getTipoReferenciaGeografica() {
        return tipoReferenciaGeografica;
    }

    public void setTipoReferenciaGeografica(TipoReferenciaGeografica tipoReferenciaGeografica) {
        this.tipoReferenciaGeografica = tipoReferenciaGeografica;
    }

    //</editor-fold>
    /**
     * Método para crear referencia geográfica
     */
    public void crearReferencia() {
        boolean correcto = validar();
        if (correcto) {
            BodyResponse<ReferenciaGeografica> respuestaReferencia = service.setReferencia(this.referencia);
            if (respuestaReferencia.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Referencia creada correctamente!"));
                init();
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Un Departamento no puede tener padre!"));
        }
    }

    /**
     * Metodo para validar formulario
     *
     * @return
     */
    public boolean validar() {
        boolean correcto = true;
        Integer emptyValue = null;
        Integer valorTreferencia = obtenerTipoReferenciaGeografica();

        if (this.referencia.getIdTipoReferenciaGeografica() == valorTreferencia && this.referencia.getIdPadre() != null) {
            this.referencia.setIdPadre(emptyValue);
            referenciaAutoComplete = new ReferenciaGeografica();
            correcto = false;
        }

        return correcto;
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQuery(String query) {

        ReferenciaGeografica referenciaAutoComplete = new ReferenciaGeografica();
        if (referencia.getIdTipoReferenciaGeografica() != null) {
            referenciaAutoComplete.setIdTipoReferenciaGeografica(referencia.getIdTipoReferenciaGeografica());
        }
        referenciaAutoComplete.setDescripcion(query);
        adapter = adapter.fillData(referenciaAutoComplete);
        return adapter.getData();
    }

    /**
     * Selecciona el padre
     *
     * @param event
     */
    public void onItemSelectReferenciaFilter(SelectEvent event) {
        referencia.setIdPadre(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para filtrar tipo de referencia geográfica
     */
    public void filterReferencia() {
        adapterTipoReferencia = adapterTipoReferencia.fillData(tipoReferenciaGeografica);
    }

    public Integer obtenerTipoReferenciaGeografica() {
        TipoReferenciaGeograficaAdapter adapterTipoReferenciaT = new TipoReferenciaGeograficaAdapter();
        TipoReferenciaGeografica tipoReferenciaGeograficaT = new TipoReferenciaGeografica();

        tipoReferenciaGeograficaT.setCodigo("DEPARTAMENTO");
        adapterTipoReferenciaT = adapterTipoReferenciaT.fillData(tipoReferenciaGeograficaT);

        return adapterTipoReferenciaT.getData().get(0).getId();
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.adapterTipoReferencia = new TipoReferenciaGeograficaAdapter();
        this.tipoReferenciaGeografica = new TipoReferenciaGeografica();
        this.referencia = new ReferenciaGeografica();
        this.referenciaAutoComplete = new ReferenciaGeografica();
        this.adapter = new ReferenciaGeograficaAdapter();
        filterReferencia();
        obtenerTipoReferenciaGeografica();
    }

    /**
     * Constructor de la clase
     */
    public ReferenciaGeograficaCrearController() {
        init();
        this.service = new ReferenciaGeograficaService();
    }
}
