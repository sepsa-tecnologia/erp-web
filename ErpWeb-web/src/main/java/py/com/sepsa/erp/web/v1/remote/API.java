package py.com.sepsa.erp.web.v1.remote;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;

/**
 * API para conexión a servicios
 *
 * @author Daniel F. Escauriza Arza
 */
public class API {

    /**
     * Protocolo de conexión
     */
    private Scheme protocol;

    /**
     * Host o IP del servidor
     */
    private String host;

    /**
     * Puerto del servidor
     */
    private int port;

    /**
     * Path base del API
     */
    private String baseApi;

    /**
     * Token de sesión
     */
    protected String jwt;

    /**
     * Tiempo máximo de conexión
     */
    private int connTimeOut;

    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    /**
     * Obtiene el protocolo de conexión
     *
     * @return Protocolo de conexión
     */
    public Scheme getProtocol() {
        return protocol;
    }

    /**
     * Setea el protocolo de conexión
     *
     * @param protocol Protocolo de conexión
     */
    public void setProtocol(Scheme protocol) {
        this.protocol = protocol;
    }

    /**
     * Obtiene el host o IP del servidor
     *
     * @return Host o IP del servidor
     */
    public String getHost() {
        return host;
    }

    /**
     * Setea el host o IP del servidor
     *
     * @param host Host o IP del servidor
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Obtiene el puerto del servidor
     *
     * @return Puerto del servidor
     */
    public int getPort() {
        return port;
    }

    /**
     * Setea el puerto del servidor
     *
     * @param port Puerto del servidor
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Obtiene el path base del servicio
     *
     * @return Path base del servicio
     */
    public String getBaseApi() {
        return baseApi;
    }

    /**
     * Setea el path base del servicio
     *
     * @param baseApi Path base del servicio
     */
    public void setBaseApi(String baseApi) {
        this.baseApi = baseApi;
    }

    /**
     * Obtiene el token de sesión
     *
     * @return Token de sesión
     */
    public String getJwt() {
        return jwt;
    }

    /**
     * Setea el token de sesión
     *
     * @param jwt Token de sesión
     */
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    /**
     * Obtiene el tiempo máximo de conexión al servidor
     *
     * @return Tiempo máximo de conexión al servidor
     */
    public int getConnTimeOut() {
        return connTimeOut;
    }

    /**
     * Setea el tiempo máximo de conexión al servidor
     *
     * @param connTimeOut Tiempo máximo de conexión al servidor
     */
    public void setConnTimeOut(int connTimeOut) {
        this.connTimeOut = connTimeOut;
    }

    /**
     * Obtiene la URL de conexión
     *
     * @return URL de conexión
     */
    public String getURL() {
        return String.format("%s://%s:%d/%s/", protocol.getScheme(), host,
                port, baseApi);
    }

    /**
     * Setea el token JWT desde el bean de sesión del usuario
     */
    protected void setBearer() {
        try {
            if (jwt == null || jwt.trim().isEmpty() || jwt.toLowerCase().trim().equals("bearer null")) {
                //TODO: Cambiar a @Inject
                FacesContext context = FacesContext.getCurrentInstance();
                SessionData session = context.getApplication()
                        .evaluateExpressionGet(context, "#{sessionData}",
                                SessionData.class);
                this.jwt = session == null ? null : String.format("Bearer %s",
                        session.getJwt());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * GET para servicios
     *
     * @param service Servicio web
     * @param contentType Tipo de contenido
     * @param params Lista de parámetros
     * @return Conexión HTTP
     */
    protected HttpURLConnection GET(String service, ContentType contentType,
            Map<String, String> params) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            if (params != null && !params.isEmpty()) {
                url = String.format("%s%s", url, "?");

                List<String> keyList = new ArrayList(params.keySet());

                for (int i = 0; i < keyList.size(); i++) {
                    String format
                            = i == keyList.size() - 1
                            ? "%s%s=%s"
                            : "%s%s=%s&";

                    url = String.format(format, url, keyList.get(i),
                            URLEncoder.encode(String.valueOf(params.get(keyList.get(i))), "UTF-8"));
                }
            }

            WebLogger.get().debug(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setRequestMethod(Method.GET.toString());

            httpConn.setConnectTimeout(connTimeOut);
            httpConn.setReadTimeout(connTimeOut);

            httpConn.setRequestProperty("Content-type", contentType.getValue());

            this.setBearer();
            if (jwt != null && !jwt.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", jwt);
            }

        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }

        return httpConn;
    }

    /**
     * PUT para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @return Conexión HTTP
     */
    protected HttpURLConnection PUT(String service, ContentType contentType) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            WebLogger.get().debug(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setDoOutput(true);

            httpConn.setRequestMethod(Method.PUT.toString());

            httpConn.setConnectTimeout(connTimeOut);

            httpConn.setRequestProperty("Content-type", contentType.getValue());

            this.setBearer();
            if (jwt != null && !jwt.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", jwt);
            }

        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }

        return httpConn;
    }

    /**
     * POST para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @return Conexión HTTP
     */
    protected HttpURLConnection POST(String service, ContentType contentType) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            WebLogger.get().debug(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true);

            httpConn.setRequestMethod(Method.POST.toString());

            httpConn.setConnectTimeout(connTimeOut);

            String content = contentType.getValue();

            //Se agrega el boundary para las diferentes partes del POST
            if (BOUNDARY != null && contentType.equals(ContentType.MULTIPART)) {
                content = String.format("%s; boundary=%s", content, BOUNDARY);
            }

            httpConn.setRequestProperty("Content-type", content);

            this.setBearer();
            if (jwt != null && !jwt.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", jwt);
            }

        } catch (MalformedURLException | ProtocolException ex) {
            WebLogger.get().fatal(ex);
        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }

        return httpConn;
    }

    /**
     * PUT para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @param params
     * @return Conexión HTTP
     */
    protected HttpURLConnection PUT(String service, ContentType contentType,
            Map<String, String> params) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            if (params != null && !params.isEmpty()) {
                url = String.format("%s%s", url, "?");

                List<String> keyList = new ArrayList(params.keySet());

                for (int i = 0; i < keyList.size(); i++) {
                    String format
                            = i == keyList.size() - 1
                            ? "%s%s=%s"
                            : "%s%s=%s&";

                    url = String.format(format, url, keyList.get(i),
                            URLEncoder.encode(String.valueOf(params.get(keyList.get(i))), "UTF-8"));
                }
            }

            WebLogger.get().debug(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setDoOutput(true);

            httpConn.setRequestMethod(Method.PUT.toString());

            httpConn.setConnectTimeout(connTimeOut);

            httpConn.setRequestProperty("Content-type", contentType.getValue());

            this.setBearer();
            if (jwt != null && !jwt.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", jwt);
            }

        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }

        return httpConn;
    }

    /**
     * DELETE para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @return Conexión HTTP
     */
    protected HttpURLConnection DELETE(String service, ContentType contentType) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            WebLogger.get().debug(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true);

            httpConn.setRequestMethod(Method.DELETE.toString());

            httpConn.setConnectTimeout(connTimeOut);

            String content = contentType.getValue();

            httpConn.setRequestProperty("Content-type", content);

            this.setBearer();
            if (jwt != null && !jwt.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", jwt);
            }

        } catch (MalformedURLException | ProtocolException ex) {
            WebLogger.get().fatal(ex);
        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }

        return httpConn;
    }

    /**
     * Agrega un body al request
     *
     * @param httpConn Conexión HTTP
     * @param body Body del mensaje
     */
    protected void addBody(HttpURLConnection httpConn, String body) {
        try {

            if (body != null && !body.trim().isEmpty()) {
                WebLogger.get().debug(String.format("Body request: %s", body));

                OutputStream os = httpConn.getOutputStream();
                os.write(body.getBytes("UTF-8"));
                os.flush();
            }
        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }
    }

    /**
     * Agrega un archivo a un formulario HTTP
     *
     * @param writer Clase utilizada para escribir en el buss de datos
     * @param os Stream de salida
     * @param fieldName Nombre del campo a enviar
     * @param uploadFile Archivo a enviar
     * @throws java.io.IOException
     */
    public void addFormFile(PrintWriter writer, OutputStream os,
            String fieldName, Attach uploadFile) throws IOException {

        if (uploadFile != null && uploadFile.getData() != null && writer != null) {

            String fileName = uploadFile.getFileName();
            String data = String.format("Content-Disposition: "
                    + "form-data; name=\"%s\"; filename=\"%s\"",
                    fieldName, fileName);

            writer
                    .append(String.format("--%s", BOUNDARY))
                    .append(LINE_FEED)
                    .append(data)
                    .append(LINE_FEED)
                    .append(LINE_FEED)
                    .flush();
            byte[] buffer = new byte[4096];

            try ( InputStream inputStream = uploadFile.getData()) {
                int bytesRead = inputStream.read(buffer);

                while (bytesRead != -1) {
                    os.write(buffer, 0, bytesRead);
                    bytesRead = inputStream.read(buffer);
                }
                os.flush();
            }
            writer.append(LINE_FEED).flush();
        }
    }

    /**
     * Actualiza la información de conexión
     *
     * @param info Info
     */
    protected final void updateConnInfo(InfoPojo info) {
        this.protocol = info.getProtocol();
        this.port = info.getPort();
        this.host = info.getHost();
    }

    /**
     * Constructor de API
     *
     * @param protocol Protocolo
     * @param host Host
     * @param port Puerto
     * @param baseApi Base api
     * @param connTimeOut Timeout
     */
    public API(Scheme protocol, String host, int port, String baseApi,
            int connTimeOut) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.baseApi = baseApi;
        this.connTimeOut = connTimeOut;
    }

    /**
     * Constructor de API
     *
     * @param protocol Protocolo
     * @param host Host
     * @param port Puerto
     * @param baseApi Base api
     * @param connTimeOut Timeout
     * @param jwt Token
     */
    public API(Scheme protocol, String host, int port, String baseApi,
            int connTimeOut, String jwt) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.baseApi = baseApi;
        this.connTimeOut = connTimeOut;
        this.jwt = jwt;
    }

    /**
     * Métodos para el request
     */
    public enum Method {
        GET, POST, PUT, DELETE
    }

    /**
     * Tipo de contenido
     */
    public enum ContentType {

        JSON("application/json"),
        XML("application/xml"),
        MULTIPART("multipart/form-data");

        /**
         * Valor del enum
         */
        private String value;

        /**
         * Obtiene el valor del enum
         *
         * @return Valor del enum
         */
        public String getValue() {
            return value;
        }

        /**
         * Setea el valor del enum
         *
         * @param value Valor del enum
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Constructor de ContentType
         *
         * @param value Valor
         */
        private ContentType(String value) {
            this.value = value;
        }
    }

    /**
     * Esquema de conexión
     */
    public enum Scheme {
        HTTP("http"),
        HTTPS("https");

        /**
         * Esquema
         */
        private String scheme;

        /**
         * Obtiene el esquema
         *
         * @return Esquema
         */
        public String getScheme() {
            return scheme;
        }

        /**
         * Setea el esquema
         *
         * @param scheme Esquema
         */
        public void setScheme(String scheme) {
            this.scheme = scheme;
        }

        /**
         * Constructor de Scheme
         *
         * @param scheme Esquema
         */
        Scheme(String scheme) {
            this.scheme = scheme;
        }
    }

}
