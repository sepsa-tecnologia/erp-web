package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;
import py.com.sepsa.erp.web.v1.info.remote.TipoMenuServiceClient;

/**
 * Adaptador para la lista de TipoMenu
 *
 * @author alext
 */
public class TipoMenuListAdapter extends DataListAdapter<TipoMenu> {

    /**
     * Cliente para el servicio de TipoMenu
     */
    private final TipoMenuServiceClient service;

    @Override
    public TipoMenuListAdapter fillData(TipoMenu searchData) {
        return service.
                getTipoEmailList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoMenuListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoMenuListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new TipoMenuServiceClient();
    }

    /**
     * Constructor de TipoMenuListAdapter
     */
    public TipoMenuListAdapter() {
        super();
        this.service = new TipoMenuServiceClient();
    }

}
