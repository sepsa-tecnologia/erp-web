package py.com.sepsa.erp.web.v1.info.controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.info.adapters.ReporteEmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Reporte;
import py.com.sepsa.erp.web.v1.info.pojos.ReporteEmpresa;
import py.com.sepsa.erp.web.v1.info.remote.ReporteEmpresaService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para reporte de venta
 *
 * @author Jonathan Bernal
 */
@ViewScoped
@Named("reporteEmpresa")
public class ReporteEmpresaController implements Serializable {

    @Inject
    private SessionData session;

    @Getter
    @Setter
    private ReporteEmpresa reporteEmpresa;

    @Getter
    @Setter
    private Reporte reporte;

    @Getter
    @Setter
    private Boolean visualizarPanel;

    private ReporteEmpresaService service;

    @Getter
    @Setter
    private ReporteEmpresaAdapter adapter;

    @Getter
    @Setter
    private ReporteEmpresa filter;

    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {

        boolean download = true;

        for (Reporte.Parametro parametro : reporte.getParametros()) {
            if (parametro.getCodigo().equals("ID_EMPRESA")) {
                parametro.setIntegerValue(session.getUser().getIdEmpresa());
            }

            if (parametro.getMandatorio() != null
                    && parametro.getMandatorio()
                    && !parametro.hasValue()) {
                download = false;
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                                String.format("El campo <<%s>> es mandatorio",
                                        parametro.getDescripcion())));
            }
        }

        if (download == true) {
            String contentType = "application/pdf";
            switch (reporte.getFormato()) {
                case "XLSX":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "PDF":
                    contentType = "application/pdf";
                    break;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH_mm_ss ");
            String fileName = String.format("%s_%s.%s",
                    reporte.getDescripcion(),
                    sdf.format(Calendar.getInstance().getTime()),
                    reporte.getFormato().toLowerCase());
            byte[] data = service.generar(generarParametros(reporteEmpresa, reporte));
            if (data != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(data),
                        contentType, fileName);
            }

        }

        return null;
    }

    public JsonElement generarParametros(ReporteEmpresa reporteEmpresa, Reporte reporte) {

        JsonArray parametros = new JsonArray();

        for (Reporte.Parametro parametro : reporte.getParametros()) {
            parametros.add(parametro.getJsonObject());
        }

        JsonObject result = new JsonObject();
        result.add("parametros", parametros);
        result.addProperty("id", reporteEmpresa.getId());
        return result;
    }

    public void getReportes() {
        this.adapter = adapter.fillData(filter);
    }

    public List<ReporteEmpresa> completeQueryReporteEmpresa(String query) {
        ReporteEmpresa filtro = new ReporteEmpresa();
        filtro.setActivo('S');
        filtro.setIdEmpresa(session.getUser().getIdEmpresa());
        filtro.setDescripcionReporte(query);
        adapter = adapter.fillData(filtro);
        return adapter.getData();
    }

    public void onItemSelectReporteEmpresa(SelectEvent event) {
        ReporteEmpresa value = (ReporteEmpresa) event.getObject();
        reporte = value.getReporte();
        visualizarPanel = true;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.filter = new ReporteEmpresa();
        this.service = new ReporteEmpresaService();
        this.adapter = new ReporteEmpresaAdapter();
    }
}
