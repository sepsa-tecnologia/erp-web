package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.task.GeneracionDteNotaDebitoMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoTalonarioAdapter;
//import py.com.sepsa.erp.web.v1.facturacion.adapters.SolicitudNotaDebitoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
//import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaDebito;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Detalles
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("notaDebitoCrearAlternativo")
public class NotaDebitoCrearAlternativoController implements Serializable {

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean create;

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaMontoAdapter adapterFactura;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    private Factura facturaSeleccionada;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Crédito
     */
    private NotaDebitoService serviceNotaDebito;
    /**
     * POJO Datos Nota de Crédito
     */
    private DatosNotaDebito datoNotaDebito;
    /**
     * POJO Nota de Crédito
     */
    private NotaDebito notaDebitoCreate;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.C.
     */
    private List<NotaDebitoDetalle> listaDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private ClientePojo cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientPojoAdapter adapterCliente;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato de factura
     */
    private String idFactura;
    /**
     *
     * Adaptador para la lista de motivoEmisionInterno
     *
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;
    /**
     *
     * POJO de MotivoEmisionInterno
     *
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Descripción de motivo emisión
     */
    private String motivoEmision;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Identificador de solicitud
     */
    private String idSolicitud;
    /**
     * Dato de control
     */
    private boolean showDatatableSolicitud;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listFacturaSolicitud = new ArrayList<>();
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Moneda> listMoneda = new ArrayList<>();
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFacturaSolicitud = new ArrayList<>();
//    /**
//     * Adapter solicitud
//     */
//    private SolicitudNotaDebitoListAdapter adapterSolicitud;
//    /**
//     * POJO Solicitud
//     */
//    private SolicitudNotaDebito solicitudNC;
    /**
     * Bandera
     */
    private boolean habilitarSeleccion;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Adapter Factura Talonario
     */
    private NotaDebitoTalonarioAdapter adapterNCTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo ncTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para agregar campos de dirección
     */
    private String tieneDireccion;
    /**
     * Bandera para mostrar formulario en vista alternativa
     */
    private Boolean showFactura;
    /**
     * Bandera para manejar el tipo de factura
     */
    private String tipoFactura;
    
    private String timbradoFactura;
    
    private String nroFactura;
    
    private String cdcFactura;
    
    private Date fechaFactura;
    
    private TipoCambioAdapter adapterTipoCambio;
    
    private boolean habilitarPanelCotizacion;
    
    private TipoCambio tipoCambioFilter;
    
    private List<TipoCambio> listSelectedCotizacion = new ArrayList<>();
    
    private String nroNCcreada;

    private Integer idNcCreada;

    private String siediApi;

    private FileServiceClient fileServiceClient;
    
    private Producto producto;
    
        /**
     * Adaptador producto;
     */
    private ProductoAdapter adapterProducto;
    private Integer idProducto;
    private String descripcion;
    private Integer porcentaje;
    private String loteProducto;
    private String fechaVencimientoLote;
    private Integer nroLineaProducto;
    private Integer tipoDescuento;
    
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(Integer tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }
    
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getLoteProducto() {
        return loteProducto;
    }

    public void setLoteProducto(String loteProducto) {
        this.loteProducto = loteProducto;
    }

    public String getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(String fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    
    
    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    
    
    public String getNroNCcreada() {
        return nroNCcreada;
    }

    public void setNroNCcreada(String nroNCcreada) {
        this.nroNCcreada = nroNCcreada;
    }

    public Integer getIdNcCreada() {
        return idNcCreada;
    }

    public void setIdNcCreada(Integer idNcCreada) {
        this.idNcCreada = idNcCreada;
    }

    public String getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(String siediApi) {
        this.siediApi = siediApi;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }
    
    public List<TipoCambio> getListSelectedCotizacion() {
        return listSelectedCotizacion;
    }

    public void setListSelectedCotizacion(List<TipoCambio> listSelectedCotizacion) {
        this.listSelectedCotizacion = listSelectedCotizacion;
    }
    
    public TipoCambio getTipoCambioFilter() {
        return tipoCambioFilter;
    }

    public void setTipoCambioFilter(TipoCambio tipoCambioFilter) {
        this.tipoCambioFilter = tipoCambioFilter;
    }    
    
    public boolean isHabilitarPanelCotizacion() {
        return habilitarPanelCotizacion;
    }

    public void setHabilitarPanelCotizacion(boolean habilitarPanelCotizacion) {
        this.habilitarPanelCotizacion = habilitarPanelCotizacion;
    }
    
    public TipoCambioAdapter getAdapterTipoCambio() {
        return adapterTipoCambio;
    }

    public void setAdapterTipoCambio(TipoCambioAdapter adapterTipoCambio) {
        this.adapterTipoCambio = adapterTipoCambio;
    }
        
    public void setListMoneda(List<Moneda> listMoneda) {
        this.listMoneda = listMoneda;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public void setAdapterCliente(ClientPojoAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public ClientePojo getCliente() {
        return cliente;
    }

    public ClientPojoAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public List<Moneda> getListMoneda() {
        return listMoneda;
    }

    public void setTieneDireccion(String tieneDireccion) {
        this.tieneDireccion = tieneDireccion;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public String getTieneDireccion() {
        return tieneDireccion;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setFacturaSeleccionada(Factura facturaSeleccionada) {
        this.facturaSeleccionada = facturaSeleccionada;
    }

    public Factura getFacturaSeleccionada() {
        return facturaSeleccionada;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public NotaDebitoTalonarioAdapter getAdapterNCTalonario() {
        return adapterNCTalonario;
    }

    public void setAdapterNCTalonario(NotaDebitoTalonarioAdapter adapterNCTalonario) {
        this.adapterNCTalonario = adapterNCTalonario;
    }

    public void setNcTalonario(TalonarioPojo ncTalonario) {
        this.ncTalonario = ncTalonario;
    }

    public TalonarioPojo getNcTalonario() {
        return ncTalonario;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setHabilitarSeleccion(boolean habilitarSeleccion) {
        this.habilitarSeleccion = habilitarSeleccion;
    }

    public boolean isHabilitarSeleccion() {
        return habilitarSeleccion;
    }

//    public void setSolicitudNC(SolicitudNotaDebito solicitudNC) {
//        this.solicitudNC = solicitudNC;
//    }
//
//    public void setAdapterSolicitud(SolicitudNotaDebitoListAdapter adapterSolicitud) {
//        this.adapterSolicitud = adapterSolicitud;
//    }
//
//    public SolicitudNotaDebito getSolicitudNC() {
//        return solicitudNC;
//    }
//
//    public SolicitudNotaDebitoListAdapter getAdapterSolicitud() {
//        return adapterSolicitud;
//    }

    public void setListSelectedFacturaSolicitud(List<Factura> listSelectedFacturaSolicitud) {
        this.listSelectedFacturaSolicitud = listSelectedFacturaSolicitud;
    }

    public void setListFacturaSolicitud(List<Factura> listFacturaSolicitud) {
        this.listFacturaSolicitud = listFacturaSolicitud;
    }

    public List<Factura> getListSelectedFacturaSolicitud() {
        return listSelectedFacturaSolicitud;
    }

    public List<Factura> getListFacturaSolicitud() {
        return listFacturaSolicitud;
    }

    public void setShowDatatableSolicitud(boolean showDatatableSolicitud) {
        this.showDatatableSolicitud = showDatatableSolicitud;
    }

    public boolean isShowDatatableSolicitud() {
        return showDatatableSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public void setMotivoEmision(String motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public String getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {
        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;
    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {
        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;
    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {
        return motivoEmisionInternoFilter;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {
        return motivoEmisionInternoAdapterList;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListSelectedFactura(List<Factura> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public List<Factura> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListaDetalle(List<NotaDebitoDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<NotaDebitoDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setNotaDebitoCreate(NotaDebito notaDebitoCreate) {
        this.notaDebitoCreate = notaDebitoCreate;
    }

    public NotaDebito getNotaDebitoCreate() {
        return notaDebitoCreate;
    }

    public NotaDebitoService getServiceNotaDebito() {
        return serviceNotaDebito;
    }

    public void setServiceNotaDebito(NotaDebitoService serviceNotaDebito) {
        this.serviceNotaDebito = serviceNotaDebito;
    }

    

    public DatosNotaDebito getDatoNotaDebito() {
        return datoNotaDebito;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaMontoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaMontoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

    public Boolean getShowFactura() {
        return showFactura;
    }

    public void setShowFactura(Boolean showFactura) {
        this.showFactura = showFactura;
    }

    public String getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    


//</editor-fold>
    
    
    /**
     * Método para seleccionar el talonario
     * @param event 
     */
    public void onCheckTalonario(SelectEvent event) {
        TalonarioPojo selected = (TalonarioPojo) event.getObject();
        talonario = selected;
        notaDebitoCreate.setIdTipoCambio(selected.getId()); 
    } 
    
    /**
     * Método para agregar detalle
     *
     * @param event
     */
    public void onCheckFactura(SelectEvent event) {
        WebLogger.get().debug("entra");
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();

        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(((Factura) event.getObject()).getId());
        afp = afp.fillData(fdp);

        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {

            NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();

            if (listaDetalle.isEmpty()) {
                notaDebitoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaDebitoDetalle.setNroLinea(linea);
            }
            notaDebitoDetalle.setNroLinea(linea);
            notaDebitoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaDebitoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());

            notaDebitoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();
            notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);

            /**
             * if (facturaDetalle.getProducto() != null) {
             * notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE
             * LA FACTURA NRO." + nroFactura + " DEL PRODUCTO: " +
             * facturaDetalle.getProducto().getDescripcion());
             *
             * } else { notaDebitoDetalle.setDescripcion("BONIFICACION " +
             * linea + " DE LA FACTURA NRO." + nroFactura + " DEL PRODUCTO "); }
             *
             */
            notaDebitoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaDebitoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaDebitoDetalle.setCantidad(facturaDetalle.getCantidad());

            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva().
                    subtract(facturaDetalle.getDescuentoParticularUnitario()).
                    subtract(facturaDetalle.getDescuentoGlobalUnitario());
            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaDebitoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaDebitoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaDebitoDetalle.setIdProducto(facturaDetalle.getIdProducto());

            listaDetalle.add(notaDebitoDetalle);
        }
        listSelectedFactura = new ArrayList<>();
        listSelectedFactura.add(((Factura) event.getObject()));

        calcularTotales();
    }

    /**
     * Método para calcular el precio unitario sin IVA
     *
     * @param porcentaje
     * @param precioUnitIVA
     * @return
     */
    public BigDecimal calcularPrecioUnitSinIVA(Integer porcentaje, BigDecimal precioUnitIVA) {
        BigDecimal precioUnitSinIVA = new BigDecimal("0");
        if (porcentaje == 0) {
            precioUnitSinIVA = precioUnitIVA;
        }

        if (porcentaje == 5) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

        }

        if (porcentaje == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

        }

        return precioUnitSinIVA;
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        try {
            configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_ND_DIGITAL");
            configuracionValorFilter.setActivo("S");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
            //digital = adapterConfigValor.getData().get(0).getValor();
            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                digital = "N";
            } else {
                digital = adapterConfigValor.getData().get(0).getValor();
            }
            obtenerDatosTalonario();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void obtenerDatosTalonario() {
        ncTalonario.setDigital(digital);
        ncTalonario.setIdEncargado(session.getUser().getIdPersonaFisica());
        adapterNCTalonario = adapterNCTalonario.fillData(ncTalonario);

        if (adapterNCTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            //PF.current().ajax().update(":list-nota-debito-create-form:form-data:pnl-pop-up");
            PF.current().executeScript("$('#modalTalonario').modal('show');");
        } else  if (adapterNCTalonario.getData().size() > 0){
            talonario.setId(adapterNCTalonario.getData().get(0).getId());
            obtenerDatosNC();
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontró un talonario"));
        }
    }

    /**
     * Método invocado para deseleccionar liquidacion
     *
     * @param event
     */
    public void onUncheckFactura(UnselectEvent event) {

        BigDecimal subTotal = new BigDecimal("0");
        NotaDebitoDetalle info = null;

        for (int i = 0; i < ((Factura) event.getObject()).getFacturaDetalles().size(); i++) {
            for (NotaDebitoDetalle info0 : listaDetalle) {
                if (Objects.equals(info0.getIdFactura(), ((Factura) event.getObject()).getId())) {
                    info = info0;
                    subTotal = info.getMontoTotal();
                    listaDetalle.remove(info);
                    break;
                }
            }

            BigDecimal monto = notaDebitoCreate.getMontoTotalNotaDebito();
            BigDecimal totalNotaDebito = monto.subtract(subTotal);
            notaDebitoCreate.setMontoTotalNotaDebito(totalNotaDebito);

        }

        calcularTotales();

    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void cambiarDescripcion(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        String h = info.getDescripcion();
        WebLogger.get().debug(h);
    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontosa(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal cinco = new BigDecimal("5");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoImponible.divide(cinco, 2, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal diez = new BigDecimal("10");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoImponible.divide(diez, 5, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotales();

    }

    public NotaDebitoDetalle obtenerDetalle(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                return info;
            }
        }

        return null;
    }
    
    public void calcularMontos(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        info = listaDetalle.get(nroLinea);
        if (info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == -1){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pueden ingresar montos menores a 0."));
        } else {
            // Se calcula el monto total del detalle
            BigDecimal cantidad = info.getCantidad();
            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
            BigDecimal descuentoParticularUnitario = info.getDescuentoParticularUnitarioAux(); 
            BigDecimal montoPrecioXCantidadSinDescuento = precioUnitarioConIVA.multiply(cantidad);
            //Se aplica el descuento al item
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();
            
            //Casos en que se hayan ingresado primero el monto/porcentaje descuento y luego el precioUnitarioIVA
            
            if (descuentoParticularUnitario != null && descuentoParticularUnitario.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,1);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = listaDetalle.get(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            }
            
            else if( montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,2);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = listaDetalle.get(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            } 

            if (montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) { 
                montoDescuentoItem = info.getMontoDescuentoParticular();
                precioUnitarioConIVA = precioUnitarioConIVA.subtract(montoDescuentoItem);
            } else {
                info.setDescuentoGlobalUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
            }
            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);
            // Se calcula el monto base            
            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxB = new BigDecimal(100).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxC = auxA.multiply(auxB);
            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);

            // Se calcula el monto exento gravado
            BigDecimal auxD = new BigDecimal("100").subtract(new BigDecimal(100));
            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
            // Integer aux1 = ((100 - info.getPorcentajeIva()) / 100);
            BigDecimal montoExentoGravado = montoBase.multiply(auxF);

            //Se calcula el monto imponible
            //Integer aux2 = info.getPorcentajeGravada() / 100;
            //BigDecimal auxF = new BigDecimal(info.getPorcentajeGravada()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
            BigDecimal montoImponible = montoBase.multiply(auxB);

            //Se calcula el monto iva
            // Integer aux3 = info.getPorcentajeIva() / 100;
            BigDecimal montoIva = montoImponible.multiply(auxA);

            //Se suman los montos para el total
            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);

            //Precio unitario
            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);

            info.setMontoIva(montoIva);
            info.setMontoImponible(montoImponible);
         //   info.setMontoExentoGravado(montoExentoGravado);
            info.setMontoTotal(total);
            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
            info.setSubTotalSinDescuento(montoPrecioXCantidadSinDescuento);
            calcularTotales();
            
            if (notaDebitoCreate.getPorcentajeDescuentoGlobal()!= null && notaDebitoCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1){
                calcularDescuentos(tipoDescuento);
            }
        }
        
        

    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");
        BigDecimal auxTotalSinDescuento = new BigDecimal("0");
        
        for (int i = 0; i < listaDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaDetalle.get(i).getMontoTotal());
            if (listaDetalle.get(i).getMontoDescuentoParticular().compareTo(BigDecimal.ZERO)>0) {
                auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getPrecioUnitarioConIva().multiply(listaDetalle.get(i).getCantidad()));
            } else {
                 auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getMontoTotal());
            }
        }

        notaDebitoCreate.setMontoTotalNotaDebito(acumTotales);
        notaDebitoCreate.setMontoTotalGuaranies(acumTotales);
        notaDebitoCreate.setSubTotalSinDescuento(auxTotalSinDescuento);
        

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        notaDebitoCreate.setMontoIva5(montoIva5Acumulador);
        notaDebitoCreate.setMontoImponible5(montoImponible5Acumulador);
        notaDebitoCreate.setMontoTotal5(montoTotal5Acumulador);
        notaDebitoCreate.setMontoIva10(montoIva10Acumulador);
        notaDebitoCreate.setMontoImponible10(montoImponible10Acumulador);
        notaDebitoCreate.setMontoTotal10(montoTotal10Acumulador);
        notaDebitoCreate.setMontoTotalExento(montoExcentoAcumulador);
        notaDebitoCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        notaDebitoCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }
    
    /**
     * Método para calcular descuent global
     * @param param (1 = calcula en base al porcentaje ingresado 2= Calcula en base al monto ingresado)
     */
    public void calcularDescuentos(Integer param){
        calcularTotales();
        if (notaDebitoCreate.getMontoTotalNotaDebito() != null && notaDebitoCreate.getMontoTotalNotaDebito().compareTo(BigDecimal.ZERO) == 1){
            if (param == 1){
                if (notaDebitoCreate.getPorcentajeDescuentoGlobal()!= null) {
                    BigDecimal porcentajeDescuento = notaDebitoCreate.getPorcentajeDescuentoGlobal().divide(new BigDecimal("100"),5,RoundingMode.HALF_UP);
                    if(notaDebitoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0) {
                        notaDebitoCreate.setMontoTotalDescuentoGlobal(notaDebitoCreate.getSubTotalSinDescuento().multiply(porcentajeDescuento));
                    } else {
                        notaDebitoCreate.setMontoTotalDescuentoGlobal(notaDebitoCreate.getMontoTotalNotaDebito().multiply(porcentajeDescuento));
                    }
                    
                    notaDebitoCreate.setMontoTotalNotaDebito(notaDebitoCreate.getMontoTotalNotaDebito().subtract(notaDebitoCreate.getMontoTotalDescuentoGlobal()));
                        
                    notaDebitoCreate.setMontoTotalGuaranies(notaDebitoCreate.getMontoTotalNotaDebito());
                    obtenerMontoTotalLetras();                        
                } else {
                    notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                }
                
            } else if (param == 2) {
                if (notaDebitoCreate.getMontoTotalDescuentoGlobal() != null) {
                    BigDecimal aux = notaDebitoCreate.getMontoTotalDescuentoGlobal().multiply(new BigDecimal("100"));
                    BigDecimal porcentaje = BigDecimal.ZERO;
                    if(notaDebitoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0){
                         porcentaje = aux.divide(notaDebitoCreate.getSubTotalSinDescuento(),5,RoundingMode.HALF_UP);
                    } else {
                         porcentaje = aux.divide(notaDebitoCreate.getMontoTotalNotaDebito(),5,RoundingMode.HALF_UP);
                    }
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(porcentaje);
                    notaDebitoCreate.setMontoTotalNotaDebito(notaDebitoCreate.getMontoTotalNotaDebito().subtract(notaDebitoCreate.getMontoTotalDescuentoGlobal()));
                   
                    notaDebitoCreate.setMontoTotalGuaranies(notaDebitoCreate.getMontoTotalNotaDebito());
                    obtenerMontoTotalLetras();
                        
                } else {
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                    notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                }
                
            }
        }
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(notaDebitoCreate.getIdMoneda());
        montoLetrasFilter.setCodigoMoneda("PYG");
        BigDecimal scaled = notaDebitoCreate.getMontoTotalNotaDebito().setScale(0, RoundingMode.HALF_UP);
        montoLetrasFilter.setMonto(scaled);

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ClientePojo> completeQuery(String query) {

        ClientePojo cliente = new ClientePojo();
        cliente.setRazonSocial(query);
        cliente.setListadoPojo(true);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            notaDebitoCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        }

    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
        int id = ((ClientePojo) event.getObject()).getIdCliente();

        notaDebitoCreate = new NotaDebito();

        if (((ClientePojo) event.getObject()).getIdNaturalezaCliente() != null) {
            notaDebitoCreate.setIdNaturalezaCliente(((ClientePojo) event.getObject()).getIdNaturalezaCliente());
        }

        notaDebitoCreate.setRazonSocial(((ClientePojo) event.getObject()).getRazonSocial());
        ruc = ((ClientePojo) event.getObject()).getNroDocumento();
        ruc = ruc.trim().replace(" ", "").replace(".", "");
        idCliente = ((ClientePojo) event.getObject()).getIdCliente();
        notaDebitoCreate.setRuc(ruc);
        notaDebitoCreate.setIdCliente(idCliente);
        notaDebitoCreate.setIdMoneda(1);

     //  filterFactura(id);
        obtenerDatosNC();
        showFactura= true;

    }

    public void obtenerDatosNC() {
        try {

            Map parametros = new HashMap();
            datoNotaDebito = new DatosNotaDebito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaDebitoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaDebitoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaDebito = serviceNotaDebito.getDatosNotaDebito(parametros);

            if (datoNotaDebito != null) {
                notaDebitoCreate.setFecha(today.getTime());
                notaDebitoCreate.setIdTalonario(datoNotaDebito.getIdTalonario());
                notaDebitoCreate.setNroNotaDebito(datoNotaDebito.getNroNotaDebito());

                //Validaciones
                if (datoNotaDebito.getTelefono() != null) {
                    notaDebitoCreate.setTelefono(datoNotaDebito.getTelefono());
                }
                if (datoNotaDebito.getEmail() != null) {
                    notaDebitoCreate.setEmail(datoNotaDebito.getEmail());
                }
                if (datoNotaDebito.getDireccion() != null) {
                    notaDebitoCreate.setDireccion(datoNotaDebito.getDireccion());
                }
                if (datoNotaDebito.getIdDepartamento() != null) {
                    notaDebitoCreate.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaDebito.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datoNotaDebito.getIdDistrito() != null) {
                    notaDebitoCreate.setIdDistrito(datoNotaDebito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaDebito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaDebito.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datoNotaDebito.getIdCiudad() != null) {
                    notaDebitoCreate.setIdCiudad(datoNotaDebito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaDebito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaDebito.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datoNotaDebito.getNroCasa() != null) {
                    notaDebitoCreate.setNroCasa(datoNotaDebito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }
            
            notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
            notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
//        if (idFactura != null) {
//            obtenerDatosNCDevolucionFactura();
//            devolucionDetalleNC();
//            show = true;
//            PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:detalle-cabecera list-nota-debito-create-form:form-data:panel-cabecera-detalle:data-list-detalle-nota-debito list-nota-debito-create-form:form-data:panel-nota-debito:data-list-nota-create");
//
//        }
    }

    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }

    
    
    public Boolean validarNroFactura() {
        String formato ="\\d{3}-\\d{3}-\\d{7}";
        
        Pattern pattern = Pattern.compile(formato);       
        Matcher matcher = pattern.matcher(nroFactura);
        
        if(matcher.matches()){
            return true;
        } else {
            return false;
        }
    }
        
    /**
     * Método para crear Nota de Crédito
     */
    public void createNotaDebito() {
        calcularTotalesGenerales();
        boolean saveNC = true;

        notaDebitoCreate.setAnulado("N");
        notaDebitoCreate.setImpreso("N");
        notaDebitoCreate.setEntregado("N");
        notaDebitoCreate.setDigital(digital);
        
        if ((notaDebitoCreate.getIdMoneda() != null && !notaDebitoCreate.getIdMoneda().equals(1) ) && notaDebitoCreate.getIdTipoCambio() == null){
            saveNC = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar una cotización!"));
            PF.current().executeScript("$('#modalCotizacion').modal('show');");   
        }       
        if (tieneDireccion.equals("S")) {
            if (notaDebitoCreate.getDireccion() == null || notaDebitoCreate.getDireccion().trim().isEmpty()) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
            }

            if (notaDebitoCreate.getNroCasa() == null || notaDebitoCreate.getNroCasa().trim().isEmpty()) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
            }

            if (idDepartamento == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
            } else {
                notaDebitoCreate.setIdDepartamento(idDepartamento);
            }

            if (idDistrito == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
            } else {
                notaDebitoCreate.setIdDistrito(idDistrito);
            }

            if (idCiudad == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
            } else {
                notaDebitoCreate.setIdCiudad(idCiudad);
            }

        } else {
            notaDebitoCreate.setDireccion(null);
            notaDebitoCreate.setNroCasa(null);
            notaDebitoCreate.setIdDepartamento(null);
            notaDebitoCreate.setIdDistrito(null);
            notaDebitoCreate.setIdCiudad(null);
        }

        if (digital.equals("S")) {
            notaDebitoCreate.setArchivoSet("S");
        } else {
            notaDebitoCreate.setArchivoSet("N");
        }
        
        if(tipoFactura.equals("S")){
            if (cdcFactura == null){
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el CDC!"));
            } else {
                if (cdcFactura.length() != 44) {
                    saveNC = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "CDC Debe contener 44 dígitos!"));
                }
            }
        }
        else{
            if (timbradoFactura == null || timbradoFactura.trim().isEmpty()){
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el timbrado!"));
            }
            if (nroFactura == null || nroFactura.trim().isEmpty()){
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el número de factura!"));
            } else {
                if (!validarNroFactura()) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Formato de número de factura incorrecto. Deben ser en formato 9999999 o 999-999-9999999"));
                }
            }
            if (fechaFactura == null){
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la fecha de emisión de la factura!"));
            }
            
        }

        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
            listaDetalle.get(i).setNroNotaDebito(notaDebitoCreate.getNroNotaDebito());
        }

        setearDatosFactura();
        notaDebitoCreate.setNotaDebitoDetalles(listaDetalle);
        
        if (saveNC) {
            BodyResponse<NotaDebito> respuestaDeNC = serviceNotaDebito.createNotaDebito(notaDebitoCreate);

            if (respuestaDeNC.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "ND creada correctamente!"));
                this.nroNCcreada = notaDebitoCreate.getNroNotaDebito();
                this.idNcCreada = ((NotaDebito) respuestaDeNC.getPayload()).getId();
                if (obtenerConfiguracionGeneral("DESCARGAR_FACTURA").equalsIgnoreCase("S")) {
                    mostrarPanelDescarga();
                }
                try {
                    NotaDebito nd = (NotaDebito) respuestaDeNC.getPayload();
                    GeneracionDteNotaDebitoMultiempresa.generar(nd.getIdEmpresa(), nd.getId());
                } catch (Exception e) {
                    WebLogger.get().fatal(e);
                }
                init();
            }
        }

    }

     public void setearDatosFactura(){
         facturaFilter.setCdc(cdcFactura);
         FacturaAdapter adapterFact = new FacturaAdapter();
         adapterFact = adapterFact.fillData(facturaFilter);
         Integer idFact = null;
         if (!adapterFact.getData().isEmpty()){
             idFact = adapterFact.getData().get(0).getId();
         }
         for (NotaDebitoDetalle ncd : listaDetalle){
             ncd.setFacturaDigital(tipoFactura.charAt(0));
             if (tipoFactura.equals("S")){
                 ncd.setCdcFactura(cdcFactura);
                 ncd.setIdFactura(idFact);
             }
             else{
                 ncd.setTimbradoFactura(timbradoFactura);
                 ncd.setNroFactura(nroFactura);
                 ncd.setFechaFactura(fechaFactura);
             }
         }
     }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        idCliente = null;
        direccion = "";
        notaDebitoCreate = new NotaDebito();
        notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
        notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
        listSelectedFactura = new ArrayList<>();
        listaDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        datoNotaDebito = new DatosNotaDebito();
        
    }

    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
        this.listMoneda = new ArrayList<>();
        this.listMoneda = adapterMoneda.getData();
    }

    public void filterFacturasList() {
        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    public void clearFacturasList() {
        facturaFilter = new Factura();
        facturaFilter.setIdCliente(idCliente);
        facturaFilter.setCobrado("N");
        facturaFilter.setAnulado("N");
        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    /**
     * Método para obtener la factura en NC, cuando se redirecciona desde el
     * listado de facturas
     */
    public void obtenerFactura() {
        facturaFilter.setId(Integer.parseInt(idFactura));
        adapterFactura = adapterFactura.fillData(facturaFilter);
        listSelectedFactura.add(adapterFactura.getData().get(0));
        cliente.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());

        obtenerConfiguracion();

    }

    /**
     * Método para obtener datos de cabecera
     */
    public void obtenerDatosNCDevolucionFactura() {
        notaDebitoCreate.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());
        notaDebitoCreate.setIdNaturalezaCliente(adapterFactura.getData().get(0).getIdNaturalezaCliente());
        ruc = adapterFactura.getData().get(0).getRuc();
        idCliente = adapterFactura.getData().get(0).getIdCliente();
        notaDebitoCreate.setRuc(ruc);
        notaDebitoCreate.setIdCliente(idCliente);
        try {

            Map parametros = new HashMap();
            datoNotaDebito = new DatosNotaDebito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaDebitoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaDebitoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaDebito = serviceNotaDebito.getDatosNotaDebito(parametros);

            if (datoNotaDebito != null) {
                notaDebitoCreate.setFecha(today.getTime());
                notaDebitoCreate.setIdTalonario(datoNotaDebito.getIdTalonario());
                notaDebitoCreate.setNroNotaDebito(datoNotaDebito.getNroNotaDebito());

                //Validaciones
                if (datoNotaDebito.getTelefono() != null) {
                    notaDebitoCreate.setTelefono(datoNotaDebito.getTelefono());
                }
                if (datoNotaDebito.getEmail() != null) {
                    notaDebitoCreate.setEmail(datoNotaDebito.getEmail());
                }
                if (datoNotaDebito.getDireccion() != null) {
                    notaDebitoCreate.setDireccion(datoNotaDebito.getDireccion());
                }
                if (datoNotaDebito.getIdDepartamento() != null) {
                    notaDebitoCreate.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaDebito.getIdDepartamento());
                    //PrimeFaces.current().executeScript("initAutocomplete();");
                }
                if (datoNotaDebito.getIdDistrito() != null) {
                    notaDebitoCreate.setIdDistrito(datoNotaDebito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaDebito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaDebito.getIdDistrito());
                }
                if (datoNotaDebito.getIdCiudad() != null) {
                    notaDebitoCreate.setIdCiudad(datoNotaDebito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaDebito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaDebito.getIdCiudad());
                }
                if (datoNotaDebito.getNroCasa() != null) {
                    notaDebitoCreate.setNroCasa(datoNotaDebito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void devolucionDetalleNC() {
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();
        int nroBonificacion = 1;

        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(Integer.parseInt(idFactura));
        afp = afp.fillData(fdp);

        notaDebitoCreate.setIdNaturalezaCliente(afp.getData().get(0).getIdNaturalezaCliente());

        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {
            NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();

            if (listaDetalle.isEmpty()) {
                notaDebitoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaDebitoDetalle.setNroLinea(linea);
            }

            notaDebitoDetalle.setNroLinea(linea);
            notaDebitoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaDebitoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());

            notaDebitoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();

            notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);

            notaDebitoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaDebitoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaDebitoDetalle.setCantidad(facturaDetalle.getCantidad());
            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva().
                    subtract(facturaDetalle.getDescuentoParticularUnitario()).
                    subtract(facturaDetalle.getDescuentoGlobalUnitario());
            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);

            notaDebitoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaDebitoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaDebitoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaDebitoDetalle.setIdProducto(facturaDetalle.getIdProducto());

            listaDetalle.add(notaDebitoDetalle);
        }

        listSelectedFactura = new ArrayList<>();
        listSelectedFactura.add(afp.getData().get(0));

        calcularTotales();
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void obtenerMotivoEmisionInterno() {

        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));

    }

    public void obtenerMotivoEmision() {
        MotivoEmisionInternoAdapter motivoEmisionInternoAdapter = new MotivoEmisionInternoAdapter();
        MotivoEmisionInterno motivoEmisionInterno = new MotivoEmisionInterno();
        if (notaDebitoCreate.getIdMotivoEmisionInterno() != null) {
            motivoEmisionInterno.setId(notaDebitoCreate.getIdMotivoEmisionInterno());
            motivoEmisionInternoAdapter = motivoEmisionInternoAdapter.fillData(motivoEmisionInterno);
            motivoEmision = motivoEmisionInternoAdapter.getData().get(0).getMotivoEmision().getDescripcion();
        } else {
            this.motivoEmision = "Ninguno";
        }

    }

//    public void obtenerFacturasDeSolicitud() {
//        solicitudNC.setId(Integer.parseInt(idSolicitud));
//        adapterSolicitud = adapterSolicitud.fillData(solicitudNC);
//
//        cliente.setRazonSocial(adapterSolicitud.getData().get(0).getRazonSocial());
//
//        notaDebitoCreate.setRazonSocial(cliente.getRazonSocial());
//        ruc = adapterSolicitud.getData().get(0).getRuc();
//        idCliente = adapterSolicitud.getData().get(0).getIdCliente();
//        notaDebitoCreate.setRuc(ruc);
//        notaDebitoCreate.setIdCliente(idCliente);
//
//        facturaFilter.setId(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(0).getIdFacturaCompra());
//        adapterFactura = adapterFactura.fillData(facturaFilter);
//        listSelectedFactura.add(adapterFactura.getData().get(0));
//        obtenerDatosNC();
//        solicitudDetalleNC();
//    }

//    public void solicitudDetalleNC() {
//        if (show == false) {
//            show = true;
//        }
//
//        for (int i = 0; i < adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().size(); i++) {
//            NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();
//
//            int nroBonificacion;
//            if (listaDetalle.isEmpty()) {
//                notaDebitoDetalle.setNroLinea(linea);
//                nroBonificacion = linea;
//            } else {
//                linea++;
//                nroBonificacion = linea;
//                notaDebitoDetalle.setNroLinea(linea);
//            }
//
//            notaDebitoDetalle.setIdFactura(listSelectedFactura.get(0).getId());
//            String nroFactura = listSelectedFactura.get(0).getNroFactura();
//            notaDebitoDetalle.setDescripcion("BONIFICACION " + nroBonificacion + " DE LA FACTURA NRO." + nroFactura);
//            notaDebitoDetalle.setPorcentajeIva(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getPorcentajeIva());
//            notaDebitoDetalle.setMontoImponible(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getMontoImponible());
//            notaDebitoDetalle.setCantidad(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getCantidad());
//            notaDebitoDetalle.setPrecioUnitarioConIva(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getPrecioUnitarioConIva());
//            notaDebitoDetalle.setPrecioUnitarioSinIva(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getPrecioUnitarioSinIva());
//            notaDebitoDetalle.setMontoIva(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getMontoIva());
//            notaDebitoDetalle.setMontoTotal(adapterSolicitud.getData().get(0).getSolicitudNotaDebitoDetalles().get(i).getMontoTotal());
//
//            listaDetalle.add(notaDebitoDetalle);
//        }
//
//        calcularTotales();
//    }

    public Factura filtrarFactura(Integer idFactura) {

        FacturaAdapter adapterFactura = new FacturaAdapter();
        Factura facturaFilter = new Factura();
        facturaFilter.setId(idFactura);

        adapterFactura = adapterFactura.fillData(facturaFilter);

        return adapterFactura.getData().get(0);
    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param rowKey
     */
    public void deleteDetalle(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        info = listaDetalle.get(nroLinea);
        subTotal = info.getMontoTotal();
        listaDetalle.remove(info);
        
        //actualizar numero de linea del detalle
        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
        }
        linea = listaDetalle.size();
        
        BigDecimal monto = notaDebitoCreate.getMontoTotalNotaDebito();
        BigDecimal totalNC = monto.subtract(subTotal);
        notaDebitoCreate.setMontoTotalNotaDebito(totalNC);
        obtenerMontoTotalLetras();

    }

    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:distrito");
        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    public void filterDepartamento() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
    public void addDetalle(){
        NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();
        if (listaDetalle.isEmpty()) {
            notaDebitoDetalle.setNroLinea(1);
        } else {
            linea++;
            notaDebitoDetalle.setNroLinea(linea);
            
        }

        notaDebitoDetalle.setMontoTotal(BigDecimal.ZERO);
        notaDebitoDetalle.setDescuentoParticularUnitario(BigDecimal.ZERO);
        notaDebitoDetalle.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
        notaDebitoDetalle.setMontoDescuentoParticular(BigDecimal.ZERO);
        notaDebitoDetalle.setPorcentajeIva(5);
        listaDetalle.add(notaDebitoDetalle);
        calcularTotales();
    }

    
    public void onChangeMoneda() {
        this.adapterTipoCambio = new TipoCambioAdapter();
        tipoCambioFilter.setIdMoneda(notaDebitoCreate.getIdMoneda());
        adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);

        if (notaDebitoCreate.getIdMoneda() != null && notaDebitoCreate.getIdMoneda() != 1){
            habilitarPanelCotizacion = true;
        } else {
            habilitarPanelCotizacion = false;
            notaDebitoCreate.setIdTipoCambio(null);
        }
    }
    
    
    public void onCheckCotizacion(SelectEvent event) {
        TipoCambio selected = (TipoCambio) event.getObject();
        notaDebitoCreate.setIdTipoCambio(selected.getId()); 
    }
    
    public void onUncheckCotizacion(UnselectEvent event) {
        notaDebitoCreate.setIdTipoCambio(null); 
    }
    
    /**
     * Método para descargar el pdf de Nota de Crédito
     * @return 
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroNCcreada + ".pdf";
        String url = "nota-debito/consulta/" + idNcCreada;
        Map param = new HashMap();
        if (this.digital.equalsIgnoreCase("S")){
             param.put("ticket", false);
             if (siediApi.equalsIgnoreCase("S")) {
                param.put("siediApi", true);
             } else {
                param.put("siediApi", false);
             }
        }
        byte[] data = fileServiceClient.downloadNotaDebito(url,param);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }
    
    /**
     * Método para obtener la configuración
     */
    public String obtenerConfiguracionGeneral(String codigo) {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "N";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }
    
    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");
    }
    
    public void descuentoListener(Integer nroLinea, Integer param){
        calcularDescuentoItem(nroLinea,tipoDescuento);
        calcularMontos(nroLinea);
        calcularDescuentos(2);
        
    }
    
    
     /**
     * Método para calcular el descuento de un item del detalle
     * @param nroLinea
     * @param param 
     */
    public void calcularDescuentoItem(Integer nroLinea, Integer param) {
        NotaDebitoDetalle info = null;
        info = listaDetalle.get(nroLinea);
        
        if (info != null) {
            BigDecimal descuentoItemAux = info.getDescuentoParticularUnitarioAux();
            BigDecimal descuentoItem = info.getDescuentoParticularUnitario();
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();

            if (param == 1) {
                if (info.getPrecioUnitarioConIva() != null && info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == 1) {
                    //valida que el porcentaje de descuento ingresado sea mayor o igual a 0 y menor que 100
                    if (descuentoItemAux != null && descuentoItem != null && ((descuentoItem.compareTo(BigDecimal.ZERO) == 1 && descuentoItem.compareTo(new BigDecimal("100")) == -1) || (descuentoItem.compareTo(BigDecimal.ZERO) == 0)) ) {                
                        BigDecimal porcentajeDescuento = descuentoItemAux.divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
                        montoDescuentoItem = info.getPrecioUnitarioConIva().multiply(porcentajeDescuento);
                        info.setMontoDescuentoParticular(montoDescuentoItem);
                        info.setDescuentoParticularUnitario(descuentoItemAux);
                    } else {
                        info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                        info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                        info.setMontoDescuentoParticular(BigDecimal.ZERO);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje de descuento debe ser entre 0 y 100."));
                    }
                }   
            } else if (param == 2) {
                //valida que el montoDescuento no sea nulo, menor a cero o mayor al precio unitario
                if (montoDescuentoItem != null && ((( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1 ) ||( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 0 )) && (montoDescuentoItem.compareTo(info.getPrecioUnitarioConIva())== -1))){
                    BigDecimal aux = info.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                    descuentoItem = aux.divide(info.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                    info.setDescuentoParticularUnitario(descuentoItem);
                    info.setDescuentoParticularUnitarioAux(descuentoItem);
                } else {
                    info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                    info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                    info.setMontoDescuentoParticular(BigDecimal.ZERO);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto de descuento no puede ser menor a 0 o mayor al precio unitario con IVA"));
                }
            }
        }
    }
    
    
    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        producto = new Producto();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto.setPageSize(100);
        adapterProducto = adapterProducto.fillData(producto);
        producto = new Producto();

        return adapterProducto.getData();
    }

    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea + 1;
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            idProducto = ((Producto) event.getObject()).getId();
            descripcion = ((Producto) event.getObject()).getDescripcion();
            porcentaje = ((Producto) event.getObject()).getPorcentajeImpuesto();
            loteProducto = ((Producto) event.getObject()).getNroLote();
            Date fechaVencLote = ((Producto) event.getObject()).getFechaVencimientoLote();
            if (fechaVencLote != null ) {
                fechaVencimientoLote = sdf.format(((Producto) event.getObject()).getFechaVencimientoLote());
            } else {
                fechaVencimientoLote = null;
            }   

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para agregar el producto con validaciones
     */
    public void agregarProducto() {
        boolean save = true;
        NotaDebitoDetalle info = new NotaDebitoDetalle();

        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), idProducto)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }

         if (save == true) {
            WebLogger.get().debug("AGREGAR DATOS DETALLE");
            info.setDescripcion(descripcion);
            info.setIdProducto(idProducto);
            info.setLote(loteProducto);
            info.setFechaVencimientoLote(fechaVencimientoLote);
         
        }
    }
    
    
    @PostConstruct
    public void init() {
        this.facturaSeleccionada = new Factura();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.direccionFilter = new Direccion();
        this.listaDetalle = new ArrayList();

        this.habilitarSeleccion = true;
        create = false;
        show = false;
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.adapterNCTalonario = new NotaDebitoTalonarioAdapter();
        this.ncTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();

        showDatatableSolicitud = false;

        this.adapterFactura = new FacturaMontoAdapter();
        this.facturaFilter = new Factura();
        this.serviceNotaDebito = new NotaDebitoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new ClientePojo();
        this.adapterCliente = new ClientPojoAdapter();

//        this.adapterSolicitud = new SolicitudNotaDebitoListAdapter();
//        this.solicitudNC = new SolicitudNotaDebito();

        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();

        this.notaDebitoCreate = new NotaDebito();

        this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
        this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());

        filterMoneda();
        obtenerMotivoEmisionInterno();

        this.motivoEmision = "Ninguno";
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;

        this.tieneDireccion = "N";
        this.tipoFactura = "S";

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("id") != null) {
            idFactura = params.get("id");
            showDatatable = true;
            this.habilitarSeleccion = false;
            obtenerFactura();
        } else {
            obtenerConfiguracion();
        }

//        if (params.get("idSolicitud") != null) {
//            idSolicitud = params.get("idSolicitud");
//            showDatatable = true;
//            this.habilitarSeleccion = false;
//            obtenerFacturasDeSolicitud();
//
//        }
        adapterTipoCambio = new TipoCambioAdapter();
        this.tipoCambioFilter = new TipoCambio();
        filterDepartamento();
        filterDistrito();
        filterCiudad();
        fileServiceClient = new FileServiceClient();
        this.siediApi = obtenerConfiguracionGeneral("ARCHIVOS_DTE_SIEDI_API");
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.tipoDescuento = 1;
    }

}
