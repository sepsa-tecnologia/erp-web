/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.utils;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.pojos.ParamsReader;

/**
 * Clase utilizada para el manejo de envio de mail
 *
 * @author Rubén Ortiz
 */
public class MailUtils {
    /**
     * Sesión utilizada para el envío de mail
     */
    private static final Session SESSION;
    /**
     * Inicializa los valores para el envío de mail
     */
    static {
        Properties props= new Properties();
        props.setProperty("mail.smtp.host", ParamsReader.PARAMS.getMailServer());
        props.setProperty("mail.smtp.port", String.valueOf(ParamsReader.PARAMS.getMailPort()));
        props.setProperty("mail.smtp.user", ParamsReader.PARAMS.getMailAlert());
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        SESSION= Session.getDefaultInstance(props, null);
    }
    /**
     * Envía mensajes vía mail
     * @param msn Mensaje del mail
     * @param subject Asunto del mail
     * @param to Destinatario del mensaje
     * @param contentType Tipo de contenido del mensaje
     */
    public static void sendMail(String msn, String subject, String to, String contentType) {
        try {
            MimeMessage mMessage= new MimeMessage(SESSION);
            mMessage.setSubject(subject);
            
            mMessage.setFrom(new InternetAddress(ParamsReader.PARAMS.getMailAlert()));
            mMessage.setContent(msn, contentType);
            StringTokenizer token= new StringTokenizer(to, ";");
            
            while(token.hasMoreTokens()) {
                mMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(token.nextToken()));
            }
            
            Transport t= SESSION.getTransport("smtp");
            t.connect(ParamsReader.PARAMS.getMailAlert(), ParamsReader.PARAMS.getMailPass());
            t.sendMessage(mMessage, mMessage.getAllRecipients());
            t.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
            WebLogger.get().error("No se ha podido envíar el mail de alerta debido al siguiente error "+ex);
        }
    }
}
