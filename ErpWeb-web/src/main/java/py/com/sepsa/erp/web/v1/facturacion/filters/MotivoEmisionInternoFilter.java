/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para motivo emision interno
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionInternoFilter extends Filter {

    /**
     *
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     *
     */
    public MotivoEmisionInternoFilter id(Integer id) {
        
        if (id != null) {
            
            params.put("id", id);
            
        }
        
        return this;
        
    }

    /**
     *
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     *
     */
    public MotivoEmisionInternoFilter codigo(String codigo) {
        
        if (codigo != null) {
            
            params.put("codigo", codigo);
            
        }
        
        return this;
        
    }

    /**
     *
     * Agrega el filtro descripcion
     *
     * @param descripcion
     * @return
     *
     */
    public MotivoEmisionInternoFilter descripcion(String descripcion) {
        
        if (descripcion != null) {
            
            params.put("descripcion", descripcion);
            
        }
        return this;
    }

    /**
     * Agrega el filtro activo
     *
     * @param activo
     * @return
     */
    public MotivoEmisionInternoFilter activo(String activo) {
        
        if (activo != null) {
            
            params.put("activo", activo);
            
        }
        return this;
    }

    /**
     *
     * Agrega el filtro por idMotivoEmision
     *
     * @param idMotivoEmision
     * @return
     *
     */
    public MotivoEmisionInternoFilter idMotivoEmision(Integer idMotivoEmision) {
        
        if (idMotivoEmision != null) {
            
            params.put("idMotivoEmision", idMotivoEmision);
            
        }
        
        return this;
        
    }

    /**
     *
     * Agrega el filtro por motivoEmision
     *
     * @param motivoEmision
     * @return
     *
     */
    public MotivoEmisionInternoFilter motivoEmision(Integer motivoEmision) {
        
        if (motivoEmision != null) {
            params.put("motivoEmision", motivoEmision);
        }
        
        return this;
        
    }

    /**
     *
     * Construye el mapa de parámetros
     *
     *
     *
     * @param motivoEmisionInterno datos del filtro
     *
     * @param page página buscada en el resultado
     *
     * @param pageSize tamaño de la pagina de resultado
     *
     * @return
     *
     */
    public static Map build(MotivoEmisionInterno motivoEmisionInterno, Integer page, Integer pageSize) {
        
        MotivoEmisionInternoFilter filter = new MotivoEmisionInternoFilter();
        
        filter
                .id(motivoEmisionInterno.getId())
                .codigo(motivoEmisionInterno.getCodigo())
                .descripcion(motivoEmisionInterno.getDescripcion())
                .activo(motivoEmisionInterno.getActivo())
                .idMotivoEmision(motivoEmisionInterno.getIdMotivoEmision())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
        
    }

    /**
     *
     * Constructor de la clase
     *
     */
    public MotivoEmisionInternoFilter() {
        
        super();
        
    }
    
}
