/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CargoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmailListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Cargo;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.PersonaContactoServiceClient;

/**
 * Controlador para crear contacto
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("contactoCreate")
public class ContactoCreateController implements Serializable {

    /**
     * Cliente para el servicio personaContacto
     */
    private final PersonaContactoServiceClient service;
    /**
     * POJO de personacontacto
     */
    private PersonaContacto personaContacto;
    /**
     * POJO del contacto
     */
    private Contacto contacto;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Adaptador para la lista de tipo contacto
     */
    private TipoContactoListAdapter tipoContactoAdapterList;
    /**
     * POJO de tipo contacto
     */
    private TipoContacto tipoContacto;
    /**
     * Adaptador para la lista de personas
     */
    private PersonaListAdapter personaAdapterList;
    /**
     * POJO de persona
     */
    private Persona persona;
    /**
     * Adaptador para la lista de cargos
     */
    private CargoListAdapter cargoAdapterList;
    /**
     * POJO de cargo
     */
    private Cargo cargo;

    /**
     * Números de telefonos agregados
     */
    private String numerosTel = "";
    /**
     * Email agregados
     */
    private String mails = "";

    /**
     * Objeto PersonaTeléfono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Objeto PersonaEmail
     */
    private PersonaEmail personaEmail;

    /**
     * Lista de tipo de telefonos
     */
    private List<SelectItem> listTipoTelefono;
    /**
     * Lista de tipo de email
     */
    private List<SelectItem> listTipoEmail;

    /**
     * Adaptador de la lista tipo telefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefono;
    /**
     * Adaptador de la lista tipo email
     */
    private TipoEmailListAdapter adapterTipoEmail;

    /**
     * Datos del tipo telefono
     */
    private TipoTelefono tipoTelefono;
    /**
     * Datos del tipo email
     */
    private TipoEmail tipoEmail;

    /**
     * Tipo de telefono seleccionado
     */
    private Integer selectedTel = 0;
    /**
     * Tipo de Email seleccionado
     */
    private Integer selectedEmail = 0;
    /**
     * Número del prefijo
     */
    private String prefijo;
    /**
     * Número de telefono
     */
    private String tel;
    /**
     * email del contacto
     */
    private String email;

    /**
     * Variable booleana para principal telefono
     */
    private boolean principal = false;
    /**
     * Variabla boolean principal para email
     */
    private boolean principalEmail = false;
    /**
     * Lista de telefono
     */
    private List<Telefono> listTelefono = new ArrayList();
    /**
     * Lista de Email
     */
    private List<Email> listEmails = new ArrayList();

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the contacto
     */
    public Contacto getContacto() {
        return contacto;
    }

    /**
     * @param contacto the contacto to set
     */
    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    /**
     * @return the tipoContactoAdapterList
     */
    public TipoContactoListAdapter getTipoContactoAdapterList() {
        return tipoContactoAdapterList;
    }

    /**
     * @return the tipoContacto
     */
    public TipoContacto getTipoContacto() {
        return tipoContacto;
    }

    /**
     * @param tipoContacto the tipoContacto to set
     */
    public void setTipoContacto(TipoContacto tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    /**
     * @param tipoContactoAdapterList the tipoContactoAdapterList to set
     */
    public void setTipoContactoAdapterList(TipoContactoListAdapter tipoContactoAdapterList) {
        this.tipoContactoAdapterList = tipoContactoAdapterList;
    }

    /**
     * @return the personaAdapterList
     */
    public PersonaListAdapter getPersonaAdapterList() {
        return personaAdapterList;
    }

    /**
     * @param personaAdapterList the personaAdapterList to set
     */
    public void setPersonaAdapterList(PersonaListAdapter personaAdapterList) {
        this.personaAdapterList = personaAdapterList;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * @return the cargoAdapterList
     */
    public CargoListAdapter getCargoAdapterList() {
        return cargoAdapterList;
    }

    /**
     * @param cargoAdapterList the cargoAdapterList to set
     */
    public void setCargoAdapterList(CargoListAdapter cargoAdapterList) {
        this.cargoAdapterList = cargoAdapterList;
    }

    /**
     * @return the cargo
     */
    public Cargo getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public TipoTelefonoListAdapter getAdapterTipoTelefono() {
        return adapterTipoTelefono;
    }

    public void setAdapterTipoTelefono(TipoTelefonoListAdapter adapterTipoTelefono) {
        this.adapterTipoTelefono = adapterTipoTelefono;
    }

    public TipoTelefono getTipoTelefono() {
        return tipoTelefono;
    }

    public void setTipoTelefono(TipoTelefono tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    public Integer getSelectedTel() {
        return selectedTel;
    }

    public void setSelectedTel(Integer selectedTel) {
        this.selectedTel = selectedTel;
    }

    public String getNumerosTel() {
        return numerosTel;
    }

    public void setNumerosTel(String numerosTel) {
        this.numerosTel = numerosTel;
    }

    public String getMails() {
        return mails;
    }

    public void setMails(String mails) {
        this.mails = mails;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public PersonaEmail getPersonaEmail() {
        return personaEmail;
    }

    public void setPersonaEmail(PersonaEmail personaEmail) {
        this.personaEmail = personaEmail;
    }

    public List<SelectItem> getListTipoTelefono() {
        return listTipoTelefono;
    }

    public void setListTipoTelefono(List<SelectItem> listTipoTelefono) {
        this.listTipoTelefono = listTipoTelefono;
    }

    public List<SelectItem> getListTipoEmail() {
        return listTipoEmail;
    }

    public void setListTipoEmail(List<SelectItem> listTipoEmail) {
        this.listTipoEmail = listTipoEmail;
    }

    public TipoEmailListAdapter getAdapterTipoEmail() {
        return adapterTipoEmail;
    }

    public void setAdapterTipoEmail(TipoEmailListAdapter adapterTipoEmail) {
        this.adapterTipoEmail = adapterTipoEmail;
    }

    public TipoEmail getTipoEmail() {
        return tipoEmail;
    }

    public void setTipoEmail(TipoEmail tipoEmail) {
        this.tipoEmail = tipoEmail;
    }

    public Integer getSelectedEmail() {
        return selectedEmail;
    }

    public void setSelectedEmail(Integer selectedEmail) {
        this.selectedEmail = selectedEmail;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    public boolean isPrincipalEmail() {
        return principalEmail;
    }

    public void setPrincipalEmail(boolean principalEmail) {
        this.principalEmail = principalEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Telefono> getListTelefono() {
        return listTelefono;
    }

    public void setListTelefono(List<Telefono> listTelefono) {
        this.listTelefono = listTelefono;
    }

    public List<Email> getListEmails() {
        return listEmails;
    }

    public void setListEmails(List<Email> listEmails) {
        this.listEmails = listEmails;
    }

    public PersonaContacto getPersonaContacto() {
        return personaContacto;
    }

    public void setPersonaContacto(PersonaContacto personaContacto) {
        this.personaContacto = personaContacto;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    //</editor-fold>
    /**
     * Método para crear contacto
     */
    public void create() {

        for (int i = 0; i < listTelefono.size(); i++) {
            if (!listTelefono.get(i).getNumero().equals("")) {
                PersonaTelefono pTel = new PersonaTelefono();
                pTel.setTelefono(listTelefono.get(i));
                pTel.setActivo("S");
                persona.getPersonaTelefonos().add(pTel);
            }
        }

        for (int i = 0; i < listEmails.size(); i++) {
            if (!listEmails.get(i).getEmail().trim().isEmpty()) {
                PersonaEmail pEmail = new PersonaEmail();
                pEmail.setEmail(listEmails.get(i));
                pEmail.setActivo("S");
                persona.getPersonaEmails().add(pEmail);
            }
        }
        persona.setActivo("S");
        persona.setCodigoTipoPersona("FISICA");
        contacto.setPersona(persona);
        personaContacto.setActivo("S");
        personaContacto.setContacto(contacto);

        BodyResponse<PersonaContacto> respuestaTal = service.setPersonaContacto(personaContacto);

        if (respuestaTal.getSuccess()) {
            this.personaContacto = new PersonaContacto();
            this.limpiarCampos();
            init();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contacto creado correctamente!"));
        }

    }

    /**
     * Metodo para vaciar la lista de emails y telefonos
     */
    public void limpiarCampos() {
        this.listTelefono.removeAll(listTelefono);
        this.listEmails.removeAll(listEmails);
        this.numerosTel = "";
        this.mails = "";
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoContactos() {
        this.setTipoContactoAdapterList(getTipoContactoAdapterList().fillData(getTipoContacto()));
    }

    /**
     * Método para obtener local
     */
    public void getCargos() {
        this.setCargoAdapterList(getCargoAdapterList().fillData(getCargo()));
    }

    /**
     * Método para obtener tipo telefono y añadir a la listaTipoTelefono
     */
    public void getTipoTelefonos() {
        this.setAdapterTipoTelefono(getAdapterTipoTelefono().fillData(getTipoTelefono()));
        for (int i = 0; i < adapterTipoTelefono.getData().size(); i++) {
            listTipoTelefono.add(new SelectItem(adapterTipoTelefono.getData().get(i).getId(),
                    adapterTipoTelefono.getData().get(i).getCodigo()));
        }
        if (!listTipoTelefono.isEmpty()) {
            selectedTel = (Integer) listTipoTelefono.get(0).getValue();
        }
    }

    /**
     * Método para obtener tipo Email y añadir a la lista de tipo email
     */
    public void getTipoEmails() {
        this.setAdapterTipoEmail(getAdapterTipoEmail().fillData(getTipoEmail()));
        for (int i = 0; i < adapterTipoEmail.getData().size(); i++) {
            listTipoEmail.add(new SelectItem(adapterTipoEmail.getData().get(i).getId(),
                    adapterTipoEmail.getData().get(i).getDescripcion()));
        }
        if (!listTipoEmail.isEmpty()) {
            selectedEmail = (Integer) listTipoEmail.get(0).getValue();
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.personaContacto.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Método para agregar el telefono a la lista
     */
    public void addTelefono() {

        numerosTel = ""; // la lista que guarda los numeros en el popup
        Telefono telef = new Telefono();
        TipoTelefono tipoTel = new TipoTelefono();
        tipoTel.setId(selectedTel);
        tipoTel.setCodigo(getDescTipoTelefono(selectedTel).getCodigo());
        telef.setTipoTelefono(tipoTel);
        telef.setIdTipoTelefono(tipoTel.getId());
        telef.setCodigoTipoTelefono(tipoTel.getCodigo());
        telef.setPrefijo(prefijo);
        telef.setNumero(tel);

        if (principal == true) {
            telef.setPrincipal("S");
        } else {
            telef.setPrincipal("N");
        }

        int id = listTelefono.size();
        telef.setId(id);

        if (!telef.getPrefijo().isEmpty() && !telef.getNumero().isEmpty()) {
            listTelefono.add(telef); // se agrega a la lista el telefono del popup
            //  telefono = "";
            principal = false;
            for (int i = 0; i < listTelefono.size(); i++) {
                numerosTel = numerosTel + "(" + listTelefono.get(i).getPrefijo() + ")" + listTelefono.get(i).getNumero() + ";";
            }

            prefijo = "";
            tel = "";

        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe completar el prefijo y número");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }

    }

    /**
     * Borra los telefonos de la lista
     */
    public void delTelefono() {

        numerosTel = "";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String parametroObtenido = (String) params.get("idTel");
        for (Telefono tel : listTelefono) {
            if (tel.getId() == Integer.parseInt(parametroObtenido)) {
                listTelefono.remove(tel);
                break;
            }
        }
        for (int i = 0; i < listTelefono.size(); i++) {
            numerosTel = numerosTel + "(" + listTelefono.get(i).getPrefijo() + ")" + listTelefono.get(i).getNumero() + ";";
        }
    }

    /**
     * Descripción del tipo teléfono
     *
     * @param idTipo
     * @return
     */
    public TipoTelefono getDescTipoTelefono(Integer idTipo) {
        tipoTelefono.setId(idTipo);
        adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefono);

        return adapterTipoTelefono.getData().get(0);
    }

    /**
     * Método para agregar el email
     */
    public void addMail() {

        Email em = new Email();
        int id = adapterTipoEmail.getData().size();
        em.setId(id);
        em.setIdTipoEmail(selectedEmail);
        em.setCodigoTipoEmail(getDescTipoEmail(selectedEmail));
        em.setEmail(email);
        if (principalEmail == true) {
            em.setPrincipal("S");
        } else {
            em.setPrincipal("N");
        }
        if (!em.getEmail().isEmpty()) {
            listEmails.add(em);
            mails = "";
            for (int i = 0; i < listEmails.size(); i++) {
                mails = mails + listEmails.get(i).getEmail() + ";";
            }
            email = "";
            principalEmail = false;
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe completar el email");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Método para borrar de la lista el email
     */
    public void delMail() {

        mails = "";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String parametro = (String) params.get("idMail");
        for (Email email : listEmails) {
            if (email.getId() == Integer.parseInt(parametro)) {
                listEmails.remove(email);
                break;
            }
        }
        for (int i = 0; i < listEmails.size(); i++) {
            mails = mails + listEmails.get(i).getEmail();
        }
    }

    /**
     * Descripción del tipo de email
     *
     * @param idTipo
     * @return
     */
    public String getDescTipoEmail(Integer idTipo) {
        tipoEmail.setId(idTipo);
        adapterTipoEmail = adapterTipoEmail.fillData(tipoEmail);

        return adapterTipoEmail.getData().get(0).getCodigo();
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.setContacto(new Contacto());
        this.setPersonaAdapterList(new PersonaListAdapter());
        this.setPersona(new Persona());
        this.setTipoContactoAdapterList(new TipoContactoListAdapter());
        this.setTipoContacto(new TipoContacto());
        this.setCargoAdapterList(new CargoListAdapter());
        this.setCargo(new Cargo());
        this.personaContacto = new PersonaContacto();
        this.personaTelefono = new PersonaTelefono();
        this.personaEmail = new PersonaEmail();
        this.adapterTipoTelefono = new TipoTelefonoListAdapter();
        this.tipoTelefono = new TipoTelefono();
        this.adapterTipoEmail = new TipoEmailListAdapter();
        this.tipoEmail = new TipoEmail();
        this.listTipoTelefono = new ArrayList();
        this.listTipoEmail = new ArrayList();
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        getCargos();
        getTipoContactos();
        getTipoTelefonos();
        getTipoEmails();
    }

    /**
     * Constructor
     */
    public ContactoCreateController() {
        init();
        this.service = new PersonaContactoServiceClient();
    }

}
