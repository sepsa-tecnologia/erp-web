/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 *
 * @author Ralf Adam
 */
public class Permisos {
    
    private PermisoListarFactura listarFactura;
    private PermisoListarNotaCredito listarNotaCredito;
    private PermisoListarNotaDebito listarNotaDebito;
    private PermisoListarCobro listarCobro;
    private PermisoListarNotaRemision listarNotaRemision;
    
    
    public Permisos(){
    }

    public PermisoListarFactura getListarFactura() {
        return listarFactura;
    }

    public void setListarFactura(PermisoListarFactura listarFactura) {
        this.listarFactura = listarFactura;
    }

    public PermisoListarNotaCredito getListarNotaCredito() {
        return listarNotaCredito;
    }

    public void setListarNotaCredito(PermisoListarNotaCredito listarNotaCredito) {
        this.listarNotaCredito = listarNotaCredito;
    }

    public PermisoListarNotaDebito getListarNotaDebito() {
        return listarNotaDebito;
    }

    public void setListarNotaDebito(PermisoListarNotaDebito listarNotaDebito) {
        this.listarNotaDebito = listarNotaDebito;
    }

    public PermisoListarCobro getListarCobro() {
        return listarCobro;
    }

    public void setListarCobro(PermisoListarCobro listarCobro) {
        this.listarCobro = listarCobro;
    }

    public PermisoListarNotaRemision getListarNotaRemision() {
        return listarNotaRemision;
    }

    public void setListarNotaRemision(PermisoListarNotaRemision listarNotaRemision) {
        this.listarNotaRemision = listarNotaRemision;
    }
}
