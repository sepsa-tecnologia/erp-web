package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;

/**
 * Filtro para la entidad Financiera
 *
 * @author Cristina Insfrán
 */
public class EntidadFinancieraFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public EntidadFinancieraFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripción
     *
     * @param descripcion
     * @return
     */
    public EntidadFinancieraFilter razonSocial(String razonSocial) {
        if (razonSocial != null) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripción
     *
     * @param codigo
     * @return
     */
    public EntidadFinancieraFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripción
     *
     * @param estado
     * @return
     */
    public EntidadFinancieraFilter estado(Character estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param entidad
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(EntidadFinanciera entidad, Integer page, Integer pageSize) {
        EntidadFinancieraFilter filter = new EntidadFinancieraFilter();

        filter
                .id(entidad.getId())
                .razonSocial(entidad.getRazonSocial())
                .codigo(entidad.getCodigo())
                .estado(entidad.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de EntidadFinancieraFilter
     */
    public EntidadFinancieraFilter() {
        super();
    }

}
