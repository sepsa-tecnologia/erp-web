package py.com.sepsa.erp.web.v1.info.pojos;

import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;

/**
 * Pojo para listado de procesamiento de archivo
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class ProcesamientoArchivo {

    /**
     * Identificador de la entidad
     */
    private Integer id;
    /**
     * Parámetro nombreArchivo
     */
    private String nombreArchivo;
    /**
     * Parámetro tamaño
     */
    private Integer tamano;
    /**
     * Parámetro fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * Parámetro fechaInsercionDesde
     */
    private Date fechaInsercionDesde;
    /**
     * Parámetro fechaInsercionHasta
     */
    private Date fechaInsercionHasta;
    /**
     * Parámetro hash
     */
    private String hash;
    /**
     * Parámetro idEstado
     */
    private Integer idEstado;
    /**
     * Parámetro idEmpresa
     */
    private Integer idEmpresa;
    /**
     * Parámetro idTipoArchivo
     */
    private Integer idTipoArchivo;
    /**
     * Parámetro idTipoDocumento
     */
    private Integer idTipoDocumento;
    /**
     * Pojo de tipo Documento
     */
    private TipoDocumento tipoDocumento;
    /**
     * Parámetro idLocalOrigen
     */
    private Integer idLocalOrigen;
    /**
     * Parámetro idLocalDestino
     */
    private Integer idLocalDestino;
    /**
     * Parámetro confirmado
     */
    private String confirmado;
    /**
     * Parámetro reenviado
     */
    private String reenviado;
    /**
     * Parámetro rutaArchivoErroneo
     */
    private String rutaArchivoErroneo;
    /**
     * Pojo de tipoArchivo
     */
    private TipoArchivo tipoArchivo;
    /**
     * Pojo de localOrigen
     */
    private Local localOrigen;
    /**
     * Pojo de localDestino
     */
    private Local localDestino;
    /**
     * Pojo de estado
     */
    private Estado estado;
    /**
     * Numero de Documento
     */
    private String nroDocumento;
    /**
     * Detalle de procesamiento de archivo
     */
    private List<ProcesamientoArchivoDetalle> procesamientoArchivoDetalles;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Integer getTamano() {
        return tamano;
    }

    public void setTamano(Integer tamano) {
        this.tamano = tamano;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public String getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(String confirmado) {
        this.confirmado = confirmado;
    }

    public String getRutaArchivoErroneo() {
        return rutaArchivoErroneo;
    }

    public void setRutaArchivoErroneo(String rutaArchivoErroneo) {
        this.rutaArchivoErroneo = rutaArchivoErroneo;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public TipoArchivo getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(TipoArchivo tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public List<ProcesamientoArchivoDetalle> getProcesamientoArchivoDetalles() {
        return procesamientoArchivoDetalles;
    }

    public void setProcesamientoArchivoDetalles(List<ProcesamientoArchivoDetalle> procesamientoArchivoDetalles) {
        this.procesamientoArchivoDetalles = procesamientoArchivoDetalles;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public String getReenviado() {
        return reenviado;
    }

    public void setReenviado(String reenviado) {
        this.reenviado = reenviado;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    //</editor-fold>
    public ProcesamientoArchivo() {

    }

}
