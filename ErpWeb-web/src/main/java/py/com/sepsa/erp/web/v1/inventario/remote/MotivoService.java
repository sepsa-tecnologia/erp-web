/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.MotivoFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Motivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoService extends APIErpCore {

    /**
     * Obtiene la lista
     *
     * @param motivoEmision Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public MotivoAdapter getMotivo(Motivo motivoEmision, Integer page,
            Integer pageSize) {
        MotivoAdapter lista = new MotivoAdapter();
        try {
            Map params = MotivoFilter.build(motivoEmision, page, pageSize);
            HttpURLConnection conn = GET(Resource.MOTIVO.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        MotivoAdapter.class);
                lista = (MotivoAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param motivoEmision
     * @return
     */
    public BodyResponse<Motivo> setMotivo(Motivo motivoEmision) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MOTIVO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoEmision));
            response = BodyResponse.createInstance(conn, Motivo.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param motivoEmision
     * @return
     */
    public BodyResponse<Motivo> editMotivo(Motivo motivoEmision) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.MOTIVO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(motivoEmision));
            response = BodyResponse.createInstance(conn, Motivo.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Motivo get(Integer id) {
        Motivo data = new Motivo(id);
        MotivoAdapter list = getMotivo(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public MotivoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        MOTIVO("Motivo", "motivo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
