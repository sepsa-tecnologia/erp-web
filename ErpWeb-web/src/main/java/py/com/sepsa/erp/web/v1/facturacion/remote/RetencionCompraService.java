package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionCompraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.RetencionCompraFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionCompra;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de retención de compra
 *
 * @author alext
 */
public class RetencionCompraService extends APIErpFacturacion {

    public RetencionCompraListAdapter getRetencionCompraList(RetencionCompra retencionCompra, Integer page, Integer pageSize) {

        RetencionCompraListAdapter lista = new RetencionCompraListAdapter();

        Map params
                = RetencionCompraFilter.build(retencionCompra, page, pageSize);

        HttpURLConnection conn = GET(Resource.RETENCION_COMPRA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    RetencionCompraListAdapter.class);

            lista = (RetencionCompraListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     *
     * @param id identificador de Retencion
     * @return
     */
    public Boolean deleteRetencionCompra(Integer id) {
        BodyResponse response = new BodyResponse();
        String URL = "retencion-compra/" + id;
        HttpURLConnection conn = DELETE(URL, ContentType.JSON);
        if (conn != null) {

            response = BodyResponse.createInstance(conn, RetencionCompra.class);
        }
        return response.getSuccess();
    }

    /**
     * Constructor de la clase
     */
    public RetencionCompraService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        RETENCION_COMPRA("Lista de retencion", "retencion-compra");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
