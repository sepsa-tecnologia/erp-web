package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoEmailNotificacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoEmailNotificacionAsociadoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoEmailNotificacionFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmailNotificacion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de contacto email
 *
 * @author Romina Núñez
 */
public class ContactoEmailNotificacionService extends APIErpCore {

    /**
     * Obtiene la lista de contacto
     *
     * @param contactoEmailNotificacion
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoEmailNotificacionAdapter getContactoEmailNotificacionList(ContactoEmailNotificacion contactoEmailNotificacion, Integer page,
            Integer pageSize) {

        ContactoEmailNotificacionAdapter lista = new ContactoEmailNotificacionAdapter();

        Map params = ContactoEmailNotificacionFilter.build(contactoEmailNotificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTACTO_EMAIL_NOTIFICACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoEmailNotificacionAdapter.class);

            lista = (ContactoEmailNotificacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Obtiene la lista de contacto
     *
     * @param contactoEmailNotificacion
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoEmailNotificacionAsociadoAdapter getContactoEmailNotificacionAsociadoList(ContactoEmailNotificacion contactoEmailNotificacion, Integer page,
            Integer pageSize) {

        ContactoEmailNotificacionAsociadoAdapter lista = new ContactoEmailNotificacionAsociadoAdapter();

        Map params = ContactoEmailNotificacionFilter.build(contactoEmailNotificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTIFICACION_ASOCIADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoEmailNotificacionAsociadoAdapter.class);

            lista = (ContactoEmailNotificacionAsociadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
    
     /**
     * Método para editar contacto email notificacion
     *
     * @param contactoEmailNotificacion
     * @return
     */
    public ContactoEmailNotificacion editContactoEmailNotificacion(ContactoEmailNotificacion contactoEmailNotificacion) {

        ContactoEmailNotificacion cEmailEdit = new ContactoEmailNotificacion();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CONTACTO_EMAIL_NOTIFICACION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(contactoEmailNotificacion));

            response = BodyResponse.createInstance(conn, ContactoEmailNotificacion.class);

            cEmailEdit = ((ContactoEmailNotificacion) response.getPayload());

        }
        return cEmailEdit;
    }

    /**
     * Constructor de ContactoEmailNotificacionService
     */
    public ContactoEmailNotificacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CONTACTO_EMAIL_NOTIFICACION("Servicio Contacto Email Notificacion", "contacto-email-notificacion"),
        NOTIFICACION_ASOCIADO("Servicio Contacto Email Notificacion", "contacto-email-notificacion/asociado");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
