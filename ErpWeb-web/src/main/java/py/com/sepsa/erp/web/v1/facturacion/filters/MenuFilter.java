package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para listado de menú
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class MenuFilter extends Filter {

    /**
     * Agrega el filtro por identificador de menú
     *
     * @param id
     * @return
     */
    public MenuFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del padre
     *
     * @param idPadre
     * @return
     */
    public MenuFilter idPadre(Integer idPadre) {
        if (idPadre != null) {
            params.put("idPadre", idPadre);
        }
        return this;
    }

    /**
     * Agrega el filtro por padre
     *
     * @param padre
     * @return
     */
    public MenuFilter padre(MenuPojo padre) {
        if (padre != null) {
            params.put("padre", padre);
        }
        return this;
    }

    /**
     * Agrega el filtro por código
     *
     * @param codigo
     * @return
     */
    public MenuFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por descripcion
     *
     * @param descripcion
     * @return
     */
    public MenuFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro por titulo
     *
     * @param titulo
     * @return
     */
    public MenuFilter titulo(String titulo) {
        if (titulo != null && !titulo.trim().isEmpty()) {
            params.put("titulo", titulo);
        }
        return this;
    }

    /**
     * Agrega el filtro por url
     *
     * @param url
     * @return
     */
    public MenuFilter url(String url) {
        if (url != null && !url.trim().isEmpty()) {
            params.put("url", url);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro activo
     *
     * @param activo
     * @return
     */
    public MenuFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro empresa
     *
     * @param idEmpresa
     * @return
     */
    public MenuFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro orden
     *
     * @param orden
     * @return
     */
    public MenuFilter orden(Integer orden) {
        if (orden != null) {
            params.put("orden", orden);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param menu datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la página de resultado
     * @return
     */
    public static Map build(MenuPojo menu, Integer page, Integer pageSize) {

        MenuFilter filter = new MenuFilter();

        filter
                .id(menu.getId())
                .idPadre(menu.getIdPadre())
                .padre(menu.getPadre())
                .codigo(menu.getCodigo())
                .descripcion(menu.getDescripcion())
                .titulo(menu.getTitulo())
                .url(menu.getUrl())
                .activo(menu.getActivo())
                .orden(menu.getOrden())
                .idEmpresa(menu.getIdEmpresa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de MenuFilter
     */
    public MenuFilter() {
        super();
    }

}
