package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEtiquetaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoEtiquetaFilter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de tipo etiqueta
 *
 * @author Romina Núñez
 */
public class TipoEtiquetaService extends APIErpCore {

    /**
     * Obtiene la lista de tipo etiqueta
     *
     * @param tipoEtiqueta
     * @param page
     * @param pageSize
     * @return
     */
    public TipoEtiquetaAdapter getTipoEtiquetaList(TipoEtiqueta tipoEtiqueta, Integer page,
            Integer pageSize) {

        TipoEtiquetaAdapter lista = new TipoEtiquetaAdapter();

        Map params = TipoEtiquetaFilter.build(tipoEtiqueta, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoEtiquetaAdapter.class);

            lista = (TipoEtiquetaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para crear un nuevo tipo de etiqueta.
     *
     * @param tipoEtiqueta
     * @return
     */
    public TipoEtiqueta create(TipoEtiqueta tipoEtiqueta) {

        HttpURLConnection conn = POST(Resource.CREAR.url, ContentType.JSON);

        if (conn != null) {
            Gson gson = new Gson();
            WebLogger.get().debug(gson.toJson(tipoEtiqueta));
            this.addBody(conn, gson.toJson(tipoEtiqueta));
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoEtiqueta.class);
            tipoEtiqueta = (TipoEtiqueta) response.getPayload();
            conn.disconnect();
        }

        return tipoEtiqueta;
    }

    /**
     * Constructor de UserServiceClient
     */
    public TipoEtiquetaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LISTAR("Listar Tipo Etiqueta", "tipo-etiqueta"),
        CREAR("Crear Tipo Etiqueta", "tipo-etiqueta");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
