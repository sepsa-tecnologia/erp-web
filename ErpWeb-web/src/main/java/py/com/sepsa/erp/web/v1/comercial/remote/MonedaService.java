package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.MonedaFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Moneda
 *
 * @author Romina Núñez
 */
public class MonedaService extends APIErpComercial {

    /**
     * Obtiene la lista de tipo tarifa
     *
     * @param moneda 
     * @param page
     * @param pageSize
     * @return
     */
    public MonedaAdapter getMonedaList(Moneda moneda, Integer page,
            Integer pageSize) {

        MonedaAdapter lista = new MonedaAdapter();

        Map params = MonedaFilter.build(moneda, page, pageSize);

        HttpURLConnection conn = GET(Resource.MONEDA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MonedaAdapter.class);

            lista = (MonedaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
    /**
     * Constructor de MonedaService
     */
    public MonedaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        MONEDA("Moneda", "moneda");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
