package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetallePojo;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class FacturaDetallePojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public FacturaDetallePojoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public FacturaDetallePojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agrega el filtro de idPersona
     *
     * @param idPersona
     * @return
     */
    public FacturaDetallePojoFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public FacturaDetallePojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public FacturaDetallePojoFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public FacturaDetallePojoFilter moneda(String moneda) {
        if (moneda != null && !moneda.trim().isEmpty()) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de tipo factura
     *
     * @param idTipoFactura
     * @return
     */
    public FacturaDetallePojoFilter idTipoFactura(Integer idTipoFactura) {
        if (idTipoFactura != null) {
            params.put("idTipoFactura", idTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de tipo factura
     *
     * @param tipoFactura
     * @return
     */
    public FacturaDetallePojoFilter tipoFactura(String tipoFactura) {
        if (tipoFactura != null && !tipoFactura.trim().isEmpty()) {
            params.put("tipoFactura", tipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro factura
     *
     * @param nroFactura
     * @return
     */
    public FacturaDetallePojoFilter nroFactura(String nroFactura) {
        if (nroFactura != null && !nroFactura.trim().isEmpty()) {
            params.put("nroFactura", nroFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nroOrdenCompra
     *
     * @param nroOrdenCompra
     * @return
     */
    public FacturaDetallePojoFilter nroOrdenCompra(String nroOrdenCompra) {
        if (nroOrdenCompra != null && !nroOrdenCompra.trim().isEmpty()) {
            params.put("nroOrdenCompra", nroOrdenCompra);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public FacturaDetallePojoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha vencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public FacturaDetallePojoFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de razón social
     *
     * @param razonSocial
     * @return
     */
    public FacturaDetallePojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agregar el filtro de direccion
     *
     * @param direccion
     * @return
     */
    public FacturaDetallePojoFilter direccion(String direccion) {
        if (direccion != null && !direccion.trim().isEmpty()) {
            params.put("direccion", direccion);
        }
        return this;
    }

    /**
     * Agregar el filtro de RUC
     *
     * @param ruc
     * @return
     */
    public FacturaDetallePojoFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public FacturaDetallePojoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public FacturaDetallePojoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public FacturaDetallePojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de cobrado
     *
     * @param cobrado
     * @return
     */
    public FacturaDetallePojoFilter cobrado(String cobrado) {
        if (cobrado != null && !cobrado.trim().isEmpty()) {
            params.put("cobrado", cobrado);
        }
        return this;
    }

    /**
     * Agregar el filtro de impreso
     *
     * @param impreso
     * @return
     */
    public FacturaDetallePojoFilter impreso(String impreso) {
        if (impreso != null && !impreso.trim().isEmpty()) {
            params.put("impreso", impreso);
        }
        return this;
    }

    /**
     * Agregar el filtro de entregado
     *
     * @param entregado
     * @return
     */
    public FacturaDetallePojoFilter entregado(String entregado) {
        if (entregado != null && !entregado.trim().isEmpty()) {
            params.put("entregado", entregado);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public FacturaDetallePojoFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de días crédito
     *
     * @param diasCredito
     * @return
     */
    public FacturaDetallePojoFilter diasCredito(Integer diasCredito) {
        if (diasCredito != null) {
            params.put("diasCredito", diasCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 5
     *
     * @param montoIva5
     * @return
     */
    public FacturaDetallePojoFilter montoIva5(BigDecimal montoIva5) {
        if (montoIva5 != null) {
            params.put("montoIva5", montoIva5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 5
     *
     * @param montoImponible5
     * @return
     */
    public FacturaDetallePojoFilter montoImponible5(BigDecimal montoImponible5) {
        if (montoImponible5 != null) {
            params.put("montoImponible5", montoImponible5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 5
     *
     * @param montoTotal5
     * @return
     */
    public FacturaDetallePojoFilter montoTotal5(BigDecimal montoTotal5) {
        if (montoTotal5 != null) {
            params.put("montoTotal5", montoTotal5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 10
     *
     * @param montoIva10
     * @return
     */
    public FacturaDetallePojoFilter montoIva10(BigDecimal montoIva10) {
        if (montoIva10 != null) {
            params.put("montoIva10", montoIva10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 10
     *
     * @param montoImponible10
     * @return
     */
    public FacturaDetallePojoFilter montoImponible10(BigDecimal montoImponible10) {
        if (montoImponible10 != null) {
            params.put("montoImponible10", montoImponible10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 10
     *
     * @param montoTotal10
     * @return
     */
    public FacturaDetallePojoFilter montoTotal10(BigDecimal montoTotal10) {
        if (montoTotal10 != null) {
            params.put("montoTotal10", montoTotal10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total excento
     *
     * @param montoTotalExento
     * @return
     */
    public FacturaDetallePojoFilter montoTotalExento(BigDecimal montoTotalExento) {
        if (montoTotalExento != null) {
            params.put("montoTotalExento", montoTotalExento);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva total
     *
     * @param montoIvaTotal
     * @return
     */
    public FacturaDetallePojoFilter montoIvaTotal(BigDecimal montoIvaTotal) {
        if (montoIvaTotal != null) {
            params.put("montoIvaTotal", montoIvaTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible total
     *
     * @param montoImponibleTotal
     * @return
     */
    public FacturaDetallePojoFilter montoImponibleTotal(BigDecimal montoImponibleTotal) {
        if (montoImponibleTotal != null) {
            params.put("montoImponibleTotal", montoImponibleTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total factura
     *
     * @param montoTotalFactura
     * @return
     */
    public FacturaDetallePojoFilter montoTotalFactura(BigDecimal montoTotalFactura) {
        if (montoTotalFactura != null) {
            params.put("montoTotalFactura", montoTotalFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public FacturaDetallePojoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nota de credito
     *
     * @param montoNotaCredito
     * @return
     */
    public FacturaDetallePojoFilter montoNotaCredito(BigDecimal montoNotaCredito) {
        if (montoNotaCredito != null) {
            params.put("montoNotaCredito", montoNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de montoCobro
     *
     * @param montoCobro
     * @return
     */
    public FacturaDetallePojoFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de retencion
     *
     * @param montoRetencion
     * @return
     */
    public FacturaDetallePojoFilter montoRetencion(BigDecimal montoRetencion) {
        if (montoRetencion != null) {
            params.put("montoRetencion", montoRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro de saldo
     *
     * @param saldo
     * @return
     */
    public FacturaDetallePojoFilter saldo(BigDecimal saldo) {
        if (saldo != null) {
            params.put("saldo", saldo);
        }
        return this;
    }

    /**
     * Agrega el filtro de detalles
     *
     * @param detalles
     * @return
     */
    public FacturaDetallePojoFilter detalles(List<FacturaDetalle> detalles) {
        if (detalles != null && !detalles.isEmpty()) {
            params.put("detalles", detalles);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene retencion, true o false
     *
     * @param tieneRetencion
     * @return
     */
    public FacturaDetallePojoFilter tieneRetencion(String tieneRetencion) {
        if (tieneRetencion != null && !tieneRetencion.trim().isEmpty()) {
            params.put("tieneRetencion", tieneRetencion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro tiene saldo
     *
     * @param tieneSaldo
     * @return
     */
    public FacturaDetallePojoFilter tieneSaldo(String tieneSaldo) {
        if (tieneSaldo != null && !tieneSaldo.trim().isEmpty()) {
            params.put("tieneSaldo", tieneSaldo.trim());
        }
        return this;
    }

    /**
     * Agregar el filtro de anhoMes
     *
     * @param anhoMes
     * @return
     */
    public FacturaDetallePojoFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agregar el filtro de pagado
     *
     * @param pagado
     * @return
     */
    public FacturaDetallePojoFilter pagado(String pagado) {
        if (pagado != null && !pagado.trim().isEmpty()) {
            params.put("pagado", pagado);
        }
        return this;
    }

    /**
     * Agregar el filtro codigoTipoFactura
     *
     * @param codigoTipoFactura
     * @return
     */
    public FacturaDetallePojoFilter codigoTipoFactura(String codigoTipoFactura) {
        if (codigoTipoFactura != null && !codigoTipoFactura.trim().isEmpty()) {
            params.put("codigoTipoFactura", codigoTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param producto
     * @return
     */
    public FacturaDetallePojoFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param tieneOc
     * @return
     */
    public FacturaDetallePojoFilter tieneOrdenCompra(String tieneOrdenCompra) {
        if (tieneOrdenCompra != null && !tieneOrdenCompra.trim().isEmpty()) {
            params.put("tieneOrdenCompra", tieneOrdenCompra);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param factura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(FacturaDetallePojo factura, Integer page, Integer pageSize) {
        FacturaDetallePojoFilter filter = new FacturaDetallePojoFilter();

        filter
                .idFactura(factura.getIdFactura())
                .listadoPojo(factura.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public FacturaDetallePojoFilter() {
        super();
    }
}
