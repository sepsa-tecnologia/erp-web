package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.FirmaDigital;
import py.com.sepsa.erp.web.v1.info.remote.FirmaDigitalService;

/**
 *
 * @author Gustavo Benítez L.
 */
public class FirmaDigitalAdapter extends DataListAdapter<FirmaDigital> {

    private final FirmaDigitalService firmaDigitalService;

    @Override
    public FirmaDigitalAdapter fillData(FirmaDigital searchData) {
        return firmaDigitalService.getFirmaDigitalList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FirmaDigitalAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FirmaDigitalAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.firmaDigitalService = new FirmaDigitalService();
    }

    /**
     * Constructor de ConfiguracionValorListAdapter
     */
    public FirmaDigitalAdapter() {
        super();
        this.firmaDigitalService = new FirmaDigitalService();
    }
}
