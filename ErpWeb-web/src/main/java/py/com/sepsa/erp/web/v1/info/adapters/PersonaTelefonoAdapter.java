
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.info.remote.PersonaTelefonoClient;

/**
 * Adaptador para persona telefono
 * @author Romina Núñez
 */
public class PersonaTelefonoAdapter extends DataListAdapter<PersonaTelefono> {
    
    /**
     * Cliente para los servicios de persona telefono
     */
    private final PersonaTelefonoClient servicePersonaTelefono;
   
    
    
    /**
     * Constructor de PersonaTelefonoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PersonaTelefonoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.servicePersonaTelefono = new PersonaTelefonoClient();
    }

    /**
     * Constructor de PersonaTelefonoAdapter
     */
    public PersonaTelefonoAdapter() {
        super();
        this.servicePersonaTelefono = new PersonaTelefonoClient();
    }

    @Override
    public PersonaTelefonoAdapter fillData(PersonaTelefono searchData) {
        return servicePersonaTelefono.getPersonaTelefono(searchData,getFirstResult(),getPageSize());
    }
}



