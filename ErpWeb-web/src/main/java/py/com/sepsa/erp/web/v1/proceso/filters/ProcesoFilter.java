package py.com.sepsa.erp.web.v1.proceso.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;

/**
 * Filtro utilizado para proceso
 *
 * @author Romina Núñez
 */
public class ProcesoFilter extends Filter {

    public ProcesoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    public ProcesoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    public ProcesoFilter activo(Character activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    public ProcesoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo.trim());
        }
        return this;
    }

    public ProcesoFilter codigoTipoNotificacion(String codigoTipoNotificacion) {
        if (codigoTipoNotificacion != null && !codigoTipoNotificacion.trim().isEmpty()) {
            params.put("codigoTipoNotificacion", codigoTipoNotificacion.trim());
        }
        return this;
    }

    public static Map build(Proceso proceso, Integer page, Integer pageSize) {
        ProcesoFilter filter = new ProcesoFilter();

        filter
                .id(proceso.getId())
                .idEmpresa(proceso.getIdEmpresa())
                .codigo(proceso.getCodigo())
                .codigoTipoNotificacion(proceso.getCodigoTipoNotificacion())
                .activo(proceso.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ServiceFilter
     */
    public ProcesoFilter() {
        super();
    }

}
