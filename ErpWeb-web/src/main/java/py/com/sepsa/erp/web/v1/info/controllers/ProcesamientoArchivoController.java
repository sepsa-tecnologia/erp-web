package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProcesamientoArchivoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoArchivoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoDocumentoProcesoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.ProcesamientoArchivo;
import py.com.sepsa.erp.web.v1.info.pojos.ProcesamientoArchivoDetalle;
import py.com.sepsa.erp.web.v1.info.pojos.TipoArchivo;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDocumentoProceso;
import py.com.sepsa.erp.web.v1.info.remote.ProcesamientoArchivoService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para la lista de procesamiento de archivo
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("procesamientoArchivo")
public class ProcesamientoArchivoController implements Serializable {

    /**
     * Cliente remoto para procesamiento archivo
     */
    private final ProcesamientoArchivoService service;
    /**
     * Pojo de procesamiento de archivo
     */
    private ProcesamientoArchivo searchData;
    /**
     * Adaptador de la lista de procesamiento de archivo
     */
    private ProcesamientoArchivoAdapter adapter;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private TipoDocumentoProcesoListAdapter tipoDocumentoProcesoAdapterList;
    /**
     * POJO de tipo documento
     */
    private TipoDocumentoProceso tipoDocumentoProcesoFilter;
    /**
     * Adaptador para la lista de tipo archivo
     */
    private TipoArchivoAdapter tipoArchivoAdapterList;
    /**
     * POJO de tipo archivo
     */
    private TipoArchivo tipoArchivoFilter;
    /**
     * Adaptador de la lista de locales origen
     */
    private LocalListAdapter localOrigenAdapter;
    /**
     * Datos del local
     */
    private Local localOrigen;
    /**
     * Adaptador de la lista de locales origen
     */
    private LocalListAdapter localDestinoAdapter;
    /**
     * Datos del local
     */
    private Local localDestino;
    /**
     * Lista de detalles
     */
    private List<ProcesamientoArchivoDetalle> detalles;
    /**
     * Identificador de Empresa 
     */
    private Integer idEmpresa;
    
    @Inject
    private SessionData session;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public ProcesamientoArchivo getSearchData() {
        return searchData;
    }

    public void setSearchData(ProcesamientoArchivo searchData) {
        this.searchData = searchData;
    }

    public ProcesamientoArchivoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ProcesamientoArchivoAdapter adapter) {
        this.adapter = adapter;
    }

    public TipoDocumentoProcesoListAdapter getTipoDocumentoProcesoAdapterList() {
        return tipoDocumentoProcesoAdapterList;
    }

    public void setTipoDocumentoProcesoAdapterList(TipoDocumentoProcesoListAdapter tipoDocumentoProcesoAdapterList) {
        this.tipoDocumentoProcesoAdapterList = tipoDocumentoProcesoAdapterList;
    }

    public TipoDocumentoProceso getTipoDocumentoProcesoFilter() {
        return tipoDocumentoProcesoFilter;
    }

    public void setTipoDocumentoProcesoFilter(TipoDocumentoProceso tipoDocumentoProcesoFilter) {
        this.tipoDocumentoProcesoFilter = tipoDocumentoProcesoFilter;
    }

    public TipoArchivoAdapter getTipoArchivoAdapterList() {
        return tipoArchivoAdapterList;
    }

    public void setTipoArchivoAdapterList(TipoArchivoAdapter tipoArchivoAdapterList) {
        this.tipoArchivoAdapterList = tipoArchivoAdapterList;
    }

    public TipoArchivo getTipoArchivoFilter() {
        return tipoArchivoFilter;
    }

    public void setTipoArchivoFilter(TipoArchivo tipoArchivoFilter) {
        this.tipoArchivoFilter = tipoArchivoFilter;
    }

    public LocalListAdapter getLocalOrigenAdapter() {
        return localOrigenAdapter;
    }

    public void setLocalOrigenAdapter(LocalListAdapter localOrigenAdapter) {
        this.localOrigenAdapter = localOrigenAdapter;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public LocalListAdapter getLocalDestinoAdapter() {
        return localDestinoAdapter;
    }

    public void setLocalDestinoAdapter(LocalListAdapter localDestinoAdapter) {
        this.localDestinoAdapter = localDestinoAdapter;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public List<ProcesamientoArchivoDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<ProcesamientoArchivoDetalle> detalles) {
        this.detalles = detalles;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    //</editor-fold>
    /**
     * Método para filtrar procesamiento de archivo
     */
    public void filterProcesamientoArchivo() {
        getAdapter().setFirstResult(0);
        searchData.setIdEmpresa(idEmpresa);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        searchData = new ProcesamientoArchivo();
        searchData.setIdEmpresa(idEmpresa);
        tipoArchivoFilter = new TipoArchivo();
        tipoDocumentoProcesoFilter = new TipoDocumentoProceso();
        this.localOrigen = new Local();
        this.localDestino = new Local();
        filterProcesamientoArchivo();
    }

    public void verDetalle(ProcesamientoArchivo pArchivo) {
        detalles = pArchivo.getProcesamientoArchivoDetalles();
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoDocumentos() {
        this.setTipoDocumentoProcesoAdapterList(getTipoDocumentoProcesoAdapterList().fillData(getTipoDocumentoProcesoFilter()));
    }

    /**
     * Método para obtener tipo archivo
     */
    public void getTipoArchivos() {
        this.setTipoArchivoAdapterList(getTipoArchivoAdapterList().fillData(getTipoArchivoFilter()));
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQuery1(String query) {
        Local localOrigen = new Local();
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalOringenFilter(SelectEvent event) {
        localOrigen.setId(((Local) event.getObject()).getId());
        searchData.setIdLocalOrigen(localOrigen.getId());
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQuery2(String query) {
        Local localDestino = new Local();
        localDestino.setDescripcion(query);
        localDestinoAdapter = localDestinoAdapter.fillData(localDestino);
        return localDestinoAdapter.getData();
    }

    /**
     * Selecciona local destino
     *
     * @param event
     */
    public void onItemSelectLocalDestinoFilter(SelectEvent event) {
        localDestino.setId(((Local) event.getObject()).getId());
        searchData.setIdLocalDestino(localDestino.getId());
    }

    /**
     * Método para reenviar procesamiento archivo
     *
     * @param idProcesamientoArchivo
     */
    public void onChangeStatus(Integer idProcesamientoArchivo) {

        BodyResponse response = service.reenviar(idProcesamientoArchivo);

        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Reenviado correctamente!"));
            filterProcesamientoArchivo();
        }
    }

    @PostConstruct
    public void init() {
        this.tipoDocumentoProcesoAdapterList = new TipoDocumentoProcesoListAdapter();
        this.tipoDocumentoProcesoFilter = new TipoDocumentoProceso();
        this.tipoArchivoAdapterList = new TipoArchivoAdapter();
        this.tipoArchivoFilter = new TipoArchivo();
        this.searchData = new ProcesamientoArchivo();
        this.idEmpresa = session.getUser().getIdEmpresa();
        this.adapter = new ProcesamientoArchivoAdapter();
        this.localOrigenAdapter = new LocalListAdapter();
        this.localOrigen = new Local();
        this.localDestinoAdapter = new LocalListAdapter();
        this.localDestino = new Local();
        getTipoDocumentos();
        getTipoArchivos();
        filterProcesamientoArchivo();
    }

    /**
     * Constructor de la clase
     */
    public ProcesamientoArchivoController() {
        this.detalles = new ArrayList<ProcesamientoArchivoDetalle>();
        this.service = new ProcesamientoArchivoService();
    }

}
