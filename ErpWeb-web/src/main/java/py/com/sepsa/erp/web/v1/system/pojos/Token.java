/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 * POJO para el token de sesion
 * @author Cristina Insfran
 */
public class Token {

   
    /**
     * Token de sesion
     */
    private String token;
 
    
      /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    public Token(String token) {
        this.token = token;
    }
   

    
    public Token(){
        
    }
   
  
}
