package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TipoMotivoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de tipoTipoMotivo
 *
 * @author Alexander Triana
 */
public class TipoMotivoService extends APIErpCore {

    /**
     * Obtiene la lista
     *
     * @param tipoMotivo Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoMotivoAdapter getTipoMotivo(TipoMotivo tipoMotivo, Integer page,
            Integer pageSize) {
        TipoMotivoAdapter lista = new TipoMotivoAdapter();
        try {
            Map params = TipoMotivoFilter.build(tipoMotivo, page, pageSize);
            HttpURLConnection conn = GET(Resource.TIPO_MOTIVO.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        TipoMotivoAdapter.class);
                lista = (TipoMotivoAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param tipoTipoMotivo
     * @return
     */
    public BodyResponse<TipoMotivo> setTipoMotivo(TipoMotivo tipoTipoMotivo) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.TIPO_MOTIVO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(tipoTipoMotivo));
            response = BodyResponse.createInstance(conn, TipoMotivo.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param tipoTipoMotivo
     * @return
     */
    public BodyResponse<TipoMotivo> editTipoMotivo(TipoMotivo tipoTipoMotivo) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.TIPO_MOTIVO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(tipoTipoMotivo));
            response = BodyResponse.createInstance(conn, TipoMotivo.class);
        }
        return response;
    }

    /**
     * Obtiene el identificador del objeto a ser editado
     *
     * @param id
     * @return
     */
    public TipoMotivo get(Integer id) {
        TipoMotivo data = new TipoMotivo(id);
        TipoMotivoAdapter list = getTipoMotivo(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public TipoMotivoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_MOTIVO("Listar tipoMotivo", "tipo-motivo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
