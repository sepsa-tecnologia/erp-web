package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteVentaNotaDebito;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteVentaNotaDebitoService;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para reporte de venta nota de debito
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("reporteVentaNotaDebito")
public class ReporteVentaNotaDebitoController implements Serializable {

    /**
     * Objeto para filtro
     */
    private ReporteVentaNotaDebito parametro;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private ReporteVentaNotaDebitoService reporteCliente;
    /**
     * Adaptador para la lista de local
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de local
     */
    private Local localFilter;
    
    private Integer diasFiltro;
    @Inject
    private SessionData session;
    
    private Cliente client;
    
    private ClientListAdapter adapterCliente;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public ReporteVentaNotaDebito getParametro() {
        return parametro;
    }

    public void setParametro(ReporteVentaNotaDebito parametro) {
        this.parametro = parametro;
    }

    public ReporteVentaNotaDebitoService getReporteCliente() {
        return reporteCliente;
    }

    public void setReporteCliente(ReporteVentaNotaDebitoService reporteCliente) {
        this.reporteCliente = reporteCliente;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    //</editor-fold>
    /**
     * Método para descargar reporte
     *
     * @return
     */
        public StreamedContent download() {
        if (validacionesDownloadReporte()) {
            String contentType = ("application/vnd.ms-excel");
            String fileName = "libro-venta-nota-debito.xlsx";
            byte[] data;
            data = reporteCliente.getReporteVenta(parametro);
            if (data != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(data),
                        contentType, fileName);               
            }
        }
        return null;
    }
    
    public void clear(){
        client = new Cliente();
        parametro = new ReporteVentaNotaDebito();
        Calendar today = Calendar.getInstance();
        parametro.setFechaDesde(today.getTime());
        parametro.setFechaHasta(today.getTime());
        localFilter = new Local();
    }
    
    public boolean validacionesDownloadReporte() {
        boolean save = true;
        if (parametro.getIdCliente() == null && parametro.getFechaDesde() == null && parametro.getFechaHasta() == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe elegir al menos un cliente o un rango de fechas"));
            save = false;
        } else {
            if (parametro.getFechaDesde() != null && parametro.getFechaHasta() == null) {
                FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha hasta"));
                save = false;
            }else{
                if (parametro.getFechaDesde() == null && parametro.getFechaHasta() != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha desde"));
                    save = false;
                }
            }
            
            if (parametro.getFechaDesde() != null && parametro.getFechaHasta() != null) {
                Instant from = parametro.getFechaDesde().toInstant();
                Instant to = parametro.getFechaHasta().toInstant();

                long days = ChronoUnit.DAYS.between(from, to);
                Integer maxDias = 31;
                if (diasFiltro != null) {
                    maxDias = diasFiltro;
                }
                if (days > maxDias) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                    "Fecha Incorrecta", "El rango de fecha máximo es de 31 dias"));
                    save = false;
                }
            } 
        }
        return save;
    }
    
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        parametro.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /**
     * Método para obtener local
     */
    public void getLocales() {
        this.localFilter.setLocalExterno("N");
        this.setLocalAdapterList(getLocalAdapterList().fillData(getLocalFilter()));
    }
    
        /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setLocalExterno("N");
        local.setDescripcion(query);
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocal(SelectEvent event) {
        parametro.setIdLocalTalonario(((Local) event.getObject()).getId());
    }
    

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteVentaNotaDebito();
        this.reporteCliente = new ReporteVentaNotaDebitoService();
        this.localAdapterList = new LocalListAdapter();
        this.localFilter = new Local();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        Calendar today = Calendar.getInstance();
        parametro.setFechaDesde(today.getTime());
        parametro.setFechaHasta(today.getTime());
        getLocales();
        this.diasFiltro = ConfiguracionValorServiceClient.getConfValorInteger(session.getUser().getIdEmpresa(), "CANTIDAD_MAXIMA_DIAS_FILTRO", null);
    }

}
