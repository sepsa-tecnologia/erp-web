package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;

/**
 * POJO para Detalle Nota Credito
 *
 * @author Williams Vera
 */
public class NotaRemisionDetalle {
    /**
     * Identificador de Nota de Crédito
     */
    private Integer idNotaRemision;
    /**
     * Nro nota de crédito
     */
    private String nroNotaRemision;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    
    private String lote;
    
    private String fechaVencimientoLote;
    
    private Boolean listadoPojo;

    //<editor-fold defaultstate="collapsed" desc="***GETTER & SETTER***">

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(String fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }
    
    
    
    public Integer getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Integer idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }


    public BigDecimal getCantidad() {
        return cantidad;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }
    
    
//</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public NotaRemisionDetalle() {
    }
}
