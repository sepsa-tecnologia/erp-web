/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;

/**
 * Filtro para Tipo Repositorio
 *
 * @author Romina Núñez
 */
public class TipoRepositorioFilter extends Filter {

    /**
     * Agrega el parametro para el filtro id
     *
     * @param id
     * @return
     */
    public TipoRepositorioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoRepositorioFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro codigo
     *
     * @param codigo
     * @return
     */
    public TipoRepositorioFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoRepositorio datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoRepositorio tipoRepositorio, Integer page, Integer pageSize) {
        TipoRepositorioFilter filter = new TipoRepositorioFilter();

        filter
                .id(tipoRepositorio.getId())
                .descripcion(tipoRepositorio.getDescripcion())
                .codigo(tipoRepositorio.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoRepositorioFilter
     */
    public TipoRepositorioFilter() {
        super();
    }

}
