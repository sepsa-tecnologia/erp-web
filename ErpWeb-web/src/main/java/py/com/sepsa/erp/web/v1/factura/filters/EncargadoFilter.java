
package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.Encargado;

/**
 * Filtro para la lista de encargados
 * @author Cristina Insfrán
 */
public class EncargadoFilter extends Filter{
    
    
    /**
     * Agrega el filtro de identificador 
     *
     * @param id
     * @return
     */
    public EncargadoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
     
    /**
     * Agrega el filtro descripción
     *
     * @param descripcion
     * @return
     */
    public EncargadoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion.trim());
        }
        return this;
    }
    
      /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public EncargadoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo.trim());
        }
        return this;
    }
    
     /**
     * Agrega el filtro estado
     *
     * @param estado
     * @return
     */
    public EncargadoFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }
    
      /**
     * Agrega el filtro  charToString
     *
     * @param charToString
     * @return
     */
    public EncargadoFilter charToString(Boolean charToString) {
        if (charToString != null ) {
            params.put("charToString", charToString);
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     * 
     * @param encargado
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Encargado encargado, Integer page, Integer pageSize) {
        EncargadoFilter filter = new EncargadoFilter();

        filter
                .id(encargado.getId())
                .descripcion(encargado.getDescripcion())
                .codigo(encargado.getCodigo())
                .estado(encargado.getEstado())
                .charToString(encargado.getCharToString())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de EncargadoFilter
     */
    public EncargadoFilter() {
        super();
    }
}
