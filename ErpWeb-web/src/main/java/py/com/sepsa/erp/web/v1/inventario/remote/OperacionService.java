/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.OperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.OperacionFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Cliente para el servicio de Operacion inventario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OperacionService extends APIErpInventario {

    /**
     * Obtiene la lista de operaciones de inventario 
     *
     * @param operacion Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public OperacionAdapter getOperacion(Operacion operacion, Integer page,
            Integer pageSize) {
        OperacionAdapter lista = new OperacionAdapter();
        try {
            Map params = OperacionFilter.build(operacion, page, pageSize);
            HttpURLConnection conn = GET(Resource.OPERACION.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        OperacionAdapter.class);
                lista = (OperacionAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param operacion
     * @return
     */
    public BodyResponse<Operacion> setOperacion(Operacion operacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.OPERACION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(operacion));
            response = BodyResponse.createInstance(conn, Operacion.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param operacion
     * @return
     */
    public BodyResponse<Operacion> editOperacion(Operacion operacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.OPERACION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(operacion));
            response = BodyResponse.createInstance(conn, Operacion.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Operacion get(Integer id) {
        Operacion data = new Operacion(id);
        OperacionAdapter list = getOperacion(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public OperacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        OPERACION("Operacion", "operacion-inventario");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
