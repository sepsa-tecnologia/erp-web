
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de tipo telefono
 * @author Cristina Insfrán
 */
public class TipoTelefono {

    /**
     * Identificador del tipo telefono
     */
    private Integer id;
    /**
     * Descripción del tipo
     */
    private String tipo;
    /**
     * Descripción del tipo respuesta
     */
    private String descripcion;
    /**
     * Código
     */
    private String codigo;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the código
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the código to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    public TipoTelefono(){
        
    }
    
    public TipoTelefono(Integer id,String tipo){
        this.id=id;
        this.tipo=tipo;
    }
}
