package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientesServiciosAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ClientesServiciosFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientesServicios;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Servicio para el listado de clientes s/ servicios.
 *
 * @author alext
 * @author Sergio D. Riveros Vazquez
 */
public class ClientesServiciosService extends APIErpComercial {

    /**
     * Obtiene la lista de clientes según servicios.
     *
     * @param searchData
     * @param page
     * @param pagesize
     * @return
     */
    public ClientesServiciosAdapter getClientesServiciosList(ClientesServicios searchData,
            Integer page, Integer pagesize) {

        ClientesServiciosAdapter lista = new ClientesServiciosAdapter();

        Map params = ClientesServiciosFilter.build(searchData, page, pagesize);

        HttpURLConnection conn = GET(Resource.CLIENTE_SERVICIO_REPORTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ClientesServiciosAdapter.class);

            lista = (ClientesServiciosAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    /**
     * Recursos de conexión.
     */
    public enum Resource {

        // TO DO: agregar path.
        //Servicios
        CLIENTES_SERVICIOS("Servicios de Liquidaciones por cadena", "cliente-servicios"),
        CLIENTE_SERVICIO_REPORTE("Servicio de reporte de cliente segun servicio", "cliente-servicio/reporte");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
