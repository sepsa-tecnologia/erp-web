package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.filters.LocalFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.SucursalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de local
 *
 * @author Alex T.
 */
public class SucursalService extends APIErpCore {

    /**
     * Obtiene la lista de sucursales
     *
     * @param local Objeto local
     * @param page
     * @param pageSize
     * @return
     */
    public SucursalListAdapter getLocalList(Local local, Integer page,
            Integer pageSize) {

        SucursalListAdapter lista = new SucursalListAdapter();

        Map params = LocalFilter.build(local, page, pageSize);

        HttpURLConnection conn = GET(Resource.LOCAL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    SucursalListAdapter.class);

            lista = (SucursalListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de UserServiceClient
     */
    public SucursalService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LOCAL("Servicio Local", "local");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
