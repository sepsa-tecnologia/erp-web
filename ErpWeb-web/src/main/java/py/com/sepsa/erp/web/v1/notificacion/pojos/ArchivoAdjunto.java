/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.pojos;

/**
 *
 * @author Romina Núñez
 */
public class ArchivoAdjunto {
    /**
     * Tipo de Contenido
     */
    private String contentType;
    /**
     * Nombre de Archivo
     */
    private String fileName;
    /**
     * Archivo
     */
    private byte[] data;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
    
    
    
}
