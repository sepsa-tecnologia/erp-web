/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.adapters.VehiculoTrasladoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.VehiculoTrasladoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Talonario
 *
 * @author Williams Vera
 */
public class VehiculoTrasladoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de talonarios
     *
     * @param param Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public VehiculoTrasladoListAdapter getVehiculoTrasladoList(VehiculoTraslado param, Integer page,
            Integer pageSize) {

        VehiculoTrasladoListAdapter lista = new VehiculoTrasladoListAdapter();

        Map params = VehiculoTrasladoFilter.build(param, page, pageSize);

        HttpURLConnection conn = GET(Resource.VEHICULO_TRASLADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    VehiculoTrasladoListAdapter.class);

            lista = (VehiculoTrasladoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param vehiculoTraslado
     * @return
     */
    public BodyResponse<VehiculoTraslado> setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.VEHICULO_TRASLADO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(vehiculoTraslado));

            response = BodyResponse.createInstance(conn, VehiculoTraslado.class);

        }
        return response;
    }

    /**
     * Método para editar vehiculoTraslado
     *
     * @param vehiculoTraslado
     * @return
     */
    public BodyResponse<VehiculoTraslado> editVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.VEHICULO_TRASLADO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(vehiculoTraslado));
            response = BodyResponse.createInstance(conn, VehiculoTraslado.class);
        }
        return response;
    }
    
    /**
     *
     * @param id
     * @return
     */
    public VehiculoTraslado get(Integer id) {
        VehiculoTraslado data = new VehiculoTraslado();
        data.setId(id);
        VehiculoTrasladoListAdapter list = getVehiculoTrasladoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }


    /**
     * Constructor de clase
     */
    public VehiculoTrasladoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        VEHICULO_TRASLADO("vehiculo-traslado", "vehiculo-traslado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
