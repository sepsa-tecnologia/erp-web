/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Configuration;

/**
 * Filter para configuration
 *
 * @author Romina Núñez
 */
public class ConfigurationFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public ConfigurationFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de code para configuration
     *
     * @param code
     * @return
     */
    public ConfigurationFilter code(String code) {
        if (code != null && !code.trim().isEmpty()) {
            params.put("code", code.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de description para configuration
     *
     * @param description
     * @return
     */
    public ConfigurationFilter description(String description) {
        if (description != null && !description.trim().isEmpty()) {
            params.put("description", description.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param configuration datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Configuration configuration, Integer page, Integer pageSize) {
        ConfigurationFilter filter = new ConfigurationFilter();

        filter
                .id(configuration.getId())
                .code(configuration.getCode())
                .description(configuration.getDescription())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ConfigurationFilter
     */
    public ConfigurationFilter() {
        super();
    }
}
