
package py.com.sepsa.erp.web.v1.adapters;

import java.util.List;
import py.com.sepsa.erp.web.v1.system.pojos.Message;

/**
 * Adaptador para la lista de mensajes
 * @author Daniel F. Escauriza Arza
 */
public class MessageListAdapter {
    
    /**
     * Lista de mensajes
     */
    private List<Message> messageList;

    /**
     * Obtiene la lista de mensajes
     * @return Lista de mensajes
     */
    public List<Message> getMessageList() {
        return messageList;
    }

    /**
     * Setea la lista de mensajes
     * @param errorList Lista de mensajes
     */
    public void setMessageList(List<Message> errorList) {
        this.messageList = errorList;
    }
    
    /**
     * Constructor de MessageListAdapter
     */
    public MessageListAdapter() {}
    
}
