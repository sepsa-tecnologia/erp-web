/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.task;

import py.com.sepsa.erp.dte.generator.NotaCreditoGeneratorRunnable;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorUtils;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.utils.automation.AbstractTask;
import py.com.sepsa.utils.automation.Task;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Jonathan
 */
public class GeneracionDteNotaCredito extends AbstractTask {

    /**
     * Identificador de empresa
     */
    private final Integer idEmpresa;
    
    public GeneracionDteNotaCredito(Proceso proceo, Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
        this.task = new Task(proceo.getId(),
                String.format("%s - IdEmpresa:%d", proceo.getCodigo(), idEmpresa),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getFrecuencia()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getHora()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getMinuto()));
    }

    @Override
    public void task() {
        try {
            String apiToken = ConfiguracionValorUtils.getApiToken(idEmpresa);
            String user = ConfiguracionValorUtils.getUser(idEmpresa);
            String pass = ConfiguracionValorUtils.getPass(idEmpresa);
            String tipoNotificacion = ConfiguracionValorUtils.getTipoNotificacion(idEmpresa);
            
            NotaCreditoGeneratorRunnable acpvr = new NotaCreditoGeneratorRunnable(apiToken, user, pass, tipoNotificacion,idEmpresa);
            acpvr.run();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
