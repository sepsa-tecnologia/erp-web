/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * Pojo para Tipo Documento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumento {

    /**
     * Identificador de tipo de documento
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Codigo
     */
    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoDocumento() {
    }

}
