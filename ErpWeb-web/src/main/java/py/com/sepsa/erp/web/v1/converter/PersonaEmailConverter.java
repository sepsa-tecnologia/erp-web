/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.remote.PersonaEmailClient;

/**
 *
 * @author Williams Vera
 */
@FacesConverter("personaEmailConverter")
public class PersonaEmailConverter implements Converter{
    
      /**
     * Servicios para cliente
     */

    private PersonaEmailClient serviceClient;
    
    @Override
    public PersonaEmail getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new PersonaEmailClient();
            System.out.println(value);
            Integer val = null;
            try {
                val = Integer.valueOf(value);
                System.out.println(val);
            } catch(Exception ex) {}
            
            PersonaEmail pe = new PersonaEmail();
            pe.setId(val);
          
            List<PersonaEmail> emails = serviceClient.getEmailList(pe, 0, 10).getData();
            if(emails != null && !emails.isEmpty()) {
                System.out.println("Recivido en converter " + emails.get(0).getId() + " email : "+emails.get(0).getEmail().getEmail());
                return emails.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un cliente válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof PersonaEmail) {
                return String.valueOf(((PersonaEmail)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
