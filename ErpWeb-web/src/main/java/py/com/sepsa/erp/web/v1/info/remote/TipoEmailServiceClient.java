
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmailListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoEmailFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Tipo Email
 * @author Cristina Insfrán
 */
public class TipoEmailServiceClient extends  APIErpCore{
    
    /**
     * Obtiene la lista de tipo email
     *
     * @param tipo Objeto tipo email
     * @param page
     * @param pageSize
     * @return
     */
    public TipoEmailListAdapter getTipoEmailList(TipoEmail tipo, Integer page,
            Integer pageSize) {
       
        TipoEmailListAdapter lista = new TipoEmailListAdapter();
        
        Map params = TipoEmailFilter.build(tipo, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.TIPO_EMAIL.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoEmailListAdapter.class);

            lista = (TipoEmailListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }
     /**
     * Constructor de TipoEmailServiceClient
     */
    public TipoEmailServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        TIPO_EMAIL("Servivio Tipo Email", "tipo-email");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
