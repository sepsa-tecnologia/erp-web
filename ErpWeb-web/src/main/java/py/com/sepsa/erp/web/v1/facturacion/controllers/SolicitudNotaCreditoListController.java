/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.adapters.SolicitudNotaCreditoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar solicitud nota credito
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("solicitudNotaCreditoList")
public class SolicitudNotaCreditoListController implements Serializable {

    /**
     * Adaptador de solicitudNotaCredito.
     */
    private SolicitudNotaCreditoListAdapter adapter;
    /**
     * Objeto solicitudNotaCredito.
     */
    private SolicitudNotaCredito solicitudNotaCreditoFilter;
    /**
     * Adaptador de la lista de moneda
     */
    private MonedaAdapter monedaAdapter;
    /**
     * Datos del moneda
     */
    private Moneda moneda;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">

    public SolicitudNotaCreditoListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(SolicitudNotaCreditoListAdapter adapter) {
        this.adapter = adapter;
    }

    public SolicitudNotaCredito getSolicitudNotaCreditoFilter() {
        return solicitudNotaCreditoFilter;
    }

    public void setSolicitudNotaCreditoFilter(SolicitudNotaCredito solicitudNotaCreditoFilter) {
        this.solicitudNotaCreditoFilter = solicitudNotaCreditoFilter;
    }

    public MonedaAdapter getMonedaAdapter() {
        return monedaAdapter;
    }

    public void setMonedaAdapter(MonedaAdapter monedaAdapter) {
        this.monedaAdapter = monedaAdapter;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    //</editor-fold>
    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleSolicitudNotaCreditoURL(Integer id) {

        return String.format("solicitud-nota-credito-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    /**
     * Método para filtrar solicitud Nota Credito.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(solicitudNotaCreditoFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.solicitudNotaCreditoFilter = new SolicitudNotaCredito();
        this.moneda = new Moneda();
        getMonedas();
        buscar();
    }

    /**
     * Método para obtener las monedas
     */
    public void getMonedas() {
        this.setMonedaAdapter(getMonedaAdapter().fillData(getMoneda()));
    }


    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        clientAdapter = clientAdapter.fillData(cliente);
        return clientAdapter.getData();
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String ncURL(Integer idSolicitud) {
        return String.format("/app/factura/nota-credito-create.xhtml"
                + "?faces-redirect=true"
                + "&idSolicitud=%d", idSolicitud);
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        solicitudNotaCreditoFilter.setIdCliente(cliente.getIdCliente());
    }

    @PostConstruct
    public void init() {
        try {
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.adapter = new SolicitudNotaCreditoListAdapter();
            this.solicitudNotaCreditoFilter = new SolicitudNotaCredito();
            this.monedaAdapter = new MonedaAdapter();
            this.moneda = new Moneda();
            getMonedas();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
