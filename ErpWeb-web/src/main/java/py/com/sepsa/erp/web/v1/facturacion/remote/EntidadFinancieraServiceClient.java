
package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.EntidadFinancieraListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.EntidadFinancieraFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio remoto para entidad financiera
 * @author Cristina Insfrán
 */
public class EntidadFinancieraServiceClient extends APIErpFacturacion {
    
      /**
     * Obtiene la lista de entidad financiera
     * 
     * @param entidad
     * @param page
     * @param pageSize
     * @return
     */
    public EntidadFinancieraListAdapter getEntidadFinancieraList(EntidadFinanciera entidad, Integer page,
            Integer pageSize) {
       
        EntidadFinancieraListAdapter lista = new EntidadFinancieraListAdapter();
        Map params = EntidadFinancieraFilter.build(entidad, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.ENTIDAD_FINANCIERA.url, 
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EntidadFinancieraListAdapter.class);
            
            lista = (EntidadFinancieraListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }
      /**
     * Recursos de conexión o servicios del APIErpFacturacion
     */
    public enum Resource {

        //Servicios
        ENTIDAD_FINANCIERA("Servicio Entidad Financiera", "entidad-financiera");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
