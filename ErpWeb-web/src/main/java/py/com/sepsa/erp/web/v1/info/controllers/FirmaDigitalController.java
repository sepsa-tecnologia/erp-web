package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.FirmaDigitalAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.FirmaDigital;
import py.com.sepsa.erp.web.v1.info.remote.FirmaDigitalService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Gustavo Benítez L.
 */
@SessionScoped
@Named("firmaCont")
public class FirmaDigitalController implements Serializable {

    /**
     * Adaptador de la lista de FirmaDigital
     */
    private FirmaDigitalAdapter adapterFirma;
    /**
     * Objeto firmaDigital
     */
    private FirmaDigital firma;
    /**
     * Servicio a FirmaDigital
     */
    private FirmaDigitalService servicioFirma;
    private long diasRestantes;
    private String color;
    private String fechaVencimiento;

    private Empresa empresa;

    private EmpresaAdapter empresaAdapter;
    @Inject
    private SessionData session;

//<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public FirmaDigitalAdapter getAdapterFirma() {
        return adapterFirma;
    }

    public void setAdapterFirma(FirmaDigitalAdapter adapterFirma) {
        this.adapterFirma = adapterFirma;
    }

    public FirmaDigitalService getServicioFirma() {
        return servicioFirma;
    }

    public void setFirma(FirmaDigital firma) {
        this.firma = firma;
    }

    public FirmaDigital getFirma() {
        return firma;
    }

    public void setServicioFirma(FirmaDigitalService servicioFirma) {
        this.servicioFirma = servicioFirma;
    }

    public long getDiasRestantes() {
        return diasRestantes;
    }

    public void setDiasRestantes(long diasRestantes) {
        this.diasRestantes = diasRestantes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    //</editor-fold>
    public void calculateDaysRemaining() {
        if (firma == null || firma.getFechaVencimiento() == null) {
            diasRestantes = 0;
            color = "red"; // No hay firma digital o fecha de vencimiento, se considera vencida
            return;
        }
        LocalDate today = LocalDate.now();
        Date expirationDate = firma.getFechaVencimiento();
        LocalDate expirationLocalDate = expirationDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fechaVencimiento = simpleDateFormat.format(firma.getFechaVencimiento());

        // Si la fecha de vencimiento ya pasó
        if (expirationLocalDate.isBefore(today)) {
            diasRestantes = 0;
            color = "red";
        } else {
            diasRestantes = (int) ChronoUnit.DAYS.between(today, expirationLocalDate);
            // Determina el color basado en los días restantes
            if (diasRestantes > 10) {
                color = "green";
            } else if (diasRestantes > 0 && diasRestantes <= 10) {
                color = "orange";
            } else {
                color = "red";
            }
        }
    }

    public void filter() {
        getAdapterFirma().setFirstResult(0);
        empresa.setId(session.getUser().getIdEmpresa());
        empresaAdapter = empresaAdapter.fillData(empresa);
        FirmaDigital filter = new FirmaDigital();
        filter.setIdCliente(empresaAdapter.getData().get(0).getIdClienteSepsa());
        filter.setActivo("S");
        adapterFirma = adapterFirma.fillData(filter);
        this.firma = Units.execute(() -> adapterFirma.getData().get(0));
        calculateDaysRemaining();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        WebLogger.get().info("Inicializando FirmaDigitalController");
        this.adapterFirma = new FirmaDigitalAdapter();
        this.empresa = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        filter();
    }

}
