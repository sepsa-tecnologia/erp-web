/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * pojo para motivo emision interno
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionInterno {

    /**
     * Id
     */
    private Integer id;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * activo
     */
    private String activo;
    /**
     * idMotivoEmision
     */
    private Integer idMotivoEmision;
    /**
     * motivoEmision
     */
    private MotivoEmision motivoEmision;
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }
    //</editor-fold>

    public MotivoEmisionInterno() {

    }

    public MotivoEmisionInterno(Integer id) {
        this.id = id;
    }

}
