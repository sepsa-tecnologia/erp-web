
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;

/**
 *
 * @author Williams Vera
 */
@FacesConverter("vehiculoTrasladoPojoConverter")
public class VehiculoTrasladoPojoConverter implements Converter{
    
    /**
     * Servicio para producto
     */
    private VehiculoTrasladoService serviceVehiculo;
    
     @Override
    public VehiculoTraslado getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceVehiculo = new VehiculoTrasladoService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
          
            VehiculoTraslado vehiculo=new VehiculoTraslado();
            vehiculo.setId(val);
          
            List<VehiculoTraslado> vehiculos = serviceVehiculo.getVehiculoTrasladoList(vehiculo, 0, 10).getData();
            if(vehiculos != null && !vehiculos.isEmpty()) {
                return vehiculos.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un vehiculo de transporte válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof VehiculoTraslado) {
                return String.valueOf(((VehiculoTraslado)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
