/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.info.remote.MetricaService;


/**
 * Converter para Metrica
 *
 * @author Williams Vera
 */
@FacesConverter("metricaPojoConverter")
public class MetricaPojoConverter implements Converter {

    /**
     * Servicios para Metrica
     */
    private MetricaService serviceClient;

    @Override
    public Metrica getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new MetricaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            Metrica metrica = new Metrica();
            metrica.setId(val);

            List<Metrica> metricas = serviceClient.getMetricaList(metrica, 0, 10).getData();
            if (metricas != null && !metricas.isEmpty()) {
                return metricas.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es una métrica válida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof Metrica) {
                return String.valueOf(((Metrica) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
