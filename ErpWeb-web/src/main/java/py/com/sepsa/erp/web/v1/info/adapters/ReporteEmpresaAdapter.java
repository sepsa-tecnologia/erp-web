
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.ReporteEmpresa;
import py.com.sepsa.erp.web.v1.info.remote.EstadoService;
import py.com.sepsa.erp.web.v1.info.remote.ReporteEmpresaService;

/**
 * Adaptador para reporte empresa
 * @author Jonathan Bernal
 */
public class ReporteEmpresaAdapter extends DataListAdapter<ReporteEmpresa> {

    private final ReporteEmpresaService estadoService;
   
    
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ReporteEmpresaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.estadoService = new ReporteEmpresaService();
        this.filter = new ReporteEmpresa();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public ReporteEmpresaAdapter() {
        super();
        this.estadoService = new ReporteEmpresaService();
    }

    @Override
    public ReporteEmpresaAdapter fillData(ReporteEmpresa searchData) {
        return estadoService.find(searchData, getFirstResult(),getPageSize());
    }
}



