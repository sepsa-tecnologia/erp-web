/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 *
 * @author Gustavo Benítez
 */
public class Vendedorfilter extends Filter {

    /**
     * Agrega el filtro de identificador deL Vendedor
     *
     * @param id
     * @return
     */
    public Vendedorfilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la empresa
     *
     * @param idEmpresa
     * @return
     */
    public Vendedorfilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona
     * @return
     */
    public Vendedorfilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro para el estado activo
     *
     * @param activo
     * @return
     */
    public Vendedorfilter activo(Character activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de listado pojo
     *
     * @param listadoPojo Listado pojo
     * @return
     */
    public Vendedorfilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param vende
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(VendedorPojo vende, Integer page, Integer pageSize) {
        Vendedorfilter filter = new Vendedorfilter();
        filter
                .id(vende.getId())
                .idEmpresa(vende.getIdEmpresa())
                .idPersona(vende.getIdPersona())
                .activo(vende.getActivo())
                .listadoPojo(vende.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public Vendedorfilter() {
        super();
    }
}
