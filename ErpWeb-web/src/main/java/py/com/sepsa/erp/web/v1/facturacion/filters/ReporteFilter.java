package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para reporte de venta
 *
 * @author alext
 */
public class ReporteFilter extends Filter {

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public ReporteFilter fechaDesde(Date fechaDesde) {

        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public ReporteFilter fechaHasta(Date fechaHasta) {

        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param venta
     * @return
     */
    public static Map build(ReporteParam venta) {
        ReporteFilter filter = new ReporteFilter();

        filter
                .fechaDesde(venta.getFechaDesde())
                .fechaHasta(venta.getFechaHasta());

        return filter.getParams();

    }

    /**
     * Constructor de la clase
     */
    public ReporteFilter() {
        super();
    }

}
