package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.CargoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoContactoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de tipo contacto
 *
 * @author Cristina Insfrán
 */
public class TipoContactoService extends APIErpCore {

    /**
     * Obtiene la lista de tipo contacto
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public TipoContactoListAdapter getTipoContactoList(TipoContacto dato, Integer page,
            Integer pageSize) {

        TipoContactoListAdapter lista = new TipoContactoListAdapter();

        Map params = TipoContactoFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_CONTACTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoContactoListAdapter.class);

            lista = (TipoContactoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Obtiene la lista de cargos
     *
     * @param dato
     * @param page
     * @param pageSize
     * @return
     */
    public CargoListAdapter getCargoList(TipoContacto dato, Integer page,
            Integer pageSize) {

        CargoListAdapter lista = new CargoListAdapter();

        Map params = TipoContactoFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.CARGO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CargoListAdapter.class);

            lista = (CargoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de TipoContactoServiceClient
     */
    public TipoContactoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_CONTACTO("Servicio Tipo Contacto", "tipo-contacto"),
        CARGO("Servicio para cargo", "cargo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
