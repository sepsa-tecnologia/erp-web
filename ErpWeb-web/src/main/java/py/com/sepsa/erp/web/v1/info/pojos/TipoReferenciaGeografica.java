
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para referencia geografica
 * @author Cristina Insfrán
 */
public class TipoReferenciaGeografica {

    /**
     * Identificador de tipoReferenciaGeografica
     */
    private Integer id;

    /**
     * Parámetro código
     */
    private String codigo;

    /**
     * Parámetro descripción
     */
    private String descripcion;
    
    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    //</editor-fold>
    
    /**
     * Constructor
     */
    public TipoReferenciaGeografica(){
        
    }
    
}
