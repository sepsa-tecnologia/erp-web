package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;

/**
 * Filtro para el servicio de configuración de aprobación.
 *
 * @author Alexnder Triana.
 */
public class ConfiguracionAprobacionFilter extends Filter {

    /**
     * Agrega el filtro por identificador de configuración de aprobación.
     *
     * @param id
     * @return
     */
    public ConfiguracionAprobacionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por id_tipo_etiqueta.
     *
     * @param idTipoEtiqueta
     * @return
     */
    public ConfiguracionAprobacionFilter idTipoEtiqueta(Integer idTipoEtiqueta) {
        if (idTipoEtiqueta != null) {
            params.put("idTipoEtiqueta", idTipoEtiqueta);
        }
        return this;
    }

    /**
     * Filtro por tipoEtiqueta.
     *
     * @param tipoEtiqueta
     * @return
     */
    public ConfiguracionAprobacionFilter tipoEtiqueta(String tipoEtiqueta) {
        if (tipoEtiqueta != null && tipoEtiqueta.trim().isEmpty()) {
            params.put("tipoEtiqueta", tipoEtiqueta);
        }
        return this;
    }

    /**
     * Agrega el filtro por id_etiqueta.
     *
     * @param idEtiqueta
     * @return
     */
    public ConfiguracionAprobacionFilter idEtiqueta(Integer idEtiqueta) {
        if (idEtiqueta != null) {
            params.put("idEtiqueta", idEtiqueta);
        }
        return this;
    }

    /**
     * Filtro por etiqueta.
     *
     * @param etiqueta
     * @return
     */
    public ConfiguracionAprobacionFilter etiqueta(String etiqueta) {
        if (etiqueta != null && etiqueta.trim().isEmpty()) {
            params.put("etiqueta", etiqueta);
        }
        return this;
    }

    /**
     * Agrega el filtro por orden.
     *
     * @param orden
     * @return
     */
    public ConfiguracionAprobacionFilter orden(Integer orden) {
        if (orden != null) {
            params.put("orden", orden);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado.
     *
     * @param estado
     * @return
     */
    public ConfiguracionAprobacionFilter estado(Character estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param configAprobacion
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(ConfiguracionAprobacion configAprobacion, Integer page, Integer pageSize) {
        ConfiguracionAprobacionFilter filter = new ConfiguracionAprobacionFilter();

        filter
                .id(configAprobacion.getId())
                .idTipoEtiqueta(configAprobacion.getIdTipoEtiqueta())
                .tipoEtiqueta(configAprobacion.getTipoEtiqueta())
                .idEtiqueta(configAprobacion.getIdEtiqueta())
                .etiqueta(configAprobacion.getEtiqueta())
                .orden(configAprobacion.getOrden())
                .estado(configAprobacion.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor ConfiguracionAprobacionFilter.
     */
    public ConfiguracionAprobacionFilter() {
        super();
    }

}
