package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO de producto
 *
 * @author Romina E. Núñez Rojas
 */
public class Producto {

    /**
     * Identificador del producto
     */
    private Integer id;
    /**
     * Descripción del producto
     */
    private String descripcion;
    /**
     * Código del producto
     */
    private String codigo;
   
    /**
     * Bonificable
     */
    private String bonificable;
    /**
     * Estado del producto
     */
    private String activo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setBonificable(String bonificable) {
        this.bonificable = bonificable;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getBonificable() {
        return bonificable;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

//</editor-fold>
    /**
     * Contructor de la clase
     */
    public Producto() {

    }

    /**
     * Constructor con parámetros
     *
     * @param id
     * @param descripcion
     * @param codigo
     * @param bonificable
     */
    public Producto(Integer id, String descripcion, String codigo, String bonificable) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.bonificable = bonificable;
    }

}
