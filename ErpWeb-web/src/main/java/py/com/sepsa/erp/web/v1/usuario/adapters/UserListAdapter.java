package py.com.sepsa.erp.web.v1.usuario.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;

/**
 * Adaptador para la lista de usuario
 *
 * @author Cristina Insfrán
 */
public class UserListAdapter extends DataListAdapter<Usuario> {

    /**
     * Cliente para los servicios de clientes
     */
    private final UserServiceClient userClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
   @Override
    public UserListAdapter fillData(Usuario searchData) {
     
        return userClient.getUserList(searchData, getFirstResult(), getPageSize());

    }

    /**
     * Constructor de ClientListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UserListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.userClient = new UserServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public UserListAdapter() {
        super();
        this.userClient = new UserServiceClient();
    }

}
