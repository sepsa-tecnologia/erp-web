/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;

/**
 * Controlador para editar sucursal
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("sucursalEdit")
public class SucursalEditController implements Serializable {

    /**
     * Cliente para el servicio de local.
     */
    private final LocalService service;
    /**
     * POJO del Local
     */
    private Local local;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    //</editor-fold>

    /**
     * Método para editar Local
     */
    public void edit() {
        BodyResponse<Local> respuestaLocal = service.editLocal(this.local);
        if (respuestaLocal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Sucursal editado correctamente!"));
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public void filterCliente() {
        cliente.setIdCliente(local.getIdPersona());
        clientAdapter = clientAdapter.fillData(cliente);
        cliente.setRazonSocial(clientAdapter.getData().get(0).getRazonSocial());
        cliente.setNroDocumento(clientAdapter.getData().get(0).getNroDocumento());
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.local = service.get(id);
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        filterCliente();
    }

    /**
     * Constructor
     */
    public SucursalEditController() {
        this.service = new LocalService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
