package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.RetencionDetalleFilter;
import py.com.sepsa.erp.web.v1.facturacion.filters.RetencionFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionDetalle;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de retención
 *
 * @author alext
 */
public class RetencionService extends APIErpFacturacion {

    public RetencionAdapter getRetencionList(Retencion ret, Integer page,
            Integer pageSize) {

        RetencionAdapter lista = new RetencionAdapter();

        Map params = RetencionFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.RETENCION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    RetencionAdapter.class);

            lista = (RetencionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Detalle de retención
     *
     * @param ret
     * @param page
     * @param pageSize
     * @return
     */
    public RetencionDetalleListAdapter getDetalleRetencionList(RetencionDetalle ret, Integer page,
            Integer pageSize) {

        RetencionDetalleListAdapter lista = new RetencionDetalleListAdapter();

        Map params = RetencionDetalleFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.DETALLE_RETENCION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    RetencionDetalleListAdapter.class);

            lista = (RetencionDetalleListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para anular retencion
     *
     * @param retencion
     * @return
     */
    public BodyResponse anularRetencion(Retencion retencion) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "retencion/anular/" + retencion.getId();

        HttpURLConnection conn = PUT(ANULAR_URL, ContentType.JSON);

        if (conn != null) {
               Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(retencion));
            response = BodyResponse.createInstance(conn, Retencion.class);

            conn.disconnect();
        }
        return response;
    }
    
    /**
     *
     * @param retencion
     * @return
     */
    public BodyResponse setRetencionCompra(Retencion retencion) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.RETENCION_COMPRA.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(retencion));

            response = BodyResponse.createInstance(conn, Retencion.class);

        }
        return response;
    }

    /**
     *
     * @param retencion
     * @return
     */
    public BodyResponse setRetencion(Retencion retencion) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.RETENCION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(retencion));

            response = BodyResponse.createInstance(conn, Retencion.class);

        }
        return response;
    }

    /**
     * Método para editar retencion
     *
     * @param retencion
     * @return
     */
    public Retencion editRetencion(Retencion retencion) {

        Retencion retencionEdit = new Retencion();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.RETENCION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(retencion));

            response = BodyResponse.createInstance(conn, Retencion.class);

            retencionEdit = ((Retencion) response.getPayload());

        }
        return retencionEdit;
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        RETENCION("Servicio para Retencion", "retencion"),
        RETENCION_COMPRA("Servicio para Retencion", "retencion-compra"),
        DETALLE_RETENCION("Servicio para el detalle de retencion", "retencion-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
