
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.facturacion.remote.TransportistaService;

/**
 *
 * @author Williams Vera
 */
@FacesConverter("transportistaPojoConverter")
public class TransportistaPojoConverter implements Converter{
    
    /**
     * Servicio para producto
     */
    private TransportistaService serviceTransportista;
    
     @Override
    public Transportista getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceTransportista = new TransportistaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
          
            Transportista trans=new Transportista();
            trans.setId(val);
          
            List<Transportista> transportistas = serviceTransportista.getTransportistaList(trans, 0, 10).getData();
            if(transportistas != null && !transportistas.isEmpty()) {
                return transportistas.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un transportista válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Transportista) {
                return String.valueOf(((Transportista)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
