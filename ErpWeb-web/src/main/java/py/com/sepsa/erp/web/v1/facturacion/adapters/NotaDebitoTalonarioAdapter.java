/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoTalonarioAdapter extends DataListAdapter<TalonarioPojo> {

    private final NotaDebitoService serviceND;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaDebitoTalonarioAdapter fillData(TalonarioPojo searchData) {

        return serviceND.getNDTalonarioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaDebitoTalonarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceND = new NotaDebitoService();
    }

    /**
     * Constructor de NotaDebitoTalonarioAdapter
     */
    public NotaDebitoTalonarioAdapter() {
        super();
        this.serviceND = new NotaDebitoService();
    }
}
