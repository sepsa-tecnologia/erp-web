/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.repositorio.adapters.TipoRepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;
import py.com.sepsa.erp.web.v1.repositorio.remote.RepositorioService;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para editar Repositorio
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("repositorioEdit")
public class RepositorioEditController implements Serializable {

    /**
     * Cliente para el servicio de Repositorio.
     */
    private RepositorioService service;

    /**
     * Pojo del repositorio
     */
    private Repositorio repositorio;

    /**
     * Adaptador para la lista de Tipos de Repositorios
     */
    private TipoRepositorioAdapter tipoRepositorioListAdapter;

    /**
     * Pojo de tipo Repositorio
     */
    private TipoRepositorio tipoRepositorio;

    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;

    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the service
     */
    public RepositorioService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(RepositorioService service) {
        this.service = service;
    }

    /**
     * @return the repositorio
     */
    public Repositorio getRepositorio() {
        return repositorio;
    }

    /**
     * @param repositorio the repositorio to set
     */
    public void setRepositorio(Repositorio repositorio) {
        this.repositorio = repositorio;
    }

    public TipoRepositorioAdapter getTipoRepositorioListAdapter() {
        return tipoRepositorioListAdapter;
    }

    public void setTipoRepositorioListAdapter(TipoRepositorioAdapter tipoRepositorioListAdapter) {
        this.tipoRepositorioListAdapter = tipoRepositorioListAdapter;
    }

    public TipoRepositorio getTipoRepositorio() {
        return tipoRepositorio;
    }

    public void setTipoRepositorio(TipoRepositorio tipoRepositorio) {
        this.tipoRepositorio = tipoRepositorio;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    //</editor-fold>
    /**
     * Método para editar un Repositorio.
     */
    public void edit() {
        BodyResponse<Repositorio> respuestaDeRepositorio = service.editRepositorio(repositorio);

        if (respuestaDeRepositorio.getSuccess()) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "¡Repositorio editado con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            for (Mensaje mensaje : respuestaDeRepositorio.getStatus().getMensajes()) {
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "¡Error al editar Repositorio!", mensaje.getDescripcion());
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }

        }
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        repositorio.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método para obtener los Tipo de Repositorios
     */
    public void getTipoRepositorios() {
        this.setTipoRepositorioListAdapter(getTipoRepositorioListAdapter().fillData(getTipoRepositorio()));
    }

    /**
     * Inicializa los datos el repositorio
     *
     * @param id Identificador de la entidad configuracionAprobacion.
     */
    private void init(int id) {
        this.repositorio = service.get(id);
        this.tipoRepositorioListAdapter = new TipoRepositorioAdapter();
        this.tipoRepositorio = new TipoRepositorio();
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        getTipoRepositorios();
    }

    /**
     * Constructor de RepositorioEditController
     */
    public RepositorioEditController() {
        this.service = new RepositorioService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
}
