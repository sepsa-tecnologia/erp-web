
package py.com.sepsa.erp.web.v1.http.pojos;

/**
 * POJO para la respuesta HTTP
 * @author Daniel F. Escauriza Arza
 * @param <T> Cláse genérica
 */
public class HttpResponse<T> {
    
    /**
     * Código de la respuesta
     */
    protected ResponseCode code;
    
    /**
     * Input de la respuesta
     */
    protected T input;

    /**
     * Obtiene el código de la respuesta
     * @return Código de la respuesta
     */
    public ResponseCode getCode() {
        return code;
    }

    /**
     * Setea el código de la respuesta
     * @param code Código de la respuesta
     */
    public void setCode(ResponseCode code) {
        this.code = code;
    }

    /**
     * Obtiene el input de la respuesta
     * @return Input de la respuesta
     */
    public T getInput() {
        return input;
    }

    /**
     * Setea el input de la respuesta
     * @param input Input de la respuesta
     */
    public void setInput(T input) {
        this.input = input;
    }
    
    /**
     * Constructor de HttpResponse
     * @param code Código de la respuesta
     * @param input Input de datos
     */
    public HttpResponse(ResponseCode code, T input) {
        this.code = code;
        this.input = input;
    }

    /**
     * Constructor de HttpResponse
     */
    public HttpResponse() {}
    
    /**
     * Códigos de respuesta
     */
    public enum ResponseCode {
        OK, BR, ERROR;
    }
}
