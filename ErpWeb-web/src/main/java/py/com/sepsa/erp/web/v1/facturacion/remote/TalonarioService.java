/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioLocalRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioFilter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioLocalFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioLocal;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Talonario
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class TalonarioService extends APIErpFacturacion {

    /**
     * Obtiene la lista de talonarios
     *
     * @param talonario Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TalonarioAdapter getTalonarioList(Talonario talonario, Integer page,
            Integer pageSize) {

        TalonarioAdapter lista = new TalonarioAdapter();

        Map params = TalonarioFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.TALONARIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TalonarioAdapter.class);

            lista = (TalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param talonario
     * @return
     */
    public BodyResponse<Talonario> setTalonario(Talonario talonario) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.TALONARIO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(talonario));

            response = BodyResponse.createInstance(conn, Talonario.class);

        }
        return response;
    }

    /**
     * Método para editar talonario
     *
     * @param talonario
     * @return
     */
    public BodyResponse<Talonario> editTalonario(Talonario talonario) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.TALONARIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(talonario));
            response = BodyResponse.createInstance(conn, Talonario.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Talonario get(Integer id) {
        Talonario data = new Talonario(id);
        TalonarioAdapter list = getTalonarioList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Obtiene la lista de usuario local
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public TalonarioLocalRelacionadoAdapter getTalonarioLocalRelacionadoList(TalonarioLocal dato, Integer page,
            Integer pageSize) {

        TalonarioLocalRelacionadoAdapter lista = new TalonarioLocalRelacionadoAdapter();

        Map params = TalonarioLocalFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.TALONARIO_LOCAL_RELACIONADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TalonarioLocalRelacionadoAdapter.class);

            lista = (TalonarioLocalRelacionadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para enviar y recibir el Usuario Local editado/creado
     *
     * @param usuarioLocal
     * @return
     */
    public BodyResponse<TalonarioLocal> editAsociacionTalonarioLocal(TalonarioLocal talonarioLocal) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.TALONARIO_LOCAL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(talonarioLocal));

            response = BodyResponse.createInstance(conn, TalonarioLocal.class);

        }
        return response;
    }

    /**
     * Constructor de clase
     */
    public TalonarioService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TALONARIO_LOCAL_RELACIONADO("Servicio Talonario Local Relacionado", "talonario-local/relacionado"),
        TALONARIO_LOCAL("Servicio Talonario Local", "talonario-local"),
        TALONARIO("talonario", "talonario");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
