package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO para cliente
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ClientePojo {

    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * id del estado
     */
    private Integer idEmpresa;
    /**
     * Estado
     */
    private String empresa;
    /**
     * id del estado
     */
    private Integer idEstado;
    /**
     * Estado
     */
    private String estado;
    /**
     * Código de estado
     */
    private String codigoEstado;
    /**
     * Código de estado
     */
    private String proveedor;
    /**
     * Código de estado
     */
    private String comprador;
    /**
     * Razón social
     */
    private String razonSocial;
    /**
     * Número de documento
     */
    private String nroDocumento;
    /**
     * Identificador tipo de documento
     */
    private Integer idTipoDocumento;
    /**
     * Tipo de documento
     */
    private String tipoDocumento;
    /**
     * Tipo de documento
     */
    private String codigoTipoPersona;
    /**
     * Identificador de canal de venta
     */
    private Integer idDireccion;
    /**
     * Canal de venta
     */
    private String direccion;
    /**
     * Clave de pago
     */
    private Integer idDepartamento;
    /**
     * Clave de pago
     */
    private String departamento;
    /**
     * Clave de pago
     */
    private Integer idDistrito;
    /**
     * Clave de pago
     */
    private String distrito;
    /**
     * Clave de pago
     */
    private Integer idCiudad;
        /**
     * Clave de pago
     */
    private Integer idCanalVenta;
    /**
     * Clave de pago
     */
    private String ciudad;
    /**
     * Factura electronica
     */
    private String numeroDireccion;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * Código de Naturaleza de Cliente
     */
    private String codigoNaturalezaCliente;
        /**
     * Clave de pago
     */
    private Integer idNaturalezaCliente;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getCodigoTipoPersona() {
        return codigoTipoPersona;
    }

    public void setCodigoTipoPersona(String codigoTipoPersona) {
        this.codigoTipoPersona = codigoTipoPersona;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getNumeroDireccion() {
        return numeroDireccion;
    }

    public void setNumeroDireccion(String numeroDireccion) {
        this.numeroDireccion = numeroDireccion;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public ClientePojo() {
    }

}
