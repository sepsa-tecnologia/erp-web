/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;
import py.com.sepsa.erp.web.v1.info.remote.TipoDatoServiceClient;
/**
 *
 * @author Sepsa
 */

public class TipoDatoListAdapter extends DataListAdapter<TipoDato>  {
    
     /**
     * Cliente para los servicios de clientes
     */
    private final TipoDatoServiceClient tipoDatoClient;

    /**
     * Constructor de TipoDatoPersonaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoDatoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoDatoClient = new TipoDatoServiceClient();
    }

    /**
     * Constructor de TipoDatoListAdapter
     */
    public TipoDatoListAdapter() {
        super();
        this.tipoDatoClient = new TipoDatoServiceClient();
    }

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoDatoListAdapter fillData(TipoDato searchData) {
        return tipoDatoClient.getTipoDatoList(searchData, getFirstResult(),getPageSize());
    }
    

}