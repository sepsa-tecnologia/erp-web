
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.remote.ContactoEmailService;

/**
 * Adaptador de la lista de contacto email
 * @author Cristina Insfrán
 */
public class ContactoEmailAdapter extends DataListAdapter<ContactoEmail> {
    
    /**
     * Cliente para el servicio de contacto email
     */
    private final ContactoEmailService contactoEmailService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoEmailAdapter fillData(ContactoEmail searchData) {
     
        return contactoEmailService.getContactoEmailList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ContactoEmailAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoEmailAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoEmailService = new ContactoEmailService();
    }

    /**
     * Constructor de ContactoEmailAdapter
     */
    public ContactoEmailAdapter() {
        super();
        this.contactoEmailService = new ContactoEmailService();
    }
}
