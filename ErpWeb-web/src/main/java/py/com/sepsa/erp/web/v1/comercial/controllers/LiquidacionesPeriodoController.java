package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionesPeriodoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesPeriodo;

/**
 * Controlador para la lista de liquidaciones por periodo.
 *
 * @author alext
 */
@ViewScoped
@Named("liquidacionesPeriodo")
public class LiquidacionesPeriodoController implements Serializable {

    private final String ano;

    private final String mes;

    /**
     * Adaptador de liquidaciones por periodo.
     */
    private LiquidacionesPeriodoAdapter adapter;

    /**
     * Objeto liquidaciones por periodo.
     */
    private LiquidacionesPeriodo searchData;

    /**
     * Cantidad de filas en la tabla.
     */
    Integer rows = 10;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public LiquidacionesPeriodoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LiquidacionesPeriodoAdapter adapter) {
        this.adapter = adapter;
    }

    public LiquidacionesPeriodo getSearchData() {
        return searchData;
    }

    public void setSearchData(LiquidacionesPeriodo searchData) {
        this.searchData = searchData;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
    //</editor-fold>

    /**
     * Método para generar lista de liquidaciones por periodo.
     */
    public void search() {
        adapter = new LiquidacionesPeriodoAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new LiquidacionesPeriodo(ano, mes);
        search();
    }

    /**
     * Actualiza la lista de liquidaciones.
     */
    public void loadLiquidaciones() {
        adapter.setPageSize(rows);
        adapter = adapter.fillData(searchData);
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public LiquidacionesPeriodoController() {
        adapter = new LiquidacionesPeriodoAdapter();
        searchData = new LiquidacionesPeriodo();
        Calendar now = Calendar.getInstance();
        int m = now.get(Calendar.MONTH) + 1;
        int a = now.get(Calendar.YEAR);
        String mes = String.valueOf(m);
        mes = mes.length() == 1 ? "0" + mes : mes;
        this.mes = mes;
        this.ano = String.valueOf(a);
    }
}
