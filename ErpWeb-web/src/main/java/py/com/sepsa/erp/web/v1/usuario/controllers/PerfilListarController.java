package py.com.sepsa.erp.web.v1.usuario.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.usuario.adapters.PerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.remote.PerfilServiceClient;

/**
 * Controlador para perfil
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("listarPerfil")
public class PerfilListarController implements Serializable {

    /**
     * Adaptador de la lista de perfiles
     */
    private PerfilListAdapter adapterPerfil;
    /**
     * Entidad perfil
     */
    private Perfil perfil;
    /**
     * Servicio Perfil
     */
    private PerfilServiceClient servicePerfil;
    /**
     * Pojo para empresa
     */
    private Empresa empresaComplete;
    /**
     * Adapter para empresa
     */
    private EmpresaAdapter adapterEmpresa;

    private Perfil perfilEditarPerms;
    
    public Perfil getPefilEditarPerms() {
        return perfilEditarPerms;
    }

    public void setPefilEditarPerms(Perfil pefilEditarPerms) {
        this.perfilEditarPerms = pefilEditarPerms;
    }
    
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * @return the adapterPerfil
     */
    public PerfilListAdapter getAdapterPerfil() {
        return adapterPerfil;
    }

    public void setServicePerfil(PerfilServiceClient servicePerfil) {
        this.servicePerfil = servicePerfil;
    }

    public PerfilServiceClient getServicePerfil() {
        return servicePerfil;
    }

    /**
     * @param adapterPerfil the adapterPerfil to set
     */
    public void setAdapterPerfil(PerfilListAdapter adapterPerfil) {
        this.adapterPerfil = adapterPerfil;
    }

    /**
     * @return the perfil
     */
    public Perfil getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Empresa getEmpresaComplete() {
        return empresaComplete;
    }

    public void setEmpresaComplete(Empresa empresaComplete) {
        this.empresaComplete = empresaComplete;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    //</editor-fold>

    /**
     * Método para filtrar perfil
     */
    public void filterPerfil() {
        adapterPerfil.setFirstResult(0);
        adapterPerfil = adapterPerfil.fillData(perfil);
    }

    /**
     * Método para limpiar los filtros
     */
    public void clean() {
        perfil = new Perfil();
        filterPerfil();
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa empresa = new Empresa();
        empresa.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(empresa);
        return adapterEmpresa.getData();
    } 

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        empresaComplete.setId(((Empresa) event.getObject()).getId());
    }

    /**
     * Método para editar el cobro
     *
     * @param event
     */
    public void onRowEditPerfil(RowEditEvent event) {
        Perfil perfilEdit = new Perfil();
        perfilEdit = ((Perfil) event.getObject());

        perfilEdit = servicePerfil.editPerfil(perfilEdit);

        if (perfilEdit != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Error al realizar el cambio!"));
        }

    }
    
    public void forceReload() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }
    
    
    private String permisos;

    public String getPermisos() {
        return permisos;
    }

    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }
    public void mostrarDatos(Perfil perfil){
        this.perfilEditarPerms = perfil;
        if(this.perfilEditarPerms.getPermisos()!=null){
            this.permisos = perfilEditarPerms.getPermisos().toString();

        } else{
            setPermisos("");
        }
    }
    
    public void editarPermisos(){
//        this.perfilEditarPerms = new Perfil();
//        this.perfilEditarPerms.setPermisos(new Gson().fromJson(this.permisos, JsonElement.class));
        JsonElement jPerms = null;

        if(permisos == null || permisos.trim().equals("")){
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "JSON Inexistente!"));
        }else{
            try {
                jPerms = new Gson().fromJson(permisos, JsonElement.class);
            } catch (Exception ex) {
                FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "JSON no valido!"));
            }
        }
        if(jPerms != null){
            this.perfilEditarPerms.setPermisos(jPerms);
            this.perfilEditarPerms = servicePerfil.editPerfil(perfilEditarPerms);
            if (perfilEditarPerms != null) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Info", "Cambio correcto!"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", "Error al realizar el cambio!"));
            }
        }
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapterPerfil = new PerfilListAdapter();
        this.perfil = new Perfil();
        this.perfilEditarPerms = new Perfil();
        this.servicePerfil = new PerfilServiceClient();
        this.empresaComplete = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
        
        filterPerfil();
    }
}
