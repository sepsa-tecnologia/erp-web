/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.remote.DepositoLogisticoService;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("crearDeposito")
public class DepositoLogisticoCreateController implements Serializable {

    /**
     * POJO de Deposito
     */
    private DepositoLogistico deposito;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de tipo documento
     */
    private Local localFilter;
    private DepositoLogisticoService service;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public void setService(DepositoLogisticoService service) {
        this.service = service;
    }

    public DepositoLogisticoService getService() {
        return service;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setDeposito(DepositoLogistico deposito) {
        this.deposito = deposito;
    }

    public DepositoLogistico getDeposito() {
        return deposito;
    }

    //</editor-fold>
    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.deposito = new DepositoLogistico();
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        local.setLocalExterno("N");
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        deposito.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Método para crear
     */
    public void create() {
        this.deposito.setActivo("S");
        BodyResponse<DepositoLogistico> respuestaDep = service.setDeposito(deposito);

        if (respuestaDep.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Deposito creado correctamente!"));
            clear();
        }

    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.deposito = new DepositoLogistico();

        this.localFilter = new Local();
        this.localAdapterList = new LocalListAdapter();

        this.service = new DepositoLogisticoService();
    }

    /**
     * Constructor
     */
    public DepositoLogisticoCreateController() {
        init();
    }

}
