
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.adapters.DevolucionAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.DevolucionDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.TipoOperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.inventario.pojos.DevolucionDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;

/**
 * Controlador para la vista listar operaciones
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("devolucionList")
public class DevolucionListController implements Serializable {

    /**
     * POJO de operacion
     */
    private Devolucion devolucionFilter;
    /**
     * Adaptador para la lista de operacion
     */
    private DevolucionAdapter devolucionAdapterList;
    /**
     * Pojo de detalle de operación de inventario
     */
    private DevolucionDetalle devolucionDetalle;
    /**
     * Adaptador de detalle de operación de inventario
     */
    private DevolucionDetalleAdapter devolucionDetalleAdapter;
    /**
     * POJO de tipo motivo
     */
    private TipoOperacion tipoOperacionFilter;
    /**
     * Adaptador para la lista de tipo operacion
     */
    private TipoOperacionAdapter tipoOperacionAdapterList;
    /**
     * POJO de Motivo
     */
    private Motivo motivoOperacionFilter;
    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoOperacionAdapterList;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Cliente
     */
    private Cliente proveedor;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterProveedor;
    /**
     * Adaptador para la lista de local origen
     */
    private LocalListAdapter localOrigenAdapterList;
    /**
     * POJO de local origen
     */
    private Local localOrigenFilter;
    /**
     * Adaptador para la lista de local Destino
     */
    private LocalListAdapter localDestinoAdapterList;
    /**
     * POJO de local destino
     */
    private Local localDestinoFilter;

    /**
     * Adaptador para la lista de estados
     */
    private EstadoAdapter estadoAdapterList;

    /**
     * POJO de estado
     */
    private Estado estado;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setAdapterProveedor(ClientListAdapter adapterProveedor) {
        this.adapterProveedor = adapterProveedor;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setProveedor(Cliente proveedor) {
        this.proveedor = proveedor;
    }

    public Cliente getProveedor() {
        return proveedor;
    }

    public ClientListAdapter getAdapterProveedor() {
        return adapterProveedor;
    }

    public TipoOperacion getTipooperacionFilter() {
        return tipoOperacionFilter;
    }

    public void setTipooperacionFilter(TipoOperacion tipoOperacionFilter) {
        this.tipoOperacionFilter = tipoOperacionFilter;
    }

    public TipoOperacionAdapter getTipooperacionAdapterList() {
        return tipoOperacionAdapterList;
    }

    public void setTipooperacionAdapterList(TipoOperacionAdapter tipoOperacionAdapterList) {
        this.tipoOperacionAdapterList = tipoOperacionAdapterList;
    }

    public Motivo getMotivooperacionFilter() {
        return motivoOperacionFilter;
    }

    public void setMotivooperacionFilter(Motivo motivoOperacionFilter) {
        this.motivoOperacionFilter = motivoOperacionFilter;
    }

    public MotivoAdapter getMotivooperacionAdapterList() {
        return motivoOperacionAdapterList;
    }

    public void setMotivooperacionAdapterList(MotivoAdapter motivoOperacionAdapterList) {
        this.motivoOperacionAdapterList = motivoOperacionAdapterList;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public LocalListAdapter getLocalOrigenAdapterList() {
        return localOrigenAdapterList;
    }

    public void setLocalOrigenAdapterList(LocalListAdapter localOrigenAdapterList) {
        this.localOrigenAdapterList = localOrigenAdapterList;
    }

    public Local getLocalOrigenFilter() {
        return localOrigenFilter;
    }

    public void setLocalOrigenFilter(Local localOrigenFilter) {
        this.localOrigenFilter = localOrigenFilter;
    }

    public LocalListAdapter getLocalDestinoAdapterList() {
        return localDestinoAdapterList;
    }

    public void setLocalDestinoAdapterList(LocalListAdapter localDestinoAdapterList) {
        this.localDestinoAdapterList = localDestinoAdapterList;
    }

    public Local getLocalDestinoFilter() {
        return localDestinoFilter;
    }

    public void setLocalDestinoFilter(Local localDestinoFilter) {
        this.localDestinoFilter = localDestinoFilter;
    }

    public TipoOperacion getTipoOperacionFilter() {
        return tipoOperacionFilter;
    }

    public void setTipoOperacionFilter(TipoOperacion tipoOperacionFilter) {
        this.tipoOperacionFilter = tipoOperacionFilter;
    }

    public TipoOperacionAdapter getTipoOperacionAdapterList() {
        return tipoOperacionAdapterList;
    }

    public void setTipoOperacionAdapterList(TipoOperacionAdapter tipoOperacionAdapterList) {
        this.tipoOperacionAdapterList = tipoOperacionAdapterList;
    }

    public Motivo getMotivoOperacionFilter() {
        return motivoOperacionFilter;
    }

    public void setMotivoOperacionFilter(Motivo motivoOperacionFilter) {
        this.motivoOperacionFilter = motivoOperacionFilter;
    }

    public MotivoAdapter getMotivoOperacionAdapterList() {
        return motivoOperacionAdapterList;
    }

    public void setMotivoOperacionAdapterList(MotivoAdapter motivoOperacionAdapterList) {
        this.motivoOperacionAdapterList = motivoOperacionAdapterList;
    }

    public DevolucionAdapter getDevolucionAdapterList() {
        return devolucionAdapterList;
    }

    public DevolucionDetalle getDevolucionDetalle() {
        return devolucionDetalle;
    }

    public DevolucionDetalleAdapter getDevolucionDetalleAdapter() {
        return devolucionDetalleAdapter;
    }

    public Devolucion getDevolucionFilter() {
        return devolucionFilter;
    }

    public void setDevolucionAdapterList(DevolucionAdapter devolucionAdapterList) {
        this.devolucionAdapterList = devolucionAdapterList;
    }

    public void setDevolucionDetalle(DevolucionDetalle devolucionDetalle) {
        this.devolucionDetalle = devolucionDetalle;
    }

    public void setDevolucionDetalleAdapter(DevolucionDetalleAdapter devolucionDetalleAdapter) {
        this.devolucionDetalleAdapter = devolucionDetalleAdapter;
    }

    public void setDevolucionFilter(Devolucion devolucionFilter) {
        this.devolucionFilter = devolucionFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener operaciones
     */
    public void getDevoluciones() {
        getDevolucionAdapterList().setFirstResult(0);
        this.devolucionFilter.setListadoPojo("true");
        this.devolucionAdapterList = this.devolucionAdapterList.fillData(devolucionFilter);
    }

    /**
     * Método para filtrar devoluciones
     *
     * @param idDevolucion
     */
    public void filterOperacionInvetarioDetalle(Integer idDevolucion) {
        devolucionDetalle.setListadoPojo("true");
        devolucionDetalle.setIdDevolucion(idDevolucion);
        devolucionDetalleAdapter = devolucionDetalleAdapter.fillData(devolucionDetalle);
    }

    /**
     * Método para obtener los motivos de operaciones filtradas
     *
     * @param query
     * @return
     */
    public List<Motivo> completeQueryMotivoOperacion(String query) {
        Motivo object = new Motivo();
        object.setDescripcion(query);
        motivoOperacionAdapterList = motivoOperacionAdapterList.fillData(object);
        return motivoOperacionAdapterList.getData();
    }

    /**
     * Selecciona el motivo de operacion
     *
     * @param event
     */
    public void onItemSelectMotivooperacionFilter(SelectEvent event) {
        motivoOperacionFilter.setId(((Motivo) event.getObject()).getId());
        //     this.operacionFilter.setIdMotivo(motivoOperacionFilter.getId());
    }

    /**
     * Método para obtener los tipos de operaciones filtradas
     *
     * @param query
     * @return
     */
    public List<TipoOperacion> completeQueryTipoOperacion(String query) {
        tipoOperacionFilter = new TipoOperacion();
        tipoOperacionFilter.setDescripcion(query);
        tipoOperacionAdapterList = tipoOperacionAdapterList.fillData(tipoOperacionFilter);
        return tipoOperacionAdapterList.getData();
    }

    /**
     * Selecciona el tipo de operacion
     *
     * @param event
     */
    public void onItemSelectTipoOperacionFilter(SelectEvent event) {
        tipoOperacionFilter.setId(((TipoOperacion) event.getObject()).getId());
        //      this.operacionFilter.setIdTipoOperacion(tipoOperacionFilter.getId());
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(cliente);
        return adapterCliente.getData();
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQueryProveedor(String query) {
        Cliente proveedor = new Cliente();
        proveedor.setRazonSocial(query);
        proveedor.setProveedor("S");
        adapterProveedor = adapterProveedor.fillData(proveedor);
        return adapterProveedor.getData();
    }

    /**
     * Selecciona cliente en Autocomplete y lo guarda en operacionFilter
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        //       this.operacionFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /* Método para obtener los locales origen filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocalOrigen(String query) {
        localOrigenFilter = new Local();
        localOrigenFilter.setLocalExterno("N");
        localOrigenFilter.setDescripcion(query);
        localOrigenAdapterList = localOrigenAdapterList.fillData(localOrigenFilter);
        return localOrigenAdapterList.getData();
    }

    /**
     * Selecciona local Origen
     *
     * @param event
     */
    public void onItemSelectLocalOrigen(SelectEvent event) {
        //    this.operacionFilter.setIdLocalOrigen(((Local) event.getObject()).getId());
    }

    /* Método para obtener los locales destinos filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocalDestino(String query) {
        localDestinoFilter = new Local();
        localDestinoFilter.setLocalExterno("N");
        localDestinoFilter.setDescripcion(query);
        localDestinoAdapterList = localDestinoAdapterList.fillData(localDestinoFilter);
        return localDestinoAdapterList.getData();
    }

    /**
     * Selecciona local Destino
     *
     * @param event
     */
    public void onItemSelectLocalDestino(SelectEvent event) {
        //     this.operacionFilter.setIdLocalDestino(((Local) event.getObject()).getId());
    }

    /**
     * Método para seleccionar el proveedor
     *
     * @param event
     */
    public void onItemSelectProveedor(SelectEvent event) {
        //     this.operacionFilter.setIdProveedor(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.devolucionFilter = new Devolucion();
        this.devolucionFilter.setListadoPojo("true");
        this.tipoOperacionFilter = new TipoOperacion();
        this.tipoOperacionAdapterList = new TipoOperacionAdapter();
        this.motivoOperacionFilter = new Motivo();
        this.motivoOperacionAdapterList = new MotivoAdapter();
        this.localOrigenFilter = new Local();
        this.localOrigenAdapterList = new LocalListAdapter();
        this.localDestinoFilter = new Local();
        this.localDestinoAdapterList = new LocalListAdapter();
        getDevoluciones();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("operacion-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Método para obtener estado
     */
    public void getEstados() {
        this.setEstadoAdapterList(getEstadoAdapterList().fillData(getEstado()));
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.devolucionFilter = new Devolucion();
        this.devolucionFilter.setListadoPojo("true");
        this.devolucionAdapterList = new DevolucionAdapter();
        this.cliente = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.tipoOperacionFilter = new TipoOperacion();
        this.tipoOperacionAdapterList = new TipoOperacionAdapter();
        this.motivoOperacionFilter = new Motivo();
        this.motivoOperacionAdapterList = new MotivoAdapter();
        this.devolucionDetalle = new DevolucionDetalle();
        this.devolucionDetalle.setListadoPojo("true");
        this.devolucionDetalleAdapter = new DevolucionDetalleAdapter();
        this.localOrigenAdapterList = new LocalListAdapter();
        this.localOrigenFilter = new Local();
        this.localDestinoAdapterList = new LocalListAdapter();
        this.localDestinoFilter = new Local();
        this.adapterProveedor = new ClientListAdapter();
        this.proveedor = new Cliente();

        this.estado = new Estado();
        this.estadoAdapterList = new EstadoAdapter();

        getDevoluciones();
        getEstados();
    }

    /**
     * Constructor
     */
    public DevolucionListController() {
        init();
    }

}
