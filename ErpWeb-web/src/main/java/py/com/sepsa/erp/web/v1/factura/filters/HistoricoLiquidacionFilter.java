package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;

/**
 * Filtro utilizado para el servicio de Histórico Liquidación
 *
 * @author Romina Núñez
 */
public class HistoricoLiquidacionFilter extends Filter {

    /**
     * Agrega el filtro de identificador de cliente
     *
     * @param idCliente
     * @return
     */
    public HistoricoLiquidacionFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de actual
     *
     * @param actual 
     * @return
     */
    public HistoricoLiquidacionFilter actual(boolean actual) {
        actual = true;
        params.put("actual", actual);
        return this;
    }


    /**
     * Construye el mapa de parametros
     *
     * @param historicoLiquidacion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(HistoricoLiquidacion historicoLiquidacion, Integer page, Integer pageSize) {
        HistoricoLiquidacionFilter filter = new HistoricoLiquidacionFilter();

        filter
                .idCliente(historicoLiquidacion.getIdCliente())
                .actual(historicoLiquidacion.isActual())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de HistoricoLiquidacionFilter
     */
    public HistoricoLiquidacionFilter() {
        super();
    }
}
