package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoMenuListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoMenuFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de TipoMenu
 *
 * @author alext
 */
public class TipoMenuServiceClient extends APIErpCore {
    
    public TipoMenuListAdapter getTipoEmailList(TipoMenu tipoMenu, Integer page,
            Integer pageSize) {
       
        TipoMenuListAdapter lista = new TipoMenuListAdapter();
        
        Map params = TipoMenuFilter.build(tipoMenu, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.TIPO_MENU.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoMenuListAdapter.class);

            lista = (TipoMenuListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }

    /**
     * Constructor de TipoMenuServiceClient
     */
    public TipoMenuServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        TIPO_MENU("Servivio para listado de TipoMenu", "tipo-menu");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
