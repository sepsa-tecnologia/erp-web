/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.info.remote.TipoEstadoService;

/**
 * converter para tipo estado
 *
 * @author Sergio D. Riveros Vazquez
 */
@FacesConverter("tipoEstadoPojoConverter")
public class TipoEstadoConverter implements Converter {

    /**
     * Servicio para tipo estado
     */
    private TipoEstadoService tipoEstadoService;

    @Override
    public TipoEstado getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            tipoEstadoService = new TipoEstadoService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            TipoEstado tipoEstado = new TipoEstado();
            tipoEstado.setId(val);

            List<TipoEstado> tipoEstados = tipoEstadoService.getTipoEstadoList(tipoEstado, 0, 10).getData();
            if (tipoEstados != null && !tipoEstados.isEmpty()) {
                return tipoEstados.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "No es un tipo Estado válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof TipoEstado) {
                return String.valueOf(((TipoEstado) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
