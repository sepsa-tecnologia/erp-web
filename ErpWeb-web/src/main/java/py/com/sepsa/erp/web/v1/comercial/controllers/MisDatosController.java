package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.ReverseGeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmailListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.DireccionServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;

/**
 * Controlador para Clientes
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("misDatos")
public class MisDatosController implements Serializable {

    /**
     * Adaptador de la persona
     */
    private PersonaListAdapter adapterPersona;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Adaptador para el listadod e cliente
     */
    private ClientListAdapter adapter;
    /**
     * Datos de la persona
     */
    private Persona persona;
    /**
     * Servicios para login al sistema
     */
    private ClientServiceClient serviceClient;
    /**
     * Servicio para cliente para persona
     */
    private PersonaServiceClient personaClient;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;

    /**
     * Datos del cliente
     */
    private Cliente clienteNuevo;
    /**
     * Cliente remote para dirección
     */
    private DireccionServiceClient direccionClient;
    /**
     * Objeto Dirección
     */
    private Direccion direccion;
    /**
     * Adaptador de la lista de tipo de referencia geografica
     */
    private TipoReferenciaGeograficaAdapter adapterTipoRefGeo;
    /**
     * Objeto tipo referencia geo
     */
    private TipoReferenciaGeografica tipoRefGeo;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Latitud de la direccion del local
     */
    private double lat = -25.273884;
    /**
     * Longitud de la direccion del local
     */
    private double lng = -57.634671;

    /**
     *
     */
    private MapModel draggableModel;
    /**
     *
     */
    private Marker marker;
    /**
     * Números de telefonos agregados
     */
    private String numerosTel = "";
    /**
     * Email agradados
     */
    private String mails = "";
    /**
     * Objeto PersonaTeléfono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Objeto PersonaEmail
     */
    private PersonaEmail personaEmail;
    /**
     * Lista de tipo de telefonos
     */
    private List<SelectItem> listTipoTelefono;
    /**
     * Lista de tipo de email
     */
    private List<SelectItem> listTipoEmail;
    /**
     * Adaptador de la lista tipo telefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefono;
    /**
     * Datos del tipo telefono
     */
    private TipoTelefono tipoTelefono;
    /**
     * Tipo de telefono seleccionado
     */
    private Integer selectedTel = 0;
    /**
     * Tipo de Email seleccionado
     */
    private Integer selectedEmail = 0;
    /**
     * Número del prefijo
     */
    private String prefijo;
    /**
     * Número de telefono
     */
    private String tel;
    /**
     * Variable booleana para principal telefono
     */
    private boolean principal = false;
    /**
     * Variabla boolean principal para email
     */
    private boolean principalEmail = false;
    /**
     * Lista de telefono
     */
    private List<Telefono> listTelefono = new ArrayList();
    /**
     * Lista de Email
     */
    private List<Email> listEmails = new ArrayList();
    /**
     * Adaptador de la lista de tipo Email
     */
    private TipoEmailListAdapter adapterTipoEmail;
    /**
     * Datos del tipo Email
     */
    private TipoEmail tipoEmail;
    /**
     * email del cliente
     */
    private String email;
    private Integer idDepartamento;
    private Integer idDistrito;
    private Integer idCiudad;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    
    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }
    
    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }
    
    public Integer getIdDistrito() {
        return idDistrito;
    }
    
    public Integer getIdCiudad() {
        return idCiudad;
    }
    
    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    
    public Integer getIdDepartamento() {
        return idDepartamento;
    }
    
    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }
    
    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }
    
    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }
    
    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tipoEmail
     */
    public TipoEmail getTipoEmail() {
        return tipoEmail;
    }

    /**
     * @param tipoEmail the tipoEmail to set
     */
    public void setTipoEmail(TipoEmail tipoEmail) {
        this.tipoEmail = tipoEmail;
    }

    /**
     * @return the adapterTipoEmail
     */
    public TipoEmailListAdapter getAdapterTipoEmail() {
        return adapterTipoEmail;
    }

    /**
     * @param adapterTipoEmail the adapterTipoEmail to set
     */
    public void setAdapterTipoEmail(TipoEmailListAdapter adapterTipoEmail) {
        this.adapterTipoEmail = adapterTipoEmail;
    }

    /**
     * @return the listEmails
     */
    public List<Email> getListEmails() {
        return listEmails;
    }

    /**
     * @param listEmails the listEmails to set
     */
    public void setListEmails(List<Email> listEmails) {
        this.listEmails = listEmails;
    }

    /**
     * @return the listTelefono
     */
    public List<Telefono> getListTelefono() {
        return listTelefono;
    }

    /**
     * @param listTelefono the listTelefono to set
     */
    public void setListTelefono(List<Telefono> listTelefono) {
        this.listTelefono = listTelefono;
    }

    /**
     * @return the principal
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    /**
     * @return the principalEmail
     */
    public boolean isPrincipalEmail() {
        return principalEmail;
    }

    /**
     * @param principalEmail the principalEmail to set
     */
    public void setPrincipalEmail(boolean principalEmail) {
        this.principalEmail = principalEmail;
    }

    /**
     * @return the prefijo
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * @param prefijo the prefijo to set
     */
    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the selectedTel
     */
    public Integer getSelectedTel() {
        return selectedTel;
    }

    /**
     * @param selectedTel the selectedTel to set
     */
    public void setSelectedTel(Integer selectedTel) {
        this.selectedTel = selectedTel;
    }

    /**
     * @return the selectedEmail
     */
    public Integer getSelectedEmail() {
        return selectedEmail;
    }

    /**
     * @param selectedEmail the selectedEmail to set
     */
    public void setSelectedEmail(Integer selectedEmail) {
        this.selectedEmail = selectedEmail;
    }

    /**
     * @return the tipoTelefono
     */
    public TipoTelefono getTipoTelefono() {
        return tipoTelefono;
    }

    /**
     * @param tipoTelefono the tipoTelefono to set
     */
    public void setTipoTelefono(TipoTelefono tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    /**
     * @return the adapterTipoTelefono
     */
    public TipoTelefonoListAdapter getAdapterTipoTelefono() {
        return adapterTipoTelefono;
    }

    /**
     * @param adapterTipoTelefono the adapterTipoTelefono to set
     */
    public void setAdapterTipoTelefono(TipoTelefonoListAdapter adapterTipoTelefono) {
        this.adapterTipoTelefono = adapterTipoTelefono;
    }

    /**
     * @return the listTipoTelefono
     */
    public List<SelectItem> getListTipoTelefono() {
        return listTipoTelefono;
    }

    /**
     * @param listTipoTelefono the listTipoTelefono to set
     */
    public void setListTipoTelefono(List<SelectItem> listTipoTelefono) {
        this.listTipoTelefono = listTipoTelefono;
    }

    /**
     * @return the listTipoEmail
     */
    public List<SelectItem> getListTipoEmail() {
        return listTipoEmail;
    }

    /**
     * @param listTipoEmail the listTipoEmail to set
     */
    public void setListTipoEmail(List<SelectItem> listTipoEmail) {
        this.listTipoEmail = listTipoEmail;
    }

    /**
     * @return the personaEmail
     */
    public PersonaEmail getPersonaEmail() {
        return personaEmail;
    }

    /**
     * @param personaEmail the personaEmail to set
     */
    public void setPersonaEmail(PersonaEmail personaEmail) {
        this.personaEmail = personaEmail;
    }

    /**
     * @return the personaTelefono
     */
    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    /**
     * @param personaTelefono the personaTelefono to set
     */
    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    /**
     * @return the mails
     */
    public String getMails() {
        return mails;
    }

    /**
     * @param mails the mails to set
     */
    public void setMails(String mails) {
        this.mails = mails;
    }

    /**
     * @return the numerosTel
     */
    public String getNumerosTel() {
        return numerosTel;
    }

    /**
     * @param numerosTel the numerosTel to set
     */
    public void setNumerosTel(String numerosTel) {
        this.numerosTel = numerosTel;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return the lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng the lng to set
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

    /**
     * @return the draggableModel
     */
    public MapModel getDraggableModel() {
        return draggableModel;
    }

    /**
     * @param draggableModel the draggableModel to set
     */
    public void setDraggableModel(MapModel draggableModel) {
        this.draggableModel = draggableModel;
    }

    /**
     * @return the marker
     */
    public Marker getMarker() {
        return marker;
    }

    /**
     * @param marker the marker to set
     */
    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    /**
     * @return the adapterRefGeo
     */
    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    /**
     * @param adapterRefGeo the adapterRefGeo to set
     */
    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    /**
     * @return the tipoRefGeo
     */
    public TipoReferenciaGeografica getTipoRefGeo() {
        return tipoRefGeo;
    }

    /**
     * @param tipoRefGeo the tipoRefGeo to set
     */
    public void setTipoRefGeo(TipoReferenciaGeografica tipoRefGeo) {
        this.tipoRefGeo = tipoRefGeo;
    }

    /**
     * @return the adapterTipoRefGeo
     */
    public TipoReferenciaGeograficaAdapter getAdapterTipoRefGeo() {
        return adapterTipoRefGeo;
    }

    /**
     * @param adapterTipoRefGeo the adapterTipoRefGeo to set
     */
    public void setAdapterTipoRefGeo(TipoReferenciaGeograficaAdapter adapterTipoRefGeo) {
        this.adapterTipoRefGeo = adapterTipoRefGeo;
    }

    /**
     * @return the direccionClient
     */
    public DireccionServiceClient getDireccionClient() {
        return direccionClient;
    }

    /**
     * @param direccionClient the direccionClient to set
     */
    public void setDireccionClient(DireccionServiceClient direccionClient) {
        this.direccionClient = direccionClient;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the referenciaGeoCiudad
     */
    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    /**
     * @param referenciaGeoCiudad the referenciaGeoCiudad to set
     */
    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    /**
     * @return the referenciaGeoDistrito
     */
    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    /**
     * @param referenciaGeoDistrito the referenciaGeoDistrito to set
     */
    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    /**
     * @return the referenciaGeoDepartamento
     */
    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    /**
     * @param referenciaGeoDepartamento the referenciaGeoDepartamento to set
     */
    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    /**
     * @return the referenciaGeo
     */
    public ReferenciaGeografica getReferenciaGeo() {
        return referenciaGeo;
    }

    /**
     * @param referenciaGeo the referenciaGeo to set
     */
    public void setReferenciaGeo(ReferenciaGeografica referenciaGeo) {
        this.referenciaGeo = referenciaGeo;
    }
    
    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }
    
    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public ClientListAdapter getAdapter() {
        return adapter;
    }
    
    public void setAdapter(ClientListAdapter adapter) {
        this.adapter = adapter;
    }
    
    public Persona getPersona() {
        return persona;
    }
    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public ClientServiceClient getServiceClient() {
        return serviceClient;
    }
    
    public void setServiceClient(ClientServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }
    
    public PersonaServiceClient getPersonaClient() {
        return personaClient;
    }
    
    public void setPersonaClient(PersonaServiceClient personaClient) {
        this.personaClient = personaClient;
    }
    
    public Cliente getClienteNuevo() {
        return clienteNuevo;
    }
    
    public void setClienteNuevo(Cliente clienteNuevo) {
        this.clienteNuevo = clienteNuevo;
    }
//</editor-fold>

    /**
     * Descripción del tipo de email
     *
     * @param idTipo
     * @return
     */
    public String getDescTipoEmail(Integer idTipo) {
        tipoEmail.setId(idTipo);
        adapterTipoEmail = adapterTipoEmail.fillData(tipoEmail);
        
        return adapterTipoEmail.getData().get(0).getCodigo();
    }

    /**
     * Método para agregar el email
     */
    public void addMail() {
        
        Email em = new Email();
        int id = adapterTipoEmail.getData().size();
        em.setId(id);
        em.setIdTipoEmail(selectedEmail);
        em.setCodigoTipoEmail(getDescTipoEmail(selectedEmail));
        em.setEmail(email);
        if (principalEmail == true) {
            em.setPrincipal("S");
        } else {
            em.setPrincipal("N");
        }
        if (!em.getEmail().isEmpty()) {
            listEmails.add(em);
            mails = "";
            for (int i = 0; i < listEmails.size(); i++) {
                mails = mails + listEmails.get(i).getEmail() + ";";
            }
            email = "";
            principalEmail = false;
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe completar el email");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Método para borrar de la lista el email
     */
    public void delMail() {
        
        mails = "";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String parametro = (String) params.get("idMail");
        for (Email email : listEmails) {
            if (email.getId() == Integer.parseInt(parametro)) {
                listEmails.remove(email);
                break;
            }
        }
        for (int i = 0; i < listEmails.size(); i++) {
            mails = mails + listEmails.get(i).getEmail();
        }
    }

    /**
     * Borra los telefonos de la lista
     */
    public void delTelefono() {
        
        numerosTel = "";
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String parametroObtenido = (String) params.get("idTel");
        for (Telefono tel : listTelefono) {
            if (tel.getId() == Integer.parseInt(parametroObtenido)) {
                listTelefono.remove(tel);
                break;
            }
        }
        for (int i = 0; i < listTelefono.size(); i++) {
            numerosTel = numerosTel + "(" + listTelefono.get(i).getPrefijo() + ")" + listTelefono.get(i).getNumero() + ";";
        }
    }

    /**
     * Descripción del tipo teléfono
     *
     * @param idTipo
     * @return
     */
    public TipoTelefono getDescTipoTelefono(Integer idTipo) {
        tipoTelefono.setId(idTipo);
        adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefono);
        
        return adapterTipoTelefono.getData().get(0);
    }

    /**
     * Método para agregar el telefono a la lista
     */
    public void addTelefono() {
        
        numerosTel = "";
        Telefono telef = new Telefono();
        TipoTelefono tipoTel = new TipoTelefono();
        tipoTel.setId(selectedTel);
        tipoTel.setCodigo(getDescTipoTelefono(selectedTel).getCodigo());
        telef.setTipoTelefono(tipoTel);
        telef.setIdTipoTelefono(tipoTel.getId());
        telef.setCodigoTipoTelefono(tipoTel.getCodigo());
        telef.setPrefijo(prefijo);
        telef.setNumero(tel);
        
        if (principal == true) {
            telef.setPrincipal("S");
        } else {
            telef.setPrincipal("N");
        }
        int id = listTelefono.size();
        telef.setId(id);
        
        if (!telef.getPrefijo().isEmpty() && !telef.getNumero().isEmpty()) {
            listTelefono.add(telef);
            //  telefono = "";
            principal = false;
            for (int i = 0; i < listTelefono.size(); i++) {
                numerosTel = numerosTel + "(" + listTelefono.get(i).getPrefijo() + ")" + listTelefono.get(i).getNumero() + ";";
            }
            
            prefijo = "";
            tel = "";
            
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe completar el prefijo y número");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
    }


    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccion.setIdDepartamento(empyInt);
        direccion.setIdDistrito(empyInt);
        direccion.setIdCiudad(empyInt);
        
        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }
        
        PF.current().ajax().update(":client-form:form-data:dato-client:distrito");
        PF.current().ajax().update(":client-form:form-data:dato-client:ciudad");
        
        return datos;
    }
    
    public void limpiarDatosReferencia() {
        Integer empyInt = null;
        referenciaGeoDepartamento = new ReferenciaGeografica();
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDepartamento(empyInt);
        direccion.setIdDistrito(empyInt);
        direccion.setIdCiudad(empyInt);
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccion.setIdDistrito(empyInt);
        direccion.setIdCiudad(empyInt);
        
        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccion.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }
        
        PF.current().ajax().update(":client-form:form-data:dato-client:ciudad");
        
        return datos;
        
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {

        //    adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        
        ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
        referenciaGeoDistrito.setIdPadre(direccion.getIdDistrito());
        referenciaGeoDistrito.setDescripcion(query);
        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
        
        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccion.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para crear Cliente
     */
    public void createClient() {
        
        persona.setActivo("S");
        clienteNuevo.setNroDocumento(persona.getNroDocumento());
        direccion.setLatitud(getLat() + "");
        direccion.setLongitud(getLng() + "");
        direccion.setIdDepartamento(idDepartamento);
        direccion.setIdDistrito(idDistrito);
        direccion.setIdCiudad(idCiudad);
        persona.setDireccion(direccion);
        
        for (int i = 0; i < listTelefono.size(); i++) {
            if (!listTelefono.get(i).getNumero().equals("")) {
                PersonaTelefono pTel = new PersonaTelefono();
                pTel.setTelefono(listTelefono.get(i));
                pTel.setActivo("S");
                persona.getPersonaTelefonos().add(pTel);
            }
        }
        
        for (int i = 0; i < listEmails.size(); i++) {
            if (!listEmails.get(i).getEmail().trim().isEmpty()) {
                PersonaEmail pEmail = new PersonaEmail();
                pEmail.setEmail(listEmails.get(i));
                pEmail.setActivo("S");
                persona.getPersonaEmails().add(pEmail);
            }
        }
        
        if ((persona.getNombreFantasia() == null || persona.getNombreFantasia().isEmpty()) && persona.getRazonSocial() != null) {
            persona.setNombreFantasia(persona.getRazonSocial());
        }
        
        clienteNuevo.setPersona(persona);
        clienteNuevo.setFactElectronica("N");
        clienteNuevo.setIdTipoDocumento(persona.getIdTipoPersona());
        clienteNuevo.setCodigoEstado("ACTIVO");
        clienteNuevo.setProveedor("N");
        clienteNuevo.setComprador("N");
        
        BodyResponse<Cliente> respuestaDeCliente = serviceClient.setCliente(clienteNuevo);
        
        if (respuestaDeCliente.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Cliente creado correctamente!"));
            limpiarForm();
        }
        
    }

    /**
     * Método para limpiar campos de creación de cliente
     */
    public void limpiarForm() {
        persona = new Persona();
        persona.setIdTipoPersona(1);
        clienteNuevo = new Cliente();
        direccion = new Direccion();
        listEmails = new ArrayList<>();
        listTelefono = new ArrayList<>();
        mails = "";
        numerosTel = "";
        init();
    }
    
    public void filterCliente() {
        
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clearFilter() {
        cliente = new Cliente();
        filterCliente();
    }

    /**
     *
     * @param event
     */
    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        lat = marker.getLatlng().getLat();
        lng = marker.getLatlng().getLng();
    }

    /**
     * Enfoca el mapa en la direccion indicada
     *
     * @param event Evento que contiene la direccion
     */
    public void onGeocode(GeocodeEvent event) {
        GeocodeResult result = event.getResults().get(0);
        if (result != null) {
            LatLng center = result.getLatLng();
            lat = center.getLat();
            lng = center.getLng();
            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setDraggable(true);
                premarker.setLatlng(center);
                premarker.setTitle(result.getAddress());
            });
        }
    }

    /**
     * Obtiene la direccion exacta de la coordenada indicada
     *
     * @param event Evento que contiene las coordenadas
     */
    public void onReverseGeocode(ReverseGeocodeEvent event) {
        
        String address = event.getAddresses().get(0);
        LatLng center = event.getLatlng();
        if (address != null && center != null) {
            lat = center.getLat();
            lng = center.getLng();
            direccion.setDireccion(address);
            
            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setLatlng(center);
                premarker.setTitle(address);
            });
        }
        PrimeFaces.current().executeScript("initAutocomplete();");
    }
    
    public void filterDepartamento() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }
    
    public void filterDistrito() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }
    
    public void filterCiudad() {
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }
    
    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.clienteNuevo = new Cliente();
        this.persona = new Persona();
        this.persona.setIdTipoPersona(1);
        this.adapterPersona = new PersonaListAdapter();
        this.serviceClient = new ClientServiceClient();
        this.adapter = new ClientListAdapter();
        adapter.setFirstResult(0);
        this.cliente = new Cliente();
        filterCliente();
        this.personaClient = new PersonaServiceClient();
        this.adapterPersona = new PersonaListAdapter();
        this.direccionClient = new DireccionServiceClient();
        this.direccion = new Direccion();
        this.adapterTipoRefGeo = new TipoReferenciaGeograficaAdapter();
        this.tipoRefGeo = new TipoReferenciaGeografica();
        this.referenciaGeo = new ReferenciaGeografica();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        
        this.personaTelefono = new PersonaTelefono();
        this.personaEmail = new PersonaEmail();
        this.adapterTipoTelefono = new TipoTelefonoListAdapter();
        this.tipoTelefono = new TipoTelefono();
        this.adapterTipoEmail = new TipoEmailListAdapter();
        this.tipoEmail = new TipoEmail();
        this.listTipoTelefono = new ArrayList();
        this.listTipoEmail = new ArrayList();
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;
        
        setAdapterTipoRefGeo(getAdapterTipoRefGeo().fillData(getTipoRefGeo()));
        
        adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefono);
        
        for (int i = 0; i < adapterTipoTelefono.getData().size(); i++) {
            
            listTipoTelefono.add(new SelectItem(adapterTipoTelefono.getData().get(i).getId(),
                    adapterTipoTelefono.getData().get(i).getCodigo()));
        }
        
        if (!listTipoTelefono.isEmpty()) {
            selectedTel = (Integer) listTipoTelefono.get(0).getValue();
            
        }
        
        adapterTipoEmail = adapterTipoEmail.fillData(tipoEmail);
        for (int i = 0; i < adapterTipoEmail.getData().size(); i++) {
            listTipoEmail.add(new SelectItem(adapterTipoEmail.getData().
                    get(i).getId(), adapterTipoEmail.getData().get(i).getDescripcion()));
        }
        
        if (!listTipoEmail.isEmpty()) {
            selectedEmail = (Integer) listTipoEmail.get(0).getValue();
        }
        
        lat = -25.286534173063284;
        lng = -57.63632285404583;
        draggableModel = new DefaultMapModel();
        LatLng coord = new LatLng(lat, lng);
        draggableModel.addOverlay(new Marker(coord));
        for (Marker premarker : draggableModel.getMarkers()) {
            premarker.setDraggable(true);
        }
        
        filterDepartamento();
        filterDistrito();
        filterCiudad();
        
    }
    
}
