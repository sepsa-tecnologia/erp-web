/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.api.v1.services;

import java.io.File;
import java.nio.file.Paths;
import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * Controlador para PING
 *
 * @author Jonathan D. Bernal Fernández
 */
@Path("/archivos.descargarPdf")
public class DescargarPdfService {

    /**
     * Método que maneja las peticiones de ping
     *
     * @return Response
     */
    @GET
    @PermitAll
    @Produces("application/pdf")
    public Response descargar(@QueryParam("id") String id) {
        String tmpdir = System.getProperty("java.io.tmpdir");
        File file = Paths.get(tmpdir, "sepsa_lite_" + id).toFile();
        if (file.exists()) {
            return Response
                    .status(200)
                    .type("application/pdf")
                    .entity(file)
                    .build();
        } else {
            return Response
                    .status(404)
                    .type("application/pdf")
                    .build();
        }
    }
}
