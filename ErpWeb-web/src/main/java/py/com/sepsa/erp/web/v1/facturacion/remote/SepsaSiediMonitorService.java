package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APISepsaSiediMonitor;

/**
 * Cliente para el servicio de facturacion
 *
 * @author Romina Núñez, Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class SepsaSiediMonitorService extends APISepsaSiediMonitor {

    /**
     * Método para anular la factura
     *
     * @param factura Objeto
     * @return
     */
    public BodyResponse<Factura> reenviarNotificacion(String cdc) {

        BodyResponse response = new BodyResponse();

        Map params = new HashMap<>();

        String service = String.format(Resource.SIEDI_CONN.url, cdc);

        HttpURLConnection conn = PUT(service, ContentType.JSON, params);

        if (conn != null) {

            //Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            // this.addBody(conn, gson.toJson(factura));
            response = BodyResponse.createInstance(conn,
                    Factura.class);
            conn.disconnect();
        }
        return response;
    }

    /**
     * Constructor de FacturacionService
     */
    public SepsaSiediMonitorService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        SIEDI_CONN("Conexión al Siedi", "v1/transaccion/%s/reenviar-notificacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
