/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.TipoOperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.inventario.remote.OperacionService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista crear operaciones o Ajuste de inventario
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("operacionCreate")
public class OperacionCreateController implements Serializable {

    /**
     * Cliente para el servicio de operacion
     */
    private final OperacionService service;
    /**
     * POJO de operacion
     */
    private Operacion operacion;
    /**
     * Adapter para tipo de operaciones
     */
    private TipoOperacionAdapter adapterTipoOperacion;
    /**
     * POJO de tipo operacion
     */
    private TipoOperacion tOperacion;
    /**
     * Adapter para motivo de operacion
     */
    private MotivoAdapter adapterMotivo;
    /**
     * POJO de motivo
     */
    private Motivo motivo;
    /**
     * Adapter para Estado
     */
    private EstadoAdapter adapterEstado;
    /**
     * Pojo para estado
     */
    private Estado estado;
    /**
     * Adapter para Estado de producto
     */
    private EstadoAdapter adapterEstadoForProducto;
    /**
     * Pojo para estado de producto
     */
    private Estado estadoForProducto;
    /**
     * Objeto depositoDestinoLogistico
     */
    private DepositoLogistico depositoDestino;
    /**
     * Adaptador de la lista de depositoDestinoLogistico
     */
    private DepositoLogisticoAdapter depositoDestinoAdapter;
    /**
     * Objeto con lista de detalles de operaciones
     */
    private List<OperacionInventarioDetalle> operacionInventarioDetalleList;
    /**
     * objeto detalle para guardar en la lista de detalles
     */
    private OperacionInventarioDetalle operacionInventarioDetalle;
    /**
     * Objeto producto
     */
    private Producto producto;
    /**
     * Adapter para producto
     */
    private ProductoAdapter adapterProducto;
    /**
     * Auxiliar para guardar la cantidad del producto en el detalle
     */
    private Integer cantidad;
    /**
     * estadoAuxiliar para guardar el estado seleccionado para añadir al detalle
     */
    private String estadoAuxiliar;
    private Date fechaVtoAuxiliar;
    private Boolean agregar;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Operacion getOperacion() {
        return operacion;
    }

    public void setAgregar(Boolean agregar) {
        this.agregar = agregar;
    }

    public Boolean getAgregar() {
        return agregar;
    }

    public void setFechaVtoAuxiliar(Date fechaVtoAuxiliar) {
        this.fechaVtoAuxiliar = fechaVtoAuxiliar;
    }

    public Date getFechaVtoAuxiliar() {
        return fechaVtoAuxiliar;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public TipoOperacionAdapter getAdapterTipoOperacion() {
        return adapterTipoOperacion;
    }

    public void setAdapterTipoOperacion(TipoOperacionAdapter adapterTipoOperacion) {
        this.adapterTipoOperacion = adapterTipoOperacion;
    }

    public TipoOperacion gettOperacion() {
        return tOperacion;
    }

    public void settOperacion(TipoOperacion tOperacion) {
        this.tOperacion = tOperacion;
    }

    public MotivoAdapter getAdapterMotivo() {
        return adapterMotivo;
    }

    public void setAdapterMotivo(MotivoAdapter adapterMotivo) {
        this.adapterMotivo = adapterMotivo;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public EstadoAdapter getAdapterEstado() {
        return adapterEstado;
    }

    public void setAdapterEstado(EstadoAdapter adapterEstado) {
        this.adapterEstado = adapterEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public EstadoAdapter getAdapterEstadoForProducto() {
        return adapterEstadoForProducto;
    }

    public void setAdapterEstadoForProducto(EstadoAdapter adapterEstadoForProducto) {
        this.adapterEstadoForProducto = adapterEstadoForProducto;
    }

    public Estado getEstadoForProducto() {
        return estadoForProducto;
    }

    public void setEstadoForProducto(Estado estadoForProducto) {
        this.estadoForProducto = estadoForProducto;
    }

    public DepositoLogistico getDepositoDestino() {
        return depositoDestino;
    }

    public void setDepositoDestino(DepositoLogistico depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public DepositoLogisticoAdapter getDepositoDestinoAdapter() {
        return depositoDestinoAdapter;
    }

    public void setDepositoDestinoAdapter(DepositoLogisticoAdapter depositoDestinoAdapter) {
        this.depositoDestinoAdapter = depositoDestinoAdapter;
    }

    public List<OperacionInventarioDetalle> getOperacionInventarioDetalleList() {
        return operacionInventarioDetalleList;
    }

    public void setOperacionInventarioDetalleList(List<OperacionInventarioDetalle> operacionInventarioDetalleList) {
        this.operacionInventarioDetalleList = operacionInventarioDetalleList;
    }

    public OperacionInventarioDetalle getOperacionInventarioDetalle() {
        return operacionInventarioDetalle;
    }

    public void setOperacionInventarioDetalle(OperacionInventarioDetalle operacionInventarioDetalle) {
        this.operacionInventarioDetalle = operacionInventarioDetalle;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getEstadoAuxiliar() {
        return estadoAuxiliar;
    }

    public void setEstadoAuxiliar(String estadoAuxiliar) {
        this.estadoAuxiliar = estadoAuxiliar;
    }

    //</editor-fold>
    /**
     * Metodo para crear Operacion
     */
    public void create() {
        boolean create = true;

        if (operacion.getIdDepositoDestino() == null) {
            create = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Debe indicar el depósito destino"));
        }

        

        if (create) {
            añadirDetalles();
            operacion.setCodigoEstado("CONFIRMADO");
            operacion.setCodigoTipoMotivo("OPERACION_INVENTARIO_AJUSTE");
            operacion.setCodigoTipoOperacion("AJUSTE_INVENTARIO");
            BodyResponse<Operacion> respuesta = service.setOperacion(operacion);
            if (respuesta.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Info", "Operación creada correctamente!"));
                init();
                PF.current().ajax().update(":data-form:form-data:producto");
            }
        }
    }

    /**
     * Método para obtener el listado de tipo operacion
     */
    public void filterTipoOperacion() {
        adapterTipoOperacion = adapterTipoOperacion.fillData(tOperacion);
    }

    /**
     * Método para obtener el listado de motivos para operacion
     */
    public void filterMotivoOperacion() {
        this.motivo.setCodigoTipoMotivo("OPERACION_INVENTARIO_AJUSTE");
        adapterMotivo = adapterMotivo.fillData(motivo);
    }

    /**
     * Método para obtener el listado de estados
     */
    public void getEstadoList() {
        estado.setCodigoTipoEstado("OPERACION_INVENTARIO");
        adapterEstado = adapterEstado.fillData(estado);
    }

    /**
     * Método para obtener el listado de estados de productos
     */
    public void getEstadoForProductoList() {
        estadoForProducto.setCodigoTipoEstado("PRODUCTO_AJUSTE_INVENTARIO");
        adapterEstadoForProducto = adapterEstadoForProducto.fillData(estadoForProducto);
    }

    /**
     * Autocomplete para depositoDestinoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        depositoDestino = new DepositoLogistico();
        depositoDestino.setDescripcion(query);
        depositoDestino.setListadoPojo(true);
        depositoDestinoAdapter = depositoDestinoAdapter.fillData(depositoDestino);

        return depositoDestinoAdapter.getData();
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        operacion.setIdDepositoDestino(((DepositoLogistico) event.getObject()).getId());
    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {
        producto = new Producto();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        producto.setId(((Producto) event.getObject()).getId());
        producto.setDescripcion(((Producto) event.getObject()).getDescripcion());
    }

    /**
     * Método para consultar inventario detalle
     */
    public void consultarInventarioDetalle() {
        boolean filtrar = true;
        if (operacion.getIdDepositoDestino() == null) {
            filtrar = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Debe indicar el depósito destino"));
        }

        if (producto.getId() == null) {
            filtrar = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Debe indicar el producto"));
        }

        if (estadoAuxiliar == null || estadoAuxiliar.trim().isEmpty()) {
            filtrar = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Debe indicar el estado de inventario"));
        }
        if (filtrar == true) {
            //Se busca si existen datos asociados al producto en el detalle de inventario
            InventarioDetalleAdapter inventarioDetalleAdapter = new InventarioDetalleAdapter();
            InventarioDetalle inventarioDetalle = new InventarioDetalle();
            inventarioDetalle.setListadoPojo(true);
            inventarioDetalle.setIdDepositoLogistico(operacion.getIdDepositoDestino());
            inventarioDetalle.setIdProducto(producto.getId());
            inventarioDetalle.setCodigoEstado(estadoAuxiliar);
            inventarioDetalle.setFechaVencimiento(fechaVtoAuxiliar);
            inventarioDetalle.setTieneFechaVencimiento(fechaVtoAuxiliar != null);
            inventarioDetalleAdapter = inventarioDetalleAdapter.fillData(inventarioDetalle);
            if (!inventarioDetalleAdapter.getData().isEmpty()) {
                for (InventarioDetalle inventarioDetalle1 : inventarioDetalleAdapter.getData()) {
                    this.operacionInventarioDetalle = new OperacionInventarioDetalle();
                    this.operacionInventarioDetalle.setCantidadInicial(inventarioDetalle1.getCantidad());
                    this.operacionInventarioDetalle.setIdProducto(this.producto.getId());
                    this.operacionInventarioDetalle.setProducto(producto.getDescripcion());
                    this.operacionInventarioDetalle.setCodigoEstadoInventarioDestino(inventarioDetalle1.getCodigoEstado());
                    this.operacionInventarioDetalle.setEstadoInventarioDestino(inventarioDetalle1.getEstado());
                    this.operacionInventarioDetalle.setFechaVencimiento(inventarioDetalle1.getFechaVencimiento());
                    this.operacionInventarioDetalleList.add(operacionInventarioDetalle);
                }

                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "El producto se encuentra registrado en el inventario. Proceda a realizar ajustar las cantidades"));
                this.operacionInventarioDetalle = new OperacionInventarioDetalle();
                this.producto = new Producto();
                this.estadoAuxiliar = null;
                this.fechaVtoAuxiliar = null;
                PF.current().ajax().update(":data-form:form-data:producto");
                PF.current().ajax().update(":data-form:form-data:estForProducto");
                PF.current().ajax().update(":data-form:form-data:data-list-detalle-create");
                PF.current().ajax().update(":data-form:form-data:fechaVto");
                PF.current().ajax().update(":message-form:message");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El producto no se encuentra registrado en el inventario. Proceda a realizar la carga"));
                añadirDetalle();

            }
        }
    }

    /**
     * Metodo para agregar un detalle a la lista de detalles.
     */
    public void añadirDetalle() {
        try {
            this.operacionInventarioDetalle.setCantidadInicial(0);
            this.operacionInventarioDetalle.setCantidadFinal(0);
            this.operacionInventarioDetalle.setIdProducto(this.producto.getId());
            this.operacionInventarioDetalle.setProducto(producto.getDescripcion());
            this.operacionInventarioDetalle.setCodigoEstadoInventarioDestino(estadoAuxiliar);
            this.operacionInventarioDetalle.setEstadoInventarioDestino(estadoAuxiliar);
            this.operacionInventarioDetalle.setFechaVencimiento(fechaVtoAuxiliar);
            this.operacionInventarioDetalleList.add(operacionInventarioDetalle);

            this.operacionInventarioDetalle = new OperacionInventarioDetalle();
            this.producto = new Producto();
            this.estadoAuxiliar = null;
            this.cantidad = 0;
            this.fechaVtoAuxiliar = null;
            PF.current().ajax().update(":data-form:form-data:producto");
            PF.current().ajax().update(":data-form:form-data:estForProducto");
            PF.current().ajax().update(":data-form:form-data:data-list-detalle-create");
            PF.current().ajax().update(":data-form:form-data:fechaVto");
            PF.current().ajax().update(":message-form:message");
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Metodo para eliminar un detalle de la lista de detalles.
     *
     * @param opD
     */
    public void deleteDetalle(OperacionInventarioDetalle opD) {
        this.operacionInventarioDetalleList.remove(opD);
    }

    /**
     * Metodo para añadir la lista de detalles al objeto operacion a enviar
     */
    public void añadirDetalles() {
        for (OperacionInventarioDetalle operacionInventarioDetalle1 : operacionInventarioDetalleList) {
            operacionInventarioDetalle1.setIdDepositoDestino(operacion.getIdDepositoDestino());
            operacionInventarioDetalle1.setCantidad(operacionInventarioDetalle1.getCantidadFinal());
        }

        this.operacion.setOperacionInventarioDetalles(operacionInventarioDetalleList);

    }

    /**
     * Metodo para inicializar los datos
     */
    private void init() {
        try {
            this.operacion = new Operacion();
            this.tOperacion = new TipoOperacion();
            this.adapterTipoOperacion = new TipoOperacionAdapter();
            this.motivo = new Motivo();
            this.adapterMotivo = new MotivoAdapter();
            this.estado = new Estado();
            this.adapterEstado = new EstadoAdapter();
            this.estadoForProducto = new Estado();
            this.adapterEstadoForProducto = new EstadoAdapter();
            this.depositoDestino = new DepositoLogistico();
            this.depositoDestino.setListadoPojo(true);
            this.depositoDestinoAdapter = new DepositoLogisticoAdapter();
            this.cantidad = 1;
            this.producto = new Producto();
            this.adapterProducto = new ProductoAdapter();
            this.operacionInventarioDetalle = new OperacionInventarioDetalle();
            this.operacionInventarioDetalleList = new ArrayList<>();
            this.filterMotivoOperacion();
            this.filterTipoOperacion();
            this.getEstadoList();
            this.getEstadoForProductoList();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Constructor
     *
     */
    public OperacionCreateController() {
        init();
        this.service = new OperacionService();
    }

}
