/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.inventario.remote.TipoOperacionService;

/**
 * Converter para tipo operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
@FacesConverter("tipoOperacionPojoConverter")
public class TipoOperacionPojoConverter implements Converter {

    /**
     * Servicio para tipo estado
     */
    private TipoOperacionService tipoOperacionService;

    @Override
    public TipoOperacion getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            tipoOperacionService = new TipoOperacionService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            TipoOperacion tipoOperacion = new TipoOperacion();
            tipoOperacion.setId(val);

            List<TipoOperacion> tipoOperacions = tipoOperacionService.getTipoOperacion(tipoOperacion, 0, 10).getData();
            if (tipoOperacions != null && !tipoOperacions.isEmpty()) {
                return tipoOperacions.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "No es un Tipo Operacion válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof TipoOperacion) {
                return String.valueOf(((TipoOperacion) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
