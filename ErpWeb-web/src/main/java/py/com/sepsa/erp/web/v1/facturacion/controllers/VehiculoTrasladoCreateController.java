/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.adapters.VehiculoTrasladoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("vehiculoTrasladoCreate")
public class VehiculoTrasladoCreateController implements Serializable {
    
    private VehiculoTraslado vehiculoTraslado;
    
    private VehiculoTrasladoService vehiculoTrasladoService;
    
    private VehiculoTrasladoListAdapter adaptervehiculoTraslado;
    
    private Integer modalidadTransporte;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public Integer getModalidadTransporte() {
        return modalidadTransporte;
    }

    public void setModalidadTransporte(Integer modalidadTransporte) {
        this.modalidadTransporte = modalidadTransporte;
    }
 
    public VehiculoTraslado getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    public VehiculoTrasladoService getVehiculoTrasladoService() {
        return vehiculoTrasladoService;
    }

    public void setVehiculoTrasladoService(VehiculoTrasladoService vehiculoTrasladoService) {
        this.vehiculoTrasladoService = vehiculoTrasladoService;
    }

    public VehiculoTrasladoListAdapter getAdaptervehiculoTraslado() {
        return adaptervehiculoTraslado;
    }

    public void setAdaptervehiculoTraslado(VehiculoTrasladoListAdapter adaptervehiculoTraslado) {
        this.adaptervehiculoTraslado = adaptervehiculoTraslado;
    }

    //</editor-fold>
    
    /**
     * Método que valida un string a partir de un patrón regex
     * @param cadena
     * @param patronRegex
     * @return 
     */
    public boolean validadorDePatron(String cadena, String patronRegex) {
        try {
            Pattern pattern = Pattern.compile(patronRegex);

            Matcher matcher = pattern.matcher(cadena);

            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean validarCampos() {
        formatearCampos();
        boolean saveNr = true;
        if (vehiculoTraslado != null) {
            if (vehiculoTraslado.getTipoVehiculo().trim().equalsIgnoreCase("") ) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de vehículo!"));
            }
            
            if (modalidadTransporte == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar modalidad y tipo de transporte!"));
            }
            
            if (vehiculoTraslado.getMarca().trim().equalsIgnoreCase("") ) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la Marca!"));
            }
            
            if (vehiculoTraslado.getTipoIdentificacion() == null ) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de identificación del vehículo!"));
            } else {
                if (vehiculoTraslado.getTipoIdentificacion() == 1){
                    if (vehiculoTraslado.getNroIdentificacion().trim().equalsIgnoreCase("") ) {
                        saveNr = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el número de identificación del vehículo!"));
                    } 
                } else if (vehiculoTraslado.getTipoIdentificacion() == 2){
                    if (vehiculoTraslado.getNroMatricula().trim().equalsIgnoreCase("") ) {
                        saveNr = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el número de matricula del vehículo!"));
                    }
                }
            }
            
            

        } else {
            saveNr = false;
        }

        return saveNr;
    }
    
    public void clearForm(){
        this.vehiculoTraslado = new VehiculoTraslado();
        modalidadTransporte = null;
        
    }
    
    public void formatearCampos(){
        if (vehiculoTraslado != null){
            if (vehiculoTraslado.getMarca() != null) {
                String str = vehiculoTraslado.getMarca().trim().replace(" ","");
                vehiculoTraslado.setMarca(str);
            }
            
            if (vehiculoTraslado.getNroIdentificacion() != null){
                String str = vehiculoTraslado.getNroIdentificacion().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroIdentificacion(str);
            }
            
            if (vehiculoTraslado.getNroMatricula() != null){
                String str = vehiculoTraslado.getNroMatricula().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroMatricula(str);
            }
            
            if (vehiculoTraslado.getNroVuelo() != null){
                String str = vehiculoTraslado.getNroVuelo().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroVuelo(str);
            }
        }
    }
    
    public void crearVehiculoTraslado(){
        if (validarCampos()){          
             BodyResponse response = vehiculoTrasladoService.setVehiculoTraslado(vehiculoTraslado);
             if (response != null){
                 if (response.getSuccess()){
                     clearForm();
                     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Vehiculo de traslado cargado correctamente!"));
                 }
             }
        } 
    }
    
    @PostConstruct
    public void init(){
        this.vehiculoTraslado = new VehiculoTraslado();
        this.vehiculoTrasladoService = new VehiculoTrasladoService();
    }
}
