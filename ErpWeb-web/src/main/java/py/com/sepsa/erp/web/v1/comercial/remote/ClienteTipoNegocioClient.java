
package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClienteTipoNegocioAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ClienteTipoNegocioFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteTipoNegocio;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio Cliente tipo negocio
 * @author Cristina Insfrán
 */
public class ClienteTipoNegocioClient extends APIErpCore{
    
     /**
     * Método para crear cliente tipo negocio
     *
     * @param tipoNegocio
     * @return
     */
    public ClienteTipoNegocio setClienteTipoNegocio(ClienteTipoNegocio  tipoNegocio) {

        ClienteTipoNegocio tn = new ClienteTipoNegocio();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CLIENTE_TIPO_NEGOCIO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(tipoNegocio));

            response = BodyResponse.createInstance(conn, ClienteTipoNegocio.class);
         
            tn = ((ClienteTipoNegocio) response.getPayload());
           
        }
        return tn;
    }
    
    /**
     * Obtiene la lista de descuentos
     *
     * @param descuento 
     * @param page
     * @param pageSize
     * @return
     */
    public ClienteTipoNegocioAdapter getClienteTipoNegocioList(ClienteTipoNegocio clienteTipoNegocio, Integer page,
            Integer pageSize) {

        ClienteTipoNegocioAdapter lista = new ClienteTipoNegocioAdapter();

        Map params = ClienteTipoNegocioFilter.build(clienteTipoNegocio, page, pageSize);

        HttpURLConnection conn = GET(Resource.CLIENTE_ASOC_TIPO_NEGOCIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ClienteTipoNegocioAdapter.class);

            lista = (ClienteTipoNegocioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Constructor de ClienteTipoNegocioClient
     */
    public ClienteTipoNegocioClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CLIENTE_TIPO_NEGOCIO("Servicio Cliente tipo Negocio",
            "cliente-tipo-negocio"), 
        CLIENTE_ASOC_TIPO_NEGOCIO("Cliente tipo negocio", "cliente-tipo-negocio/asociado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
