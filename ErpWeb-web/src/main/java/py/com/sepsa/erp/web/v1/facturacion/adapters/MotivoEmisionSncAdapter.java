
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionSnc;
import py.com.sepsa.erp.web.v1.facturacion.remote.SolicitudNotaCreditoService;

/**
 * Adaptador de la lista de motivo de snc
 * @author Cristina Insfráb
 */
public class MotivoEmisionSncAdapter extends DataListAdapter<MotivoEmisionSnc> {
     /**
     * Cliente para el servicio de cobro
     */
    private final SolicitudNotaCreditoService serviceSolicitudNotaCredito;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MotivoEmisionSncAdapter fillData(MotivoEmisionSnc searchData) {

        return serviceSolicitudNotaCredito.getMotivoEmisionSncList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de MotivoEmisionSncAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoEmisionSncAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceSolicitudNotaCredito = new SolicitudNotaCreditoService();
    }

    /**
     * Constructor de MotivoEmisionSncAdapter
     */
    public MotivoEmisionSncAdapter() {
        super();
        this.serviceSolicitudNotaCredito = new SolicitudNotaCreditoService();
    }
}
