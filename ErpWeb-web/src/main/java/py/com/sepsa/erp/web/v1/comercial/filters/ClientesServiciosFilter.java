package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientesServicios;

/**
 * Filtros para clientes según servicios.
 *
 * @author alext
 * @author Sergio D. Riveros Vazquez
 */
public class ClientesServiciosFilter extends Filter {

    /**
     * Filtro para identificador de cliente.
     *
     * @param idCliente
     * @return
     */
    public ClientesServiciosFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Filtro para cliente.
     *
     * @param cliente
     * @return
     */
    public ClientesServiciosFilter cliente(String cliente) {
        if (cliente != null && !cliente.trim().isEmpty()) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Filtro para nroDocumento.
     *
     * @param nroDocumento
     * @return
     */
    public ClientesServiciosFilter nroDocumento(String nroDocumento) {
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            params.put("nroDocumento", nroDocumento);
        }
        return this;
    }

    /**
     * Filtro para estado.
     *
     * @param estado
     * @return
     */
    public ClientesServiciosFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Filtro para identificador de producto.
     *
     * @param idProducto
     * @return
     */
    public ClientesServiciosFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Filtro por prodcuto.
     *
     * @param prodcuto
     * @return
     */
    public ClientesServiciosFilter prodcuto(String prodcuto) {
        if (prodcuto != null && !prodcuto.trim().isEmpty()) {
            params.put("prodcuto", prodcuto);
        }
        return this;
    }

    /**
     * Filtro para identificador de servicio.
     *
     * @param idServicio
     * @return
     */
    public ClientesServiciosFilter idServicio(Integer idServicio) {
        if (idServicio != null) {
            params.put("idServicio", idServicio);
        }
        return this;
    }

    /**
     * Filtro pot servicio utilizado.
     *
     * @param servicio
     * @return
     */
    public ClientesServiciosFilter servicio(String servicio) {
        if (servicio != null && !servicio.trim().isEmpty()) {
            params.put("servicio", servicio);
        }
        return this;
    }

    /**
     * Filtro por idComercial.
     *
     * @param idComercial
     * @return
     */
    public ClientesServiciosFilter idComercial(Integer idComercial) {
        if (idComercial != null) {
            params.put("idComercial", idComercial);
        }
        return this;
    }

    /**
     * Filtro port agente comercial.
     *
     * @param comercial
     * @return
     */
    public ClientesServiciosFilter comercial(String comercial) {
        if (comercial != null && !comercial.trim().isEmpty()) {
            params.put("comercial", comercial);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param clientesServicios datos del filtro.
     * @param page Página buscada en el resultado.
     * @param pageSize Tamaño de la página resultado.
     * @return parámetros del filtro.
     */
    public static Map build(ClientesServicios clientesServicios, Integer page, Integer pageSize) {
        ClientesServiciosFilter filter = new ClientesServiciosFilter();
        
        filter
                .idCliente(clientesServicios.getIdCliente())
                .cliente(clientesServicios.getCliente())
                .nroDocumento(clientesServicios.getNroDocumento())
                .estado(clientesServicios.getEstado())
                .idProducto(clientesServicios.getIdProducto())
                .prodcuto(clientesServicios.getProducto())
                .idServicio(clientesServicios.getIdServicio())
                .servicio(clientesServicios.getServicio())
                .idComercial(clientesServicios.getIdComercial())
                .comercial(clientesServicios.getComercial())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de ClientesServiciosFilter.
     */
    public ClientesServiciosFilter() {
        super();
    }
    
}
