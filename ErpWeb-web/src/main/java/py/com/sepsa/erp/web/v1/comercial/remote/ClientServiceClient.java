package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ClientFilter;
import py.com.sepsa.erp.web.v1.comercial.filters.ClientPojoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de cliente
 *
 * @author Cristina Insfrán
 */
public class ClientServiceClient extends APIErpCore {

    /**
     * Método para crear Cliente
     *
     * @param cliente
     * @return
     */
    public BodyResponse<Cliente> setCliente(Cliente cliente) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CLIENTES.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(cliente));

            response = BodyResponse.createInstance(conn, Cliente.class);

        }
        return response;
    }

    /**
     * Método para crear Cliente
     *
     * @param cliente
     * @return
     */
    public BodyResponse<Cliente> putCliente(Cliente cliente) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CLIENTES.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(cliente));

            response = BodyResponse.createInstance(conn, Cliente.class);

        }
        return response;
    }

    /**
     * Obtiene la lista de clientes
     *
     * @param cliente Objeto cliente
     * @param page
     * @param pageSize
     * @return
     */
    public ClientListAdapter getClientList(Cliente cliente, Integer page,
            Integer pageSize) {

        ClientListAdapter lista = new ClientListAdapter();

        Map params = ClientFilter.build(cliente, page, pageSize);

        HttpURLConnection conn = GET(Resource.CLIENTES.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ClientListAdapter.class);

            lista = (ClientListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de clientes
     *
     * @param cliente Objeto cliente
     * @param page
     * @param pageSize
     * @return
     */
    public ClientPojoAdapter getClientPojoList(ClientePojo cliente, Integer page,
            Integer pageSize) {

        ClientPojoAdapter lista = new ClientPojoAdapter();

        Map params = ClientPojoFilter.build(cliente, page, pageSize);

        HttpURLConnection conn = GET(Resource.CLIENTES.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ClientPojoAdapter.class);

            lista = (ClientPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de ClientServiceClient
     */
    public ClientServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CLIENTES("Servicios de Clientes", "cliente");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
