/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaDebitoPojoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    public NotaDebitoPojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public NotaDebitoPojoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }
    
    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public NotaDebitoPojoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public NotaDebitoPojoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nd
     *
     * @param nroNotaDebito
     * @return
     */
    public NotaDebitoPojoFilter nroNotaDebito(String nroNotaDebito) {
        if (nroNotaDebito != null && !nroNotaDebito.trim().isEmpty()) {
            params.put("nroNotaDebito", nroNotaDebito);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaDebitoPojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaDebitoPojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo emision interno
     *
     * @param idMotivoEmisionInterno
     * @return
     */
    public NotaDebitoPojoFilter idMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        if (idMotivoEmisionInterno != null) {
            params.put("idMotivoEmisionInterno", idMotivoEmisionInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public NotaDebitoPojoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de cdc
     *
     * @param cdc
     * @return
     */
    public NotaDebitoPojoFilter cdc(String cdc) {
        if (cdc != null && !cdc.trim().isEmpty()) {
            params.put("cdc", cdc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public NotaDebitoPojoFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public NotaDebitoPojoFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }
    
        /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaDebitoPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notaDebito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaDebitoPojo notaDebito, Integer page, Integer pageSize) {
        NotaDebitoPojoFilter filter = new NotaDebitoPojoFilter();

        filter
                .id(notaDebito.getId())
                .idCliente(notaDebito.getIdCliente())
                .razonSocial(notaDebito.getRazonSocial())
                .fecha(notaDebito.getFecha())
                .fechaDesde(notaDebito.getFechaDesde())
                .fechaHasta(notaDebito.getFechaHasta())
                .nroNotaDebito(notaDebito.getNroNotaDebito())
                .anulado(notaDebito.getAnulado())
                .idMotivoEmisionInterno(notaDebito.getIdMotivoEmisionInterno())
                .idFactura(notaDebito.getIdFactura())
                .listadoPojo(notaDebito.getListadoPojo())
                .cdc(notaDebito.getCdc())
                .ruc(notaDebito.getRuc())
                .codigoEstado(notaDebito.getCodigoEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaDebitoPojoFilter
     */
    public NotaDebitoPojoFilter() {
        super();
    }
}
