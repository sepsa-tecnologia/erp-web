package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import py.com.sepsa.erp.web.v1.system.pojos.Token;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.system.pojos.Credential;
import py.com.sepsa.erp.web.v1.system.pojos.User;
import py.com.sepsa.erp.web.v1.system.remote.LoginServiceClient;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para la página de login
 *
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("login")
public class LoginController implements Serializable {

    /**
     * Credenciales del usuario
     */
    private Credential credential;

    /**
     * Mensaje de error del login
     */
    private String error;

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;

    /**
     * Servicios para login al sistema
     */
    private LoginServiceClient serviceClient;

    /**
     * Obtiene las credenciales del usuario
     *
     * @return Credenciales del usuario
     */
    public Credential getCredential() {
        return credential;
    }

    /**
     * Setea las credenciales del usuario
     *
     * @param credential Credenciales del usuario
     */
    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    /**
     * Obtiene el error del login
     *
     * @return Error del login
     */
    public String getError() {
        return error;
    }

    /**
     * Setea el error del login
     *
     * @param error Error del login
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return the session
     */
    public SessionData getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(SessionData session) {
        this.session = session;
    }

    /**
     * Método de autenticación para usuarios
     *
     * @return Página a redirigir
     */
    public String login() {
        Usuario usuario = new Usuario();

        session.clearMsn();

        BodyResponse response = serviceClient.login(credential);

        if (response.getSuccess()) {

            try {
                session.setJwt(((Token) response.getPayload()).getToken());
                usuario = serviceClient.userInfo();
                session.setUser(User.createInstance(usuario));
                ;
                if (session.getUser().getUsuarioEmpresas().size() > 1) {
                    return "/empresa.xhtml?faces-redirect=true;";
                } else {
                    session.setNombreEmpresa(session.getUser().getUsuarioEmpresas().get(0).getEmpresa().getDescripcion());
                    return "/app/home.xhtml?faces-redirect=true;";
                }
                
            } catch (Exception ex) {
                WebLogger.get().fatal(ex);
            }
        } else {
            WebLogger.get().debug("ERROR");
            error = String.valueOf(response.getStatus().getDescripcion());
        }

        return null;

    }

    /**
     * Mantener el home en una sesion activa
     *
     */
    public void getHomeLogin() {
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();

        if (session.getUser() != null) {
            try {
                ctx.redirect(ctx.getRequestContextPath() + "/app/home.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.serviceClient = new LoginServiceClient();
        this.credential = new Credential();
        getHomeLogin();
    }

    /**
     * Constructor de LoginController
     */
    public LoginController() {
    }
}
