package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.DatoPersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.DatoPersonaFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.DatoPersona;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Dato Persona
 *
 * @author Cristina Insfrán
 */
public class DatoPersonaServiceClient extends APIErpCore {

    /**
     * Método para crear DatoPersona
     *
     * @param proveedorComprador
     * @return
     */
    public DatoPersona createDatoPersona(DatoPersona datoPersona) {

        DatoPersona dato = new DatoPersona();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.DATO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(datoPersona));

            response = BodyResponse.createInstance(conn, DatoPersona.class);

            dato = ((DatoPersona) response.getPayload());

        }
        return dato;
    }

    /**
     * Obtiene la lista de dato persona
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public DatoPersonaListAdapter getDatoPersonaList(DatoPersona dato, Integer page,
            Integer pageSize) {

        DatoPersonaListAdapter lista = new DatoPersonaListAdapter();

        Map params = DatoPersonaFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.DATO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DatoPersonaListAdapter.class);

            lista = (DatoPersonaListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de DatoPersonaServiceClient
     */
    public DatoPersonaServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        DATO("Listar Dato Persona", "dato-persona");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
