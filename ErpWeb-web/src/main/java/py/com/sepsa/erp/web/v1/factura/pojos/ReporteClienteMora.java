
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * POJO de reporte cliente mora
 * @author Cristina Insfrán
 */
public class ReporteClienteMora {


   /**
     * Fecha desde
     */
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Tipo de reporte
     */
    private String tipoReporte;
     /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código del tipo de tarifa
     */
    private String codigo;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Character como string
     */
    private Boolean charToString;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Identificador de comercial
     */
    private Integer idComercial;
    
    /**
     * Comercial
     */
    private String comercial;
    
    /**
     * Monto total facturado
     */
    private BigDecimal montoTotalFacturado = BigDecimal.ZERO;
    
    /**
     * Monto total pendiente
     */
    private BigDecimal montoTotalPendiente = BigDecimal.ZERO;
    
    /**
     * Porcentaje de mora
     */
    private BigDecimal porcentajeMora = BigDecimal.ZERO;
    
    /**
     * Monto 0
     */
    private BigDecimal monto0 = BigDecimal.ZERO;
    
    /**
     * Monto 30
     */
    private BigDecimal monto30 = BigDecimal.ZERO;
    
    /**
     * Monto 60
     */
    private BigDecimal monto60 = BigDecimal.ZERO;
    
    /**
     * Monto 90
     */
    private BigDecimal monto90 = BigDecimal.ZERO;
    
    /**
     * Monto 120
     */
    private BigDecimal monto120 = BigDecimal.ZERO;
    
    /**
     * Monto 150
     */
    private BigDecimal monto150 = BigDecimal.ZERO;
    
    /**
     * Monto 180
     */
    private BigDecimal monto180 = BigDecimal.ZERO;
     /**
     * Mes inicio seleccionado para la consulta
     */
    private Integer mesDesde;
     /**
     * Año inicio seleccionado para la consulta
     */
    private Integer anhoDesde;

    /**
     * Mes fin seleccionado para la consulta
     */
    private Integer mesHasta;

    /**
     * Año fin seleccionado para la consulta
     */
    private Integer anhoHasta;
        /**
     * Lista de años seleccionables
     */
    private List<SelectItem> listAnhos = new ArrayList();
    /**
     * Reporte resument
     */
    private Boolean resumen;
    
    /**
     * Constructor
     */
    public ReporteClienteMora(){
        
    }
    
    /**
     * Contrusctor con paramtros
     * @param mesHasta
     * @param anhoHasta
     */
    public ReporteClienteMora(Integer mesHasta, Integer anhoHasta){
        
        this.mesHasta = mesHasta;
        this.anhoHasta = anhoHasta;
        for (int i = 2015; i <= anhoHasta; i++) {
            listAnhos.add(new SelectItem(i, i + ""));
        }
        
    }
    //<editor-fold defaultstate="collapsed" desc="***Getters y Setters***">
    
    /**
     * @return the resumen
     */
    public Boolean getResumen() {
        return resumen;
    }

    /**
     * @param resumen the resumen to set
     */
    public void setResumen(Boolean resumen) {
        this.resumen = resumen;
    }
    /**
     * @return the listAnhos
     */
    public List<SelectItem> getListAnhos() {
        return listAnhos;
    }

    /**
     * @param listAnhos the listAnhos to set
     */
    public void setListAnhos(List<SelectItem> listAnhos) {
        this.listAnhos = listAnhos;
    }

    
    /**
     * @return the mesDesde
     */
    public Integer getMesDesde() {
        return mesDesde;
    }
    
    /**
     * @param mesDesde the mesDesde to set
     */
    public void setMesDesde(Integer mesDesde) {
        this.mesDesde = mesDesde;
    }
    
    /**
     * @return the anhoDesde
     */
    public Integer getAnhoDesde() {
        return anhoDesde;
    }
    
    /**
     * @param anhoDesde the anhoDesde to set
     */
    public void setAnhoDesde(Integer anhoDesde) {
        this.anhoDesde = anhoDesde;
    }
    
    /**
     * @return the mesHasta
     */
    public Integer getMesHasta() {
        return mesHasta;
    }
    
    /**
     * @param mesHasta the mesHasta to set
     */
    public void setMesHasta(Integer mesHasta) {
        this.mesHasta = mesHasta;
    }
    
    /**
     * @return the anhoHasta
     */
    public Integer getAnhoHasta() {
        return anhoHasta;
    }
    
    /**
     * @param anhoHasta the anhoHasta to set
     */
    public void setAnhoHasta(Integer anhoHasta) {
        this.anhoHasta = anhoHasta;
    }
    
    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }
    
    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }
    
    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }
    
    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
    
    /**
     * @return the idProducto
     */
    public Integer getIdProducto() {
        return idProducto;
    }
    
    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }
    
    /**
     * @return the tipoReporte
     */
    public String getTipoReporte() {
        return tipoReporte;
    }
    
    /**
     * @param tipoReporte the tipoReporte to set
     */
    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }
    
    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }
    
    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }
    
    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    /**
     * @return the charToString
     */
    public Boolean getCharToString() {
        return charToString;
    }
    
    /**
     * @param charToString the charToString to set
     */
    public void setCharToString(Boolean charToString) {
        this.charToString = charToString;
    }
    
    /**
     * @return the idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }
    
    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }
    
    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * @return the idComercial
     */
    public Integer getIdComercial() {
        return idComercial;
    }
    
    /**
     * @param idComercial the idComercial to set
     */
    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }
    
    /**
     * @return the comercial
     */
    public String getComercial() {
        return comercial;
    }
    
    /**
     * @param comercial the comercial to set
     */
    public void setComercial(String comercial) {
        this.comercial = comercial;
    }
    
    /**
     * @return the montoTotalFacturado
     */
    public BigDecimal getMontoTotalFacturado() {
        return montoTotalFacturado;
    }
    
    /**
     * @param montoTotalFacturado the montoTotalFacturado to set
     */
    public void setMontoTotalFacturado(BigDecimal montoTotalFacturado) {
        this.montoTotalFacturado = montoTotalFacturado;
    }
    
    /**
     * @return the montoTotalPendiente
     */
    public BigDecimal getMontoTotalPendiente() {
        return montoTotalPendiente;
    }
    
    /**
     * @param montoTotalPendiente the montoTotalPendiente to set
     */
    public void setMontoTotalPendiente(BigDecimal montoTotalPendiente) {
        this.montoTotalPendiente = montoTotalPendiente;
    }
    
    /**
     * @return the porcentajeMora
     */
    public BigDecimal getPorcentajeMora() {
        return porcentajeMora;
    }
    
    /**
     * @param porcentajeMora the porcentajeMora to set
     */
    public void setPorcentajeMora(BigDecimal porcentajeMora) {
        this.porcentajeMora = porcentajeMora;
    }
    
    /**
     * @return the monto0
     */
    public BigDecimal getMonto0() {
        return monto0;
    }
    
    /**
     * @param monto0 the monto0 to set
     */
    public void setMonto0(BigDecimal monto0) {
        this.monto0 = monto0;
    }
    
    /**
     * @return the monto30
     */
    public BigDecimal getMonto30() {
        return monto30;
    }
    
    /**
     * @param monto30 the monto30 to set
     */
    public void setMonto30(BigDecimal monto30) {
        this.monto30 = monto30;
    }
    
    /**
     * @return the monto60
     */
    public BigDecimal getMonto60() {
        return monto60;
    }
    
    /**
     * @param monto60 the monto60 to set
     */
    public void setMonto60(BigDecimal monto60) {
        this.monto60 = monto60;
    }
    
    /**
     * @return the monto90
     */
    public BigDecimal getMonto90() {
        return monto90;
    }
    
    /**
     * @param monto90 the monto90 to set
     */
    public void setMonto90(BigDecimal monto90) {
        this.monto90 = monto90;
    }
    
    /**
     * @return the monto120
     */
    public BigDecimal getMonto120() {
        return monto120;
    }
    
    /**
     * @param monto120 the monto120 to set
     */
    public void setMonto120(BigDecimal monto120) {
        this.monto120 = monto120;
    }
    
    /**
     * @return the monto150
     */
    public BigDecimal getMonto150() {
        return monto150;
    }
    
    /**
     * @param monto150 the monto150 to set
     */
    public void setMonto150(BigDecimal monto150) {
        this.monto150 = monto150;
    }
    
    /**
     * @return the monto180
     */
    public BigDecimal getMonto180() {
        return monto180;
    }
    
    /**
     * @param monto180 the monto180 to set
     */
    public void setMonto180(BigDecimal monto180) {
        this.monto180 = monto180;
    }
//</editor-fold>
}
