/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.info.remote.MetricaService;

/**
 * Adapter para metrica info
 *
 * @author Williams Vera
 */
public class MetricaAdapter extends DataListAdapter<Metrica> {

    /**
     * Cliente para el servicio producto
     */
    private final MetricaService metricaService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MetricaAdapter fillData(Metrica searchData) {

        return metricaService.getMetricaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de MetricaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MetricaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.metricaService = new MetricaService();
    }

    /**
     * Constructor de MetricaAdapter
     */
    public MetricaAdapter() {
        super();
        this.metricaService = new MetricaService();
    }
}
