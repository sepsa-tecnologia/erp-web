
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ServicioProducto;
import py.com.sepsa.erp.web.v1.comercial.remote.ServicioProductoService;

/**
 * Adaptador para la lista de servicio
 * @author Romina Núñez
 */
public class ServicioProductoAdapter extends DataListAdapter<ServicioProducto>{
    
    /**
     * Cliente para el servicio producto
     */
    private final ServicioProductoService servicioProductoService;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ServicioProductoAdapter fillData(ServicioProducto searchData) {
     
        return servicioProductoService.getServicioProductoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ServicioProductoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ServicioProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.servicioProductoService = new ServicioProductoService();
    }

    /**
     * Constructor de ServicioProductoAdapter
     */
    public ServicioProductoAdapter() {
        super();
        this.servicioProductoService= new ServicioProductoService();
    }
}
