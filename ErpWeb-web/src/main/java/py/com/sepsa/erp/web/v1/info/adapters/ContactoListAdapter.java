package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.remote.ContactoServiceClient;

/**
 * Adaptador de la lista de contacto
 *
 * @author Cristina Insfrán
 */
public class ContactoListAdapter extends DataListAdapter<Contacto> {

    /**
     * Cliente para el servicio de contacto
     */
    private final ContactoServiceClient contactoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoListAdapter fillData(Contacto searchData) {

        return contactoClient.getContactoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ContactoListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoClient = new ContactoServiceClient();
    }

    /**
     * Constructor de ContactoListAdapter
     */
    public ContactoListAdapter() {
        super();
        this.contactoClient = new ContactoServiceClient();
    }
}
