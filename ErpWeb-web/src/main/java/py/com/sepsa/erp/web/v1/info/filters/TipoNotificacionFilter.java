package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoNotificacion;

/**
 * Filtro utilizado para el servicio de tipo notificación
 *
 * @author Romina Núñez
 */
public class TipoNotificacionFilter extends Filter {

  
    /**
     * Agrega el filtro de email
     *
     * @param email
     * @return
     */
    public TipoNotificacionFilter email(String email) {
        if (email != null && !email.trim().isEmpty()) {
            params.put("email", email.trim());
        }
        return this;
    }

 
    
    /**
     * Agrega el filtro teléfono
     *
     * @param telefono
     * @return
     */
    public TipoNotificacionFilter telefono(String telefono) {
        if (telefono != null && !telefono.trim().isEmpty()) {
            params.put("telefono", telefono.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoNotificacion   
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoNotificacion tipoNotificacion, Integer page, Integer pageSize) {
        TipoNotificacionFilter filter = new TipoNotificacionFilter();

        filter
                .email(tipoNotificacion.getEmail())
                .telefono(tipoNotificacion.getTelefono())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoNotificacionFilter
     */
    public TipoNotificacionFilter() {
        super();
    }
}
