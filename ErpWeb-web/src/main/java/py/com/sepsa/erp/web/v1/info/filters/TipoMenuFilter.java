package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;

/**
 * Filter para tipoMenu
 *
 * @author alext
 */
public class TipoMenuFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id Identificador de tipoMenu
     * @return
     */
    public TipoMenuFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por descripción
     *
     * @param descripcion descripción de tipoMenu
     * @return
     */
    public TipoMenuFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro por código
     *
     * @param codigo código de tipoMenu
     * @return
     */
    public TipoMenuFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param tipoMenu datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la página de resultado
     * @return
     */
    public static Map build(TipoMenu tipoMenu, Integer page, Integer pageSize) {
        TipoMenuFilter filter = new TipoMenuFilter();

        filter
                .id(tipoMenu.getId())
                .descripcion(tipoMenu.getDescripcion())
                .codigo(tipoMenu.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();

    }

    /**
     * Constructor de TipoMenuFilter
     */
    public TipoMenuFilter() {
        super();
    }

}
