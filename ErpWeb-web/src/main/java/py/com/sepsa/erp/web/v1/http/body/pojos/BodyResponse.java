package py.com.sepsa.erp.web.v1.http.body.pojos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ContextNotActiveException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.json.JSONException;
import org.json.JSONObject;
import py.com.sepsa.erp.web.v1.controllers.message.MessageController;
import py.com.sepsa.erp.web.v1.http.pojos.HttpResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.http.pojos.HttpStringResponse;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;
import py.com.sepsa.erp.web.v1.utils.DateDeserializer;

/**
 * POJO para el cuerpo de las respuestas HTTP
 *
 * @author Daniel F. Escauriza Arza
 * @param <T>
 */
public class BodyResponse<T> {

    /**
     * Código de respuesta
     */
    private BodyResponseStatus status;
    /**
     * Bandera que indica el suceso
     */
    private Boolean success;
    /**
     * Datos de respuesta
     */
    private Object payload;

    /**
     * Obtiene el estatus de la respuesta
     *
     * @return Estatus de la respuesta
     */
    public BodyResponseStatus getStatus() {
        return status;
    }

    /**
     * Setea el estatus de la respuesta
     *
     * @param status Estatus de la respuesta
     */
    public void setStatus(BodyResponseStatus status) {
        this.status = status;
    }

    /**
     * Setea los datos de la respuesta
     *
     * @param payload Datos de la respuesta
     */
    public void setPayload(Object payload) {
        this.payload = payload;
    }

    /**
     * @return the payload
     */
    public Object getPayload() {
        return payload;
    }

    /**
     * @return the success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * Parsea la respuesta en bruto a un objeto BodyResponse
     *
     * @param conn Conexión HTTP
     * @param payloadClass Clase del payload
     * @return Objecto BodyResponse con el payload formateado
     */
    public static BodyResponse createInstance(HttpURLConnection conn,
            Class payloadClass) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
        return createInstance(conn, payloadClass, gson);
    }

    public static BodyResponse createInstance(HttpURLConnection conn,
            Class payloadClass, Gson gson) {
        BodyResponse bodyResponse = new BodyResponse();
        try {
            HttpStringResponse response = HttpStringResponse
                    .createInstance(conn);
            String input = response.getInput();
            if (input != null && !input.trim().isEmpty()) {
                JSONObject root = new JSONObject(input);
                Boolean success = root.isNull("success")
                        ? null
                        : (Boolean) root.getBoolean("success");
                JSONObject statusJson = root.getJSONObject("status");
                BodyResponseStatus status = gson.fromJson(statusJson.toString(),
                        BodyResponseStatus.class);
                Object payload = null;
                if (success && root.has("payload")) {
                    Object payloadJson = root.get("payload");
                    payload = gson.fromJson(payloadJson.toString(), payloadClass);
                }
                bodyResponse = new BodyResponse(status, success, payload);
                //Push de errores al controlador de mensajes
                try {
                    if (!success && response.getCode().equals(HttpResponse.ResponseCode.ERROR)) {
                        FacesContext.getCurrentInstance()
                                .addMessage(null,
                                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                "Error", "Error en el servicio!"));
                    }
                } catch (Exception e) {
                    WebLogger.get().fatal(e);
                }
                try {
                    FacesContext context = FacesContext.getCurrentInstance();
                    if (context != null) {
                        MessageController messageController
                                = context
                                        .getApplication()
                                        .evaluateExpressionGet(
                                                context,
                                                "#{message}",
                                                MessageController.class);
                        List<Mensaje> messageList = status.getMensajes();
                        messageController.push(messageList);
                    }
                } catch (ContextNotActiveException e) {
                    
                }
                catch (Exception e) {
                    WebLogger.get().fatal(e);
                }
            }
        } catch (JsonSyntaxException | JSONException ex) {
            WebLogger.get().fatal(ex);
        }
        return bodyResponse;
    }

    /**
     * Constructor de BodyResponse
     *
     * @param status Estatus de la respuesta
     * @param success Si la respuesta tuvo exito
     * @param payload Datos de la respuesta
     */
    public BodyResponse(BodyResponseStatus status, Boolean success,
            Object payload) {
        this.status = status;
        this.success = success;
        this.payload = payload;
    }

    /**
     * Constructor de BodyResponse
     */
    public BodyResponse() {
        Calendar now = Calendar.getInstance();
        this.success = true;
        this.status = new BodyResponseStatus(401, now.toString());
        this.payload = "Sin datos en la respuesta";
    }
}
