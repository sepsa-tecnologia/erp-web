package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.ReverseGeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.DireccionServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para Clientes
 *
 * @author Cristina Insfrán, Romina Núñez
 */
@ViewScoped
@Named("clienteDireccion")
public class ClienteDireccionEditController implements Serializable {

    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para el listado de personas
     */
    private PersonaListAdapter adapterPersonaDireccion;
    /**
     * Servicios remoto para cliente
     */
    private ClientServiceClient serviceClient;
    /**
     * Servicio remoto para persona
     */
    private PersonaServiceClient servicePersona;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para el listado de dirección
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionEdit;
    /**
     * Objeto MapModel
     */
    private MapModel draggableModel;
    /**
     * Latitud de la direccion del local
     */
    private double lat;
    /**
     * Longitud de la direccion del local
     */
    private double lng;
    /**
     * Dirección
     */
    private String direccion;
    /**
     * Objeto Marker
     */
    private Marker marker;
    /**
     * Dato del cliente
     */
    private String idCliente;
    /**
     * Identificador de la dirección
     */
    private String idDireccion;
    /**
     * Identificador del local
     */
    private String idLocal;
    /**
     * Adaptador para el listado de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * POJO Cliente
     */
    private Cliente clienteFilter;
    /**
     * Cliente remoto para dirección
     */
    private DireccionServiceClient direccionServiceCliente;
    /**
     * Adaptador de la lista de tipo de referencia geografica
     */
    private TipoReferenciaGeograficaAdapter adapterTipoRefGeo;
    /**
     * Objeto tipo referencia geo
     */
    private TipoReferenciaGeografica tipoRefGeo;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la lista de local
     */
    private LocalListAdapter adapterListLocal;
    private String dir;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    private Integer idDepartamento;
    private Integer idDistrito;
    private Integer idCiudad;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the servicePersona
     */
    public PersonaServiceClient getServicePersona() {
        return servicePersona;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }

    /**
     * @param servicePersona the servicePersona to set
     */
    public void setServicePersona(PersonaServiceClient servicePersona) {
        this.servicePersona = servicePersona;
    }

    /**
     * @return the serviceClient
     */
    public ClientServiceClient getServiceClient() {
        return serviceClient;
    }

    /**
     * @param serviceClient the serviceClient to set
     */
    public void setServiceClient(ClientServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    /**
     * @return the adapterListLocal
     */
    public LocalListAdapter getAdapterListLocal() {
        return adapterListLocal;
    }

    /**
     * @param adapterListLocal the adapterListLocal to set
     */
    public void setAdapterListLocal(LocalListAdapter adapterListLocal) {
        this.adapterListLocal = adapterListLocal;
    }

    /**
     * @return the idLocal
     */
    public String getIdLocal() {
        return idLocal;
    }

    /**
     * @param idLocal the idLocal to set
     */
    public void setIdLocal(String idLocal) {
        this.idLocal = idLocal;
    }

    /**
     * @return the adapterTipoRefGeo
     */
    public TipoReferenciaGeograficaAdapter getAdapterTipoRefGeo() {
        return adapterTipoRefGeo;
    }

    /**
     * @param adapterTipoRefGeo the adapterTipoRefGeo to set
     */
    public void setAdapterTipoRefGeo(TipoReferenciaGeograficaAdapter adapterTipoRefGeo) {
        this.adapterTipoRefGeo = adapterTipoRefGeo;
    }

    /**
     * @return the tipoRefGeo
     */
    public TipoReferenciaGeografica getTipoRefGeo() {
        return tipoRefGeo;
    }

    /**
     * @param tipoRefGeo the tipoRefGeo to set
     */
    public void setTipoRefGeo(TipoReferenciaGeografica tipoRefGeo) {
        this.tipoRefGeo = tipoRefGeo;
    }

    /**
     * @return the adapterRefGeo
     */
    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    /**
     * @param adapterRefGeo the adapterRefGeo to set
     */
    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    /**
     * @return the referenciaGeo
     */
    public ReferenciaGeografica getReferenciaGeo() {
        return referenciaGeo;
    }

    /**
     * @param referenciaGeo the referenciaGeo to set
     */
    public void setReferenciaGeo(ReferenciaGeografica referenciaGeo) {
        this.referenciaGeo = referenciaGeo;
    }

    /**
     * @return the referenciaGeoDepartamento
     */
    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    /**
     * @param referenciaGeoDepartamento the referenciaGeoDepartamento to set
     */
    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    /**
     * @return the referenciaGeoDistrito
     */
    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    /**
     * @param referenciaGeoDistrito the referenciaGeoDistrito to set
     */
    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    /**
     * @return the referenciaGeoCiudad
     */
    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    /**
     * @param referenciaGeoCiudad the referenciaGeoCiudad to set
     */
    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    /**
     * @return the idDireccion
     */
    public String getIdDireccion() {
        return idDireccion;
    }

    /**
     * @param idDireccion the idDireccion to set
     */
    public void setIdDireccion(String idDireccion) {
        this.idDireccion = idDireccion;
    }

    public void setPersonaFilter(Persona personaFilter) {

        this.personaFilter = personaFilter;

    }

    public void setDireccionServiceCliente(DireccionServiceClient direccionServiceCliente) {
        this.direccionServiceCliente = direccionServiceCliente;
    }

    public DireccionServiceClient getDireccionServiceCliente() {
        return direccionServiceCliente;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setDraggableModel(MapModel draggableModel) {
        this.draggableModel = draggableModel;
    }

    public MapModel getDraggableModel() {
        return draggableModel;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public void setDireccionEdit(Direccion direccionEdit) {
        this.direccionEdit = direccionEdit;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public void setAdapterPersonaDireccion(PersonaListAdapter adapterPersonaDireccion) {
        this.adapterPersonaDireccion = adapterPersonaDireccion;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public Direccion getDireccionEdit() {
        return direccionEdit;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public PersonaListAdapter getAdapterPersonaDireccion() {
        return adapterPersonaDireccion;
    }

//</editor-fold>
    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {

        ReferenciaGeografica referenciaGeoDepartamento = new ReferenciaGeografica();
        referenciaGeoDepartamento.setDescripcion(query);
        referenciaGeoDepartamento.setIdTipoReferenciaGeografica(1);
        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDepartamento);

        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        referenciaGeoDistrito = new ReferenciaGeografica();
        referenciaGeoCiudad = new ReferenciaGeografica();
        adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        direccionEdit.setIdDepartamento(direccionFilter.getIdDepartamento());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
        referenciaGeoDistrito.setDescripcion(query);
        referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
        return adapterRefGeo.getData();

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        direccionEdit.setIdDistrito(direccionFilter.getIdDistrito());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        ReferenciaGeografica referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionEdit.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la dirección del cliente
     *
     */
    public void datosClienteDireccion() {

        if (idDireccion != null) {

            referenciaGeoDepartamento = new ReferenciaGeografica();
            referenciaGeoDistrito = new ReferenciaGeografica();
            direccionFilter.setId(Integer.valueOf(idDireccion));
            direccionAdapter = direccionAdapter.fillData(direccionFilter);

            if (!direccionAdapter.getData().isEmpty()) {
                direccionEdit.setId(direccionAdapter.getData().get(0).getId());
                direccionEdit.setLatitud(direccionAdapter.getData().get(0).getLatitud());
                direccionEdit.setLongitud(direccionAdapter.getData().get(0).getLongitud());
                direccionEdit.setDireccion(direccionAdapter.getData().get(0).getDireccion());
                direccionEdit.setNumero(direccionAdapter.getData().get(0).getNumero());
                direccionEdit.setObservacion(direccionAdapter.getData().get(0).getObservacion());

                if (direccionAdapter.getData().get(0).getDepartamento() != null) {
                    referenciaGeoDepartamento.setDescripcion(direccionAdapter.getData().get(0).getDepartamento().getDescripcion());
                    direccionFilter.setIdDepartamento(direccionAdapter.getData().get(0).getDepartamento().getId());
                    this.completeQueryDpto(direccionAdapter.getData().get(0).getDepartamento().getDescripcion());
                    direccionEdit.setIdDepartamento(direccionFilter.getIdDepartamento());
                    this.idDepartamento = direccionFilter.getIdDepartamento();
                }

                if (direccionAdapter.getData().get(0).getDistrito() != null) {
                    referenciaGeoDistrito.setDescripcion(direccionAdapter.getData().get(0).getDistrito().getDescripcion());
                    direccionFilter.setIdDistrito(direccionAdapter.getData().get(0).getDistrito().getId());
                    this.completeQueryDistritos(direccionAdapter.getData().get(0).getDistrito().getDescripcion());
                    direccionEdit.setIdDistrito(direccionFilter.getIdDistrito());
                    this.idDistrito = direccionFilter.getIdDistrito();
                }

                if (direccionAdapter.getData().get(0).getCiudad() != null) {
                    referenciaGeoCiudad.setDescripcion(direccionAdapter.getData().get(0).getCiudad().getDescripcion());
                    direccionFilter.setIdCiudad(direccionAdapter.getData().get(0).getCiudad().getId());
                    this.completeQueryCiudad(direccionAdapter.getData().get(0).getCiudad().getDescripcion());
                    direccionEdit.setIdCiudad(direccionFilter.getIdCiudad());
                    this.idCiudad = direccionFilter.getIdCiudad();
                    WebLogger.get().debug("CIUDAD " + idCiudad);
                }

                if (direccionAdapter.getData().get(0).getLatitud() != null) {
                    lat = Double.parseDouble(direccionAdapter.getData().get(0).getLatitud());
                } else {
                    lat = -25.286534173063284;
                }

                if (direccionAdapter.getData().get(0).getLongitud() != null) {
                    lng = Double.parseDouble(direccionAdapter.getData().get(0).getLongitud());
                } else {
                    lng = -57.63632285404583;
                }

                draggableModel = new DefaultMapModel();
                LatLng coord = new LatLng(lat, lng);
                draggableModel.addOverlay(new Marker(coord));
                for (Marker premarker : draggableModel.getMarkers()) {
                    premarker.setDraggable(true);
                }
            }

        }

    }

    /**
     * Enfoca el mapa en la direccion indicada
     *
     * @param event Evento que contiene la direccion
     */
    public void onGeocode(GeocodeEvent event) {
        GeocodeResult result = event.getResults().get(0);
        if (result != null) {
            LatLng center = result.getLatLng();
            lat = center.getLat();
            lng = center.getLng();
            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setDraggable(true);
                premarker.setLatlng(center);
                premarker.setTitle(result.getAddress());
            });
        }
    }

    /**
     * Obtiene la direccion exacta de la coordenada indicada
     *
     * @param event Evento que contiene las coordenadas
     */
    public void onReverseGeocode(ReverseGeocodeEvent event) {

        String address = event.getAddresses().get(0);
        LatLng center = event.getLatlng();
        if (address != null && center != null) {
            lat = center.getLat();
            lng = center.getLng();
            direccionEdit.setDireccion(address);

            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setLatlng(center);
                premarker.setTitle(address);
            });
        }
        PrimeFaces.current().executeScript("initAutocomplete();");
    }

    /**
     *
     * @param event
     */
    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        lat = marker.getLatlng().getLat();
        lng = marker.getLatlng().getLng();

    }

    /**
     * Método para filtrar el cliente y obtener los datos
     */
    public void filterClienteDireccion() {
        clienteFilter.setIdCliente(Integer.parseInt(idCliente));
        adapterCliente = adapterCliente.fillData(clienteFilter);

        if (!adapterCliente.getData().isEmpty()) {
            clienteFilter.setRazonSocial(adapterCliente.getData().get(0).getRazonSocial());
            clienteFilter.setNroDocumento(adapterCliente.getData().get(0).getNroDocumento());
        }

    }

    /**
     * Método para editar la dirección
     */
    public void editDireccion() {

        direccionEdit.setLatitud(String.valueOf(lat));
        direccionEdit.setLongitud(String.valueOf(lng));
        direccionEdit.setIdDepartamento(idDepartamento);
        direccionEdit.setIdDistrito(idDistrito);
        direccionEdit.setIdCiudad(idCiudad);

        if (direccionEdit.getId() != null) {
            BodyResponse direccionE = direccionServiceCliente.editDireccion(direccionEdit);
            if (direccionE.getSuccess() == true) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Dirección actualizada!"));
                datosClienteDireccion();
            }

        } else {
            BodyResponse<Direccion> respuestaDireccion = direccionServiceCliente.setDireccion(direccionEdit);

            if (respuestaDireccion.getSuccess()) {
                personaFilter.setId(Integer.valueOf(idCliente));

                adapterPersonaDireccion = adapterPersonaDireccion.fillData(personaFilter);

                if (!adapterPersonaDireccion.getData().isEmpty()) {
                    personaFilter = adapterPersonaDireccion.getData().get(0);

                    personaFilter.setIdDireccion(((Direccion) respuestaDireccion.getPayload()).getId());

                    BodyResponse<Persona> respuestaPersona = servicePersona.putPersona(personaFilter);
                    if (respuestaPersona.getSuccess()) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                                "Info", "Dirección actualizada!"));
                        datosClienteDireccion();
                    }
                }
            }
        }
    }

    public void filterDepartamento() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(10000);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.adapterPersonaDireccion = new PersonaListAdapter();
        this.direccionAdapter = new DireccionAdapter();
        this.direccionFilter = new Direccion();
        this.direccionEdit = new Direccion();
        this.adapterCliente = new ClientListAdapter();
        this.clienteFilter = new Cliente();
        this.direccionServiceCliente = new DireccionServiceClient();
        this.adapterTipoRefGeo = new TipoReferenciaGeograficaAdapter();
        this.tipoRefGeo = new TipoReferenciaGeografica();
        this.referenciaGeo = new ReferenciaGeografica();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.adapterListLocal = new LocalListAdapter();
        this.serviceClient = new ClientServiceClient();
        this.servicePersona = new PersonaServiceClient();
        adapterTipoRefGeo = adapterTipoRefGeo.fillData(tipoRefGeo);

        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;

        filterCiudad();
        filterClienteDireccion();
        datosClienteDireccion();
        filterDepartamento();
        filterDistrito();

    }

    /**
     * Constructor de la clase
     */
    public ClienteDireccionEditController() {
        this.personaFilter = new Persona();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();

        String idC = (String) myRequest.getParameter("idCliente");
        String idDir = (String) myRequest.getParameter("idDireccion");

        idCliente = idC;
        if (!idDir.isEmpty()) {
            idDireccion = idDir;

        }
    }

}
