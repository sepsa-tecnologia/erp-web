package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioResumenAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.InventarioDetalleFilter;
import py.com.sepsa.erp.web.v1.inventario.filters.InventarioFilter;
import py.com.sepsa.erp.web.v1.inventario.filters.InventarioResumenFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Inventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Servicio para listar inventario
 *
 * @author Alexander Triana
 */
public class InventarioService extends APIErpInventario {

    /**
     * Método para obtener la lista de inventario
     *
     * @param inventario
     * @param page
     * @param pageSize
     * @return
     */
    public InventarioAdapter getInventario(Inventario inventario, Integer page,
            Integer pageSize) {
        InventarioAdapter lista = new InventarioAdapter();
        try {
            Map params = InventarioFilter.build(inventario, page, pageSize);
            HttpURLConnection conn = GET(Resource.INVENTARIO.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        InventarioAdapter.class);
                lista = (InventarioAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return lista;
    }

    /**
     * Método para obtener la lista de inventario
     *
     * @param inventarioResumen
     * @param page
     * @param pageSize
     * @return
     */
    public InventarioResumenAdapter getInventarioResumen(InventarioResumen inventarioResumen, Integer page,
            Integer pageSize) {
        InventarioResumenAdapter lista = new InventarioResumenAdapter();
        try {
            Map params = InventarioResumenFilter.build(inventarioResumen, page, pageSize);
            HttpURLConnection conn = GET(Resource.INVENTARIO_RESUMEN.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        InventarioResumenAdapter.class);
                lista = (InventarioResumenAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return lista;
    }

    /**
     * Método para obtener la lista de inventario
     *
     * @param inventarioDetalle
     * @param page
     * @param pageSize
     * @return
     */
    public InventarioDetalleAdapter getInventarioDetalle(InventarioDetalle inventarioDetalle, Integer page,
            Integer pageSize) {
        InventarioDetalleAdapter lista = new InventarioDetalleAdapter();
        try {
            Map params = InventarioDetalleFilter.build(inventarioDetalle, page, pageSize);
            HttpURLConnection conn = GET(Resource.INVENTARIO_DETALLE.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        InventarioDetalleAdapter.class);
                lista = (InventarioDetalleAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return lista;
    }

    /**
     * Método para crear inventario
     *
     * @param inventario
     * @return
     */
    public BodyResponse<Inventario> setInventario(Inventario inventario) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.INVENTARIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(inventario));
            response = BodyResponse.createInstance(conn, Inventario.class);
        }
        return response;
    }

    /**
     * Constructor de la clase
     */
    public InventarioService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpFacturacion
     */
    public enum Resource {

        //Servicios
        INVENTARIO("Inventario", "inventario"),
        INVENTARIO_RESUMEN("Inventario", "inventario/resumen"),
        INVENTARIO_DETALLE("Inventario", "inventario-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
