/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;

/**
 *
 * @author Romina Núñez
 */
public class ResguardoDocumento {
    /**
     * Identificador de tipo factura
     */
    private Integer id;
    /**
     * nombre de archivo
     */
    private String nombreArchivo;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercion;
    /**
     * Tipo Documento
     */
    private TipoDocumento tipoDocumento;
    /**
     * Identificador de tipo de documento
     */
    private Integer idTipoDocumento;
    /**
     * Fecha inserción desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha inserción hasta
     */
    private Date fechaInsercionHasta;
    /**
     * URL Descarga
     */
    private String urlDescarga;

    public Integer getId() {
        return id;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setUrlDescarga(String urlDescarga) {
        this.urlDescarga = urlDescarga;
    }

    public String getUrlDescarga() {
        return urlDescarga;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    

    public ResguardoDocumento() {
    }
    
}
