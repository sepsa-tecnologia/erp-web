/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para editar vehiculo de traslado
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("vehiculoTrasladoEdit")

public class VehiculoTrasladoEditController implements Serializable {

    /**
     * Cliente para el servicio de vehiculo-traslado.
     */
    private final VehiculoTrasladoService service;
    /**
     * POJO del producto
     */
    private VehiculoTraslado vehiculoTraslado;
    
    private Integer modalidadTransporte;

    public Integer getModalidadTransporte() {
        return modalidadTransporte;
    }

    public void setModalidadTransporte(Integer modalidadTransporte) {
        this.modalidadTransporte = modalidadTransporte;
    }

    public VehiculoTraslado getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    
    public void formatearCampos() {
    if (vehiculoTraslado != null){
            if (vehiculoTraslado.getMarca() != null) {
                String str = vehiculoTraslado.getMarca().trim().replace(" ","");
               vehiculoTraslado.setMarca(str);
            }
            
            if (vehiculoTraslado.getNroIdentificacion() != null){
                String str = vehiculoTraslado.getNroIdentificacion().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroIdentificacion(str);
            }
            
            if (vehiculoTraslado.getNroMatricula() != null){
                String str = vehiculoTraslado.getNroMatricula().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroMatricula(str);
            }
            
            if (vehiculoTraslado.getNroVuelo() != null){
                String str = vehiculoTraslado.getNroVuelo().trim().replace(" ", "").replace(".", "");
                vehiculoTraslado.setNroVuelo(str);
            }
     
        }    
    }
    
    public boolean validarCampos() {
        formatearCampos();
        boolean save = true;
        if (vehiculoTraslado != null) {
            if (vehiculoTraslado.getTipoVehiculo().trim().equalsIgnoreCase("") ) {
                    save = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de vehículo!"));
            }
            
            if (modalidadTransporte == null) {
                save = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar modalidad y tipo de transporte!"));
            }
            
            if (vehiculoTraslado.getMarca().trim().equalsIgnoreCase("") ) {
                    save = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la Marca!"));
            }
            
            if (vehiculoTraslado.getTipoIdentificacion() == null ) {
                    save = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de identificación del vehículo!"));
            } else {
                if (vehiculoTraslado.getTipoIdentificacion() == 1){
                    if (vehiculoTraslado.getNroIdentificacion().trim().equalsIgnoreCase("") ) {
                        save = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el número de identificación del vehículo!"));
                    } 
                } else if (vehiculoTraslado.getTipoIdentificacion() == 2){
                    if (vehiculoTraslado.getNroMatricula().trim().equalsIgnoreCase("") ) {
                        save = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el número de matricula del vehículo!"));
                    }
                }
            }
            
            

        } else {
            save = false;
        }

        return save;
    }

    //</editor-fold>
    /**
     * Método para editar Producto
     */
    public void edit() {
        boolean create = validarCampos();

        if (create){
            BodyResponse<VehiculoTraslado> respuestaProducto = service.editVehiculoTraslado(this.vehiculoTraslado);
            if (respuestaProducto.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Vehiculo de traslado editado correctamente!"));
            } else {
                for (Mensaje mensaje : respuestaProducto.getStatus().getMensajes()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
                }
            }
        }
    }

   
    /**
     * Inicializa los datos
     */
    private void init(int id) {
        try {
            this.vehiculoTraslado = service.get(id);
        } catch (Exception e) {
            System.err.print(e);
        }
    }

    /**
     * Constructor
     */
    public VehiculoTrasladoEditController() {
        this.service = new VehiculoTrasladoService();
        this.modalidadTransporte = 1;
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
}
