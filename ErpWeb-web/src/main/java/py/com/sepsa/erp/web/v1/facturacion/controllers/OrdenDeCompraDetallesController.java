/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraDetalleAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetalles;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetallesPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;

/**
 * Controlador para la vista de detalles de orden de compra
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("ordenDeCompraDetalles")
public class OrdenDeCompraDetallesController implements Serializable {

    /**
     * Cliente para el servicio de OrdenDeCompra.
     */
    private final OrdenDeCompraService service;
    /**
     * Adaptador para la lista de orden de compra
     */
    private OrdenDeCompraAdapter ordenDeCompraAdapterList;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompra;
    /**
     * Adaptador para estado
     */
    private EstadoAdapter estadoAdapterList;
    /**
     * Pojo de estado
     */

    private Estado estado;

    private boolean edit;

    private List<OrdenCompraDetalles> listaDetalles = new ArrayList<>();
    /**
     * Identificador de OC
     */
    private Integer idOC;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;
    /**
     * Bandera
     */
    private boolean config_picking;
    /**
     * POJO Producto Precio para parámetro
     */
    private ProductoPrecio parametroProductoPrecio;
    /**
     * Adapter Orden de Compra
     */
    private OrdenDeCompraDetalleAdapter adapterOrdenCompraDetalle;
    /**
     * POJO Orden de Compra
     */
    private OrdenCompraDetallesPojo ordenCompraFilter;
    /**
     * Título de la página
     */
    private String titulo;
    /**
     * Descripción de la página
     */
    private String descripcion;
    /**
     * Objeto depositoDestinoLogistico
     */
    private DepositoLogistico depositoDestino;
    /**
     * Adaptador de la lista de depositoDestinoLogistico
     */
    private DepositoLogisticoAdapter depositoDestinoAdapter;
    /**
     * Identificador de Desposito
     */
    private Integer idDepositoDestino;
    /**
     * Registra Inventario
     */
    private String registraInventario;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public OrdenDeCompraAdapter getOrdenDeCompraAdapterList() {
        return ordenDeCompraAdapterList;
    }

    public void setRegistraInventario(String registraInventario) {
        this.registraInventario = registraInventario;
    }

    public String getRegistraInventario() {
        return registraInventario;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public DepositoLogisticoAdapter getDepositoDestinoAdapter() {
        return depositoDestinoAdapter;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public void setDepositoDestinoAdapter(DepositoLogisticoAdapter depositoDestinoAdapter) {
        this.depositoDestinoAdapter = depositoDestinoAdapter;
    }

    public void setDepositoDestino(DepositoLogistico depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public DepositoLogistico getDepositoDestino() {
        return depositoDestino;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public OrdenDeCompraService getService() {
        return service;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setOrdenCompraFilter(OrdenCompraDetallesPojo ordenCompraFilter) {
        this.ordenCompraFilter = ordenCompraFilter;
    }

    public void setAdapterOrdenCompraDetalle(OrdenDeCompraDetalleAdapter adapterOrdenCompraDetalle) {
        this.adapterOrdenCompraDetalle = adapterOrdenCompraDetalle;
    }

    public OrdenCompraDetallesPojo getOrdenCompraFilter() {
        return ordenCompraFilter;
    }

    public OrdenDeCompraDetalleAdapter getAdapterOrdenCompraDetalle() {
        return adapterOrdenCompraDetalle;
    }

    public void setParametroProductoPrecio(ProductoPrecio parametroProductoPrecio) {
        this.parametroProductoPrecio = parametroProductoPrecio;
    }

    public ProductoPrecio getParametroProductoPrecio() {
        return parametroProductoPrecio;
    }

    public void setConfig_picking(boolean config_picking) {
        this.config_picking = config_picking;
    }

    public boolean isConfig_picking() {
        return config_picking;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setIdOC(Integer idOC) {
        this.idOC = idOC;
    }

    public Integer getIdOC() {
        return idOC;
    }

    public void setListaDetalles(List<OrdenCompraDetalles> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

    public List<OrdenCompraDetalles> getListaDetalles() {
        return listaDetalles;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setOrdenDeCompraAdapterList(OrdenDeCompraAdapter ordenDeCompraAdapterList) {
        this.ordenDeCompraAdapterList = ordenDeCompraAdapterList;
    }

    public OrdenDeCompra getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void setOrdenDeCompra(OrdenDeCompra ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

//</editor-fold>
    /**
     * Inicializa los datos
     */
    public void habilitarEdicion() {
        edit = true;
    }

    private void init(int id) {
        this.ordenDeCompra = service.get(id);
        this.idOC = id;
        this.edit = false;
        this.config_picking = false;
        this.listaDetalles = ordenDeCompra.getOrdenCompraDetalles();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.parametroProductoPrecio = new ProductoPrecio();

        this.depositoDestino = new DepositoLogistico();
        this.depositoDestino.setListadoPojo(true);
        this.depositoDestinoAdapter = new DepositoLogisticoAdapter();
        this.idDepositoDestino = null;

        this.adapterOrdenCompraDetalle = new OrdenDeCompraDetalleAdapter();
        this.ordenCompraFilter = new OrdenCompraDetallesPojo();
        this.registraInventario = null;

        if (ordenDeCompra.getRecibido().equals("S")) {
            this.titulo = "DETALLE DE PEDIDO";
            this.descripcion = "Página para ver detalles de Pedido";
        } else {
            this.titulo = "DETALLE DE ORDEN DE COMPRA";
            this.descripcion = "Página para ver detalles de Orden de Compra";
            obtenerConfiguracionRegistroInventario();
        }
        obtenerConfiguracion();
        obtenerDetalleOC(idOC);
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracionRegistroInventario() {
        ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
        ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("REGISTRO_INVENTARIO");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            registraInventario = "N";
        } else {
            registraInventario = adapterConfigValor.getData().get(0).getValor();
        }

    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        this.idDepositoDestino = ((DepositoLogistico) event.getObject()).getId();
        this.ordenDeCompra.setIdDepositoLogistico(idDepositoDestino);
    }

    /**
     * Autocomplete para depositoDestinoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        depositoDestino = new DepositoLogistico();
        depositoDestino.setDescripcion(query);
        depositoDestino.setListadoPojo(true);
        depositoDestinoAdapter = depositoDestinoAdapter.fillData(depositoDestino);

        return depositoDestinoAdapter.getData();
    }

    public BigDecimal obtenerPrecioLista(OrdenCompraDetallesPojo ocDetalle) {
        BigDecimal valorCero = new BigDecimal("0");
        ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
        ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
        configuracionValorFilter.setCodigoConfiguracion("PRODUCTO_PRECIO_MONEDA");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        MonedaAdapter adapterMonedaDefecto = new MonedaAdapter();
        Moneda monedadefecto = new Moneda();
        monedadefecto.setCodigo(adapterConfigValor.getData().get(0).getValor());
        adapterMonedaDefecto = adapterMonedaDefecto.fillData(monedadefecto);
        if (!adapterMonedaDefecto.getData().isEmpty()) {
            parametroProductoPrecio.setIdMoneda(adapterMonedaDefecto.getData().get(0).getId());
        } else {
            parametroProductoPrecio.setIdMoneda(0);

        }

        Calendar today = Calendar.getInstance();
        ProductoService serviceProductoPrecio = new ProductoService();

        parametroProductoPrecio.setIdProducto(ocDetalle.getIdProducto());
        parametroProductoPrecio.setFecha(today.getTime());

        if (ordenDeCompra.getIdClienteOrigen() != null) {
            parametroProductoPrecio.setIdCliente(ordenDeCompra.getIdClienteOrigen());
            if (ordenDeCompra.getClienteOrigen().getIdCanalVenta() != null) {
                parametroProductoPrecio.setIdCanalVenta(ordenDeCompra.getClienteOrigen().getIdCanalVenta());
            } else {
                parametroProductoPrecio.setIdCanalVenta(0);
            }

        } else {
            parametroProductoPrecio.setIdCanalVenta(0);
            parametroProductoPrecio.setIdCliente(0);
        }

        ProductoPrecio prodRespuesta = new ProductoPrecio();
        WebLogger.get().debug("PRIMERA");
        prodRespuesta = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);

        if (prodRespuesta == null) {
            parametroProductoPrecio.setIdCliente(0);
            parametroProductoPrecio.setIdCanalVenta(0);
            ProductoPrecio prodRespuesta2 = new ProductoPrecio();
            WebLogger.get().debug("SEGUNDA");
            prodRespuesta2 = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);
            if (prodRespuesta2 == null) {
                return valorCero;

            } else {

                return prodRespuesta2.getPrecio();

            }

        } else {

            return prodRespuesta.getPrecio();

        }

    }

    /**
     * Método para obtener detalle de OC
     */
    public void obtenerDetalleOC(Integer idOc) {
        ordenCompraFilter.setIdOrdenCompra(idOc);
        adapterOrdenCompraDetalle = adapterOrdenCompraDetalle.fillData(ordenCompraFilter);
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        configuracionValorFilter.setCodigoConfiguracion("PICKING_ORDER");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData().get(0).getValor().equals("S")) {
            config_picking = true;
        }

        WebLogger.get().debug("VALOR=" + config_picking);
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos(Integer idDetalle) {
        OrdenCompraDetallesPojo info = null;

        for (OrdenCompraDetallesPojo info0 : adapterOrdenCompraDetalle.getData()) {
            if (Objects.equals(info0.getId(), idDetalle)) {
                info = info0;
                break;
            }
        }

        BigDecimal cantidadConfirmada = info.getCantidadConfirmada();
        WebLogger.get().debug("Cantidad confirmada" + cantidadConfirmada);
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();

        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidadConfirmada);

        BigDecimal iva = new BigDecimal(info.getPorcentajeIva());
        BigDecimal ceroPorc = new BigDecimal("0");
        BigDecimal cincoPorc = new BigDecimal("5");
        BigDecimal diezPorc = new BigDecimal("10");
        BigDecimal prueba = new BigDecimal("0");

        if (iva.compareTo(ceroPorc) == 0) {
            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (iva.compareTo(cincoPorc) == 0) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (iva.compareTo(diezPorc) == 0) {
            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }
        if (info.getMontoTotal().compareTo(prueba) == 0) {

        } else {
            calcularTotal();
        }

    }

    /**
     * Método para calcular el total de la factura
     */
    public void calcularTotal() {
        BigDecimal totalOC = new BigDecimal("0");

        for (int i = 0; i < listaDetalles.size(); i++) {
            BigDecimal montoTotal = listaDetalles.get(i).getMontoTotal();
            totalOC = totalOC.add(montoTotal);
        }

        ordenDeCompra.setMontoTotalOrdenCompra(totalOC);
    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalles.size(); i++) {
            if (listaDetalles.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalles.get(i).getMontoImponible());
            } else if (listaDetalles.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalles.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalles.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalles.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalles.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalles.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalles.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalles.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalles.get(i).getMontoIva());

        }

        ordenDeCompra.setMontoIva5(montoIva5Acumulador);
        ordenDeCompra.setMontoImponible5(montoImponible5Acumulador);
        ordenDeCompra.setMontoTotal5(montoTotal5Acumulador);
        ordenDeCompra.setMontoIva10(montoIva10Acumulador);
        ordenDeCompra.setMontoImponible10(montoImponible10Acumulador);
        ordenDeCompra.setMontoTotal10(montoTotal10Acumulador);
        ordenDeCompra.setMontoTotalExento(montoExcentoAcumulador);
        ordenDeCompra.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        ordenDeCompra.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para rechazar OC
     */
    public void rechazarOC() {
        String mensaje = " rechazada correctamente";
        String codigo = "RECHAZADO";

        for (OrdenCompraDetalles detalle : listaDetalles) {
            detalle.setIdOrdenCompra(idOC);
        }

        ordenDeCompra.setOrdenCompraDetalles(listaDetalles);
        calcularTotalesGenerales();

        BodyResponse<OrdenDeCompra> respuestaDeOC = service.anularOrdenCompra(ordenDeCompra);

        if (respuestaDeOC.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "OC rechazada"));
            init(ordenDeCompra.getId());
        }

        // llamadaServicio(mensaje, ordenDeCompra);
    }

    /**
     * Método para aprobar OC
     */
    public void aprobarOC() {
        String mensaje = " aprobada correctamente";
        String codigo = "PENDIENTE_FACTURACION";

        for (OrdenCompraDetallesPojo info0 : adapterOrdenCompraDetalle.getData()) {
            for (OrdenCompraDetalles detalle : listaDetalles) {
                detalle.setIdOrdenCompra(idOC);
                if (Objects.equals(detalle.getId(), info0.getId())) {
                    detalle.setCantidadConfirmada(info0.getCantidadConfirmada());
                    break;
                }
            }

        }

        ordenDeCompra.setOrdenCompraDetalles(listaDetalles);
        calcularTotalesGenerales();

        ordenDeCompra.setIdEstado(obtenerEstado(codigo));
        llamadaServicio(mensaje, ordenDeCompra);
    }

    /**
     * Método para aprobar OC
     */
    public void confirmarOC() {
        String mensaje = " confirmada correctamente";
        String codigo = "CONFIRMADO";

        for (OrdenCompraDetallesPojo info0 : adapterOrdenCompraDetalle.getData()) {
            for (OrdenCompraDetalles detalle : listaDetalles) {
                detalle.setIdOrdenCompra(idOC);
                detalle.setIdDepositoLogistico(idDepositoDestino);
                detalle.setCodigoEstadoInventario("DISPONIBLE");
                if (Objects.equals(detalle.getId(), info0.getId())) {
                    detalle.setCantidadConfirmada(info0.getCantidadConfirmada());
                    detalle.setFechaVencimiento(info0.getFechaVencimiento());
                    break;
                }
            }

        }

        ordenDeCompra.setOrdenCompraDetalles(listaDetalles);
        calcularTotalesGenerales();

        ordenDeCompra.setIdEstado(obtenerEstado(codigo));
        ordenDeCompra.setRegistroInventario(registraInventario);
        llamadaServicio(mensaje, ordenDeCompra);
    }

    /**
     * Método para aprobar OC
     */
    public void aprobarPreparacionOC() {
        String mensaje = " aprobada correctamente";
        String codigo = "PENDIENTE_PREPARACION";

        for (OrdenCompraDetallesPojo info0 : adapterOrdenCompraDetalle.getData()) {
            for (OrdenCompraDetalles detalle : listaDetalles) {
                detalle.setIdOrdenCompra(idOC);
                if (Objects.equals(detalle.getId(), info0.getId())) {
                    detalle.setCantidadConfirmada(info0.getCantidadConfirmada());
                    break;
                }
            }

        }

        ordenDeCompra.setOrdenCompraDetalles(listaDetalles);
        calcularTotalesGenerales();

        ordenDeCompra.setIdEstado(obtenerEstado(codigo));
        llamadaServicio(mensaje, ordenDeCompra);
    }

    /**
     * Método para obtener estado
     *
     * @param cod
     * @return
     */
    public Integer obtenerEstado(String cod) {
        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoOC = new Estado();
        estadoOC.setCodigo(cod);
        estadoOC.setCodigoTipoEstado("ORDEN_COMPRA");
        adapterEstado = adapterEstado.fillData(estadoOC);
        return adapterEstado.getData().get(0).getId();
    }

    public void llamadaServicio(String msg, OrdenDeCompra ordenCompraEdit) {
        try {
            BodyResponse<OrdenDeCompra> respuestaDeOC = service.editarOrdenCompra(ordenCompraEdit);

            if (respuestaDeOC.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "OC" + msg));
                init(ordenCompraEdit.getId());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Constructor
     */
    public OrdenDeCompraDetallesController() {
        this.service = new OrdenDeCompraService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
}
