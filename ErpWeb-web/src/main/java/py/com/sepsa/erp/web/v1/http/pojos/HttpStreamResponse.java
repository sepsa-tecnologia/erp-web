/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.http.pojos;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * POJO para la respuesta en formato Stream
 * @author Daniel F. Escauriza Arza
 */
public class HttpStreamResponse extends HttpResponse<byte[]> {
    
    /**
     * Crea una instancia de HttpStreamResponse
     * @param conn Conexión HTTP
     * @return Instancia HttpStreamResponse
     */
    public static HttpStreamResponse createInstance(HttpURLConnection conn) {
        return new HttpStreamResponse(conn);
    }
    
    /**
     * Constructor de HttpStreamResponse
     * @param conn Conexión HTTP
     */
    public HttpStreamResponse(HttpURLConnection conn) {
        
        try {
            int respCode = conn.getResponseCode();
            this.code = respCode == 200 ? ResponseCode.OK : ResponseCode.ERROR;
            
            InputStream stream = code.equals(ResponseCode.OK) 
                    ? conn.getInputStream() 
                    : conn.getErrorStream();
            
            if(stream != null) {
                
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = stream.read(buffer);
                
                while (bytesRead != -1) {
                    output.write(buffer, 0, bytesRead);
                    bytesRead = stream.read(buffer);
                }
                
                output.flush();
                this.input = output.toByteArray();

            }
        
        } catch (IOException ex) {
            WebLogger.get().fatal(ex);
        }
        
    }

    /**
     * Constructor de HttpStreamResponse
     */
    public HttpStreamResponse() {}
    
}