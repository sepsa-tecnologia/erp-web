package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteNotaCreditoVentaParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteVentaNotaCreditoService;

/**
 * Controlador para reporte de nc de venta
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("reporteNotaCreditoVenta")
public class ReporteComprobanteNotaCreditoVentaController implements Serializable {

    /**
     * Objeto
     */
    private ReporteComprobanteNotaCreditoVentaParam parametro;

    /**
     * Cliente para servicio de descarga de archivo
     */
    private ReporteVentaNotaCreditoService reporteComprobanteNC;
    /**
     * POJO Cliente
     */
    private Cliente client;
    /**
     * Adapter Cliente
     */
    private ClientListAdapter adapterCliente;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }
    
    public ReporteComprobanteNotaCreditoVentaParam getParametro() {
        return parametro;
    }

    public void setParametro(ReporteComprobanteNotaCreditoVentaParam parametro) {
        this.parametro = parametro;
    }

    public ReporteVentaNotaCreditoService getReporteComprobanteNC() {
        return reporteComprobanteNC;
    }

    public void setReporteComprobanteNC(ReporteVentaNotaCreditoService reporteComprobanteNC) {
        this.reporteComprobanteNC = reporteComprobanteNC;
    }
    //</editor-fold>

    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {

        String contentType = ("application/zip");
        String fileName = "reporte-comprobante-nc-venta.zip";
        byte[] data = reporteComprobanteNC.getReporteComprobanteNcVenta(parametro);
        if (data != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(data),
                    contentType, fileName);
        }

        return null;
    }
    
        /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Detalle de lista de facturas pendientes y canceladas del cliente
     * seleccionado
     */
    public void onItemSelectCliente() {
        if (client != null && client.getIdCliente() != null) {
            this.parametro.setIdCliente(client.getIdCliente());
        }
    }


    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteComprobanteNotaCreditoVentaParam();
        this.reporteComprobanteNC = new ReporteVentaNotaCreditoService();
        Calendar today = Calendar.getInstance();
        parametro.setFecha(today.getTime());
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
    }

}
