package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;

/**
 * Filtro para servicio.
 *
 * @author alext
 */
public class ServicioFilter extends Filter {

    /**
     * Filtro por identificador de servicio.
     *
     * @param id
     * @return
     */
    public ServicioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Filtro por descripcion.
     *
     * @param descripcion
     * @return
     */
    public ServicioFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Filtro por código de servicio.
     *
     * @param codigo
     * @return
     */
    public ServicioFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Filtro por identificador de producto.
     *
     * @param idProducto
     * @return
     */
    public ServicioFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Filtro por código de producto.
     *
     * @param codigoProducto
     * @return
     */
    public ServicioFilter codigoProducto(String codigoProducto) {
        if (codigoProducto != null && !codigoProducto.trim().isEmpty()) {
            params.put("codigoProducto", codigoProducto);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param servicios
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(Servicios servicios, Integer page, Integer pageSize) {
        ServicioFilter filter = new ServicioFilter();

        filter
                .id(servicios.getId())
                .descripcion(servicios.getDescripcion())
                .codigo(servicios.getCodigo())
                .idProducto(servicios.getIdProducto())
                .codigoProducto(servicios.getCodigoProducto())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ServiciosFilter.
     */
    public ServicioFilter() {
        super();
    }

}
