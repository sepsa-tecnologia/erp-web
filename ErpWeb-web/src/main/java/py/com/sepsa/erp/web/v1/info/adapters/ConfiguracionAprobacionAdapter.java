package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionAprobacionService;

/**
 * Adaptador para configuración de aprobación.
 *
 * @author Alex Triana.
 */
public class ConfiguracionAprobacionAdapter extends DataListAdapter<ConfiguracionAprobacion> {

    /**
     * Cliente para el servicio configuracion aprobación.
     */
    private final ConfiguracionAprobacionService configuracionAprobacionService;

    /**
     * Método para cargar la lista de datos.
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ConfiguracionAprobacionAdapter fillData(ConfiguracionAprobacion searchData) {
        return configuracionAprobacionService.getList(searchData, getFirstResult(), getPageSize());
        
        /*ConfiguracionAprobacionAdapter lista = new ConfiguracionAprobacionAdapter();
        
        ConfiguracionAprobacion data1 = new ConfiguracionAprobacion(1, 2, 3, 4, 'A');
        
        lista.getData().add(data1);
        
        return lista;*/
    }

    /**
     * Constructor de ConfiguracionAprobacionAdapter.
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConfiguracionAprobacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.configuracionAprobacionService = new ConfiguracionAprobacionService();
    }

    /**
     * Constructor de ConfiguracionAprobacionAdapter.
     */
    public ConfiguracionAprobacionAdapter() {
        super();
        this.configuracionAprobacionService = new ConfiguracionAprobacionService();
    }
}
