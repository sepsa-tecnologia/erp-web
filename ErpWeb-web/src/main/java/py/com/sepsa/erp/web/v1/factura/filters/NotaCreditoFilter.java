package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro utilizado para el servicio de Nota Crédito
 *
 * @author Romina Núñez,Sergio D. Riveros Vazquez
 */
public class NotaCreditoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaCreditoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    public NotaCreditoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public NotaCreditoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nc
     *
     * @param nroNotaCredito
     * @return
     */
    public NotaCreditoFilter nroNotaCredito(String nroNotaCredito) {
        if (nroNotaCredito != null && !nroNotaCredito.trim().isEmpty()) {
            params.put("nroNotaCredito", nroNotaCredito);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaCreditoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaCreditoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo emision interno
     *
     * @param idMotivoEmisionInterno
     * @return
     */
    public NotaCreditoFilter idMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        if (idMotivoEmisionInterno != null) {
            params.put("idMotivoEmisionInterno", idMotivoEmisionInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public NotaCreditoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
        /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaCreditoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notaCredito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaCredito notaCredito, Integer page, Integer pageSize) {
        NotaCreditoFilter filter = new NotaCreditoFilter();

        filter
                .id(notaCredito.getId())
                .idCliente(notaCredito.getIdCliente())
                .razonSocial(notaCredito.getRazonSocial())
                .fecha(notaCredito.getFecha())
                .nroNotaCredito(notaCredito.getNroNotaCredito())
                .anulado(notaCredito.getAnulado())
                .idMotivoEmisionInterno(notaCredito.getIdMotivoEmisionInterno())
                .idFactura(notaCredito.getIdFactura())
                .listadoPojo(notaCredito.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoFilter
     */
    public NotaCreditoFilter() {
        super();
    }
}
