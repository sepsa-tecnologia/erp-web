package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionDetallePojo;

/**
 * Filtro para la lista de nota de credito detalle
 *
 * @author Williams Vera
 */
public class NotaRemisionDetallePojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de idNotaCredito
     *
     * @param idNotaCredito
     * @return
     */
    public NotaRemisionDetallePojoFilter idNotaRemision(Integer idNotaRemision) {
        if (idNotaRemision != null) {
            params.put("idNotaRemision", idNotaRemision);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idFactura
     *
     * @param idFactura
     * @return
     */
    public NotaRemisionDetallePojoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nroLinea
     *
     * @param nroLinea
     * @return
     */
    public NotaRemisionDetallePojoFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de porcentajeIva
     *
     * @param porcentajeIva
     * @return
     */
    public NotaRemisionDetallePojoFilter porcentajeIva(Integer porcentajeIva) {
        if (porcentajeIva != null) {
            params.put("porcentajeIva", porcentajeIva);
        }
        return this;
    }

    /**
     * Agrega el filtro para montoIva
     *
     * @param montoIva
     * @return
     */
    public NotaRemisionDetallePojoFilter montoIva(BigDecimal montoIva) {
        if (montoIva != null) {
            params.put("montoIva", montoIva.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoImponible
     *
     * @param montoImponible
     * @return
     */
    public NotaRemisionDetallePojoFilter montoImponible(BigDecimal montoImponible) {
        if (montoImponible != null) {
            params.put("montoImponible", montoImponible.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoTotal
     *
     * @param montoTotal
     * @return
     */
    public NotaRemisionDetallePojoFilter montoTotal(BigDecimal montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal.toPlainString());
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaRemisionDetallePojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agrega el filtro para anhoMes
     *
     * @param anhoMes
     * @return
     */
    public NotaRemisionDetallePojoFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaRemisionDetallePojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param nc
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaRemisionDetallePojo nr, Integer page, Integer pageSize) {
        NotaRemisionDetallePojoFilter filter = new NotaRemisionDetallePojoFilter();

        filter
                .idNotaRemision(nr.getIdNotaRemision())
                .idFactura(nr.getIdFactura())
                .nroLinea(nr.getNroLinea())
                .porcentajeIva(nr.getPorcentajeIva())
                .montoIva(nr.getMontoIva())
                .montoImponible(nr.getMontoImponible())
                .montoTotal(nr.getMontoTotal())
                .anhoMes(nr.getAnhoMes())
                .anulado(nr.getAnulado())
                .listadoPojo(nr.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaRemisionDetalleFilter
     */
    public NotaRemisionDetallePojoFilter() {
        super();
    }

}
