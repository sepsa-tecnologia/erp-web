package py.com.sepsa.erp.web.v1.configuracionAprobacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionAprobacionAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;

/**
 * Controlador para Configuración de Aprobación.
 *
 * @author alext
 */
@ViewScoped
@Named("listarConfiguracionAprobacion")
public class ConfiguracionAprobacionListarController implements Serializable {

    /**
     * Adaptador de Configuracion de Aprobación.
     */
    private ConfiguracionAprobacionAdapter adapter;

    /**
     * Objeto Configuracion de Aprobación.
     */
    private ConfiguracionAprobacion searchData;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public ConfiguracionAprobacionAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ConfiguracionAprobacionAdapter adapter) {
        this.adapter = adapter;
    }

    public ConfiguracionAprobacion getSearchData() {
        return searchData;
    }

    public void setSearchData(ConfiguracionAprobacion searchData) {
        this.searchData = searchData;
    }
    //</editor-fold>
    
    /**
     * Método para filtrar configuración de aprobación.
     */
    public void filter() {
        this.adapter = adapter.fillData(searchData);
    }
    
    /**
     * Método para limpiar filtros.
     */
    public void limpiar(){
        searchData = new ConfiguracionAprobacion();
        filter();
    }

    
    /**
     * Método para generar lista de configuracion de aprobación.
     */
    public void search() {
        adapter = new ConfiguracionAprobacionAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new ConfiguracionAprobacion();
        search();
    }
    
    public String editUrl(Integer id) {
        return String.format("configuracion-aprobacion-edit?faces-redirect=true&id=%d", id);
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public ConfiguracionAprobacionListarController() {

    }

}
