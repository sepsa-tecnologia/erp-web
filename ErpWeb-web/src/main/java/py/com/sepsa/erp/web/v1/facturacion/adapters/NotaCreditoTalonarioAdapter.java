package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class NotaCreditoTalonarioAdapter extends DataListAdapter<TalonarioPojo> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final NotaCreditoService serviceNC;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoTalonarioAdapter fillData(TalonarioPojo searchData) {

        return serviceNC.getNCTalonarioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoTalonarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNC = new NotaCreditoService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public NotaCreditoTalonarioAdapter() {
        super();
        this.serviceNC = new NotaCreditoService();
    }
}
