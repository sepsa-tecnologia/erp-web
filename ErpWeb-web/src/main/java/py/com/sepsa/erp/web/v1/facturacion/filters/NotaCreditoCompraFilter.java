/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para nota credito compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotaCreditoCompraFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaCreditoCompraFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de persona
     *
     * @param idPersona
     * @return
     */
    public NotaCreditoCompraFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public NotaCreditoCompraFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public NotaCreditoCompraFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo Emision Interna
     *
     * @param idMotivoEmisionInterno
     * @return
     */
    public NotaCreditoCompraFilter idMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        if (idMotivoEmisionInterno != null) {
            params.put("idMotivoEmisionInterno", idMotivoEmisionInterno);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaDesde
     * @return
     */
    public NotaCreditoCompraFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
                
            } catch (Exception e) {
                
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaHasta
     * @return
     */
    public NotaCreditoCompraFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
                
            } catch (Exception e) {
                
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaCreditoCompraFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public NotaCreditoCompraFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaCreditoCompraFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agregar el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public NotaCreditoCompraFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nc
     *
     * @param nroNotaCredito
     * @return
     */
    public NotaCreditoCompraFilter nroNotaCredito(String nroNotaCredito) {
        if (nroNotaCredito != null && !nroNotaCredito.trim().isEmpty()) {
            params.put("nroNotaCredito", nroNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro por idFacturaCompra
     *
     * @param idFacturaCompra
     * @return
     */
    public NotaCreditoCompraFilter idFacturaCompra(Integer idFacturaCompra) {
        if (idFacturaCompra != null) {
            params.put("idFacturaCompra", idFacturaCompra);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notaCreditoCompra datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaCreditoCompra notaCreditoCompra, Integer page, Integer pageSize) {
        NotaCreditoCompraFilter filter = new NotaCreditoCompraFilter();
        
        filter
                .id(notaCreditoCompra.getId())
                .idEmpresa(notaCreditoCompra.getIdEmpresa())
                .idPersona(notaCreditoCompra.getIdPersona())
                .idMoneda(notaCreditoCompra.getIdMoneda())
                .idFacturaCompra(notaCreditoCompra.getIdFacturaCompra())
                .idMotivoEmisionInterno(notaCreditoCompra.getIdMotivoEmisionInterno())
                .fechaDesde(notaCreditoCompra.getFechaDesde())
                .fechaHasta(notaCreditoCompra.getFechaHasta())
                .anulado(notaCreditoCompra.getAnulado())
                .digital(notaCreditoCompra.getDigital())
                .razonSocial(notaCreditoCompra.getRazonSocial())
                .ruc(notaCreditoCompra.getRuc())
                .nroNotaCredito(notaCreditoCompra.getNroNotaCredito())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoFilter
     */
    public NotaCreditoCompraFilter() {
        super();
    }
    
}
