package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para tipo de archivo
 *
 * @author alext
 */
public class TipoArchivo {

    /**
     * Identificador de tipo de archivo
     */
    private Integer id;
    /**
     * Parámetro descripción
     */
    private String descripcion;
    /**
     * Parámetro código
     */
    private String codigo;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public TipoArchivo() {
    }

}
