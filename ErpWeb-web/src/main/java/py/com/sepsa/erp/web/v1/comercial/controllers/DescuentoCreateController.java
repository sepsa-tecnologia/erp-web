/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServicioAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.TipoDescuentoListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoDescuento;
import py.com.sepsa.erp.web.v1.comercial.remote.DescuentoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;

/**
 * Controlador para crear Descuento
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("descuentoCreate")
public class DescuentoCreateController implements Serializable {

    /**
     * Cliente para el servicio de Descuento.
     */
    private final DescuentoService service;
    /**
     * POJO del descuento
     */
    private Descuento descuento;
    /**
     * Adaptador de la lista de tipo descuento
     */
    private TipoDescuentoListAdapter tipoDescuentoAdapter;
    /**
     * Datos de tipo Descuento
     */
    private TipoDescuento tipoDescuento;
    /**
     * Adaptador de la lista de estados
     */
    private EstadoAdapter estadoAdapter;
    /**
     * Datos del estado
     */
    private Estado estado;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de la lista de servicios
     */
    private ServicioAdapter servicioAdapter;
    /**
     * Datos del servicio
     */
    private Servicios servicio;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Descuento getDescuento() {
        return descuento;
    }

    public void setDescuento(Descuento descuento) {
        this.descuento = descuento;
    }

    public TipoDescuentoListAdapter getTipoDescuentoAdapter() {
        return tipoDescuentoAdapter;
    }

    public void setTipoDescuentoAdapter(TipoDescuentoListAdapter tipoDescuentoAdapter) {
        this.tipoDescuentoAdapter = tipoDescuentoAdapter;
    }

    public TipoDescuento getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(TipoDescuento tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ServicioAdapter getServicioAdapter() {
        return servicioAdapter;
    }

    public void setServicioAdapter(ServicioAdapter servicioAdapter) {
        this.servicioAdapter = servicioAdapter;
    }

    public Servicios getServicio() {
        return servicio;
    }

    public void setServicio(Servicios servicio) {
        this.servicio = servicio;
    }

    //</editor-fold>
    /**
     * Método para crear Descuento
     */
    public void create() {
        BodyResponse<Descuento> respuestaTal = service.setDescuento(this.descuento);
        if (respuestaTal.getSuccess()) {
            this.descuento = new Descuento();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Descuento creado correctamente!"));
        }
    }

    /**
     * Método para obtener las tipo descuentos
     */
    public void getTipoDescuentos() {
        this.setTipoDescuentoAdapter(getTipoDescuentoAdapter().fillData(getTipoDescuento()));
    }

    /**
     * Método para obtener estados
     */
    public void getEstados() {
        this.setEstadoAdapter(getEstadoAdapter().fillData(getEstado()));
    }

    /**
     * Método para obtener servicios
     */
    public void getServicios() {
        this.setServicioAdapter(getServicioAdapter().fillData(getServicio()));
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.descuento.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        this.descuento = new Descuento();
        this.tipoDescuentoAdapter = new TipoDescuentoListAdapter();
        this.tipoDescuento = new TipoDescuento();
        this.estadoAdapter = new EstadoAdapter();
        this.estado = new Estado();
        this.estado.setCodigoTipoEstado("DESCUENTO");
        this.servicio = new Servicios();
        this.servicioAdapter = new ServicioAdapter();
        getTipoDescuentos();
        getEstados();
        getServicios();
    }

    /**
     * Constructor
     */
    public DescuentoCreateController() {
        init();
        this.service = new DescuentoService();
    }

}
