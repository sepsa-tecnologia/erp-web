package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReporteEmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ReporteEmpresaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.ReporteEmpresa;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de reporte empresa
 *
 * @author Jonathan Bernal
 */
public class ReporteEmpresaService extends APIErpCore {

    public ReporteEmpresaAdapter find(ReporteEmpresa param, Integer page, Integer pageSize) {

        ReporteEmpresaAdapter lista = new ReporteEmpresaAdapter();

        Map params = ReporteEmpresaFilter.build(param, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ReporteEmpresaAdapter.class);

            lista = (ReporteEmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    public byte[] generar(JsonElement param) {

        HttpURLConnection conn = POST(Resource.GENERAR.url, ContentType.JSON);

        byte[] result = null;

        if (conn != null) {
            
            this.addBody(conn, param.toString());

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LISTAR("Listar Reporte Empresa", "reporte-empresa"),
        GENERAR("Generar reporte empresa", "reporte-empresa/generar");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
