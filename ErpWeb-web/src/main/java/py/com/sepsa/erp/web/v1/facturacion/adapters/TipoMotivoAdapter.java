package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoMotivoService;

/**
 * Adapter para tipoMotivo
 *
 * @author Alexander Triana
 */
public class TipoMotivoAdapter extends DataListAdapter<TipoMotivo> {

    /**
     * Cliente remoto para tipo tipoMotivo
     */
    private final TipoMotivoService tipoMotivoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return TipoMotivoAdapter
     */
    @Override
    public TipoMotivoAdapter fillData(TipoMotivo searchData) {

        return tipoMotivoClient.getTipoMotivo(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoMotivoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoMotivoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoMotivoClient = new TipoMotivoService();
    }

    /**
     * Constructor de TipoMotivoAdapter
     */
    public TipoMotivoAdapter() {
        super();
        this.tipoMotivoClient = new TipoMotivoService();
    }
}
