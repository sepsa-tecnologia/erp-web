/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("listarDeposito")
public class DepositoLogisticoListController implements Serializable {

    /**
     * Adaptador para la lista de motivos
     */
    private DepositoLogisticoAdapter depositoAdapterList;

    /**
     * POJO de Deposito
     */
    private DepositoLogistico depositoFilter;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de tipo documento
     */
    private Local localFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public DepositoLogisticoAdapter getDepositoAdapterList() {
        return depositoAdapterList;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setDepositoAdapterList(DepositoLogisticoAdapter depositoAdapterList) {
        this.depositoAdapterList = depositoAdapterList;
    }

    public void setDepositoFilter(DepositoLogistico depositoFilter) {
        this.depositoFilter = depositoFilter;
    }

    public DepositoLogistico getDepositoFilter() {
        return depositoFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener depositos
     */
    public void getDepositos() {
        getDepositoAdapterList().setFirstResult(0);
        getDepositoFilter().setListadoPojo(true);
        this.depositoAdapterList = this.depositoAdapterList.fillData(depositoFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.depositoFilter = new DepositoLogistico();
        getDepositos();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editDeposito(Integer id) {
        return String.format("deposito-edit?faces-redirect=true&id=%d", id);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        local.setLocalExterno("N");
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        depositoFilter.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.depositoFilter = new DepositoLogistico();
        this.depositoAdapterList = new DepositoLogisticoAdapter();
        
        this.localFilter = new Local();
        this.localAdapterList = new LocalListAdapter();
        getDepositos();
    }

    /**
     * Constructor
     */
    public DepositoLogisticoListController() {
        init();
    }

}
