package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaCreditoDetalleFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaCreditoDetallePojoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaCreditoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaCreditoPojoFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Nota de Credito
 *
 * @author Romina Núñez
 */
public class NotaCreditoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de nota de credito
     *
     * @param notaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoAdapter getNotaCreditoList(NotaCredito notaCredito, Integer page,
            Integer pageSize) {

        NotaCreditoAdapter lista = new NotaCreditoAdapter();

        Map params = NotaCreditoFilter.build(notaCredito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoAdapter.class);

            lista = (NotaCreditoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de nota de credito
     *
     * @param notaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoPojoAdapter getNotaCreditoPojoList(NotaCreditoPojo notaCredito, Integer page,
            Integer pageSize) {

        NotaCreditoPojoAdapter lista = new NotaCreditoPojoAdapter();

        Map params = NotaCreditoPojoFilter.build(notaCredito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoPojoAdapter.class);

            lista = (NotaCreditoPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene detalles de la nota de crédito
     *
     * @param notaCredito Objeto
     * @return
     */
    public NotaCreditoPojo getNotaCreditoDetalle(NotaCreditoPojo notaCredito) {

        NotaCreditoPojo nc = new NotaCreditoPojo();

        Map params = new HashMap<>();

        params.put("listadoPojo", notaCredito.getListadoPojo());

        String url = "nota-credito/id/" + notaCredito.getId();

        HttpURLConnection conn = GET(url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoPojo.class);

            nc = (NotaCreditoPojo) response.getPayload();

            conn.disconnect();
        }
        return nc;
    }

    /**
     * Obtiene detalles de la nota de crédito
     *
     * @param notaCredito Objeto
     * @return
     */
    public NotaCreditoCompra getNotaCreditoCompraDetalle(NotaCreditoCompra notaCredito) {

        NotaCreditoCompra nc = new NotaCreditoCompra();

        Map params = new HashMap<>();

        String url = "nota-credito-compra/id/" + notaCredito.getId();

        HttpURLConnection conn = GET(url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoCompra.class);

            nc = (NotaCreditoCompra) response.getPayload();

            conn.disconnect();
        }
        return nc;
    }

    /**
     * Datos de NC
     *
     * @param digital
     * @return
     */
    public DatosNotaCredito getDatosNotaCredito(Map parametros) {

        DatosNotaCredito dnc = new DatosNotaCredito();

        Map params = new HashMap<>();

        params = parametros;

        HttpURLConnection conn = GET(Resource.DATOS_NOTA_CREDITO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DatosNotaCredito.class);

            dnc = (DatosNotaCredito) response.getPayload();

            conn.disconnect();
        }
        return dnc;
    }

    /**
     * Método para crear Nota Crédito
     *
     * @param notaCredito
     * @return
     */
    public BodyResponse<NotaCredito> createNotaCredito(NotaCredito notaCredito) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.NOTA_CREDITO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaCredito));

            response = BodyResponse.createInstance(conn, NotaCredito.class);

        }
        return response;
    }
    
    /**
     * Método para crear nota de crédito de forma masiva
     *
     * @param notaCredito
     * @return
     */
    public byte[] sendFileMassive(Attach cargaNotaCredito) {

        byte[] result = null;

        HttpURLConnection conn = POST(Resource.CARGA_MASIVA.url, ContentType.MULTIPART);

        if (conn != null) {
            try {
                OutputStream os = conn.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(os), true);
                addFormFile(writer, os, "uploadedFile", cargaNotaCredito);
                BodyByteArrayResponse response = BodyByteArrayResponse
                        .createInstance(conn, writer);

                result = response.getPayload();

                conn.disconnect();

            } catch (IOException ex) {
                WebLogger.get().fatal(ex);
            }
        }

        return result;
    }

    /**
     * Método para crear Factura
     *
     * @param notaCredito
     * @return
     */
    public BodyResponse<NotaCredito> createNotaCreditoCompra(NotaCredito notaCredito) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.NOTA_CREDITO_COMPRA.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaCredito));

            response = BodyResponse.createInstance(conn, Factura.class);

        }
        return response;
    }

    /**
     * Obtiene la lista del detalle de nota de credito VERIFICAR DONDE SE USA
     * ESTE SERVICIO
     *
     * @param notaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoDetalleListAdapter getNotaCreditoDetalleList(NotaCreditoDetalle notaCredito, Integer page,
            Integer pageSize) {

        NotaCreditoDetalleListAdapter lista = new NotaCreditoDetalleListAdapter();

        Map params = NotaCreditoDetalleFilter.build(notaCredito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_DETALLE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoDetalleListAdapter.class);

            lista = (NotaCreditoDetalleListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Obtiene la lista del detalle de nota de credito VERIFICAR DONDE SE USA
     * ESTE SERVICIO
     *
     * @param notaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoDetalleListPojoAdapter getNotaCreditoDetalleListPojo(NotaCreditoDetallePojo notaCredito, Integer page,
            Integer pageSize) {

        NotaCreditoDetalleListPojoAdapter lista = new NotaCreditoDetalleListPojoAdapter();

        Map params = NotaCreditoDetallePojoFilter.build(notaCredito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_DETALLE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoDetalleListPojoAdapter.class);

            lista = (NotaCreditoDetalleListPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para anular nota de credito
     *
     * @param nota
     * @return
     */
    public BodyResponse anularNotaCredito(NotaCredito nota) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "nota-credito/anular/" + nota.getId();

        Map params = new HashMap<>();
        params.put("ignorarPeriodoAnulacion", nota.getIgnorarPeriodoAnulacion());

        HttpURLConnection conn = PUT(ANULAR_URL, ContentType.JSON, params);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(nota));

            response = BodyResponse.createInstance(conn, NotaCredito.class);

        }
        return response;
    }

    /**
     * Método para regenerar la NC
     *
     * @param notaCredito Objeto
     * @return
     */
    public BodyResponse<Factura> regenerarNC(NotaCredito notaCredito) {

        BodyResponse response = new BodyResponse();

        //String URL = "factura/anular/" + factura.getId();
        HttpURLConnection conn = PUT(Resource.NOTA_CREDITO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaCredito));
            response = BodyResponse.createInstance(conn,
                    NotaCredito.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Método para anular nota de credito
     *
     * @param nota
     * @return
     */
    public BodyResponse anularNotaCreditoCompra(NotaCreditoCompra nota) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "nota-credito-compra/anular/" + nota.getId();

        HttpURLConnection conn = PUT(ANULAR_URL, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(nota));

            response = BodyResponse.createInstance(conn, NotaCreditoCompra.class);

        }
        return response;
    }

    /**
     * Obtiene la lista de facturas recibidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaCreditoTalonarioAdapter getNCTalonarioList(TalonarioPojo talonario, Integer page,
            Integer pageSize) {

        NotaCreditoTalonarioAdapter lista = new NotaCreditoTalonarioAdapter();

        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_TALONARIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaCreditoTalonarioAdapter.class);

            lista = (NotaCreditoTalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de FacturacionService
     */
    public NotaCreditoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        NOTA_CREDITO("NotaCredito", "nota-credito"),
        NOTA_CREDITO_TALONARIO("NotaCredito Talonario", "nota-credito/talonario"),
        CARGA_MASIVA("NotaCreditoCargaMasiva", "nota-credito/crear-masivo"),
        NOTA_CREDITO_COMPRA("NotaCredito", "nota-credito-compra"),
        DATOS_NOTA_CREDITO("DatosNotaCredito", "nota-credito/datos-crear"),
        NOTA_CREDITO_CONSULTA("NotaCreditoDetalle", "nota-credito/consulta"),
        NOTA_CREDITO_DETALLE("nota credito detalle", "nota-credito-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
