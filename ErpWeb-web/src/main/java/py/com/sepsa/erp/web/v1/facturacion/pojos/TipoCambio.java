/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;

/**
 * Pojo para tipo cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoCambio {

    /**
     * Identificador de tipo de documento
     */
    private Integer id;
    /**
     * Fecha Insercion
     */
    private Date fechaInsercion;
    /**
     * Fecha Insercion
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha Insercion
     */
    private Date fechaInsercionHasta;
    /**
     * id Moneda
     */
    private Integer idMoneda;
    /**
     * Compra
     */
    private BigDecimal compra;
    /**
     * venta
     */
    private BigDecimal venta;
    /**
     * activo
     */
    private String activo;
    /**
     * moneda
     */
    private Moneda moneda;
    /**
     * Identificador de Cotizacion actual
     */
    private Integer idTipoCambioActual;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public BigDecimal getVenta() {
        return venta;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public BigDecimal getCompra() {
        return compra;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Integer getIdTipoCambioActual() {
        return idTipoCambioActual;
    }

    public void setIdTipoCambioActual(Integer idTipoCambioActual) {
        this.idTipoCambioActual = idTipoCambioActual;
    }
    
    public void TipoCambio() {

    }

}
