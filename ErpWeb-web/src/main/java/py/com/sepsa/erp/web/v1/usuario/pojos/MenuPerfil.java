package py.com.sepsa.erp.web.v1.usuario.pojos;

/**
 * POJO de Menu Perfil
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class MenuPerfil {

    /**
     * Identificador del menu
     */
    private Integer idMenu;
    /**
     * Código menu
     */
    private String codigoMenu;
    /**
     * Identificador del perfil
     */
    private Integer idPerfil;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Código perfil
     */
    private String codigoPerfil;
    /**
     * Objeto Perfil
     */
    private Perfil perfil;
    /**
     * Estado
     */
    private String activo;

    /**
     * @return the perfil
     */
    public Perfil getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    /**
     * @return the idMenu
     */
    public Integer getIdMenu() {
        return idMenu;
    }

    /**
     * @param idMenu the idMenu to set
     */
    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    /**
     * @return the codigoMenu
     */
    public String getCodigoMenu() {
        return codigoMenu;
    }

    /**
     * @param codigoMenu the codigoMenu to set
     */
    public void setCodigoMenu(String codigoMenu) {
        this.codigoMenu = codigoMenu;
    }

    /**
     * @return the idPerfil
     */
    public Integer getIdPerfil() {
        return idPerfil;
    }

    /**
     * @param idPerfil the idPerfil to set
     */
    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    /**
     * @return the codigoPerfil
     */
    public String getCodigoPerfil() {
        return codigoPerfil;
    }

    /**
     * @param codigoPerfil the codigoPerfil to set
     */
    public void setCodigoPerfil(String codigoPerfil) {
        this.codigoPerfil = codigoPerfil;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     * Constructor de la clase
     */
    public MenuPerfil() {

    }
}
