package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroDetallePojoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.SepsaSiediMonitorService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ArchivoAdjunto;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.notificacion.remote.NotificacionService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;

/**
 * Controlador para listar facturas
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
@SessionScoped
@Named("listarFactura")
public class FacturaListarController implements Serializable {

    /**
     * Adaptador para la lista de facturas
     */
    private FacturaPojoAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private FacturaPojo facturaFilter;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * Servicio Factura
     */
    private FacturacionService serviceFactura;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * POJO moneda
     */
    private Moneda moneda;
    /**
     * Dato de Liquidacion
     */
    private String idHistoricoLiquidacion;
    /**
     * Email Reenvio
     */
    private String emailReenvio;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    /**
     * POJO para Factura
     */
    private Factura facturaReenvio;
    private Retencion retencion;
    private RetencionAdapter retencionAsociadosAdapter;
    private NotaCredito notaCredito;
    private NotaCreditoAdapter notaCreditoAsociadosAdapter;
    private Boolean ignorarPeriodoAnulacion;
    
    private CobroDetallePojoListAdapter cobroDetalleAdapter;
    private CobroDetallePojo cobroDetalle;
    /**
     * Pojo de procesamiento de archivo
     */
    private DocumentoProcesamiento procesamientoFilter;
    /**
     * Adaptador de la lista de procesamiento de archivo
     */
    private FacturaProcesamientoAdapter adapterProcesamiento;
    private Boolean addEmail;
    /**
     * Lista de facturas seleccionadas para reenvio de notificaciónes
     */
    private List<FacturaPojo> selectedFacturas;
    
    private Cliente client;
    
    private ClientListAdapter adapterCliente;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    private String notificar;
    private Integer diasFiltro;
    private Boolean siediApi = false;
    

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getDiasFiltro() {
        return diasFiltro;
    }

    public void setDiasFiltro(Integer diasFiltro) {
        this.diasFiltro = diasFiltro;
    }
    
    public Boolean getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(Boolean siediApi) {
        this.siediApi = siediApi;
    }

    public String getNotificar() {
        return notificar;
    }

    public void setNotificar(String notificar) {
        this.notificar = notificar;
    }

    public List<FacturaPojo> getSelectedFacturas() {
        return selectedFacturas;
    }

    public void setSelectedFacturas(List<FacturaPojo> selectedFacturas) {
        this.selectedFacturas = selectedFacturas;
    }
    
    public String getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setAddEmail(Boolean addEmail) {
        this.addEmail = addEmail;
    }

    public Boolean getAddEmail() {
        return addEmail;
    }

    public void setFacturaFilter(FacturaPojo facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaPojoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaPojo getFacturaFilter() {
        return facturaFilter;
    }

    public FacturaPojoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setProcesamientoFilter(DocumentoProcesamiento procesamientoFilter) {
        this.procesamientoFilter = procesamientoFilter;
    }

    public void setAdapterProcesamiento(FacturaProcesamientoAdapter adapterProcesamiento) {
        this.adapterProcesamiento = adapterProcesamiento;
    }

    public DocumentoProcesamiento getProcesamientoFilter() {
        return procesamientoFilter;
    }

    public FacturaProcesamientoAdapter getAdapterProcesamiento() {
        return adapterProcesamiento;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setFacturaReenvio(Factura facturaReenvio) {
        this.facturaReenvio = facturaReenvio;
    }

    public Factura getFacturaReenvio() {
        return facturaReenvio;
    }

    public void setEmailReenvio(String emailReenvio) {
        this.emailReenvio = emailReenvio;
    }

    public String getEmailReenvio() {
        return emailReenvio;
    }

    public void setIdHistoricoLiquidacion(String idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @return the adapterMotivoAnulacion
     */
    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    /**
     * @param adapterMotivoAnulacion the adapterMotivoAnulacion to set
     */
    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    /**
     * @return the motivoAnulacion
     */
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    /**
     * @param motivoAnulacion the motivoAnulacion to set
     */
    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public Retencion getRetencion() {
        return retencion;
    }

    public void setRetencion(Retencion retencion) {
        this.retencion = retencion;
    }

    public RetencionAdapter getRetencionAsociadosAdapter() {
        return retencionAsociadosAdapter;
    }

    public void setRetencionAsociadosAdapter(RetencionAdapter retencionAsociadosAdapter) {
        this.retencionAsociadosAdapter = retencionAsociadosAdapter;
    }

    public NotaCredito getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(NotaCredito notaCredito) {
        this.notaCredito = notaCredito;
    }

    public NotaCreditoAdapter getNotaCreditoAsociadosAdapter() {
        return notaCreditoAsociadosAdapter;
    }

    public void setNotaCreditoAsociadosAdapter(NotaCreditoAdapter notaCreditoAsociadosAdapter) {
        this.notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter;
    }


    public CobroDetallePojoListAdapter getCobroDetalleAdapter() {
        return cobroDetalleAdapter;
    }

    public void setCobroDetalleAdapter(CobroDetallePojoListAdapter cobroDetalleAdapter) {
        this.cobroDetalleAdapter = cobroDetalleAdapter;
    }    

    public CobroDetallePojo getCobroDetalle() {
        return cobroDetalle;
    }

    public void setCobroDetalle(CobroDetallePojo cobroDetalle) {
        this.cobroDetalle = cobroDetalle;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }
    
//</editor-fold>
    
    /**
     * Metodo para limpiar filtros
     */
    public void clear() {
        facturaFilter = new FacturaPojo();
        client = new Cliente();
        asignarFiltrosFecha();
        filterFacturas();
    }

    /**
     * Método para filtrar las facturas
     */
    public void filterFacturas() {
        if (validacionesFiltrarFactura()) {
            getAdapterFactura().setFirstResult(0);
            this.adapterFactura = adapterFactura.fillData(facturaFilter);
        }
    }
    
    public boolean validacionesFiltrarFactura() {
        boolean save = true;
        if (facturaFilter.getIdCliente() == null && facturaFilter.getFechaDesde() == null && facturaFilter.getFechaHasta() == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "Se debe elegir al menos un cliente o un rango de fechas"));
            save = false;
        }else{
            if(facturaFilter.getIdCliente() == null) {
                if (facturaFilter.getFechaDesde() != null && facturaFilter.getFechaHasta() == null) {
                    FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", "Se debe indicar la fecha hasta"));
                    save = false;
                }else if (facturaFilter.getFechaDesde() == null && facturaFilter.getFechaHasta() != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha desde"));
                    save = false;
                }

                if (facturaFilter.getFechaDesde() != null && facturaFilter.getFechaHasta() != null) {
                    Instant from = facturaFilter.getFechaDesde().toInstant();
                    Instant to = facturaFilter.getFechaHasta().toInstant();
                    facturaFilter.setListadoPojo(true);

                    long days = ChronoUnit.DAYS.between(from, to);

                    Integer maxDias = 31;
                    if (diasFiltro != null) {
                        maxDias = diasFiltro;
                    }

                    if (days > maxDias) {

                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        "Fecha Incorrecta", String.format("El rango de fecha no puede ser más de %d días", maxDias)));
                        save = false;
                    }
                }
            }
        }
        return save;
    }
    
  
    
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        Cliente client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);
        return adapterCliente.getData();
    }
    
    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        this.facturaFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /**
     * Método para filtrar las monedas
     */
    public void filterMoneda() {
        adapterMoneda = adapterMoneda.fillData(moneda);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleURL(Integer id) {
        return String.format("factura-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }
    
     /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String repetirCarga(Integer id) {
        return String.format("factura-create"
                + "?faces-redirect=true"
                + "&idFactura=%d", id);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String notaCreditoURL(Integer id) {
        return String.format("nota-credito-create"
                + "?faces-redirect=true"
                + "&id=%d", id
        );
    }

    /**
     * Método para obtener bandera de redirección
     *
     * @param f
     * @return
     */
    public boolean allowRedirect(String cobrado, BigDecimal saldo) {
        boolean ar = false;
        if (cobrado.equals("S") || saldo.compareTo(BigDecimal.ZERO) == 0) {
            ar = false;
        } else {
            ar = true;
        }
        return ar;
    }

    /**
     * Método para mostrar el Mensaje
     * @param idFactura
     */
    public void showMessage(Integer idFactura) {
        String msg = "";
        
        NotaCredito ncFilter = new NotaCredito();
        ncFilter.setIdFactura(idFactura);
        NotaCreditoAdapter ncAdapter = new NotaCreditoAdapter();
        ncAdapter = ncAdapter.fillData(ncFilter);

        if(ncAdapter.getData()!=null && !ncAdapter.getData().isEmpty()){
            msg = "La Factura ya cuenta con NC. ";
        }
        
        Cobro cobroFilter = new Cobro();
        cobroFilter.setIdFactura(idFactura);
        CobroAdapter cobroAdapter = new CobroAdapter();
        cobroAdapter = cobroAdapter.fillData(cobroFilter);
        if(cobroAdapter.getData()!=null && !cobroAdapter.getData().isEmpty()){
            msg = msg + "Puede anular un cobro asociado para realizar la NC.";
        }
        
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "No puede crearse la NC porque la Factura ya está cobrada. "
                                + msg));
    }

    /**
     * Método para anular factura
     *
     * @param factura
     */
    public void anularFactura(FacturaPojo factura) {
        Factura fact = new Factura();
        fact.setId(factura.getId());
        fact.setIdMotivoAnulacion(factura.getIdMotivoAnulacion());
        fact.setObservacionAnulacion(factura.getObservacionAnulacion());
        fact.setIgnorarPeriodoAnulacion(ignorarPeriodoAnulacion);
        fact.setDigital(factura.getDigital());
        if (factura.getIdEstado() != null) {
            fact.setIdEstado(factura.getIdEstado());
        }
        BodyResponse<Factura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.anularFactura(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura anulada correctamente!"));
            facturaFilter = new FacturaPojo();
            asignarFiltrosFecha();
            filterFacturas();

        }
    }

    public void guardarFacturaAReenviar(Integer id, String nro, String email, String digital, String cdc, String cliente) {
        this.facturaReenvio = new Factura();
        Factura factFilter = new Factura();
        FacturaAdapter factAdapter = new FacturaAdapter();
        
        factFilter.setId(id);
        factAdapter = factAdapter.fillData(factFilter);
        
        if (factAdapter.getData() != null && !factAdapter.getData().isEmpty()) {
            this.facturaReenvio = factAdapter.getData().get(0);
        } else {
            this.facturaReenvio.setId(id);
            this.facturaReenvio.setNroFactura(nro);
            this.facturaReenvio.setDigital(digital);
            this.facturaReenvio.setCdc(cdc);
            this.facturaReenvio.setRazonSocial(cliente);
        }
        this.emailReenvio = null;
        this.emailReenvio = email;

        if (emailReenvio != null) {
            addEmail = true;
        } else {
            addEmail = false;
        }
    }
    
    public String crearCobro(Integer idCliente, Integer nroFactura){
         return String.format("/app/cobro/cobro-direct-create.xhtml"
                + "?faces-redirect=true"
                + "&idCliente=%d"
                + "&nroFactura=%d",idCliente, nroFactura);
    }

    public void validarCorreo(String email) {
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void reenviar() {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String cliente = facturaReenvio.getRazonSocial();
        String mailTo = emailReenvio;
        String empresa = session.getNombreEmpresa();
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su Documento Tributario en formato PDF";
        String subjet = "Factura No. " + facturaReenvio.getNroFactura();
        String url = "factura/consulta/" + facturaReenvio.getId();
        String fileName = facturaReenvio.getNroFactura() + ".pdf";
       
        byte[] data = null;
        if (siediApi) {
            Map parametros = new HashMap();
            parametros.put("ticket", false);
            parametros.put("siediApi", true);
            data = fileServiceClient.downloadFactura(url, parametros);
            reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, facturaReenvio.getNroFactura(), host, port, empresa, emailEmpresa, telefonoEmpresa);
        } else {
            data = fileServiceClient.download(url);
            if (facturaReenvio.getDigital().equalsIgnoreCase("S")) {
                if (notificar.equalsIgnoreCase("S")){
                    reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, facturaReenvio.getNroFactura(), host, port, empresa, emailEmpresa, telefonoEmpresa);
                }
                reenviarFactElectronica();
            } else if (facturaReenvio.getDigital().equalsIgnoreCase("N") && !mailSender.equalsIgnoreCase("N") && !passSender.equalsIgnoreCase("N")) {
                reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, facturaReenvio.getNroFactura(), host, port, empresa, emailEmpresa, telefonoEmpresa);
            } else {
                reenviarViaNotificacionSepsa(mensaje, subjet, mailTo, data, fileName);
            }
        }
    }

    public void reenviarFactElectronica() {
        SepsaSiediMonitorService serviceSiediMonitor = new SepsaSiediMonitorService();
        BodyResponse<Factura> respuestaFact = new BodyResponse<>();
        respuestaFact = serviceSiediMonitor.reenviarNotificacion(facturaReenvio.getCdc());

        if (respuestaFact.getSuccess()) {
            LocalDate fecha = LocalDate.now();
            Date fechaHoy = Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
            facturaReenvio.setFechaEntrega(fechaHoy);
            facturaReenvio.setEntregado("C");
            serviceFactura.editFactura(facturaReenvio);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
        }

    }

    /**
     * Método para reenviar documento por correo
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     * @param from
     * @param passFrom
     * @param cliente
     * @param nroDoc
     */
    public void reenviarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            Boolean notificado = MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host, port);
            if (notificado) {
                LocalDate fecha = LocalDate.now();
                // Convertir LocalDate a Date
                Date fechaHoy = Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
                facturaReenvio.setFechaEntrega(fechaHoy);
                facturaReenvio.setEntregado("C");
                serviceFactura.editFactura(facturaReenvio);
                
            } else {
                // actualizar campo "entregado" a P (Pendiente)
                facturaReenvio.setEntregado("P");
                serviceFactura.editFactura(facturaReenvio);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
    public void reenvioMasivoNotificaciones(){
        WebLogger.get().debug(String.format("Cantidad de seleccionados: %d",selectedFacturas.size()));
        List<Integer> idFacturas = new ArrayList();
        for (FacturaPojo factura : selectedFacturas){
            if (!factura.getEntregado().equalsIgnoreCase("N")) {
                idFacturas.add(factura.getId());
            }
        }
        if (!idFacturas.isEmpty()){
            serviceFactura.reenviarMasivo(idFacturas);
            filterFacturas();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Las facturas seleccionadas serán reenviadas."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar al menos una factura para reenviar"));
        }
    }

    /**
     * Método para reenviar documento via sepsa notificación
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     */
    public void reenviarViaNotificacionSepsa(String msn, String subject, String to, byte[] file, String fileName) {
        Notificacion notificacion = new Notificacion();
        TipoNotificaciones tipoNotificacion = new TipoNotificaciones();
        ArchivoAdjunto archivo = new ArchivoAdjunto();
        NotificacionService servicioNoti = new NotificacionService();
        List<ArchivoAdjunto> notificationAttachments = new ArrayList<>();

        BodyResponse<Notificacion> respuestaNoti = new BodyResponse<>();

        try {
            tipoNotificacion.setCode("MAIL_ALERTAS");

            archivo.setContentType("application/pdf");
            archivo.setFileName(fileName);
            archivo.setData(file);
            notificationAttachments.add(archivo);

            notificacion.setSubject(subject);
            notificacion.setMessage(msn);
            notificacion.setTo(to);
            notificacion.setState("P");
            notificacion.setObservation("--");
            notificacion.setNotificationType(tipoNotificacion);
            notificacion.setNotificationAttachments(notificationAttachments);

            respuestaNoti = servicioNoti.createNotificacion(notificacion);

            if (respuestaNoti.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la factura al dirección confirmada"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }

    public void cancelarAnulacion(FacturaPojo factura) {
        Factura fact = new Factura();
        fact.setId(factura.getId());
        fact.setEntregado(factura.getEntregado());
        fact.setImpreso(factura.getImpreso());
        fact.setArchivoEdi(factura.getArchivoEdi());
        fact.setArchivoSet(factura.getArchivoSet());
        fact.setGeneradoEdi(factura.getGeneradoEdi());
        fact.setGeneradoSet(factura.getGeneradoSet());
        fact.setDigital(factura.getDigital());
        fact.setIgnorarPeriodoAnulacion(this.ignorarPeriodoAnulacion);
        
        BodyResponse<Factura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.cancelarAnulacion(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura regenerada correctamente!"));
            facturaFilter = new FacturaPojo();
            asignarFiltrosFecha();
            filterFacturas();

        }
    }
    
    /**
     *
     * @param factura
     */
    public void regenerarFactura(FacturaPojo factura) {
        Factura fact = new Factura();
        fact.setId(factura.getId());
        fact.setEntregado(factura.getEntregado());
        fact.setImpreso(factura.getImpreso());
        fact.setArchivoEdi(factura.getArchivoEdi());
        fact.setArchivoSet(factura.getArchivoSet());
        fact.setGeneradoEdi(factura.getGeneradoEdi());
        fact.setObservacion(factura.getObservacion());
        fact.setGeneradoSet("N");
        BodyResponse<Factura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.regenerarFactura(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura regenerada!"));
            facturaFilter = new FacturaPojo();
            asignarFiltrosFecha();
            filterFacturas();
            PF.current().ajax().update(":list-factura-form:form-data:data-list-facturas ");
            PF.current().ajax().update(":message-form:message");

        }
    }

    /**
     * Método para obtener lista de retención
     *
     * @param idFactura
     */
    public void filterRetencion(Integer idFactura) {
        this.retencion.setIdFactura(idFactura);
        getRetencionAsociadosAdapter().setFirstResult(0);
        retencionAsociadosAdapter = retencionAsociadosAdapter.fillData(retencion);
    }

    /**
     * Método para obtener nota de credito
     *
     * @param idFactura
     */
    public void filterNotaCredito(Integer idFactura) {
        this.notaCredito.setIdFactura(idFactura);
        getNotaCreditoAsociadosAdapter().setFirstResult(0);
        notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter.fillData(notaCredito);
    }
    
    public void filterCobro(Integer idFactura) {
        this.cobroDetalle.setIdFactura(idFactura);
        this.cobroDetalle.setListadoPojo(true);
        getCobroDetalleAdapter().setFirstResult(0);

        cobroDetalleAdapter = cobroDetalleAdapter.fillData(cobroDetalle);
    }
    
    /**
     * Metodo para obtener los doc asociados
     *
     * @param idFactura
     */
    public void verDocAsociados(Integer idFactura) {
        filterRetencion(idFactura);
        filterNotaCredito(idFactura);
        filterCobro(idFactura);
    }

    /**
     * Metodo para obtener los doc asociados
     *
     * @param idProcesamiento
     */
    public void verProcesamiento(Integer idProcesamiento) {
        WebLogger.get().debug("PROCESAMIENTO" + idProcesamiento);
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new FacturaProcesamientoAdapter();
        if (idProcesamiento != null) {
            this.procesamientoFilter.setIdProcesamiento(idProcesamiento);
            this.adapterProcesamiento = adapterProcesamiento.fillData(procesamientoFilter);
            this.procesamientoFilter = this.adapterProcesamiento.getData().get(0);
        }
    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionPeriodoAnulacionFactura() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("IGNORAR_PERIODO_ANULACION_FACTURA");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ignorarPeriodoAnulacion = false;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.ignorarPeriodoAnulacion = true;
        } else {
            this.ignorarPeriodoAnulacion = false;
        }

    }

    /**
     * Método para obtener fechas desde y hasta para filtro
     */
    public void asignarFiltrosFecha() {
        Calendar today = Calendar.getInstance();
        facturaFilter.setFechaHasta(today.getTime());

        today.add(Calendar.DATE, -10);
        facturaFilter.setFechaDesde(today.getTime());

    }
    
        /**
     * Método para obtener la configuración de Notificación por email
     */
    public void obtenerConfiguracionNotificacion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("NOTIFICACION_FACTURA_EMAIL");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.notificar = "N";
            } else {
                this.notificar = adapterConfigValor.getData().get(0).getValor();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
    
    /**
     * Método para obtener la configuración de Cantidad maxima de dias para los filtros.
     */
    public void obtenerConfiguracionDiasFiltro() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("CANTIDAD_MAXIMA_DIAS_FILTRO");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.diasFiltro = null;
            } else {
                this.diasFiltro = Integer.parseInt(adapterConfigValor.getData().get(0).getValor());
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
    
    /**
     * Método para determinar si el cliente utiliza conexión SiediAPI
     */
    public void obtenerConfiguracionSiediApi() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("ARCHIVOS_DTE_SIEDI_API");
        cvf.setIdEmpresa(session.getUser().getIdEmpresa());
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.siediApi = false;
        } else if (adaptercv.getData().get(0).getValor().equalsIgnoreCase("S")) {
            this.siediApi = true;
        }
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.serviceFactura = new FacturacionService();
        this.adapterFactura = new FacturaPojoAdapter();
        this.facturaFilter = new FacturaPojo();
        asignarFiltrosFecha();
        filterFacturas();
        this.moneda = new Moneda();
        this.adapterMoneda = new MonedaAdapter();
        filterMoneda();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();
        this.retencion = new Retencion();
        this.retencionAsociadosAdapter = new RetencionAdapter();
        this.notaCredito = new NotaCredito();
        this.notaCreditoAsociadosAdapter = new NotaCreditoAdapter();
        this.cobroDetalle = new CobroDetallePojo();
        this.cobroDetalleAdapter = new CobroDetallePojoListAdapter();       
        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("FACTURA");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
        this.emailReenvio = null;
        this.fileServiceClient = new FileServiceClient();
        this.facturaReenvio = new Factura();
        this.ignorarPeriodoAnulacion = false;
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new FacturaProcesamientoAdapter();
        obtenerConfiguracionPeriodoAnulacionFactura();
        this.addEmail = false;
        this.selectedFacturas = new ArrayList();    
        obtenerConfiguracionNotificacion();
        obtenerConfiguracionSiediApi();
        obtenerConfiguracionDiasFiltro();
    }
}
