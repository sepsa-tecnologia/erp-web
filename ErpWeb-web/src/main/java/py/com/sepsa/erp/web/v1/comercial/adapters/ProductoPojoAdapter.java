
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProductoPojo;
import py.com.sepsa.erp.web.v1.comercial.remote.ProductoService;

/**
 * Adaptador para la lista de producto pojo
 * @author Romina Núñez
 */
public class ProductoPojoAdapter extends DataListAdapter<ProductoPojo>{
    
    /**
     * Cliente para el servicio producto
     */
    private final ProductoService productoService;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoPojoAdapter fillData(ProductoPojo searchData) {
     
        return productoService.getProductoPojoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ProductoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoService();
    }

    /**
     * Constructor de ProductoAdapter
     */
    public ProductoPojoAdapter() {
        super();
        this.productoService= new ProductoService();
    }
}
