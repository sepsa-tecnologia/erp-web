/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;
import java.util.List;

/**
 * Pojo para Control Inventario Detalle
 *
 * @author Romina Nuñez
 */
public class ControlInventarioDetalle {

    /**
     * Id
     */
    private Integer id;
    /**
     * Identificador de control inventario
     */
    private Integer idControlInventario;
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    /**
     * Código de depósito
     */
    private String codigoDeposito;
    /**
     * Id Local
     */
    private Integer idLocal;
    /**
     * Local
     */
    private String local;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
    /**
     * Codigo Gtin
     */
    private String codigoGtin;
    /**
     * Codigo Interno
     */
    private String codigoInterno;
    /**
     * Cantidad Inicial
     */
    private Integer cantidadInicial;
    /**
     * Cantidad Final
     */
    private Integer cantidadFinal;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * Fecha vto
     */
    private Date fechaVencimiento;
    /**
     * Observacion
     */
    private String observacion;


    public Integer getId() {
        return id;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getCodigoDeposito() {
        return codigoDeposito;
    }

    public void setCodigoDeposito(String codigoDeposito) {
        this.codigoDeposito = codigoDeposito;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    /**
     * Constructor de la clase
     */
    public ControlInventarioDetalle() {

    }

    public ControlInventarioDetalle(Integer id) {
        this.id = id;
    }

}
