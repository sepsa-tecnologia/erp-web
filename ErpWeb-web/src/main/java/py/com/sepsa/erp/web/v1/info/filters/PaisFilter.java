package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Pais;

/**
 * Filtro utilizado para el servicio de pais
 *
 * @author Cristina Insfrán
 */
public class PaisFilter extends Filter {

    /**
     * Agrega el filtro de identificador del pais
     *
     * @param id Identificador del pais
     * @return
     */
    public PaisFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para descripcion
     *
     * @param descripcion descripcion de configuracion
     * @return
     */
    public PaisFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro para codigo
     *
     * @param codigo codigo de configuración
     * @return
     */
    public PaisFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param conf datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Pais pais, Integer page, Integer pageSize) {
        PaisFilter filter = new PaisFilter();

        filter
                .id(pais.getId())
                .descripcion(pais.getDescripcion())
                .codigo(pais.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PaisFilter
     */
    public PaisFilter() {
        super();
    }
}
