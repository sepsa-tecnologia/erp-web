/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para deposito
 *
 * @author Romina Núñez
 */
public class DepositoLogisticoFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public DepositoLogisticoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de local
     *
     * @param idLocal
     * @return
     */
    public DepositoLogisticoFilter idLocal(Integer idLocal) {
        if (idLocal != null) {
            params.put("idLocal", idLocal);
        }
        return this;
    }
    
        /**
     * Agrega el filtro por identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public DepositoLogisticoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public DepositoLogisticoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param activo
     * @return
     */
    public DepositoLogisticoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro descripcion
     *
     * @param descripcion
     * @return
     */
    public DepositoLogisticoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public DepositoLogisticoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public DepositoLogisticoFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);

                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionHasta
     * @return
     */
    public DepositoLogisticoFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);

                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param deposito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(DepositoLogistico deposito, Integer page, Integer pageSize) {
        DepositoLogisticoFilter filter = new DepositoLogisticoFilter();

        filter
                .id(deposito.getId())
                .idEmpresa(deposito.getIdEmpresa())
                .codigo(deposito.getCodigo())
                .descripcion(deposito.getDescripcion())
                .listadoPojo(deposito.getListadoPojo())
                .activo(deposito.getActivo())
                .idLocal(deposito.getIdLocal())
                .fechaInsercionDesde(deposito.getFechaInsercionDesde())
                .fechaInsercionHasta(deposito.getFechaInsercionHasta())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public DepositoLogisticoFilter() {
        super();
    }

}
