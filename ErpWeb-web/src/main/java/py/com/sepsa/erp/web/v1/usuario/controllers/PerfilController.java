package py.com.sepsa.erp.web.v1.usuario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.usuario.adapters.PerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioPerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioPerfilRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfil;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioPerfilServiceClient;

/**
 * Controlador para perfil
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("perfil")
public class PerfilController implements Serializable {

    /**
     * Adaptador de la lista de perfiles
     */
    private PerfilListAdapter adapterPerfil;
    /**
     * Adaptador de la lista de usuario perfil
     */
    private UsuarioPerfilListAdapter adapterUsuarioPerfil;
    /**
     * Objeto de Sesión
     */
    @Inject
    private SessionData session;
    /**
     * Entidad perfil
     */
    private Perfil perfil;
    /**
     * Lista de perfiles seleccionados
     */
    private List<UsuarioPerfilRelacionado> perfilSelect = new ArrayList<>();
    /**
     * Objeto usuario perfil
     */
    private UsuarioPerfil usuarioPerfil;
    /**
     * Identificador del usuario
     */
    private Integer idUsuario;
    /**
     * Servicio para el cliente usuarioPerfil
     */
    private UsuarioPerfilServiceClient usuarioPerfilClient;
    /**
     * Adaptador para la lista de Usuario Perfil Relacionado
     */
    private UsuarioPerfilRelacionadoAdapter adapterUsuarioPerfilRelacionado;
    /**
     * POJO Usuario Perfil Relacionado
     */
    private UsuarioPerfilRelacionado usuarioPerfilRelacionadoFilter;
    private Integer idUser = null;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * @return the usuarioPerfilClient
     */
    public UsuarioPerfilServiceClient getUsuarioPerfilClient() {
        return usuarioPerfilClient;
    }

    /**
     * @param usuarioPerfilClient the usuarioPerfilClient to set
     */
    public void setUsuarioPerfilClient(UsuarioPerfilServiceClient usuarioPerfilClient) {
        this.usuarioPerfilClient = usuarioPerfilClient;
    }

    /**
     * @return the idUsuario
     */
    public Integer getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the usuarioPerfil
     */
    public UsuarioPerfil getUsuarioPerfil() {
        return usuarioPerfil;
    }

    /**
     * @param usuarioPerfil the usuarioPerfil to set
     */
    public void setUsuarioPerfil(UsuarioPerfil usuarioPerfil) {
        this.usuarioPerfil = usuarioPerfil;
    }

    public void setUsuarioPerfilRelacionadoFilter(UsuarioPerfilRelacionado usuarioPerfilRelacionadoFilter) {
        this.usuarioPerfilRelacionadoFilter = usuarioPerfilRelacionadoFilter;
    }

    public UsuarioPerfilRelacionado getUsuarioPerfilRelacionadoFilter() {
        return usuarioPerfilRelacionadoFilter;
    }

    public void setAdapterUsuarioPerfilRelacionado(UsuarioPerfilRelacionadoAdapter adapterUsuarioPerfilRelacionado) {
        this.adapterUsuarioPerfilRelacionado = adapterUsuarioPerfilRelacionado;
    }

    public UsuarioPerfilRelacionadoAdapter getAdapterUsuarioPerfilRelacionado() {
        return adapterUsuarioPerfilRelacionado;
    }

    /**
     * @return the adapterPerfil
     */
    public PerfilListAdapter getAdapterPerfil() {
        return adapterPerfil;
    }

    /**
     * @param adapterPerfil the adapterPerfil to set
     */
    public void setAdapterPerfil(PerfilListAdapter adapterPerfil) {
        this.adapterPerfil = adapterPerfil;
    }

    public void setPerfilSelect(List<UsuarioPerfilRelacionado> perfilSelect) {
        this.perfilSelect = perfilSelect;
    }

    public List<UsuarioPerfilRelacionado> getPerfilSelect() {
        return perfilSelect;
    }

    public void setAdapterUsuarioPerfil(UsuarioPerfilListAdapter adapterUsuarioPerfil) {
        this.adapterUsuarioPerfil = adapterUsuarioPerfil;
    }

    public UsuarioPerfilListAdapter getAdapterUsuarioPerfil() {
        return adapterUsuarioPerfil;
    }

    /**
     * @return the perfil
     */
    public Perfil getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
//</editor-fold>

    /**
     * Metodo para listar los locales del usuario
     *
     * @param usuario
     */
    public void getlistPerfil(Usuario usuario) {
        idUser = usuario.getId();

        usuarioPerfilRelacionadoFilter = new UsuarioPerfilRelacionado();
        usuarioPerfilRelacionadoFilter.setIdUsuario(idUser);
       
        adapterUsuarioPerfilRelacionado = adapterUsuarioPerfilRelacionado.fillData(usuarioPerfilRelacionadoFilter);

    }

    /**
     * Método para encontrar el perfil asociado
     *
     * @param usuarioPerfilRelacionado
     * @return
     */
    public String findPerfil(UsuarioPerfilRelacionado usuarioPerfilRelacionado) {
        UsuarioPerfilRelacionadoAdapter adapterUsuarioPerfilRelacionadoEditar = new UsuarioPerfilRelacionadoAdapter();
        UsuarioPerfilRelacionado usuarioPerfilRelacionadoEditar = new UsuarioPerfilRelacionado();

        usuarioPerfilRelacionadoEditar.setIdUsuario(idUser);
        usuarioPerfilRelacionadoEditar.setIdPerfil(usuarioPerfilRelacionado.getIdPerfil());
        adapterUsuarioPerfilRelacionadoEditar = adapterUsuarioPerfilRelacionadoEditar.fillData(usuarioPerfilRelacionadoEditar);

        return adapterUsuarioPerfilRelacionadoEditar.getData().get(0).getEstado().getDescripcion();
    }

    /**
     * Método invocado al seleccionar usuario perfil
     *
     * @param usuarioPerfilRelacionado
     */
    public void activarPerfil(UsuarioPerfilRelacionado usuarioPerfilRelacionado) {
        Usuario userFilter = new Usuario();
        userFilter.setId(idUser);
        usuarioPerfil = new UsuarioPerfil();
        usuarioPerfil.setIdUsuario(idUser);
        usuarioPerfil.setIdPerfil(usuarioPerfilRelacionado.getIdPerfil());
        usuarioPerfil.setCodigoEstado("ACTIVO");
        UsuarioPerfil up = usuarioPerfilClient.setUserPerfil(usuarioPerfil);

        if (up != null) {

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Rol perfil actualizado correctamente!"));
            getlistPerfil(userFilter);
        }

    }

    /**
     * Método invocado para deseleccionar usuario perfil
     *
     * @param usuarioPerfilRelacionado
     */
    public void inactivarPerfil(UsuarioPerfilRelacionado usuarioPerfilRelacionado) {
        Usuario userFilter = new Usuario();
        userFilter.setId(idUser);
        usuarioPerfil = new UsuarioPerfil();
        usuarioPerfil.setIdUsuario(idUser);
        usuarioPerfil.setIdPerfil(usuarioPerfilRelacionado.getIdPerfil());
        usuarioPerfil.setCodigoEstado("INACTIVO");
        UsuarioPerfil up = usuarioPerfilClient.setUserPerfil(usuarioPerfil);

        if (up != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Rol perfil actualizado correctamente!"));
            getlistPerfil(userFilter);
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.setPerfil(new Perfil());
        this.adapterPerfil = new PerfilListAdapter();
        this.idUsuario = null;
        this.adapterUsuarioPerfil = new UsuarioPerfilListAdapter();
        this.usuarioPerfilClient = new UsuarioPerfilServiceClient();

        this.adapterUsuarioPerfilRelacionado = new UsuarioPerfilRelacionadoAdapter();
    }
}
