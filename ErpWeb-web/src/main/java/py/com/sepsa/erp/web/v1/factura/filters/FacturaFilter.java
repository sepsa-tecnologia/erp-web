package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class FacturaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param id
     * @return
     */
    public FacturaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de idPersona
     *
     * @param idPersona
     * @return
     */
    public FacturaFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public FacturaFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }
    
    public FacturaFilter idTalonario(Integer idTalonario) {
        if (idTalonario != null) {
            params.put("idTalonario", idTalonario);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public FacturaFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public FacturaFilter moneda(String moneda) {
        if (moneda != null && !moneda.trim().isEmpty()) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de tipo factura
     *
     * @param idTipoFactura
     * @return
     */
    public FacturaFilter idTipoFactura(Integer idTipoFactura) {
        if (idTipoFactura != null) {
            params.put("idTipoFactura", idTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de tipo factura
     *
     * @param tipoFactura
     * @return
     */
    public FacturaFilter tipoFactura(String tipoFactura) {
        if (tipoFactura != null && !tipoFactura.trim().isEmpty()) {
            params.put("tipoFactura", tipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro factura
     *
     * @param nroFactura
     * @return
     */
    public FacturaFilter nroFactura(String nroFactura) {
        if (nroFactura != null && !nroFactura.trim().isEmpty()) {
            params.put("nroFactura", nroFactura);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de nro timbrado
     *
     * @param nroFactura
     * @return
     */
    public FacturaFilter nroTimbrado(String nroTimbrado) {
        if (nroTimbrado != null && !nroTimbrado.trim().isEmpty()) {
            params.put("nroTimbrado", nroTimbrado);
        }
        return this;
    }

    /**
     * Agregar el filtro de nroOrdenCompra
     *
     * @param nroOrdenCompra
     * @return
     */
    public FacturaFilter nroOrdenCompra(String nroOrdenCompra) {
        if (nroOrdenCompra != null && !nroOrdenCompra.trim().isEmpty()) {
            params.put("nroOrdenCompra", nroOrdenCompra);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public FacturaFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha vencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public FacturaFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de razón social
     *
     * @param razonSocial
     * @return
     */
    public FacturaFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agregar el filtro de direccion
     *
     * @param direccion
     * @return
     */
    public FacturaFilter direccion(String direccion) {
        if (direccion != null && !direccion.trim().isEmpty()) {
            params.put("direccion", direccion);
        }
        return this;
    }

    /**
     * Agregar el filtro de RUC
     *
     * @param ruc
     * @return
     */
    public FacturaFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public FacturaFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public FacturaFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public FacturaFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de cobrado
     *
     * @param cobrado
     * @return
     */
    public FacturaFilter cobrado(String cobrado) {
        if (cobrado != null && !cobrado.trim().isEmpty()) {
            params.put("cobrado", cobrado);
        }
        return this;
    }

    /**
     * Agregar el filtro de impreso
     *
     * @param impreso
     * @return
     */
    public FacturaFilter impreso(String impreso) {
        if (impreso != null && !impreso.trim().isEmpty()) {
            params.put("impreso", impreso);
        }
        return this;
    }

    /**
     * Agregar el filtro de entregado
     *
     * @param entregado
     * @return
     */
    public FacturaFilter entregado(String entregado) {
        if (entregado != null && !entregado.trim().isEmpty()) {
            params.put("entregado", entregado);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public FacturaFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de días crédito
     *
     * @param diasCredito
     * @return
     */
    public FacturaFilter diasCredito(Integer diasCredito) {
        if (diasCredito != null) {
            params.put("diasCredito", diasCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 5
     *
     * @param montoIva5
     * @return
     */
    public FacturaFilter montoIva5(BigDecimal montoIva5) {
        if (montoIva5 != null) {
            params.put("montoIva5", montoIva5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 5
     *
     * @param montoImponible5
     * @return
     */
    public FacturaFilter montoImponible5(BigDecimal montoImponible5) {
        if (montoImponible5 != null) {
            params.put("montoImponible5", montoImponible5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 5
     *
     * @param montoTotal5
     * @return
     */
    public FacturaFilter montoTotal5(BigDecimal montoTotal5) {
        if (montoTotal5 != null) {
            params.put("montoTotal5", montoTotal5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 10
     *
     * @param montoIva10
     * @return
     */
    public FacturaFilter montoIva10(BigDecimal montoIva10) {
        if (montoIva10 != null) {
            params.put("montoIva10", montoIva10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 10
     *
     * @param montoImponible10
     * @return
     */
    public FacturaFilter montoImponible10(BigDecimal montoImponible10) {
        if (montoImponible10 != null) {
            params.put("montoImponible10", montoImponible10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 10
     *
     * @param montoTotal10
     * @return
     */
    public FacturaFilter montoTotal10(BigDecimal montoTotal10) {
        if (montoTotal10 != null) {
            params.put("montoTotal10", montoTotal10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total excento
     *
     * @param montoTotalExento
     * @return
     */
    public FacturaFilter montoTotalExento(BigDecimal montoTotalExento) {
        if (montoTotalExento != null) {
            params.put("montoTotalExento", montoTotalExento);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva total
     *
     * @param montoIvaTotal
     * @return
     */
    public FacturaFilter montoIvaTotal(BigDecimal montoIvaTotal) {
        if (montoIvaTotal != null) {
            params.put("montoIvaTotal", montoIvaTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible total
     *
     * @param montoImponibleTotal
     * @return
     */
    public FacturaFilter montoImponibleTotal(BigDecimal montoImponibleTotal) {
        if (montoImponibleTotal != null) {
            params.put("montoImponibleTotal", montoImponibleTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total factura
     *
     * @param montoTotalFactura
     * @return
     */
    public FacturaFilter montoTotalFactura(BigDecimal montoTotalFactura) {
        if (montoTotalFactura != null) {
            params.put("montoTotalFactura", montoTotalFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public FacturaFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nota de credito
     *
     * @param montoNotaCredito
     * @return
     */
    public FacturaFilter montoNotaCredito(BigDecimal montoNotaCredito) {
        if (montoNotaCredito != null) {
            params.put("montoNotaCredito", montoNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de montoCobro
     *
     * @param montoCobro
     * @return
     */
    public FacturaFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de retencion
     *
     * @param montoRetencion
     * @return
     */
    public FacturaFilter montoRetencion(BigDecimal montoRetencion) {
        if (montoRetencion != null) {
            params.put("montoRetencion", montoRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro de saldo
     *
     * @param saldo
     * @return
     */
    public FacturaFilter saldo(BigDecimal saldo) {
        if (saldo != null) {
            params.put("saldo", saldo);
        }
        return this;
    }

    /**
     * Agrega el filtro de detalles
     *
     * @param detalles
     * @return
     */
    public FacturaFilter detalles(List<FacturaDetalle> detalles) {
        if (detalles != null && !detalles.isEmpty()) {
            params.put("detalles", detalles);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene retencion, true o false
     *
     * @param tieneRetencion
     * @return
     */
    public FacturaFilter tieneRetencion(String tieneRetencion) {
        if (tieneRetencion != null && !tieneRetencion.trim().isEmpty()) {
            params.put("tieneRetencion", tieneRetencion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro tiene saldo
     *
     * @param tieneSaldo
     * @return
     */
    public FacturaFilter tieneSaldo(String tieneSaldo) {
        if (tieneSaldo != null && !tieneSaldo.trim().isEmpty()) {
            params.put("tieneSaldo", tieneSaldo.trim());
        }
        return this;
    }

    /**
     * Agregar el filtro de anhoMes
     *
     * @param anhoMes
     * @return
     */
    public FacturaFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agregar el filtro de pagado
     *
     * @param pagado
     * @return
     */
    public FacturaFilter pagado(String pagado) {
        if (pagado != null && !pagado.trim().isEmpty()) {
            params.put("pagado", pagado);
        }
        return this;
    }

    /**
     * Agregar el filtro codigoTipoFactura
     *
     * @param codigoTipoFactura
     * @return
     */
    public FacturaFilter codigoTipoFactura(String codigoTipoFactura) {
        if (codigoTipoFactura != null && !codigoTipoFactura.trim().isEmpty()) {
            params.put("codigoTipoFactura", codigoTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param producto
     * @return
     */
    public FacturaFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param tieneOc
     * @return
     */
    public FacturaFilter tieneOrdenCompra(String tieneOrdenCompra) {
        if (tieneOrdenCompra != null && !tieneOrdenCompra.trim().isEmpty()) {
            params.put("tieneOrdenCompra", tieneOrdenCompra);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de cdc
     * @param cdc
     * @return
     */
    public FacturaFilter cdc(String cdc) {
        if (cdc != null && !cdc.trim().isEmpty()) {
            params.put("cdc", cdc);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param factura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Factura factura, Integer page, Integer pageSize) {
        FacturaFilter filter = new FacturaFilter();

        filter
                .id(factura.getId())
                .idPersona(factura.getIdPersona())
                .idCliente(factura.getIdCliente())
                .idTalonario(factura.getIdTalonario())
                .nroFactura(factura.getNroFactura())
                .nroOrdenCompra(factura.getNroOrdenCompra())
                .ruc(factura.getRuc())
                .tieneRetencion(factura.getTieneRetencion())
                .tieneSaldo(factura.getTieneSaldo())
                .razonSocial(factura.getRazonSocial())
                .cobrado(factura.getCobrado())
                .codigoTipoFactura(factura.getCodigoTipoFactura())
                .anulado(factura.getAnulado())
                .idMoneda(factura.getIdMoneda())
                .tieneOrdenCompra(factura.getTieneOrdenCompra())
                .fechaDesde(factura.getFechaDesde())
                .fechaHasta(factura.getFechaHasta())
                .cdc(factura.getCdc())
                .nroTimbrado(factura.getNroTimbrado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public FacturaFilter() {
        super();
    }
}
