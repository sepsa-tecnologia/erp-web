/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;

/**
 *
 * @author Antonella Lucero
 */
public class DatosNotaDebito {
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * N° Nota Credito
     */
    private String nroNotaDebito;
    /**
     * Timbrado
     */
    private String timbrado;
    /**
     * Fecha Vto Timbrado
     */
    private Date fechaVencimientoTimbrado;
        /**
     * telefono
     */
    private String telefono;
    /**
     * direccion
     */
    private String direccion;
    /**
     * email
     */
    private String email;
    /**
     * Nro de casa
     */
    private String nroCasa;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
        /**
     * Punto de expedicion
     */
    private String puntoExpedicion;
    /**
     * Identificador de departamento 
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Establecimiento
     */
    private String establecimiento;
    /**
     * Fecha 
     */
    private Date fecha;

    public Integer getIdTalonario() {
        return idTalonario;
    }
    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setNroNotaDebito(String nroNotaDebito) {
        this.nroNotaDebito = nroNotaDebito;
    }

    public String getNroNotaDebito() {
        return nroNotaDebito;
    }
    
    
    /**
     * Constructor de la clase
     */
    public DatosNotaDebito() {
    }

    /**
     * Constructor con parametros
     * @param idTalonario
     * @param nroNotaDebito
     * @param timbrado
     * @param fechaVencimientoTimbrado
     * @param fecha 
     */
    public DatosNotaDebito(Integer idTalonario, String nroNotaDebito, String timbrado, Date fechaVencimientoTimbrado, Date fecha) {
        this.idTalonario = idTalonario;
        this.nroNotaDebito = nroNotaDebito;
        this.timbrado = timbrado;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fecha = fecha;
    }
}
