package py.com.sepsa.erp.web.v1.remote;

/**
 * APIErpCore para conexión a servicios
 *
 * @author Jonathan D. Bernal Fernández, Romina Núñez
 */
public class APIErpCore extends API {

    //Parámetros de conexións
    private final static String BASE_API = "erp-core-api/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;
    
    public APIErpCore() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);
        
        InfoPojo info = InfoPojo.createInstance("api-erp-core");
        
        updateConnInfo(info);
    }

    public APIErpCore(String token) {
        super(null, null, 0, BASE_API, CONN_TIMEOUT, token);
        
        InfoPojo info = InfoPojo.createInstance("api-erp-core");
        
        updateConnInfo(info);
    }
}
