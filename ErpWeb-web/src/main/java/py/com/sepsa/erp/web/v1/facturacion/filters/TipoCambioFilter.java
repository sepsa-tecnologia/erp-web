/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para tipo cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoCambioFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoCambioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercionDesde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public TipoCambioFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionDesde);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercionHasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public TipoCambioFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercion
     *
     * @param fechaInsercion
     * @return
     */
    public TipoCambioFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro de idMoneda
     *
     * @param idMoneda
     * @return
     */
    public TipoCambioFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de compra
     *
     * @param compra
     * @return
     */
    public TipoCambioFilter compra(BigDecimal compra) {
        if (compra != null) {
            params.put("compra", compra);
        }
        return this;
    }

    /**
     * Agrega el filtro de venta
     *
     * @param venta
     * @return
     */
    public TipoCambioFilter venta(BigDecimal venta) {
        if (venta != null) {
            params.put("venta", venta);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public TipoCambioFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public TipoCambioFilter moneda(Moneda moneda) {
        if (moneda != null) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoCambio datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoCambio tipoCambio, Integer page, Integer pageSize) {
        TipoCambioFilter filter = new TipoCambioFilter();

        filter
                .id(tipoCambio.getId())
                .fechaInsercion(tipoCambio.getFechaInsercion())
                .fechaInsercionDesde(tipoCambio.getFechaInsercionDesde())
                .fechaInsercionHasta(tipoCambio.getFechaInsercionHasta())
                .idMoneda(tipoCambio.getIdMoneda())
                .compra(tipoCambio.getCompra())
                .venta(tipoCambio.getVenta())
                .activo(tipoCambio.getActivo())
                .moneda(tipoCambio.getMoneda())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoCambioFilter() {
        super();
    }

}
