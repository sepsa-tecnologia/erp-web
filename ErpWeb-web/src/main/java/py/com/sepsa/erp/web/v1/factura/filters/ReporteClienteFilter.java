
package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteClienteMora;

/**
 * Filtro para el reporte de cliente
 * @author Cristina Insfrán
 */
public class ReporteClienteFilter extends Filter {
   
     /**
     * Agrega el filtro de identificador de reporte
     *
     * @param id
     * @return
     */
    public ReporteClienteFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
      /**
     * Agrega el filtro descripción
     *
     * @param descripcion
     * @return
     */
    public ReporteClienteFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
       /**
     * Agrega el filtro de código
     *
     * @param codigo
     * @return
     */
    public ReporteClienteFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }
    
       /**
     * Agrega el filtro de estado
     *
     * @param estado
     * @return
     */
    public ReporteClienteFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de charToString
     *
     * @param charToString
     * @return
     */
    public ReporteClienteFilter charToString(Boolean charToString) {
        if (charToString != null) {
            params.put("charToString", charToString);
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public ReporteClienteFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
           
              try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               
                String date = simpleDateFormat.format(fechaDesde);
              
               params.put("fechaDesde", date);
            } catch (Exception ex) {
                
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaHasta
     * @return
     */
    public ReporteClienteFilter fechaHasta(Date fechaHasta) {
         if (fechaHasta != null) {
           
              try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               
                String date = simpleDateFormat.format(fechaHasta);
              
               params.put("fechaHasta", date);
            } catch (Exception ex) {
                
            }
        }
        return this;
    }
    
      /**
     * Agrega el filtro de identificador idProducto
     *
     * @param idProducto
     * @return
     */
    public ReporteClienteFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }
    
      /**
     * Agrega el filtro de identificador idProducto
     *
     * @param tipoReporte
     * @return
     */
    public ReporteClienteFilter tipoReporte(String tipoReporte) {
        if (tipoReporte != null) {
            params.put("tipoReporte", tipoReporte);
        }
        return this;
    }
    
       /**
     * Agrega el filtro de resumen
     *
     * @param resumen
     * @return
     */
    public ReporteClienteFilter resumen(Boolean resumen) {
        if (resumen != null) {
            params.put("resumen", resumen);
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     *
     * @param reporte
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ReporteClienteMora reporte, Integer page, Integer pageSize) {
        ReporteClienteFilter filter = new ReporteClienteFilter();

        filter
                .id(reporte.getId())
                .descripcion(reporte.getDescripcion())
                .codigo(reporte.getCodigo())
                .estado(reporte.getEstado())
                .charToString(reporte.getCharToString())
                .fechaDesde(reporte.getFechaDesde())
                .fechaHasta(reporte.getFechaHasta())
                .idProducto(reporte.getIdProducto())
                .tipoReporte(reporte.getTipoReporte())
                .resumen(reporte.getResumen())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ReporteClienteFilter
     */
    public ReporteClienteFilter() {
        super();
    }
}
