/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para Tipo de producto
 *
 * @author Williams Vera
 */
public class TipoProducto {

    /**
     * Identificador de Tipo Producto
     */
    private Integer id;
    /**
     * Descripción de Tipo Producto
     */
    private String descripcion;
    /**
     * Código de Tipo Producto
     */
    private String codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public TipoProducto() {
    }

}
