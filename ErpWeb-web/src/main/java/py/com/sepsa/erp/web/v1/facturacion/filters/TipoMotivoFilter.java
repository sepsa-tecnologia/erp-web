package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para listado de tipoMotivo
 *
 * @author Alexander Triana
 */
public class TipoMotivoFilter extends Filter {

    /**
     * Agrega el filtro por identificador de tipoMotivo
     *
     * @param id
     * @return
     */
    public TipoMotivoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigo de tipoMotivo
     *
     * @param codigo
     * @return
     */
    public TipoMotivoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por descripción de tipoMotivo
     *
     * @param descripcion
     * @return
     */
    public TipoMotivoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param tipoMotivo
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(TipoMotivo tipoMotivo, Integer page,
            Integer pageSize) {

        TipoMotivoFilter filter = new TipoMotivoFilter();

        filter
                .id(tipoMotivo.getId())
                .codigo(tipoMotivo.getCodigo())
                .descripcion(tipoMotivo.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
}
