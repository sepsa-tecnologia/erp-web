package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;

/**
 * Filtro utilizado para el servicio de configuracion valor
 *
 * @author Cristina Insfrán
 */
public class ConfiguracionFilter extends Filter {

    /**
     * Agrega el filtro de identificador de la configuración
     *
     * @param id Identificador de la configuración
     * @return
     */
    public ConfiguracionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para descripcion
     *
     * @param descripcion descripcion de configuracion
     * @return
     */
    public ConfiguracionFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro para codigo
     *
     * @param codigo codigo de configuración
     * @return
     */
    public ConfiguracionFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param conf datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Configuracion conf, Integer page, Integer pageSize) {
        ConfiguracionFilter filter = new ConfiguracionFilter();

        filter
                .id(conf.getId())
                .descripcion(conf.getDescripcion())
                .codigo(conf.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ConfiguracionValorFilter
     */
    public ConfiguracionFilter() {
        super();
    }
}
