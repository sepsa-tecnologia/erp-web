package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.UsuarioEncargadosListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.UsuarioLocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.UsuarioLocalRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.EncargadoFilter;
import py.com.sepsa.erp.web.v1.info.filters.UsuarioLocalFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.Encargado;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de usuario local
 *
 * @author Cristina Insfrán
 */
public class UsuarioLocalServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de usuario local
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioLocalListAdapter getUsuarioLocalList(UsuarioLocal dato, Integer page,
            Integer pageSize) {

        UsuarioLocalListAdapter lista = new UsuarioLocalListAdapter();

        Map params = UsuarioLocalFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_LOCAL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioLocalListAdapter.class);

            lista = (UsuarioLocalListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
        /**
     * Obtiene la lista de usuario local
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioLocalRelacionadoAdapter getUsuarioLocalRelacionadoList(UsuarioLocal dato, Integer page,
            Integer pageSize) {

        UsuarioLocalRelacionadoAdapter lista = new UsuarioLocalRelacionadoAdapter();

        Map params = UsuarioLocalFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_LOCAL_RELACIONADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioLocalRelacionadoAdapter.class);

            lista = (UsuarioLocalRelacionadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
    /**
     * Método para enviar y recibir el Usuario Local editado/creado
     * @param usuarioLocal
     * @return 
     */

    public UsuarioLocal editAsociacionUsuarioLocal(UsuarioLocal usuarioLocal) {

        UsuarioLocal usuarioLocalEdit = new UsuarioLocal();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.USUARIO_LOCAL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuarioLocal));

            response = BodyResponse.createInstance(conn, UsuarioLocal.class);

            usuarioLocalEdit = ((UsuarioLocal) response.getPayload());

        }
        return usuarioLocalEdit;
    }
    
      /**
     * Obtiene la lista de encargados
     * 
     * @param encargado
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioEncargadosListAdapter getUsuariosEncargadosList(Encargado encargado, Integer page,
            Integer pageSize) {
      
        UsuarioEncargadosListAdapter lista = new UsuarioEncargadosListAdapter();

        Map params = EncargadoFilter.build(encargado, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIOS_ENCARGADOS.url,
                ContentType.JSON, params);
        
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioEncargadosListAdapter.class);

            lista = (UsuarioEncargadosListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    } 
    
    /**
     * Asocia los
     * @param usuarioLocal  
     * @return 
     */
    public Boolean asociarMasivamenteUsuarioLocal(UsuarioLocal usuarioLocal) {

        BodyResponse response = new BodyResponse();

       HttpURLConnection conn = PUT(Resource.ASOCIAR_USUARIO_LOCAL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuarioLocal));

            response = BodyResponse.createInstance(conn, UsuarioLocal.class);

        }
        return response.getSuccess();
    }

    /**
     * Constructor de UsuarioLocalServiceClient
     */
    public UsuarioLocalServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        USUARIO_LOCAL_RELACIONADO("Servicio Usuario Local Relacionado", "usuario-local/relacionado"),
        USUARIO_LOCAL("Servicio Usuario Local", "usuario-local"),
        ASOCIAR_USUARIO_LOCAL("Servicio Usuario Local Asociar Masivamente", "usuario-local/asociar-masivo"),
        USUARIOS_ENCARGADOS("Encargados","usuario/encargados");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
