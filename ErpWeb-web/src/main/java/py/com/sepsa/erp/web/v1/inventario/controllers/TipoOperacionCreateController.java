package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.inventario.remote.TipoOperacionService;

/**
 * Controlador para creación de TipoOperacion
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("tipoOperacionCreate")
public class TipoOperacionCreateController implements Serializable {

    /**
     * Cliente para el servicio de tipoOperacion
     */
    private final TipoOperacionService service;

    /**
     * Pojo de tipoOperacion
     */
    private TipoOperacion tipoOperacion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(TipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    //</editor-fold>

    public void create() {

        BodyResponse<TipoOperacion> respuestaTal
                = service.setTipoOperacion(tipoOperacion);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Tipo de Operación creada correctamente!"));
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    private void init() {
        this.tipoOperacion = new TipoOperacion();
    }

    /**
     * Constructor de la clase
     */
    public TipoOperacionCreateController() {
        init();
        this.service = new TipoOperacionService();
    }

}
