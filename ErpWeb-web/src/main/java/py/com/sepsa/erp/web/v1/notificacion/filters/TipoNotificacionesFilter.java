/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;

/**
 * Tipo notificacion filter
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoNotificacionesFilter extends Filter {

    /**
     * Agrega el filtro de identificador de tipo notificacion
     *
     * @param id
     * @return
     */
    public TipoNotificacionesFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Filtro por codigo de tipo notificacion
     *
     * @param code
     * @return
     */
    public TipoNotificacionesFilter code(String code) {
        if (code != null) {
            params.put("code", code);
        }
        return this;
    }

    /**
     * Filtro por description de tipo notificacion
     *
     * @param description
     * @return
     */
    public TipoNotificacionesFilter description(String description) {
        if (description != null) {
            params.put("description", description);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoNotificacion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoNotificaciones tipoNotificacion, Integer page, Integer pageSize) {
        TipoNotificacionesFilter filter = new TipoNotificacionesFilter();

        filter
                .id(tipoNotificacion.getId())
                .code(tipoNotificacion.getCode())
                .description(tipoNotificacion.getDescription())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoNotificacionFilter
     */
    public TipoNotificacionesFilter() {
        super();
    }
}
