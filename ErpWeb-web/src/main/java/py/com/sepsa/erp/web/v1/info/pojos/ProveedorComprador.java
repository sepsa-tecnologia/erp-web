package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de ProveedorComprador
 *
 * @author Romina E. Núñez Rojas
 */
public class ProveedorComprador {

    /**
     * Identificador del comprador
     */
    private Integer idComprador;
    /**
     * Identificador del proveedor
     */
    private Integer idProveedor;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Estado
     */
    private String estado;
    /**
     * Fecha inserción
     */
    private String fechaInsercion;
    /**
     * Fecha modificación
     */
    private String fechaModificacion;

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    /**
     * Constructor de la clase
     */
    public ProveedorComprador() {

    }

    /**
     * Contructor de la clase con parametros
     *
     * @param idComprador
     * @param idProveedor
     * @param estado
     * @param idProducto
     */
    public ProveedorComprador(Integer idComprador, Integer idProveedor, String estado, Integer idProducto) {
        this.idComprador = idComprador;
        this.idProveedor = idProveedor;
        this.estado = estado;
        this.idProducto = idProducto;
    }
    
    /**
     * Contructor de la clase con parametros
     * @param idComprador
     * @param idProveedor
     * @param idProducto
     * @param estado
     * @param fechaInsercion
     * @param fechaModificacion 
     */
    public ProveedorComprador(Integer idComprador, Integer idProveedor, Integer idProducto, String estado, String fechaInsercion, String fechaModificacion) {
        this.idComprador = idComprador;
        this.idProveedor = idProveedor;
        this.idProducto = idProducto;
        this.estado = estado;
        this.fechaInsercion = fechaInsercion;
        this.fechaModificacion = fechaModificacion;
    }
    
    
}
