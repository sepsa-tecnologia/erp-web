
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
  * POJO para la lista retención detalle
 * @author Cristina Insfrán
 */
public class RetencionDetalle {

     /**
     * Identificador de retención
     */
    private Integer idRetencion;
    /**
     * Identificador de factura de compra
     */
    private Integer idFacturaCompra;
    /**
     * Fecha retención
     */
    private Date fecha;
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    /**
     * Nro de retención
     */
    private String nroRetencion;
    /**
     * Porcentaje Retención
     */
    private String porcentajeRetencion;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto iva
     */
    private BigDecimal montoIva;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Monto retenido
     */
    private BigDecimal montoRetenido;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Codigo de estado
     */
    private String codigoEstado;
    /**
     * Objeto retención
     */
    private Retencion retencion;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    
    /**
     * @return the idFacturaCompra
     */
    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    /**
     * @param idFacturaCompra the idFacturaCompra to set
     */
    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    /**
     * @return the retencion
     */
    public Retencion getRetencion() {
        return retencion;
    }

    /**
     * @param retencion the retencion to set
     */
    public void setRetencion(Retencion retencion) {
        this.retencion = retencion;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the codigoEstado
     */
    public String getCodigoEstado() {
        return codigoEstado;
    }

    /**
     * @param codigoEstado the codigoEstado to set
     */
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }
    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }
    
    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    /**
     * @return the porcentajeRetencion
     */
    public String getPorcentajeRetencion() {
        return porcentajeRetencion;
    }
    
    /**
     * @param porcentajeRetencion the porcentajeRetencion to set
     */
    public void setPorcentajeRetencion(String porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }
    
    /**
     * @return the nroRetencion
     */
    public String getNroRetencion() {
        return nroRetencion;
    }
    
    /**
     * @param nroRetencion the nroRetencion to set
     */
    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }
    
    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }
    
    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    /**
     * @return the idRetencion
     */
    public Integer getIdRetencion() {
        return idRetencion;
    }
    
    /**
     * @param idRetencion the idRetencion to set
     */
    public void setIdRetencion(Integer idRetencion) {
        this.idRetencion = idRetencion;
    }
    
    /**
     * @return the idFactura
     */
    public Integer getIdFactura() {
        return idFactura;
    }
    
    /**
     * @param idFactura the idFactura to set
     */
    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }
    
    /**
     * @return the montoImponible
     */
    public BigDecimal getMontoImponible() {
        return montoImponible;
    }
    
    /**
     * @param montoImponible the montoImponible to set
     */
    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }
    
    /**
     * @return the montoIva
     */
    public BigDecimal getMontoIva() {
        return montoIva;
    }
    
    /**
     * @param montoIva the montoIva to set
     */
    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }
    
    /**
     * @return the montoTotal
     */
    public BigDecimal getMontoTotal() {
        return montoTotal;
    }
    
    /**
     * @param montoTotal the montoTotal to set
     */
    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }
    
    /**
     * @return the montoRetenido
     */
    public BigDecimal getMontoRetenido() {
        return montoRetenido;
    }
    
    /**
     * @param montoRetenido the montoRetenido to set
     */
    public void setMontoRetenido(BigDecimal montoRetenido) {
        this.montoRetenido = montoRetenido;
    }
//</editor-fold>
    
     public RetencionDetalle(Integer idRetencion, 
             Integer idFactura, BigDecimal montoImponible, 
             BigDecimal montoIva, BigDecimal montoTotal, 
             BigDecimal montoRetenido) {
        this.idRetencion = idRetencion;
        this.idFactura = idFactura;
        this.montoImponible = montoImponible;
        this.montoIva = montoIva;
        this.montoTotal = montoTotal;
        this.montoRetenido = montoRetenido;
    }
     
     /**
      * Constructor
      */
     public RetencionDetalle(){
         
     }
}
