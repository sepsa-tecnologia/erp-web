package py.com.sepsa.erp.api.response;

/**
 * Definición de códigos de respuesta
 * @author Daniel F. Escauriza Arza
 */
public enum ResponseCode {
    
    SERVICE_OK(200, "Servicio ejecutado con éxito"),
    
    SERVICE_KO(500, "Se produjo un error en el servicio"),
    
    INVALID_PARAM(401, "Parámetro inválido"),
    ACCESS_DENIED(402, "Acceso denegado"),
    ACCESS_FORBIDDEN(403, "Acceso bloqueado"),
    EXPIRED_TOKEN(416, "Token expirado");
    
    /**
     * Código de respuesta
     */
    private int code;
    
    /**
     * Descripción de la respuesta
     */
    private String description;

    /**
     * Obtiene el código de respuesta
     * @return Código de respuesta
     */
    public int getCode() {
        return code;
    }

    /**
     * Setea el código de respuesta
     * @param code Código de respuesta
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción de la respuesta
     * @return Descripción de la respuesta
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción de la respuesta
     * @param description Descripción de la respuesta
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Constructor de ResponseCode
     * @param code Código de respuesta
     * @param description Descripción de la respuesta
     */
    private ResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
