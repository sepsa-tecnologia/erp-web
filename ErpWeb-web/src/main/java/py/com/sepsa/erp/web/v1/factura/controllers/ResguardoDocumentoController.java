package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.ResguardoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ResguardoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.ResguardoDocumentoService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar facturas
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("listarResguardoDocumento")
public class ResguardoDocumentoController implements Serializable {

    /**
     * POJO Resguardo Documento
     */
    private ResguardoDocumento resguardoDocumentoFilter;
    /**
     * Adaptador para la lista de datos
     */
    private ResguardoDocumentoAdapter adapterResguardoDocumento;
    /**
     * Adaptador para el tipo de documento
     */
    private TipoDocumentoAdapter adapterTipoDoc;
    /**
     * POJO Tipo Documento
     */
    private TipoDocumento tipoDocFilter;
    /**
     * Service
     */
    private ResguardoDocumentoService serviceResguardoDoc;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public ResguardoDocumentoAdapter getAdapterResguardoDocumento() {
        return adapterResguardoDocumento;
    }

    public void setServiceResguardoDoc(ResguardoDocumentoService serviceResguardoDoc) {
        this.serviceResguardoDoc = serviceResguardoDoc;
    }

    public ResguardoDocumentoService getServiceResguardoDoc() {
        return serviceResguardoDoc;
    }

    public void setTipoDocFilter(TipoDocumento tipoDocFilter) {
        this.tipoDocFilter = tipoDocFilter;
    }

    public void setAdapterTipoDoc(TipoDocumentoAdapter adapterTipoDoc) {
        this.adapterTipoDoc = adapterTipoDoc;
    }

    public TipoDocumento getTipoDocFilter() {
        return tipoDocFilter;
    }

    public TipoDocumentoAdapter getAdapterTipoDoc() {
        return adapterTipoDoc;
    }

    public ResguardoDocumento getResguardoDocumentoFilter() {
        return resguardoDocumentoFilter;
    }

    public void setAdapterResguardoDocumento(ResguardoDocumentoAdapter adapterResguardoDocumento) {
        this.adapterResguardoDocumento = adapterResguardoDocumento;
    }

    public void setResguardoDocumentoFilter(ResguardoDocumento resguardoDocumentoFilter) {
        this.resguardoDocumentoFilter = resguardoDocumentoFilter;
    }
//</editor-fold>

    /**
     * Método para filtrar el listado
     */
    public void filter() {
        adapterResguardoDocumento = adapterResguardoDocumento.fillData(resguardoDocumentoFilter);
    }

    /**
     * Método para filtrar el listado
     */
    public void filterTipoDoc() {
        adapterTipoDoc = adapterTipoDoc.fillData(tipoDocFilter);
    }

    public void clear() {
        resguardoDocumentoFilter = new ResguardoDocumento();
        filter();
    }

    public String obtenerURLDescarga(Integer id) {
        ResguardoDocumento resDocumentoDescarga = new ResguardoDocumento();
        resDocumentoDescarga = serviceResguardoDoc.getURLDescarga(id);
        String r = resDocumentoDescarga.getUrlDescarga();
        return r;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.resguardoDocumentoFilter = new ResguardoDocumento();
        this.adapterResguardoDocumento = new ResguardoDocumentoAdapter();

        this.tipoDocFilter = new TipoDocumento();
        this.adapterTipoDoc = new TipoDocumentoAdapter();
        this.serviceResguardoDoc = new ResguardoDocumentoService();
        filter();
        filterTipoDoc();
    }
}
