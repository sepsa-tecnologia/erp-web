/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;

/**
 *
 * @author Romina Núñez
 */
public class MenuPerfil {
    
    /**
     * Identificador de Menu Perfil
     */
    private Integer id;
    /**
     * Identificador de Perfil
     */
    private Integer idPerfil;
    /**
     * Identificador de Menú
     */
    private Integer idMenu;
    /**
     * Activo
     */
    private String activo;
    /**
     * POJO Perfil
     */
    private Perfil perfil;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Integer getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    /**
     * Constructor
     */
    public MenuPerfil() {
    }

}
