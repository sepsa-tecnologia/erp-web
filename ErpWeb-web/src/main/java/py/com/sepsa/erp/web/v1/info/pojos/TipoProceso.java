/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * pojo para tipo proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoProceso {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * codigo
     */
    private String codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public TipoProceso() {

    }
}
