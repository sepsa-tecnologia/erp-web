package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobrosService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para detalles de cobros
 *
 * @author alext, Romina Núñez
 */
@ViewScoped
@Named("cobrosDetalle")
public class CobrosDetalleController implements Serializable {

    /**
     * Servicio para lista de cobros
     */
    private CobrosService service;

    /**
     * Pojo de Cobros
     */
    private Cobro cobrosDetalle;

    /**
     * Identificador del cobro
     */
    private String idCobros;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;

    @Inject
    private SessionData session;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * Obtiene el servicio para lista de cobros
     *
     * @return lista de cobros
     */
    
    public CobrosService getService() {
        return service;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    /**
     * Setea el servicio para lista de cobros
     *
     * @param service servicio para lista de cobros
     */
    public void setService(CobrosService service) {
        this.service = service;
    }

    /**
     * Obtiene el pojo de cobros
     *
     * @return pojo de cobros
     */
    public Cobro getCobrosDetalle() {
        return cobrosDetalle;
    }

    /**
     * Setea el pojo para cobros
     *
     * @param cobrosDetalle pojo con detalle de cobros
     */
    public void setCobrosDetalle(Cobro cobrosDetalle) {
        this.cobrosDetalle = cobrosDetalle;
    }

    /**
     * Obtiene el identificador del cobro a ser visualizado en detalle
     *
     * @return cobro a ser visualizado
     */
    public String getIdCobros() {
        return idCobros;
    }

    /**
     * Setea el identificador del cobro a ser visualizado en detalle
     *
     * @param idCobros identificador del cobro a ser visualizados
     */
    public void setIdCobros(String idCobros) {
        this.idCobros = idCobros;
    }
    //</editor-fold>

    /**
     * Método para obtener detalle de cobros
     */
    public void obtenerCobroDetalle() {
        cobrosDetalle.setId(Integer.parseInt(idCobros));

        cobrosDetalle = service.getCobroDetalle(cobrosDetalle);
    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = cobrosDetalle.getNroRecibo() + ".pdf";
        String url = "cobro/consulta/" + idCobros;
        byte[] data = fileServiceClient.download(url);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "cobro/consulta/" + idCobros;
        byte[] data = fileServiceClient.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idCobros = params.get("id");
        this.service = new CobrosService();
        this.cobrosDetalle = new Cobro();
        this.fileServiceClient = new FileServiceClient();
        obtenerCobroDetalle();
    }

}
