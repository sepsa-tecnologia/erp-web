package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.task.GeneracionDteFacturaMultiempresa;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("facturaCargaMasivaAlternativo")
public class FacturaCargaMasivaControllerAlternativo implements Serializable {

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    
    /**
     * Archivo de instructivo para la carga de facturas
     */
    private StreamedContent loadFacturaCarga;
    /**
     * Objeto Attach
     */
    private Attach attach;
    /**
     * Factura Service
     */
    private FacturacionService serviceFacturacion;
    /**
     * Archivo
     */
    private UploadedFile file;
    /**
     * Datos para descarga
     */
    private byte[] dataDownload;
    /**
     * Bandera para descargar
     */
    private boolean downloadBtn;

    private String fileNameDownload = "carga_masiva_factura.csv";

    public UploadedFile getFile() {
        return file;
    }

    public void setDownloadBtn(boolean downloadBtn) {
        this.downloadBtn = downloadBtn;
    }

    public boolean isDownloadBtn() {
        return downloadBtn;
    }

    public void setDataDownload(byte[] dataDownload) {
        this.dataDownload = dataDownload;
    }

    public byte[] getDataDownload() {
        return dataDownload;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void setServiceFacturacion(FacturacionService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    public void setLoadFacturaCarga(StreamedContent loadFacturaCarga) {
        this.loadFacturaCarga = loadFacturaCarga;
    }

    public FacturacionService getServiceFacturacion() {
        return serviceFacturacion;
    }

    /**
     * Método para descargar el archivo plantilla
     *
     * @return
     */
    public StreamedContent getLoadFacturaCarga() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/app/factura/download/factura-matriz-carga-masiva.csv");
        loadFacturaCarga = new DefaultStreamedContent(stream, "text/csv", "factura-matriz-carga-masiva.csv");
        return loadFacturaCarga;
    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     * @throws IOException
     */
    public void upload() throws IOException {
        if (file != null) {
            attach = new Attach(file.getFileName(), file.getInputstream(), file.getContentType());
            byte[] data = serviceFacturacion.sendFileMassive(attach);
            dataDownload = data;
            if (data != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado el archivo correctamente!"));
                this.downloadBtn = true;

                try {
                    GeneracionDteFacturaMultiempresa.generar(session.getUser().getIdEmpresa());
                } catch (Exception e) {
                    WebLogger.get().fatal(e);
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar un archivo con el formato correcto: (csv)"));
        }

    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     */
    public StreamedContent download() {
        String contentType = file.getContentType();
        return new DefaultStreamedContent(new ByteArrayInputStream(dataDownload),
                contentType, fileNameDownload);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.attach = new Attach();
        this.file = null;
        this.serviceFacturacion = new FacturacionService();
        this.downloadBtn = false;
    }
}
