package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para edición de referencia geográfica
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("referenciaEdit")
public class ReferenciaGeograficaEditarController implements Serializable {

    /**
     * Cliente para el servicio de referencia geográfica
     */
    private final ReferenciaGeograficaService service;

    /**
     * Adaptador de referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapter;

    /**
     * Pojo de referencia geográfica
     */
    private ReferenciaGeografica referenciaGeografica;

    /**
     * Pojo de Referencia Geográfica
     */
    private ReferenciaGeografica referenciaAutoComplete;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public ReferenciaGeograficaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ReferenciaGeograficaAdapter adapter) {
        this.adapter = adapter;
    }

    public ReferenciaGeografica getReferenciaGeografica() {
        return referenciaGeografica;
    }

    public void setReferenciaGeografica(ReferenciaGeografica referenciaGeografica) {
        this.referenciaGeografica = referenciaGeografica;
    }

    public ReferenciaGeografica getReferenciaAutoComplete() {
        return referenciaAutoComplete;
    }

    public void setReferenciaAutoComplete(ReferenciaGeografica referenciaAutoComplete) {
        this.referenciaAutoComplete = referenciaAutoComplete;
    }
    //</editor-fold>

    /**
     * Método para editar menu
     */
    public void editarReferencia() {
        BodyResponse<ReferenciaGeografica> respuestaRef
                = service.editReferencia(this.referenciaGeografica);
        if (respuestaRef.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Referencia Geográfica editado correctamente!"));
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQuery(String query) {

        ReferenciaGeografica referencia = new ReferenciaGeografica();
        referencia.setDescripcion(query);

        adapter = adapter.fillData(referencia);

        return adapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectReferenciaFilter(SelectEvent event) {
        referenciaGeografica.setIdPadre(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Inicializa los datos
     *
     * @param id
     */
    private void init(int id) {
        this.referenciaGeografica = service.get(id);
        this.referenciaAutoComplete = new ReferenciaGeografica();
        this.adapter = new ReferenciaGeograficaAdapter();
    }

    /**
     * Constructo de la clase
     */
    public ReferenciaGeograficaEditarController() {
        this.service = new ReferenciaGeograficaService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
