package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesCadena;
import py.com.sepsa.erp.web.v1.comercial.remote.LiquidacionesCadenaService;

/**
 * Adaptador para lista de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionesCadenaAdapter extends DataListAdapter<LiquidacionesCadena> {

    /**
     * Servicio de liquidaciones por cadena.
     */
    private final LiquidacionesCadenaService liquidacionesCadenaService;

    /**
     * Método para cargar la lista de liquidaciones.
     *
     * @param searchData
     * @return
     */
    @Override
    public LiquidacionesCadenaAdapter fillData(LiquidacionesCadena searchData) {
        return liquidacionesCadenaService.getLiquidacionesCadenaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de LiquidacionesCadenaAdapter.
     *
     * @param page
     * @param pageSize
     */
    public LiquidacionesCadenaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.liquidacionesCadenaService = new LiquidacionesCadenaService();
    }

    /**
     * Constructor de LiquidacionesCadenaAdapter
     */
    public LiquidacionesCadenaAdapter() {
        super();
        this.liquidacionesCadenaService = new LiquidacionesCadenaService();
    }
}
