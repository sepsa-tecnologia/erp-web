/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.ResguardoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.ResguardoDocumentoService;

/**
 * Adapter para Resguardo Documento
 *
 * @author Romina Núñez
 */
public class ResguardoDocumentoAdapter extends DataListAdapter<ResguardoDocumento> {

    /**
     * Cliente remoto para resguardo documento
     */
    private final ResguardoDocumentoService serviceResguardoDocumento;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ResguardoDocumentoAdapter fillData(ResguardoDocumento searchData) {

        return serviceResguardoDocumento.getResguardoDocumento(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ResguardoDocumentoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceResguardoDocumento = new ResguardoDocumentoService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public ResguardoDocumentoAdapter() {
        super();
        this.serviceResguardoDocumento = new ResguardoDocumentoService();
    }
}
