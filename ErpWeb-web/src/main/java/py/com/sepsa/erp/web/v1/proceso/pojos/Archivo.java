/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.pojos;

/**
 *
 * @author Antonella Lucero
 */
public class Archivo {
    /**
     * Nombre del Archivo
     * */
    private String nombreArchivo;
    /**
     * Código del Archivo
     * */
    private String codigo;
    /**
     * Contenido del Archivo
     * */
    private String contenido;

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Archivo() {
    }
    
    
}
