/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoArchivo;
import py.com.sepsa.erp.web.v1.info.remote.TipoArchivoService;


/**
 * Adapter para Tipo Archivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoArchivoAdapter extends DataListAdapter<TipoArchivo> {

    /**
     * Cliente para el servicio de tipo archivo
     */
    private final TipoArchivoService serviceTipoArchivo;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoArchivoAdapter fillData(TipoArchivo searchData) {

        return serviceTipoArchivo.getTipoArchivoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoArchivoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoArchivoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoArchivo = new TipoArchivoService();
    }

    /**
     * Constructor de TalonarioAdapter
     */
    public TipoArchivoAdapter() {
        super();
        this.serviceTipoArchivo = new TipoArchivoService();
    }
}
