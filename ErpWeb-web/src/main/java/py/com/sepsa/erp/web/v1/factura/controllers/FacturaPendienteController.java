package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPendAgrupadosAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;

/**
 * Controlador para la vista de reporte de facturas pendientes
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("facPendienteController")
public class FacturaPendienteController implements Serializable {

    /**
     * Adaptador de la lista de facturas pendientes agrupados
     */
    private FacturaPendAgrupadosAdapter adapterFactPendAgrupados;
    /**
     * Adaptador de la lista de facturas pendientes sin agrupar
     */
    private FacturaAdapter adapterFactPendSinAgrupar;
    /**
     * Identificador del tipo de reporte
     */
    private Integer idTipoReporte;
    /**
     * Objeto Factura
     */
    private Factura factura;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the factura
     */
    public Factura getFactura() {
        return factura;
    }

    /**
     * @param factura the factura to set
     */
    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    /**
     * @return the idTipoReporte
     */
    public Integer getIdTipoReporte() {
        return idTipoReporte;
    }

    /**
     * @param idTipoReporte the idTipoReporte to set
     */
    public void setIdTipoReporte(Integer idTipoReporte) {
        this.idTipoReporte = idTipoReporte;
    }

    /**
     * @return the adapterFactPendAgrupados
     */
    public FacturaPendAgrupadosAdapter getAdapterFactPendAgrupados() {
        return adapterFactPendAgrupados;
    }

    /**
     * @param adapterFactPendAgrupados the adapterFactPendAgrupados to set
     */
    public void setAdapterFactPendAgrupados(FacturaPendAgrupadosAdapter adapterFactPendAgrupados) {
        this.adapterFactPendAgrupados = adapterFactPendAgrupados;
    }

    /**
     * @return the adapterFactPendSinAgrupar
     */
    public FacturaAdapter getAdapterFactPendSinAgrupar() {
        return adapterFactPendSinAgrupar;
    }

    /**
     * @param adapterFactPendSinAgrupar the adapterFactPendSinAgrupar to set
     */
    public void setAdapterFactPendSinAgrupar(FacturaAdapter adapterFactPendSinAgrupar) {
        this.adapterFactPendSinAgrupar = adapterFactPendSinAgrupar;
    }

//</editor-fold>
    /**
     *
     */
    public void filterReporte() {
        factura.setCobrado("N");
        factura.setAnulado("N");

        if (idTipoReporte != null) {
            if (idTipoReporte == 1) {
                adapterFactPendAgrupados.setPageSize(adapterFactPendAgrupados.getPageSize());
                adapterFactPendAgrupados = adapterFactPendAgrupados.fillData(factura);
            } else {
                adapterFactPendSinAgrupar.setPageSize(adapterFactPendSinAgrupar.getPageSize());
                adapterFactPendSinAgrupar = adapterFactPendSinAgrupar.fillData(factura);
            }
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                    "Debe Seleccionar el campo agrupados!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }

    }

    /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {
        this.adapterFactPendAgrupados = new FacturaPendAgrupadosAdapter();
        this.adapterFactPendSinAgrupar = new FacturaAdapter();
        this.factura = new Factura();

        Calendar today = Calendar.getInstance();
        today.set(2015, 1, 1);

    }
}
