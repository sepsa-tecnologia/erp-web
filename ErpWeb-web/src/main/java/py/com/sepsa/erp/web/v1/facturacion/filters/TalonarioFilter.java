/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;

/**
 * Filter para talonario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TalonarioFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TalonarioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de timbrado
     *
     * @param timbrado
     * @return
     */
    public TalonarioFilter timbrado(String timbrado) {
        if (timbrado != null && !timbrado.trim().isEmpty()) {
            params.put("timbrado", timbrado);
        }
        return this;
    }

    /**
     * Agrega el filtro inicio
     *
     * @param inicio
     * @return
     */
    public TalonarioFilter inicio(Integer inicio) {
        if (inicio != null) {
            params.put("inicio", inicio);
        }
        return this;
    }

    /**
     * Agrega el filtro de fin
     *
     * @param fin
     * @return
     */
    public TalonarioFilter fin(Integer fin) {
        if (fin != null) {
            params.put("fin", fin);
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInicio
     *
     * @param fechaInicio
     * @return
     */
    public TalonarioFilter fechaInicio(Date fechaInicio) {
        if (fechaInicio != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInicio);
                params.put("fechaInicio", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaVencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public TalonarioFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public TalonarioFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public TalonarioFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de nroSucursal
     *
     * @param nroSucursal
     * @return
     */
    public TalonarioFilter nroSucursal(String nroSucursal) {
        if (nroSucursal != null && !nroSucursal.trim().isEmpty()) {
            params.put("nroSucursal", nroSucursal);
        }
        return this;
    }

    /**
     * Agrega el filtro de nroPuntoVenta
     *
     * @param nroPuntoVenta
     * @return
     */
    public TalonarioFilter nroPuntoVenta(String nroPuntoVenta) {
        if (nroPuntoVenta != null && !nroPuntoVenta.trim().isEmpty()) {
            params.put("nroPuntoVenta", nroPuntoVenta);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param idLocal
     * @return
     */
    public TalonarioFilter idLocal(Integer idLocal) {
        if (idLocal != null) {
            params.put("idLocal", idLocal);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de código interno
     *
     * @param idLocal
     * @return
     */
    public TalonarioFilter codigoInterno(Integer codigoInterno) {
        if (codigoInterno != null) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de idTipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public TalonarioFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro de idTipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public TalonarioFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipoDocumento
     *
     * @param tipoDocumento
     * @return
     */
    public TalonarioFilter tipoDocumento(TipoDocumento tipoDocumento) {
        if (tipoDocumento != null) {
            params.put("tipoDocumento", tipoDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro de local
     *
     * @param local
     * @return
     */
    public TalonarioFilter local(Local local) {
        if (local != null) {
            params.put("local", local);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de idEncargado
     *
     * @param idEncargado
     * @return
     */
    public TalonarioFilter idEncargado(Integer idEncargado) {
        if (idEncargado != null) {
            params.put("idEncargado", idEncargado);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de idEmpresa
     *
     * @param idEmpresa
     * @return
     */
    public TalonarioFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param talonario datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Talonario talonario, Integer page, Integer pageSize) {
        TalonarioFilter filter = new TalonarioFilter();

        filter
                .id(talonario.getId())
                .timbrado(talonario.getTimbrado())
                .inicio(talonario.getInicio())
                .fin(talonario.getFin())
                .activo(talonario.getActivo())
                .fechaVencimiento(talonario.getFechaVencimiento())
                .nroSucursal(talonario.getNroSucursal())
                .nroPuntoVenta(talonario.getNroPuntoVenta())
                .idLocal(talonario.getIdLocal())
                .codigoInterno(talonario.getCodigoInterno())
                .fechaInicio(talonario.getFechaInicio())
                .digital(talonario.getDigital())
                .idTipoDocumento(talonario.getIdTipoDocumento())
                .tipoDocumento(talonario.getTipoDocumento())
                .local(talonario.getLocal())
                .idUsuario(talonario.getIdUsuario())
                .idEncargado(talonario.getIdEncargado())
                .idEmpresa(talonario.getIdEmpresa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TalonarioFilter() {
        super();
    }

}
