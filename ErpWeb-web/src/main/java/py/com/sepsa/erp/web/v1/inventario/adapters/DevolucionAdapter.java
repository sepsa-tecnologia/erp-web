/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.inventario.remote.DevolucionService;

/**
 * Adapter para operacion de inventario
 *
 * @author Romina Núñez
 */
public class DevolucionAdapter extends DataListAdapter<Devolucion> {

    /**
     * Cliente remoto para operacion de inventario
     */
    private final DevolucionService devolucionService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return OperacionAdapter
     */
    @Override
    public DevolucionAdapter fillData(Devolucion searchData) {

        return devolucionService.getDevolucion(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de OperacionAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public DevolucionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.devolucionService = new DevolucionService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public DevolucionAdapter() {
        super();
        this.devolucionService = new DevolucionService();
    }
}
