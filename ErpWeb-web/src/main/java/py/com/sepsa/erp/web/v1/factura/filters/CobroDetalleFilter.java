
package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetallePojo;
/**
 * Filtro para la vista cobro detalle
 * @author Cristina Insfrán
 */
public class CobroDetalleFilter extends Filter{
    
     /**
     * Agrega el parametro para el filtro idCobro
     *
     * @param idCobro
     * @return
     */
    public CobroDetalleFilter idCobro(Integer idCobro) {
        if (idCobro != null) {
            params.put("idCobro", idCobro);
        }
        return this;
    }
    
     /**
     * Agrega el parametro para el filtro nroLinea
     *
     * @param nroLinea
     * @return
     */
    public CobroDetalleFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro idFactura
     *
     * @param idFactura
     * @return
     */
    public CobroDetalleFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro idTipoCobro
     *
     * @param idTipoCobro
     * @return
     */
    public CobroDetalleFilter idTipoCobro(Integer idTipoCobro) {
        if (idTipoCobro != null) {
            params.put("idTipoCobro", idTipoCobro);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro idCheque
     *
     * @param idCheque
     * @return
     */
    public CobroDetalleFilter idCheque(Integer idCheque) {
        if (idCheque != null) {
            params.put("idCheque", idCheque);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro montoCobro
     *
     * @param montoCobro
     * @return
     */
    public CobroDetalleFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro idConceptoCobro
     *
     * @param idConceptoCobro
     * @return
     */
    public CobroDetalleFilter idConceptoCobro(Integer idConceptoCobro) {
        if (idConceptoCobro != null) {
            params.put("idConceptoCobro", idConceptoCobro);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro nroRecibo
     *
     * @param nroRecibo
     * @return
     */
    public CobroDetalleFilter nroRecibo(String nroRecibo) {
        if (nroRecibo != null && !nroRecibo.trim().isEmpty()) {
            params.put("nroRecibo", nroRecibo);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro nroFactura
     *
     * @param nroFactura
     * @return
     */
    public CobroDetalleFilter nroFactura(String nroFactura) {
        if (nroFactura!= null && !nroFactura.trim().isEmpty()) {
            params.put("nroFactura", nroFactura);
        }
        return this;
    }
    
          /**
     * Agrega el parametro para el filtro listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public CobroDetalleFilter listadoPojo(boolean listadoPojo) {
        if (listadoPojo) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }
    
    
       /**
     * Agrega el parametro para el filtro estado
     *
     * @param estado
     * @return
     */
   /* public CobroDetalleFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }*/
    
    
     /**
     * Construye el mapa de parametros
     *
     * @param cob
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(CobroDetalle cob,Integer page, Integer pageSize) {
        CobroDetalleFilter filter = new CobroDetalleFilter();

        filter
                .idCobro(cob.getIdCobro())
                .nroLinea(cob.getNroLinea())
                .idFactura(cob.getIdFactura())
                .idTipoCobro(cob.getIdTipoCobro())
                .idCheque(cob.getIdCheque())
                .nroRecibo(cob.getNroRecibo())
                .montoCobro(cob.getMontoCobro())
                .idConceptoCobro(cob.getIdConceptoCobro())
                //.estado(cob.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
    
    public static Map build(CobroDetallePojo cob,Integer page, Integer pageSize) {
        CobroDetalleFilter filter = new CobroDetalleFilter();

        filter
                .idCobro(cob.getIdCobro())
                .nroLinea(cob.getNroLinea())
                .idFactura(cob.getIdFactura())
                .idTipoCobro(cob.getIdTipoCobro())
                .nroRecibo(cob.getNroRecibo())
                .nroFactura(cob.getNroFactura())
                .montoCobro(cob.getMontoCobro())
                .idConceptoCobro(cob.getIdConceptoCobro())
                .listadoPojo(cob.getListadoPojo())
                //.estado(cob.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de CobroDetalleFilter
     */
    public CobroDetalleFilter() {
        super();
    }

}
