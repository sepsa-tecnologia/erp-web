package py.com.sepsa.erp.web.v1.system.pojos;

/**
 * POJO para la credencial de un usuario
 *
 * @author Daniel F. Escauriza Arza
 */
public class Credential {

    /**
     * Usuario
     */
    private String usuario;

    /**
     * Contraseña del usuario
     */
    private String contrasena;

    /**
     * Usuario
     */
    private String userName;

    /**
     * Contraseña del usuario
     */
    private String password;

    public String getUsuario() {
        return usuario;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Constructor de Credential
     *
     * @param usuario Usuario
     * @param contrasena Contraseña del usuario
     */
    public Credential(String usuario, String contrasena) {
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    /**
     * Constructor de Credential
     */
    public Credential() {
    }

}
