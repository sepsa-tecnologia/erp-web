package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.Date;

/**
 * POJO de MotivoDescuento
 *
 * @author Romina E. Núñez Rojas
 */
public class MotivoDescuento {

    /**
     * Identificador del motivo descuento
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Rango Desde
     */
    private Double rangoDesde;
    /**
     * Rango Hasta
     */
    private Double rangoHasta;
    /**
     * Porcentual
     */
    private String porcentual;
    /**
     * Estado del descuento
     */
    private String estado;
    /**
     * Identificador del tipo etiqueta 
     */
    private Integer idTipoEtiqueta;
    /**
     * Tipo Etiqueta
     */
    private String tipoEtiqueta;
    /**
     * Fecha Desde
     */
    private Date fechaDesde;
    /**
     * Fecha hasta
     */
    private Date fechaHasta;

    public Integer getId() {
        return id;
    }

    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    public String getTipoEtiqueta() {
        return tipoEtiqueta;
    }

    public void setTipoEtiqueta(String tipoEtiqueta) {
        this.tipoEtiqueta = tipoEtiqueta;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getRangoDesde() {
        return rangoDesde;
    }

    public void setRangoDesde(Double rangoDesde) {
        this.rangoDesde = rangoDesde;
    }

    public Double getRangoHasta() {
        return rangoHasta;
    }

    public void setRangoHasta(Double rangoHasta) {
        this.rangoHasta = rangoHasta;
    }

    public String getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(String porcentual) {
        this.porcentual = porcentual;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    /**
     * Contructor de la clase
     */
    public MotivoDescuento() {

    }

    
    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion
     * @param rangoDesde
     * @param rangoHasta
     * @param porcentual
     * @param estado
     * @param idTipoEtiqueta
     * @param tipoEtiqueta
     * @param fechaDesde
     * @param fechaHasta 
     */
    public MotivoDescuento(Integer id, String descripcion, Double rangoDesde, Double rangoHasta, String porcentual, String estado, Integer idTipoEtiqueta, String tipoEtiqueta, Date fechaDesde, Date fechaHasta) {
        this.id = id;
        this.descripcion = descripcion;
        this.rangoDesde = rangoDesde;
        this.rangoHasta = rangoHasta;
        this.porcentual = porcentual;
        this.estado = estado;
        this.idTipoEtiqueta = idTipoEtiqueta;
        this.tipoEtiqueta = tipoEtiqueta;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
    }
    
   

}
