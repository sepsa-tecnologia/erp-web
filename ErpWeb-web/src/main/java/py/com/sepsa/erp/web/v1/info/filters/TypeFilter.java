
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;

/**
 * Filtro utilizado para tipo de dato
 * @author Romina Núñez
 */
public class TypeFilter extends Filter{
 

     /**
     * Agrega el filtro de identificador del tipo de dato
     *
     * @param id Identificador del tipo de dato
     * @return
     */
    public TypeFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    /**
     * Agregar el filtro de descripcion del tipo de dato
     *
     * @param tipoDato Tipo de Dato
     * @return
     */
    public TypeFilter tipodato (String tipoDato) {
        if (tipoDato != null) {
            params.put("tipoDato",tipoDato);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param tipodato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoDato tipodato, Integer page, Integer pageSize) {
        TypeFilter filter = new TypeFilter();
        
        filter
                .id(tipodato.getId())
                .tipodato(tipodato.getTipoDato())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClientFilter
     */
    public TypeFilter() {
        super();
    }


}
