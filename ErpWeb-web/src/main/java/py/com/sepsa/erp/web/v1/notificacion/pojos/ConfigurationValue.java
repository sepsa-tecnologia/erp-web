/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.pojos;

/**
 * Pojo para configuracion valor / configuration value
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ConfigurationValue {

    /**
     * Identificador del contrato
     */
    private Integer id;

    /**
     * Valor de la configuracion
     */
    private String value;

    /**
     * Objeto configuration
     */
    private Configuration configuration;

    /**
     * Objeto tipo noficicacion
     */
    private TipoNotificaciones notificationType;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * @return the notificationType
     */
    public TipoNotificaciones getNotificationType() {
        return notificationType;
    }

    /**
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(TipoNotificaciones notificationType) {
        this.notificationType = notificationType;
    }
//</editor-fold>

    /**
     * Contructor de la clase
     */
    public ConfigurationValue() {

    }

}
