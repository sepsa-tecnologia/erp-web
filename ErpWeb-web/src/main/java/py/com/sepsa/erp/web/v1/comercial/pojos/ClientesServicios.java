package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para clientes según servicios.
 *
 * @author alext
 */
public class ClientesServicios {

    /**
     * Identificador del cliente.
     */
    private Integer idCliente;
    /**
     * Razón social del cliente.
     */
    private String cliente;
    /**
     * nroDocumento del cliente.
     */
    private String nroDocumento;
    /**
     * Identificador del producto.
     */
    private Integer idProducto;
    /**
     * nombre del producto.
     */
    private String producto;
    /**
     * Identificador del servicio.
     */
    private Integer idServicio;
    /**
     * Nombre del Servicio.
     */
    private String servicio;
    /**
     * Identificador del agente comercial.
     */
    private Integer idComercial;
    /**
     * Agente Comercial.
     */
    private String comercial;
    /**
     * Estado del cliente.
     */
    private String estado;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

//</editor-fold>
    /**
     * Constructor.
     */
    public ClientesServicios() {

    }

    public ClientesServicios(Integer idCliente, String cliente, Integer idProducto, String producto,
            Integer idServicio, String servicio, Integer idComercial, String comercial, String nroDocumento, String estado) {
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.idComercial = idComercial;
        this.comercial = comercial;
        this.nroDocumento = nroDocumento;
        this.estado = estado;
    }

}
