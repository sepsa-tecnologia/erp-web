/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para transportista
 *
 * @author Williams Vera
 */
public class VehiculoTrasladoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public VehiculoTrasladoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }



    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public VehiculoTrasladoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    
    /**
     * Agrega el filtro de marca
     *
     * @param marca
     * @return
     */
    public VehiculoTrasladoFilter marca(String marca) {
        if (marca != null && !marca.trim().isEmpty()) {
            params.put("marca", marca);
        }
        return this;
    }
    
        /**
     * Agrega el filtro de nroIdentificacion
     *
     * @param nroIdentificacion
     * @return
     */
    public VehiculoTrasladoFilter nroIdentificacion(String nroIdentificacion) {
        if (nroIdentificacion != null && !nroIdentificacion.trim().isEmpty()) {
            params.put("nroIdentificacion", nroIdentificacion);
        }
        return this;
    }
    
            /**
     * Agrega el filtro de nroMatricula
     *
     * @param nroMatricula
     * @return
     */
    public VehiculoTrasladoFilter nroMatricula(String nroMatricula) {
        if (nroMatricula != null && !nroMatricula.trim().isEmpty()) {
            params.put("nroMatricula", nroMatricula);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipoIdentificacion
     *
     * @param tipoIdentificacion
     * @return
     */
    public VehiculoTrasladoFilter tipoIdentificacion(Integer tipoIdentificacion) {
        if (tipoIdentificacion != null) {
            params.put("tipoIdentificacion", tipoIdentificacion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param vehiculoTraslado datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(VehiculoTraslado vehiculoTraslado, Integer page, Integer pageSize) {
        VehiculoTrasladoFilter filter = new VehiculoTrasladoFilter();

        filter
                .id(vehiculoTraslado.getId())
                .activo(vehiculoTraslado.getActivo())
                .marca(vehiculoTraslado.getMarca())
                .nroMatricula(vehiculoTraslado.getNroMatricula())
                .tipoIdentificacion(vehiculoTraslado.getTipoIdentificacion())
                .nroIdentificacion(vehiculoTraslado.getNroIdentificacion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public VehiculoTrasladoFilter() {
        super();
    }

}
