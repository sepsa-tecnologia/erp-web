/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;

/**
 *
 * @author Romina Núñez
 */
@FacesConverter("clienteListadoPojoConverter")
public class ClienteListadoPojoConverter implements Converter{
    
      /**
     * Servicios para cliente
     */
    private ClientServiceClient serviceClient;
    
    @Override
    public ClientePojo getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new ClientServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            ClientePojo cli=new ClientePojo();
            cli.setListadoPojo(true);
            cli.setIdCliente(val);
          
            List<ClientePojo> clientes = serviceClient.getClientPojoList(cli, 0, 10).getData();
            if(clientes != null && !clientes.isEmpty()) {
                return clientes.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un cliente válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof ClientePojo) {
                return String.valueOf(((ClientePojo)object).getIdCliente());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
