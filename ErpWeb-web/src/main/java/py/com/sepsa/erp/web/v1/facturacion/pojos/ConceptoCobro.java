package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * Pojo para la entidad Concepto de Cobro
 *
 * @author alext
 */
public class ConceptoCobro {
    
    /**
     * Identificador de concepto de cobro
     */
    private Integer id;
    
    /**
     * Descripción de concepto de cobro
     */
    private String descripcion;
    
    /**
     * Código de concepto de cobro
     */
    private String codigo;
    
    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public ConceptoCobro() {
        
    }

}
