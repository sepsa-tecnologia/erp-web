/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para Tipo Menu
 *
 * @author Jonathan Bernal
 */
public class TipoMenu {

    /**
     * Id
     */
    private Integer id;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * Codigo
     */
    private String codigo;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
