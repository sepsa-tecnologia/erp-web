
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;




/**
 * Adaptador de la lista de configuración valor
 * @author Cristina Insfrán
 */
public class ConfiguracionValorListAdapter extends DataListAdapter<ConfiguracionValor> {
    
    /**
     * Cliente para el servicio de Configuracion Valor
     */
    private final ConfiguracionValorServiceClient confValorClient;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ConfiguracionValorListAdapter fillData(ConfiguracionValor searchData) {
     
        return confValorClient.getConfValorList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ConfiguracionValorListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConfiguracionValorListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.confValorClient = new ConfiguracionValorServiceClient();
    }

    /**
     * Constructor de ConfiguracionValorListAdapter
     */
    public ConfiguracionValorListAdapter() {
        super();
        this.confValorClient = new ConfiguracionValorServiceClient();
    }
    
}
