package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;

/**
 * Controlador para Crear Configuracion Valor
 *
 * @author Crisitina Insfrán, Romina Nuñez, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("crearConfiguracionValor")
public class ConfiguracionValorCrearController implements Serializable {

    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorNuevo;
    /**
     * Adaptador para la lista de configuracion
     */
    private ConfiguracionListAdapter adapterConfig;
    /**
     * POJO Configuración
     */
    private Configuracion configuracionFilter;
    /**
     * Llamada al servicio de Configuración Valor
     */
    private ConfiguracionValorServiceClient serviceConfiguracionValor;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public ConfiguracionValor getConfiguracionValorNuevo() {
        return configuracionValorNuevo;
    }

    public void setServiceConfiguracionValor(ConfiguracionValorServiceClient serviceConfiguracionValor) {
        this.serviceConfiguracionValor = serviceConfiguracionValor;
    }

    public ConfiguracionValorServiceClient getServiceConfiguracionValor() {
        return serviceConfiguracionValor;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public void setConfiguracionValorNuevo(ConfiguracionValor configuracionValorNuevo) {
        this.configuracionValorNuevo = configuracionValorNuevo;
    }

    public ConfiguracionListAdapter getAdapterConfig() {
        return adapterConfig;
    }

    public void setAdapterConfig(ConfiguracionListAdapter adapterConfig) {
        this.adapterConfig = adapterConfig;
    }

    public Configuracion getConfiguracionFilter() {
        return configuracionFilter;
    }

    public void setConfiguracionFilter(Configuracion configuracionFilter) {
        this.configuracionFilter = configuracionFilter;
    }

    //</editor-fold>
    /**
     * Método para filtrar configuración
     */
    public void filterConfiguracion() {
        this.adapterConfig = adapterConfig.fillData(configuracionFilter);
    }

    /**
     * Metodo para crear Configuracion Valor
     */
    public void createConfiguracion() {
        configuracionValorNuevo.setActivo("S");
        BodyResponse<ConfiguracionValor> respuesta = serviceConfiguracionValor.createConfiguracionValor(configuracionValorNuevo);
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Configuracion Valor creado correctamente!"));
            init();
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Configuracion> completeQuery(String query) {
        Configuracion configuracion = new Configuracion();
        configuracion.setDescripcion(query);
        adapterConfig = adapterConfig.fillData(configuracion);
        return adapterConfig.getData();
    }

    /**
     * Selecciona configuracion
     *
     * @param event
     */
    public void onItemSelectConfiguracion(SelectEvent event) {
        configuracionValorNuevo.setIdConfiguracion(((Configuracion) event.getObject()).getId());
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        configuracionValorNuevo.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        this.configuracionValorNuevo = new ConfiguracionValor();
        this.adapterConfig = new ConfiguracionListAdapter();
        this.configuracionFilter = new Configuracion();
        this.serviceConfiguracionValor = new ConfiguracionValorServiceClient();
        filterConfiguracion();
    }
}
