/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * Filter para producto info
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MetricaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public MetricaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de producto
     *
     * @param descripcion
     * @return
     */
    public MetricaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigoGtin del producto
     *
     * @param codigoGtin
     * @return
     */
    public MetricaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo del producto
     *
     * @param activo
     * @return
     */
    public MetricaFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Metrica metrica, Integer page, Integer pageSize) {
        MetricaFilter filter = new MetricaFilter();

        filter
                .id(metrica.getId())
                .descripcion(metrica.getDescripcion())
                .codigo(metrica.getCodigo())
                .activo(metrica.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public MetricaFilter() {
        super();
    }
}
