package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ConfiguracionFilter;
import py.com.sepsa.erp.web.v1.info.filters.ConfiguracionValorFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de configuracion valor
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class ConfiguracionValorServiceClient extends APIErpCore {

    /**
     * Método para setea la configuracion valor
     *
     * @param confValor
     * @return
     */
    public ConfiguracionValor editConfiguracionValor(ConfiguracionValor confValor) {

        ConfiguracionValor configValor = new ConfiguracionValor();

        HttpURLConnection conn = PUT(Resource.CONF_VALOR.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(confValor));

            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionValor.class);

            configValor = (ConfiguracionValor) response.getPayload();

            conn.disconnect();
        }
        return configValor;
    }

    /**
     * Método para setear configuración valor
     *
     * @param confValor
     * @return
     */
    public BodyResponse<ConfiguracionValor> createConfiguracionValor(ConfiguracionValor confValor) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CONF_VALOR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(confValor));
            response = BodyResponse.createInstance(conn, ConfiguracionValor.class);
        }
        return response;
    }

    /**
     * Método para setear configuración
     *
     * @param conf
     * @return
     */
    public Integer createConfiguracion(Configuracion conf) {

        Integer idConfig = null;

        HttpURLConnection conn = POST(Resource.CONF_SOFTWARE.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(conf));

            BodyResponse response = BodyResponse.createInstance(conn,
                    Configuracion.class);

            idConfig = ((Configuracion) response.getPayload()).getId();

            conn.disconnect();
        }
        return idConfig;
    }

    /**
     * Obtiene la lista de Configuración valor
     *
     * @param conf
     * @param page
     * @param pageSize
     * @return
     */
    public ConfiguracionValorListAdapter getConfValorList(ConfiguracionValor conf, Integer page,
            Integer pageSize) {

        ConfiguracionValorListAdapter lista = new ConfiguracionValorListAdapter(page, pageSize);

        Map params = ConfiguracionValorFilter.build(conf, page, pageSize);
        
        HttpURLConnection conn = GET(Resource.CONF_VALOR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionValorListAdapter.class);

            lista = (ConfiguracionValorListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
    public Integer getConfValorInteger(Integer idEmpresa, String codigo) {
        return getConfValorInteger(idEmpresa, codigo, null);
    }
    
    public static Integer getConfValorInteger(Integer idEmpresa, String codigo, Integer default_) {
        String valor = getConfValor(idEmpresa, codigo, null);
        try {
            return Integer.valueOf(valor);
        } catch(Exception e) {
            return default_;
        }
    }
    
    public String getConfValor(Integer idEmpresa, String codigo) {
        return getConfValor(idEmpresa, codigo, null);
    }
    
    public static String getConfValor(Integer idEmpresa, String codigo, String default_) {
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setIdEmpresa(idEmpresa);
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            return default_;
        } else {
            return a.getData().get(0).getValor();
        }
    }

    /**
     * Obtiene la lista de Configuración valor
     *
     * @param conf
     * @param page
     * @param pageSize
     * @return
     */
    public ConfiguracionListAdapter getConfiguracionList(Configuracion conf, Integer page,
            Integer pageSize) {

        ConfiguracionListAdapter lista = new ConfiguracionListAdapter(page, pageSize);

        Map params = ConfiguracionFilter.build(conf, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONF.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionListAdapter.class);

            lista = (ConfiguracionListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de ConfiguracionValorServiceClient
     */
    public ConfiguracionValorServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        CONF_VALOR("Servicio configuración valor", "configuracion-valor"),
        CONF("Servicio configuración ", "configuracion"),
        CONF_SOFTWARE("Servicio configuracion software", "configuracion"),
        CONF_VALOR_DEFAULT("Obtener Configuración Valor o Default", "configuracion-valor/config-valor-default/");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
