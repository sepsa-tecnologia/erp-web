
package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ReporteClienteMoraListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.ReporteClienteFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteClienteMora;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio cliente para reporte
 * @author Cristina Insfrán
 */
public class ReporteClienteMoraService extends APIErpFacturacion {
   
      /**
     * Obtiene la lista de cobros
     *
     * @param reporte
     * @param page
     * @param pageSize
     * @return
     */
    public ReporteClienteMoraListAdapter getReporteList(ReporteClienteMora reporte, Integer page,
            Integer pageSize) {

        ReporteClienteMoraListAdapter lista = new ReporteClienteMoraListAdapter();

        Map params = ReporteClienteFilter.build(reporte, page, pageSize);

        HttpURLConnection conn = GET(Resource.REPORTE_CLIENTE.url,
                ContentType.JSON, params);
  
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ReporteClienteMoraListAdapter.class);

            lista = (ReporteClienteMoraListAdapter) response.getPayload();
           
            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Resumen de reporte
     * @param reporte
     * @param page
     * @param pageSize
     * @return 
     */
    public ReporteClienteMora getReporteResumen(ReporteClienteMora reporte, Integer page,
            Integer pageSize) {

        ReporteClienteMora rep = new ReporteClienteMora();

        Map params = ReporteClienteFilter.build(reporte, page, pageSize);

        HttpURLConnection conn = GET(Resource.REPORTE_CLIENTE.url,
                ContentType.JSON, params);
      
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ReporteClienteMora.class);
        
            rep = (ReporteClienteMora) response.getPayload();
       
            conn.disconnect();
        }
        return rep;
    }
    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        REPORTE_CLIENTE("Reporte mora", "reporte/cliente-mora");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
