package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para reporte de proveedores operativos por comprador.
 *
 * @author alext
 */
public class ProveedoresOperativosPorComprador {

    /**
     * Identificador de proveedor.
     */
    private Integer idProveedor;
    /**
     * Proveedor.
     */
    private String proveedor;
    /**
     * Identificador de comprador.
     */
    private Integer idComprador;
    /**
     * Comprador.
     */
    private String comprador;
    /**
     * Tipo de tarifa.
     */
    private String tipoTarifa;
    /**
     * Tarifa.
     */
    private String tarifa;
    /**
     * Monto de tarifa.
     */
    private Integer montoTarifa;
    /**
     * Monto de descuento.
     */
    private Integer montoDescuento;
    /**
     * Monto total.
     */
    private Integer montoTotal;
    /**
     * Cantidad de documentos enviados.
     */
    private Integer cantDocEnviados;
    /**
     * Cantidad de documentos recibidos.
     */
    private Integer cantDocRecibidos;
    /**
     * Año
     */
    private String ano;
    /**
     * Mes
     */
    private String mes;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public Integer getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(Integer montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public Integer getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(Integer montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public Integer getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Integer montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }
    
    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    //</editor-fold>

    /**
     * Constructor.
     */
    public ProveedoresOperativosPorComprador() {

    }

    public ProveedoresOperativosPorComprador(Integer idComprador, String ano, String mes) {
        this.idComprador = idComprador;
        this.ano = ano;
        this.mes = mes;
    }

    /**
     * Constructor con parámetros.
     *
     * @param idProveedor
     * @param proveedor
     * @param idComprador
     * @param comprador
     * @param tipoTarifa
     * @param tarifa
     * @param montoTarifa
     * @param montoDescuento
     * @param montoTotal
     * @param cantDocEnviados
     * @param cantDocRecibidos
     * @param ano
     * @param mes
     */
    public ProveedoresOperativosPorComprador(Integer idProveedor, String proveedor, Integer idComprador,
            String comprador, String tipoTarifa, String tarifa, Integer montoTarifa, Integer montoDescuento,
            Integer montoTotal, Integer cantDocEnviados, Integer cantDocRecibidos, String ano, String mes) {
        this.idProveedor = idProveedor;
        this.proveedor = proveedor;
        this.idComprador = idComprador;
        this.comprador = comprador;
        this.tipoTarifa = tipoTarifa;
        this.tarifa = tarifa;
        this.montoTarifa = montoTarifa;
        this.montoDescuento = montoDescuento;
        this.montoTotal = montoTotal;
        this.cantDocEnviados = cantDocEnviados;
        this.cantDocRecibidos = cantDocRecibidos;
        this.ano = ano;
        this.mes = mes;
    }

}
