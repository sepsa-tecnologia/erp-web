package py.com.sepsa.erp.web.v1.factura.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ConceptoCobro;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * POJO para detalle del cobro
 *
 * @author Cristina Insfrán
 */
public class CobroDetalle implements Serializable {

    /**
     * Identificador del cobro
     */
    private Integer id;
    /**
     * Identificador de cobro
     */
    private Integer idCobro;
    /**
     * Nro de linea
     */
    private Integer nroLinea;

    /**
     * Identificador de factura
     */
    private Integer idFactura;

    /**
     * Identificador de tipo de cobro
     */
    private Integer idTipoCobro;

    /**
     * Identificador de cheque
     */
    private Integer idCheque;

    /**
     * Monto de cobro
     */
    private BigDecimal montoCobro;
    
    /**
     * Monto de cobro
     */
    private BigDecimal montoCobroGuaranies;

    /**
     * Identificador de concepto de cobro
     */
    private Integer idConceptoCobro;
    /**
     * Identificador de la entidad financiera
     */
    private Integer idEntidadFinanciera;

    /**
     * Número de recibo
     */
    private String nroRecibo;
    
    /**
     * Número de recibo
     */
    private String descripcion;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Identificador de lugar de cobro
     */
    private Integer idLugarCobro;
    /**
     * Lugar de cobro
     */
    private String lugarCobro;
    /**
     * Tipo de cobro
     */
    private TipoCobro tipoCobro;
    /**
     * Factura
     */
    private Factura factura;

    /**
     * Nro cheque
     */
    private String nroCheque;
    /**
     * Fecha de pago
     */
    private Date fechaPago;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Codigo Concepto Cobro
     */
    private String codigoConceptoCobro;
    /**
     * Objeto Cheque
     */
    private Cheque cheque;
    /**
     * Código estado
     */
    private String codigoEstado;
    /**
     * Codigo tipo cobro
     */
    private String codigoTipoCobro;
    /**
     * Parámetro conceptoCobro
     */
    private ConceptoCobro conceptoCobro;
    private EntidadFinanciera entidadFinanciera;
    
    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public BigDecimal getMontoCobroGuaranies() {
        return montoCobroGuaranies;
    }

    public void setMontoCobroGuaranies(BigDecimal montoCobroGuaranies) {
        this.montoCobroGuaranies = montoCobroGuaranies;
    }
    
    /**
     * @return the conceptoCobro
     */
    public ConceptoCobro getConceptoCobro() {
        return conceptoCobro;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    /**
     * @param conceptoCobro the conceptoCobro to set
     */
    public void setConceptoCobro(ConceptoCobro conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }

    /**
     * @return the tipoCobro
     */
    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    /**
     * @param tipoCobro the tipoCobro to set
     */
    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the codigoConceptoCobro
     */
    public String getCodigoConceptoCobro() {
        return codigoConceptoCobro;
    }

    /**
     * @param codigoConceptoCobro the codigoConceptoCobro to set
     */
    public void setCodigoConceptoCobro(String codigoConceptoCobro) {
        this.codigoConceptoCobro = codigoConceptoCobro;
    }

    /**
     * @return the codigoTipoCobro
     */
    public String getCodigoTipoCobro() {
        return codigoTipoCobro;
    }

    /**
     * @param codigoTipoCobro the codigoTipoCobro to set
     */
    public void setCodigoTipoCobro(String codigoTipoCobro) {
        this.codigoTipoCobro = codigoTipoCobro;
    }

    /**
     * @return the codigoEstado
     */
    public String getCodigoEstado() {
        return codigoEstado;
    }

    /**
     * @param codigoEstado the codigoEstado to set
     */
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    /**
     * @return the idEntidadFinanciera
     */
    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    /**
     * @param idEntidadFinanciera the idEntidadFinanciera to set
     */
    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    /**
     * @return the cheque
     */
    public Cheque getCheque() {
        return cheque;
    }

    /**
     * @param cheque the cheque to set
     */
    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    /**
     * @return the nroRecibo
     */
    public String getNroRecibo() {
        return nroRecibo;
    }

    /**
     * @param nroRecibo the nroRecibo to set
     */
    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the idLugarCobro
     */
    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    /**
     * @param idLugarCobro the idLugarCobro to set
     */
    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    /**
     * @return the lugarCobro
     */
    public String getLugarCobro() {
        return lugarCobro;
    }

    /**
     * @param lugarCobro the lugarCobro to set
     */
    public void setLugarCobro(String lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    /**
     * @return the nroCheque
     */
    public String getNroCheque() {
        return nroCheque;
    }

    /**
     * @param nroCheque the nroCheque to set
     */
    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    /**
     * @return the idCobro
     */
    public Integer getIdCobro() {
        return idCobro;
    }

    /**
     * @param idCobro the idCobro to set
     */
    public void setIdCobro(Integer idCobro) {
        this.idCobro = idCobro;
    }

    /**
     * @return the nroLinea
     */
    public Integer getNroLinea() {
        return nroLinea;
    }

    /**
     * @param nroLinea the nroLinea to set
     */
    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    /**
     * @return the idFactura
     */
    public Integer getIdFactura() {
        return idFactura;
    }

    /**
     * @param idFactura the idFactura to set
     */
    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    /**
     * @return the idTipoCobro
     */
    public Integer getIdTipoCobro() {
        return idTipoCobro;
    }

    /**
     * @param idTipoCobro the idTipoCobro to set
     */
    public void setIdTipoCobro(Integer idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    /**
     * @return the idCheque
     */
    public Integer getIdCheque() {
        return idCheque;
    }

    /**
     * @param idCheque the idCheque to set
     */
    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    /**
     * @return the montoCobro
     */
    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    /**
     * @param montoCobro the montoCobro to set
     */
    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    /**
     * @return the idConceptoCobro
     */
    public Integer getIdConceptoCobro() {
        return idConceptoCobro;
    }

    /**
     * @param idConceptoCobro the idConceptoCobro to set
     */
    public void setIdConceptoCobro(Integer idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }
//</editor-fold>

    /**
     * Constructor
     */
    public CobroDetalle() {

    }

    public CobroDetalle(Integer idCobro, String nroRecibo, Date fecha,
            Integer idLugarCobro, String lugarCobro, Integer nroLinea,
            Integer idFactura, Integer idTipoCobro, TipoCobro tipoCobro,
            Integer idCheque, String nroCheque, Date fechaPago,
            BigDecimal montoCobro, Estado estado, Integer idConceptoCobro,
            ConceptoCobro conceptoCobro, Factura factura) {
        this.idCobro = idCobro;
        this.nroRecibo = nroRecibo;
        this.fecha = fecha;
        this.idLugarCobro = idLugarCobro;
        this.lugarCobro = lugarCobro;
        this.nroLinea = nroLinea;
        this.idFactura = idFactura;
        this.idTipoCobro = idTipoCobro;
        this.tipoCobro = tipoCobro;
        this.idCheque = idCheque;
        this.nroCheque = nroCheque;
        this.fechaPago = fechaPago;
        this.montoCobro = montoCobro;
        this.factura = factura;
        this.estado = estado;
        this.idConceptoCobro = idConceptoCobro;
        this.conceptoCobro = conceptoCobro;
    }

}
