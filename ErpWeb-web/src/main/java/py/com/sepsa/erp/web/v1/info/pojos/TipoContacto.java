
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de tipo contacto
 * @author Cristina Insfrán
 */
public class TipoContacto {
    /**
     * Identificador del tipo contacto
     */
    private Integer id;
    /**
     * Descripcion del tipo contacto
     */
    private String descripcion;
     /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Constructor
     */
    public TipoContacto(){
        
    }
    
    /**
     * Constructor del tipo contacto
     * @param id
     * @param descripcion
     */
    public TipoContacto(Integer id, String descripcion){
        this.id=id;
        this.descripcion=descripcion;
    }
    
}
