package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaContactoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de personaContacto
 *
 * @author Sergio D. Riveros Vazquez
 */
public class PersonaContactoServiceClient extends APIErpCore {

    /**
     * Método para crear
     *
     * @param personaContacto
     * @return
     */
    public BodyResponse<PersonaContacto> setPersonaContacto(PersonaContacto personaContacto) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERSONA_CONTACTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaContacto));

            response = BodyResponse.createInstance(conn, PersonaContacto.class);

        }
        return response;
    }

    /**
     * Método para crear/editar paesona
     *
     * @param personaContacto
     * @return
     */
    public BodyResponse<PersonaContacto> putPersonaContacto(PersonaContacto personaContacto) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PERSONA_CONTACTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaContacto));

            response = BodyResponse.createInstance(conn, PersonaContacto.class);

        }
        return response;
    }

    /**
     * Obtiene la lista de personaContactos
     *
     * @param personaContacto Objeto cliente
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaContactoListAdapter getPersonaContactoList(PersonaContacto personaContacto, Integer page,
            Integer pageSize) {

        PersonaContactoListAdapter lista = new PersonaContactoListAdapter();

        Map params = PersonaContactoFilter.build(personaContacto, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_CONTACTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaContactoListAdapter.class);

            lista = (PersonaContactoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de PersonaContactoServiceClient
     */
    public PersonaContactoServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        PERSONA_CONTACTO("Servicio para PersonaContacto", "persona-contacto");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
