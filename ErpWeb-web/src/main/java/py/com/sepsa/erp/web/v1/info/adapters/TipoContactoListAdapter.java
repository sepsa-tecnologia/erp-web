package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;
import py.com.sepsa.erp.web.v1.info.remote.TipoContactoService;

/**
 * Adaptador para la lista de tipo contacto
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class TipoContactoListAdapter extends DataListAdapter<TipoContacto> {

    /**
     * Cliente para el servicio de tipo contacto
     */
    private final TipoContactoService tipoContactoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoContactoListAdapter fillData(TipoContacto searchData) {

        return tipoContactoClient.getTipoContactoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoContactoListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoContactoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoContactoClient = new TipoContactoService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public TipoContactoListAdapter() {
        super();
        this.tipoContactoClient = new TipoContactoService();
    }
}
