
package py.com.sepsa.erp.web.v1.system.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.system.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.system.filters.MenuFilter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;

/**
 * Cliente remoto para Menu
 * @author Cristina Insfrán
 */
public class MenuServiceClient extends APIErpCore {
    
     /**
     * Obtiene la lista de menus
     *
     * @param menu
     * @param page
     * @param pageSize
     * @return
     */
    public MenuListAdapter getMenuList(Menu menu, Integer page,
            Integer pageSize) {
       
        MenuListAdapter lista = new MenuListAdapter();

        Map params = MenuFilter.build(menu, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.LISTA_MENU.url, 
                ContentType.JSON, params);
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MenuListAdapter.class);

            lista = (MenuListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {
        
        //Servicios
        LISTA_MENU("Lista Menu", "menu");
        
        /**
         * Nombre del recurso
         */
        private String resource;
        
        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }
        
        /**
         * Constructor de Resource
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
