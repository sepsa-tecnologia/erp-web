/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.comercial.adapters;

import java.util.List;
import java.util.Map;
import org.primefaces.model.SortOrder;
import py.com.sepsa.erp.web.v1.adapters.AbstractDataModel;
import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;

public class ClienteDataModel extends AbstractDataModel<Cliente, Cliente> {

    /**
     * Cliente para los servicios de clientes
     */
    private final ClientServiceClient service;
    
    @Override
    public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        DataListAdapter<Cliente> result = callService(filtro, first, pageSize);
        
        setRowCount(new Integer(result.getTotalSize() + ""));
        
        return result.getData();
    }
    
    public ClienteDataModel() {
        this.service = new ClientServiceClient();
        this.filtro = new Cliente();
    }

    @Override
    public DataListAdapter<Cliente> callService(Cliente filtro, Integer first, Integer pageSize) {
        return service.getClientList(filtro, first, pageSize);
    }
}
