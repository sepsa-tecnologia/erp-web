/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.info.remote.EstadoService;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para la vista editar estado
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("estadoEdit")

public class EstadoEditController implements Serializable {

    /**
     * Cliente para el servicio estado.
     */
    private final EstadoService service;
    /**
     * POJO del estado
     */
    private Estado estado;
    /**
     * Adaptador de la lista de tipo estados
     */
    private TipoEstadoAdapter tipoEstadoAdapter;
    /**
     * Datos del cliente
     */
    private TipoEstado tipoEstado;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public TipoEstadoAdapter getTipoEstadoAdapter() {
        return tipoEstadoAdapter;
    }

    public void setTipoEstadoAdapter(TipoEstadoAdapter tipoEstadoAdapter) {
        this.tipoEstadoAdapter = tipoEstadoAdapter;
    }

    public TipoEstado getTipoEstado() {
        return tipoEstado;
    }

    public void setTipoEstado(TipoEstado tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

    //</editor-fold>
    /**
     * Método para editar Estado
     */
    public void edit() {
        BodyResponse<Estado> respuestaProducto = service.editEstado(this.estado);
        if (respuestaProducto.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Estado editado correctamente!"));
        } else {
            for (Mensaje mensaje : respuestaProducto.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }
        }
    }

    /**
     * Método para obtener las tipoEstados
     */
    public void getTipoEstados() {
        this.setTipoEstadoAdapter(getTipoEstadoAdapter().fillData(getTipoEstado()));
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.estado = service.get(id);
        this.tipoEstadoAdapter = new TipoEstadoAdapter();
        this.tipoEstado = new TipoEstado();
        getTipoEstados();
    }

    /**
     * Constructor
     */
    public EstadoEditController() {
        this.service = new EstadoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
