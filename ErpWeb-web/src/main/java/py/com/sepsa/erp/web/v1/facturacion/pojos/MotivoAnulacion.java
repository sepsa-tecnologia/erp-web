
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * POJOS para la lista de motivo anulación
 * @author Cristina Insfrán
 */
public class MotivoAnulacion {


    /**
     * Identificador
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Código 
     */
    private String codigo;
    /**
     * Identificador del tipo motivo anulación
     */
    private Integer idTipoMotivoAnulacion;
    /**
     * Código tipo motivo anulación
     */
    private String codigoTipoMotivoAnulacion;
    /**
     * tipo motivo anulacion
     */
    private TipoMotivoAnulacion tipoMotivoAnulacion;
    /**
     * activo 
     */
    private Character activo;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setTipoMotivoAnulacion(TipoMotivoAnulacion tipoMotivoAnulacion) {
        this.tipoMotivoAnulacion = tipoMotivoAnulacion;
    }

    public TipoMotivoAnulacion getTipoMotivoAnulacion() {
        return tipoMotivoAnulacion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the activo
     */
    public Character getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(Character activo) {
        this.activo = activo;
    }

  
    /**
     * @return the idTipoMotivoAnulacion
     */
    public Integer getIdTipoMotivoAnulacion() {
        return idTipoMotivoAnulacion;
    }
    
    /**
     * @param idTipoMotivoAnulacion the idTipoMotivoAnulacion to set
     */
    public void setIdTipoMotivoAnulacion(Integer idTipoMotivoAnulacion) {
        this.idTipoMotivoAnulacion = idTipoMotivoAnulacion;
    }
    
    
    /**
     * @return the codigoTipoMotivoAnulacion
     */
    public String getCodigoTipoMotivoAnulacion() {
        return codigoTipoMotivoAnulacion;
    }

    /**
     * @param codigoTipoMotivoAnulacion the codigoTipoMotivoAnulacion to set
     */
    public void setCodigoTipoMotivoAnulacion(String codigoTipoMotivoAnulacion) {
        this.codigoTipoMotivoAnulacion = codigoTipoMotivoAnulacion;
    }
  
//</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public MotivoAnulacion(){
        
    }
}
