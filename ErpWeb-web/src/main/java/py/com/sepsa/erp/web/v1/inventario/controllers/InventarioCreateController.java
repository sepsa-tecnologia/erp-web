package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.pojos.Inventario;
import py.com.sepsa.erp.web.v1.inventario.remote.InventarioService;

/**
 * Controlador para creación de inventario
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("inventarioCreate")
public class InventarioCreateController implements Serializable {

    /**
     * Cliente para el servicio de inventario
     */
    private final InventarioService service;
    /**
     * POJO del motivoEmision
     */
    private Inventario inventario;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Inventario getInventario() {
        return inventario;
    }

    public void setTipoMotivo(Inventario inventario) {
        this.inventario = inventario;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {

        BodyResponse<Inventario> respuestaTal = 
                service.setInventario(inventario);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Inventario creado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.inventario = new Inventario();
    }

    /**
     * Constructor
     */
    public InventarioCreateController() {
        init();
        this.service = new InventarioService();
    }

}
