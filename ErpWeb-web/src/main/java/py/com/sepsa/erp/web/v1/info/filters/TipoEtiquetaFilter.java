package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;

/**
 * Filtro para el servicio de tipo etiqueta
 *
 * @author Romina Núñez
 */
public class TipoEtiquetaFilter extends Filter {

    /**
     * Agrega el filtro de identificador del tipo etiqueta
     *
     * @param id
     * @return
     */
    public TipoEtiquetaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de código
     *
     * @param codigo
     * @return
     */
    public TipoEtiquetaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoEtiquetaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de configuración aprobación
     *
     * @param configAprobacion
     * @return
     */
    public TipoEtiquetaFilter configAprobacion(Character configAprobacion) {
        if (configAprobacion != null) {
            params.put("configAprobacion", configAprobacion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoEtiqueta datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoEtiqueta tipoEtiqueta, Integer page, Integer pageSize) {
        TipoEtiquetaFilter filter = new TipoEtiquetaFilter();

        filter
                .id(tipoEtiqueta.getId())
                .codigo(tipoEtiqueta.getCodigo())
                .descripcion(tipoEtiqueta.getDescripcion())
                .configAprobacion(tipoEtiqueta.getConfigAprobacion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor TipoTelefonoFilter
     */
    public TipoEtiquetaFilter() {
        super();
    }

}
