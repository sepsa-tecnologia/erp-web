package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClienteProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ClienteProductoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteProducto;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Contrato
 *
 * @author Romina Núñez
 */
public class ClienteProductoService extends APIErpComercial {

    /**
     * Obtiene la lista de descuentos
     *
     * @param clienteProducto 
     * @param page
     * @param pageSize
     * @return
     */
    public ClienteProductoAdapter getClienteProductoList(ClienteProducto clienteProducto, Integer page,
            Integer pageSize) {

        ClienteProductoAdapter lista = new ClienteProductoAdapter();

        Map params = ClienteProductoFilter.build(clienteProducto, page, pageSize);

        HttpURLConnection conn = GET(Resource.CLIENTEPRODUCTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ClienteProductoAdapter.class);

            lista = (ClienteProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
    /**
     * Constructor de ContratoService
     */
    public ClienteProductoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        CLIENTEPRODUCTO("ClienteProducto", "cliente-producto/asociado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
