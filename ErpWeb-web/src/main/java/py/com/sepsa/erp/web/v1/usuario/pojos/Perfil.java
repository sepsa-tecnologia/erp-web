/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.pojos;

import com.google.gson.JsonElement;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * POJO de Perfiles
 *
 * @author Sepsa
 */
public class Perfil {

    /**
     * Identificador del perfil
     */
    private Integer id;

    /**
     * Código
     */
    private String codigo;

    /**
     * Descripción del perfil
     */
    private String descripcion;

    /**
     * Pojo para empresa
     */
    private Empresa empresa;

    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Campo activo
     */
    private String activo;
    
    /**
     * Permisos
     */
    private JsonElement permisos;
    
    /**
     * Get y Set
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public JsonElement getPermisos() {
        return permisos;
    }

    public void setPermisos(JsonElement permisos) {
        this.permisos = permisos;
    }
    
    public Perfil() {
    }

}
