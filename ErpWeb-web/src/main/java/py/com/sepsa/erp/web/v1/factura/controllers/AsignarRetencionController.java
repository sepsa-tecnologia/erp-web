package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para Asignar Retención
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("asignarRetencion")
public class AsignarRetencionController implements Serializable {

    /**
     * Objeto Cliente
     */
    private Cliente client;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Variable de control
     */
    private boolean show;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaMontoAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaConRetencionFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaMontoAdapter adapterFacturaConRetencion;
    /**
     * Objeto Retención
     */
    private Retencion retencion;
    /**
     * Objeto retención detalle
     */
    private RetencionDetalle retencionDetalle;
    /**
     * Fecha de la factura
     */
    private Date fechaFactura;
  
    /**
     * Adaptador de la lista retención detalle
     */
    private RetencionDetalleListAdapter adapterRetencionDetalle;
    /**
     * Objeto cobro detalle
     */
    private CobroDetalle cobroDetalle;
    /**
     * Adaptador de la lista de cobro detalle
     */
    private CobroDetalleListAdapter adapterCobroDetalle;
    /**
     * Objeto detalle nota de credito
     */
    private NotaCreditoDetalle notaCreditoDetalle;
    /**
     * Adaptador de la lista del detalle de nota de credito
     */
    private NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle;
    /**
     * Cliente para retención
     */
    private RetencionService retencionClient;
    /**
     * Variable para guardar el valor de nro de factura
     */
    private String nroFactura;
  

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    

    /**
     * @return the adapterFactura
     */
    public FacturaMontoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    /**
     * @param adapterFactura the adapterFactura to set
     */
    public void setAdapterFactura(FacturaMontoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    /**
     * @return the adapterFacturaConRetencion
     */
    public FacturaMontoAdapter getAdapterFacturaConRetencion() {
        return adapterFacturaConRetencion;
    }

    /**
     * @param adapterFacturaConRetencion the adapterFacturaConRetencion to set
     */
    public void setAdapterFacturaConRetencion(FacturaMontoAdapter adapterFacturaConRetencion) {
        this.adapterFacturaConRetencion = adapterFacturaConRetencion;
    }

    /**
     * @return the retencionClient
     */
    public RetencionService getRetencionClient() {
        return retencionClient;
    }

    /**
     * @param retencionClient the retencionClient to set
     */
    public void setRetencionClient(RetencionService retencionClient) {
        this.retencionClient = retencionClient;
    }

    /**
     * @return the adapterNotaCreditoDetalle
     */
    public NotaCreditoDetalleListAdapter getAdapterNotaCreditoDetalle() {
        return adapterNotaCreditoDetalle;
    }

    /**
     * @param adapterNotaCreditoDetalle the adapterNotaCreditoDetalle to set
     */
    public void setAdapterNotaCreditoDetalle(NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle) {
        this.adapterNotaCreditoDetalle = adapterNotaCreditoDetalle;
    }

    /**
     * @return the notaCreditoDetalle
     */
    public NotaCreditoDetalle getNotaCreditoDetalle() {
        return notaCreditoDetalle;
    }

    /**
     * @param notaCreditoDetalle the notaCreditoDetalle to set
     */
    public void setNotaCreditoDetalle(NotaCreditoDetalle notaCreditoDetalle) {
        this.notaCreditoDetalle = notaCreditoDetalle;
    }

    /**
     * @return the adapterCobroDetalle
     */
    public CobroDetalleListAdapter getAdapterCobroDetalle() {
        return adapterCobroDetalle;
    }

    /**
     * @param adapterCobroDetalle the adapterCobroDetalle to set
     */
    public void setAdapterCobroDetalle(CobroDetalleListAdapter adapterCobroDetalle) {
        this.adapterCobroDetalle = adapterCobroDetalle;
    }

    /**
     * @return the cobroDetalle
     */
    public CobroDetalle getCobroDetalle() {
        return cobroDetalle;
    }

    /**
     * @param cobroDetalle the cobroDetalle to set
     */
    public void setCobroDetalle(CobroDetalle cobroDetalle) {
        this.cobroDetalle = cobroDetalle;
    }

    /**
     * @return the adapterRetencionDetalle
     */
    public RetencionDetalleListAdapter getAdapterRetencionDetalle() {
        return adapterRetencionDetalle;
    }

    /**
     * @param adapterRetencionDetalle the adapterRetencionDetalle to set
     */
    public void setAdapterRetencionDetalle(RetencionDetalleListAdapter adapterRetencionDetalle) {
        this.adapterRetencionDetalle = adapterRetencionDetalle;
    }

    /**
     * @return the fechaFactura
     */
    public Date getFechaFactura() {
        return fechaFactura;
    }

    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    /**
     * @return the retencionDetalle
     */
    public RetencionDetalle getRetencionDetalle() {
        return retencionDetalle;
    }

    /**
     * @param retencionDetalle the retencionDetalle to set
     */
    public void setRetencionDetalle(RetencionDetalle retencionDetalle) {
        this.retencionDetalle = retencionDetalle;
    }

    /**
     * @return the retencion
     */
    public Retencion getRetencion() {
        return retencion;
    }

    /**
     * @param retencion the retencion to set
     */
    public void setRetencion(Retencion retencion) {
        this.retencion = retencion;
    }

    /**
     * @return the facturaConRetencionFilter
     */
    public Factura getFacturaConRetencionFilter() {
        return facturaConRetencionFilter;
    }

    /**
     * @param facturaConRetencionFilter the facturaConRetencionFilter to set
     */
    public void setFacturaConRetencionFilter(Factura facturaConRetencionFilter) {
        this.facturaConRetencionFilter = facturaConRetencionFilter;
    }

    /**
     * @return the facturaFilter
     */
    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    /**
     * @param facturaFilter the facturaFilter to set
     */
    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    /**
     * @return the show
     */
    public boolean isShow() {
        return show;
    }

    /**
     * @param show the show to set
     */
    public void setShow(boolean show) {
        this.show = show;
    }

    /**
     * @return the client
     */
    public Cliente getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Cliente client) {
        this.client = client;
    }

    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

//</editor-fold>
    /**
     * Método que selecciona una factura
     *
     */
    public void selectFactura() {
        retencion = new Retencion();

        fechaFactura = facturaFilter.getFecha();
        nroFactura=facturaFilter.getNroFactura();
        retencion.setIdCliente(client.getIdCliente());
        
        //retencion.setNroRetencion("");
        retencion.setPorcentajeRetencion(client.getPorcentajeRetencion());

        //retencion.setFecha(Calendar.getInstance().getTime());
        retencion.setMontoRetenidoTotal(BigDecimal.ZERO);
        retencion.setMontoImponibleTotal(facturaFilter.getMontoImponibleTotal());
        retencion.setMontoIvaTotal(facturaFilter.getMontoIvaTotal());
        retencion.setMontoTotal(facturaFilter.getMontoTotalFactura());

        retencion.setCodigoEstado("CONFIRMADO");

        retencion.setRetencionDetalles(new ArrayList<>());
        retencionDetalle = new RetencionDetalle();

        retencionDetalle.setIdFactura(facturaFilter.getId());
        retencionDetalle.setMontoImponible(facturaFilter.getMontoImponibleTotal());
        retencionDetalle.setMontoIva(facturaFilter.getMontoIvaTotal());
        retencionDetalle.setMontoTotal(facturaFilter.getMontoTotalFactura());
        retencionDetalle.setMontoRetenido(facturaFilter.getMontoRetencion());

        retencionDetalle.setCodigoEstado("CONFIRMADO");

    }

    /**
     * Método para ver el detalle de cobro y retencion
     *
     * @param factura
     */
    public void viewCobroDet(Factura factura) {
        setNroFactura(factura.getNroFactura());
        retencionDetalle.setIdFactura(factura.getId());
        adapterRetencionDetalle.setPageSize(adapterRetencionDetalle.getPageSize());
        adapterRetencionDetalle.setFirstResult(0);
        adapterRetencionDetalle = adapterRetencionDetalle.fillData(retencionDetalle);

        cobroDetalle.setIdFactura(factura.getId());
        adapterCobroDetalle.setPageSize(adapterCobroDetalle.getPageSize());
        adapterCobroDetalle.setFirstResult(0);
        adapterCobroDetalle = adapterCobroDetalle.fillData(cobroDetalle);

        notaCreditoDetalle.setIdFactura(factura.getId());
        adapterNotaCreditoDetalle.setPageSize(adapterNotaCreditoDetalle.getPageSize());
        adapterNotaCreditoDetalle.setFirstResult(0);
        adapterNotaCreditoDetalle = adapterNotaCreditoDetalle.fillData(notaCreditoDetalle);

    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }

    /**
     * Método para seleccionar el cliente origen
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
        show = true;
        client = new Cliente();

        client.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        adapterCliente = adapterCliente.fillData(client);
        client = adapterCliente.getData().get(0);

        filterFacturasPendientes();
        filterFacturasConRetencion();

    }

    /**
     * Método para filtras las facturas pendientes
     */
    public void filterFacturasPendientes() {
        this.facturaFilter = new Factura();
        facturaFilter.setIdCliente(client.getIdCliente());
        facturaFilter.setTieneRetencion("false");
        facturaFilter.setTieneSaldo("true");
        facturaFilter.setCobrado("N");
        facturaFilter.setAnulado("N");

        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    /**
     * Método para filtrar las facturas con retencion
     */
    public void filterFacturasConRetencion() {
        this.facturaConRetencionFilter = new Factura();
        facturaConRetencionFilter.setIdCliente(client.getIdCliente());
        facturaConRetencionFilter.setTieneRetencion("true");
        //facturaConRetencionFilter.setTieneSaldo("false");

        adapterFacturaConRetencion = adapterFacturaConRetencion.fillData(facturaConRetencionFilter);

    }

    /**
     * Método para agregar retención
     */
    public void addRetencion() {

        retencion.getRetencionDetalles().add(retencionDetalle);

        BodyResponse response = retencionClient.setRetencion(retencion);

        if (response.getSuccess() != null) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Retención asignada correctamente!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            filterFacturasConRetencion();
        }
    }


    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.facturaFilter = new Factura();
        this.adapterFactura = new FacturaMontoAdapter();
        this.facturaConRetencionFilter = new Factura();
        this.adapterFacturaConRetencion = new FacturaMontoAdapter();
        this.retencion = new Retencion();
        this.retencionDetalle = new RetencionDetalle();
        this.adapterRetencionDetalle = new RetencionDetalleListAdapter();
        this.cobroDetalle = new CobroDetalle();
        this.adapterCobroDetalle = new CobroDetalleListAdapter();
        this.notaCreditoDetalle = new NotaCreditoDetalle();
        this.adapterNotaCreditoDetalle = new NotaCreditoDetalleListAdapter();
        this.retencionClient = new RetencionService();
        

    }
}
