package py.com.sepsa.erp.web.v1.producto.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.comercial.remote.ProductoService;

@ViewScoped
@Named("crearProducto")
public class ProductoCrearController implements Serializable {

    /**
     * Cliente para el servicio de producto.
     */
    private final ProductoService service;
    
    /**
     * Datos del talonario
     */
    private Producto producto;

    /**
     * Getter y Setters.
     */
    public Producto getProducto() {
        return producto;
    }
    
    public void setProducto(Producto producto) {    
        this.producto = producto;
    }

    /**
     * Método para crear un talonario
     */
    public void create() {
        producto = service.create(producto);
        if (producto != null && producto.getId() != null) {
            init();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Producto creado con éxito!", "Producto creado con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear producto!", "Error al crear producto!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.producto = new Producto();
    }

    /**
     * Método para reiniciar el formulario
    
    public void clear() {
        init();
    }*/

    /**
     * Constructor de BookNewController
     */
    public ProductoCrearController() {
        init();
        this.service = new ProductoService();
    }

}
