
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;
import py.com.sepsa.erp.web.v1.info.remote.PersonaRolServiceClient;

/**
 * Adaptador de la lista de Persona Rol
 * @author Romina E. Núñez Rojas
 */
public class PersonaRolAdapter extends DataListAdapter<PersonaRol> {
    
    /**
     * Cliente para el servicio de Rol
     */
    private final PersonaRolServiceClient servicePersonaRol;
 
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public PersonaRolAdapter fillData(PersonaRol searchData) {
     
        return servicePersonaRol.getPersonaRol(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PersonaRolAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.servicePersonaRol = new PersonaRolServiceClient();
    }

    /**
     * Constructor de RolListAdapter
     */
    public PersonaRolAdapter() {
        super();
        this.servicePersonaRol = new PersonaRolServiceClient();
    }
}
