package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Filro utilizado para los clientes del servicio
 *
 * @author Romina Nuñez
 */
public class ClientPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public ClientPojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public ClientPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agrega el filtro de id Estado
     *
     * @param idEstado
     * @return
     */
    public ClientPojoFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por idCanalVenta
     *
     * @param idCanalVenta
     * @return
     */
    public ClientPojoFilter idCanalVenta(Integer idCanalVenta) {
        if (idCanalVenta != null) {
            params.put("idCanalVenta", idCanalVenta);
        }
        return this;
    }

    /**
     * Agrega el filtro para razon social
     *
     * @param razonSocial
     * @return
     */
    public ClientPojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro para canalVenta
     *
     * @param canalVenta
     * @return
     */
    public ClientPojoFilter canalVenta(CanalVenta canalVenta) {
        if (canalVenta != null) {
            params.put("canalVenta", canalVenta);
        }
        return this;
    }

    /**
     * Agrega el filtro para clave Pago
     *
     * @param clavePago
     * @return
     */
    public ClientPojoFilter clavePago(String clavePago) {
        if (clavePago != null && !clavePago.trim().isEmpty()) {
            params.put("clavePago", clavePago);
        }
        return this;
    }

    /**
     * Agrega el filtro para factura Electronica
     *
     * @param factElectronica
     * @return
     */
    public ClientPojoFilter factElectronica(String factElectronica) {
        if (factElectronica != null && !factElectronica.trim().isEmpty()) {
            params.put("factElectronica", factElectronica);
        }
        return this;
    }

    /**
     * Agregar el filtro de id TipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public ClientPojoFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro para el número de documento
     *
     * @param nroDocumento
     * @return
     */
    public ClientPojoFilter nroDocumento(String nroDocumento) {
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            params.put("nroDocumento", nroDocumento);
        }
        return this;
    }

    /**
     * Agrega el filtro para tipo Documento
     *
     * @param tipoDocumento
     * @return
     */
    public ClientPojoFilter tipoDocumento(TipoDocumento tipoDocumento) {
        if (tipoDocumento != null) {
            params.put("tipoDocumento", tipoDocumento);
        }
        return this;
    }

    /**
     * Agregar el filtro de id Persona
     *
     * @param persona
     * @return
     */
    public ClientPojoFilter persona(Persona persona) {
        if (persona != null) {
            params.put("persona", persona);
        }
        return this;
    }

    /**
     * Agrega el filtro para el número de documento
     *
     * @param codigoEstado
     * @return
     */
    public ClientPojoFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param client datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ClientePojo client, Integer page, Integer pageSize) {
        ClientPojoFilter filter = new ClientPojoFilter();

        filter
                .idCliente(client.getIdCliente())
                .idEstado(client.getIdEstado())
                .codigoEstado(client.getCodigoEstado())
                .razonSocial(client.getRazonSocial())
                .idTipoDocumento(client.getIdTipoDocumento())
                .nroDocumento(client.getNroDocumento())
                .listadoPojo(client.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClientFilter
     */
    public ClientPojoFilter() {
        super();
    }
}
