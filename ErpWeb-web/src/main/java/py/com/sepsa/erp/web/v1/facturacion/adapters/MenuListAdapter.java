package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;

/**
 * Adaptador para listado de menú
 *
 * @author alext
 */
public class MenuListAdapter extends DataListAdapter<MenuPojo> {

    /**
     * Cliente para el servicio de menu
     */
    private final MenuService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData datos buscados
     * @return MenuListAdapter
     */
    @Override
    public MenuListAdapter fillData(MenuPojo searchData) {
        return service.getMenuList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de MenuListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MenuListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new MenuService();
    }

    /**
     * Constructor de MenuListAdapter
     */
    public MenuListAdapter() {
        super();
        this.service = new MenuService();
    }

}
