package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;

/**
 * Filter para marca
 *
 * @author Sergio D. Riveros Vazquez & Alexander Triana
 */
public class MarcaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public MarcaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idEmpresa
     *
     * @param idEmpresa
     * @return
     */
    public MarcaFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de producto
     *
     * @param descripcion
     * @return
     */
    public MarcaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo del producto
     *
     * @param codigo
     * @return
     */
    public MarcaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por empresa
     *
     * @param empresa
     * @return
     */
    public MarcaFilter empresa(Empresa empresa) {
        if (empresa != null) {
            params.put("empresa", empresa);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param marca datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Marca marca, Integer page, Integer pageSize) {
        MarcaFilter filter = new MarcaFilter();

        filter
                .id(marca.getId())
                .idEmpresa(marca.getIdEmpresa())
                .descripcion(marca.getDescripcion())
                .codigo(marca.getCodigo())
                .empresa(marca.getEmpresa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public MarcaFilter() {
        super();
    }

}
