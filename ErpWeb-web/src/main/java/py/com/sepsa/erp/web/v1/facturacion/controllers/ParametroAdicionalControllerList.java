/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.factura.pojos.ParametroAdicional;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ParametroAdicionalAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.ParametroAdicionalService;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("listarParametroAdicional")
public class ParametroAdicionalControllerList implements Serializable {
    
    private ParametroAdicional parametroAdicional;
    
    private ParametroAdicional parametroFilter;
    
    private ParametroAdicionalAdapter parametroAdicionalAdapter;
    
    private ParametroAdicionalService parametroAdicionaService;
    
    private Empresa empresaAutocomplete;
    
    private EmpresaAdapter empresaAdapter;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public ParametroAdicionalService getParametroAdicionaService() {
        return parametroAdicionaService;
    }

    public void setParametroAdicionaService(ParametroAdicionalService parametroAdicionaService) {
        this.parametroAdicionaService = parametroAdicionaService;
    }
    
    public ParametroAdicional getParametroAdicional() {
        return parametroAdicional;
    }

    public void setParametroAdicional(ParametroAdicional parametroAdicional) {
        this.parametroAdicional = parametroAdicional;
    }

    public ParametroAdicional getParametroFilter() {
        return parametroFilter;
    }

    public void setParametroFilter(ParametroAdicional parametroFilter) {
        this.parametroFilter = parametroFilter;
    }

    public ParametroAdicionalAdapter getParametroAdicionalAdapter() {
        return parametroAdicionalAdapter;
    }

    public void setParametroAdicionalAdapter(ParametroAdicionalAdapter parametroAdicionalAdapter) {
        this.parametroAdicionalAdapter = parametroAdicionalAdapter;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }
    //</editor-fold>
    
    /**
     * Método para filtrar listado de parametros
     *
     */
    public void filter() {
        parametroAdicionalAdapter = parametroAdicionalAdapter.fillData(parametroFilter);
    }
    
    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        parametroFilter = new ParametroAdicional();
        empresaAutocomplete = new Empresa();
        filter();
    }
    
    
    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }
    
    
    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        parametroFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }
    
    
    /**
     * Método para cambiar el estado del usuario
     *
     * @param parametro
     */
    public void onChangeStateConfig(ParametroAdicional parametro) {

        ParametroAdicional paramEdit = new ParametroAdicional();

        switch (parametro.getActivo()) {
            case "S":
                paramEdit.setActivo("N");
                break;
            case "N":
                paramEdit.setActivo("S");
                break;
            default:
                break;
        }

        paramEdit.setId(parametro.getId());
        paramEdit.setCodigo(parametro.getCodigo());
        paramEdit.setIdEmpresa(parametro.getIdEmpresa());
        paramEdit.setDescripcion(parametro.getDescripcion());

        paramEdit = parametroAdicionaService.editParametroAdicional(paramEdit);

        if (paramEdit != null) {
            filter();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        }
    }
    
    @PostConstruct
    public void init() {
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        this.parametroAdicional = new ParametroAdicional();
        this.parametroFilter = new ParametroAdicional();
        this.parametroAdicionalAdapter = new ParametroAdicionalAdapter();
        filter();
    }
    
}
