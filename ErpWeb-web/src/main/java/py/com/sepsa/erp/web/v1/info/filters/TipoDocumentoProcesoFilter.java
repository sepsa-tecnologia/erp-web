/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDocumentoProceso;

/**
 * Filter para tipo documento proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoProcesoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoDocumentoProcesoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoDocumentoProcesoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agregar el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoDocumentoProcesoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de archivo
     *
     * @param idTipoArchivo
     * @return
     */
    public TipoDocumentoProcesoFilter idTipoArchivo(Integer idTipoArchivo) {
        if (idTipoArchivo != null) {
            params.put("idTipoArchivo", idTipoArchivo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoDocumentoProceso datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoDocumentoProceso tipoDocumentoProceso, Integer page, Integer pageSize) {
        TipoDocumentoProcesoFilter filter = new TipoDocumentoProcesoFilter();

        filter
                .id(tipoDocumentoProceso.getId())
                .descripcion(tipoDocumentoProceso.getDescripcion())
                .codigo(tipoDocumentoProceso.getCodigo())
                .idTipoArchivo(tipoDocumentoProceso.getIdTipoArchivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoDocumentoProcesoFilter() {
        super();
    }

}
