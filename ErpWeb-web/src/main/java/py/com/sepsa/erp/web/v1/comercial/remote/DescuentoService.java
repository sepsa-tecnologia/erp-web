package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.DescuentoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.TipoDescuentoListAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.DescuentoFilter;
import py.com.sepsa.erp.web.v1.comercial.filters.TipoDescuentoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoDescuento;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Instalaciones
 *
 * @author Romina Núñez
 */
public class DescuentoService extends APIErpComercial {

    /**
     * Obtiene la lista de descuentos
     *
     * @param descuento
     * @param page
     * @param pageSize
     * @return
     */
    public DescuentoAdapter getDescuentoList(Descuento descuento, Integer page,
            Integer pageSize) {

        DescuentoAdapter lista = new DescuentoAdapter();

        Map params = DescuentoFilter.build(descuento, page, pageSize);

        HttpURLConnection conn = GET(Resource.DESCUENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DescuentoAdapter.class);

            lista = (DescuentoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear Descuento
     *
     * @param descuento
     * @return
     */
    public BodyResponse<Descuento> setDescuento(Descuento descuento) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.DESCUENTO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(descuento));
            response = BodyResponse.createInstance(conn, Descuento.class);
        }
        return response;
    }

    /**
     * Método para aprobar Descuento
     *
     * @param descuento
     * @return
     */
    public Descuento aprobarDescuento(Descuento descuento) {

        Descuento desc = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.APROBARDESCUENTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(descuento));

            response = BodyResponse.createInstance(conn, Descuento.class);

            desc = ((Descuento) response.getPayload());

        }
        return desc;
    }

    /**
     * Método para aprobar Descuento
     *
     * @param descuento
     * @return
     */
    public Descuento inactivarDescuento(Descuento descuento) {

        Descuento desc = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.INACTIVAR_DESCUENTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(descuento));

            response = BodyResponse.createInstance(conn, Descuento.class);

            desc = ((Descuento) response.getPayload());

        }
        return desc;
    }

    /**
     * Obtiene la lista de tipo descuento
     *
     * @param tipodescuento
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la página de resultado
     * @return
     */
    public TipoDescuentoListAdapter getTipoDescuentoList(TipoDescuento tipodescuento, Integer page,
            Integer pageSize) {

        TipoDescuentoListAdapter lista = new TipoDescuentoListAdapter();

        Map params = TipoDescuentoFilter.build(tipodescuento, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_DESCUENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoDescuentoListAdapter.class);

            lista = (TipoDescuentoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de DescuentoService
     */
    public DescuentoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        DESCUENTO("Descuento", "descuento"),
        APROBARDESCUENTO("Aprobar Descuento", "descuento/aprobar"),
        INACTIVAR_DESCUENTO("Inactivar Descuento", "descuento/inactivar"),
        TIPO_DESCUENTO("Tipo descuento", "tipo-descuento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
