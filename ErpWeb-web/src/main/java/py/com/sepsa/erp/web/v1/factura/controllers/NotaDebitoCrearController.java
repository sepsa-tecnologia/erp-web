package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.task.GeneracionDteNotaDebitoMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Alta ND
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("notaDebitoCrear")
public class NotaDebitoCrearController implements Serializable {

    /**
     * Dato bandera de prueba para la vista de crear ND
     */
    private boolean create;

    /**
     * Dato bandera de prueba para la vista de crear ND
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaMontoAdapter adapterFactura;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas Pojo
     */
    private FacturaPojoAdapter adapterFacturaPojo;
    /**
     * POJO Factura
     */
    private FacturaPojo facturaPojo;
    private Factura facturaSeleccionada;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Débito
     */
    private NotaDebitoService serviceNotaDebito;
    /**
     * POJO Datos Nota de Débito
     */
    private DatosNotaDebito datoNotaDebito;
    /**
     * POJO Nota de Débito
     */
    private NotaDebito notaDebitoCreate;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.D.
     */
    private List<NotaDebitoDetalle> listaDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Nota de Débito
     */
    private List<FacturaPojo> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private ClientePojo cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientPojoAdapter adapterCliente;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato de factura
     */
    private String idFactura;
    /**
     *
     * Adaptador para la lista de motivoEmisionInterno
     *
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;
    /**
     *
     * POJO de MotivoEmisionInterno
     *
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Descripción de motivo emisión
     */
    private String motivoEmision;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Lista seleccionada de Nota de Débito
     */
    private List<Moneda> listMoneda = new ArrayList<>();
    /**
     * Bandera
     */
    private boolean habilitarSeleccion;
    /**
     * Identificador de Empresa
     */
    private Integer idEmpresa;

    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Adapter Factura Talonario
     */
    private NotaDebitoTalonarioAdapter adapterNDTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo ndTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para agregar campos de dirección
     */
    private String tieneDireccion;
    /**
     * Bandera para cargar descripcion por defecto o descripcion del producto al detalle de NC
     */
    private String descripcionProductoDetalle;
    /**
     * Número de ND creada para descarga del KuDE
     */
    private String nroNDcreada;
    /**
     * ID de la ND creada para descarga del KuDE
     */
    private Integer idNdCreada;
    /**
     * Bandera siediApi
     */
    private String siediApi;

    private FileServiceClient fileServiceClient;
    
    /**
     * Tipo de descuento seleccionado
     */
    private Integer tipoDescuento;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(Integer tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public String getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(String siediApi) {
        this.siediApi = siediApi;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }  
    
    public void setListMoneda(List<Moneda> listMoneda) {
        this.listMoneda = listMoneda;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public void setAdapterCliente(ClientPojoAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public ClientePojo getCliente() {
        return cliente;
    }

    public ClientPojoAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public List<Moneda> getListMoneda() {
        return listMoneda;
    }

    public void setTieneDireccion(String tieneDireccion) {
        this.tieneDireccion = tieneDireccion;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public String getTieneDireccion() {
        return tieneDireccion;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setFacturaSeleccionada(Factura facturaSeleccionada) {
        this.facturaSeleccionada = facturaSeleccionada;
    }

    public Factura getFacturaSeleccionada() {
        return facturaSeleccionada;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setHabilitarSeleccion(boolean habilitarSeleccion) {
        this.habilitarSeleccion = habilitarSeleccion;
    }

    public boolean isHabilitarSeleccion() {
        return habilitarSeleccion;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public void setMotivoEmision(String motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public String getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {
        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;
    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {
        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;
    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {
        return motivoEmisionInternoFilter;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {
        return motivoEmisionInternoAdapterList;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListSelectedFactura(List<FacturaPojo> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public List<FacturaPojo> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaMontoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaMontoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

    public String getDescripcionProductoDetalle() {
        return descripcionProductoDetalle;
    }

    public void setDescripcionProductoDetalle(String descripcionProductoDetalle) {
        this.descripcionProductoDetalle = descripcionProductoDetalle;
    }

    public NotaDebitoService getServiceNotaDebito() {
        return serviceNotaDebito;
    }

    public void setServiceNotaDebito(NotaDebitoService serviceNotaDebito) {
        this.serviceNotaDebito = serviceNotaDebito;
    }

    public DatosNotaDebito getDatoNotaDebito() {
        return datoNotaDebito;
    }

    public void setDatoNotaDebito(DatosNotaDebito datoNotaDebito) {
        this.datoNotaDebito = datoNotaDebito;
    }

    public NotaDebito getNotaDebitoCreate() {
        return notaDebitoCreate;
    }

    public void setNotaDebitoCreate(NotaDebito notaDebitoCreate) {
        this.notaDebitoCreate = notaDebitoCreate;
    }

    public List<NotaDebitoDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<NotaDebitoDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public NotaDebitoTalonarioAdapter getAdapterNDTalonario() {
        return adapterNDTalonario;
    }

    public void setAdapterNDTalonario(NotaDebitoTalonarioAdapter adapterNDTalonario) {
        this.adapterNDTalonario = adapterNDTalonario;
    }

    public TalonarioPojo getNdTalonario() {
        return ndTalonario;
    }

    public void setNdTalonario(TalonarioPojo ndTalonario) {
        this.ndTalonario = ndTalonario;
    }

    public String getNroNDcreada() {
        return nroNDcreada;
    }

    public void setNroNDcreada(String nroNDcreada) {
        this.nroNDcreada = nroNDcreada;
    }

    public Integer getIdNdCreada() {
        return idNdCreada;
    }

    public void setIdNdCreada(Integer idNdCreada) {
        this.idNdCreada = idNdCreada;
    }

    public FacturaPojoAdapter getAdapterFacturaPojo() {
        return adapterFacturaPojo;
    }

    public void setAdapterFacturaPojo(FacturaPojoAdapter adapterFacturaPojo) {
        this.adapterFacturaPojo = adapterFacturaPojo;
    }

    public FacturaPojo getFacturaPojo() {
        return facturaPojo;
    }

    public void setFacturaPojo(FacturaPojo facturaPojo) {
        this.facturaPojo = facturaPojo;
    }
//</editor-fold>
    /**
     * Método para agregar detalle
     *
     * @param event
     */
    public void onCheckFactura(SelectEvent event) {
        WebLogger.get().debug("entra");
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();

        FacturaPojoAdapter afpj = new FacturaPojoAdapter();
        FacturaPojo fdpj = new FacturaPojo();
        fdpj.setId(((FacturaPojo) event.getObject()).getId());
        fdpj.setListadoPojo(true);
        afpj = afpj.fillData(fdpj);
        
        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(afpj.getData().get(0).getId());
        afp = afp.fillData(fdp);
        Factura selectedFac = afp.getData().get(0);
        Integer idTipoCambio = (afp.getData().get(0).getIdTipoCambio());
        
        if (idTipoCambio != null){
            notaDebitoCreate.setIdTipoCambio(idTipoCambio);
        }
        
        if (selectedFac.getObservacion() != null) {
            notaDebitoCreate.setObservacion(selectedFac.getObservacion());  
        }
                       
        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {

            NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();

            if (listaDetalle.isEmpty()) {
                notaDebitoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaDebitoDetalle.setNroLinea(linea);
            }
            notaDebitoDetalle.setNroLinea(linea);
            notaDebitoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaDebitoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());

            notaDebitoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();
            
            if(descripcionProductoDetalle.equalsIgnoreCase("S")){
                notaDebitoDetalle.setDescripcion(facturaDetalle.getDescripcion());
           } else {
                notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);
           }  
            
            if (selectedFac.getIdDepartamento() != null && selectedFac.getIdDistrito() != null && selectedFac.getIdCiudad() != null) {
                idDepartamento = selectedFac.getIdDepartamento();
                filterDistrito();
                idDistrito = selectedFac.getIdDistrito();
                filterCiudad();
                idCiudad = selectedFac.getIdCiudad();
                
                notaDebitoCreate.setDireccion(selectedFac.getDireccion());
                notaDebitoCreate.setNroCasa(selectedFac.getNroCasa());
            }

            /**
             * if (facturaDetalle.getProducto() != null) {
             * notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE
             * LA FACTURA NRO." + nroFactura + " DEL PRODUCTO: " +
             * facturaDetalle.getProducto().getDescripcion());
             *
             * } else { notavDetalle.setDescripcion("BONIFICACION " +
             * linea + " DE LA FACTURA NRO." + nroFactura + " DEL PRODUCTO "); }
             *
             */
            notaDebitoDetalle.setDescuentoParticularUnitarioAux(facturaDetalle.getDescuentoParticularUnitario());
            notaDebitoDetalle.setDescuentoParticularUnitario(facturaDetalle.getDescuentoParticularUnitario());
            notaDebitoDetalle.setMontoDescuentoParticular(facturaDetalle.getMontoDescuentoParticular());
            notaDebitoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaDebitoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaDebitoDetalle.setCantidad(facturaDetalle.getCantidad());

            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva();

            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaDebitoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaDebitoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaDebitoDetalle.setIdProducto(facturaDetalle.getIdProducto());
            notaDebitoDetalle.setSubTotalSinDescuento(facturaDetalle.getTotalSinDescuento());

            listaDetalle.add(notaDebitoDetalle);
        }
        listSelectedFactura = new ArrayList<>();
        listSelectedFactura.add(((FacturaPojo) event.getObject()));
        
        if (selectedFac.getMontoTotalDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1) {
            notaDebitoCreate.setMontoTotalDescuentoGlobal(selectedFac.getMontoTotalDescuentoGlobal());
            notaDebitoCreate.setPorcentajeDescuentoGlobal(selectedFac.getPorcentajeDescuentoGlobal());
            calcularDescuentos(2);
        } else {
            notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
            notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
            calcularTotales();
        }
        
    }

    /**
     * Método para calcular el precio unitario sin IVA
     *
     * @param porcentaje
     * @param precioUnitIVA
     * @return
     */
    public BigDecimal calcularPrecioUnitSinIVA(Integer porcentaje, BigDecimal precioUnitIVA) {
        BigDecimal precioUnitSinIVA = new BigDecimal("0");
        if (porcentaje == 0) {
            precioUnitSinIVA = precioUnitIVA;
        }

        if (porcentaje == 5) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

        }

        if (porcentaje == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

        }

        return precioUnitSinIVA;
    }

    public void obtenerDatosTalonario() {
        ndTalonario.setDigital(digital);
        //ncTalonario.setIdUsuario(session.getUser().getId());
        ndTalonario.setIdEncargado(session.getUser().getIdPersonaFisica());
        adapterNDTalonario = adapterNDTalonario.fillData(ndTalonario);

        if (adapterNDTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            //PF.current().ajax().update(":list-nota-debito-create-form:form-data:pnl-pop-up");
            PF.current().executeScript("$('#modalTalonario').modal('show');");
        } else if (adapterNDTalonario.getData().size() > 0){
            talonario.setId(adapterNDTalonario.getData().get(0).getId());
            obtenerDatosND();
        } else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontró un talonario"));
        }
    }

    /**
     * Método invocado para deseleccionar liquidacion
     *
     * @param event
     */
    public void onUncheckFactura(UnselectEvent event) {

        BigDecimal subTotal = new BigDecimal("0");
        NotaDebitoDetalle info = null;
        
        FacturaPojoAdapter afpj = new FacturaPojoAdapter();
        FacturaPojo fdpj = new FacturaPojo();
        fdpj.setId(((FacturaPojo) event.getObject()).getId());
        fdpj.setListadoPojo(true);
        afpj = afpj.fillData(fdpj);
        
        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(afpj.getData().get(0).getId());
        afp = afp.fillData(fdp);
        
        for (int i = 0; i < afp.getData().get(0).getFacturaDetalles().size(); i++) {
            for (NotaDebitoDetalle info0 : listaDetalle) {
                if (Objects.equals(info0.getIdFactura(), ((FacturaPojo) event.getObject()).getId())) {
                    info = info0;
                    subTotal = info.getMontoTotal();
                    listaDetalle.remove(info);
                    break;
                }
            }

            BigDecimal monto = notaDebitoCreate.getMontoTotalNotaDebito();
            BigDecimal totalNotaDebito = monto.subtract(subTotal);
            notaDebitoCreate.setMontoTotalNotaDebito(totalNotaDebito);

        }

        calcularTotales();

    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void cambiarDescripcion(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        String h = info.getDescripcion();
        WebLogger.get().debug(h);
    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontosa(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal cinco = new BigDecimal("5");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoImponible.divide(cinco, 2, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal diez = new BigDecimal("10");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoImponible.divide(diez, 5, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotales();

    }
    
    public NotaDebitoDetalle obtenerDetalle(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                return info;
            }
        }

        return null;
    }

    public void calcularMontos(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        if (info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == -1){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pueden ingresar montos menores a 0."));
        } else {
            // Se calcula el monto total del detalle
            BigDecimal cantidad = info.getCantidad();
            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
            BigDecimal descuentoParticularUnitario = info.getDescuentoParticularUnitarioAux(); 
            BigDecimal montoPrecioXCantidadSinDescuento = precioUnitarioConIVA.multiply(cantidad);
            //Se aplica el descuento al item
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();
            
                        //Casos en que se hayan ingresado primero el monto/porcentaje descuento y luego el precioUnitarioIVA
           if( montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            } else if (descuentoParticularUnitario != null && descuentoParticularUnitario.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            }

            if (montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) { 
                montoDescuentoItem = info.getMontoDescuentoParticular();
                precioUnitarioConIVA = precioUnitarioConIVA.subtract(montoDescuentoItem);
            } else {
                info.setDescuentoGlobalUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
            }
            
            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);
            
            // Se calcula el monto base
            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxB = new BigDecimal(100).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxC = auxA.multiply(auxB);
            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);

            // Se calcula el monto exento gravado
            BigDecimal auxD = new BigDecimal("100").subtract(new BigDecimal(100));
            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
            // Integer aux1 = ((100 - info.getPorcentajeIva()) / 100);
            BigDecimal montoExentoGravado = montoBase.multiply(auxF);

            //Se calcula el monto imponible
            //Integer aux2 = info.getPorcentajeGravada() / 100;
            //BigDecimal auxF = new BigDecimal(info.getPorcentajeGravada()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
            BigDecimal montoImponible = montoBase.multiply(auxB);

            //Se calcula el monto iva
            // Integer aux3 = info.getPorcentajeIva() / 100;
            BigDecimal montoIva = montoImponible.multiply(auxA);

            //Se suman los montos para el total
            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);

            //Precio unitario
            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);

            info.setMontoIva(montoIva);
            info.setMontoImponible(montoImponible);
         //   info.setMontoExentoGravado(montoExentoGravado);
            info.setMontoTotal(total);
            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
            info.setSubTotalSinDescuento(montoPrecioXCantidadSinDescuento);
            calcularTotales();
            
            if (notaDebitoCreate.getPorcentajeDescuentoGlobal()!= null && notaDebitoCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1){
                calcularDescuentos(tipoDescuento);
            } else if ((notaDebitoCreate.getMontoTotalDescuentoGlobal()!= null && notaDebitoCreate.getMontoTotalDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1)){
                
            }
            
        }
    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");
        BigDecimal auxTotalSinDescuento = new BigDecimal("0");
        
        for (int i = 0; i < listaDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaDetalle.get(i).getMontoTotal());
            if (listaDetalle.get(i).getMontoDescuentoParticular().compareTo(BigDecimal.ZERO)>0) {
                auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getPrecioUnitarioConIva().multiply(listaDetalle.get(i).getCantidad()));
            } else {
                 auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getMontoTotal());
            }
        }

        notaDebitoCreate.setMontoTotalNotaDebito(acumTotales);
        notaDebitoCreate.setMontoTotalGuaranies(acumTotales);
        notaDebitoCreate.setSubTotalSinDescuento(auxTotalSinDescuento);
        

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        notaDebitoCreate.setMontoIva5(montoIva5Acumulador);
        notaDebitoCreate.setMontoImponible5(montoImponible5Acumulador);
        notaDebitoCreate.setMontoTotal5(montoTotal5Acumulador);
        notaDebitoCreate.setMontoIva10(montoIva10Acumulador);
        notaDebitoCreate.setMontoImponible10(montoImponible10Acumulador);
        notaDebitoCreate.setMontoTotal10(montoTotal10Acumulador);
        notaDebitoCreate.setMontoTotalExento(montoExcentoAcumulador);
        notaDebitoCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        notaDebitoCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(notaDebitoCreate.getIdMoneda());
        montoLetrasFilter.setCodigoMoneda("PYG");
        BigDecimal scaled = notaDebitoCreate.getMontoTotalNotaDebito().setScale(0, RoundingMode.HALF_UP);
        montoLetrasFilter.setMonto(scaled);

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ClientePojo> completeQuery(String query) {

        ClientePojo cliente = new ClientePojo();
        cliente.setRazonSocial(query);
        cliente.setListadoPojo(true);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            notaDebitoCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        }

    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
        int id = ((ClientePojo) event.getObject()).getIdCliente();

        notaDebitoCreate = new NotaDebito();

        if (((ClientePojo) event.getObject()).getIdNaturalezaCliente() != null) {
            notaDebitoCreate.setIdNaturalezaCliente(((ClientePojo) event.getObject()).getIdNaturalezaCliente());
        }

        notaDebitoCreate.setRazonSocial(((ClientePojo) event.getObject()).getRazonSocial());
        ruc = ((ClientePojo) event.getObject()).getNroDocumento();
        ruc = ruc.trim().replace(" ","").replace(".","");
        idCliente = ((ClientePojo) event.getObject()).getIdCliente();
        notaDebitoCreate.setRuc(ruc);
        notaDebitoCreate.setIdCliente(idCliente);
        notaDebitoCreate.setIdMoneda(1);

        filterFacturaPojo(id);
        obtenerDatosND();
        showDatatable = true;

    }

    public void obtenerDatosND() {
        try {

            Map parametros = new HashMap();
            datoNotaDebito = new DatosNotaDebito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaDebitoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaDebitoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaDebito = serviceNotaDebito.getDatosNotaDebito(parametros);

            if (datoNotaDebito != null) {
                notaDebitoCreate.setFecha(today.getTime());
                notaDebitoCreate.setIdTalonario(datoNotaDebito.getIdTalonario());
                notaDebitoCreate.setNroTimbrado(datoNotaDebito.getTimbrado());
                notaDebitoCreate.setNroNotaDebito(datoNotaDebito.getNroNotaDebito());

                //Validaciones
                if (datoNotaDebito.getTelefono() != null) {
                    notaDebitoCreate.setTelefono(datoNotaDebito.getTelefono());
                }
                if (datoNotaDebito.getEmail() != null) {
                    notaDebitoCreate.setEmail(datoNotaDebito.getEmail());
                }
                if (datoNotaDebito.getDireccion() != null) {
                    notaDebitoCreate.setDireccion(datoNotaDebito.getDireccion());
                }
                if (datoNotaDebito.getIdDepartamento() != null) {
                    notaDebitoCreate.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaDebito.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datoNotaDebito.getIdDistrito() != null) {
                    notaDebitoCreate.setIdDistrito(datoNotaDebito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaDebito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaDebito.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datoNotaDebito.getIdCiudad() != null) {
                    notaDebitoCreate.setIdCiudad(datoNotaDebito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaDebito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaDebito.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datoNotaDebito.getNroCasa() != null) {
                    notaDebitoCreate.setNroCasa(datoNotaDebito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
        if (idFactura != null) {
            obtenerDatosNDDevolucionFactura();
            devolucionDetalleND();
            show = true;
            PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:detalle-cabecera list-nota-debito-create-form:form-data:panel-cabecera-detalle:data-list-detalle-nota-debito list-nota-debito-create-form:form-data:panel-nota-debito:data-list-nota-create");

        }
    }

    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }

    /**
     * Método para crear Nota de Débito
     */
    public void createNotaDebito() {
        calcularTotalesGenerales();
        boolean saveND = true;

        notaDebitoCreate.setAnulado("N");
        notaDebitoCreate.setImpreso("N");
        notaDebitoCreate.setEntregado("N");
        notaDebitoCreate.setDigital(digital);

        if (tieneDireccion.equals("S")) {
            if (notaDebitoCreate.getDireccion() == null || notaDebitoCreate.getDireccion().trim().isEmpty()) {
                saveND = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
            }

            if (notaDebitoCreate.getNroCasa() == null || notaDebitoCreate.getNroCasa().trim().isEmpty()) {
                saveND = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
            }

            if (idDepartamento == null) {
                saveND = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
            } else {
                notaDebitoCreate.setIdDepartamento(idDepartamento);
            }

            if (idDistrito == null) {
                saveND = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
            } else {
                notaDebitoCreate.setIdDistrito(idDistrito);
            }

            if (idCiudad == null) {
                saveND = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
            } else {
                notaDebitoCreate.setIdCiudad(idCiudad);
            }

        } else {
            notaDebitoCreate.setDireccion(null);
            notaDebitoCreate.setNroCasa(null);
            notaDebitoCreate.setIdDepartamento(null);
            notaDebitoCreate.setIdDistrito(null);
            notaDebitoCreate.setIdCiudad(null);
        }

        if (digital.equals("S")) {
            notaDebitoCreate.setArchivoSet("S");
        } else {
            notaDebitoCreate.setArchivoSet("N");
        }

        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
            listaDetalle.get(i).setNroNotaDebito(notaDebitoCreate.getNroNotaDebito());
        }

        notaDebitoCreate.setNotaDebitoDetalles(listaDetalle);

        if (saveND) {
            BodyResponse<NotaDebito> respuestaDeND = serviceNotaDebito.createNotaDebito(notaDebitoCreate);
                       
            if (respuestaDeND.getSuccess()) {
                this.nroNDcreada = notaDebitoCreate.getNroNotaDebito();
                this.idNdCreada = ((NotaDebito) respuestaDeND.getPayload()).getId();
                if (ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCARGAR_FACTURA", "N").equalsIgnoreCase("S")) {
                    mostrarPanelDescarga();
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "ND creada correctamente!"));
                try {
                    NotaDebito nd = (NotaDebito) respuestaDeND.getPayload();
                    GeneracionDteNotaDebitoMultiempresa.generar(nd.getIdEmpresa(), nd.getId());
                } catch (Exception e){
                    WebLogger.get().fatal(e);
                }
                init();
            }
        }

    }

    /**
     * Método para filtrar la factura
     */
    public void filterFacturaPojo(Integer idCliente) {
        facturaPojo.setIdCliente(idCliente);
        facturaPojo.setCobrado("N");
        facturaPojo.setAnulado("N");
        facturaPojo.setListadoPojo(true);
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        idCliente = null;
        direccion = "";
        notaDebitoCreate = new NotaDebito();
        listSelectedFactura = new ArrayList<>();
        listaDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        datoNotaDebito = new DatosNotaDebito();
    }

    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
        this.listMoneda = new ArrayList<>();
        this.listMoneda = adapterMoneda.getData();
    }

    public void filterFacturasList() {
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    public void clearFacturasList() {
        facturaPojo = new FacturaPojo();
        facturaPojo.setIdCliente(idCliente);
        facturaPojo.setCobrado("N");
        facturaPojo.setAnulado("N");
        facturaPojo.setListadoPojo(true);
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    /**
     * Método para obtener la factura en ND, cuando se redirecciona desde el
     * listado de facturas
     */
    public void obtenerFactura() {
        facturaFilter.setId(Integer.parseInt(idFactura));
        adapterFactura = adapterFactura.fillData(facturaFilter);
        
        facturaPojo.setId(Integer.parseInt(idFactura));
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
        listSelectedFactura.add(adapterFacturaPojo.getData().get(0));
        cliente.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());
        Factura selectedFac = adapterFactura.getData().get(0);
        
        descripcionProductoDetalle = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCRIPCION_PRODUCTO_DETALLE_ND", "N");
        digital = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "TIPO_TALONARIO_ND_DIGITAL", "N");
        obtenerDatosTalonario();
        
        if (selectedFac.getIdDepartamento() != null && selectedFac.getIdDistrito() != null && selectedFac.getIdCiudad() != null) {
            idDepartamento = selectedFac.getIdDepartamento();
            filterDistrito();
            idDistrito = selectedFac.getIdDistrito();
            filterCiudad();
            idCiudad = selectedFac.getIdCiudad();

            notaDebitoCreate.setDireccion(selectedFac.getDireccion());
            notaDebitoCreate.setNroCasa(selectedFac.getNroCasa());
        }
        
        if (selectedFac.getIdTipoCambio() != null) {
            notaDebitoCreate.setIdTipoCambio(selectedFac.getIdTipoCambio());
        }
        
        if (selectedFac.getObservacion() != null) {
            notaDebitoCreate.setObservacion(selectedFac.getObservacion());
        }
        

    }

    /**
     * Método para obtener datos de cabecera
     */
    public void obtenerDatosNDDevolucionFactura() {
        notaDebitoCreate.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());
        notaDebitoCreate.setIdNaturalezaCliente(adapterFactura.getData().get(0).getIdNaturalezaCliente());
        ruc = adapterFactura.getData().get(0).getRuc();
        idCliente = adapterFactura.getData().get(0).getIdCliente();
        notaDebitoCreate.setRuc(ruc);
        notaDebitoCreate.setIdCliente(idCliente);
        try {

            Map parametros = new HashMap();
            datoNotaDebito = new DatosNotaDebito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaDebitoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaDebitoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaDebito = serviceNotaDebito.getDatosNotaDebito(parametros);

            if (datoNotaDebito != null) {
                notaDebitoCreate.setFecha(today.getTime());
                notaDebitoCreate.setIdTalonario(datoNotaDebito.getIdTalonario());
                notaDebitoCreate.setNroNotaDebito(datoNotaDebito.getNroNotaDebito());

                //Validaciones
                if (datoNotaDebito.getTelefono() != null) {
                    notaDebitoCreate.setTelefono(datoNotaDebito.getTelefono());
                }
                if (datoNotaDebito.getEmail() != null) {
                    notaDebitoCreate.setEmail(datoNotaDebito.getEmail());
                }
                if (datoNotaDebito.getDireccion() != null) {
                    notaDebitoCreate.setDireccion(datoNotaDebito.getDireccion());
                }
                if (datoNotaDebito.getIdDepartamento() != null) {
                    notaDebitoCreate.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaDebito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaDebito.getIdDepartamento());
                }
                if (datoNotaDebito.getIdDistrito() != null) {
                    notaDebitoCreate.setIdDistrito(datoNotaDebito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaDebito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaDebito.getIdDistrito());
                }
                if (datoNotaDebito.getIdCiudad() != null) {
                    notaDebitoCreate.setIdCiudad(datoNotaDebito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaDebito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaDebito.getIdCiudad());
                }
                if (datoNotaDebito.getNroCasa() != null) {
                    notaDebitoCreate.setNroCasa(datoNotaDebito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void devolucionDetalleND() {
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();
        int nroBonificacion = 1;

        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(Integer.parseInt(idFactura));
        afp = afp.fillData(fdp);

        notaDebitoCreate.setIdNaturalezaCliente(afp.getData().get(0).getIdNaturalezaCliente());
        if(afp.getData().get(0).getPorcentajeDescuentoGlobal() == null || afp.getData().get(0).getMontoTotalDescuentoGlobal() == null){
            notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
            notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
        } else {
            notaDebitoCreate.setPorcentajeDescuentoGlobal(afp.getData().get(0).getPorcentajeDescuentoGlobal());
            notaDebitoCreate.setMontoTotalDescuentoGlobal(afp.getData().get(0).getMontoTotalDescuentoGlobal());
        }
        
        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {
            NotaDebitoDetalle notaDebitoDetalle = new NotaDebitoDetalle();

            if (listaDetalle.isEmpty()) {
                notaDebitoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaDebitoDetalle.setNroLinea(linea);
            }

            notaDebitoDetalle.setNroLinea(linea);
            notaDebitoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaDebitoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());
            

            notaDebitoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();
           if(descripcionProductoDetalle.equalsIgnoreCase("S")){
                notaDebitoDetalle.setDescripcion(facturaDetalle.getDescripcion());
           } else {
                notaDebitoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);
           }  
            
           
            notaDebitoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaDebitoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaDebitoDetalle.setCantidad(facturaDetalle.getCantidad());
            notaDebitoDetalle.setDescuentoParticularUnitario(facturaDetalle.getDescuentoParticularUnitario());
            notaDebitoDetalle.setDescuentoParticularUnitarioAux(facturaDetalle.getDescuentoParticularUnitario());
            notaDebitoDetalle.setMontoDescuentoParticular(facturaDetalle.getMontoDescuentoParticular());
            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva();
            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);

            notaDebitoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaDebitoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaDebitoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaDebitoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaDebitoDetalle.setIdProducto(facturaDetalle.getIdProducto());

            listaDetalle.add(notaDebitoDetalle);
        }

        listSelectedFactura = new ArrayList<>();
        facturaPojo.setId(afp.getData().get(0).getId());
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
        listSelectedFactura.add(adapterFacturaPojo.getData().get(0));

        calcularTotales();
        
        calcularDescuentos(2);
        
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void obtenerMotivoEmisionInterno() {

        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));

    }

    public void obtenerMotivoEmision() {
        MotivoEmisionInternoAdapter motivoEmisionInternoAdapter = new MotivoEmisionInternoAdapter();
        MotivoEmisionInterno motivoEmisionInterno = new MotivoEmisionInterno();
        if (notaDebitoCreate.getIdMotivoEmisionInterno() != null) {
            motivoEmisionInterno.setId(notaDebitoCreate.getIdMotivoEmisionInterno());
            motivoEmisionInternoAdapter = motivoEmisionInternoAdapter.fillData(motivoEmisionInterno);
            motivoEmision = motivoEmisionInternoAdapter.getData().get(0).getMotivoEmision().getDescripcion();
        } else {
            this.motivoEmision = "Ninguno";
        }

    }

    public Factura filtrarFactura(Integer idFactura) {

        FacturaAdapter adapterFactura = new FacturaAdapter();
        Factura facturaFilter = new Factura();
        facturaFilter.setId(idFactura);

        adapterFactura = adapterFactura.fillData(facturaFilter);

        return adapterFactura.getData().get(0);
    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param nroLinea
     */
    public void deleteDetalle(Integer nroLinea) {
        NotaDebitoDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        BigDecimal monto = notaDebitoCreate.getMontoTotalNotaDebito();
        BigDecimal totalND = monto.subtract(subTotal);
        notaDebitoCreate.setMontoTotalNotaDebito(totalND);
        obtenerMontoTotalLetras();

    }

    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:distrito");
        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-debito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        // referenciaGeoDistrito = new ReferenciaGeografica();
        // referenciaGeoCiudad = new ReferenciaGeografica();
        // adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        //referenciaGeoCiudad = new ReferenciaGeografica();
        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        notaDebitoCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    public void filterDepartamento() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
    public void descuentoListener(Integer nroLinea, Integer param){
        calcularDescuentoItem(nroLinea,tipoDescuento);
        calcularMontos(nroLinea);
        calcularDescuentos(2);
        
    }
    
     /**
     * Método para calcular el descuento de un item del detalle
     * @param nroLinea
     * @param param 
     */
    public void calcularDescuentoItem(Integer nroLinea, Integer param) {
        NotaDebitoDetalle info = null;
        
        for (NotaDebitoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        
        if (info != null) {
            BigDecimal descuentoItemAux = info.getDescuentoParticularUnitarioAux();
            BigDecimal descuentoItem = info.getDescuentoParticularUnitario();
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();

            if (param == 1) {
                if (info.getPrecioUnitarioConIva() != null && info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == 1) {
                    //valida que el porcentaje de descuento ingresado sea mayor o igual a 0 y menor que 100
                    if (descuentoItemAux != null && descuentoItem != null && ((descuentoItem.compareTo(BigDecimal.ZERO) == 1 && descuentoItem.compareTo(new BigDecimal("100")) == -1) || (descuentoItem.compareTo(BigDecimal.ZERO) == 0)) ) {                
                        BigDecimal porcentajeDescuento = descuentoItemAux.divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
                        montoDescuentoItem = info.getPrecioUnitarioConIva().multiply(porcentajeDescuento);
                        info.setMontoDescuentoParticular(montoDescuentoItem);
                        info.setDescuentoParticularUnitario(descuentoItemAux);
                    } else {
                        info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                        info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                        info.setMontoDescuentoParticular(BigDecimal.ZERO);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje de descuento debe ser entre 0 y 100."));
                    }
                }   
            } else if (param == 2) {
                //valida que el montoDescuento no sea nulo, menor a cero o mayor al precio unitario
                if (montoDescuentoItem != null && ((( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1 ) ||( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 0 )) && (montoDescuentoItem.compareTo(info.getPrecioUnitarioConIva())== -1))){
                    BigDecimal aux = info.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                    descuentoItem = aux.divide(info.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                    info.setDescuentoParticularUnitario(descuentoItem);
                    info.setDescuentoParticularUnitarioAux(descuentoItem);
                } else {
                    info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                    info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                    info.setMontoDescuentoParticular(BigDecimal.ZERO);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto de descuento no puede ser menor a 0 o mayor al precio unitario con IVA"));
                }
            }
        }
    }
    
    /**
     * Método que calcula el descuento global
     * @param param 1 = Calcular descuento segun porcentaje, 2 = Calcular según monto descuento
     */
    public void calcularDescuentos(Integer param){
        calcularTotales();
        if (notaDebitoCreate.getMontoTotalNotaDebito() != null && notaDebitoCreate.getMontoTotalNotaDebito().compareTo(BigDecimal.ZERO) == 1){
            if (param == 1){
                if (notaDebitoCreate.getPorcentajeDescuentoGlobal()!= null) {
                    BigDecimal porcentajeDescuento = notaDebitoCreate.getPorcentajeDescuentoGlobal().divide(new BigDecimal("100"),5,RoundingMode.HALF_UP);
                    if(notaDebitoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0) {
                        notaDebitoCreate.setMontoTotalDescuentoGlobal(notaDebitoCreate.getSubTotalSinDescuento().multiply(porcentajeDescuento));
                    } else {
                        notaDebitoCreate.setMontoTotalDescuentoGlobal(notaDebitoCreate.getMontoTotalNotaDebito().multiply(porcentajeDescuento));
                    }
                    
                    notaDebitoCreate.setMontoTotalNotaDebito(notaDebitoCreate.getMontoTotalNotaDebito().subtract(notaDebitoCreate.getMontoTotalDescuentoGlobal()));
                        
                    notaDebitoCreate.setMontoTotalGuaranies(notaDebitoCreate.getMontoTotalNotaDebito());
                    obtenerMontoTotalLetras();                        
                } else {
                    notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                }
                
            } else if (param == 2) {
                if (notaDebitoCreate.getMontoTotalDescuentoGlobal() != null) {
                    BigDecimal aux = notaDebitoCreate.getMontoTotalDescuentoGlobal().multiply(new BigDecimal("100"));
                    BigDecimal porcentaje = BigDecimal.ZERO;
                    if(notaDebitoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0){
                         porcentaje = aux.divide(notaDebitoCreate.getSubTotalSinDescuento(),5,RoundingMode.HALF_UP);
                    } else {
                         porcentaje = aux.divide(notaDebitoCreate.getMontoTotalNotaDebito(),5,RoundingMode.HALF_UP);
                    }
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(porcentaje);
                    notaDebitoCreate.setMontoTotalNotaDebito(notaDebitoCreate.getMontoTotalNotaDebito().subtract(notaDebitoCreate.getMontoTotalDescuentoGlobal()));
                   
                    notaDebitoCreate.setMontoTotalGuaranies(notaDebitoCreate.getMontoTotalNotaDebito());
                    obtenerMontoTotalLetras();
                        
                } else {
                    notaDebitoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                    notaDebitoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                }
                
            }
        }
    }
    
    /**
     * Método para descargar el pdf de Nota de Débito
     * @return 
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroNDcreada + ".pdf";
        String url = "nota-debito/consulta/" + idNdCreada;
        Map param = new HashMap();
        if (this.digital.equalsIgnoreCase("S")){
             param.put("ticket", false);
             if (siediApi.equalsIgnoreCase("S")) {
                param.put("siediApi", true);
             } else {
                param.put("siediApi", false);
             }
        }
        byte[] data = fileServiceClient.downloadNotaDebito(url,param);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }
    
    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.facturaSeleccionada = new Factura();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.direccionFilter = new Direccion();

        this.habilitarSeleccion = true;
        create = false;
        show = false;
        this.idEmpresa = session.getUser().getIdEmpresa();
        this.adapterNDTalonario = new NotaDebitoTalonarioAdapter();
        this.ndTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();

        this.adapterFactura = new FacturaMontoAdapter();
        this.facturaFilter = new Factura();
        this.adapterFacturaPojo = new FacturaPojoAdapter();
        this.facturaPojo = new FacturaPojo();
        this.facturaPojo.setListadoPojo(true);
        this.serviceNotaDebito = new NotaDebitoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new ClientePojo();
        this.adapterCliente = new ClientPojoAdapter();

        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();

        this.notaDebitoCreate = new NotaDebito();

        this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
        this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());

        filterMoneda();
        obtenerMotivoEmisionInterno();

        this.motivoEmision = "Ninguno";
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;

        this.tieneDireccion = "N";

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("id") != null) {
            idFactura = params.get("id");
            showDatatable = true;
            this.habilitarSeleccion = false;
            obtenerFactura();
        } else {
            descripcionProductoDetalle = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCRIPCION_PRODUCTO_DETALLE_ND", "N");
            digital = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "TIPO_TALONARIO_ND_DIGITAL", "N");
            obtenerDatosTalonario();
        }

        filterDepartamento();
        filterDistrito();
        filterCiudad();
        fileServiceClient = new FileServiceClient();
        this.siediApi = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "ARCHIVOS_DTE_SIEDI_API", "N");
        this.tipoDescuento = 1;
    }

}
