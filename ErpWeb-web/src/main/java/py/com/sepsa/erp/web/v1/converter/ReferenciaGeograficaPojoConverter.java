package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;

/**
 *
 * @author alext
 */
@FacesConverter("referenciaPojoConverter")
public class ReferenciaGeograficaPojoConverter implements Converter {

    /**
     * Servicios para login al sistema
     */
    private ReferenciaGeograficaService service;

    @Override
    public ReferenciaGeografica getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            service = new ReferenciaGeograficaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setId(val);

            List<ReferenciaGeografica> referencias
                    = service.getRefGeograficaList(ref, 0, 10).getData();
            if (referencias != null && !referencias.isEmpty()) {
                return referencias.get(0);
            } else {
                throw new ConverterException(new FacesMessage(
                        FacesMessage.SEVERITY_ERROR,
                        "Error", "No es una referencia geográfica válida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof ReferenciaGeografica) {
                return String.valueOf(((ReferenciaGeografica) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
