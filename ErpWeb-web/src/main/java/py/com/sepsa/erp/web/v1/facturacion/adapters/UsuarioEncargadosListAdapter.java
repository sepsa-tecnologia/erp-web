
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Encargado;
import py.com.sepsa.erp.web.v1.info.remote.UsuarioLocalServiceClient;

/**
 * Adaptador de la lista de usuarios encargados
 * @author Cristina Insfrán
 */
public class UsuarioEncargadosListAdapter extends DataListAdapter<Encargado> {
    
     /**
     * Cliente para el servicio de usuario
     */
    private final UsuarioLocalServiceClient encargadosClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioEncargadosListAdapter fillData(Encargado searchData) {

        return encargadosClient.getUsuariosEncargadosList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de UsuarioEncargadosListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioEncargadosListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.encargadosClient = new UsuarioLocalServiceClient();
    }

    /**
     * Constructor de UsuarioEncargadosListAdapter
     */
    public UsuarioEncargadosListAdapter() {
        super();
        this.encargadosClient = new UsuarioLocalServiceClient();
    }
}
