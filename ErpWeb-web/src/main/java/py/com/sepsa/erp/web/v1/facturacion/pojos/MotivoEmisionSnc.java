
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * POJO  para listar los motivos de solicitud snc
 * @author Cristina Insfrán
 */
public class MotivoEmisionSnc {

   
    /**
     * Identificador
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Código xml
     */
    private String codigoXml;
    /**
     * Código txt
     */
    private String codigoTxt;
    /**
     * Activo
     */
    private String activo;
     /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripción to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigoXml
     */
    public String getCodigoXml() {
        return codigoXml;
    }

    /**
     * @param codigoXml the codigoXml to set
     */
    public void setCodigoXml(String codigoXml) {
        this.codigoXml = codigoXml;
    }

    /**
     * @return the codigoTxt
     */
    public String getCodigoTxt() {
        return codigoTxt;
    }

    /**
     * @param codigoTxt the codigoTxt to set
     */
    public void setCodigoTxt(String codigoTxt) {
        this.codigoTxt = codigoTxt;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }
    
    /**
     * Constructor
     */
    public MotivoEmisionSnc(){
        
    }
}
