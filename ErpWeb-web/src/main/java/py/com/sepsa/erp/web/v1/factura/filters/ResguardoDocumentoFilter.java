package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ResguardoDocumento;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para el resguardo de documento
 *
 * @author Romina Núñez
 */
public class ResguardoDocumentoFilter extends Filter {

    /**
     * Agregar el filtro de nombreArchivo
     *
     * @param nombreArchivo
     * @return
     */
    public ResguardoDocumentoFilter nombreArchivo(String nombreArchivo) {
        if (nombreArchivo != null && !nombreArchivo.trim().isEmpty()) {
            params.put("nombreArchivo", nombreArchivo);
        }
        return this;
    }

    /**
     * Agregar el filtro de idTipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public ResguardoDocumentoFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha fechaInsercionDesde
     *
     * @param fechaVencimiento
     * @return
     */
    public ResguardoDocumentoFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionDesde);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercionHasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public ResguardoDocumentoFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param resguardoDocumento
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ResguardoDocumento resguardoDocumento, Integer page, Integer pageSize) {
        ResguardoDocumentoFilter filter = new ResguardoDocumentoFilter();

        filter
                .nombreArchivo(resguardoDocumento.getNombreArchivo())
                .idTipoDocumento(resguardoDocumento.getIdTipoDocumento())
                .fechaInsercionDesde(resguardoDocumento.getFechaInsercionDesde())
                .fechaInsercionHasta(resguardoDocumento.getFechaInsercionHasta())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de MotivoAnulacionFilter
     */
    public ResguardoDocumentoFilter() {
        super();
    }
}
