package py.com.sepsa.erp.web.v1.info.filters;

import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;

/**
 * Filtro utilizado para el servicio de contacto
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class ContactoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de contacto
     *
     * @param idContacto Identificador contacto
     * @return
     */
    public ContactoFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro activo
     *
     * @param activo activo
     * @return
     */
    public ContactoFilter activo(String activo) {
        if (activo != null && !activo.isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cargo
     *
     * @param idCargo Identificador del cargo
     * @return
     */
    public ContactoFilter idCargo(Integer idCargo) {
        if (idCargo != null && idCargo != 0) {
            params.put("idCargo", idCargo);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del tipo contacto
     *
     * @param idTipoContacto Identificador del tipo contacto
     * @return
     */
    public ContactoFilter idTipoContacto(Integer idTipoContacto) {
        if (idTipoContacto != null) {
            params.put("idTipoContacto", idTipoContacto);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contacto
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Contacto contacto, Integer page, Integer pageSize) {
        ContactoFilter filter = new ContactoFilter();

        filter
                .idContacto(contacto.getIdContacto())
                .activo(contacto.getActivo())
                .idCargo(contacto.getIdCargo())
                .idTipoContacto(contacto.getIdTipoContacto())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ContactoFilter
     */
    public ContactoFilter() {
        super();
    }
}
