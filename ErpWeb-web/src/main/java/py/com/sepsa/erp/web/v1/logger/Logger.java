package py.com.sepsa.erp.web.v1.logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

/** 
 * Manejador de log
 * @author Daniel F. Escauriza Arza
 */
public class Logger {
    
    /**
     * Log de la aplicación
     */
    private static org.apache.log4j.Logger logger;
    
    /**
     * Registra un evento del tipo info
     * @param msn Mensaje a registrar
     */
    public void info(String msn) {
        logger.info(msn);
    }
    
    /**
     * Registra un evento del tipo debug
     * @param msn Mensaje a registrar
     */
    public void debug(String msn) {
        logger.debug(msn);
    }
    
    /**
     * Registra un mensaje del tipo error
     * @param msn Mensaje a registrar
     */
    public void error(String msn) {
        logger.error(msn);
    }
    
    /**
     * Registra un evento del tipo fatal
     * @param ex Error o excepción producida
     */
    public void fatal(Exception ex) {
        logger.fatal("Se ha producido el siguiente error: ", ex);
    }

    /**
     * Constructor de Logger
     * @param logName Nombre del log
     */
    public Logger (String logName) {
        try {
            InputStream is = Logger
                    .class
                    .getClassLoader()
                    .getResourceAsStream("Log.properties");

            Properties serverProp = new Properties();

            if(is == null) {
                BasicConfigurator.configure();
            } else {                
                serverProp.load(is);
                PropertyConfigurator.configure(serverProp);
            }

            logger = org.apache.log4j.Logger.getLogger(logName);

        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    public void fatal(String format, Exception ex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}