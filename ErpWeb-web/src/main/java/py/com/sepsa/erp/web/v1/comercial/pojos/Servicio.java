
package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO para el servicio
 * @author Cristina Insfrán
 */
public class Servicio {

    /**
     * Identificador del servicio
     */
    private Integer id;
    /**
     * Descricpión del servicio
     */
    private String descripcion;
    /**
     * Identificador del usuario
     */
    private Integer idUser;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del local
     */
    private Integer idLocal;
    /**
     * Esado del servicio
     */
    private String estado;
    
    /**
     * @return the idUser
     */
    public Integer getIdUser() {
        return idUser;
    }

    /**
     * @param idUser the idUser to set
     */
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the idLocal
     */
    public Integer getIdLocal() {
        return idLocal;
    }

    /**
     * @param idLocal the idLocal to set
     */
    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    
    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    
    /**
     * Constructor de la clase
     */
    public Servicio(){
        
    }
}
