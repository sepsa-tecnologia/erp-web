
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 * POJO para los mensajes mostrados en las vistas
 * @author Daniel F. Escauriza Arza
 */
public class Message {
    
    /**
     * Mensaje
     */
    private String message;
    
    /**
     * Campo asociado al mensaje
     */
    private String field;
    
    /**
     * Tipo de mensaje
     */
    private String type;

    /**
     * Obtiene el mensaje de error
     * @return Mensaje de error
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setea el mensaje
     * @param message Mensaje
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Obtiene el campo asociado al mensaje
     * @return Campo asociado al mensaje
     */
    public String getField() {
        return field;
    }

    /**
     * Setea el campo asociado al mensaje
     * @param field Campo asociado al mensaje
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * Obtiene el tipo de mensaje
     * @return Tipo de mensaje
     */
    public String getType() {
        return type;
    }

    /**
     * Setea el tipo de mensaje
     * @param type Tipo de mensaje
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Constructor de Message
     * @param message Mensaje
     * @param field Campo asociado al mensaje
     * @param type Tipo de mensaje
     */
    public Message(String message, String field, String type) {
        this.message = message;
        this.field = field;
        this.type = type;
    }
    
    /**
     * Constructor de Message
     */
    public Message() {}
    
}
