package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.pojos.Attach;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("notaCreditoCargaMasiva")
public class NotaCreditoCargaMasivaController implements Serializable {
    /**
     * Archivo de instructivo para la carga de NC
     */
    private StreamedContent loadNotaCreditoCarga;
    /**
     * Objeto Attach
     */
    private Attach attach;
    /**
     * NotaCredito Service
     */
    private NotaCreditoService serviceCredito;
    /**
     * Archivo
     */
    private UploadedFile file;
    /**
     * Datos para descarga
     */
    private byte[] dataDownload;
    /**
     * Bandera para descargar
     */
    private boolean downloadBtn;
    
    private String fileNameDownload = "carga_masiva_nota_credito.csv";

    public UploadedFile getFile() {
        return file;
    }

    public void setDownloadBtn(boolean downloadBtn) {
        this.downloadBtn = downloadBtn;
    }

    public boolean isDownloadBtn() {
        return downloadBtn;
    }

    public void setDataDownload(byte[] dataDownload) {
        this.dataDownload = dataDownload;
    }

    public byte[] getDataDownload() {
        return dataDownload;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public NotaCreditoService getServiceCredito() {
        return serviceCredito;
    }

    public void setServiceCredito(NotaCreditoService serviceCredito) {
        this.serviceCredito = serviceCredito;
    }

    public void setLoadNotaCreditoCarga(StreamedContent loadNotaCreditoCarga) {
        this.loadNotaCreditoCarga = loadNotaCreditoCarga;
    } 

    /**
     * Método para descargar el archivo plantilla
     *
     * @return
     */
    public StreamedContent getLoadNotaCreditoCarga() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/app/factura/download/nc-matriz-carga-masiva.csv");
        loadNotaCreditoCarga = new DefaultStreamedContent(stream, "text/csv", "nc-matriz-carga-masiva.csv");
        return loadNotaCreditoCarga;
    }

    /**
     * Método para subir y enviar el archivo
     *
     * @throws IOException
     */
    public void upload() throws IOException {
        if (file != null) {
            attach = new Attach(file.getFileName(), file.getInputstream(), file.getContentType());
            byte[] data = serviceCredito.sendFileMassive(attach);
            dataDownload = data;
            if (data != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado el archivo correctamente!"));
                this.downloadBtn = true;

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar un archivo con el formato correcto: (csv)"));
        }

    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     */
    public StreamedContent download() {
        String contentType = file.getContentType();
        return new DefaultStreamedContent(new ByteArrayInputStream(dataDownload),
                contentType, fileNameDownload);
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.attach = new Attach();
        this.file = null;
        this.serviceCredito = new NotaCreditoService();
        this.downloadBtn = false;
    }
}