
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteTipoNegocio;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.remote.ClienteTipoNegocioClient;

/**
 * Adaptador para la lista de Descuento
 * @author Romina Núñez
 */

public class ClienteTipoNegocioAdapter extends DataListAdapter<ClienteTipoNegocio> {
    
    /**
     * Cliente para los servicios de Documento
     */
    private final ClienteTipoNegocioClient serviceClienteTipoNegocio;

    /**
     * Constructor de InstallationListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ClienteTipoNegocioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClienteTipoNegocio = new ClienteTipoNegocioClient();
    }

    /**
     * Constructor de DocumentAdapter
     */
        public ClienteTipoNegocioAdapter() {
        super();
        this.serviceClienteTipoNegocio= new ClienteTipoNegocioClient();
    }


    @Override
    public ClienteTipoNegocioAdapter fillData(ClienteTipoNegocio searchData) {
        return serviceClienteTipoNegocio.getClienteTipoNegocioList(searchData, getFirstResult(), getPageSize());
    }

    
}
