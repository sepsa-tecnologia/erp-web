/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.repositorio.adapters.TipoRepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.filters.TipoRepositorioFilter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;

/**
 * Cliente para el servicio de Tipo Repositorio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoRepositorioService extends APIErpCore {

    /**
     * Lista de Tipo Repositorio
     *
     * @param ret
     * @param page
     * @param pageSize
     * @return
     */
    public TipoRepositorioAdapter getTipoRepositorioList(TipoRepositorio ret, Integer page, Integer pageSize) {

        TipoRepositorioAdapter lista = new TipoRepositorioAdapter();

        Map params = TipoRepositorioFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_REPOSITORIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoRepositorioAdapter.class);

            lista = (TipoRepositorioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_REPOSITORIO("Servicio para tipo Repositorio", "tipo-repositorio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
