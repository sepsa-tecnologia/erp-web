/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;

/**
 *
 * @author Ralf Adam
 */
public class PermisoListarCobro{

    public PermisoListarCobro() {
        this.puedeAnular = Boolean.TRUE;
        this.puedeDescargar = Boolean.TRUE;
    }

    
    protected Boolean puedeDescargar;
    protected Boolean puedeAnular;

    public Boolean getPuedeDescargar() {
        return puedeDescargar;
    }

    public void setPuedeDescargar(Boolean puedeDescargar) {
        this.puedeDescargar = puedeDescargar;
    }

    public Boolean getPuedeAnular() {
        return puedeAnular;
    }

    public void setPuedeAnular(Boolean puedeAnular) {
        this.puedeAnular = puedeAnular;
    }
    
}
