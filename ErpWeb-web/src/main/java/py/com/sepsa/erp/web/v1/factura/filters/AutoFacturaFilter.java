package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetalle;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class AutoFacturaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param id
     * @return
     */
    public AutoFacturaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de idPersona
     *
     * @param idPersona
     * @return
     */
    public AutoFacturaFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de naturaleza del vendedor
     *
     * @param idNaturalezaVendedor
     * @return
     */
    public AutoFacturaFilter idNaturalezaVendedor(Integer idNaturalezaVendedor) {
        if (idNaturalezaVendedor != null) {
            params.put("idNaturalezaVendedor", idNaturalezaVendedor);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public AutoFacturaFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public AutoFacturaFilter moneda(String moneda) {
        if (moneda != null && !moneda.trim().isEmpty()) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de tipo factura
     *
     * @param idTipoFactura
     * @return
     */
    public AutoFacturaFilter idTipoFactura(Integer idTipoFactura) {
        if (idTipoFactura != null) {
            params.put("idTipoFactura", idTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de tipo factura
     *
     * @param tipoFactura
     * @return
     */
    public AutoFacturaFilter tipoFactura(String tipoFactura) {
        if (tipoFactura != null && !tipoFactura.trim().isEmpty()) {
            params.put("tipoFactura", tipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro factura
     *
     * @param nroFactura
     * @return
     */
    public AutoFacturaFilter nroAutofactura(String nroAutofactura) {
        if (nroAutofactura != null && !nroAutofactura.trim().isEmpty()) {
            params.put("nroAutofactura", nroAutofactura);
        }
        return this;
    }


    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public AutoFacturaFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha vencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public AutoFacturaFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nombre de vendedor
     *
     * @param nombreVendedor
     * @return
     */
    public AutoFacturaFilter nombreVendedor(String nombreVendedor) {
        if (nombreVendedor != null && !nombreVendedor.trim().isEmpty()) {
            params.put("nombreVendedor", nombreVendedor);
        }
        return this;
    }

    /**
     * Agregar el filtro de direccion
     *
     * @param direccion
     * @return
     */
    public AutoFacturaFilter direccion(String direccion) {
        if (direccion != null && !direccion.trim().isEmpty()) {
            params.put("direccion", direccion);
        }
        return this;
    }

    /**
     * Agregar el filtro de RUC
     *
     * @param ruc
     * @return
     */
    public AutoFacturaFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public AutoFacturaFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public AutoFacturaFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public AutoFacturaFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de cobrado
     *
     * @param cobrado
     * @return
     */
    public AutoFacturaFilter cobrado(String cobrado) {
        if (cobrado != null && !cobrado.trim().isEmpty()) {
            params.put("cobrado", cobrado);
        }
        return this;
    }

    /**
     * Agregar el filtro de impreso
     *
     * @param impreso
     * @return
     */
    public AutoFacturaFilter impreso(String impreso) {
        if (impreso != null && !impreso.trim().isEmpty()) {
            params.put("impreso", impreso);
        }
        return this;
    }

    /**
     * Agregar el filtro de entregado
     *
     * @param entregado
     * @return
     */
    public AutoFacturaFilter entregado(String entregado) {
        if (entregado != null && !entregado.trim().isEmpty()) {
            params.put("entregado", entregado);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public AutoFacturaFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 5
     *
     * @param montoIva5
     * @return
     */
    public AutoFacturaFilter montoIva5(BigDecimal montoIva5) {
        if (montoIva5 != null) {
            params.put("montoIva5", montoIva5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 5
     *
     * @param montoImponible5
     * @return
     */
    public AutoFacturaFilter montoImponible5(BigDecimal montoImponible5) {
        if (montoImponible5 != null) {
            params.put("montoImponible5", montoImponible5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 5
     *
     * @param montoTotal5
     * @return
     */
    public AutoFacturaFilter montoTotal5(BigDecimal montoTotal5) {
        if (montoTotal5 != null) {
            params.put("montoTotal5", montoTotal5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 10
     *
     * @param montoIva10
     * @return
     */
    public AutoFacturaFilter montoIva10(BigDecimal montoIva10) {
        if (montoIva10 != null) {
            params.put("montoIva10", montoIva10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 10
     *
     * @param montoImponible10
     * @return
     */
    public AutoFacturaFilter montoImponible10(BigDecimal montoImponible10) {
        if (montoImponible10 != null) {
            params.put("montoImponible10", montoImponible10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 10
     *
     * @param montoTotal10
     * @return
     */
    public AutoFacturaFilter montoTotal10(BigDecimal montoTotal10) {
        if (montoTotal10 != null) {
            params.put("montoTotal10", montoTotal10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total excento
     *
     * @param montoTotalExento
     * @return
     */
    public AutoFacturaFilter montoTotalExento(BigDecimal montoTotalExento) {
        if (montoTotalExento != null) {
            params.put("montoTotalExento", montoTotalExento);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva total
     *
     * @param montoIvaTotal
     * @return
     */
    public AutoFacturaFilter montoIvaTotal(BigDecimal montoIvaTotal) {
        if (montoIvaTotal != null) {
            params.put("montoIvaTotal", montoIvaTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible total
     *
     * @param montoImponibleTotal
     * @return
     */
    public AutoFacturaFilter montoImponibleTotal(BigDecimal montoImponibleTotal) {
        if (montoImponibleTotal != null) {
            params.put("montoImponibleTotal", montoImponibleTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total factura
     *
     * @param montoTotalFactura
     * @return
     */
    public AutoFacturaFilter montoTotalFactura(BigDecimal montoTotalFactura) {
        if (montoTotalFactura != null) {
            params.put("montoTotalFactura", montoTotalFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public AutoFacturaFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nota de credito
     *
     * @param montoNotaCredito
     * @return
     */
    public AutoFacturaFilter montoNotaCredito(BigDecimal montoNotaCredito) {
        if (montoNotaCredito != null) {
            params.put("montoNotaCredito", montoNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de montoCobro
     *
     * @param montoCobro
     * @return
     */
    public AutoFacturaFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de retencion
     *
     * @param montoRetencion
     * @return
     */
    public AutoFacturaFilter montoRetencion(BigDecimal montoRetencion) {
        if (montoRetencion != null) {
            params.put("montoRetencion", montoRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro de saldo
     *
     * @param saldo
     * @return
     */
    public AutoFacturaFilter saldo(BigDecimal saldo) {
        if (saldo != null) {
            params.put("saldo", saldo);
        }
        return this;
    }

    /**
     * Agrega el filtro de detalles
     *
     * @param detalles
     * @return
     */
    public AutoFacturaFilter detalles(List<AutoFacturaDetalle> detalles) {
        if (detalles != null && !detalles.isEmpty()) {
            params.put("detalles", detalles);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene retencion, true o false
     *
     * @param tieneRetencion
     * @return
     */
    public AutoFacturaFilter tieneRetencion(String tieneRetencion) {
        if (tieneRetencion != null && !tieneRetencion.trim().isEmpty()) {
            params.put("tieneRetencion", tieneRetencion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro tiene saldo
     *
     * @param tieneSaldo
     * @return
     */
    public AutoFacturaFilter tieneSaldo(String tieneSaldo) {
        if (tieneSaldo != null && !tieneSaldo.trim().isEmpty()) {
            params.put("tieneSaldo", tieneSaldo.trim());
        }
        return this;
    }

    /**
     * Agregar el filtro de anhoMes
     *
     * @param anhoMes
     * @return
     */
    public AutoFacturaFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agregar el filtro de pagado
     *
     * @param pagado
     * @return
     */
    public AutoFacturaFilter pagado(String pagado) {
        if (pagado != null && !pagado.trim().isEmpty()) {
            params.put("pagado", pagado);
        }
        return this;
    }

    /**
     * Agregar el filtro codigoTipoFactura
     *
     * @param codigoTipoFactura
     * @return
     */
    public AutoFacturaFilter codigoTipoFactura(String codigoTipoFactura) {
        if (codigoTipoFactura != null && !codigoTipoFactura.trim().isEmpty()) {
            params.put("codigoTipoFactura", codigoTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param producto
     * @return
     */
    public AutoFacturaFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }


    /**
     * Construye el mapa de parametros
     *
     * @param factura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(AutoFactura factura, Integer page, Integer pageSize) {
        AutoFacturaFilter filter = new AutoFacturaFilter();

        filter
                .id(factura.getId())
                .idPersona(factura.getIdPersona())
                .idNaturalezaVendedor(factura.getIdNaturalezaVendedor())
                .nroAutofactura(factura.getNroAutofactura())
                .ruc(factura.getRuc())
                .tieneRetencion(factura.getTieneRetencion())
                .tieneSaldo(factura.getTieneSaldo())
                .nombreVendedor(factura.getNombreVendedor())
                .cobrado(factura.getCobrado())
                .codigoTipoFactura(factura.getCodigoTipoFactura())
                .anulado(factura.getAnulado())
                .idMoneda(factura.getIdMoneda())
                .fechaDesde(factura.getFechaDesde())
                .fechaHasta(factura.getFechaHasta())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public AutoFacturaFilter() {
        super();
    }
}
