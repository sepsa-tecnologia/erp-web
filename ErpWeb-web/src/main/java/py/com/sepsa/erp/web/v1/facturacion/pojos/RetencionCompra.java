package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Pojo para listado de retención de compra
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class RetencionCompra {

    /**
     * Identificador de retención
     */
    private Integer id;
    /**
     * Identificador de cliente
     */
    private Integer idPersona;
    /**
     * Parámetro nroRetencion
     */
    private String nroRetencion;
    /**
     * Parámetro fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * Parámetro fecha
     */
    private Date fecha;
    /**
     * Parámetro fechaDesde
     */
    private Date fechaDesde;
    /**
     * Parámetro fechaHasta
     */
    private Date fechaHasta;
    /**
     * Parámetro porcentajeRetencion
     */
    private BigDecimal porcentajeRetencion;
    /**
     * Parámetro montoImponibleTotal
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Parámetro montoIvaTotal
     */
    private BigDecimal montoIvaTotal;
    /**
     * Parámetro montoTotal
     */
    private BigDecimal montoTotal;
    /**
     * Parámetro montoRetenidoTotal
     */
    private BigDecimal montoRetenidoTotal;
    /**
     * Parámetro idEstado
     */
    private Integer idEstado;
    /**
     * Parámetro idMotivoAnulacion
     */
    private Integer idMotivoAnulacion;
    /**
     * Parámetro observacionAnulacion
     */
    private String observacionAnulacion;
    /**
     * Pojo de estado
     */
    private Estado estado;
    /**
     * Pojo de persona
     */
    private Persona persona;
    /**
     * Pojo de motivoAnulacion
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Identificador para factura compra
     */
    private Integer idFacturaCompra;
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public Integer getId() {
        return id;

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public BigDecimal getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(BigDecimal porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public RetencionCompra() {
    }

    public RetencionCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

}
