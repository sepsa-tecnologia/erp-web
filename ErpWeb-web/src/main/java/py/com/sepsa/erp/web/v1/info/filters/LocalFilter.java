package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Filtro utilizado para el servicio de local
 *
 * @author Cristina Insfrán
 * @author Sergio D. Riveros Vazquez
 */
public class LocalFilter extends Filter {

    /**
     * Agrega el filtro de identificador del local
     *
     * @param id Identificador del local
     * @return
     */
    public LocalFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para gln
     *
     * @param gln gln del local
     * @return
     */
    public LocalFilter gln(String gln) {
        if (gln != null && !gln.trim().isEmpty()) {
            params.put("gln", gln);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador idExterno
     *
     * @param idExterno Identificador
     * @return
     */
    public LocalFilter idExterno(Integer idExterno) {
        if (idExterno != null) {
            params.put("idExterno", idExterno);
        }
        return this;
    }

    /**
     * Agrega el filtro del activo
     *
     * @param activo Estado de local
     * @return
     */
    public LocalFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro del activo
     *
     * @param activo Estado de local
     * @return
     */
    public LocalFilter localExterno(String localExterno) {
        if (localExterno != null && !localExterno.trim().isEmpty()) {
            params.put("localExterno", localExterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion Descripcion del local
     * @return
     */
    public LocalFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de la observación
     *
     * @param observacion observación sobre el local
     * @return
     */
    public LocalFilter observacion(String observacion) {
        if (observacion != null && !observacion.trim().isEmpty()) {
            params.put("observacion", observacion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public LocalFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de la persona
     *
     * @param persona persona del local
     * @return
     */
    public LocalFilter persona(Persona persona) {
        if (persona != null) {
            params.put("persona", persona);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param local datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Local local, Integer page, Integer pageSize) {
        LocalFilter filter = new LocalFilter();

        filter
                .id(local.getId())
                .gln(local.getGln())
                .idExterno(local.getIdExterno())
                .activo(local.getActivo())
                .descripcion(local.getDescripcion())
                .observacion(local.getObservacion())
                .idPersona(local.getIdPersona())
                .persona(local.getPersona())
                .localExterno(local.getLocalExterno())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de LocalFilter
     */
    public LocalFilter() {
        super();
    }
}
