package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;

/**
 * POJO para reporte de comprobante de venta
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ReporteComprobanteCompraParam {

    /**
     * Identificador de persona
     */
    private Boolean anual;
    /**
     * Fecha
     */
    private Date fecha;

    public Boolean getAnual() {
        return anual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setAnual(Boolean anual) {
        this.anual = anual;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * Constructor
     */
    public ReporteComprobanteCompraParam() {

    }
}
