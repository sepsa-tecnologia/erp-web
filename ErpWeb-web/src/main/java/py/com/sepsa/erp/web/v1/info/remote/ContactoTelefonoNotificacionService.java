package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoNotificacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoNotificacionAsociadoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoTelefonoNotificacionFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefonoNotificacion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de contacto email
 *
 * @author Romina Núñez
 */
public class ContactoTelefonoNotificacionService extends APIErpCore {

    /**
     * Obtiene la lista de contacto
     *
     * @param contactoTelefonoNotificacion
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoTelefonoNotificacionAdapter getContactoTelefonoNotificacionList(ContactoTelefonoNotificacion contactoTelefonoNotificacion, Integer page,
            Integer pageSize) {

        ContactoTelefonoNotificacionAdapter lista = new ContactoTelefonoNotificacionAdapter();

        Map params = ContactoTelefonoNotificacionFilter.build(contactoTelefonoNotificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTACTO_TELEFONO_NOTIFICACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoTelefonoNotificacionAdapter.class);

            lista = (ContactoTelefonoNotificacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
    /**
     * Método para editar contacto telefono notificacion
     *
     * @param contactoTelefonoNotificacion 
     * @return
     */
    public ContactoTelefonoNotificacion editContactoTelefonoNotificacion(ContactoTelefonoNotificacion contactoTelefonoNotificacion) {

        ContactoTelefonoNotificacion cTelefonoEdit = new ContactoTelefonoNotificacion();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CONTACTO_TELEFONO_NOTIFICACION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(contactoTelefonoNotificacion));

            response = BodyResponse.createInstance(conn, ContactoTelefonoNotificacion.class);

            cTelefonoEdit = ((ContactoTelefonoNotificacion) response.getPayload());

        }
        return cTelefonoEdit;
    }

    /**
     * Obtiene la lista de contacto
     *
     * @param contactoTelefonoNotificacion
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoTelefonoNotificacionAsociadoAdapter getContactoTelefonoNotificacionAsociadoList(ContactoTelefonoNotificacion contactoTelefonoNotificacion, Integer page,
            Integer pageSize) {

        ContactoTelefonoNotificacionAsociadoAdapter lista = new ContactoTelefonoNotificacionAsociadoAdapter();

        Map params = ContactoTelefonoNotificacionFilter.build(contactoTelefonoNotificacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTACTO_TELEFONO_NOTIFICACION_ASOCIADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoTelefonoNotificacionAsociadoAdapter.class);

            lista = (ContactoTelefonoNotificacionAsociadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de ContactoEmailService
     */
    public ContactoTelefonoNotificacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CONTACTO_TELEFONO_NOTIFICACION("Servicio Contacto Telefono Notificacion", "contacto-telefono-notificacion"),
        CONTACTO_TELEFONO_NOTIFICACION_ASOCIADO("Servicio Contacto Telefono Notificacion", "contacto-telefono-notificacion/asociado");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
