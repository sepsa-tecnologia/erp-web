
package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO  del local tipo negocio
 * @author Sepsa
 */
public class LocalTipoNegocio {

    
    /**
     * Identificador del local
     */
    private Integer idLocal;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del tipo de negocio
     */
    private Integer idTipoNegocio;
    
    /**
     * @return the idLocal
     */
    public Integer getIdLocal() {
        return idLocal;
    }

    /**
     * @param idLocal the idLocal to set
     */
    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the idTipoNegocio
     */
    public Integer getIdTipoNegocio() {
        return idTipoNegocio;
    }

    /**
     * @param idTipoNegocio the idTipoNegocio to set
     */
    public void setIdTipoNegocio(Integer idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }
    
    /**
     * Constructor
     */
    public LocalTipoNegocio(){
        
    }
    
    /**
     * Constructor con parametros
     * @param idLocal
     * @param idPersona
     * @param idTipoNegocio
     */
    public LocalTipoNegocio(Integer idLocal, Integer idPersona,
            Integer idTipoNegocio){
        this.idLocal=idLocal;
        this.idPersona=idPersona;
        this.idTipoNegocio=idTipoNegocio;
    }
    
}
