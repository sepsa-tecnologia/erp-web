package py.com.sepsa.erp.web.v1.servicio.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServicioAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;

/**
 * TODO: corregir comentarios.
 *
 * @author alext
 */
@ViewScoped
@Named("listarServicio")
public class ServicioListarController implements Serializable {

    /**
     * Adaptador de servicios.
     */
    private ServicioAdapter adapter;

    /**
     * Objeto Servicios.
     */
    private Servicios searchData;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public ServicioAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ServicioAdapter adapter) {
        this.adapter = adapter;
    }

    public Servicios getSearchData() {
        return searchData;
    }

    public void setSearchData(Servicios searchData) {
        this.searchData = searchData;
    }

    //</editor-fold>
    
    /**
     * Método para filtrar servicio.
     */
    public void filter() {
        this.adapter = adapter.fillData(searchData);
    }
    
    /**
     * Método para limpiar filtros.
     */
    public void limpiar(){
        searchData = new Servicios();
        filter();
    }

    /**
     * Método para generar lista de clientes según servicios.
     */
    public void search() {
        adapter = new ServicioAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new Servicios();
        search();
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public ServicioListarController() {
        adapter = new ServicioAdapter();
        searchData = new Servicios();
    }

}
