package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDatoPersona;

/**
 * Filtro para el servicio de tipo etiqueta
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDatoPersonaFilter extends Filter {

    /**
     * Agrega el filtro de identificador del tipo etiqueta
     *
     * @param id
     * @return
     */
    public TipoDatoPersonaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de código
     *
     * @param codigo
     * @return
     */
    public TipoDatoPersonaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoDatoPersonaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoDatoPersona datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoDatoPersona tipoDatoPersona, Integer page, Integer pageSize) {
        TipoDatoPersonaFilter filter = new TipoDatoPersonaFilter();

        filter
                .id(tipoDatoPersona.getId())
                .codigo(tipoDatoPersona.getCodigo())
                .descripcion(tipoDatoPersona.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor TipoTelefonoFilter
     */
    public TipoDatoPersonaFilter() {
        super();
    }

}
