/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoProcesoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para tipo porceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoProcesoService extends APIErpCore {

    /**
     * Obtiene la lista de tipo de procesos.
     *
     * @param tipoProceso
     * @param page
     * @param pageSize
     * @return
     */
    public TipoProcesoAdapter getTipoProcesoList(TipoProceso tipoProceso, Integer page,
            Integer pageSize) {

        TipoProcesoAdapter lista = new TipoProcesoAdapter();

        Map params = TipoProcesoFilter.build(tipoProceso, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_PROCESO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoProcesoAdapter.class);

            lista = (TipoProcesoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de ProductoService
     */
    public TipoProcesoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        TIPO_PROCESO("Listado de tipos de procesos", "tipo-proceso");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
