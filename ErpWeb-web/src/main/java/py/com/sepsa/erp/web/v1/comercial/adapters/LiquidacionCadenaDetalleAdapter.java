package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionCadenaDetalle;
import py.com.sepsa.erp.web.v1.comercial.remote.LiquidacionCadenaDetalleService;

/**
 * Adaptador para lista de detalle de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionCadenaDetalleAdapter extends DataListAdapter<LiquidacionCadenaDetalle> {

    private final LiquidacionCadenaDetalleService service;

    /**
     * Método para cargar la lista de detalle de liquidaciones por periodo.
     *
     * @param searchData
     * @return
     */
    @Override
    public LiquidacionCadenaDetalleAdapter fillData(LiquidacionCadenaDetalle searchData) {
        return service.getLiquidacionesCadenaDetalle(searchData, getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de LiquidacionCadenaDetalleAdapter.
     *
     * @param page
     * @param pageSize
     */
    public LiquidacionCadenaDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new LiquidacionCadenaDetalleService();
    }

    /**
     * Constructor de LiquidacionCadenaDetalleAdapter.
     */
    public LiquidacionCadenaDetalleAdapter() {
        super();
        this.service = new LiquidacionCadenaDetalleService();
    }

}
