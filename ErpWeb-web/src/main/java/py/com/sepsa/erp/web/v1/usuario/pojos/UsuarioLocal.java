/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Local;

/**
 *
 * @author SEPSA
 */
public class UsuarioLocal {
    /**
     * Identificador de usuario local
     */
    private Integer id;
    /**
     * Identificador de local
     */
    private Integer idLocal;
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    /**
     * Activo
     */
    private String activo;
    /**
     * Local
     */
    private Local local;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public UsuarioLocal() {
    }
    
}
