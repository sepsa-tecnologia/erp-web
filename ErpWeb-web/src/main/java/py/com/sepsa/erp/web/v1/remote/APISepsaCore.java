package py.com.sepsa.erp.web.v1.remote;

import javax.faces.context.FacesContext;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * APISepsaCore
 *
 * @author Gustavo Benítez L.
 */
public class APISepsaCore extends API {

    //Parámetros de conexións
    private final static String BASE_API = "SepsaCore-api/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;

    /**
     * Constructor de API
     */
    public APISepsaCore() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);
        InfoPojo info = InfoPojo.createInstance("api-sepsa-core");
        updateConnInfo(info);
    }

    public APISepsaCore(String token) {
        super(null, null, 0, BASE_API, CONN_TIMEOUT, token);
        InfoPojo info = InfoPojo.createInstance("api-sepsa-core");
        updateConnInfo(info);
    }

    @Override
    protected void setBearer() {
        FacesContext context = FacesContext.getCurrentInstance();
        SessionData session = context.getApplication().evaluateExpressionGet(context, "#{sessionData}", SessionData.class);
        InfoPojo info = InfoPojo.createInstance("api-sepsa-core");

        this.jwt = session == null ? null : info.getApiTokenSepsaCore();
    }
}
