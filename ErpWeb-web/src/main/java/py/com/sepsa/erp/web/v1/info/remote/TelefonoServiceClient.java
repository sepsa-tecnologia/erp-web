
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TelefonoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de telefono
 * @author Cristina Insfrán
 */
public class TelefonoServiceClient extends APIErpCore{
    
     /**
     * Método para crear telefono
     *
     * @param telefono
     * @return
     */
    public Integer setTelefono(Telefono telefono) {

        Integer idTelefono = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(telefono));

            response = BodyResponse.createInstance(conn, Telefono.class);

            idTelefono = ((Telefono) response.getPayload()).getId();
           
        }
        return idTelefono;
    }
    
    /**
     * Método para editar teléfono
     */
     public BodyResponse putTelefono(Telefono telefono) {


        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(telefono));

            response = BodyResponse.createInstance(conn, Telefono.class);

           
        }
        return response;
    }
     
         /**
     * Método para obtener el listado de telefono
     *
     * @param telefono
     * @param page
     * @param pageSize
     * @return
     */
    public TelefonoListAdapter getTelefonoList(Telefono telefono, Integer page,
            Integer pageSize) {

        TelefonoListAdapter lista = new TelefonoListAdapter();

        Map params = TelefonoFilter.build(telefono, page, pageSize);

        HttpURLConnection conn = GET(Resource.TELEFONO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TelefonoListAdapter.class);

            lista = (TelefonoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
     
    /**
     * Constructor de TelefonoServiceClient
     */
    public TelefonoServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TELEFONO("Servicio Telefono", "telefono");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
