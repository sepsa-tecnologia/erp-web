package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ReporteClienteMoraListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteClienteMora;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteClienteMoraService;

/**
 * Controlador del reporte cliente
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("reporteCliente")
public class ReporteClienteController implements Serializable {

    /**
     * @return the tipoReporte
     */
    public String getTipoReporte() {
        return tipoReporte;
    }

    /**
     * @param tipoReporte the tipoReporte to set
     */
    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    /**
     * Adaptador de la liste de reporte cliente
     */
    private ReporteClienteMoraListAdapter adapterReporteCliente;
    /**
     * Adaptador de la lista de reporte cliente resumen
     */
    private ReporteClienteMoraListAdapter adapterReporteClienteResumen;
    /**
     * Lista de años seleccionables
     */
    private List<SelectItem> listAnhos = new ArrayList();
    /**
     * Servicio cliente
     */
    private ReporteClienteMoraService reporteClienteService;
    /**
     * Objero del resumen
     */
    private ReporteClienteMora reporteClienteMora;
    /**
     * Variable visible tipoReporte
     */
    private String tipoReporte;

    //<editor-fold defaultstate="collapsed" desc="***Getters y Setters***">
    /**
     * @return the reporteClienteMora
     */
    public ReporteClienteMora getReporteClienteMora() {
        return reporteClienteMora;
    }

    /**
     * @param reporteClienteMora the reporteClienteMora to set
     */
    public void setReporteClienteMora(ReporteClienteMora reporteClienteMora) {
        this.reporteClienteMora = reporteClienteMora;
    }

    /**
     * @return the reporteClienteService
     */
    public ReporteClienteMoraService getReporteClienteService() {
        return reporteClienteService;
    }

    /**
     * @param reporteClienteService the reporteClienteService to set
     */
    public void setReporteClienteService(ReporteClienteMoraService reporteClienteService) {
        this.reporteClienteService = reporteClienteService;
    }

    /**
     * @return the adapterReporteClienteResumen
     */
    public ReporteClienteMoraListAdapter getAdapterReporteClienteResumen() {
        return adapterReporteClienteResumen;
    }

    /**
     * @param adapterReporteClienteResumen the adapterReporteClienteResumen to
     * set
     */
    public void setAdapterReporteClienteResumen(ReporteClienteMoraListAdapter adapterReporteClienteResumen) {
        this.adapterReporteClienteResumen = adapterReporteClienteResumen;
    }

    /**
     * @return the listAnhos
     */
    public List<SelectItem> getListAnhos() {
        return listAnhos;
    }

    /**
     * @param listAnhos the listAnhos to set
     */
    public void setListAnhos(List<SelectItem> listAnhos) {
        this.listAnhos = listAnhos;
    }

    /**
     * @return the adapterReporteCliente
     */
    public ReporteClienteMoraListAdapter getAdapterReporteCliente() {
        return adapterReporteCliente;
    }

    /**
     * @param adapterReporteCliente the adapterReporteCliente to set
     */
    public void setAdapterReporteCliente(ReporteClienteMoraListAdapter adapterReporteCliente) {
        this.adapterReporteCliente = adapterReporteCliente;
    }

//</editor-fold>
    /**
     *
     * Método para filtrar el reporte
     */
    public void filterReporte() {

        if (adapterReporteCliente.getFilter().getMesHasta() != null
                && adapterReporteCliente.getFilter().getAnhoHasta() != null
                && !adapterReporteCliente.getFilter().getAnhoHasta().equals(0)) {

            Calendar fechaDesde = Calendar.getInstance();
            if (adapterReporteCliente.getFilter().getMesDesde() != null
                    && adapterReporteCliente.getFilter().getAnhoDesde() != null
                    && !adapterReporteCliente.getFilter().getAnhoDesde().equals(0)) {
                fechaDesde.set(adapterReporteCliente.getFilter().getAnhoDesde(),
                        adapterReporteCliente.getFilter().getMesDesde(), 1);
                adapterReporteCliente.getFilter().setFechaDesde(fechaDesde.getTime());
                adapterReporteClienteResumen.getFilter().setFechaDesde(fechaDesde.getTime());

            }
           tipoReporte=adapterReporteCliente.getFilter().getTipoReporte();
           adapterReporteClienteResumen.getFilter().setTipoReporte(adapterReporteCliente.getFilter().getTipoReporte());
            Calendar fechaHasta = Calendar.getInstance();
            fechaDesde.set(adapterReporteCliente.getFilter().getAnhoHasta(),
                    adapterReporteCliente.getFilter().getMesHasta(), 1);

            adapterReporteCliente.getFilter().setFechaHasta(fechaHasta.getTime());
            adapterReporteCliente.getFilter().setResumen(Boolean.FALSE);
            adapterReporteCliente.setFilter(adapterReporteCliente.getFilter());

            adapterReporteCliente = adapterReporteCliente.fillData(adapterReporteCliente.getFilter());

            adapterReporteClienteResumen.getFilter().setFechaHasta(fechaHasta.getTime());
            adapterReporteClienteResumen.getFilter().setResumen(Boolean.TRUE);

            adapterReporteClienteResumen.setFilter(reporteClienteService.getReporteResumen(adapterReporteClienteResumen.getFilter(), null, null));

        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapterReporteCliente = new ReporteClienteMoraListAdapter();
        this.adapterReporteClienteResumen = new ReporteClienteMoraListAdapter();
        this.reporteClienteService = new ReporteClienteMoraService();
        this.reporteClienteMora = new ReporteClienteMora();

        //adapterReporteClienteResumen.getFilter().setTipoReporte("CLIENTE");
        adapterReporteCliente.getFilter().setTipoReporte("CLIENTE");

        filterReporte();
    }
}
