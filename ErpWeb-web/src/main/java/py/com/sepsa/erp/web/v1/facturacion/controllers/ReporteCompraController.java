package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteCompraServiceClient;

/**
 * Controlador para reporte de compra
 *
 * @author alext
 */
@ViewScoped
@Named("reporteCompra")
public class ReporteCompraController implements Serializable {

    /**
     * Parámetros para reporte
     */
    private ReporteParam parametro;

    /**
     * Cliente para el servicio de descarga de archivo
     */
    private ReporteCompraServiceClient reporteCliente;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public ReporteParam getParametro() {
        return parametro;
    }

    public void setParametro(ReporteParam parametro) {
        this.parametro = parametro;
    }

    public ReporteCompraServiceClient getReporteCliente() {
        return reporteCliente;
    }

    public void setReporteCliente(ReporteCompraServiceClient reporteCliente) {
        this.reporteCliente = reporteCliente;
    }
    //</editor-fold>

    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {

        Instant from = parametro.getFechaDesde().toInstant();
        Instant to = parametro.getFechaHasta().toInstant();

        long days = ChronoUnit.DAYS.between(from, to);

        boolean download = true;

        if (days > 31) {
            download = false;
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fecha Incorrecta", "El rango de fecha máximo es de 31 dias"));
        }

        if (download == true) {
            String contentType = ("application/vnd.ms-excel");
            String fileName = "reporte-compra.xlsx";
            byte[] data = reporteCliente.getReporteCompra(parametro);
            if (data != null) {
                return new DefaultStreamedContent(new ByteArrayInputStream(data),
                        contentType, fileName);
            }

        }
        return null;
    }

    /**
     * Incializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteParam();
        this.reporteCliente = new ReporteCompraServiceClient();

        Calendar today = Calendar.getInstance();
        parametro.setFechaDesde(today.getTime());
        parametro.setFechaHasta(today.getTime());
    }

}
