package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("facturaCompraDetalle")
public class FacturaCompraDetalleController implements Serializable {

    /**
     * Servicio Facturacion
     */
    private FacturacionService serviceFactura;
    /**
     * POJO Factura
     */
    private Factura facturaDetalle;
    /**
     * Dato de factura
     */
    private String idFactura;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setFacturaDetalle(Factura facturaDetalle) {
        this.facturaDetalle = facturaDetalle;
    }

    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public Factura getFacturaDetalle() {
        return facturaDetalle;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public String getIdFactura() {
        return idFactura;
    }

//</editor-fold>
    /**
     * Método para obtener los detalles de la factura
     */
    public void obtenerFacturaDetalle() {
        facturaDetalle.setId(Integer.parseInt(idFactura));

        facturaDetalle = serviceFactura.getFacturaCompraDetalle(facturaDetalle);

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idFactura = params.get("id");

        this.serviceFactura = new FacturacionService();
        this.facturaDetalle = new Factura();

        obtenerFacturaDetalle();
    }

}
