
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO Dirección
 * @author Cristina Insfrán
 */
public class Direccion {


    /**
     * Identificador de la dirección
     */
    private Integer id;
    /**
     * Número
     */
    private String numero;
    /**
     * observación de la dirección
     */
    private String observacion;
    /**
     * Latitud de la dirección
     */
    private String latitud;
    /**
     * Longitud de la dirección
     */
    private String longitud;
    /**
     * Dirección
     */
    private String direccion;
    /**
     * Referencia de la dirección
     */
    private String referencia;
    /**
     * Identificador del departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador del distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de la ciudad
     */
    private Integer idCiudad;
    /**
     * Departamento
     */
    private ReferenciaGeografica  departamento;
    /**
     * Distrito
     */
    private ReferenciaGeografica distrito;
    /**
     * Ciudad 
     */
    private ReferenciaGeografica ciudad;
    /**
     * Bandera cliente extranjero
     */
    private String extranjero;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }
    
    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }
    
    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * @return the latitud
     */
    public String getLatitud() {
        return latitud;
    }
    
    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
    
    /**
     * @return the longitud
     */
    public String getLongitud() {
        return longitud;
    }
    
    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
    
    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }
    
    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * @return the idDepartamento
     */
    public Integer getIdDepartamento() {
        return idDepartamento;
    }
    
    /**
     * @param idDepartamento the idDepartamento to set
     */
    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    
    /**
     * @return the idDistrito
     */
    public Integer getIdDistrito() {
        return idDistrito;
    }
    
    /**
     * @param idDistrito the idDistrito to set
     */
    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }
    
    /**
     * @return the idCiudad
     */
    public Integer getIdCiudad() {
        return idCiudad;
    }
    
    /**
     * @param idCiudad the idCiudad to set
     */
    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }
    
    
    /**
     * @return the departamento
     */
    public ReferenciaGeografica getDepartamento() {
        return departamento;
    }
    
    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(ReferenciaGeografica departamento) {
        this.departamento = departamento;
    }
    
    /**
     * @return the distrito
     */
    public ReferenciaGeografica getDistrito() {
        return distrito;
    }
    
    /**
     * @param distrito the distrito to set
     */
    public void setDistrito(ReferenciaGeografica distrito) {
        this.distrito = distrito;
    }
    
    /**
     * @return the ciudad
     */
    public ReferenciaGeografica getCiudad() {
        return ciudad;
    }
    
    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(ReferenciaGeografica ciudad) {
        this.ciudad = ciudad;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }
//</editor-fold>
    /**
     * Constructor con parametros
     */
    public Direccion(){
        
    }
}
