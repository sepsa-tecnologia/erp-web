/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TipoDocumentoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para Tipo Documento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de objetos
     *
     * @param tipoDocumento Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoDocumentoAdapter getTipoDocumentoList(TipoDocumento tipoDocumento, Integer page,
            Integer pageSize) {

        TipoDocumentoAdapter lista = new TipoDocumentoAdapter();

        Map params = TipoDocumentoFilter.build(tipoDocumento, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_DOCUMENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoDocumentoAdapter.class);

            lista = (TipoDocumentoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de clase
     */
    public TipoDocumentoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_DOCUMENTO("Tipo Documento", "tipo-documento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
