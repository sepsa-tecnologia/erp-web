/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.VendedorListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.Vendedorfilter;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 *
 * @author Gustavo Benítez
 */
public class VendedorService extends APIErpCore {

    /**
     * Método para crear Vendedor
     *
     * @param vendedor
     * @return
     */
    public BodyResponse<VendedorPojo> create(VendedorPojo vendedor) {

        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(VendedorService.Resource.VENDEDOR.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(vendedor));

            response = BodyResponse.createInstance(conn, VendedorPojo.class);

        }
        return response;
    }

    /**
     * Método para crear Vendedor
     *
     * @param vendedor
     * @return
     */
    public BodyResponse<VendedorPojo> edit(VendedorPojo vendedor) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(VendedorService.Resource.VENDEDOR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(vendedor));
            response = BodyResponse.createInstance(conn, VendedorPojo.class);
        }
        return response;
    }

    /**
     * Obtiene la lista de Vendedores
     *
     * @param vendedor
     * @param page
     * @param pageSize
     * @return
     */
    public VendedorListAdapter list(VendedorPojo vendedor, Integer page,
            Integer pageSize) {

        VendedorListAdapter lista = new VendedorListAdapter();
        Map params = Vendedorfilter.build(vendedor, page, pageSize);

        HttpURLConnection conn = GET(VendedorService.Resource.VENDEDOR.url, ContentType.JSON, params);
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn, VendedorListAdapter.class);
            lista = (VendedorListAdapter) response.getPayload();
            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor
     */
    public VendedorService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        VENDEDOR("ServicioS de Vendedor", "vendedor");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
