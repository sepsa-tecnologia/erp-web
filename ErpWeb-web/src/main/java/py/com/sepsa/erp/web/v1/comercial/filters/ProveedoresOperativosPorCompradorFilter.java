package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProveedoresOperativosPorComprador;

/**
 * Filtro para reporte de proveedores operativos por comprador.
 *
 * @author alext
 */
public class ProveedoresOperativosPorCompradorFilter extends Filter {

    /**
     * Filtro por idProveedor.
     *
     * @param idProveedor
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter idProveedor(Integer idProveedor) {
        if (idProveedor != null) {
            params.put("idProveedor", idProveedor);
        }
        return this;
    }

    /**
     * Filtro por proveedor.
     *
     * @param proveedor
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter proveedor(String proveedor) {
        if (proveedor != null && !proveedor.trim().isEmpty()) {
            params.put("proveedor", proveedor);
        }
        return this;
    }

    /**
     * Filtro por idComprador.
     *
     * @param idComprador
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter idComprador(Integer idComprador) {
        if (idComprador != null) {
            params.put("idComprador", idComprador);
        }
        return this;
    }

    /**
     * Filtro por comprador.
     *
     * @param comprador
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter comprador(String comprador) {
        if (comprador != null && !comprador.trim().isEmpty()) {
            params.put("comprador", comprador);
        }
        return this;
    }

    /**
     * Filtro por tipoTarifa.
     *
     * @param tipoTarifa
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter tipoTarifa(String tipoTarifa) {
        if (tipoTarifa != null && !tipoTarifa.trim().isEmpty()) {
            params.put("tipoTarifa", tipoTarifa);
        }
        return this;
    }

    /**
     * Filtro por tarifa.
     *
     * @param tarifa
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter tarifa(String tarifa) {
        if (tarifa != null && !tarifa.trim().isEmpty()) {
            params.put("tarifa", tarifa);
        }
        return this;
    }

    /**
     * Filtro por montoTarifa.
     *
     * @param montoTarifa
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter montoTarifa(Integer montoTarifa) {
        if (montoTarifa != null) {
            params.put("montoTarifa", montoTarifa);
        }
        return this;
    }

    /**
     * Filtro por montoDescuento.
     *
     * @param montoDescuento
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter montoDescuento(Integer montoDescuento) {
        if (montoDescuento != null) {
            params.put("montoDescuento", montoDescuento);
        }
        return this;
    }

    /**
     * Filtro por montoTotal.
     *
     * @param montoTotal
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter montoTotal(Integer montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal);
        }
        return this;
    }

    /**
     * Filtro por cantDocEnviados.
     *
     * @param cantDocEnviados
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter cantDocEnviados(Integer cantDocEnviados) {
        if (cantDocEnviados != null) {
            params.put("cantDocEnviados", cantDocEnviados);
        }
        return this;
    }

    /**
     * Filtro por cantDocRecibidos.
     *
     * @param cantDocRecibidos
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter cantDocRecibidos(Integer cantDocRecibidos) {
        if (cantDocRecibidos != null) {
            params.put("cantDocRecibidos", cantDocRecibidos);
        }
        return this;
    }
    
    /**
     * Filtro por ano.
     *
     * @param ano
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter ano(String ano) {
        if (ano != null && !ano.trim().isEmpty()) {
            params.put("ano", ano);
        }
        return this;
    }

    /**
     * Filtro por mes.
     *
     * @param mes
     * @return
     */
    public ProveedoresOperativosPorCompradorFilter mes(String mes) {
        if (mes != null && !mes.trim().isEmpty()) {
            params.put("mes", mes);
        }
        return this;
    }

    /**
     * Construye el mapa de par{ametros.
     *
     * @param proveedoresOperativosPorComprador
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(ProveedoresOperativosPorComprador proveedoresOperativosPorComprador,
            Integer page, Integer pageSize) {

        ProveedoresOperativosPorCompradorFilter filter = new ProveedoresOperativosPorCompradorFilter();

        filter
                .idProveedor(proveedoresOperativosPorComprador.getIdProveedor())
                .proveedor(proveedoresOperativosPorComprador.getProveedor())
                .idComprador(proveedoresOperativosPorComprador.getIdComprador())
                .comprador(proveedoresOperativosPorComprador.getComprador())
                .tipoTarifa(proveedoresOperativosPorComprador.getTipoTarifa())
                .tarifa(proveedoresOperativosPorComprador.getTarifa())
                .montoTarifa(proveedoresOperativosPorComprador.getMontoTarifa())
                .montoDescuento(proveedoresOperativosPorComprador.getMontoDescuento())
                .montoTotal(proveedoresOperativosPorComprador.getMontoTotal())
                .cantDocEnviados(proveedoresOperativosPorComprador.getCantDocEnviados())
                .cantDocRecibidos(proveedoresOperativosPorComprador.getCantDocRecibidos())
                .ano(proveedoresOperativosPorComprador.getAno())
                .mes(proveedoresOperativosPorComprador.getMes())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor.
     */
    public ProveedoresOperativosPorCompradorFilter() {
        super();
    }

}
