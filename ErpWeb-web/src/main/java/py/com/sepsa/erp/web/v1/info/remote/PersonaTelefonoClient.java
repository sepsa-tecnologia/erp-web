
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaTelefonoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de persona telefono
 * @author Cristina Insfrán
 */
public class PersonaTelefonoClient extends APIErpCore{
    
    /**
     * Método para crear persona telefono
     *
     * @param personaTelefono
     * @return
     */
    public BodyResponse setTelefonoPersona(PersonaTelefono  personaTelefono) {


        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERSONA_TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaTelefono));

            response = BodyResponse.createInstance(conn, PersonaTelefono.class);
           
           
        }
        return response;
    }
    
      /**
     * Método para crear/editar persona telefono
     *
     * @param personaTelefono
     * @return
     */
    public BodyResponse putTelefonoPersona(PersonaTelefono  personaTelefono) {


        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PERSONA_TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaTelefono));

            response = BodyResponse.createInstance(conn, PersonaTelefono.class);
           
           
        }
        return response;
    }
    
    /**
     * Obtiene la lista de persona email
     *
     * @param personaTelefono 
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaTelefonoAdapter getPersonaTelefono(PersonaTelefono personaTelefono, Integer page,
            Integer pageSize) {

        PersonaTelefonoAdapter lista = new PersonaTelefonoAdapter();

        Map params = PersonaTelefonoFilter.build(personaTelefono, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_TELEFONO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaTelefonoAdapter.class);

            lista = (PersonaTelefonoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
     /**
     * Elimina telefono-persona
     * @param personaTelefono  
     * @return 
     */
    public Boolean deletePersonaTelefono(PersonaTelefono personaTelefono) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = DELETE(Resource.PERSONA_TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaTelefono));

            response = BodyResponse.createInstance(conn, PersonaTelefono.class);

        }
        return response.getSuccess();
    }
    
    /**
     * Constructor de PersonaTelefonoClient
     */
    public PersonaTelefonoClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        PERSONA_TELEFONO("Servicio Persona Telefono", "persona-telefono");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
