
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoNotificacion;
import py.com.sepsa.erp.web.v1.info.remote.TipoNotificacionServiceClient;

/**
 * Adaptador de la lista de Tipo Notificacion
 * @author Romina Núñez
 */
public class TipoNotificacionAdapter extends DataListAdapter<TipoNotificacion> {
    
    /**
     * Cliente para el servicio de Tipo Notificacion
     */
    private final TipoNotificacionServiceClient tipoNotificacionService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoNotificacionAdapter fillData(TipoNotificacion searchData) {
     
        return tipoNotificacionService.getTipoNotificacionList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de TipoNotificacionAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoNotificacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoNotificacionService = new TipoNotificacionServiceClient();
    }

    /**
     * Constructor de TipoNotificacionAdapter
     */
    public TipoNotificacionAdapter() {
        super();
        this.tipoNotificacionService = new TipoNotificacionServiceClient();
    }
}
