package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para persona Email
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class PersonaEmail {

    /**
     * Identificadro
     */
    private Integer id;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del email
     */
    private Integer idEmail;
    /**
     * Activo
     */
    private String activo;
    /**
     * Objeto Email
     */
    private Email email;
    /**
     * Persona
     */
    private Persona persona;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

//</editor-fold>
    /**
     * Constructor de la clase
     */
    public PersonaEmail() {

    }

}
