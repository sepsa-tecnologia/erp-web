/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.DevolucionAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.DevolucionDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.DevolucionDetalleFilter;
import py.com.sepsa.erp.web.v1.inventario.filters.DevolucionFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.inventario.pojos.DevolucionDetalle;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Cliente para el servicio de Operacion inventario
 *
 * @author Romina Núñez
 */
public class DevolucionService extends APIErpInventario {

    /**
     * Obtiene la lista de operaciones de inventario
     *
     * @param devolucion Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public DevolucionAdapter getDevolucion(Devolucion devolucion, Integer page,
            Integer pageSize) {
        DevolucionAdapter lista = new DevolucionAdapter();
        try {
            Map params = DevolucionFilter.build(devolucion, page, pageSize);
            HttpURLConnection conn = GET(Resource.DEVOLUCION.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        DevolucionAdapter.class);
                lista = (DevolucionAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Obtiene la lista de operaciones de inventario
     *
     * @param devolucionDetalle Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public DevolucionDetalleAdapter getDevolucionDetalle(DevolucionDetalle devolucionDetalle, Integer page,
            Integer pageSize) {
        DevolucionDetalleAdapter lista = new DevolucionDetalleAdapter();
        try {
            Map params = DevolucionDetalleFilter.build(devolucionDetalle, page, pageSize);
            HttpURLConnection conn = GET(Resource.DEVOLUCION_DETALLE.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        DevolucionDetalleAdapter.class);
                lista = (DevolucionDetalleAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param devolucion
     * @return
     */
    public BodyResponse<Devolucion> createDevolucion(Devolucion devolucion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.DEVOLUCION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(devolucion));
            response = BodyResponse.createInstance(conn, Devolucion.class);
        }
        return response;
    }

    /**
     * Constructor de clase
     */
    public DevolucionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        DEVOLUCION("Devolucion", "devolucion"),
        DEVOLUCION_DETALLE("Devolucion Detalle", "devolucion-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
