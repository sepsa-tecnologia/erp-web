package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;
import py.com.sepsa.erp.web.v1.system.pojos.Token;
import py.com.sepsa.erp.web.v1.system.pojos.User;
import py.com.sepsa.erp.web.v1.system.pojos.UsuarioEmpresa;
import py.com.sepsa.erp.web.v1.system.remote.LoginServiceClient;

/**
 * Controlador para la vista de HOME
 *
 * @author Romina Núñez
 */
@ManagedBean(name = "empresaSeleccionController")
@ViewScoped
public class EmpresaSeleccionController implements Serializable {

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Datos del usuario
     */
    @Inject
    private MenuController menu;
    /**
     * Lista de perfiles habilitados
     */
    private List<UsuarioEmpresa> empresasHabilitadas = new ArrayList();
    /**
     *
     */
    private Integer idEmpresa;
    /**
     *
     */
    private String descripcionEmpresa;
    /**
     * Servicios para login al sistema
     */
    private LoginServiceClient serviceClient;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresasHabilitadas(List<UsuarioEmpresa> empresasHabilitadas) {
        this.empresasHabilitadas = empresasHabilitadas;
    }

    public List<UsuarioEmpresa> getEmpresasHabilitadas() {
        return empresasHabilitadas;
    }

    public void setDescripcionEmpresa(String descripcionEmpresa) {
        this.descripcionEmpresa = descripcionEmpresa;
    }

    public String getDescripcionEmpresa() {
        return descripcionEmpresa;
    }
//</editor-fold>

    /**
     * Método para cargar la lista de perfiles habilitados
     */
    public void cargarLista() {
        this.empresasHabilitadas = new ArrayList<>();
        empresasHabilitadas = session.getUser().getUsuarioEmpresas();
        empresasHabilitadas.sort(Comparator.comparing(e -> e.getEmpresa().getDescripcion()));
    }

    /**
     * Definir Perfil
     */
    public String definirTokenEmpresa() {
        String token = null;

        BodyResponse response = serviceClient.tokenEmpresa(idEmpresa);
        if (response.getSuccess()) {

            try {
                session.setJwt(((Token) response.getPayload()).getToken());
                token = session.getJwt();
                WebLogger.get().debug("TOKEN =" + token);
                obtenerDescripcionEmpresa();
                menu.init();

                return "/app/home.xhtml?faces-redirect=true;";

            } catch (Exception ex) {
                WebLogger.get().fatal(ex);
            }
        } else {
            WebLogger.get().debug("ERROR");
        }

        //     menu.setMenuAdapter();
        return null;
    }

    /**
     * Método para obtener el nombre de la empresa
     */
    public void obtenerDescripcionEmpresa() {
        User nuevaInfoUser = new User();
        nuevaInfoUser = session.getUser();
        for (UsuarioEmpresa empresasHabilitada : empresasHabilitadas) {
            if (empresasHabilitada.getIdEmpresa().equals(idEmpresa)) {
                descripcionEmpresa = empresasHabilitada.getEmpresa().getDescripcion();
                session.setNombreEmpresa(descripcionEmpresa);
                nuevaInfoUser.setIdEmpresa(empresasHabilitada.getEmpresa().getId());
                nuevaInfoUser.setCodigoTipoEmpresa(empresasHabilitada.getEmpresa().getTipoEmpresa().getCodigo());
                nuevaInfoUser.setModuloInventario(empresasHabilitada.getEmpresa().getModuloInventario());
                session.setUser(nuevaInfoUser);
                break;
            }
        }
    }

    /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {
        this.idEmpresa = null;
        this.descripcionEmpresa = null;
        this.serviceClient = new LoginServiceClient();
        cargarLista();
    }

}
