/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoArchivo;

/**
 * Filter para tipo archivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoArchivoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoArchivoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoArchivoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agregar el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoArchivoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoArchivo datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoArchivo tipoArchivo, Integer page, Integer pageSize) {
        TipoArchivoFilter filter = new TipoArchivoFilter();
        
        filter
                .id(tipoArchivo.getId())
                .descripcion(tipoArchivo.getDescripcion())
                .codigo(tipoArchivo.getCodigo())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoArchivoFilter() {
        super();
    }
    
}
