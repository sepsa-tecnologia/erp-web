package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.List;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;

/**
 * Pojo para listado de menú
 *
 * @author alext
 */
public class MenuPojo {

    /**
     * Identificador de la entidad
     */
    private Integer id;

    /**
     * Identificador de la entidad Empresa
     */
    private Integer idEmpresa;

    /**
     * Parámetro empresa
     */
    private Empresa empresa;

    /**
     * Parámetro codigo
     */
    private String codigo;

    /**
     * Parámetro descripcion
     */
    private String descripcion;

    /**
     * Parámetro titulo
     */
    private String titulo;

    /**
     * Parámetro url
     */
    private String url;

    /**
     * Parámetro activo
     */
    private String activo;

    /**
     * Parámetro orden
     */
    private Integer orden;

    /**
     * Parámetro idPadre
     */
    private Integer idPadre;

    /**
     * Parámetro idTipoMenu
     */
    private Integer idTipoMenu;

    /**
     * Lista de elementos hijos
     */
    private List<MenuPojo> hijos;

    /**
     * Parámetro Padre
     */
    private MenuPojo padre;

    /**
     * Parámetro tipoMenu
     */
    private TipoMenu tipoMenu;
    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public List<MenuPojo> getHijos() {
        return hijos;
    }

    public void setHijos(List<MenuPojo> hijos) {
        this.hijos = hijos;
    }

    public MenuPojo getPadre() {
        return padre;
    }

    public void setPadre(MenuPojo padre) {
        this.padre = padre;
    }

    public Integer getIdTipoMenu() {
        return idTipoMenu;
    }

    public void setIdTipoMenu(Integer idTipoMenu) {
        this.idTipoMenu = idTipoMenu;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public TipoMenu getTipoMenu() {
        return tipoMenu;
    }

    public void setTipoMenu(TipoMenu tipoMenu) {
        this.tipoMenu = tipoMenu;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public MenuPojo() {
    }

    /**
     * Constructor con parámetros
     *
     * @param id
     */
    public MenuPojo(Integer id) {
        this.id = id;
    }

}
