
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ConceptoFactura;
import py.com.sepsa.erp.web.v1.comercial.remote.ConceptoFacturaService;

/**
 * Adaptador para la lista de TipoTarifa
 * @author Romina Núñez
 */

public class ConceptoFacturaAdapter extends DataListAdapter<ConceptoFactura> {
    
    /**
     * Cliente para los servicios de ConceptoFactura
     */
    private final ConceptoFacturaService serviceConceptoFactura;

    /**
     * Constructor de ConceptoFacturaAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConceptoFacturaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceConceptoFactura = new ConceptoFacturaService();
    }

    /**
     * Constructor de ConceptoFacturaAdapter
     */
        public ConceptoFacturaAdapter() {
        super();
        this.serviceConceptoFactura = new ConceptoFacturaService();
    }


    @Override
    public ConceptoFacturaAdapter fillData(ConceptoFactura searchData) {
        return serviceConceptoFactura.getConceptoFacturaList(searchData, getFirstResult(), getPageSize());
    }

    
}
