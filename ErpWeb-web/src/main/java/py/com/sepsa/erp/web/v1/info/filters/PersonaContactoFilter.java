package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;

/**
 * Filtro para el servicio persona contacto
 *
 * @author Cristina Insfrán,Sergio D. Riveros Vazquez
 */
public class PersonaContactoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public PersonaContactoFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del contacto
     *
     * @param idContacto Identificador del usuario
     * @return
     */
    public PersonaContactoFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de contacto
     *
     * @param contacto
     * @return
     */
    public PersonaContactoFilter contacto(Contacto contacto) {
        if (contacto != null) {
            params.put("contacto", contacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de estado
     *
     * @param activo
     * @return
     */
    public PersonaContactoFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param dato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(PersonaContacto dato, Integer page, Integer pageSize) {
        PersonaContactoFilter filter = new PersonaContactoFilter();

        filter
                .idPersona(dato.getIdPersona())
                .idContacto(dato.getIdContacto())
                .contacto(dato.getContacto())
                .activo(dato.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PersonaContacto
     */
    public PersonaContactoFilter() {
        super();
    }

}
