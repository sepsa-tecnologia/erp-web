/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionInternoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para la vista crear motivo emision Interno
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionInternoCreate")
public class MotivoEmisionInternoCreateController implements Serializable {

    /**
     * Cliente para el servicio
     */
    private final MotivoEmisionInternoService service;
    /**
     * POJO del motivoEmisionInterno
     */
    private MotivoEmisionInterno motivoEmisionInterno;
    /**
     * Adaptador para la lista de motivoEmisions
     */
    private MotivoEmisionAdapter motivoEmisionAdapterList;
    /**
     * POJO de motivoEmision
     */
    private MotivoEmision motivoEmisionFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoEmisionInterno getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public MotivoEmisionAdapter getMotivoEmisionAdapterList() {
        return motivoEmisionAdapterList;
    }

    public void setMotivoEmisionAdapterList(MotivoEmisionAdapter motivoEmisionAdapterList) {
        this.motivoEmisionAdapterList = motivoEmisionAdapterList;
    }

    public MotivoEmision getMotivoEmisionFilter() {
        return motivoEmisionFilter;
    }

    public void setMotivoEmisionFilter(MotivoEmision motivoEmisionFilter) {
        this.motivoEmisionFilter = motivoEmisionFilter;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {

        BodyResponse<MotivoEmisionInterno> respuestaTal = 
                service.setMotivoEmisionInterno(motivoEmisionInterno);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, 
                            "Info", "Motivo de Emision creado correctamente!"));
            init();
        }

    }

    /**
     * Método para obtener motivoEmision
     */
    public void getMotivoEmisiones() {
        this.setMotivoEmisionAdapterList(getMotivoEmisionAdapterList().fillData(getMotivoEmisionFilter()));
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivoEmisionInterno = new MotivoEmisionInterno();
        this.motivoEmisionAdapterList = new MotivoEmisionAdapter();
        this.motivoEmisionFilter = new MotivoEmision();
        getMotivoEmisiones();
    }

    /**
     * Constructor
     */
    public MotivoEmisionInternoCreateController() {
        init();
        this.service = new MotivoEmisionInternoService();
    }

}
