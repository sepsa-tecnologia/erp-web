/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MarcaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar productos info
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("listarProductoInfo")
public class ProductoListController implements Serializable {

    /**
     * Adaptador de producto.
     */
    private ProductoAdapter adapter;
    /**
     * Objeto producto.
     */
    private Producto productoFilter;
    /**
     * Adaptador para la lista de Marca para autocomplete
     */
    private MarcaAdapter marcaAdapterListAux;
    /**
     * POJO de Marca para autocomplete
     */
    private Marca marcaFilterAux;
    
        /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    /**
     * @return the adapter
     */
    public ProductoAdapter getAdapter() {
        return adapter;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(ProductoAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * @return the productoFilter
     */
    public Producto getProductoFilter() {
        return productoFilter;
    }

    /**
     * @param productoFilter the productoFilter to set
     */
    public void setProductoFilter(Producto productoFilter) {
        this.productoFilter = productoFilter;
    }

    public MarcaAdapter getMarcaAdapterListAux() {
        return marcaAdapterListAux;
    }

    public void setMarcaAdapterListAux(MarcaAdapter marcaAdapterListAux) {
        this.marcaAdapterListAux = marcaAdapterListAux;
    }

    public Marca getMarcaFilterAux() {
        return marcaFilterAux;
    }

    public void setMarcaFilterAux(Marca marcaFilterAux) {
        this.marcaFilterAux = marcaFilterAux;
    }

    //</editor-fold>
    /**
     * Método para filtrar productos.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(productoFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.productoFilter = new Producto();
        this.marcaFilterAux = new Marca();
        buscar();
    }

    /**
     * Metodo para redirigir a la vista Editar Talonario
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("producto-edit?faces-redirect=true&id=%d", id);
    }

    /* Método para obtener las marcas filtrados
     *
     * @param query
     * @return
     */
    public List<Marca> completeQuery(String query) {
        Marca marcaFilterAux = new Marca();
        marcaFilterAux.setDescripcion(query);
        marcaAdapterListAux = marcaAdapterListAux.fillData(marcaFilterAux);

        return marcaAdapterListAux.getData();
    }
    
        /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }
    
        /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        productoFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }
    

    /**
     * Selecciona marca
     *
     * @param event
     */
    public void onItemSelectMarcaFilter(SelectEvent event) {
        productoFilter.setIdMarca(((Marca) event.getObject()).getId());
    }

    @PostConstruct
    public void init() {
        try {
            this.adapter = new ProductoAdapter();
            this.productoFilter = new Producto();
            this.marcaAdapterListAux = new MarcaAdapter();
            this.marcaFilterAux = new Marca();
            this.empresaAutocomplete = new Empresa();
            this.empresaAdapter = new EmpresaAdapter();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
