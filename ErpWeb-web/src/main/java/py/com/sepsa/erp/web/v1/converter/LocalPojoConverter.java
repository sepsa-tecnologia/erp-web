/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;

/**
 *
 * @author Romina Núñez
 */
@FacesConverter("localPojoConverter")
public class LocalPojoConverter implements Converter{
    
      /**
     * Servicios para login al sistema
     */
    private LocalService serviceLocal;
    
    @Override
    public Local getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceLocal = new LocalService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            Local loc=new Local();
            loc.setId(val);
            List<Local> locales = serviceLocal.getLocalList(loc, 0, 10).getData();
            if(locales != null && !locales.isEmpty()) {
                return locales.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un local válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Local) {
                return String.valueOf(((Local)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
