package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.task.GeneracionDteFacturaMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import static py.com.sepsa.erp.web.v1.system.pojos.DownloadUtils.sendFile;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;
import py.com.sepsa.utils.comercial.adapters.ClienteAdapter;

/**
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("facturaCargaMasiva")
public class FacturaCargaMasivaController implements Serializable {

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Archivo de instructivo para la carga de facturas
     */
    private StreamedContent loadFacturaCarga;
    /**
     * Objeto Attach
     */
    private Attach attach;
    /**
     * Producto Service
     */
    private FacturacionService serviceFacturacion;
    /**
     * Archivo
     */
    private UploadedFile file;
    /**
     * Datos para descarga
     */
    private byte[] dataDownload;
    /**
     * Bandera para descargar
     */
    private boolean downloadBtn;
    /**
     * Pojo Factura
     */
    private Factura factura;
        /**
     * Pojo Factura
     */
    private Factura facturaFilter;
    /**
     * Adapter de Factura
     */
    private FacturaAdapter adapterFactura;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    /**
     * nroFactura generada
     */
    private String nroFacturaCreada;
    /**
     * Identificador de factura generada;
     */
    private Integer idFacturaCreada;
    /**
     * Pojo cliente
     */
    private Cliente cliente;
    /**
     * Pojo cliente para filtro
     */
    private Cliente clienteFilter;
    /**
     * Adapter de cliente
     */
    private ClientListAdapter adapterCliente;
    
    public UploadedFile getFile() {
        return file;
    }

    public void setDownloadBtn(boolean downloadBtn) {
        this.downloadBtn = downloadBtn;
    }

    public boolean isDownloadBtn() {
        return downloadBtn;
    }

    public void setDataDownload(byte[] dataDownload) {
        this.dataDownload = dataDownload;
    }

    public byte[] getDataDownload() {
        return dataDownload;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void setServiceFacturacion(FacturacionService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    public void setLoadFacturaCarga(StreamedContent loadFacturaCarga) {
        this.loadFacturaCarga = loadFacturaCarga;
    }   

    public FacturacionService getServiceFacturacion() {
        return serviceFacturacion;
    }

    /**
     * Método para descargar el archivo plantilla
     *
     * @return
     */
    public StreamedContent getLoadFacturaCarga() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/app/factura/download/factura-matriz-carga.csv");
        loadFacturaCarga = new DefaultStreamedContent(stream, "text/csv", "factura-matriz-carga.csv");
        return loadFacturaCarga;
    }
    
    
     public void onDescargarTemplate() {
        File file = null;
        try {
            InputStream stream = FacturaCargaMasivaController.class.getResourceAsStream("/template/factura-matriz-carga.csv");
            sendFile(stream, "factura-matriz-carga.csv", -1, "text/csv");
        } catch (Exception thr) {
            WebLogger.get().debug("Ocurrió un error descargando archivo de muestra");
        } finally {
            if (file != null) {
                file.delete();
            }
        }
    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     * @throws IOException
     */
    public void upload() throws IOException {
        try {
            if (file != null) {
                attach = new Attach(file.getFileName(), file.getInputstream(), file.getContentType());
                Map result = serviceFacturacion.sendFile(attach);  
                String csv = result.get(0).toString();
                byte[] data = csv.getBytes();
                List<String> idsNotificar = (List) result.get(1);
                dataDownload = data;
                if (data != null) {
                    if (data.length>0){
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado el archivo, para ver detalles del procesamiento descargue el archivo!"));
                        this.downloadBtn = true;
                        try {
                            GeneracionDteFacturaMultiempresa.generar(session.getUser().getIdEmpresa());
                        } catch (Exception e) {
                            WebLogger.get().fatal(e);
                        }
//                        if(idsNotificar.size()>0){
//                            for (String idFactura : idsNotificar){
//                                obtenerDatosFactura(idFactura);
//                                notificar(factura.getEmail());
//                            }  
//                        }
                    }
                    else{
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al procesar el archivo. Archivo incorrecto o mal estructurado!"));
                    }
                } else {
                     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo!"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Archivo sin adjuntar, favor seleccionar archivo!"));
            }
        }
        catch(Exception e){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo! Asegurese de haber seleccionado algún archivo para su envío"));
        }
    }

    /**
     * Método para subir y enviar el archivo
     *
     * @return
     */
    public StreamedContent download() {
        String contentType = file.getContentType();

        return new DefaultStreamedContent(new ByteArrayInputStream(dataDownload),
                contentType, "detalle-procesamiento.csv");
    }
    /**
     * Método para obtener configuración
     * 
     * @param code
     * @return 
     */
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }
    
    public void notificar(String emailDestino) {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String cliente = this.cliente.getRazonSocial();
        String empresa = session.getNombreEmpresa();
        String mailTo = emailDestino;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su recibo en formato PDF";

        String subjet = "Factura No. " + nroFacturaCreada;
        String url = "factura/consulta/" + idFacturaCreada;
        byte[] data = fileServiceClient.download(url);
        String fileName = nroFacturaCreada + ".pdf";
        

        notificarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, nroFacturaCreada, host, port, empresa, emailEmpresa, telefonoEmpresa);

        
    }
    
     public void notificarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            Boolean notificado = MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host,port);  
            if (notificado) {
                LocalDate fecha = LocalDate.now();
                // Convertir LocalDate a Date
                Date fechaHoy = Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
                factura.setFechaEntrega(fechaHoy);
                factura.setEntregado("C");
                serviceFacturacion.editFactura(factura);
                
            } else {
                // actualizar campo "entregado" a P (Pendiente)
                factura.setEntregado("P");
                serviceFacturacion.editFactura(factura);
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

     
     public void obtenerDatosFactura(String idFactura){
         facturaFilter.setId(Integer.parseInt(idFactura));
         adapterFactura = adapterFactura.fillData(facturaFilter);
         this.factura = adapterFactura.getData().get(0);
         this.nroFacturaCreada = factura.getNroFactura();
         this.idFacturaCreada = factura.getId();

         clienteFilter.setIdCliente(factura.getIdCliente());
         adapterCliente = adapterCliente.fillData(clienteFilter);
         cliente = adapterCliente.getData().get(0);
         
     }
     

     
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.attach = new Attach();
        this.file = null;
        this.serviceFacturacion = new FacturacionService();
        this.downloadBtn = false;
        this.adapterFactura = new FacturaAdapter();
        this.factura = new Factura();
        this.facturaFilter = new Factura();
        this.fileServiceClient = new FileServiceClient();
        this.cliente = new Cliente();
        this.clienteFilter = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        
    }
}
