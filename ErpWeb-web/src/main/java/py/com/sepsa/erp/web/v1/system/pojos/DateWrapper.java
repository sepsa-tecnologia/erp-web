package py.com.sepsa.erp.web.v1.system.pojos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * POJO para parametros de fechas
 * @author Daniel F. Escauriza Arza
 */
public class DateWrapper {
    
    /**
     * Fecha
     */
    private Date date;

    /**
     * Obtiene la fecha
     * @return Fecha
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setea la fecha
     * @param date Fecha
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Constructor de DateWrapper
     * @param date Fecha en formato String
     */
    public DateWrapper(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            this.date = sdf.parse(date);
        } catch(ParseException ex) {
            WebLogger.get().fatal(ex);
        }
        
    }
    
}
