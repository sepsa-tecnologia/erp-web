package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionPeriodoDetalle;

/**
 * Filtro para detalle de liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionPeriodoDetalleFilter extends Filter {

    /**
     * Filtro por idHistoricoLiquidacion.
     *
     * @param idHistoricoLiquidacion
     * @return
     */
    public LiquidacionPeriodoDetalleFilter idHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        if (idHistoricoLiquidacion != null) {
            params.put("idHistoricoLiquidacion", idHistoricoLiquidacion);
        }
        return this;
    }

    /**
     * Filtro por nroLinea.
     *
     * @param nroLinea
     * @return
     */
    public LiquidacionPeriodoDetalleFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Filtro por idTarifa.
     *
     * @param idTarifa
     * @return
     */
    public LiquidacionPeriodoDetalleFilter idTarifa(Integer idTarifa) {
        if (idTarifa != null) {
            params.put("idTarifa", idTarifa);
        }
        return this;
    }

    /**
     * Filtro por idCliente.
     *
     * @param idCliente
     * @return
     */
    public LiquidacionPeriodoDetalleFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Filtro por cliente.
     *
     * @param cliente
     * @return
     */
    public LiquidacionPeriodoDetalleFilter cliente(String cliente) {
        if (cliente != null && !cliente.trim().isEmpty()) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Filtro por clienteRel.
     *
     * @param clienteRel
     * @return
     */
    public LiquidacionPeriodoDetalleFilter clienteRel(String clienteRel) {
        if (clienteRel != null && !clienteRel.trim().isEmpty()) {
            params.put("clienteRel", clienteRel);
        }
        return this;
    }

    /**
     * Filtro por idGrupoClienteRel.
     *
     * @param idGrupoClienteRel
     * @return
     */
    public LiquidacionPeriodoDetalleFilter idGrupoClienteRel(Integer idGrupoClienteRel) {
        if (idGrupoClienteRel != null) {
            params.put("idGrupoClienteRel", idGrupoClienteRel);
        }
        return this;
    }

    /**
     * Filtro por montoTarifa.
     *
     * @param montoTarifa
     * @return
     */
    public LiquidacionPeriodoDetalleFilter montoTarifa(Integer montoTarifa) {
        if (montoTarifa != null) {
            params.put("montoTarifa", montoTarifa);
        }
        return this;
    }

    /**
     * Filtro por montoDescuento.
     *
     * @param montoDescuento
     * @return
     */
    public LiquidacionPeriodoDetalleFilter montoDescuento(Integer montoDescuento) {
        if (montoDescuento != null) {
            params.put("montoDescuento", montoDescuento);
        }
        return this;
    }

    /**
     * Filtro por montoTotal.
     *
     * @param montoTotal
     * @return
     */
    public LiquidacionPeriodoDetalleFilter montoTotal(Integer montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal);
        }
        return this;
    }

    /**
     * Filtro por mes.
     *
     * @param mes
     * @return
     */
    public LiquidacionPeriodoDetalleFilter mes(String mes) {
        if (mes != null && !mes.trim().isEmpty()) {
            params.put("mes", mes);
        }
        return this;
    }

    /**
     * Filtro por año.
     *
     * @param ano
     * @return
     */
    public LiquidacionPeriodoDetalleFilter ano(String ano) {
        if (ano != null && !ano.trim().isEmpty()) {
            params.put("ano", ano);
        }
        return this;
    }

    public static Map build(LiquidacionPeriodoDetalle liquidacionPeriodoDetalle, Integer page, Integer pageSize) {
        LiquidacionPeriodoDetalleFilter filter = new LiquidacionPeriodoDetalleFilter();

        filter
                .idHistoricoLiquidacion(liquidacionPeriodoDetalle.getIdHistoricoLiquidacion())
                .nroLinea(liquidacionPeriodoDetalle.getNroLinea())
                .idTarifa(liquidacionPeriodoDetalle.getIdTarifa())
                .idCliente(liquidacionPeriodoDetalle.getIdCliente())
                .cliente(liquidacionPeriodoDetalle.getCliente())
                .clienteRel(liquidacionPeriodoDetalle.getClienteRel())
                .idGrupoClienteRel(liquidacionPeriodoDetalle.getIdGrupoClienteRel())
                .montoTarifa(liquidacionPeriodoDetalle.getMontoTarifa())
                .montoDescuento(liquidacionPeriodoDetalle.getMontoDescuento())
                .montoTotal(liquidacionPeriodoDetalle.getMontoTotal())
                .mes(liquidacionPeriodoDetalle.getMes())
                .ano(liquidacionPeriodoDetalle.getAno())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de LiquidacionPeriodoDetalleFilter.
     */
    public LiquidacionPeriodoDetalleFilter() {
        super();
    }

}
