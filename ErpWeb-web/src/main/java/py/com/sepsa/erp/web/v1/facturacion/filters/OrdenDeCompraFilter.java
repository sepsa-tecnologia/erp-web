/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para Orden de Compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OrdenDeCompraFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public OrdenDeCompraFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador estado
     *
     * @param idEstado
     * @return
     */
    public OrdenDeCompraFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaRecepcionDesde
     *
     * @param fechaRecepcionDesde
     * @return
     */
    public OrdenDeCompraFilter fechaRecepcionDesde(Date fechaRecepcionDesde) {
        if (fechaRecepcionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

                String date = simpleDateFormat.format(fechaRecepcionDesde);
                params.put("fechaRecepcionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaRecepcionHasta
     *
     * @param fechaRecepcionHasta
     * @return
     */
    public OrdenDeCompraFilter fechaRecepcionHasta(Date fechaRecepcionHasta) {
        if (fechaRecepcionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

                String date = simpleDateFormat.format(fechaRecepcionHasta);
                params.put("fechaRecepcionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercionDesde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public OrdenDeCompraFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

                String date = simpleDateFormat.format(fechaInsercionDesde);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercionHasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public OrdenDeCompraFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de Nro Orden de Compra
     *
     * @param nroOrdenCompra
     * @return
     */
    public OrdenDeCompraFilter nroOrdenCompra(String nroOrdenCompra) {
        if (nroOrdenCompra != null && !nroOrdenCompra.trim().isEmpty()) {
            params.put("nroOrdenCompra", nroOrdenCompra);
        }
        return this;
    }

    /**
     * Agregar el filtro de Nro Orden de Compra
     *
     * @param nroOrdenCompra
     * @return
     */
    public OrdenDeCompraFilter recibido(String recibido) {
        if (recibido != null && !recibido.trim().isEmpty()) {
            params.put("recibido", recibido);
        }
        return this;
    }

    /**
     * Agregar el filtro de Nro Orden de Compra
     *
     * @param nroOrdenCompra
     * @return
     */
    public OrdenDeCompraFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param ordenDeCompra
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(OrdenDeCompra ordenDeCompra, Integer page, Integer pageSize) {
        OrdenDeCompraFilter filter = new OrdenDeCompraFilter();

        filter
                .id(ordenDeCompra.getId())
                .fechaRecepcionDesde(ordenDeCompra.getFechaRecepcionDesde())
                .fechaRecepcionHasta(ordenDeCompra.getFechaRecepcionHasta())
                .fechaInsercionDesde(ordenDeCompra.getFechaInsercionDesde())
                .fechaInsercionHasta(ordenDeCompra.getFechaInsercionHasta())
                .nroOrdenCompra(ordenDeCompra.getNroOrdenCompra())
                .codigoEstado(ordenDeCompra.getCodigoEstado())
                .idEstado(ordenDeCompra.getIdEstado())
                .recibido(ordenDeCompra.getRecibido())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public OrdenDeCompraFilter() {
        super();
    }
}
