/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.task;

import py.com.sepsa.erp.dte.generator.FacturaGeneratorRunnable;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorUtils;
import py.com.sepsa.erp.web.v1.info.remote.EmpresaService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.utils.automation.AbstractTask;
import py.com.sepsa.utils.automation.Task;
import py.com.sepsa.utils.info.remote.LoginService;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Jonathan
 */
public class GeneracionDteFacturaMultiempresa extends AbstractTask {

    /**
     * Identificador de empresa
     */
    private final Integer idEmpresa;

    public GeneracionDteFacturaMultiempresa(Proceso proceo, Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
        this.task = new Task(proceo.getId(),
                String.format("%s - IdEmpresa:%d", proceo.getCodigo(), idEmpresa),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getFrecuencia()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getHora()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getMinuto()));
    }

    @Override
    public void task() {
        try {
            
            String user = ConfiguracionValorUtils.getUser(idEmpresa);
            String pass = ConfiguracionValorUtils.getPass(idEmpresa);

            String token = LoginService.doLogin(user, pass);
            token = token != null && !token.toLowerCase().startsWith("bearer ")
                    ? String.format("Bearer %s", token)
                    : token;
            
            Empresa efilter = new Empresa();
            EmpresaService empresaServiceClient = new EmpresaService();
            empresaServiceClient.setJwt(token);
            EmpresaAdapter eadapter = empresaServiceClient.getEmpresasFacturasPendientes(efilter, 0, 1000);
            for (Empresa empresa : eadapter.getData()) {
                generar(empresa.getId());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public static void generar(Integer idEmpresa) {
        generar(idEmpresa, null);
    }

    public static void generar(Integer idEmpresa, Integer idFactura) {
        try {
            String apiToken = ConfiguracionValorUtils.getApiToken(idEmpresa);
            String user = ConfiguracionValorUtils.getUser(idEmpresa);
            String pass = ConfiguracionValorUtils.getPass(idEmpresa);
            String tipoNotificacion = ConfiguracionValorUtils.getTipoNotificacion(idEmpresa);

            FacturaGeneratorRunnable acpvr = new FacturaGeneratorRunnable(apiToken, user, pass, tipoNotificacion, idEmpresa);
            if (idFactura != null) {
                acpvr.generarFactura(idFactura);
            } else {
                acpvr.run();
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
