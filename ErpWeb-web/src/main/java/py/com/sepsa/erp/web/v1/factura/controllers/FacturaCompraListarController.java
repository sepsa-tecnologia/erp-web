package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionCompraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para Factura Compra
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("listarFacturaCompra")
public class FacturaCompraListarController implements Serializable {

    /**
     * Adaptador para la lista de facturas
     */
    private FacturaCompraAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Servicio Factura
     */
    private FacturacionService serviceFactura;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO moneda
     */
    private Moneda moneda;
    /**
     * Pojo de retención
     */
    private RetencionCompra retencion;
    /**
     * Adaptador de la lista de retención
     */
    private RetencionCompraListAdapter retencionAsociadosAdapter;
    /**
     * Objeto notaCreditoCompra.
     */
    private NotaCreditoCompra notaCreditoCompra;
    /**
     * Adaptador de notaCreditoCompra.
     */
    private NotaCreditoCompraAdapter notaCreditoAsociadosAdapter;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public FacturaCompraAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setAdapterFactura(FacturaCompraAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public RetencionCompra getRetencion() {
        return retencion;
    }

    public void setRetencion(RetencionCompra retencion) {
        this.retencion = retencion;
    }

    public RetencionCompraListAdapter getRetencionAsociadosAdapter() {
        return retencionAsociadosAdapter;
    }

    public void setRetencionAsociadosAdapter(RetencionCompraListAdapter retencionAsociadosAdapter) {
        this.retencionAsociadosAdapter = retencionAsociadosAdapter;
    }

    public NotaCreditoCompra getNotaCreditoCompra() {
        return notaCreditoCompra;
    }

    public void setNotaCreditoCompra(NotaCreditoCompra notaCreditoCompra) {
        this.notaCreditoCompra = notaCreditoCompra;
    }

    public NotaCreditoCompraAdapter getNotaCreditoAsociadosAdapter() {
        return notaCreditoAsociadosAdapter;
    }

    public void setNotaCreditoAsociadosAdapter(NotaCreditoCompraAdapter notaCreditoAsociadosAdapter) {
        this.notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter;
    }

    //</editor-fold>
    /**
     * Metodo para obtener lista de facturas
     */
    public void filterFacturas() {
        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    /**
     * Método para obtener lista de retención
     *
     * @param idFacturaCompra
     */
    public void filterRetencion(Integer idFacturaCompra) {
        this.retencion.setIdFacturaCompra(idFacturaCompra);
        getRetencionAsociadosAdapter().setFirstResult(0);
        retencionAsociadosAdapter = retencionAsociadosAdapter.fillData(retencion);
    }

    /**
     * Método para obtener nota de credito
     *
     * @param idFacturaCompra
     */
    public void filterNotaCredito(Integer idFacturaCompra) {
        this.notaCreditoCompra.setIdFacturaCompra(idFacturaCompra);
        getNotaCreditoAsociadosAdapter().setFirstResult(0);
        notaCreditoAsociadosAdapter = notaCreditoAsociadosAdapter.fillData(notaCreditoCompra);
    }

    /**
     * Metodo para obtener los doc asociados
     *
     * @param idFacturaCompra
     */
    public void verDocAsociados(Integer idFacturaCompra) {
        filterRetencion(idFacturaCompra);
        filterNotaCredito(idFacturaCompra);
    }

    /**
     * Metodo para limpiar los filtros
     */
    public void clear() {
        facturaFilter = new Factura();
        filterFacturas();
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleURL(Integer id) {
        return String.format("factura-compra-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    /**
     * Método para anular factura
     *
     * @param factura
     */
    public void anularFactura(Factura factura) {
        Factura fact = new Factura();
        fact.setId(factura.getId());
        fact.setIdMotivoAnulacion(factura.getIdMotivoAnulacion());
        fact.setObservacionAnulacion(factura.getObservacionAnulacion());
        BodyResponse<Factura> respuestaFactura = new BodyResponse<>();
        respuestaFactura = serviceFactura.anularFacturaCompra(fact);

        if (respuestaFactura.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura anulada correctamente!"));
            facturaFilter = new Factura();
            filterFacturas();

        }
    }

    /**
     * * Método para filtrar las monedas
     */
    public void filterMoneda() {
        adapterMoneda = adapterMoneda.fillData(moneda);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String notaCreditoURL(Integer id) {

        return String.format("nota-credito-compra-create"
                + "?faces-redirect=true"
                + "&id=%d", id
        );
    }

    /**
     * Método para eliminar la factura
     *
     * @param id
     */
    public void eliminarFactura(Integer id) {
        boolean respuesta = serviceFactura.deleteFactura(id);
        if (respuesta == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura eliminada!"));
            facturaFilter = new Factura();
            filterFacturas();
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al eliminar la Factura!"));
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapterFactura = new FacturaCompraAdapter();
        this.facturaFilter = new Factura();
        filterFacturas();
        this.serviceFactura = new FacturacionService();
        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();

        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("FACTURA_COMPRA");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);

        this.moneda = new Moneda();
        this.adapterMoneda = new MonedaAdapter();
        this.retencion = new RetencionCompra();
        this.retencionAsociadosAdapter = new RetencionCompraListAdapter();
        this.notaCreditoCompra = new NotaCreditoCompra();
        this.notaCreditoAsociadosAdapter = new NotaCreditoCompraAdapter();
        filterMoneda();
    }

}
