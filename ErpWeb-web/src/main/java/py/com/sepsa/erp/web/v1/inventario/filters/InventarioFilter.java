package py.com.sepsa.erp.web.v1.inventario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Inventario;

/**
 * Filtro para lista de inventario
 *
 * @author Alexander Triana
 */
public class InventarioFilter extends Filter {

    /**
     * Agrega el filtro por id de inventario
     *
     * @param id
     * @return
     */
    public InventarioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDeposito
     *
     * @param idDeposito
     * @return
     */
    public InventarioFilter idDepositoLogistico(Integer idDepositoLogistico) {
        if (idDepositoLogistico != null) {
            params.put("idDepositoLogistico", idDepositoLogistico);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoDeposito
     *
     * @param codigoDepositoLogistico
     * @return
     */
    public InventarioFilter codigoDepositoLogistico(String codigoDepositoLogistico) {
        if (codigoDepositoLogistico != null && codigoDepositoLogistico.trim().isEmpty()) {
            params.put("codigoDepositoLogistico", codigoDepositoLogistico);
        }
        return this;
    }

    /**
     * Agrega el filtro por deposito
     *
     * @param deposito
     * @return
     */
    public InventarioFilter depositoLogistico(String depositoLogistico) {
        if (depositoLogistico != null && depositoLogistico.trim().isEmpty()) {
            params.put("depositoLogistico", depositoLogistico);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProducto
     *
     * @param idProducto
     * @return
     */
    public InventarioFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro por producto
     *
     * @param producto
     * @return
     */
    public InventarioFilter producto(String producto) {
        if (producto != null && producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoGtin
     *
     * @param codigoGtin
     * @return
     */
    public InventarioFilter codigoGtin(String codigoGtin) {
        if (codigoGtin != null && codigoGtin.trim().isEmpty()) {
            params.put("codigoGtin", codigoGtin);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoInterno
     *
     * @param codigoInterno
     * @return
     */
    public InventarioFilter codigoInterno(String codigoInterno) {
        if (codigoInterno != null && codigoInterno.trim().isEmpty()) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public InventarioFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado
     *
     * @param estado
     * @return
     */
    public InventarioFilter estado(String estado) {
        if (estado != null && estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public InventarioFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por cantidad
     *
     * @param cantidad
     * @return
     */
    public InventarioFilter cantidad(Integer cantidad) {
        if (cantidad != null) {
            params.put("cantidad", cantidad);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public InventarioFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param inventario datos del filtro
     * @param page página consultada
     * @param pageSize tamaño de la página consultada
     * @return
     */
    public static Map build(Inventario inventario, Integer page,
            Integer pageSize) {

        InventarioFilter filter = new InventarioFilter();

        filter
                .id(inventario.getId())
                .idDepositoLogistico(inventario.getIdDepositoLogistico())
                .codigoDepositoLogistico(inventario.getCodigoDepositoLogistico())
                .depositoLogistico(inventario.getDepositoLogistico())
                .idProducto(inventario.getIdProducto())
                .producto(inventario.getProducto())
                .codigoGtin(inventario.getCodigoGtin())
                .codigoInterno(inventario.getCodigoInterno())
                .idEstado(inventario.getIdEstado())
                .estado(inventario.getEstado())
                .codigoEstado(inventario.getCodigoEstado())
                .cantidad(inventario.getCantidad())
                .listadoPojo(inventario.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public InventarioFilter() {
        super();
    }

}
