package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobrosListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.CobrosFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Ciente para el servicio de cobros
 *
 * @author alext
 */
public class CobrosService extends APIErpFacturacion {

    /**
     * Obtiene la lista de cobros
     *
     * @param cobro Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public CobrosListAdapter getCobrosList(Cobro cobro, Integer page,
            Integer pageSize) {

        CobrosListAdapter lista = new CobrosListAdapter();

        Map params = CobrosFilter.build(cobro, page, pageSize);

        HttpURLConnection conn = GET(Resource.COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CobrosListAdapter.class);

            lista = (CobrosListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para obetener detalle de cobros
     *
     * @param cobros objeto
     * @return
     */
    public Cobro getCobroDetalle(Cobro cobros) {

        Cobro cp = new Cobro();

        Map params = new HashMap<>();

        String url = "cobro/id/" + cobros.getId();

        HttpURLConnection conn = GET(url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    Cobro.class);

            cp = (Cobro) response.getPayload();

            conn.disconnect();
        }
        return cp;
    }

    /**
     * Constructor de CobrosService
     */
    public CobrosService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        COBRO("Lista de cobros", "cobro");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
