/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;

/**
 * Pojo para solicitud nota credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class SolicitudNotaCredito {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador de persona/cliente
     */
    private Integer idCliente;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * Fecha para filtrar desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha para filtrar hasta
     */
    private Date fechaInsercionHasta;
    /**
     * RUC del cliente
     */
    private String ruc;
    /**
     * nroDocumento
     */
    private String nroDocumento;
    /**
     * Razón Social
     */
    private String razonSocial;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total solicitud nota de credito
     */
    private BigDecimal montoTotalSolicitudNotaCredito;
    /**
     * Empresa
     */
    private Empresa empresa;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Listado de detalles
     */
    private List<SolicitudNotaCreditoDetalles> solicitudNotaCreditoDetalles;
    /**
     * Moneda
     */
    private Moneda moneda;
    /**
     * Código moneda
     */
    private String codigoMoneda;
    /**
     * Generado Edi
     */
    private String generadoEdi;
    /**
     * Archivo Edi
     */
    private String archivoEdi;
    /**
     * Identificador del local origen
     */
    private Integer idLocalOrigen;
    /**
     * Identificador del local destino
     */
    private Integer idLocalDestino;
    /**
     * Documento Recibido
     */
    private String recibido;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the recibido
     */
    public String getRecibido() {
        return recibido;
    }

    /**
     * @param recibido the recibido to set
     */
    public void setRecibido(String recibido) {
        this.recibido = recibido;
    }

    /**
     * @return the idLocalOrigen
     */
    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    /**
     * @param idLocalOrigen the idLocalOrigen to set
     */
    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    /**
     * @return the idLocalDestino
     */
    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    /**
     * @param idLocalDestino the idLocalDestino to set
     */
    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    /**
     * @return the archivoEdi
     */
    public String getArchivoEdi() {
        return archivoEdi;
    }

    /**
     * @param archivoEdi the archivoEdi to set
     */
    public void setArchivoEdi(String archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    /**
     * @return the codigoMoneda
     */
    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    /**
     * @param codigoMoneda the codigoMoneda to set
     */
    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalSolicitudNotaCredito() {
        return montoTotalSolicitudNotaCredito;
    }

    public void setMontoTotalSolicitudNotaCredito(BigDecimal montoTotalSolicitudNotaCredito) {
        this.montoTotalSolicitudNotaCredito = montoTotalSolicitudNotaCredito;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<SolicitudNotaCreditoDetalles> getSolicitudNotaCreditoDetalles() {
        return solicitudNotaCreditoDetalles;
    }

    public void setSolicitudNotaCreditoDetalles(List<SolicitudNotaCreditoDetalles> solicitudNotaCreditoDetalles) {
        this.solicitudNotaCreditoDetalles = solicitudNotaCreditoDetalles;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public String getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(String generadoEdi) {
        this.generadoEdi = generadoEdi;
    }


    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    //</editor-fold>
    public SolicitudNotaCredito() {
    }

}
