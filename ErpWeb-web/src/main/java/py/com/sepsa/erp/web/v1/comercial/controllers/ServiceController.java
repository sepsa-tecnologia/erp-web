
package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServiceListAdapter;
import py.com.sepsa.erp.web.v1.comercial.remote.ServicioServiceClient;

/**
 * Controlador para servicios
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("service")
public class ServiceController implements Serializable{
    
      /**
     * Cliente para el servicio de Servicios
     */
    private ServicioServiceClient serviceClient;
    /**
     * Adaptador de la lista de servicios
     */
    private ServiceListAdapter adapterService;
    
    /**
     * @return the serviceClient
     */
    public ServicioServiceClient getServiceClient() {
        return serviceClient;
    }

    /**
     * @param serviceClient the serviceClient to set
     */
    public void setServiceClient(ServicioServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }
    
     /**
     * @return the adapterService
     */
    public ServiceListAdapter getAdapterService() {
        return adapterService;
    }

    /**
     * @param adapterService the adapterService to set
     */
    public void setAdapterService(ServiceListAdapter adapterService) {
        this.adapterService = adapterService;
    }
    
     /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.serviceClient = new ServicioServiceClient();
        this.adapterService = new ServiceListAdapter();
    }
}
