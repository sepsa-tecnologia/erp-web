/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * pojo para proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Proceso {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * codigo
     */
    private String codigo;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador tipo proceso
     */
    private Integer idTipoProceso;
    /**
     * Tipo proceso
     */
    private TipoProceso tipoProceso;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Empresa
     */
    private Empresa empresa;
    /**
     * Identificador FrecuenciaEjecucion
     */
    private Integer idFrecuenciaEjecucion;
    /**
     * objeto frecuencia ejecucion
     */
    private FrecuenciaEjecucion frecuenciaEjecucion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdTipoProceso() {
        return idTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        this.idTipoProceso = idTipoProceso;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(TipoProceso tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public Integer getIdFrecuenciaEjecucion() {
        return idFrecuenciaEjecucion;
    }

    public void setIdFrecuenciaEjecucion(Integer idFrecuenciaEjecucion) {
        this.idFrecuenciaEjecucion = idFrecuenciaEjecucion;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public FrecuenciaEjecucion getFrecuenciaEjecucion() {
        return frecuenciaEjecucion;
    }

    public void setFrecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        this.frecuenciaEjecucion = frecuenciaEjecucion;
    }
    //</editor-fold>

    public Proceso() {

    }

    public Proceso(Integer id) {
        this.id = id;
    }
}
