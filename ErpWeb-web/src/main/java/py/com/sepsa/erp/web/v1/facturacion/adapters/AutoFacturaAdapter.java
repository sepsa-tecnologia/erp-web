package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFactura;
import py.com.sepsa.erp.web.v1.facturacion.remote.AutoFacturaService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class AutoFacturaAdapter extends DataListAdapter<AutoFactura> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final AutoFacturaService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public AutoFacturaAdapter fillData(AutoFactura searchData) {

        return serviceFactura.getFacturaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public AutoFacturaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new AutoFacturaService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public AutoFacturaAdapter() {
        super();
        this.serviceFactura = new AutoFacturaService();
    }
}
