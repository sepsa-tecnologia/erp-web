
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;
import py.com.sepsa.erp.web.v1.info.remote.TipoEtiquetaService;

/**
 * Adaptador para tipo etiqueta
 * @author Romina Núñez
 */
public class TipoEtiquetaAdapter extends DataListAdapter<TipoEtiqueta> {
    
    /**
     * Cliente para los servicios de tipo etiqueta
     */
    private final TipoEtiquetaService serviceTipoEtiqueta;
   
    
    
    /**
     * Constructor de TipoEtiquetaAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoEtiquetaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoEtiqueta = new TipoEtiquetaService();
    }

    /**
     * Constructor de TipoEtiquetaAdapter
     */
    public TipoEtiquetaAdapter() {
        super();
        this.serviceTipoEtiqueta = new TipoEtiquetaService();
    }

    @Override
    public TipoEtiquetaAdapter fillData(TipoEtiqueta searchData) {
        return serviceTipoEtiqueta.getTipoEtiquetaList(searchData,getFirstResult(),getPageSize());
    }
}



