
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionService;



/**
 * Adaptador de la lista detalle de retención
 * @author Cristina Insfrán
 */
public class RetencionDetalleListAdapter extends DataListAdapter<RetencionDetalle>{
   
       /**
     * Cliente para los servicios de retencion detalle
     */
    private final RetencionService retencionClient;
   
    
    @Override
    public RetencionDetalleListAdapter fillData(RetencionDetalle searchData) {
        return retencionClient.getDetalleRetencionList(searchData,getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de RetencionDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public RetencionDetalleListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.retencionClient = new RetencionService();
    }

    /**
     * Constructor de RetencionDetalleListAdapter
     */
    public RetencionDetalleListAdapter() {
        super();
        this.retencionClient = new RetencionService();
    }
}
