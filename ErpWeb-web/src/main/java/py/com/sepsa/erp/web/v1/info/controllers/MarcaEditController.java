package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.remote.MarcaService;

/**
 * Controlador para edición de Marca
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("marcaEdit")
public class MarcaEditController implements Serializable {

    /**
     * Servicio para edición de Marca
     */
    private final MarcaService service;

    /**
     * Pojo de Marca
     */
    private Marca marca;

    /**
     * Pojo para empresa
     */
    private Empresa empresaComplete;

    /**
     * Adapter para empresa
     */
    private EmpresaAdapter adapterEmpresa;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Empresa getEmpresaComplete() {
        return empresaComplete;
    }

    public void setEmpresaComplete(Empresa empresaComplete) {
        this.empresaComplete = empresaComplete;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    //</editor-fold>

    /**
     * Método para edición de Marca
     */
    public void edit() {
        BodyResponse<Marca> respuesta = service.editMarca(marca);

        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Marca editada correctamente!"));
        }
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa empresa = new Empresa();
        empresa.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(empresa);
        return adapterEmpresa.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        empresaComplete.setId(((Empresa) event.getObject()).getId());
    }

    /**
     * Inicializa los datos del controlador
     *
     * @param id
     */
    private void init(int id) {
        this.marca = service.get(id);
    }

    /**
     * Constructor de la clase
     */
    public MarcaEditController() {
        this.service = new MarcaService();
        this.empresaComplete = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
