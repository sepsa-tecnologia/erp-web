package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;

/**
 * Reporte de venta
 *
 * @author alext
 */
public class ReporteParam {

    /**
     * Fecha desde
     */
    private Date fechaDesde;

    /**
     * Fecha Hasta
     */
    private Date fechaHasta;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public ReporteParam() {
    }
    
}
