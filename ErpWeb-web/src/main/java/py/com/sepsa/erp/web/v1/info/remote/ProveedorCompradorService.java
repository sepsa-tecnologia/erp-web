package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ProveedorCompradorAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ProveedorCompradorFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ProveedorComprador;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de proveedor comprador
 *
 * @author Romina E. Núñez Rojas
 */
public class ProveedorCompradorService extends APIErpCore {

    /**
     * Método para crear el local
     * @param proveedorComprador
     * @return 
     */
    public ProveedorComprador createProveedorComprador(ProveedorComprador proveedorComprador) {

        ProveedorComprador proCom = new ProveedorComprador();
        
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PROVEEDORCOMPRADOR.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(proveedorComprador));

            response = BodyResponse.createInstance(conn, ProveedorComprador.class);

            proCom = ((ProveedorComprador) response.getPayload());

        }
        return proCom;
    }
    
     /**
     * Obtiene la lista de personaRol
     *
     * @param personaRol 
     * @param page
     * @param pageSize
     * @return
     */
    public ProveedorCompradorAdapter getProveedorComprador(ProveedorComprador proveedorComprador, Integer page,
            Integer pageSize) {

        ProveedorCompradorAdapter lista = new ProveedorCompradorAdapter();

        Map params = ProveedorCompradorFilter.build(proveedorComprador, page, pageSize);

        HttpURLConnection conn = GET(Resource.PROVEEDORCOMPRADOR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProveedorCompradorAdapter.class);

            lista = (ProveedorCompradorAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }    
   

    /**
     * Constructor de UserServiceClient
     */
    public ProveedorCompradorService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        PROVEEDORCOMPRADOR("Servicio ProveedorComprador", "proveedor-comprador");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
