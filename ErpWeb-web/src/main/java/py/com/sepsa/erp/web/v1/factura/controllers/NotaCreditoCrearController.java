package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.task.GeneracionDteNotaCreditoMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoParametroAdicional;
import py.com.sepsa.erp.web.v1.factura.pojos.ParametroAdicional;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ParametroAdicionalAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.SolicitudNotaCreditoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("notaCreditoCrear")
public class NotaCreditoCrearController implements Serializable {

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean create;

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaMontoAdapter adapterFactura;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas Pojo
     */
    private FacturaPojoAdapter adapterFacturaPojo;
    /**
     * POJO Factura
     */
    private FacturaPojo facturaPojo;
    private Factura facturaSeleccionada;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * POJO Datos Nota de Crédito
     */
    private DatosNotaCredito datoNotaCredito;
    /**
     * POJO Nota de Crédito
     */
    private NotaCredito notaCreditoCreate;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.C.
     */
    private List<NotaCreditoDetalle> listaDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Facturas
     */
    private List<FacturaPojo> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private ClientePojo cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientPojoAdapter adapterCliente;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato de factura
     */
    private String idFactura;
    /**
     *
     * Adaptador para la lista de motivoEmisionInterno
     *
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;
    /**
     *
     * POJO de MotivoEmisionInterno
     *
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Descripción de motivo emisión
     */
    private String motivoEmision;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Identificador de solicitud
     */
    private String idSolicitud;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listFacturaSolicitud = new ArrayList<>();
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Moneda> listMoneda = new ArrayList<>();
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFacturaSolicitud = new ArrayList<>();
    /**
     * Adapter solicitud
     */
    private SolicitudNotaCreditoListAdapter adapterSolicitud;
    /**
     * POJO Solicitud
     */
    private SolicitudNotaCredito solicitudNC;
    /**
     * Bandera
     */
    private boolean habilitarSeleccion;
    /**
     * Identificador de Empresa
     */
    private Integer idEmpresa;

    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Adapter Factura Talonario
     */
    private NotaCreditoTalonarioAdapter adapterNCTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo ncTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para agregar campos de dirección
     */
    private String tieneDireccion;
    /**
     * Bandera para cargar descripcion por defecto o descripcion del producto al detalle de NC
     */
    private String descripcionProductoDetalle;
    /**
     * Número de NC creada para descarga del KuDE
     */
    private String nroNCcreada;
    /**
     * ID de la NC creada para descarga del KuDE
     */
    private Integer idNcCreada;
    /**
     * Bandera siediApi
     */
    private String siediApi;

    private FileServiceClient fileServiceClient;
    
    /**
     * Tipo de descuento seleccionado
     */
    private Integer tipoDescuento;

    /**
     * Gravada
     */
    private String gravada;
    
     private boolean tieneParametrosAdicionales;
    
    private List<ParametroAdicional> parametrosAdicionales;
    
    private ParametroAdicional parametroAdicional;
    
    private ParametroAdicionalAdapter parametroAdicionalAdapter;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public String getGravada() {
        return gravada;
    }

    public void setGravada(String gravada) {
        this.gravada = gravada;
    }
    
    public boolean isTieneParametrosAdicionales() {
        return tieneParametrosAdicionales;
    }

    public void setTieneParametrosAdicionales(boolean tieneParametrosAdicionales) {
        this.tieneParametrosAdicionales = tieneParametrosAdicionales;
    }

    public List<ParametroAdicional> getParametrosAdicionales() {
        return parametrosAdicionales;
    }

    public void setParametrosAdicionales(List<ParametroAdicional> parametrosAdicionales) {
        this.parametrosAdicionales = parametrosAdicionales;
    }

    public ParametroAdicional getParametroAdicional() {
        return parametroAdicional;
    }

    public void setParametroAdicional(ParametroAdicional parametroAdicional) {
        this.parametroAdicional = parametroAdicional;
    }

    public ParametroAdicionalAdapter getParametroAdicionalAdapter() {
        return parametroAdicionalAdapter;
    }

    public void setParametroAdicionalAdapter(ParametroAdicionalAdapter parametroAdicionalAdapter) {
        this.parametroAdicionalAdapter = parametroAdicionalAdapter;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(Integer tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public String getNroNCcreada() {
        return nroNCcreada;
    }

    public void setNroNCcreada(String nroNCcreada) {
        this.nroNCcreada = nroNCcreada;
    }

    public Integer getIdNcCreada() {
        return idNcCreada;
    }

    public void setIdNcCreada(Integer idNcCreada) {
        this.idNcCreada = idNcCreada;
    }

    public String getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(String siediApi) {
        this.siediApi = siediApi;
    }

    public FacturaMontoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setAdapterFactura(FacturaMontoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public List<FacturaPojo> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setListSelectedFactura(List<FacturaPojo> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }  
    
    public void setListMoneda(List<Moneda> listMoneda) {
        this.listMoneda = listMoneda;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public void setAdapterCliente(ClientPojoAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public ClientePojo getCliente() {
        return cliente;
    }

    public ClientPojoAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public List<Moneda> getListMoneda() {
        return listMoneda;
    }

    public void setTieneDireccion(String tieneDireccion) {
        this.tieneDireccion = tieneDireccion;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public String getTieneDireccion() {
        return tieneDireccion;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setFacturaSeleccionada(Factura facturaSeleccionada) {
        this.facturaSeleccionada = facturaSeleccionada;
    }

    public Factura getFacturaSeleccionada() {
        return facturaSeleccionada;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public NotaCreditoTalonarioAdapter getAdapterNCTalonario() {
        return adapterNCTalonario;
    }

    public void setAdapterNCTalonario(NotaCreditoTalonarioAdapter adapterNCTalonario) {
        this.adapterNCTalonario = adapterNCTalonario;
    }

    public void setNcTalonario(TalonarioPojo ncTalonario) {
        this.ncTalonario = ncTalonario;
    }

    public TalonarioPojo getNcTalonario() {
        return ncTalonario;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public void setHabilitarSeleccion(boolean habilitarSeleccion) {
        this.habilitarSeleccion = habilitarSeleccion;
    }

    public boolean isHabilitarSeleccion() {
        return habilitarSeleccion;
    }

    public void setSolicitudNC(SolicitudNotaCredito solicitudNC) {
        this.solicitudNC = solicitudNC;
    }

    public void setAdapterSolicitud(SolicitudNotaCreditoListAdapter adapterSolicitud) {
        this.adapterSolicitud = adapterSolicitud;
    }

    public SolicitudNotaCredito getSolicitudNC() {
        return solicitudNC;
    }

    public SolicitudNotaCreditoListAdapter getAdapterSolicitud() {
        return adapterSolicitud;
    }

    public void setListSelectedFacturaSolicitud(List<Factura> listSelectedFacturaSolicitud) {
        this.listSelectedFacturaSolicitud = listSelectedFacturaSolicitud;
    }

    public void setListFacturaSolicitud(List<Factura> listFacturaSolicitud) {
        this.listFacturaSolicitud = listFacturaSolicitud;
    }

    public List<Factura> getListSelectedFacturaSolicitud() {
        return listSelectedFacturaSolicitud;
    }

    public List<Factura> getListFacturaSolicitud() {
        return listFacturaSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public void setMotivoEmision(String motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public String getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {
        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;
    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {
        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;
    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {
        return motivoEmisionInternoFilter;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {
        return motivoEmisionInternoAdapterList;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListaDetalle(List<NotaCreditoDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<NotaCreditoDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setNotaCreditoCreate(NotaCredito notaCreditoCreate) {
        this.notaCreditoCreate = notaCreditoCreate;
    }

    public NotaCredito getNotaCreditoCreate() {
        return notaCreditoCreate;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

    public void setDatoNotaCredito(DatosNotaCredito datoNotaCredito) {
        this.datoNotaCredito = datoNotaCredito;
    }

    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    public DatosNotaCredito getDatoNotaCredito() {
        return datoNotaCredito;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

    public String getDescripcionProductoDetalle() {
        return descripcionProductoDetalle;
    }

    public void setDescripcionProductoDetalle(String descripcionProductoDetalle) {
        this.descripcionProductoDetalle = descripcionProductoDetalle;
    }

    public FacturaPojoAdapter getAdapterFacturaPojo() {
        return adapterFacturaPojo;
    }

    public void setAdapterFacturaPojo(FacturaPojoAdapter adapterFacturaPojo) {
        this.adapterFacturaPojo = adapterFacturaPojo;
    }

    public FacturaPojo getFacturaPojo() {
        return facturaPojo;
    }

    public void setFacturaPojo(FacturaPojo facturaPojo) {
        this.facturaPojo = facturaPojo;
    }
//</editor-fold>
    /**
     * Método para agregar detalle
     *
     * @param event
     */
    public void onCheckFactura(SelectEvent event) {
        WebLogger.get().debug("entra");
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();

        FacturaPojoAdapter afpj = new FacturaPojoAdapter();
        FacturaPojo fdpj = new FacturaPojo();
        fdpj.setId(((FacturaPojo) event.getObject()).getId());
        fdpj.setListadoPojo(true);
        afpj = afpj.fillData(fdpj);
        
        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(afpj.getData().get(0).getId());
        afp = afp.fillData(fdp);
        Factura selectedFac = afp.getData().get(0);
        Integer idTipoCambio = (afp.getData().get(0).getIdTipoCambio());
        
        if (idTipoCambio != null){
            notaCreditoCreate.setIdTipoCambio(idTipoCambio);
        }
        
        if (selectedFac.getObservacion() != null) {
            notaCreditoCreate.setObservacion(selectedFac.getObservacion());  
        }
                       
        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {

            NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

            if (listaDetalle.isEmpty()) {
                notaCreditoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaCreditoDetalle.setNroLinea(linea);
            }
            notaCreditoDetalle.setNroLinea(linea);
            notaCreditoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaCreditoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());

            notaCreditoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();
            
            if(descripcionProductoDetalle.equalsIgnoreCase("S")){
                notaCreditoDetalle.setDescripcion(facturaDetalle.getDescripcion());
           } else {
                notaCreditoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);
           }  
            
            if (selectedFac.getIdDepartamento() != null && selectedFac.getIdDistrito() != null && selectedFac.getIdCiudad() != null) {
                idDepartamento = selectedFac.getIdDepartamento();
                filterDistrito();
                idDistrito = selectedFac.getIdDistrito();
                filterCiudad();
                idCiudad = selectedFac.getIdCiudad();
                
                notaCreditoCreate.setDireccion(selectedFac.getDireccion());
                notaCreditoCreate.setNroCasa(selectedFac.getNroCasa());
            }

            /**
             * if (facturaDetalle.getProducto() != null) {
             * notaCreditoDetalle.setDescripcion("BONIFICACION " + linea + " DE
             * LA FACTURA NRO." + nroFactura + " DEL PRODUCTO: " +
             * facturaDetalle.getProducto().getDescripcion());
             *
             * } else { notaCreditoDetalle.setDescripcion("BONIFICACION " +
             * linea + " DE LA FACTURA NRO." + nroFactura + " DEL PRODUCTO "); }
             *
             */
            notaCreditoDetalle.setDescuentoParticularUnitarioAux(facturaDetalle.getDescuentoParticularUnitario());
            notaCreditoDetalle.setDescuentoParticularUnitario(facturaDetalle.getDescuentoParticularUnitario());
            notaCreditoDetalle.setMontoDescuentoParticular(facturaDetalle.getMontoDescuentoParticular());
            notaCreditoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaCreditoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaCreditoDetalle.setCantidad(facturaDetalle.getCantidad());
            notaCreditoDetalle.setPorcentajeGravada(facturaDetalle.getPorcentajeGravada());
            notaCreditoDetalle.setMontoExentoGravado(facturaDetalle.getMontoExentoGravado());

            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva();

            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);
            notaCreditoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaCreditoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaCreditoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaCreditoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaCreditoDetalle.setIdProducto(facturaDetalle.getIdProducto());
            notaCreditoDetalle.setSubTotalSinDescuento(facturaDetalle.getTotalSinDescuento());

            listaDetalle.add(notaCreditoDetalle);
        }
        listSelectedFactura = new ArrayList<>();
        listSelectedFactura.add(((FacturaPojo) event.getObject()));
        
        if (selectedFac.getMontoTotalDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1) {
            notaCreditoCreate.setMontoTotalDescuentoGlobal(selectedFac.getMontoTotalDescuentoGlobal());
            notaCreditoCreate.setPorcentajeDescuentoGlobal(selectedFac.getPorcentajeDescuentoGlobal());
            calcularDescuentos(2);
        } else {
            notaCreditoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
            notaCreditoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
            calcularTotales();
        }
        
    }

    /**
     * Método para calcular el precio unitario sin IVA
     *
     * @param porcentaje
     * @param precioUnitIVA
     * @return
     */
    public BigDecimal calcularPrecioUnitSinIVA(Integer porcentaje, BigDecimal precioUnitIVA) {
        BigDecimal precioUnitSinIVA = new BigDecimal("0");
        if (porcentaje == 0) {
            precioUnitSinIVA = precioUnitIVA;
        }

        if (porcentaje == 5) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

        }

        if (porcentaje == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            precioUnitSinIVA = precioUnitIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

        }

        return precioUnitSinIVA;
    }

    public void obtenerDatosTalonario() {
        ncTalonario.setDigital(digital);
        //ncTalonario.setIdUsuario(session.getUser().getId());
        ncTalonario.setIdEncargado(session.getUser().getIdPersonaFisica());
        adapterNCTalonario = adapterNCTalonario.fillData(ncTalonario);

        if (adapterNCTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            //PF.current().ajax().update(":list-nota-credito-create-form:form-data:pnl-pop-up");
            PF.current().executeScript("$('#modalTalonario').modal('show');");
        } else if (adapterNCTalonario.getData().size() > 0) {
            talonario.setId(adapterNCTalonario.getData().get(0).getId());
            obtenerDatosNC();
        } else if (adapterNCTalonario.getData().size() < 1 && ncTalonario.getIdEncargado() != null) {
            ncTalonario = new TalonarioPojo();
            ncTalonario.setDigital(digital);
            adapterNCTalonario = adapterNCTalonario.fillData(ncTalonario);
            if (adapterNCTalonario.getData().size() > 1) {
                showTalonarioPopup = true;
                PF.current().executeScript("$('#modalTalonario').modal('show');");
            } else if (adapterNCTalonario.getData().size() > 0) {
                talonario.setId(adapterNCTalonario.getData().get(0).getId());
                obtenerDatosNC();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontró un talonario"));
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontró un talonario"));
        }
    }

    /**
     * Método invocado para deseleccionar liquidacion
     *
     * @param event
     */
    public void onUncheckFactura(UnselectEvent event) {

        BigDecimal subTotal = new BigDecimal("0");
        NotaCreditoDetalle info = null;

        FacturaPojoAdapter afpj = new FacturaPojoAdapter();
        FacturaPojo fdpj = new FacturaPojo();
        fdpj.setId(((FacturaPojo) event.getObject()).getId());
        fdpj.setListadoPojo(true);
        afpj = afpj.fillData(fdpj);
        
        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(afpj.getData().get(0).getId());
        afp = afp.fillData(fdp);
        
        for (int i = 0; i < afp.getData().get(0).getFacturaDetalles().size(); i++) {
            for (NotaCreditoDetalle info0 : listaDetalle) {
                if (Objects.equals(info0.getIdFactura(), ((FacturaPojo) event.getObject()).getId())) {
                    info = info0;
                    subTotal = info.getMontoTotal();
                    listaDetalle.remove(info);
                    break;
                }
            }

            BigDecimal monto = notaCreditoCreate.getMontoTotalNotaCredito();
            BigDecimal totalNotaCredito = monto.subtract(subTotal);
            notaCreditoCreate.setMontoTotalNotaCredito(totalNotaCredito);

        }

        calcularTotales();

    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void cambiarDescripcion(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        String h = info.getDescripcion();
        WebLogger.get().debug(h);
    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontosa(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal cinco = new BigDecimal("5");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoImponible.divide(cinco, 2, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal diez = new BigDecimal("10");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoImponible.divide(diez, 5, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotales();

    }
    
    public NotaCreditoDetalle obtenerDetalle(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                return info;
            }
        }

        return null;
    }

    public void calcularMontos(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        if (info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == -1){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pueden ingresar montos menores a 0."));
        } else {
            // Se calcula el monto total del detalle
            BigDecimal cantidad = info.getCantidad();
            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
            BigDecimal descuentoParticularUnitario = info.getDescuentoParticularUnitarioAux(); 
            BigDecimal montoPrecioXCantidadSinDescuento = precioUnitarioConIVA.multiply(cantidad);
            //Se aplica el descuento al item
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();
            
                        //Casos en que se hayan ingresado primero el monto/porcentaje descuento y luego el precioUnitarioIVA
           if( montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            } else if (descuentoParticularUnitario != null && descuentoParticularUnitario.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            }

            if (montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) { 
                montoDescuentoItem = info.getMontoDescuentoParticular();
                precioUnitarioConIVA = precioUnitarioConIVA.subtract(montoDescuentoItem);
            } else {
                info.setDescuentoGlobalUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
            }
            
            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);
            
            // Se calcula el monto base
            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxB = new BigDecimal(100).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxC = auxA.multiply(auxB);
            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);

            // Se calcula el monto exento gravado
            BigDecimal auxD = new BigDecimal("100").subtract(new BigDecimal(100));
            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
            // Integer aux1 = ((100 - info.getPorcentajeIva()) / 100);
            BigDecimal montoExentoGravado = montoBase.multiply(auxF);

            //Se calcula el monto imponible
            //Integer aux2 = info.getPorcentajeGravada() / 100;
            //BigDecimal auxF = new BigDecimal(info.getPorcentajeGravada()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
            BigDecimal montoImponible = montoBase.multiply(auxB);

            //Se calcula el monto iva
            // Integer aux3 = info.getPorcentajeIva() / 100;
            BigDecimal montoIva = montoImponible.multiply(auxA);

            //Se suman los montos para el total
            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);

            //Precio unitario
            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);

            info.setMontoIva(montoIva);
            info.setMontoImponible(montoImponible);
            info.setMontoExentoGravado(montoExentoGravado);
            info.setMontoTotal(total);
            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
            info.setSubTotalSinDescuento(montoPrecioXCantidadSinDescuento);
            calcularTotales();
            
            if (notaCreditoCreate.getPorcentajeDescuentoGlobal()!= null && notaCreditoCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1){
                calcularDescuentos(tipoDescuento);
            } else if ((notaCreditoCreate.getMontoTotalDescuentoGlobal()!= null && notaCreditoCreate.getMontoTotalDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1)){
                
            }
            
        }
    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");
        BigDecimal auxTotalSinDescuento = new BigDecimal("0");
        
        for (int i = 0; i < listaDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaDetalle.get(i).getMontoTotal());
            if (listaDetalle.get(i).getMontoDescuentoParticular().compareTo(BigDecimal.ZERO)>0) {
                auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getPrecioUnitarioConIva().multiply(listaDetalle.get(i).getCantidad()));
            } else {
                 auxTotalSinDescuento = auxTotalSinDescuento.add(listaDetalle.get(i).getMontoTotal());
            }
        }

        notaCreditoCreate.setMontoTotalNotaCredito(acumTotales);
        notaCreditoCreate.setMontoTotalGuaranies(acumTotales);
        notaCreditoCreate.setSubTotalSinDescuento(auxTotalSinDescuento);
        

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        notaCreditoCreate.setMontoIva5(montoIva5Acumulador);
        notaCreditoCreate.setMontoImponible5(montoImponible5Acumulador);
        notaCreditoCreate.setMontoTotal5(montoTotal5Acumulador);
        notaCreditoCreate.setMontoIva10(montoIva10Acumulador);
        notaCreditoCreate.setMontoImponible10(montoImponible10Acumulador);
        notaCreditoCreate.setMontoTotal10(montoTotal10Acumulador);
        notaCreditoCreate.setMontoTotalExento(montoExcentoAcumulador);
        notaCreditoCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        notaCreditoCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(notaCreditoCreate.getIdMoneda());
        montoLetrasFilter.setCodigoMoneda("PYG");
        BigDecimal scaled = notaCreditoCreate.getMontoTotalNotaCredito().setScale(0, RoundingMode.HALF_UP);
        montoLetrasFilter.setMonto(scaled);

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ClientePojo> completeQuery(String query) {

        ClientePojo cliente = new ClientePojo();
        cliente.setRazonSocial(query);
        cliente.setListadoPojo(true);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            notaCreditoCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        }

    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
        int id = ((ClientePojo) event.getObject()).getIdCliente();

        notaCreditoCreate = new NotaCredito();

        if (((ClientePojo) event.getObject()).getIdNaturalezaCliente() != null) {
            notaCreditoCreate.setIdNaturalezaCliente(((ClientePojo) event.getObject()).getIdNaturalezaCliente());
        }

        notaCreditoCreate.setRazonSocial(((ClientePojo) event.getObject()).getRazonSocial());
        ruc = ((ClientePojo) event.getObject()).getNroDocumento();
        ruc = ruc.trim().replace(" ","").replace(".","");
        idCliente = ((ClientePojo) event.getObject()).getIdCliente();
        notaCreditoCreate.setRuc(ruc);
        notaCreditoCreate.setIdCliente(idCliente);
        notaCreditoCreate.setIdMoneda(1);

        filterFacturaPojo(id);
        obtenerDatosNC();
        showDatatable = true;

    }

    public void obtenerDatosNC() {
        try {

            Map parametros = new HashMap();
            datoNotaCredito = new DatosNotaCredito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaCreditoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaCreditoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaCredito = serviceNotaCredito.getDatosNotaCredito(parametros);

            if (datoNotaCredito != null) {
                notaCreditoCreate.setFecha(today.getTime());
                notaCreditoCreate.setIdTalonario(datoNotaCredito.getIdTalonario());
                notaCreditoCreate.setNroNotaCredito(datoNotaCredito.getNroNotaCredito());

                //Validaciones
                if (datoNotaCredito.getTelefono() != null) {
                    notaCreditoCreate.setTelefono(datoNotaCredito.getTelefono());
                }
                if (datoNotaCredito.getEmail() != null) {
                    notaCreditoCreate.setEmail(datoNotaCredito.getEmail());
                }
                if (datoNotaCredito.getDireccion() != null) {
                    notaCreditoCreate.setDireccion(datoNotaCredito.getDireccion());
                }
                if (datoNotaCredito.getIdDepartamento() != null) {
                    notaCreditoCreate.setIdDepartamento(datoNotaCredito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaCredito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaCredito.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datoNotaCredito.getIdDistrito() != null) {
                    notaCreditoCreate.setIdDistrito(datoNotaCredito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaCredito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaCredito.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datoNotaCredito.getIdCiudad() != null) {
                    notaCreditoCreate.setIdCiudad(datoNotaCredito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaCredito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaCredito.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datoNotaCredito.getNroCasa() != null) {
                    notaCreditoCreate.setNroCasa(datoNotaCredito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
        if (idFactura != null) {
            obtenerDatosNCDevolucionFactura();
            devolucionDetalleNC();
            show = true;
            PF.current().ajax().update(":list-nota-credito-create-form:form-data:panel-cabecera-detalle:detalle-cabecera list-nota-credito-create-form:form-data:panel-cabecera-detalle:data-list-detalle-nota-credito list-nota-credito-create-form:form-data:panel-nota-credito:data-list-nota-create");

        }
    }

    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }

    /**
     * Método para crear Nota de Crédito
     */
    public void createNotaCredito() {
        calcularTotalesGenerales();
        boolean saveNC = true;

        notaCreditoCreate.setAnulado("N");
        notaCreditoCreate.setImpreso("N");
        notaCreditoCreate.setEntregado("N");
        notaCreditoCreate.setDigital(digital);

        if (tieneDireccion.equals("S")) {
            if (notaCreditoCreate.getDireccion() == null || notaCreditoCreate.getDireccion().trim().isEmpty()) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
            }

            if (notaCreditoCreate.getNroCasa() == null || notaCreditoCreate.getNroCasa().trim().isEmpty()) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
            }

            if (idDepartamento == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
            } else {
                notaCreditoCreate.setIdDepartamento(idDepartamento);
            }

            if (idDistrito == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
            } else {
                notaCreditoCreate.setIdDistrito(idDistrito);
            }

            if (idCiudad == null) {
                saveNC = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
            } else {
                notaCreditoCreate.setIdCiudad(idCiudad);
            }

        } else {
            notaCreditoCreate.setDireccion(null);
            notaCreditoCreate.setNroCasa(null);
            notaCreditoCreate.setIdDepartamento(null);
            notaCreditoCreate.setIdDistrito(null);
            notaCreditoCreate.setIdCiudad(null);
        }

        if (digital.equals("S")) {
            notaCreditoCreate.setArchivoSet("S");
        } else {
            notaCreditoCreate.setArchivoSet("N");
        }

        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
            listaDetalle.get(i).setNroNotaCredito(notaCreditoCreate.getNroNotaCredito());
        }

        notaCreditoCreate.setNotaCreditoDetalles(listaDetalle);
        
        if (!parametrosAdicionales.isEmpty()) {
            List<NotaCreditoParametroAdicional> listFpa = new ArrayList();
            for (ParametroAdicional pa : parametrosAdicionales) {
                NotaCreditoParametroAdicional npa = new NotaCreditoParametroAdicional();
                npa.setIdParametroAdicional(pa.getId());
                npa.setValor(pa.getValorDefecto());
                if (npa.getValor() == null || npa.getValor().trim().isEmpty()) {
                    if (pa.getMandatorio().equals('S')) {
                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", String.format("El campo %s es obligatorio.", pa.getDescripcion())));
                        saveNC = false;
                    }

                } else {
                    listFpa.add(npa);
                }
            }
            notaCreditoCreate.setParametrosAdicionales(listFpa);
        }

        if (saveNC) {
            BodyResponse<NotaCredito> respuestaDeNC = serviceNotaCredito.createNotaCredito(notaCreditoCreate);
                       
            if (respuestaDeNC.getSuccess()) {
                this.nroNCcreada = notaCreditoCreate.getNroNotaCredito();
                this.idNcCreada = ((NotaCredito) respuestaDeNC.getPayload()).getId();
                if (ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCARGAR_FACTURA", "N").equalsIgnoreCase("S")) {
                    mostrarPanelDescarga();
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "NC creada correctamente!"));
                try {
                    NotaCredito nc = (NotaCredito) respuestaDeNC.getPayload();
                    GeneracionDteNotaCreditoMultiempresa.generar(nc.getIdEmpresa(), nc.getId());
                } catch (Exception e){
                    WebLogger.get().fatal(e);
                }
                init();
            }
        }

    }
    
    public void filterFacturaPojo(Integer idCliente) {
        facturaPojo.setIdCliente(idCliente);
        facturaPojo.setCobrado("N");
        facturaPojo.setAnulado("N");
        facturaPojo.setListadoPojo(true);
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        idCliente = null;
        direccion = "";
        notaCreditoCreate = new NotaCredito();
        listSelectedFactura = new ArrayList<>();
        listaDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        datoNotaCredito = new DatosNotaCredito();
    }

    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
        this.listMoneda = new ArrayList<>();
        this.listMoneda = adapterMoneda.getData();
    }

    public void filterFacturasList() {
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    public void clearFacturasList() {
        facturaPojo = new FacturaPojo();
        facturaPojo.setIdCliente(idCliente);
        facturaPojo.setCobrado("N");
        facturaPojo.setAnulado("N");
        facturaPojo.setListadoPojo(true);
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
    }

    /**
     * Método para obtener la factura en NC, cuando se redirecciona desde el
     * listado de facturas
     */
    public void obtenerFactura() {
        facturaFilter.setId(Integer.parseInt(idFactura));
        adapterFactura = adapterFactura.fillData(facturaFilter);
        
        facturaPojo.setId(Integer.parseInt(idFactura));
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
        listSelectedFactura.add(adapterFacturaPojo.getData().get(0));
        
        cliente.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());
        Factura selectedFac = adapterFactura.getData().get(0);
        
        descripcionProductoDetalle = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCRIPCION_PRODUCTO_DETALLE_NC", "N");
        digital = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "TIPO_TALONARIO_NC_DIGITAL", "N");
        obtenerDatosTalonario();
        
        if (selectedFac.getIdDepartamento() != null && selectedFac.getIdDistrito() != null && selectedFac.getIdCiudad() != null) {
            idDepartamento = selectedFac.getIdDepartamento();
            filterDistrito();
            idDistrito = selectedFac.getIdDistrito();
            filterCiudad();
            idCiudad = selectedFac.getIdCiudad();

            notaCreditoCreate.setDireccion(selectedFac.getDireccion());
            notaCreditoCreate.setNroCasa(selectedFac.getNroCasa());
        }
        
        if (selectedFac.getIdTipoCambio() != null) {
            notaCreditoCreate.setIdTipoCambio(selectedFac.getIdTipoCambio());
        }
        
        if (selectedFac.getObservacion() != null) {
            notaCreditoCreate.setObservacion(selectedFac.getObservacion());
        }
        

    }

    /**
     * Método para obtener datos de cabecera
     */
    public void obtenerDatosNCDevolucionFactura() {
        notaCreditoCreate.setRazonSocial(adapterFactura.getData().get(0).getRazonSocial());
        notaCreditoCreate.setIdNaturalezaCliente(adapterFactura.getData().get(0).getIdNaturalezaCliente());
        ruc = adapterFactura.getData().get(0).getRuc();
        idCliente = adapterFactura.getData().get(0).getIdCliente();
        notaCreditoCreate.setRuc(ruc);
        notaCreditoCreate.setIdCliente(idCliente);
        try {

            Map parametros = new HashMap();
            datoNotaCredito = new DatosNotaCredito();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaCreditoCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaCreditoCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoNotaCredito = serviceNotaCredito.getDatosNotaCredito(parametros);

            if (datoNotaCredito != null) {
                notaCreditoCreate.setFecha(today.getTime());
                notaCreditoCreate.setIdTalonario(datoNotaCredito.getIdTalonario());
                notaCreditoCreate.setNroNotaCredito(datoNotaCredito.getNroNotaCredito());

                //Validaciones
                if (datoNotaCredito.getTelefono() != null) {
                    notaCreditoCreate.setTelefono(datoNotaCredito.getTelefono());
                }
                if (datoNotaCredito.getEmail() != null) {
                    notaCreditoCreate.setEmail(datoNotaCredito.getEmail());
                }
                if (datoNotaCredito.getDireccion() != null) {
                    notaCreditoCreate.setDireccion(datoNotaCredito.getDireccion());
                }
                if (datoNotaCredito.getIdDepartamento() != null) {
                    notaCreditoCreate.setIdDepartamento(datoNotaCredito.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoNotaCredito.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoNotaCredito.getIdDepartamento());
                    //PrimeFaces.current().executeScript("initAutocomplete();");
                }
                if (datoNotaCredito.getIdDistrito() != null) {
                    notaCreditoCreate.setIdDistrito(datoNotaCredito.getIdDistrito());
                    direccionFilter.setIdDistrito(datoNotaCredito.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoNotaCredito.getIdDistrito());
                }
                if (datoNotaCredito.getIdCiudad() != null) {
                    notaCreditoCreate.setIdCiudad(datoNotaCredito.getIdCiudad());
                    direccionFilter.setIdCiudad(datoNotaCredito.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoNotaCredito.getIdCiudad());
                }
                if (datoNotaCredito.getNroCasa() != null) {
                    notaCreditoCreate.setNroCasa(datoNotaCredito.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void devolucionDetalleNC() {
        if (show == false) {
            show = true;
        }

        listaDetalle = new ArrayList();
        int nroBonificacion = 1;

        FacturaAdapter afp = new FacturaAdapter();
        Factura fdp = new Factura();
        fdp.setId(Integer.parseInt(idFactura));
        afp = afp.fillData(fdp);

        notaCreditoCreate.setIdNaturalezaCliente(afp.getData().get(0).getIdNaturalezaCliente());
        if(afp.getData().get(0).getPorcentajeDescuentoGlobal() == null || afp.getData().get(0).getMontoTotalDescuentoGlobal() == null){
            notaCreditoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
            notaCreditoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
        } else {
            notaCreditoCreate.setPorcentajeDescuentoGlobal(afp.getData().get(0).getPorcentajeDescuentoGlobal());
            notaCreditoCreate.setMontoTotalDescuentoGlobal(afp.getData().get(0).getMontoTotalDescuentoGlobal());
        }
        
        for (FacturaDetalle facturaDetalle : afp.getData().get(0).getFacturaDetalles()) {
            NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

            if (listaDetalle.isEmpty()) {
                notaCreditoDetalle.setNroLinea(linea);
            } else {
                linea++;
                notaCreditoDetalle.setNroLinea(linea);
            }

            notaCreditoDetalle.setNroLinea(linea);
            notaCreditoCreate.setIdMoneda(afp.getData().get(0).getIdMoneda());
            notaCreditoCreate.setCodigoMoneda(afp.getData().get(0).getMoneda().getCodigo());
            

            notaCreditoDetalle.setIdFactura(afp.getData().get(0).getId());
            String nroFactura = afp.getData().get(0).getNroFactura();
           if(descripcionProductoDetalle.equalsIgnoreCase("S")){
                notaCreditoDetalle.setDescripcion(facturaDetalle.getDescripcion());
           } else {
                notaCreditoDetalle.setDescripcion("BONIFICACION " + linea + " DE LA FACTURA NRO." + nroFactura);
           }  
            
           
            notaCreditoDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            notaCreditoDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            notaCreditoDetalle.setCantidad(facturaDetalle.getCantidad());
            notaCreditoDetalle.setDescuentoParticularUnitario(facturaDetalle.getDescuentoParticularUnitario());
            notaCreditoDetalle.setDescuentoParticularUnitarioAux(facturaDetalle.getDescuentoParticularUnitario());
            notaCreditoDetalle.setMontoDescuentoParticular(facturaDetalle.getMontoDescuentoParticular());
            BigDecimal precioUnitarioConIVA = facturaDetalle.getPrecioUnitarioConIva();
            BigDecimal precioUnitarioSinIVA = calcularPrecioUnitSinIVA(facturaDetalle.getPorcentajeIva(), precioUnitarioConIVA);

            notaCreditoDetalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            notaCreditoDetalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            notaCreditoDetalle.setMontoIva(facturaDetalle.getMontoIva());
            notaCreditoDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            notaCreditoDetalle.setIdProducto(facturaDetalle.getIdProducto());
            notaCreditoDetalle.setPorcentajeGravada(facturaDetalle.getPorcentajeGravada());
            notaCreditoDetalle.setMontoExentoGravado(facturaDetalle.getMontoExentoGravado());

            listaDetalle.add(notaCreditoDetalle);
        }

        listSelectedFactura = new ArrayList<>();
        facturaPojo.setId(afp.getData().get(0).getId());
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
        listSelectedFactura.add(adapterFacturaPojo.getData().get(0));

        calcularTotales();
        
        calcularDescuentos(2);
        
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void obtenerMotivoEmisionInterno() {

        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));

    }

    public void obtenerMotivoEmision() {
        MotivoEmisionInternoAdapter motivoEmisionInternoAdapter = new MotivoEmisionInternoAdapter();
        MotivoEmisionInterno motivoEmisionInterno = new MotivoEmisionInterno();
        if (notaCreditoCreate.getIdMotivoEmisionInterno() != null) {
            motivoEmisionInterno.setId(notaCreditoCreate.getIdMotivoEmisionInterno());
            motivoEmisionInternoAdapter = motivoEmisionInternoAdapter.fillData(motivoEmisionInterno);
            motivoEmision = motivoEmisionInternoAdapter.getData().get(0).getMotivoEmision().getDescripcion();
        } else {
            this.motivoEmision = "Ninguno";
        }

    }

    public void obtenerFacturasDeSolicitud() {
        solicitudNC.setId(Integer.parseInt(idSolicitud));
        adapterSolicitud = adapterSolicitud.fillData(solicitudNC);

        cliente.setRazonSocial(adapterSolicitud.getData().get(0).getRazonSocial());

        notaCreditoCreate.setRazonSocial(cliente.getRazonSocial());
        ruc = adapterSolicitud.getData().get(0).getRuc();
        idCliente = adapterSolicitud.getData().get(0).getIdCliente();
        notaCreditoCreate.setRuc(ruc);
        notaCreditoCreate.setIdCliente(idCliente);

        facturaPojo.setId(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(0).getIdFacturaCompra());
        adapterFacturaPojo = adapterFacturaPojo.fillData(facturaPojo);
        listSelectedFactura.add(adapterFacturaPojo.getData().get(0));
        obtenerDatosNC();
        solicitudDetalleNC();
    }

    public void solicitudDetalleNC() {
        if (show == false) {
            show = true;
        }

        for (int i = 0; i < adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().size(); i++) {
            NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

            int nroBonificacion;
            if (listaDetalle.isEmpty()) {
                notaCreditoDetalle.setNroLinea(linea);
                nroBonificacion = linea;
            } else {
                linea++;
                nroBonificacion = linea;
                notaCreditoDetalle.setNroLinea(linea);
            }

            notaCreditoDetalle.setIdFactura(listSelectedFactura.get(0).getId());
            String nroFactura = listSelectedFactura.get(0).getNroFactura();
           if(descripcionProductoDetalle.equalsIgnoreCase("S")){
                notaCreditoDetalle.setDescripcion(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(0).getDescripcion());
           } else {
                notaCreditoDetalle.setDescripcion("BONIFICACION " + nroBonificacion + " DE LA FACTURA NRO." + nroFactura);
           }  
            notaCreditoDetalle.setPorcentajeIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPorcentajeIva());
            notaCreditoDetalle.setMontoImponible(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoImponible());
            notaCreditoDetalle.setCantidad(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getCantidad());
            notaCreditoDetalle.setPrecioUnitarioConIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPrecioUnitarioConIva());
            notaCreditoDetalle.setPrecioUnitarioSinIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPrecioUnitarioSinIva());
            notaCreditoDetalle.setMontoIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoIva());
            notaCreditoDetalle.setMontoTotal(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoTotal());

            listaDetalle.add(notaCreditoDetalle);
        }

        calcularTotales();
    }

    public Factura filtrarFactura(Integer idFactura) {

        FacturaAdapter adapterFactura = new FacturaAdapter();
        Factura facturaFilter = new Factura();
        facturaFilter.setId(idFactura);

        adapterFactura = adapterFactura.fillData(facturaFilter);

        return adapterFactura.getData().get(0);
    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param nroLinea
     */
    public void deleteDetalle(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        BigDecimal monto = notaCreditoCreate.getMontoTotalNotaCredito();
        BigDecimal totalNC = monto.subtract(subTotal);
        notaCreditoCreate.setMontoTotalNotaCredito(totalNC);
        obtenerMontoTotalLetras();

    }

    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-credito-create-form:form-data:panel-cabecera-detalle:distrito");
        PF.current().ajax().update(":list-nota-credito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-credito-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        // referenciaGeoDistrito = new ReferenciaGeografica();
        // referenciaGeoCiudad = new ReferenciaGeografica();
        // adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        notaCreditoCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        //referenciaGeoCiudad = new ReferenciaGeografica();
        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        notaCreditoCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        notaCreditoCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    public void filterDepartamento() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
    public void descuentoListener(Integer nroLinea, Integer param){
        calcularDescuentoItem(nroLinea,tipoDescuento);
        calcularMontos(nroLinea);
        calcularDescuentos(2);
        
    }
    
     /**
     * Método para calcular el descuento de un item del detalle
     * @param nroLinea
     * @param param 
     */
    public void calcularDescuentoItem(Integer nroLinea, Integer param) {
        NotaCreditoDetalle info = null;
        
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        
        if (info != null) {
            BigDecimal descuentoItemAux = info.getDescuentoParticularUnitarioAux();
            BigDecimal descuentoItem = info.getDescuentoParticularUnitario();
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();

            if (param == 1) {
                if (info.getPrecioUnitarioConIva() != null && info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == 1) {
                    //valida que el porcentaje de descuento ingresado sea mayor o igual a 0 y menor que 100
                    if (descuentoItemAux != null && descuentoItem != null && ((descuentoItem.compareTo(BigDecimal.ZERO) == 1 && descuentoItem.compareTo(new BigDecimal("100")) == -1) || (descuentoItem.compareTo(BigDecimal.ZERO) == 0)) ) {                
                        BigDecimal porcentajeDescuento = descuentoItemAux.divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
                        montoDescuentoItem = info.getPrecioUnitarioConIva().multiply(porcentajeDescuento);
                        info.setMontoDescuentoParticular(montoDescuentoItem);
                        info.setDescuentoParticularUnitario(descuentoItemAux);
                    } else {
                        info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                        info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                        info.setMontoDescuentoParticular(BigDecimal.ZERO);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje de descuento debe ser entre 0 y 100."));
                    }
                }   
            } else if (param == 2) {
                //valida que el montoDescuento no sea nulo, menor a cero o mayor al precio unitario
                if (montoDescuentoItem != null && ((( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1 ) ||( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 0 )) && (montoDescuentoItem.compareTo(info.getPrecioUnitarioConIva())== -1))){
                    BigDecimal aux = info.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                    descuentoItem = aux.divide(info.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                    info.setDescuentoParticularUnitario(descuentoItem);
                    info.setDescuentoParticularUnitarioAux(descuentoItem);
                } else {
                    info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                    info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                    info.setMontoDescuentoParticular(BigDecimal.ZERO);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto de descuento no puede ser menor a 0 o mayor al precio unitario con IVA"));
                }
            }
        }
    }
    
    /**
     * Método que calcula el descuento global
     * @param param 1 = Calcular descuento segun porcentaje, 2 = Calcular según monto descuento
     */
    public void calcularDescuentos(Integer param){
        calcularTotales();
        if (notaCreditoCreate.getMontoTotalNotaCredito() != null && notaCreditoCreate.getMontoTotalNotaCredito().compareTo(BigDecimal.ZERO) == 1){
            if (param == 1){
                if (notaCreditoCreate.getPorcentajeDescuentoGlobal()!= null) {
                    BigDecimal porcentajeDescuento = notaCreditoCreate.getPorcentajeDescuentoGlobal().divide(new BigDecimal("100"),5,RoundingMode.HALF_UP);
                    if(notaCreditoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0) {
                        notaCreditoCreate.setMontoTotalDescuentoGlobal(notaCreditoCreate.getSubTotalSinDescuento().multiply(porcentajeDescuento));
                    } else {
                        notaCreditoCreate.setMontoTotalDescuentoGlobal(notaCreditoCreate.getMontoTotalNotaCredito().multiply(porcentajeDescuento));
                    }
                    
                    notaCreditoCreate.setMontoTotalNotaCredito(notaCreditoCreate.getMontoTotalNotaCredito().subtract(notaCreditoCreate.getMontoTotalDescuentoGlobal()));
                        
                    notaCreditoCreate.setMontoTotalGuaranies(notaCreditoCreate.getMontoTotalNotaCredito());
                    obtenerMontoTotalLetras();                        
                } else {
                    notaCreditoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                    notaCreditoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                }
                
            } else if (param == 2) {
                if (notaCreditoCreate.getMontoTotalDescuentoGlobal() != null) {
                    BigDecimal aux = notaCreditoCreate.getMontoTotalDescuentoGlobal().multiply(new BigDecimal("100"));
                    BigDecimal porcentaje = BigDecimal.ZERO;
                    if(notaCreditoCreate.getSubTotalSinDescuento().compareTo(BigDecimal.ZERO) > 0){
                         porcentaje = aux.divide(notaCreditoCreate.getSubTotalSinDescuento(),5,RoundingMode.HALF_UP);
                    } else {
                         porcentaje = aux.divide(notaCreditoCreate.getMontoTotalNotaCredito(),5,RoundingMode.HALF_UP);
                    }
                    notaCreditoCreate.setPorcentajeDescuentoGlobal(porcentaje);
                    notaCreditoCreate.setMontoTotalNotaCredito(notaCreditoCreate.getMontoTotalNotaCredito().subtract(notaCreditoCreate.getMontoTotalDescuentoGlobal()));
                   
                    notaCreditoCreate.setMontoTotalGuaranies(notaCreditoCreate.getMontoTotalNotaCredito());
                    obtenerMontoTotalLetras();
                        
                } else {
                    notaCreditoCreate.setPorcentajeDescuentoGlobal(BigDecimal.ZERO);
                    notaCreditoCreate.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                }
                
            }
        }
    }
    
    /**
     * Método para descargar el pdf de Nota de Crédito
     * @return 
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroNCcreada + ".pdf";
        String url = "nota-credito/consulta/" + idNcCreada;
        Map param = new HashMap();
        if (this.digital.equalsIgnoreCase("S")){
             param.put("ticket", false);
             if (siediApi.equalsIgnoreCase("S")) {
                param.put("siediApi", true);
             } else {
                param.put("siediApi", false);
             }
        }
        byte[] data = fileServiceClient.downloadNotaCredito(url,param);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }
    
    /**
     * Obtiene el listado de parametros adicionales para agregar a la vista
     */
    public void obtenerParametrosAdicionales() {
        parametroAdicional.setActivo("S");
        parametroAdicional.setCodigoTipoParametroAdicional("NOTA_CREDITO");
        this.parametroAdicionalAdapter = parametroAdicionalAdapter.fillData(parametroAdicional);
        if (!parametroAdicionalAdapter.getData().isEmpty()) {
            this.tieneParametrosAdicionales = true;
            parametrosAdicionales = new ArrayList(parametroAdicionalAdapter.getData());
        }
    }
    
    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.facturaSeleccionada = new Factura();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.direccionFilter = new Direccion();

        this.habilitarSeleccion = true;
        create = false;
        show = false;
        this.idEmpresa = session.getUser().getIdEmpresa();
        this.adapterNCTalonario = new NotaCreditoTalonarioAdapter();
        this.ncTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();

        this.adapterFacturaPojo = new FacturaPojoAdapter();
        this.facturaPojo = new FacturaPojo();
        this.facturaPojo.setListadoPojo(true);
        this.adapterFactura = new FacturaMontoAdapter();
        this.facturaFilter = new Factura();
        this.serviceNotaCredito = new NotaCreditoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new ClientePojo();
        this.adapterCliente = new ClientPojoAdapter();

        this.adapterSolicitud = new SolicitudNotaCreditoListAdapter();
        this.solicitudNC = new SolicitudNotaCredito();

        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();

        this.notaCreditoCreate = new NotaCredito();

        this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
        this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());

        filterMoneda();
        obtenerMotivoEmisionInterno();

        this.motivoEmision = "Ninguno";
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;

        this.tieneDireccion = "N";
        this.gravada = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "CLIENTE_GRAVADO", "N");
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("id") != null) {
            idFactura = params.get("id");
            showDatatable = true;
            this.habilitarSeleccion = false;
            obtenerFactura();
        } else {
            descripcionProductoDetalle = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "DESCRIPCION_PRODUCTO_DETALLE_NC", "N");
            digital = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "TIPO_TALONARIO_NC_DIGITAL", "N");
            obtenerDatosTalonario();
        }

        if (params.get("idSolicitud") != null) {
            idSolicitud = params.get("idSolicitud");
            showDatatable = true;
            this.habilitarSeleccion = false;
            obtenerFacturasDeSolicitud();

        }

        filterDepartamento();
        filterDistrito();
        filterCiudad();
        fileServiceClient = new FileServiceClient();
        this.siediApi = ConfiguracionValorServiceClient.getConfValor(idEmpresa, "ARCHIVOS_DTE_SIEDI_API", "N");
        this.tipoDescuento = 1;
        this.tieneParametrosAdicionales = false;
        this.parametroAdicionalAdapter = new ParametroAdicionalAdapter();
        this.parametroAdicional = new ParametroAdicional();
        this.parametrosAdicionales = new ArrayList();
        obtenerParametrosAdicionales();
    }

}
