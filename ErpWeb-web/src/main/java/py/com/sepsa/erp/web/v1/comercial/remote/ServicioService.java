package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServicioAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ServicioFilter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Servicio para el listado de clientes s/ servicios.
 *
 * @author alext
 */
public class ServicioService extends APIErpComercial {

    /**
     * Obtiene la lista de clientes según servicios.
     *
     * @param searchData
     * @param page
     * @param pagesize
     * @return
     */
    public ServicioAdapter getServiciosList(Servicios searchData,
            Integer page, Integer pagesize) {

        ServicioAdapter lista = new ServicioAdapter();

        Map params = ServicioFilter.build(searchData, page, pagesize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ServicioAdapter.class);

            lista = (ServicioAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    /**
     * Método para crear un nuevo servicio.
     *
     * @param servicios
     * @return
     */
    public Servicios create(Servicios servicios) {

        HttpURLConnection conn = POST(Resource.CREAR.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();
            WebLogger.get().debug(gson.toJson(servicios));
            this.addBody(conn, gson.toJson(servicios));
            BodyResponse response = BodyResponse.createInstance(conn, Servicios.class);
            servicios = (Servicios) response.getPayload();
            conn.disconnect();
        }

        return servicios;
    }

    /**
     * Constructor de ServiciosService.
     */
    public ServicioService() {
        super();
    }

    /**
     * Recursos de conexión.
     */
    public enum Resource {
        //Servicios
        LISTAR("Listado de servicios", "servicio"),
        CREAR("Creación de servicio", "servicio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
