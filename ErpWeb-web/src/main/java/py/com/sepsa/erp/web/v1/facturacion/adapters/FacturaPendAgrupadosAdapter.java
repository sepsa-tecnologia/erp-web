
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista de facturas pendientes agrupadas
 * @author Cristina Insfrán
 */
public class FacturaPendAgrupadosAdapter extends DataListAdapter<Factura>{
     /**
     * Cliente para el servicio de facturacion
     */
    private final FacturacionService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public FacturaPendAgrupadosAdapter fillData(Factura searchData) {

        return serviceFactura.getFacturaPendAgrupadoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaPendAgrupadosAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FacturaPendAgrupadosAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturacionService();
    }

    /**
     * Constructor de FacturaPendAgrupadosAdapter
     */
    public FacturaPendAgrupadosAdapter() {
        super();
        this.serviceFactura = new FacturacionService();
    }
}
