package py.com.sepsa.erp.web.v1.comercial.pojos;

import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * POJO para cliente
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Cliente {
    
    public class NaturalezaCliente {
        private Integer id;
        private String descripcion;
        private String codigo;

        //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getCodigo() {
            return codigo;
        }

        public void setCodigo(String codigo) {
            this.codigo = codigo;
        }
        //</editor-fold>
        
        public NaturalezaCliente() {    
        }
    }
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * id del estado
     */
    private Integer idEstado;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Código de estado
     */
    private String codigoEstado;
    /**
     * Razón social
     */
    private String razonSocial;
    /**
     * Identificador de canal de venta
     */
    private Integer idCanalVenta;
    /**
     * Canal de venta
     */
    private CanalVenta canalVenta;
    /**
     * Clave de pago
     */
    private String clavePago;
    /**
     * Factura electronica
     */
    private String factElectronica;
    /**
     * Identificador tipo de documento
     */
    private Integer idTipoDocumento;
    /**
     * Número de documento
     */
    private String nroDocumento;
    /**
     * Tipo de documento
     */
    private TipoDocumento tipoDocumento;
    /**
     * Porcentaje de retencion
     */
    private Persona persona;
    /**
     * Porcentaje de retencion
     */
    private Integer porcentajeRetencion;
    /**
     * Proveedor
     */
    private String proveedor;
        /**
     * Proveedor
     */
    private String comprador;
    /**
     * Identificador de Naturaleza de Cliente
     */
    private Integer idNaturalezaCliente;
    /**
     * Naturaleza de Cliente
     */
    private NaturalezaCliente naturalezaCliente;
    /**
     * Código de Naturaleza de Cliente
     */
    private String codigoNaturalezaCliente;
    /**
     * Código de Naturaleza de TipoDocumento
     */
    private String codigoTipoDocumento;
    /**
     * Código de pais
     */
    private String codigoPais;
    
    private boolean rucDuplicado;
    
    private String rucDup;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the porcentajeRetencion
     */
    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }
    
    public void setNaturalezaCliente(NaturalezaCliente naturalezaCliente) {
        this.naturalezaCliente = naturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }
    
    public NaturalezaCliente getNaturalezaCliente() {
        return naturalezaCliente;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getComprador() {
        return comprador;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param porcentajeRetencion the porcentajeRetencion to set
     */
    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public String getClavePago() {
        return clavePago;
    }

    public void setClavePago(String clavePago) {
        this.clavePago = clavePago;
    }

    public String getFactElectronica() {
        return factElectronica;
    }

    public void setFactElectronica(String factElectronica) {
        this.factElectronica = factElectronica;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public boolean isRucDuplicado() {
        return rucDuplicado;
    }

    public void setRucDuplicado(boolean rucDuplicado) {
        this.rucDuplicado = rucDuplicado;
    }

    public String getRucDup() {
        return rucDup;
    }

    public void setRucDup(String rucDup) {
        this.rucDup = rucDup;
    }

    
    
    //</editor-fold>

    public Cliente() {
    }

}
