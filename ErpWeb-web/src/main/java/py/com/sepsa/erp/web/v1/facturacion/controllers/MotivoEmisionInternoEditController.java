/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionInternoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para Editar MotivoEmisionInterno
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionInternoEdit")
public class MotivoEmisionInternoEditController implements Serializable {

    /**
     * Cliente para el servicio de motivoEmisionInterno.
     */
    private final MotivoEmisionInternoService service;
    /**
     * Datos del motivoEmisionInterno
     */
    private MotivoEmisionInterno motivoEmisionInterno;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private MotivoEmisionAdapter motivoEmisionAdapterList;
    /**
     * POJO de tipo documento
     */
    private MotivoEmision motivoEmisionFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoEmisionInterno getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public MotivoEmisionAdapter getMotivoEmisionAdapterList() {
        return motivoEmisionAdapterList;
    }

    public void setMotivoEmisionAdapterList(MotivoEmisionAdapter motivoEmisionAdapterList) {
        this.motivoEmisionAdapterList = motivoEmisionAdapterList;
    }

    public MotivoEmision getMotivoEmisionFilter() {
        return motivoEmisionFilter;
    }

    public void setMotivoEmisionFilter(MotivoEmision motivoEmisionFilter) {
        this.motivoEmisionFilter = motivoEmisionFilter;
    }

    //</editor-fold>
    /**
     * Método para editar MotivoEmisionInterno
     */
    public void edit() {
        BodyResponse<MotivoEmisionInterno> respuestaTal = service.editMotivoEmisionInterno(motivoEmisionInterno);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Motivo de Emision Interno editado correctamente!"));
        }

    }

    /**
     * Método para obtener tipo documento
     */
    public void getMotivoEmision() {
        this.setMotivoEmisionAdapterList(getMotivoEmisionAdapterList().fillData(getMotivoEmisionFilter()));
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.motivoEmisionInterno = service.get(id);
        this.motivoEmisionAdapterList = new MotivoEmisionAdapter();
        this.motivoEmisionFilter = new MotivoEmision();
        getMotivoEmision();
    }

    /**
     * Constructor
     */
    public MotivoEmisionInternoEditController() {
        this.service = new MotivoEmisionInternoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
