/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoProcesamientoService;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoProcesamientoAdapter extends DataListAdapter<DocumentoProcesamiento> {


    private final NotaDebitoProcesamientoService serviceND;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaDebitoProcesamientoAdapter fillData(DocumentoProcesamiento searchData) {

        return serviceND.getNDProcesamientoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de NotaDebitoProcesamientoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaDebitoProcesamientoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceND = new NotaDebitoProcesamientoService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public NotaDebitoProcesamientoAdapter() {
        super();
        this.serviceND = new NotaDebitoProcesamientoService();
    }
}
