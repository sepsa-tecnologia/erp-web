/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.pojos;

/**
 * Pojo para Tipo Repositorio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoRepositorio {

    /**
     * Identificador del Tipo Repositorio
     */
    private Integer id;

    /**
     * Descripcion del Tipo de repositorio
     */
    private String descripcion;

    /**
     * Clave del Tipo de repositorio
     */
    private String codigo;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
//</editor-fold>

    /**
     * Constructor
     */
    public TipoRepositorio() {

    }

}
