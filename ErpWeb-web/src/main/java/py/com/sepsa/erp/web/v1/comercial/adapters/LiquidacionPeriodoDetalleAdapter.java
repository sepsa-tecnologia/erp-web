package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionPeriodoDetalle;
import py.com.sepsa.erp.web.v1.comercial.remote.LiquidacionPeriodoDetalleService;

/**
 * Adaptador para lista de detalle de liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionPeriodoDetalleAdapter extends DataListAdapter<LiquidacionPeriodoDetalle> {

    private final LiquidacionPeriodoDetalleService service;

    /**
     * Método para cargar la lista de detalle de liquidaciones por periodo.
     *
     * @param searchData
     * @return
     */
    @Override
    public LiquidacionPeriodoDetalleAdapter fillData(LiquidacionPeriodoDetalle searchData) {
        return service.getLiquidacionesPeriodoDetalle(searchData, getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de LiquidacionPeriodoDetalleAdapter.
     *
     * @param page
     * @param pageSize
     */
    public LiquidacionPeriodoDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new LiquidacionPeriodoDetalleService();
    }

    /**
     * Constructor de LiquidacionPeriodoDetalleAdapter.
     */
    public LiquidacionPeriodoDetalleAdapter() {
        super();
        this.service = new LiquidacionPeriodoDetalleService();
    }

}
