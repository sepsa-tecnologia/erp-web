/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.task.GeneracionDteNotaRemisionMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaRemision;
import py.com.sepsa.erp.web.v1.factura.pojos.LocalSalidaEntrega;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemision;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionNrAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TransportistaListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.VehiculoTrasladoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Williams Vera
 */
@Named("notaRemisionCrear")
@ViewScoped
public class NotaRemisionCrearController implements Serializable{
    /**
     * POJO nota de remision
     */
    private NotaRemision notaRemisionCreate;
    /**
     * POJO de transportista
     */
    private Transportista transportista;
    /**
     * POJO de vehiculo Traslado
     */
    private VehiculoTraslado vehiculoTraslado;
    /**
     * POJO de transportista para autocomplete
     */
    private Transportista transportistaAutoComplete;
    /**
     * POJO de vehiculo Traslado para autocomplete
     */
    private VehiculoTraslado vehiculoTrasladoAutoComplete;
    /**
     * POJOs de local salida/entrega
     */
    private LocalSalidaEntrega localSalida;
    
    private LocalSalidaEntrega localEntrega;
    /**
     * Adaptador para georeferencia distrito para localSalida
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistritoSalida;
    /**
     * Adaptador para geoReferencia ciudad para localSalida
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudadSalida;
    /**
     * Adaptador para georeferencia Departamento para localSalida
     */
    private ReferenciaGeograficaAdapter adapterRefGeoSalida;
        /**
     * Adaptador para georeferencia distrito para localEntrega
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistritoEntrega;
    /**
     * Adaptador para geoReferencia ciudad para localEntrega
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudadEntrega;
    /**
     * Adaptador para georeferencia Departamento para localEntrega
     */
    private ReferenciaGeograficaAdapter adapterRefGeoEntrega;
    
    
    /**
     * POJO de cliente
     */
    private ClientePojo cliente;
    /**
     * Adaptador para cliente
     */
    private ClientPojoAdapter adapterCliente;
     /**
     * Lista de detalles para la nota de remision
     */
    private List<NotaRemisionDetalle> listaDetalle;
    /**
     * Adaptador para georeferencia distrito
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador para geoReferencia ciudad
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Adaptador para georeferencia Departamento
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para mostrar formulario
     */
    private boolean showForm;
    /**
     * Bandera para agregar campos de dirección
     */
    private String tieneDireccion;
    /**
     * POJO Producto
     */
    private Producto producto;
    /**
     * Adaptador producto;
     */
    private ProductoAdapter adapterProducto;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Pojo Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos nota de remisión
     */
    private DatosNotaRemision datosNotaRemision;   
    
    /**
     * Factura Talonario
     */
    private TalonarioPojo nrTalonario;
    
    /**
     * Adaptador de motivo emision para nota de remision
     */
    private MotivoEmisionNrAdapter motivoEmisionNrAdapterList;
    /**
     * Adaptador de transportista
     */
    private TransportistaListAdapter adapterTransportista;
    /**
     * Adaptador de vehiculo
     */
    private VehiculoTrasladoListAdapter adapterVehiculoTraslado;
       
    private boolean disableTransportista;
    
    private boolean disableVehiculo;
    
    private String tipoVehiculo;
    
    private String lote;
    
    private String fechaVencimientoLote;
    private Integer idOC;
    private String digital;
    private Integer nroLineaProducto;
    private Integer idCliente;
    private String ruc;
    private Integer linea;
    private Integer idProducto;
    private String descripcion;
    private NotaRemisionService serviceNotaRemision;
    private NotaRemisionTalonarioAdapter adapterNRTalonario;
    private String tieneFactura;
    private String cdcFactura;
    private String timbradoFactura;
    private String nroFactura;
    private Date fechaFactura;
    private Integer idNotaRemision;
    private NotaRemisionAdapter adapterNotaRemision; 
    private String motivoEmisionNr;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Transportista getTransportistaAutoComplete() {
        return transportistaAutoComplete;
    }

    public void setTransportistaAutoComplete(Transportista transportistaAutoComplete) {
        this.transportistaAutoComplete = transportistaAutoComplete;
    }

    public VehiculoTraslado getVehiculoTrasladoAutoComplete() {
        return vehiculoTrasladoAutoComplete;
    }

    public void setVehiculoTrasladoAutoComplete(VehiculoTraslado vehiculoTrasladoAutoComplete) {
        this.vehiculoTrasladoAutoComplete = vehiculoTrasladoAutoComplete;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(String fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    
    
    
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public boolean isDisableTransportista() {
        return disableTransportista;
    }

    public void setDisableTransportista(boolean disableTransportista) {
        this.disableTransportista = disableTransportista;
    }

    public boolean isDisableVehiculo() {
        return disableVehiculo;
    }

    public void setDisableVehiculo(boolean disableVehiculo) {
        this.disableVehiculo = disableVehiculo;
    }

    public VehiculoTrasladoListAdapter getAdapterVehiculoTraslado() {
        return adapterVehiculoTraslado;
    }

    public void setAdapterVehiculoTraslado(VehiculoTrasladoListAdapter adapterVehiculoTraslado) {
        this.adapterVehiculoTraslado = adapterVehiculoTraslado;
    }

    public TransportistaListAdapter getAdapterTransportista() {
        return adapterTransportista;
    }

    public void setAdapterTransportista(TransportistaListAdapter adapterTransportista) {
        this.adapterTransportista = adapterTransportista;
    }
    
    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    
    
    public String getTieneFactura() {
        return tieneFactura;
    }

    public void setTieneFactura(String tieneFactura) {
        this.tieneFactura = tieneFactura;
    }
    
    public NotaRemisionService getServiceNotaRemision() {
        return serviceNotaRemision;
    }

    public void setServiceNotaRemision(NotaRemisionService serviceNotaRemision) {
        this.serviceNotaRemision = serviceNotaRemision;
    }

    public NotaRemisionTalonarioAdapter getAdapterNRTalonario() {
        return adapterNRTalonario;
    }
    
    public void setAdapterNRTalonario(NotaRemisionTalonarioAdapter adapterNRTalonario) {
        this.adapterNRTalonario = adapterNRTalonario;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }
    
    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }
    
    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public Integer getIdOC() {
        return idOC;
    }

    public void setIdOC(Integer idOC) {
        this.idOC = idOC;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }
    
    

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getTieneDireccion() {
        return tieneDireccion;
    }

    public void setTieneDireccion(String tieneDireccion) {
        this.tieneDireccion = tieneDireccion;
    }

    
    
    public ClientePojo getCliente() {
        return cliente;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public ClientPojoAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientPojoAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public NotaRemision getNotaRemisionCreate() {
        return notaRemisionCreate;
    }

    public void setNotaRemisionCreate(NotaRemision notaRemisionCreate) {
        this.notaRemisionCreate = notaRemisionCreate;
    }

    public List<NotaRemisionDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<NotaRemisionDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public void setShowForm(boolean showForm) {
        this.showForm = showForm;
    }

    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public MotivoEmisionNrAdapter getMotivoEmisionNrAdapterList() {
        return motivoEmisionNrAdapterList;
    }

    public void setMotivoEmisionNrAdapterList(MotivoEmisionNrAdapter motivoEmisionNrAdapterList) {
        this.motivoEmisionNrAdapterList = motivoEmisionNrAdapterList;
    }
    
    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public VehiculoTraslado getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    public LocalSalidaEntrega getLocalSalida() {
        return localSalida;
    }

    public void setLocalSalida(LocalSalidaEntrega localSalida) {
        this.localSalida = localSalida;
    }

    public LocalSalidaEntrega getLocalEntrega() {
        return localEntrega;
    }

    public void setLocalEntrega(LocalSalidaEntrega localEntrega) {
        this.localEntrega = localEntrega;
    }

    public DatosNotaRemision getDatosNotaRemision() {
        return datosNotaRemision;
    }

    public void setDatosNotaRemision(DatosNotaRemision datosNotaRemision) {
        this.datosNotaRemision = datosNotaRemision;
    }

    public TalonarioPojo getNrTalonario() {
        return nrTalonario;
    }

    public void setNrTalonario(TalonarioPojo nrTalonario) {
        this.nrTalonario = nrTalonario;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistritoSalida() {
        return adapterRefGeoDistritoSalida;
    }

    public void setAdapterRefGeoDistritoSalida(ReferenciaGeograficaAdapter adapterRefGeoDistritoSalida) {
        this.adapterRefGeoDistritoSalida = adapterRefGeoDistritoSalida;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudadSalida() {
        return adapterRefGeoCiudadSalida;
    }

    public void setAdapterRefGeoCiudadSalida(ReferenciaGeograficaAdapter adapterRefGeoCiudadSalida) {
        this.adapterRefGeoCiudadSalida = adapterRefGeoCiudadSalida;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoSalida() {
        return adapterRefGeoSalida;
    }

    public void setAdapterRefGeoSalida(ReferenciaGeograficaAdapter adapterRefGeoSalida) {
        this.adapterRefGeoSalida = adapterRefGeoSalida;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistritoEntrega() {
        return adapterRefGeoDistritoEntrega;
    }

    public void setAdapterRefGeoDistritoEntrega(ReferenciaGeograficaAdapter adapterRefGeoDistritoEntrega) {
        this.adapterRefGeoDistritoEntrega = adapterRefGeoDistritoEntrega;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudadEntrega() {
        return adapterRefGeoCiudadEntrega;
    }

    public void setAdapterRefGeoCiudadEntrega(ReferenciaGeograficaAdapter adapterRefGeoCiudadEntrega) {
        this.adapterRefGeoCiudadEntrega = adapterRefGeoCiudadEntrega;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoEntrega() {
        return adapterRefGeoEntrega;
    }

    public void setAdapterRefGeoEntrega(ReferenciaGeograficaAdapter adapterRefGeoEntrega) {
        this.adapterRefGeoEntrega = adapterRefGeoEntrega;
    }

    public String getMotivoEmisionNr() {
        return motivoEmisionNr;
    }

    public void setMotivoEmisionNr(String motivoEmisionNr) {
        this.motivoEmisionNr = motivoEmisionNr;
    }
        
    //</editor-fold>
    
     /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        producto = new Producto();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto = adapterProducto.fillData(producto);
        producto = new Producto();

        return adapterProducto.getData();
    }
    
      /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            idProducto = ((Producto) event.getObject()).getId();
            descripcion = ((Producto) event.getObject()).getDescripcion();
            lote = ((Producto) event.getObject()).getNroLote();
            Date fechaVencLote = ((Producto) event.getObject()).getFechaVencimientoLote();
            if (fechaVencLote != null ) {
                fechaVencimientoLote = sdf.format(((Producto) event.getObject()).getFechaVencimientoLote());
            } else {
                fechaVencimientoLote = null;
            }
            
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
      /**
     * Método para agregar el producto con validaciones
     */
    public void agregarProducto() {
        boolean save = true;
        NotaRemisionDetalle info = null;

        for (NotaRemisionDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (NotaRemisionDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), idProducto)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }

        if (save == true) {
            WebLogger.get().debug("AGREGAR DATOS DETALLE");
            info.setDescripcion(descripcion);
            info.setIdProducto(idProducto);
            info.setLote(lote);
            info.setFechaVencimientoLote(fechaVencimientoLote);
         
        }
    }
    
        /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            notaRemisionCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        }

    }
    
    public void onFacturaDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            this.fechaFactura=today.getTime();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        }

    }
    
    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ClientePojo> completeQuery(String query) {

        ClientePojo cliente = new ClientePojo();
        cliente.setRazonSocial(query);
        cliente.setListadoPojo(true);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }
    
     /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteFactura(SelectEvent event) {
       
        notaRemisionCreate = new NotaRemision();

        if (((ClientePojo) event.getObject()).getIdNaturalezaCliente() != null) {
            notaRemisionCreate.setIdNaturalezaCliente(((ClientePojo) event.getObject()).getIdNaturalezaCliente());
        }
        String nroDoc = ((ClientePojo) event.getObject()).getNroDocumento();
        if (nroDoc != null){
            ruc = nroDoc.trim().replace(" ", "").replace(".","");
        }
        notaRemisionCreate.setRazonSocial(((ClientePojo) event.getObject()).getRazonSocial());
        idCliente = ((ClientePojo) event.getObject()).getIdCliente();
        notaRemisionCreate.setRuc(ruc);
        notaRemisionCreate.setIdCliente(idCliente);
        
        Calendar today = Calendar.getInstance();
        notaRemisionCreate.setFecha(today.getTime());
        notaRemisionCreate.setAnulado('N');
        
        obtenerDatosNC();

        showForm = true;

    }
    
    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea;
    }
    

    /**
     * Método para agregar detalle
     */
    public void addDetalle() {
        try {
            NotaRemisionDetalle detalle = new NotaRemisionDetalle();

            if (listaDetalle.isEmpty()) {
                detalle.setNroLinea(linea);
            } else {
                linea++;
                detalle.setNroLinea(linea);
            }

            detalle.setDescripcion("--");
            detalle.setCantidad(BigDecimal.valueOf(1));
            listaDetalle.add(detalle);
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
    /**
     * Método para eliminar el detalle del listado
     *
     * @param rowKey
     */
    public void deleteDetalle(Integer nroLinea) {
        NotaRemisionDetalle info = null;

        for (NotaRemisionDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                listaDetalle.remove(info);
                break;
            }
        }

    }
    
       /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-remision-create-form:form-data:panel-cabecera-detalle:distrito");
        PF.current().ajax().update(":list-nota-remision-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-nota-remision-create-form:form-data:panel-cabecera-detalle:ciudad");

        return datos;

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        // referenciaGeoDistrito = new ReferenciaGeografica();
        // referenciaGeoCiudad = new ReferenciaGeografica();
        // adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        notaRemisionCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        //referenciaGeoCiudad = new ReferenciaGeografica();
        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        notaRemisionCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        notaRemisionCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    public void filterDepartamento() {
        WebLogger.get().debug("FILTRO DEPARTAMENTO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        WebLogger.get().debug("FILTRO DISTRITO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        WebLogger.get().debug("FILTRO CIUDAD");
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
    public void filterDepartamentoSalida() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeoSalida.setPageSize(50);
        this.adapterRefGeoSalida = this.adapterRefGeoSalida.fillData(referenciaGeo);
    }

    public void filterDistritoSalida() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(localSalida.getIdDepartamento());
        this.adapterRefGeoDistritoSalida.setPageSize(300);
        this.adapterRefGeoDistritoSalida = this.adapterRefGeoDistritoSalida.fillData(referenciaGeo);
    }

    public void filterCiudadSalida() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(localSalida.getIdDistrito());
        this.adapterRefGeoCiudadSalida.setPageSize(300);
        this.adapterRefGeoCiudadSalida = this.adapterRefGeoCiudadSalida.fillData(referenciaGeo);
    }

    public void filterGeneralSalida() {
        filterDistritoSalida();
        filterCiudadSalida();
    }
    
        public void filterDepartamentoEntrega() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeoEntrega.setPageSize(50);
        this.adapterRefGeoEntrega = this.adapterRefGeoEntrega.fillData(referenciaGeo);
    }

    public void filterDistritoEntrega() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(localEntrega.getIdDepartamento());
        this.adapterRefGeoDistritoEntrega.setPageSize(300);
        this.adapterRefGeoDistritoEntrega = this.adapterRefGeoDistritoEntrega.fillData(referenciaGeo);
    }

    public void filterCiudadEntrega() {
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(localEntrega.getIdDistrito());
        this.adapterRefGeoCiudadEntrega.setPageSize(300);
        this.adapterRefGeoCiudadEntrega = this.adapterRefGeoCiudadEntrega.fillData(referenciaGeo);
    }

    public void filterGeneralEntrega() {
        filterDistritoEntrega();
        filterCiudadEntrega();
    }
    
    public void filterRefGeoSelectors(){
        filterDepartamento();
        filterDistrito();
        filterCiudad();
        
        filterDepartamentoSalida();
        filterDistritoSalida();
        filterCiudadSalida();
        
        filterDepartamentoEntrega();
        filterDistritoEntrega();
        filterCiudadEntrega();
    }
    
    
    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }
    
     /**
     * Método para crear Nota de Crédito
     */
    
    public void formatearCampos(){
        
        if (notaRemisionCreate.getVehiculoTraslado() != null){
            if (notaRemisionCreate.getVehiculoTraslado().getMarca() != null) {
                String str = notaRemisionCreate.getVehiculoTraslado().getMarca().trim().replace(" ","");
                notaRemisionCreate.getVehiculoTraslado().setMarca(str);
            }
            
            if (notaRemisionCreate.getVehiculoTraslado().getNroIdentificacion() != null){
                String str = notaRemisionCreate.getVehiculoTraslado().getNroIdentificacion().trim().replace(" ", "").replace(".", "");
                notaRemisionCreate.getVehiculoTraslado().setNroIdentificacion(str);
            }
            
            if (notaRemisionCreate.getVehiculoTraslado().getNroMatricula() != null){
                String str = notaRemisionCreate.getVehiculoTraslado().getNroMatricula().trim().replace(" ", "").replace(".", "");
                notaRemisionCreate.getVehiculoTraslado().setNroMatricula(str);
            }
            
            if (notaRemisionCreate.getVehiculoTraslado().getNroVuelo() != null){
                String str = notaRemisionCreate.getVehiculoTraslado().getNroVuelo().trim().replace(" ", "").replace(".", "");
                notaRemisionCreate.getVehiculoTraslado().setNroVuelo(str);
            }
     
        }
        
        if (notaRemisionCreate.getTransportista() != null) {
            if (notaRemisionCreate.getTransportista().getNroDocumento() != null) {
                notaRemisionCreate.getTransportista().setNroDocumento(notaRemisionCreate.getTransportista().getNroDocumento().trim().replace(" ", "").replace(".", ""));
            }
            
            if (notaRemisionCreate.getTransportista().getNroDocumentoChofer() != null) { 
                notaRemisionCreate.getTransportista().setNroDocumentoChofer(notaRemisionCreate.getTransportista().getNroDocumentoChofer().trim().replace(" ", "").replace(".", ""));
            }
            
            if (notaRemisionCreate.getTransportista().getRuc() != null) {   
                String ruc =  notaRemisionCreate.getTransportista().getRuc().trim().replace(" ", "").replace(".", "");
                
                //  Se extrael el digito verificador del ruc si fue ingresado.       
                if (ruc.contains("-")){
                    String[] nroRuc = ruc.split("-");
                    ruc = nroRuc[0];
                }
                notaRemisionCreate.getTransportista().setRuc(ruc);
            }

        }

    }
    
    public boolean validadorDePatron(String cadena, String patronRegex) {
        try {
            Pattern pattern = Pattern.compile(patronRegex);

            Matcher matcher = pattern.matcher(cadena);

            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
    
   
    public boolean validarCampos() {
        boolean saveNr = true;
        if (transportista != null) {
            if (transportista.getIdNaturalezaTransportista() != null && transportista.getIdNaturalezaTransportista() == 1) {
                if (!transportista.getRuc().trim().isEmpty() && transportista.getRuc().trim().length() >= 3) {
                    //Validar que el RUC solo tenga dígitos
                    if (!validadorDePatron(transportista.getRuc(),"^[0-9]+$")){
                        saveNr = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                    }
                    
                } else {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                }
                if (transportista.getVerificadorRuc() != null && (transportista.getVerificadorRuc()< 0 || transportista.getVerificadorRuc()> 9 )) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un dígito verificador de RUC valido!"));
                }  
                
                if (transportista.getRazonSocial().trim().equalsIgnoreCase("") || transportista.getRazonSocial().length()<4) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre/razon social de transportista válido!"));
                }
            }
            
            if (transportista.getNombreCompleto().trim().equalsIgnoreCase("") || transportista.getNombreCompleto().length()<4) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre de chofer válido!"));
            }
            
            if (transportista.getDireccionChofer().trim().equalsIgnoreCase("") || transportista.getDireccionChofer().length()<1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección del chofer!"));
            }
            
            if (transportista.getDomicilioFiscal().trim().equalsIgnoreCase("") || transportista.getDomicilioFiscal().length()<1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el domicilio fiscal!"));
            }
            
        }
        
        vehiculoTraslado.setTipoVehiculo(tipoVehiculo);
        
        notaRemisionCreate.setTransportista(transportista);
        if (localEntrega != null) {
            if (localEntrega.getDireccion().trim().replace(" ", "").isEmpty()) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección de entrega!"));
            }
        }

        if (localSalida != null) {
            if (localSalida.getDireccion().trim().replace(" ", "").isEmpty()) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección de partida!"));
            }
        }
        notaRemisionCreate.setLocalEntrega(localEntrega);
        notaRemisionCreate.setLocalSalida(localSalida);
        notaRemisionCreate.setVehiculoTraslado(vehiculoTraslado);
        formatearCampos();
        
            if (notaRemisionCreate.getDireccion() == null || notaRemisionCreate.getDireccion().trim().isEmpty()) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
            }

            if (notaRemisionCreate.getNroCasa() == null || notaRemisionCreate.getNroCasa().trim().isEmpty()) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
            }

            if (idDepartamento == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
            } else {
                notaRemisionCreate.setIdDepartamento(idDepartamento);
            }

            if (idDistrito == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
            } else {
                notaRemisionCreate.setIdDistrito(idDistrito);
            }

            if (idCiudad == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
            } else {
                notaRemisionCreate.setIdCiudad(idCiudad);
            }


        if (digital.equals("S")) {
            notaRemisionCreate.setArchivoSet("S");
        } else {
            notaRemisionCreate.setArchivoSet("N");
        }
        
        if(tieneFactura.equals("F")){
           
            if (timbradoFactura == null || timbradoFactura.trim().isEmpty()){
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el timbrado!"));
            } else {
                notaRemisionCreate.setTimbradoFactura(timbradoFactura);
            }
            
             if (nroFactura == null || nroFactura.trim().isEmpty()){
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el número de factura!"));
            } else{
                notaRemisionCreate.setNroFactura(nroFactura);
            }
           
            if (fechaFactura == null){
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la fecha de emisión de la factura!"));
            } else {
                notaRemisionCreate.setFechaFactura(fechaFactura);
            }
            
           
        } else if (tieneFactura.equals("D")){
            if (cdcFactura == null){
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el CDC!"));
            }
            notaRemisionCreate.setCdcFactura(cdcFactura);
        }

        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
        }
        notaRemisionCreate.setTieneFactura(tieneFactura.charAt(0));
        notaRemisionCreate.setNotaRemisionDetalles(listaDetalle);
        notaRemisionCreate.setDigital(digital);
        
        if (notaRemisionCreate.getKm() == null || notaRemisionCreate.getKm() < 1) {
            saveNr = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar los kilometros recorridos!"));
        }
        
        if (notaRemisionCreate.getIdMotivoEmisionNr() != null) {
            if (notaRemisionCreate.getIdMotivoEmisionNr() == 1 && notaRemisionCreate.getTieneFactura().compareTo('D') != 0 ){
                if (notaRemisionCreate.getFechaFuturaEmision()== null){
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la fecha futura de emisión!"));
                } 
            }
        } else {
             saveNr = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar un motivo de emisión!"));
        }
        return saveNr;
    }
    
    public void createNotaRemision() {
        boolean saveNC = validarCampos();
        if (saveNC) {
            BodyResponse<NotaRemision> respuestaDeNr = serviceNotaRemision.createNotaRemision(notaRemisionCreate);

            if (respuestaDeNr.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de Remisión creada correctamente!"));
                try {
                    NotaRemision nr = (NotaRemision) respuestaDeNr.getPayload();
                    GeneracionDteNotaRemisionMultiempresa.generar(nr.getIdEmpresa(), nr.getId());
                } catch (Exception e){
                    WebLogger.get().fatal(e);
                }
                init();
            }
        }

    }
    
     public void obtenerDatosNC() {
        try {

            Map parametros = new HashMap();
            datosNotaRemision = new DatosNotaRemision();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (notaRemisionCreate.getIdCliente() != null) {
                parametros.put("idCliente", notaRemisionCreate.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datosNotaRemision = serviceNotaRemision.getDatosNotaRemision(parametros);

            if (datosNotaRemision != null) {
                notaRemisionCreate.setFecha(today.getTime());
                notaRemisionCreate.setIdTalonario(datosNotaRemision.getIdTalonario());
                notaRemisionCreate.setNroNotaRemision(datosNotaRemision.getNroNotaRemision());

                //Validaciones
//                if (datosNotaRemision.getTelefono() != null) {
//                    notaRemisionCreate.setTelefono(datosNotaRemision.getTelefono());
//                }
//                if (datosNotaRemision.getEmail() != null) {
//                    notaRemisionCreate.setEmail(datosNotaRemision.getEmail());
//                }

                if (datosNotaRemision.getDireccion() != null) {
                    notaRemisionCreate.setDireccion(datosNotaRemision.getDireccion());
                }
                if (datosNotaRemision.getIdDepartamento() != null) {
                    notaRemisionCreate.setIdDepartamento(datosNotaRemision.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datosNotaRemision.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datosNotaRemision.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datosNotaRemision.getIdDistrito() != null) {
                    notaRemisionCreate.setIdDistrito(datosNotaRemision.getIdDistrito());
                    direccionFilter.setIdDistrito(datosNotaRemision.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datosNotaRemision.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datosNotaRemision.getIdCiudad() != null) {
                    notaRemisionCreate.setIdCiudad(datosNotaRemision.getIdCiudad());
                    direccionFilter.setIdCiudad(datosNotaRemision.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datosNotaRemision.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datosNotaRemision.getNroCasa() != null) {
                    notaRemisionCreate.setNroCasa(datosNotaRemision.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
     
     public void obtenerConfiguracion() {
        try {
//            configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_NC_DIGITAL");
//            configuracionValorFilter.setActivo("S");
//            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
//            //digital = adapterConfigValor.getData().get(0).getValor();
//            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
//                digital = "N";
//            } else {
//                digital = adapterConfigValor.getData().get(0).getValor();
//            }
            digital = "S";
            obtenerDatosTalonario();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
     
    /* Método para obtener los transportista filtrados
     *
     * @param query
     * @return
     */
    public List<Transportista> completeQueryTransportista(String query) {

        Transportista transportistaFilter = new Transportista();
        transportistaFilter.setRazonSocial(query);
        transportistaFilter.setIdEmpresa(session.getUser().getIdEmpresa());
        transportistaFilter.setActivo("S");
        adapterTransportista = adapterTransportista.fillData(transportistaFilter);
        
        return adapterTransportista.getData();
    }
    
    
    /* Método para obtener los vehiculos de transporte filtrados
     *
     * @param query
     * @return
     */
    public List<VehiculoTraslado> completeQueryVehiculo(String query) {

        VehiculoTraslado vehiculoFilter = new VehiculoTraslado();
        vehiculoFilter.setMarca(query);
        vehiculoFilter.setIdEmpresa(session.getUser().getIdEmpresa());
        vehiculoFilter.setActivo("S");
        adapterVehiculoTraslado = adapterVehiculoTraslado.fillData(vehiculoFilter);
        
        return adapterVehiculoTraslado.getData();
    }
    
    /**
     * Método para seleccionar el transportista
     */
    public void onItemSelectTransportista(SelectEvent event) {
        try {
            transportista = ((Transportista) event.getObject()); 
            disableTransportista = true;
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
    /**
     * Método para seleccionar el vehiculo de traslado
     */
    public void onItemSelectVehiculo(SelectEvent event) {
        try {
            vehiculoTraslado = ((VehiculoTraslado) event.getObject()); 
            switch (vehiculoTraslado.getTipoVehiculo()){
                case ("CARGA"):
                    this.notaRemisionCreate.setModalidadTransporte(1);
                    break;
                case("FLUVIAL"):
                    this.notaRemisionCreate.setModalidadTransporte(2);
                    break;
                case("AEREO"):
                    this.notaRemisionCreate.setModalidadTransporte(3);
                    break;      
            }
             disableVehiculo = true;
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
    public void clearTransportista() {
        this.disableTransportista = false;
        this.transportista = new Transportista();
        this.transportista.setActivo("S");
        
    }
    
    public void clearVehiculo() {
        this.disableVehiculo = false;
        this.vehiculoTraslado = new VehiculoTraslado();
        this.vehiculoTraslado.setTipoVehiculo("CARGA");
        this.vehiculoTraslado.setActivo("S");
        
    }


    public void obtenerDatosTalonario() {
        nrTalonario.setDigital(digital);
        nrTalonario.setIdEncargado(session.getUser().getIdPersonaFisica());
        adapterNRTalonario = adapterNRTalonario.fillData(nrTalonario);

        if (adapterNRTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            PF.current().ajax().update(":list-nota-remision-create-form:form-data:pnl-pop-up");
        } else if (adapterNRTalonario.getData().size() > 0){
            talonario.setId(adapterNRTalonario.getData().get(0).getId());
            obtenerDatosNC();
        } else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontró un talonario"));
        }
    }
   
    public void obtenerMotivoEmisionNr() {
        MotivoEmision menr = new MotivoEmision();
        motivoEmisionNrAdapterList = motivoEmisionNrAdapterList.fillData(menr);
    }
    
    
    public void precargarNR() {

        NotaRemision nrFilter = new NotaRemision();
        nrFilter.setId(idNotaRemision);
        adapterNotaRemision = adapterNotaRemision.fillData(nrFilter);
        if (!adapterNotaRemision.getData().isEmpty()) {
            notaRemisionCreate = adapterNotaRemision.getData().get(0);
            notaRemisionCreate.setId(null);
            vehiculoTraslado = adapterNotaRemision.getData().get(0).getVehiculoTraslado();
            transportista = adapterNotaRemision.getData().get(0).getTransportista();
            localEntrega = adapterNotaRemision.getData().get(0).getLocalEntrega();
            localSalida = adapterNotaRemision.getData().get(0).getLocalSalida();
            listaDetalle = adapterNotaRemision.getData().get(0).getNotaRemisionDetalles();
            idCliente = adapterNotaRemision.getData().get(0).getIdCliente();
            ClientePojo clienteFilter = new ClientePojo();
            clienteFilter.setIdCliente(idCliente);
            clienteFilter.setListadoPojo(true);
            adapterCliente = adapterCliente.fillData(clienteFilter);
            if (!adapterCliente.getData().isEmpty()) {
                cliente = adapterCliente.getData().get(0);
            }
            obtenerConfiguracion();
            filterRefGeoSelectors();
            idDepartamento = adapterNotaRemision.getData().get(0).getIdDepartamento();
            idDistrito = adapterNotaRemision.getData().get(0).getIdDistrito();
            idCiudad = adapterNotaRemision.getData().get(0).getIdCiudad();
            showForm = true;
        }
    }
    
    @PostConstruct
    public void init(){
        this.notaRemisionCreate = new NotaRemision();       
        this.transportista = new Transportista();
        this.localSalida = new LocalSalidaEntrega();
        this.localEntrega = new LocalSalidaEntrega();
        this.vehiculoTraslado = new VehiculoTraslado();
        this.tipoVehiculo = "CARGA";
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudadSalida = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistritoSalida = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoSalida = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudadEntrega = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistritoEntrega = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoEntrega = new ReferenciaGeograficaAdapter();
        this.direccionAdapter = new DireccionAdapter();
        this.direccionFilter = new Direccion();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.tieneFactura = "N";
        this.showForm = false;
        this.linea = 1;
        
        
        this.cliente = new ClientePojo();
        this.adapterCliente = new ClientPojoAdapter();
        
        this.adapterProducto = new ProductoAdapter();
        this.listaDetalle = new ArrayList();
        this.serviceNotaRemision = new NotaRemisionService();
        this.nrTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();
        this.adapterNRTalonario = new NotaRemisionTalonarioAdapter();
        this.motivoEmisionNrAdapterList = new MotivoEmisionNrAdapter();
        this.adapterTransportista = new TransportistaListAdapter();
        this.adapterVehiculoTraslado = new VehiculoTrasladoListAdapter();
        this.disableTransportista = false;
        this.disableVehiculo = false;
        this.adapterNotaRemision = new NotaRemisionAdapter();
        obtenerMotivoEmisionNr();
        
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("idNotaRemision") != null) {
            this.idNotaRemision = Integer.parseInt(params.get("idNotaRemision"));
            precargarNR();
        } else {
            obtenerConfiguracion();
            filterRefGeoSelectors();
        }
        
    }

}
