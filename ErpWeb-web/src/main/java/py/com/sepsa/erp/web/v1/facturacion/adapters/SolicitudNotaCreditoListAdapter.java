package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.remote.SolicitudNotaCreditoService;

/**
 * Adaptador de la lista solicitud de nota credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class SolicitudNotaCreditoListAdapter extends DataListAdapter<SolicitudNotaCredito> {

    /**
     * Cliente para el servicio de cobro
     */
    private final SolicitudNotaCreditoService serviceSolicitudNotaCredito;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public SolicitudNotaCreditoListAdapter fillData(SolicitudNotaCredito searchData) {

        return serviceSolicitudNotaCredito.getSolicitudNotaCreditoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de SolicitudNotaCreditoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public SolicitudNotaCreditoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceSolicitudNotaCredito = new SolicitudNotaCreditoService();
    }

    /**
     * Constructor de SolicitudNotaCreditoAdapter
     */
    public SolicitudNotaCreditoListAdapter() {
        super();
        this.serviceSolicitudNotaCredito = new SolicitudNotaCreditoService();
    }
}
