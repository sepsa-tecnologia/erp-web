/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetallePojo;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoDetallePojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de idNotaDebito
     *
     * @param idNotaDebito
     * @return
     */
    public NotaDebitoDetallePojoFilter idNotaDebito(Integer idNotaDebito) {
        if (idNotaDebito != null) {
            params.put("idNotaDebito", idNotaDebito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idFactura
     *
     * @param idFactura
     * @return
     */
    public NotaDebitoDetallePojoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nroLinea
     *
     * @param nroLinea
     * @return
     */
    public NotaDebitoDetallePojoFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de porcentajeIva
     *
     * @param porcentajeIva
     * @return
     */
    public NotaDebitoDetallePojoFilter porcentajeIva(Integer porcentajeIva) {
        if (porcentajeIva != null) {
            params.put("porcentajeIva", porcentajeIva);
        }
        return this;
    }

    /**
     * Agrega el filtro para montoIva
     *
     * @param montoIva
     * @return
     */
    public NotaDebitoDetallePojoFilter montoIva(BigDecimal montoIva) {
        if (montoIva != null) {
            params.put("montoIva", montoIva.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoImponible
     *
     * @param montoImponible
     * @return
     */
    public NotaDebitoDetallePojoFilter montoImponible(BigDecimal montoImponible) {
        if (montoImponible != null) {
            params.put("montoImponible", montoImponible.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoTotal
     *
     * @param montoTotal
     * @return
     */
    public NotaDebitoDetallePojoFilter montoTotal(BigDecimal montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal.toPlainString());
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaDebitoDetallePojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agrega el filtro para anhoMes
     *
     * @param anhoMes
     * @return
     */
    public NotaDebitoDetallePojoFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaDebitoDetallePojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param nd
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaDebitoDetallePojo nd, Integer page, Integer pageSize) {
        NotaDebitoDetallePojoFilter filter = new NotaDebitoDetallePojoFilter();

        filter
                .idNotaDebito(nd.getIdNotaDebito())
                .idFactura(nd.getIdFactura())
                .nroLinea(nd.getNroLinea())
                .porcentajeIva(nd.getPorcentajeIva())
                .montoIva(nd.getMontoIva())
                .montoImponible(nd.getMontoImponible())
                .montoTotal(nd.getMontoTotal())
                .anhoMes(nd.getAnhoMes())
                .anulado(nd.getAnulado())
                .listadoPojo(nd.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaDebitoDetallePojoFilter
     */
    public NotaDebitoDetallePojoFilter() {
        super();
    }
}
