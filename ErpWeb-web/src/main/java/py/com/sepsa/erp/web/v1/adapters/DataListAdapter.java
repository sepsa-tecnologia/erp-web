
package py.com.sepsa.erp.web.v1.adapters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Adaptador para la lista de datos
 * @author Daniel F. Escauriza Arza
 * @param <T> Tipo de dato
 */
public abstract class DataListAdapter<T> {


    
    //Tamaño máximo de una página
    private final static int PAGE_SIZE = 10;
    private final static int SKIP_PAGE = 5;

    /**
     * Página
     */
    private int page;
    /**
     * Tamaño de la página
     */
    protected int pageSize;
    /**
     * Cantidad total de páginas
     */
    private long totalPages;
    /**
     * Tamaño total de la lista
     */
    protected long totalSize;
    /**
     * Lista de datos
     */
    protected List<T> data;
    /**
     * Primer registro
     */
    protected int firstResult;
    /**
     * Objeto filtro
     */
    protected T filter;

    
    /**
     * @return the firstResult
     */
    public int getFirstResult() {
        return firstResult;
    }

    /**
     * @param firstResult the firstResult to set
     */
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }
    /**
     * Obtiene la página
     * @return Página
     */
    public int getPage() {
        return page;
    }

    /**
     * Setea la página
     * @param page Página
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * Obtiene el tamaño de la página
     * @return Tamaño de la página
     */
    public int getPageSize() {
        return pageSize;
    }

    
    /**
     * @return the filter
     */
    public T getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(T filter) {
        this.filter = filter;
    }

    /**
     * Setea el tamaño de la página
     * @param pageSize Tamaño de la página
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Obtiene la cantidad total de páginas
     * @return Cantidad total de páginas
     */
    public long getTotalPages() {
        BigDecimal temp = new BigDecimal(totalSize)
                .divide(new BigDecimal(pageSize), RoundingMode.UP);
        this.totalPages = temp.intValue();

        return totalPages;
    }

    /**
     * Setea la cantidad total de páginas
     * @param totalPages Cantidad total de páginas
     */
    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * Obtiene el tamaño total de la lista
     * @return Tamaño total de la lista
     */
    public long getTotalSize() {
        return  totalSize;
    }

    /**
     * Setea el tamaño total de la página
     * @param totalSize Tamaño total de la página
     */
    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }

    /**
     * Obtiene la lista de datos
     * @return Lista de datos
     */
    public List<T> getData() {
        return data;
    }

    /**
     * Setea la lista de datos
     * @param data Lista de datos
     */
    public void setData(List<T> data) {
        this.data = data;
    }

    /**
     * Definición del método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return DataListAdapter
     */
    public abstract DataListAdapter fillData(T searchData);

    /**
     * Selecciona una página
     * @param searchData Datos de búsqueda
     * @param page Número de página
     */
    public void setPage(T searchData, int page) {

        if(page >= 0 && page < totalPages) {
            this.page = page;
        }

        int first = this.page*this.pageSize;

        if(first >= 0 && first < totalSize) {
            this.firstResult = first;
        }

        try {
            DataListAdapter adapter = fillData(searchData);
            this.data = adapter.getData();
        } catch (Exception ex) {
            WebLogger.get().fatal(ex);
        }
    }

    /**
     * Suma un número de páginas determinado
     * @param searchData Datos de búsqueda
     * @param val Número de páginas a sumar
     */
    private void addPage(T searchData, int val) {
        int newPage = page + val;

        if(newPage >= totalPages) {
            this.page =  (int) totalPages - 1;
        } else {
            this.page = newPage;
        }

        int first = this.page*this.pageSize;

        if(first >= totalSize) {
            this.firstResult =  (int) totalSize - (1*pageSize);
        } else {
            this.firstResult = first;
        }

        try {
            DataListAdapter adapter = fillData(searchData);
            this.data = adapter.getData();
        } catch (Exception ex) {
            WebLogger.get().fatal(ex);
        }
    }

    /**
     * Resta un número de páginas determinado
     * @param searchData Datos de búsqueda
     * @param val Número de páginas a restar
     */
    private void restPage(T searchData, int val) {
        int newPage = page - val;

        if(newPage <= 0) {
            this.page = 0;
        } else {
            this.page = newPage;
        }

        int first = this.page*this.pageSize;

        if(first <= 0) {
            this.firstResult = 0;
        } else {
            this.firstResult = first;
        }

        try {
            DataListAdapter adapter = fillData(searchData);
            this.data = adapter.getData();
        } catch (Exception ex) {
            WebLogger.get().fatal(ex);
        }
    }

    /**
     * Método para ir a la página siguiente
     * @param searchData Datos de búsqueda
     */
    public void nextPage(T searchData) {

        this.addPage(searchData, 1);
    }

    /**
     * Método para ir a la página anterior
     * @param searchData Datos de búsqueda
     */
    public void previousPage(T searchData) {
        this.restPage(searchData, 1);
    }

    /**
     * Método para adelantar {@value AbstractAdapterImpl#SKIP_PAGE} páginas
     * @param searchData Datos de búsqueda
     */
    public void skipForwardPage(T searchData) {
        this.addPage(searchData, SKIP_PAGE);
    }

    /**
     * Retrocede {@value AbstractAdapterImpl#SKIP_PAGE} páginas
     * @param searchData Datos de búsqueda
     */
    public void skipBackPage(T searchData) {
        this.restPage(searchData, SKIP_PAGE);
    }

    /**
     * Constructor de DataListAdapter
     * @param page Página
     * @param pageSize Tamaño de la página
     */
    public DataListAdapter(Integer page, Integer pageSize) {

        this.page = page == null ? 0 : page;
        this.pageSize = pageSize == null
                ? PAGE_SIZE
                : pageSize;
        this.firstResult = this.page * this.pageSize;
        this.data = data == null ? new ArrayList() : data;
    }

    /**
     * Constructor de DataListAdapter
     */
    public DataListAdapter() {

        this.firstResult = 0;
        this.page = 0;
        this.pageSize = PAGE_SIZE;
        this.data = new ArrayList();

        
        
       
    }

}
