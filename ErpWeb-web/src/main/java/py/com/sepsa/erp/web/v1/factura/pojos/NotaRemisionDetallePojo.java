package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para Detalle Nota Credito
 *
 * @author Romina Núñez, Cristina Insfrán ,Sergio D. Riveros Vazquez
 */
public class NotaRemisionDetallePojo {
    /**
     * Identificador de Nota de Crédito
     */
    private Integer idNotaRemision;
    /**
     * Nro nota de crédito
     */
    private String nroNotaRemision;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Fecha nota de crédito
     */
    private Date fechaNotaRemision;

    /**
     * Razón social
     */
    private String razonSocial;

    /**
     * RUC
     */
    private String ruc;
    /**
     * Nro de factura
     */
    private String nroFactura;

    /**
     * Fecha factura
     */
    private Date fechaFactura;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * Monto IVA
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * anhoMes
     */
    private String anhoMes;
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    /**
     * Precio unitario
     */
    private BigDecimal precioUnitarioConIva;
        /**
     * Precio unitario
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * Identificador de factura de compra
     */
    private Integer idFacturaCompra;
    private Boolean listadoPojo;

    //<editor-fold defaultstate="collapsed" desc="***GETTER & SETTER***">

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }


    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }
    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }
    public BigDecimal getMontoImponible() {
        return montoImponible;
    }
    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }
    public BigDecimal getMontoTotal() {
        return montoTotal;
    }
    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }
    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }
    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }
    /**
     * @return the fechaFactura
     */
    public Date getFechaFactura() {
        return fechaFactura;
    }
    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }
    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }
    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * @return anulado
     */
    public String getAnulado() {
        return anulado;
    }

    /**
     * @param anulado the anulado to set
     */
    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getAnhoMes() {
        return anhoMes;
    }

    public void setAnhoMes(String anhoMes) {
        this.anhoMes = anhoMes;
    }

    public Integer getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Integer idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Date getFechaNotaRemision() {
        return fechaNotaRemision;
    }

    public void setFechaNotaRemision(Date fechaNotaRemision) {
        this.fechaNotaRemision = fechaNotaRemision;
    }

//</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public NotaRemisionDetallePojo() {
    }
}
