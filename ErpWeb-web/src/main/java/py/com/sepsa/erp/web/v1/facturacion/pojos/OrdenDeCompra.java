/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;

/**
 * Pojo Para ordenes de compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OrdenDeCompra {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de Estado
     */
    private Integer idEstado;
    /**
     * Fecha de recepcion
     */
    private Date fechaRecepcion;
    /**
     * Fecha de insercion
     */
    private Date fechaInsercion;
    /**
     * Numero de orden de compra
     */
    private String nroOrdenCompra;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Fecha de recepcion desde
     */
    private Date fechaRecepcionDesde;
    /**
     * Fecha de recepcion hasta
     */
    private Date fechaRecepcionHasta;
    /**
     * Fecha de insercion desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha de insercion hasta
     */
    private Date fechaInsercionHasta;
    /**
     * Lista de emails asociados al contacto
     */
    private List<OrdenCompraDetalles> ordenCompraDetalles = new ArrayList();
    /**
     * Local Origen
     */
    private Local localOrigen;
    /**
     * Local Destino
     */
    private Local localDestino;
    /**
     * Monto IVA 5
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10
     */
    private BigDecimal montoImponible10;
    /**
     * Monto total 10
     */
    private BigDecimal montoTotal10;
    /**
     * Monto total exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total OC
     */
    private BigDecimal montoTotalOrdenCompra;
    /**
     * Codigo Estado
     */
    private String codigoEstado;
    /**
     * Codigo Estado
     */
    private String recibido;
    /**
     * Codigo Estado
     */
    private String anulado;
    /**
     * Dato Edi
     */
    private String generadoEdi;
    /**
     * Dato Edi
     */
    private String archivoEdi;

    /**
     * Identificador local origen
     */
    private Integer idLocalOrigen;
    /**
     * Identificador local destino
     */
    private Integer idLocalDestino;
    /**
     * Cliente Origen
     */
    private Integer idClienteOrigen;
    /**
     * Cliente Destino
     */
     private Integer idClienteDestino;
     private Cliente clienteOrigen;
     private Cliente clienteDestino;
     private String registroInventario;
     private Integer idDepositoLogistico;
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setRegistroInventario(String registroInventario) {
        this.registroInventario = registroInventario;
    }

    public String getRegistroInventario() {
        return registroInventario;
    }

    public void setClienteOrigen(Cliente clienteOrigen) {
        this.clienteOrigen = clienteOrigen;
    }

    public void setClienteDestino(Cliente clienteDestino) {
        this.clienteDestino = clienteDestino;
    }

    public Cliente getClienteOrigen() {
        return clienteOrigen;
    }

    public Cliente getClienteDestino() {
        return clienteDestino;
    }

    public Integer getIdClienteOrigen() {
        return idClienteOrigen;
    }

    public void setIdClienteOrigen(Integer idClienteOrigen) {
        this.idClienteOrigen = idClienteOrigen;
    }

    public Integer getIdClienteDestino() {
        return idClienteDestino;
    }

    public void setIdClienteDestino(Integer idClienteDestino) {
        this.idClienteDestino = idClienteDestino;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public void setArchivoEdi(String archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public String getArchivoEdi() {
        return archivoEdi;
    }

    public void setGeneradoEdi(String generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public String getGeneradoEdi() {
        return generadoEdi;
    }

    public String getRecibido() {
        return recibido;
    }

    public void setRecibido(String recibido) {
        this.recibido = recibido;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalOrdenCompra() {
        return montoTotalOrdenCompra;
    }

    public void setMontoTotalOrdenCompra(BigDecimal montoTotalOrdenCompra) {
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the fechaRecepcion
     */
    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    /**
     * @param fechaRecepcion the fechaRecepcion to set
     */
    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    /**
     * @return the fechaInsercion
     */
    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * @param fechaInsercion the fechaInsercion to set
     */
    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    /**
     * @return the nroOrdenCompra
     */
    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    /**
     * @param nroOrdenCompra the nroOrdenCompra to set
     */
    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Date getFechaRecepcionDesde() {
        return fechaRecepcionDesde;
    }

    public void setFechaRecepcionDesde(Date fechaRecepcionDesde) {
        this.fechaRecepcionDesde = fechaRecepcionDesde;
    }

    public Date getFechaRecepcionHasta() {
        return fechaRecepcionHasta;
    }

    public void setFechaRecepcionHasta(Date fechaRecepcionHasta) {
        this.fechaRecepcionHasta = fechaRecepcionHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public List<OrdenCompraDetalles> getOrdenCompraDetalles() {
        return ordenCompraDetalles;
    }

    public void setOrdenCompraDetalles(List<OrdenCompraDetalles> ordenCompraDetalles) {
        this.ordenCompraDetalles = ordenCompraDetalles;
    }

    //</editor-fold>
    public OrdenDeCompra() {
    }

    public OrdenDeCompra(Integer id) {
        this.id = id;
    }
}
