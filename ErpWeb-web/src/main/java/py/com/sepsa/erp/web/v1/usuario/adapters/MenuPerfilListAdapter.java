
package py.com.sepsa.erp.web.v1.usuario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;
import py.com.sepsa.erp.web.v1.usuario.pojos.MenuPerfil;

/**
 * Adaptador de la lista de perfiles por menu
 * @author Cristina Insfrán
 */
public class MenuPerfilListAdapter extends DataListAdapter<MenuPerfil>{
   
     /**
     * Cliente para el servicio de menu
     */
    private final MenuService serviceMenu;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MenuPerfilListAdapter fillData(MenuPerfil searchData) {
        return serviceMenu.getMenuPerfilList(searchData, getFirstResult(), getPageSize());

    }

    /**
     * Constructor de UsuarioPerfilListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MenuPerfilListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceMenu = new MenuService();
    }

    /**
     * Constructor de MenuPerfilListAdapter
     */
    public MenuPerfilListAdapter() {
        super();
        this.serviceMenu = new MenuService();
    }

}
