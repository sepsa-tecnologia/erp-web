 package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.task.GeneracionDteAutofacturaMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDncp;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.AutoFacturaService;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para Facturas
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("autofactura")
public class AutofacturaController implements Serializable {
    /**
     * Objeto Cliente
     */
    private ClientePojo cliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFactura;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFilterPrueba;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterClientePrueba2;
    /**
     * Variable de control
     */
    private boolean show;
    /**
     * Linea
     */
    private Integer linea;
    /**
     * ***DATOS REALES****
     */
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFacturaCancelada;
    /**
     * POJO para Factura
     */
    private Factura facturaFilterCancelada;
    /**
     * POJO DatosFactura
     */
    private DatosFactura datoFactura;
    /**
     * Servicio Facturacion
     */
    private AutoFacturaService serviceFacturacion;
    /**
     * POJO Factura para crear
     */
    private AutoFactura facturaCreate;
    /**
     * Dato del Cliente
     */
    private String ruc;
    /**
     * Dato del Cliente
     */
    private String direccion;
    /**
     * Dato del Cliente
     */
    private String telefono;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Adapter Historico Liquidación
     */
    private HistoricoLiquidacionAdapter adapterLiquidacion;
    /**
     * Historico Liquidación
     */
    private HistoricoLiquidacion historicoLiquidacionFilter;
    /**
     * Bandera
     */
    private boolean showDetalleCreate;
    /**
     * Lista Detalle
     */
    private List<AutoFacturaDetalle> listaDetalle;
    /**
     * Lista Selected Liquidación
     */
    private List<HistoricoLiquidacion> listSelectedLiquidacion = new ArrayList<>();
    /**
     * Mapa de Datos de Liquidación y nro Linea
     */
    private Map<Integer, Integer> liquidacionDetalle = new HashMap<Integer, Integer>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Producto Adaptador
     */
    private ProductoAdapter adapterProducto;
    /**
     * POJO Producto
     */
    private Producto producto;
    /**
     * POJO Producto
     */
    private Producto productoNuevo;
    /**
     * Service Factura
     */
    private FacturacionService serviceFactura;
        /**
     * Service AutoFactura
     */
    private AutoFacturaService autoFacturaService;
    /**
     * Adaptador parala lista de cliente
     */
    private ClientPojoAdapter adapter;
    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Dato para codigo de moneda
     */
    private String codMoneda;
    /**
     * Dato para habilitar panel de cotizacion
     */
    private boolean habilitarPanelCotizacion;
    /**
     * POJO Tipo Cambio
     */
    private TipoCambio tipoCambio;
    /**
     * Service tipo cambio
     */
    private TipoCambioService serviceTipoCambio;
    /**
     * Dato de nro de linea que se relaciona al producto
     */
    private Integer nroLineaProducto;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Descripción de producto
     */
    private String descripcion;
    /**
     * Porcentaje
     */
    private Integer porcentaje;
    /**
     * Unidad Mínima de Venta
     */
    private BigDecimal umv;
    /**
     * POJO Producto Precio para parámetro
     */
    private ProductoPrecio parametroProductoPrecio;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Adapter Factura Talonario
     */
    private AutoFacturaTalonarioAdapter adapterFacturaTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo facturaTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showDescargaPopup;
    /**
     * Dato a ser utilizado para la descarga
     */
    private Integer idFacturaCreada;
    /**
     * Dato a ser utilizado para la descarga
     */
    private String nroFacturaCreada;
    /**
     * Gravada
     */
    private String gravada;
    /**
     * Dato Bandera
     */
    private String ctaCtble;
    /**
     * Nro Cta Ctble
     */
    private String nroCtaCtble;
    /**
     * Dato de numeración dinámica
     */
    private Boolean numeracionDinamica;
    /**
     * Bandera para modulo de inventario
     */
    private String moduloInventario;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para agregar campos de licitacion
     */
    private String licitacion;
    /**
     * Factura Dncp
     */
    private FacturaDncp facturaDncp;
    /*
     * Bandera para agregar campo de Orden de Compra
     */
    private String tieneOC;
    /**
     * Número de orden de compra
     */
    private String nroOC;
    /**
     * Bandera que indica si el documento debe ser notificado
     */
    private String notificar;
    /**
     * Bandera que indica si el documento ha sido notificado
     */
    private Boolean notificado;
    /**
     * Bandera para indicar si el cliente se conecta al siediApi
     */
    private String siediApi;
    
    private String razonSocialEmpresa;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public String getRazonSocialEmpresa() {
        return razonSocialEmpresa;
    }

    public void setRazonSocialEmpresa(String razonSocialEmpresa) {
        this.razonSocialEmpresa = razonSocialEmpresa;
    }
    
    
    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setModuloInventario(String moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public String getModuloInventario() {
        return moduloInventario;
    }

    public Boolean getNumeracionDinamica() {
        return numeracionDinamica;
    }

    public void setNumeracionDinamica(Boolean numeracionDinamica) {
        this.numeracionDinamica = numeracionDinamica;
    }

    public void setNroCtaCtble(String nroCtaCtble) {
        this.nroCtaCtble = nroCtaCtble;
    }

    public String getNroCtaCtble() {
        return nroCtaCtble;
    }

    public void setCtaCtble(String ctaCtble) {
        this.ctaCtble = ctaCtble;
    }

    public String getCtaCtble() {
        return ctaCtble;
    }

    public void setGravada(String gravada) {
        this.gravada = gravada;
    }

    public String getGravada() {
        return gravada;
    }

    public void setNroFacturaCreada(String nroFacturaCreada) {
        this.nroFacturaCreada = nroFacturaCreada;
    }

    public String getNroFacturaCreada() {
        return nroFacturaCreada;
    }

    public void setIdFacturaCreada(Integer idFacturaCreada) {
        this.idFacturaCreada = idFacturaCreada;
    }

    public Integer getIdFacturaCreada() {
        return idFacturaCreada;
    }

    public void setShowDescargaPopup(boolean showDescargaPopup) {
        this.showDescargaPopup = showDescargaPopup;
    }

    public boolean isShowDescargaPopup() {
        return showDescargaPopup;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public AutoFacturaTalonarioAdapter getAdapterFacturaTalonario() {
        return adapterFacturaTalonario;
    }

    public void setAdapterFacturaTalonario(AutoFacturaTalonarioAdapter adapterFacturaTalonario) {
        this.adapterFacturaTalonario = adapterFacturaTalonario;
    }


    public void setFacturaTalonario(TalonarioPojo facturaTalonario) {
        this.facturaTalonario = facturaTalonario;
    }

    public TalonarioPojo getFacturaTalonario() {
        return facturaTalonario;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public void setParametroProductoPrecio(ProductoPrecio parametroProductoPrecio) {
        this.parametroProductoPrecio = parametroProductoPrecio;
    }

    public ProductoPrecio getParametroProductoPrecio() {
        return parametroProductoPrecio;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setProductoNuevo(Producto productoNuevo) {
        this.productoNuevo = productoNuevo;
    }

    public Producto getProductoNuevo() {
        return productoNuevo;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setServiceTipoCambio(TipoCambioService serviceTipoCambio) {
        this.serviceTipoCambio = serviceTipoCambio;
    }

    public TipoCambioService getServiceTipoCambio() {
        return serviceTipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setHabilitarPanelCotizacion(boolean habilitarPanelCotizacion) {
        this.habilitarPanelCotizacion = habilitarPanelCotizacion;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public boolean isHabilitarPanelCotizacion() {
        return habilitarPanelCotizacion;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapter(ClientPojoAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientPojoAdapter getAdapter() {
        return adapter;
    }

    public void setClienteFactura(Cliente clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public Cliente getClienteFactura() {
        return clienteFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public List<AutoFacturaDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<AutoFacturaDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public Map<Integer, Integer> getLiquidacionDetalle() {
        return liquidacionDetalle;
    }

    public void setLiquidacionDetalle(Map<Integer, Integer> liquidacionDetalle) {
        this.liquidacionDetalle = liquidacionDetalle;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListSelectedLiquidacion(List<HistoricoLiquidacion> listSelectedLiquidacion) {
        this.listSelectedLiquidacion = listSelectedLiquidacion;
    }

    public List<HistoricoLiquidacion> getListSelectedLiquidacion() {
        return listSelectedLiquidacion;
    }

    public void setShowDetalleCreate(boolean showDetalleCreate) {
        this.showDetalleCreate = showDetalleCreate;
    }

    public boolean isShowDetalleCreate() {
        return showDetalleCreate;
    }

    public void setHistoricoLiquidacionFilter(HistoricoLiquidacion historicoLiquidacionFilter) {
        this.historicoLiquidacionFilter = historicoLiquidacionFilter;
    }

    public void setAdapterLiquidacion(HistoricoLiquidacionAdapter adapterLiquidacion) {
        this.adapterLiquidacion = adapterLiquidacion;
    }

    public HistoricoLiquidacion getHistoricoLiquidacionFilter() {
        return historicoLiquidacionFilter;
    }

    public HistoricoLiquidacionAdapter getAdapterLiquidacion() {
        return adapterLiquidacion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setFacturaFilterCancelada(Factura facturaFilterCancelada) {
        this.facturaFilterCancelada = facturaFilterCancelada;
    }

    public void setAdapterFacturaCancelada(FacturaAdapter adapterFacturaCancelada) {
        this.adapterFacturaCancelada = adapterFacturaCancelada;
    }

    public Factura getFacturaFilterCancelada() {
        return facturaFilterCancelada;
    }

    public FacturaAdapter getAdapterFacturaCancelada() {
        return adapterFacturaCancelada;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setAdapterFactura(FacturaAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Cliente getClienteFilterPrueba() {
        return clienteFilterPrueba;
    }

    public void setClienteFilterPrueba(Cliente clienteFilterPrueba) {
        this.clienteFilterPrueba = clienteFilterPrueba;
    }

    public void setAdapterClientePrueba2(ClientListAdapter adapterClientePrueba2) {
        this.adapterClientePrueba2 = adapterClientePrueba2;
    }

    public ClientListAdapter getAdapterClientePrueba2() {
        return adapterClientePrueba2;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public void setDatoFactura(DatosFactura datoFactura) {
        this.datoFactura = datoFactura;
    }


    public DatosFactura getDatoFactura() {
        return datoFactura;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public ClientePojo getCliente() {
        return cliente;
    }

    public String getLicitacion() {
        return licitacion;
    }

    public void setLicitacion(String licitacion) {
        this.licitacion = licitacion;
    }

    public FacturaDncp getFacturaDncp() {
        return facturaDncp;
    }

    public void setFacturaDncp(FacturaDncp facturaDncp) {
        this.facturaDncp = facturaDncp;
    }

    public String getTieneOC() {
        return tieneOC;
    }

    public void setTieneOC(String tieneOC) {
        this.tieneOC = tieneOC;
    }
    
    public String getNroOC() {
        return nroOC;
    }

    public void setNroOC(String nroOC) {
        this.nroOC = nroOC;
    }

    public AutoFactura getFacturaCreate() {
        return facturaCreate;
    }

    public void setFacturaCreate(AutoFactura facturaCreate) {
        this.facturaCreate = facturaCreate;
    }

    public AutoFacturaService getAutoFacturaService() {
        return autoFacturaService;
    }

    public void setAutoFacturaService(AutoFacturaService autoFacturaService) {
        this.autoFacturaService = autoFacturaService;
    }

    public AutoFacturaService getServiceFacturacion() {
        return serviceFacturacion;
    }

    public void setServiceFacturacion(AutoFacturaService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    
   
//</editor-fold>

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        ruc = obtenerConfiguracion("RUC");
        razonSocialEmpresa = obtenerConfiguracion("RAZON_SOCIAL");
        
        configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_FACTURA_DIGITAL");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            digital = "N";
        } else {
            digital = adapterConfigValor.getData().get(0).getValor();
        }

        obtenerDatosTalonario();
    }

    /**
     * Método para obtener la configuración
     */
    public String obtenerConfiguracionGeneral(String codigo) {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "N";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }

    /**
     * Método para obtener datos de factura
     */
    public void obtenerDatosFacturacion() {

        try {
             String emptyString = null;
            Integer emptyInteger = null;
            Map parametros = new HashMap();
            datoFactura = new DatosFactura();
            Calendar today = Calendar.getInstance();
            direccionFilter = new Direccion();
            referenciaGeoDepartamento = new ReferenciaGeografica();
            referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoCiudad = new ReferenciaGeografica();
            facturaCreate.setTelefono(emptyString);
            facturaCreate.setEmail(emptyString);
            facturaCreate.setDireccion(emptyString);
            facturaCreate.setIdDepartamento(emptyInteger);
            facturaCreate.setIdDistrito(emptyInteger);
            facturaCreate.setIdCiudad(emptyInteger);
            facturaCreate.setNroCasa(emptyString);

            parametros.put("digital", digital);
            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoFactura = serviceFacturacion.getDatosFactura(parametros);
            if (datoFactura != null) {
                facturaCreate.setFecha(today.getTime());
                facturaCreate.setFechaVencimiento(today.getTime());

                facturaCreate.setNroAutofactura(datoFactura.getNroAutofactura());

                //Validaciones
                if (datoFactura.getTelefono() != null) {
                    facturaCreate.setTelefono(datoFactura.getTelefono());
                }
                if (datoFactura.getEmail() != null) {
                    facturaCreate.setEmail(datoFactura.getEmail());
                }
                if (datoFactura.getDireccion() != null) {
                    facturaCreate.setDireccion(datoFactura.getDireccion());
                }
                if (datoFactura.getIdDepartamento() != null) {
                    facturaCreate.setIdDepartamento(datoFactura.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoFactura.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoFactura.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datoFactura.getIdDistrito() != null) {
                    facturaCreate.setIdDistrito(datoFactura.getIdDistrito());
                    direccionFilter.setIdDistrito(datoFactura.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoFactura.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datoFactura.getIdCiudad() != null) {
                    facturaCreate.setIdCiudad(datoFactura.getIdCiudad());
                    direccionFilter.setIdCiudad(datoFactura.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoFactura.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datoFactura.getNroCasa() != null) {
                    facturaCreate.setNroCasa(datoFactura.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
                facturaCreate.setFecha(today.getTime());
                facturaCreate.setFechaVencimiento(today.getTime());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener datos de talonario
     */
    public void obtenerDatosTalonario() {
        facturaTalonario.setDigital(digital);
        facturaTalonario.setIdUsuario(session.getUser().getIdPersonaFisica());
        adapterFacturaTalonario = adapterFacturaTalonario.fillData(facturaTalonario);

        if (adapterFacturaTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            PF.current().ajax().update(":list-factura-create-form:form-data:pnl-pop-up");
        } else {
            if (!adapterFacturaTalonario.getData().isEmpty()) {
                talonario.setId(adapterFacturaTalonario.getData().get(0).getId());
            }
            obtenerDatosFacturacion();
        }
    }

    /**
     * Método para filtrar los datos factura
     */
    public void filterDatosFactura() {
        Map parametros = new HashMap();
        DatosFactura df = new DatosFactura();

        df = serviceFacturacion.getDatosFactura(parametros);

        facturaCreate.setDiasCredito(df.getDiasCredito());
        facturaCreate.setFechaVencimiento(df.getFechaVencimiento());
    }

    /**
     * Método para crear la factura
     */
    public void crearFactura() {
        boolean saveFactura = true;

        if (facturaCreate.getDireccion() == null || facturaCreate.getDireccion().trim().isEmpty()) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
        }

        if (facturaCreate.getNroCasa() == null || facturaCreate.getNroCasa().trim().isEmpty()) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
        }

        if (idDepartamento == null) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
        } else {
            facturaCreate.setIdDepartamento(idDepartamento);
        }

        if (idDistrito == null) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
        } else {
            facturaCreate.setIdDistrito(idDistrito);
        }

        if (idCiudad == null) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
        } else {
            facturaCreate.setIdCiudad(idCiudad);
        }

        if (listaDetalle.isEmpty() || listaDetalle == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe agregar al menos un detalle de factura!"));
            saveFactura = false;
        } else {
            for (int i = 0; i < listaDetalle.size(); i++) {
                int dn = i + 1;

                if (listaDetalle.get(i).getPrecioUnitario() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el precio unitario en el detalle N° " + dn));
                    saveFactura = false;
                }

                if (listaDetalle.get(i).getCantidad() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la cantidad en el detalle N° " + dn));
                    saveFactura = false;
                }
                
            }
        }

        if (saveFactura == true) {
            calcularTotalesGenerales();
            String str = facturaCreate.getNroDocumentoVendedor().trim().replace(" ","").replace(".", "");
            facturaCreate.setNroDocumentoVendedor(str);
            facturaCreate.setIdTalonario(datoFactura.getIdTalonario());

            facturaCreate.setAnulado("N");
            facturaCreate.setCobrado("N");
            facturaCreate.setImpreso("N");
            facturaCreate.setEntregado("N");
            facturaCreate.setDigital(digital);
            if (digital.equals("S")) {
                facturaCreate.setArchivoSet("S");
            } else {
                facturaCreate.setArchivoSet("N");
            }

            for (int i = 0; i < listaDetalle.size(); i++) {
                listaDetalle.get(i).setNroLinea(i + 1);
            }
            facturaCreate.setFacturaDetalles(listaDetalle);
            facturaCreate.setRazonSocial(razonSocialEmpresa);
            facturaCreate.setRuc(ruc);
            WebLogger.get().debug("SE ESTA POR GENERAR LA FACTURA");
            generarFactura();
            

        }

    }

    public void generarFactura() {
        WebLogger.get().debug("GENERANDO FACTURA");
        BodyResponse<AutoFactura> respuestaDeFactura = autoFacturaService.createFactura(facturaCreate);

        if (respuestaDeFactura.getSuccess()) {

            AutoFactura f = new AutoFactura();
            f = (AutoFactura) respuestaDeFactura.getPayload();
            idFacturaCreada = f.getId();
            nroFacturaCreada = f.getNroAutofactura();

            if (obtenerConfiguracionGeneral("DESCARGAR_FACTURA").equalsIgnoreCase("S")) {
                mostrarPanelDescarga();
            }
            /*     
            if (notificar.equalsIgnoreCase("S")) {
                this.notificado = false;
                notificar(facturaCreate.getEmail());
                if (notificado){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado la factura a la dirección confirmada"));
                }
                else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar la notificación. Verifique la configuración."));
                }
            }*/
            try {
                GeneracionDteAutofacturaMultiempresa.generar(session.getUser().getIdEmpresa(), idFacturaCreada);
            } catch (Exception e) {
                WebLogger.get().fatal(e);
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura N° " + nroFacturaCreada + " creada correctamente!"));
            clearForm();
        }
    }

    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroFacturaCreada + ".pdf";
        String url = "autofactura/consulta/" + idFacturaCreada;
        Map param = new HashMap();
        if (this.digital.equalsIgnoreCase("S")){
             param.put("ticket", false);
             if (siediApi.equalsIgnoreCase("S")) {
                param.put("siediApi", true);
             } else {
                param.put("siediApi", false);
             }
        }
        byte[] data = fileServiceClient.downloadFactura(url,param);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "autofactura/consulta/" + idFacturaCreada;
        byte[] data = fileServiceClient.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try ( FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("hideStatus();printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }
    
    public Integer obtenerEstadoCliente() {
        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoCliente = new Estado();
        estadoCliente.setCodigo("ACTIVO");
        estadoCliente.setCodigoTipoEstado("CLIENTE");
        adapterEstado = adapterEstado.fillData(estadoCliente);

        return adapterEstado.getData().get(0).getId();
    }

    public void obtenerConfiguracionGravada() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("CLIENTE_GRAVADO");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.gravada = "N";
        } else {
            this.gravada = adaptercv.getData().get(0).getValor();
        }

    }

    public void obtenerConfiguracionNroCtaContable() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("PRODUCTO_NRO_CTA_CONTABLE");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ctaCtble = "N";
        } else {
            this.ctaCtble = adaptercv.getData().get(0).getValor();
        }

    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionNumeracionFactura() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("NUMERACION_DINAMICA_FACTURA");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.numeracionDinamica = true;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.numeracionDinamica = true;
        } else {
            this.numeracionDinamica = false;
        }

        facturaCreate.setAsignarNroFactura(numeracionDinamica);

    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        this.codMoneda = null;
        this.facturaCreate = new AutoFactura();

        this.cliente = new ClientePojo();
        this.cliente.setListadoPojo(true);
        this.adapter = new ClientPojoAdapter();
        filterCliente();
        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();

        this.adapterFactura = new FacturaAdapter();

        this.ruc = "--";
        this.adapterFacturaCancelada = new FacturaAdapter();

        this.serviceFacturacion = new AutoFacturaService();
        this.adapterProducto = new ProductoAdapter();

        this.direccion = "--";
        this.telefono = "--";

        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        this.nroOC="";
        this.tieneOC="N";
        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();
        this.nroLineaProducto = null;
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.idProducto = null;
        this.descripcion = null;
        this.porcentaje = null;
        this.parametroProductoPrecio = new ProductoPrecio();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.facturaCreate.setIdTipoFactura(2);
        this.ctaCtble = null;
        this.nroCtaCtble = null;
        this.showTalonarioPopup = false;
        obtenerConfiguracion();
        obtenerDatosFacturacion();
        obtenerConfiguracionGravada();
        obtenerConfiguracionNroCtaContable();
        obtenerConfiguracionNumeracionFactura();

        this.licitacion="N";
        this.facturaDncp = new FacturaDncp();
        PF.current().ajax().update(":list-factura-create-form:form-data:localDestino");

    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {

        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setCodigoMoneda(obtenerCodMoneda());
        BigDecimal scaled = facturaCreate.getMontoTotalFactura().setScale(0, RoundingMode.HALF_UP);

        montoLetrasFilter.setMonto(scaled);

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
        facturaCreate.setTotalLetras(montoLetrasFilter.getTotalLetras());
    }

    /**
     * Método para mostrar Datatable de Detalle al crear
     */
    public void showDetalle() {
        showDetalleCreate = true;

        addDetalle();
    }

    /**
     * Método para agregar detalle
     */
    public void addDetalle() {
        try {
            AutoFacturaDetalle detalle = new AutoFacturaDetalle();
            Producto productoDetalle = new Producto();

            if (listaDetalle.isEmpty()) {
                detalle.setNroLinea(linea);
            } else {
                linea++;
                detalle.setNroLinea(linea);
            }

            productoDetalle.setDescripcion("--");
            detalle.setDescripcion("--");
            detalle.setCantidad(BigDecimal.valueOf(1));
            detalle.setMontoImponible(BigDecimal.valueOf(0));
            detalle.setMontoTotal(BigDecimal.valueOf(0));
            detalle.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
            detalle.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoParticular(BigDecimal.valueOf(0));
            
            listaDetalle.add(detalle);
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        facturaCreate.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        facturaCreate.setMontoTotalDescuentoParticular(new BigDecimal("0"));
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos1(Integer nroLinea) {
        AutoFacturaDetalle info = null;

        for (AutoFacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitario= info.getPrecioUnitario();
        BigDecimal montoPrecioXCantidad = precioUnitario.multiply(cantidad);
        
        BigDecimal resultCero = new BigDecimal("0");
        info.setMontoIva(resultCero);
        info.setMontoIva(BigDecimal.ZERO);
        info.setPrecioUnitario(precioUnitario);
        info.setMontoTotal(montoPrecioXCantidad);
        

        calcularTotal();
        
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos(Integer nroLinea) {
        AutoFacturaDetalle info = null;
        for (AutoFacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        // Se calcula el monto total del detalle
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitario();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        info.setMontoTotal(montoPrecioXCantidad);
        calcularTotal();

    }

    /**
     * Método para calcular el total de la factura
     */
    public void calcularTotal() {
        BigDecimal totalFactura = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            BigDecimal montoTotal = listaDetalle.get(i).getMontoTotal();
            totalFactura = totalFactura.add(montoTotal);
        }
        if (facturaCreate.getIdTipoCambio() != null) {
            facturaCreate.setMontoTotalFactura(totalFactura);
            TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
            TipoCambio tipoCambioFilter = new TipoCambio();
            tipoCambioFilter.setId(facturaCreate.getIdTipoCambio());
            adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);
            BigDecimal totalGs = totalFactura.multiply(adapterTipoCambio.getData().get(0).getVenta());
            facturaCreate.setMontoTotalGuaranies(totalGs);
            obtenerMontoTotalLetras();
        } else {
            facturaCreate.setMontoTotalFactura(totalFactura);
            facturaCreate.setMontoTotalGuaranies(totalFactura);
            obtenerMontoTotalLetras();
        }

    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param nroLinea
     */
    public void deleteDetalle(Integer nroLinea) {
        AutoFacturaDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (AutoFacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        if (facturaCreate.getMontoTotalFactura() != null) {
            BigDecimal monto = facturaCreate.getMontoTotalFactura();
            BigDecimal totalFactura = monto.subtract(subTotal);
            facturaCreate.setMontoTotalFactura(totalFactura);
            obtenerMontoTotalLetras();
        }

    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            facturaCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        } else {
            calcularFechaVto();
        }

    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelectVto(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) > 0) {
            facturaCreate.setFechaVencimiento(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha anterior!"));
        } else {
            Calendar day1 = Calendar.getInstance();
            Calendar day2 = Calendar.getInstance();
            day1.setTime(today.getTime());
            day2.setTime(facturaCreate.getFechaVencimiento());

            int daysBetween = day2.get(Calendar.DAY_OF_YEAR) - day1.get(Calendar.DAY_OF_YEAR);

            facturaCreate.setDiasCredito(daysBetween);
        }

    }

    /**
     * Método para filtrar el producto
     */
    public void filterProducto() {
        producto = new Producto();
        adapterProducto = adapterProducto.fillData(producto);
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        cliente.setListadoPojo(true);
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Método para obtener Moneda
     */
    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVto() {
        if (facturaCreate.getIdTipoFactura() == 2) {
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(facturaCreate.getFecha());
            cal.add(Calendar.DAY_OF_MONTH, facturaCreate.getDiasCredito());
            Date fechaVencimiento = cal.getTime();

            facturaCreate.setFechaVencimiento(fechaVencimiento);
        }

    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVtoContado() {
        if (facturaCreate.getIdTipoFactura() == 2) {
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
            facturaCreate.setDiasCredito(null);
        }

    }

    /**
     * Método que se dispara en el evento de cambio de moneda
     */
    public void onChangeMoneda() {
        Calendar now = Calendar.getInstance();
        Date date = now.getTime();

        TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
        TipoCambio tipoCambioFilter = new TipoCambio();

        tipoCambioFilter.setIdMoneda(facturaCreate.getIdMoneda());
        tipoCambioFilter.setActivo("S");
        tipoCambioFilter.setFechaInsercion(date);

        adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);

        codMoneda = obtenerCodMoneda();

        if (adapterTipoCambio.getData().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No existe un registro de cotización actual para la moneda seleccionada!"));
            habilitarPanelCotizacion = true;
        } else {
            facturaCreate.setIdTipoCambio(adapterTipoCambio.getData().get(0).getId());
            if (!listaDetalle.isEmpty()) {
                if (codMoneda.equals("PYG")) {
                    for (AutoFacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioVenta(facturaDetalle, adapterTipoCambio.getData().get(0).getVenta());
                    }
                } else {
                    for (AutoFacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioCompra(facturaDetalle, adapterTipoCambio.getData().get(0).getCompra());
                    }
                }
            }
        }
    }

    public void crearTipoCambio() {
        tipoCambio.setActivo("S");
        tipoCambio.setIdMoneda(facturaCreate.getIdMoneda());
        BodyResponse<TipoCambio> respuestaDeTipoCambio = serviceTipoCambio.setTipoCambios(tipoCambio);

        if (respuestaDeTipoCambio.getSuccess()) {
            habilitarPanelCotizacion = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Tipo de cambio creado correctamente!"));
            onChangeMoneda();
        } else {
            for (Mensaje mensaje : respuestaDeTipoCambio.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }

        }
    }

    public String obtenerCodMoneda() {
        Moneda mf = new Moneda();
        MonedaAdapter am = new MonedaAdapter();
        mf.setId(facturaCreate.getIdMoneda());
        am = am.fillData(mf);

        return am.getData().get(0).getCodigo();
    }

    /**
     * Método para calcular el precio unitario con gs de referencia
     *
     * @param facturaDetalle
     * @param precioCompraMoneda
     */
    public void calcularCambioCompra(AutoFacturaDetalle facturaDetalle, BigDecimal precioCompraMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitario();
        BigDecimal precioUnitarioCambio = precioUnitario.divide(precioCompraMoneda, 5, RoundingMode.HALF_UP);
        facturaDetalle.setPrecioUnitario(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para calcular el precio unitario a gs
     *
     * @param facturaDetalle
     * @param precioVentaMoneda
     */
    public void calcularCambioVenta(AutoFacturaDetalle facturaDetalle, BigDecimal precioVentaMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitario();
        BigDecimal precioUnitarioCambio = precioUnitario.multiply(precioVentaMoneda);
        facturaDetalle.setPrecioUnitario(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

//    /**
//     * Método autocomplete Productos
//     *
//     * @param query
//     * @return
//     */
//    public List<Producto> completeQueryProducto(String query) {
//
//        producto = new Producto();
//        producto.setDescripcion(query);
//        producto.setActivo("S");
//        adapterProducto = adapterProducto.fillData(producto);
//        producto = new Producto();
//
//        return adapterProducto.getData();
//    }

    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea;
    }

//    /**
//     * Método para seleccionar el producto
//     */
//    public void onItemSelectProducto(SelectEvent event) {
//        try {
//            idProducto = ((Producto) event.getObject()).getId();
//            descripcion = ((Producto) event.getObject()).getDescripcion();
//            porcentaje = ((Producto) event.getObject()).getPorcentajeImpuesto();
//            if (((Producto) event.getObject()).getUmv() != null) {
//                umv = ((Producto) event.getObject()).getUmv();
//            } else {
//                umv = BigDecimal.valueOf(1);
//            }
//
//            if (((Producto) event.getObject()).getCuentaContable() != null) {
//                nroCtaCtble = ((Producto) event.getObject()).getCuentaContable();
//            }
//
//        } catch (Exception e) {
//            WebLogger.get().fatal(e);
//        }
//
//    }

    /**
     * Método para agregar el producto con validaciones
     */
    public void agregarProducto() {
        Calendar today = Calendar.getInstance();
        boolean save = true;
        AutoFacturaDetalle info = null;
        ProductoService serviceProductoPrecio = new ProductoService();

        for (AutoFacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (AutoFacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), idProducto)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }

        if (save == true) {

            parametroProductoPrecio.setIdProducto(idProducto);
            parametroProductoPrecio.setIdMoneda(facturaCreate.getIdMoneda());
            parametroProductoPrecio.setFecha(today.getTime());

            parametroProductoPrecio.setIdCanalVenta(0);
            parametroProductoPrecio.setIdCliente(0);
            

            ProductoPrecio prodRespuesta = new ProductoPrecio();
            WebLogger.get().debug("PRIMERA");
            prodRespuesta = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);

            if (prodRespuesta == null) {
                parametroProductoPrecio.setIdCliente(0);
                parametroProductoPrecio.setIdCanalVenta(0);
                ProductoPrecio prodRespuesta2 = new ProductoPrecio();
                WebLogger.get().debug("SEGUNDA");
                prodRespuesta2 = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);
                if (prodRespuesta2 == null) {
                    info.setDescripcion(descripcion);
                    info.setPorcentajeIva(porcentaje);
                    info.setCantidad(umv);
                    info.setNroCtaContable(nroCtaCtble);
                    info.setIdProducto(idProducto);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El producto seleccionado no tiene un precio general asociado activo"));
                } else {
                    info.setDescripcion(prodRespuesta2.getProducto().getDescripcion());
                    info.setPrecioUnitario(prodRespuesta2.getPrecio());
                    info.setIdProducto(idProducto);
                    info.setPorcentajeIva(porcentaje);
                    info.setCantidad(umv);
                    info.setNroCtaContable(nroCtaCtble);
                    calcularMontos(nroLineaProducto);
                }

            } else {
                info.setDescripcion(descripcion);
                info.setPorcentajeIva(porcentaje);
                info.setCantidad(umv);
                info.setIdProducto(idProducto);
                info.setNroCtaContable(nroCtaCtble);
                info.setPrecioUnitario(prodRespuesta.getPrecio());
                calcularMontos(nroLineaProducto);
            }

        }
    }
    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-factura-create-form:form-data:distrito");
        PF.current().ajax().update(":list-factura-create-form:form-data:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-factura-create-form:form-data:ciudad");

        return datos;

    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryOrigen(String query) {
        Local localOrigen = new Local();
        LocalListAdapter localOrigenAdapter = new LocalListAdapter();
        localOrigen.setLocalExterno("N");
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }

    public void filterDepartamento() {
        WebLogger.get().debug("FILTRO DEPARTAMENTO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        WebLogger.get().debug("FILTRO DISTRITO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        WebLogger.get().debug("FILTRO CIUDAD");
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }
    
       /**
     * Método para obtener la configuración de Notificación por email
     */
    public void obtenerConfiguracionNotificacion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("NOTIFICACION_FACTURA_EMAIL");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.notificar = "N";
            } else {
                this.notificar = adapterConfigValor.getData().get(0).getValor();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
    
    public void notificar(String emailDestino) {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String cliente = this.cliente.getRazonSocial();
        String empresa = session.getNombreEmpresa();
        String mailTo = emailDestino;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su recibo en formato PDF";
        
        String subjet = "Autofactura No. " + nroFacturaCreada;
        String url = "autofactura/consulta/" + idFacturaCreada;
        byte[] data = fileServiceClient.download(url);
        String fileName = nroFacturaCreada + ".pdf";


        notificarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, nroFacturaCreada, host, port, empresa);

        
    }
    
     public void notificarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa) {
        try {
            String mensaje = MailUtils.getMailHtml(nroDoc, cliente,empresa);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host,port);  
            this.notificado = true;
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        /**
         * DATOS REALES
         */
        this.codMoneda = null;
        this.facturaCreate = new AutoFactura();

        this.cliente = new ClientePojo();
        this.cliente.setListadoPojo(true);
        this.adapter = new ClientPojoAdapter();
        this.digital="S";
        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();

        this.adapterFactura = new FacturaAdapter();

        this.adapterFacturaCancelada = new FacturaAdapter();

        this.serviceFacturacion = new AutoFacturaService();
        this.adapterProducto = new ProductoAdapter();

        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();

        this.ruc = "--";
        this.direccion = "--";
        this.telefono = "--";

        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();
        this.autoFacturaService = new AutoFacturaService();
        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();
        this.nroLineaProducto = null;
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.idProducto = null;
        this.descripcion = null;
        this.porcentaje = null;
        this.parametroProductoPrecio = new ProductoPrecio();
        this.direccionFilter = new Direccion();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.facturaCreate.setIdTipoFactura(2);
        this.showTalonarioPopup = false;
        this.adapterFacturaTalonario = new AutoFacturaTalonarioAdapter();
        this.facturaTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();
        this.showDescargaPopup = false;
        this.fileServiceClient = new FileServiceClient();
        this.idFacturaCreada = null;
        this.nroFacturaCreada = null;
        this.gravada = null;
        this.ctaCtble = null;
        this.nroCtaCtble = null;
        
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;
        
        this.licitacion ="N";
        this.facturaDncp = new FacturaDncp();
        this.tieneOC = "N";
        this.nroOC="";
        this.notificar = "N";
        this.notificado = false;
        obtenerConfiguracionNotificacion();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        obtenerConfiguracionNumeracionFactura();
        obtenerConfiguracion();
        obtenerConfiguracionGravada();
        obtenerConfiguracionNroCtaContable();

        filterDepartamento();
        filterDistrito();
        filterCiudad();
        calcularFechaVtoContado();
        
        this.siediApi = obtenerConfiguracionGeneral("ARCHIVOS_DTE_SIEDI_API");
        
    }

}
