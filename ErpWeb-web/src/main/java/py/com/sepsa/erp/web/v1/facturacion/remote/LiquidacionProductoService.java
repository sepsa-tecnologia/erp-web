/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LiquidacionProductoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.LiquidacionProductoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.LiquidacionProducto;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de liquidacion Producto
 *
 * @author Sergio D. Riveros Vazquez
 */
public class LiquidacionProductoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de liquidación producto
     *
     * @param liquidacionProducto Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public LiquidacionProductoAdapter getLiquidacionProductoList(LiquidacionProducto liquidacionProducto, Integer page,
            Integer pageSize) {

        LiquidacionProductoAdapter lista = new LiquidacionProductoAdapter();

        Map params = LiquidacionProductoFilter.build(liquidacionProducto, page, pageSize);

        HttpURLConnection conn = GET(Resource.LIQUIDACION_PRODUCTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LiquidacionProductoAdapter.class);

            lista = (LiquidacionProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear/editar LiquidacionProducto
     *
     * @param liquidacionProducto
     * @return
     */
    public Integer setLiquidacionProducto(LiquidacionProducto liquidacionProducto) {
        Integer idLiquidacionProducto = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.LIQUIDACION_PRODUCTO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(liquidacionProducto));

            response = BodyResponse.createInstance(conn, LiquidacionProducto.class);

            idLiquidacionProducto = ((LiquidacionProducto) response.getPayload()).getId();

        }
        return idLiquidacionProducto;
    }
    
    /**
     *
     * @param id
     * @return
     */
    public LiquidacionProducto get(Integer id) {
        LiquidacionProducto data = new LiquidacionProducto(id);
        LiquidacionProductoAdapter list = getLiquidacionProductoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public LiquidacionProductoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        LIQUIDACION_PRODUCTO("LiquidacionProducto", "liquidacion-producto");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
