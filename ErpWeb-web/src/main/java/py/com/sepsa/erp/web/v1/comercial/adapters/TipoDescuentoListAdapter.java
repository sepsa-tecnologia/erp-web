
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoDescuento;
import py.com.sepsa.erp.web.v1.comercial.remote.DescuentoService;

/**
 * Adaptador de la lista de tipo descuento
 * @author Cristina Insfrán
 */
public class TipoDescuentoListAdapter extends DataListAdapter<TipoDescuento>{
  
     /**
     * Cliente para los servicios de descuento
     */
    private final DescuentoService descuentoClient;

    /**
     * Constructor de TipoDatoPersonaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoDescuentoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.descuentoClient = new DescuentoService();
    }

    /**
     * Constructor de TipoDescuentoListAdapter
     */
    public TipoDescuentoListAdapter() {
        super();
        this.descuentoClient = new DescuentoService();
    }

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoDescuentoListAdapter fillData(TipoDescuento searchData) {
        return descuentoClient.getTipoDescuentoList(searchData, getFirstResult(),getPageSize());
    }
}
