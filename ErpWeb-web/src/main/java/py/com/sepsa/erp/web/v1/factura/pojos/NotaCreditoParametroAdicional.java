/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 *
 * @author Williams Vera
 */
public class NotaCreditoParametroAdicional { 
    
    private Integer id;
    
    private Integer idEmpresa;
    
    private Integer idNotaCredito;
   
    private Integer idParametroAdicional;
    
    private String valor;
   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public Integer getIdParametroAdicional() {
        return idParametroAdicional;
    }

    public void setIdParametroAdicional(Integer idParametroAdicional) {
        this.idParametroAdicional = idParametroAdicional;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
