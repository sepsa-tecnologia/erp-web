/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.LiquidacionProducto;
import py.com.sepsa.erp.web.v1.facturacion.remote.LiquidacionProductoService;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
public class LiquidacionProductoAdapter extends DataListAdapter<LiquidacionProducto> {

    /**
     * Cliente para el servicio de LiquidacionProducto
     */
    private final LiquidacionProductoService serviceLiquidacionProducto;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public LiquidacionProductoAdapter fillData(LiquidacionProducto searchData) {

        return serviceLiquidacionProducto.getLiquidacionProductoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de LiquidacionProductoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public LiquidacionProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceLiquidacionProducto = new LiquidacionProductoService();
    }

    /**
     * Constructor de LiquidacionProductoAdapter
     */
    public LiquidacionProductoAdapter() {
        super();
        this.serviceLiquidacionProducto = new LiquidacionProductoService();
    }

}
