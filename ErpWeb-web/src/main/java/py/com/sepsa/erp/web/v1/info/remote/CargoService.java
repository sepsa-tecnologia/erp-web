package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.CargoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.CargoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Cargo;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio cargo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CargoService extends APIErpCore {

    /**
     * Obtiene la lista de cargos
     *
     * @param dato
     * @param page
     * @param pageSize
     * @return
     */
    public CargoListAdapter getCargoList(Cargo dato, Integer page,
            Integer pageSize) {

        CargoListAdapter lista = new CargoListAdapter();

        Map params = CargoFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.CARGO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CargoListAdapter.class);

            lista = (CargoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de CargoServiceClient
     */
    public CargoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CARGO("Servicio para cargo", "cargo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
