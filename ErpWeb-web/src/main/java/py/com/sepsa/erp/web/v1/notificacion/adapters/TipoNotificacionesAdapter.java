/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.notificacion.remote.TipoNotificacionesService;

/**
 * Adapter de tipo notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoNotificacionesAdapter extends DataListAdapter<TipoNotificaciones> {

    /**
     * Cliente para los servicios de tipo Notificacion
     */
    private final TipoNotificacionesService tipoNotificacionClient;

    @Override
    public TipoNotificacionesAdapter fillData(TipoNotificaciones searchData) {
        return tipoNotificacionClient.getTipoNotificacionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de tipo Notificacion
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoNotificacionesAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoNotificacionClient = new TipoNotificacionesService();
    }

    /**
     * Constructor de Tipo Notificacion Adapter
     */
    public TipoNotificacionesAdapter() {
        super();
        this.tipoNotificacionClient = new TipoNotificacionesService();
    }
}
