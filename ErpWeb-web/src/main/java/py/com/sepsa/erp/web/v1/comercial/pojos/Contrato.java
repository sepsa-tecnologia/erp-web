package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * POJO de Contrato
 *
 * @author Romina E. Núñez Rojas
 */
public class Contrato {

    /**
     * Identificador del contrato
     */
    private Integer id;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
    /**
     * Identificador del concepto factura
     */
    private Integer idConceptoFactura;
    /**
     * Concepto factura
     */
    private String conceptoFactura;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Razón Social
     */
    private String razonSocial;
    /**
     * Monto mínimo
     */
    private String montoMinimo;
    /**
     * Número de contrato
     */
    private String nroContrato;
    /**
     * Fecha Inicio
     */
    private Date fechaInicio;
    /**
     * Fecha fin
     */
    private Date fechaFin;
    /**
     * Firmado
     */
    private String firmado;
    /**
     * Documento Aval
     */
    private String documentoAval;
    /**
     * Renovación Automática
     */
    private String renovacionAutomatica;
    /**
     * Débito Automático
     */
    private String debitoAutomatico;
    /**
     * Observación
     */
    private String observacion;
    /**
     * Estado
     */
    private String estado;
    /**
     * Cobro Adelantado
     */
    private String cobroAdelantado;
    /**
     * Fecha Vencimiento Acuerdo
     */
    private String fechaVencimientoAcuerdo;
    /**
     * Identificador del tipo de tarifa
     */
    private Integer idTipoTarifa;
    /**
     * Plazo Rescision
     */
    private String plazoRescision;
    /**
     * Tipo Tarifa
     */
    private String tipoTarifa;
    /**
     * Identificador de comercial
     */
    private Integer idComercial;
    /**
     * Comercial
     */
    private String comercial;
    /**
     * Listado de servicio
     */
    private List<ContratoServicio> servicios = new ArrayList<>();

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setPlazoRescision(String plazoRescision) {
        this.plazoRescision = plazoRescision;
    }

    public String getPlazoRescision() {
        return plazoRescision;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdConceptoFactura() {
        return idConceptoFactura;
    }

    public void setIdConceptoFactura(Integer idConceptoFactura) {
        this.idConceptoFactura = idConceptoFactura;
    }

    public String getConceptoFactura() {
        return conceptoFactura;
    }

    public void setConceptoFactura(String conceptoFactura) {
        this.conceptoFactura = conceptoFactura;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(String montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public String getFirmado() {
        return firmado;
    }

    public void setFirmado(String firmado) {
        this.firmado = firmado;
    }

    public String getDocumentoAval() {
        return documentoAval;
    }

    public void setDocumentoAval(String documentoAval) {
        this.documentoAval = documentoAval;
    }

    public String getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(String renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public String getDebitoAutomatico() {
        return debitoAutomatico;
    }

    public void setDebitoAutomatico(String debitoAutomatico) {
        this.debitoAutomatico = debitoAutomatico;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(String cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public String getFechaVencimientoAcuerdo() {
        return fechaVencimientoAcuerdo;
    }

    public void setFechaVencimientoAcuerdo(String fechaVencimientoAcuerdo) {
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }

    public Integer getIdTipoTarifa() {
        return idTipoTarifa;
    }

    public void setIdTipoTarifa(Integer idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public List<ContratoServicio> getServicios() {
        return servicios;
    }

    public void setServicios(List<ContratoServicio> servicios) {
        this.servicios = servicios;
    }
//</editor-fold>

    /**
     * Contructor de la clase
     */
    public Contrato() {

    }

    /**
     * Constructor con parámetros
     *
     * @param id
     * @param idProducto
     * @param producto
     * @param idConceptoFactura
     * @param conceptoFactura
     * @param idCliente
     * @param razonSocial
     * @param montoMinimo
     * @param nroContrato
     * @param fechaInicio
     * @param fechaFin
     * @param firmado
     * @param documentoAval
     * @param renovacionAutomatica
     * @param debitoAutomatico
     * @param observacion
     * @param estado
     * @param cobroAdelantado
     * @param fechaVencimientoAcuerdo
     * @param idTipoTarifa
     * @param tipoTarifa
     * @param servicios
     * @param idComercial
     * @param comercial
     */
    public Contrato(Integer id, Integer idProducto, String producto, Integer idConceptoFactura, String conceptoFactura, Integer idCliente, String razonSocial,
            String montoMinimo, String nroContrato, Date fechaInicio, Date fechaFin, String firmado, String documentoAval, String renovacionAutomatica,
            String debitoAutomatico, String plazoRescision, String observacion, String estado, String cobroAdelantado, String fechaVencimientoAcuerdo,
            Integer idTipoTarifa, String tipoTarifa, Integer idComercial, String comercial, List<ContratoServicio> servicios) {
        this.id = id;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idConceptoFactura = idConceptoFactura;
        this.conceptoFactura = conceptoFactura;
        this.idCliente = idCliente;
        this.razonSocial = razonSocial;
        this.montoMinimo = montoMinimo;
        this.nroContrato = nroContrato;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.firmado = firmado;
        this.documentoAval = documentoAval;
        this.renovacionAutomatica = renovacionAutomatica;
        this.debitoAutomatico = debitoAutomatico;
        this.plazoRescision = plazoRescision;
        this.observacion = observacion;
        this.estado = estado;
        this.cobroAdelantado = cobroAdelantado;
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
        this.idTipoTarifa = idTipoTarifa;
        this.tipoTarifa = tipoTarifa;
        this.idComercial = idComercial;
        this.comercial = comercial;
        this.servicios = servicios;
    }

}
