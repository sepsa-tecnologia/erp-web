package py.com.sepsa.erp.web.v1.system.pojos;

import java.util.List;

/**
 * POJO para control de perfiles
 *
 * @author Daniel F. Escauriza Arza
 */
public class Profiles {

    /**
     * ROLE_ADMIN Bandera
     */
    private boolean roleAdmin;
    /**
     * ROLE_FACT_OPER Bandera
     */
    private boolean roleOpFact;
    /**
     * ROLE_FACT_ADM Bandera
     */
    private boolean roleAdmFact;
    /**
     * ROLE_PREP_LOGISTICO Bandera
     */
    private boolean rolePrepLogistico;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public boolean isRoleOpFact() {
        return roleOpFact;
    }

    public void setRolePrepLogistico(boolean rolePrepLogistico) {
        this.rolePrepLogistico = rolePrepLogistico;
    }

    public boolean isRolePrepLogistico() {
        return rolePrepLogistico;
    }

    public boolean isRoleAdmin() {
        return roleAdmin;
    }

    public boolean isRoleAdmFact() {
        return roleAdmFact;
    }

    public void setRoleOpFact(boolean roleOpFact) {
        this.roleOpFact = roleOpFact;
    }

    public void setRoleAdmin(boolean roleAdmin) {
        this.roleAdmin = roleAdmin;
    }

    public void setRoleAdmFact(boolean roleAdmFact) {
        this.roleAdmFact = roleAdmFact;
    }

//</editor-fold>
    
    /**
     * Constructor de Profiles
     */
    public Profiles() {
    }

    /**
     * Constructor de Profiles
     */
    public Profiles(List<String> perfiles) {
        this.roleAdmin = perfiles.contains("ROLE_ADMIN");
        this.roleOpFact = perfiles.contains("ROLE_FACT_OPER");
        this.roleAdmFact = perfiles.contains("ROLE_FACT_ADM");
        this.rolePrepLogistico = perfiles.contains("ROLE_PREP_LOGISTICO");
    }

}
