/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoMotivoAnulacionService;

/**
 * Adapter para Tipo motivo anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoMotivoAnulacionAdapter extends DataListAdapter<TipoMotivoAnulacion> {

    /**
     * Cliente para el servicio de tipo motivo anulacion
     */
    private final TipoMotivoAnulacionService serviceTipoMotivoAnulacion;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoMotivoAnulacionAdapter fillData(TipoMotivoAnulacion searchData) {

        return serviceTipoMotivoAnulacion.getTipoMotivoAnulacionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoMotivoAnulacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoMotivoAnulacion = new TipoMotivoAnulacionService();
    }

    /**
     * Constructor
     */
    public TipoMotivoAnulacionAdapter() {
        super();
        this.serviceTipoMotivoAnulacion = new TipoMotivoAnulacionService();
    }
}
