package py.com.sepsa.erp.web.v1.usuario.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.remote.PerfilServiceClient;

/**
 * Controlador para perfil
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("crearPerfil")
public class PerfilCrearController implements Serializable {

    /**
     * Entidad perfil
     */
    private Perfil perfil;
    /**
     * Servicio Perfil
     */
    private PerfilServiceClient servicePerfil;
    /**
     * Pojo para empresa
     */
    private Empresa empresaComplete;
    /**
     * Adapter para empresa
     */
    private EmpresaAdapter adapterEmpresa;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public void setServicePerfil(PerfilServiceClient servicePerfil) {
        this.servicePerfil = servicePerfil;
    }

    public PerfilServiceClient getServicePerfil() {
        return servicePerfil;
    }

    /**
     * @return the perfil
     */
    public Perfil getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Empresa getEmpresaComplete() {
        return empresaComplete;
    }

    public void setEmpresaComplete(Empresa empresaComplete) {
        this.empresaComplete = empresaComplete;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    //</editor-fold>

    /**
     * Método para limpiar los filtros
     */
    public void clear() {
        perfil = new Perfil();
        empresaComplete = new Empresa();
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa empresa = new Empresa();
        empresa.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(empresa);
        return adapterEmpresa.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        perfil.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    public void createPerfil() {
        
        perfil.setActivo("S");
        BodyResponse<Perfil> respuestaDePerfil = servicePerfil.createPerfil(perfil);

        if (respuestaDePerfil.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Perfil creado correctamente!"));
            clear();
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.perfil = new Perfil();
        this.servicePerfil = new PerfilServiceClient();
        this.empresaComplete = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
    }
}
