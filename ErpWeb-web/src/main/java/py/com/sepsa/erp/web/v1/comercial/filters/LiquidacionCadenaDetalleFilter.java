package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionCadenaDetalle;

/**
 * Filtro para detalle de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionCadenaDetalleFilter extends Filter {

    /**
     * Filtro por idCliente.
     *
     * @param idCliente
     * @return
     */
    public LiquidacionCadenaDetalleFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Filtro por cliente.
     *
     * @param cliente
     * @return
     */
    public LiquidacionCadenaDetalleFilter cliente(String cliente) {
        if (cliente != null && !cliente.trim().isEmpty()) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Filtro por idContrato.
     *
     * @param idContrato
     * @return
     */
    public LiquidacionCadenaDetalleFilter idContrato(Integer idContrato) {
        if (idContrato != null) {
            params.put("idContrato", idContrato);
        }
        return this;
    }

    /**
     * Filtro por idProducto.
     *
     * @param idProducto
     * @return
     */
    public LiquidacionCadenaDetalleFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Filtro por producto.
     *
     * @param producto
     * @return
     */
    public LiquidacionCadenaDetalleFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Filtro por mes.
     *
     * @param mes
     * @return
     */
    public LiquidacionCadenaDetalleFilter mes(String mes) {
        if (mes != null && !mes.trim().isEmpty()) {
            params.put("mes", mes);
        }
        return this;
    }

    /**
     * Filtro por año.
     *
     * @param ano
     * @return
     */
    public LiquidacionCadenaDetalleFilter ano(String ano) {
        if (ano != null && !ano.trim().isEmpty()) {
            params.put("ano", ano);
        }
        return this;
    }

    public static Map build(LiquidacionCadenaDetalle liquidacionCadenaDetalle, Integer page, Integer pageSize) {
        LiquidacionCadenaDetalleFilter filter = new LiquidacionCadenaDetalleFilter();

        filter
                .idCliente(liquidacionCadenaDetalle.getIdCliente())
                .cliente(liquidacionCadenaDetalle.getCliente())
                .idContrato(liquidacionCadenaDetalle.getIdContrato())
                .idProducto(liquidacionCadenaDetalle.getIdProducto())
                .producto(liquidacionCadenaDetalle.getProducto())
                .mes(liquidacionCadenaDetalle.getMes())
                .ano(liquidacionCadenaDetalle.getAno())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de LiquidacionCadenaDetalleFilter.
     */
    public LiquidacionCadenaDetalleFilter() {
        super();
    }

}
