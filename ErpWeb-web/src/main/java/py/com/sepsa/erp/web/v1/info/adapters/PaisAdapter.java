package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Pais;
import py.com.sepsa.erp.web.v1.info.remote.PaisServiceClient;


/**
 * Adaptador de la lista de pais
 *
 * @author Williams Vera
 */
public class PaisAdapter extends DataListAdapter<Pais> {

    /**
     * Cliente para el servicio de Configuracion Valor
     */
    private final PaisServiceClient paisClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return PaisAdapter
     */
    @Override
    public PaisAdapter fillData(Pais searchData) {

        return paisClient.getPaisList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ConfiguracionValorListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PaisAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.paisClient = new PaisServiceClient();
    }

    /**
     * Constructor de PaisAdapter
     */
    public PaisAdapter() {
        super();
        this.paisClient = new PaisServiceClient();
    }

}
