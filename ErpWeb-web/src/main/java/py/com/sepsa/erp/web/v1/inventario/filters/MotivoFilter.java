/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;

/**
 * Filter para motivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public MotivoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public MotivoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro descripcion
     *
     * @param descripcion
     * @return
     */
    public MotivoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro activo
     *
     * @param activo
     * @return
     */
    public MotivoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoTipoMotivo
     *
     * @param codigoTipoMotivo
     * @return
     */
    public MotivoFilter codigoTipoMotivo(String codigoTipoMotivo) {
        if (codigoTipoMotivo != null && !codigoTipoMotivo.trim().isEmpty()) {
            params.put("codigoTipoMotivo", codigoTipoMotivo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param motivoFilter datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Motivo motivoFilter, Integer page, Integer pageSize) {
        MotivoFilter filter = new MotivoFilter();
        
        filter
                .id(motivoFilter.getId())
                .codigo(motivoFilter.getCodigo())
                .descripcion(motivoFilter.getDescripcion())
                .activo(motivoFilter.getActivo())
                .codigoTipoMotivo(motivoFilter.getCodigoTipoMotivo())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public MotivoFilter() {
        super();
    }
    
}
