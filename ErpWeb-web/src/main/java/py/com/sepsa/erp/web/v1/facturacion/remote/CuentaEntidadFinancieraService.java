package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CuentaEntidadFinancieraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCobroListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.CobroFilter;
import py.com.sepsa.erp.web.v1.factura.filters.CuentaEntidadFinancieraFilter;
import py.com.sepsa.erp.web.v1.factura.filters.TipoCobroFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CuentaEntidadFinanciera;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Cuenta Entidad Financiera
 *
 * @author Romina Núñez
 */
public class CuentaEntidadFinancieraService extends APIErpFacturacion {

    /**
     * Obtiene la lista de cobros
     *
     * @param cobro Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public CuentaEntidadFinancieraAdapter getCuentaEntidadFinancieraList(CuentaEntidadFinanciera cuentaEntidadFinanciera, Integer page,
            Integer pageSize) {

        CuentaEntidadFinancieraAdapter lista = new CuentaEntidadFinancieraAdapter();

        Map params = CuentaEntidadFinancieraFilter.build(cuentaEntidadFinanciera, page, pageSize);

        HttpURLConnection conn = GET(Resource.CUENTA_ENTIDAD_FINANCIERA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CuentaEntidadFinancieraAdapter.class);

            lista = (CuentaEntidadFinancieraAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    

    /**
     * Constructor de FacturacionService
     */
    public CuentaEntidadFinancieraService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        CUENTA_ENTIDAD_FINANCIERA("Cuenta Entidad Financiera", "cuenta-entidad-financiera");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
