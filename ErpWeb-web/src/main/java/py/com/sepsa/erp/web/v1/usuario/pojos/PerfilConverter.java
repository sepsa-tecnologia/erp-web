/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 *
 * @author Ralf Adam
 */
public class PerfilConverter {
    
    

    private Integer id;
    private String codigo;
    private String descripcion;
    private Empresa empresa;
    private Integer idEmpresa;
    private String activo;
    private String permisos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getPermisos() {
        return permisos;
    }

    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }

    public PerfilConverter(){
    }

    public void convert(Perfil perfil) {
        this.id = perfil.getId();
        this.codigo = perfil.getCodigo();
        this.descripcion = perfil.getDescripcion();
        this.empresa = perfil.getEmpresa();
        this.idEmpresa = perfil.getIdEmpresa();
        this.activo = perfil.getActivo();
        this.permisos = perfil.getPermisos().toString();
    }
    

    
    
}
