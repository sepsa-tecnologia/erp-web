package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;

/**
 * Filtro utilizado para usuario perfil relacionado
 *
 * @author Romina Núñez
 */
public class UsuarioPerfilRelacionadoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del usuario
     *
     * @param idUsuario Identificador del usuario
     * @return
     */
    public UsuarioPerfilRelacionadoFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del perfil
     *
     * @param idPerfil Identificador del usuario
     * @return
     */
    public UsuarioPerfilRelacionadoFilter idPerfil(Integer idPerfil) {
        if (idPerfil != null) {
            params.put("idPerfil", idPerfil);
        }
        return this;
    }

    /**
     * Agrega el filtro tiene saldo
     *
     * @param tieneSaldo
     * @return
     */
    public UsuarioPerfilRelacionadoFilter perfilesSepsa(String perfilesSepsa) {
        if (perfilesSepsa != null && !perfilesSepsa.trim().isEmpty()) {
            params.put("perfilesSepsa", perfilesSepsa.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param usuarioperfil
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(UsuarioPerfilRelacionado usuarioPerfilRelacionado, Integer page, Integer pageSize) {

        UsuarioPerfilRelacionadoFilter filter = new UsuarioPerfilRelacionadoFilter();

        filter
                .idUsuario(usuarioPerfilRelacionado.getIdUsuario())
                .idPerfil(usuarioPerfilRelacionado.getIdPerfil())
                .perfilesSepsa(usuarioPerfilRelacionado.getPerfilesSepsa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de UsuarioPerfilFilter
     */
    public UsuarioPerfilRelacionadoFilter() {
        super();
    }

}
