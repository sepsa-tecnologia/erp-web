/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.Date;

/**
 * Pojo para aprobacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Aprobacion {

    /**
     * Id aprovacion
     */
    private Integer id;
    /**
     * Id Estado
     */
    private Integer idEstado;
    /**
     * fecha Insercion
     */
    private Date fechaInsercion;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS">

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the fechaInsercion
     */
    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * @param fechaInsercion the fechaInsercion to set
     */
    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    //</editor-fold>
    /**
     * Constructor
     */
    public Aprobacion() {

    }
}
