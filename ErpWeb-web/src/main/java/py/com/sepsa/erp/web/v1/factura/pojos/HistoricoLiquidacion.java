package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * POJO para Liquidación
 *
 * @author Romina Núñez
 */
public class HistoricoLiquidacion {
    /**
     * Identificador de Historico Liquidacion
     */
    private Integer idHistoricoLiquidacion;
    /**
     * Fecha Proceso
     */
    private Date fechaProceso;
    /**
     * Monto Total Detalle
     */
    private BigDecimal montoTotalDetalle;
    /**
     * Monto total Excedente
     */
    private BigDecimal montoTotalExcedente;
    /**
     * Monto Descuento General
     */
    private BigDecimal montoDescuentoGeneral;
    /**
     * Monto Descuento Excedente
     */
    private BigDecimal montoDescuentoExcedente;
    /**
     * Monto Descuento Detalle
     */
    private BigDecimal montoDescuentoDetalle;
    /**
     * Monto Total Liquidado
     */
    private BigDecimal montoTotalLiquidado;
    /**
     * Monto Mínimo
     */
    private String montoMinimo;
    /**
     * Estado
     */
    private String estado;
    /**
     * Cobro Adelantado
     */
    private String cobroAdelantado;
    /**
     * Identificador de liquidación
     */
    private Integer idLiquidacion;
    /**
     * Cantidad de documentos recibidos
     */
    private Integer cantDocRecibidos;
    /**
     * Cantidad de documentos enviados
     */
    private Integer cantDocEnviados;
    /**
     * Cantidad de documentos excedidos
     */
    private Integer cantDocExcedidos;
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    /**
     * Moneda
     */
    private String moneda;
    /**
     * Fecha Desde
     */
    private Date fechaDesde;
    /**
     * Fecha Hasta
     */
    private Date fechaHasta;
    /**
     * Identificador de Contrato
     */
    private Integer idContrato;
    /**
     * Identificador de Producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
    /**
     * Identificador de Servicio
     */
    private Integer idServicio;
    /**
     * Servicio
     */
    private String servicio;
    /**
     * Actual
     */
    private boolean actual;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Detalle de Histórico Liquidación
     */
    private List<HistoricoLiquidacionDetalle> detalles = new ArrayList<>();


    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    
    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }
    
    public Date getFechaProceso() {
        return fechaProceso;
    }
    
    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }
    
   
    
    public String getMontoMinimo() {
        return montoMinimo;
    }
    
    public void setMontoMinimo(String montoMinimo) {
        this.montoMinimo = montoMinimo;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String getCobroAdelantado() {
        return cobroAdelantado;
    }
    
    public void setCobroAdelantado(String cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }
    
    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }
    
    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }
    
    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }
    
    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }
    
    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }
    
    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }
    
    public Integer getCantDocExcedidos() {
        return cantDocExcedidos;
    }
    
    public void setCantDocExcedidos(Integer cantDocExcedidos) {
        this.cantDocExcedidos = cantDocExcedidos;
    }
    
    public Integer getIdMoneda() {
        return idMoneda;
    }
    
    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    public String getMoneda() {
        return moneda;
    }
    
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
    
    public Date getFechaDesde() {
        return fechaDesde;
    }
    
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }
    
    public Date getFechaHasta() {
        return fechaHasta;
    }
    
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }
    
    public Integer getIdContrato() {
        return idContrato;
    }
    
    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }
    
    public Integer getIdProducto() {
        return idProducto;
    }
    
    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }
    
    public String getProducto() {
        return producto;
    }
    
    public void setProducto(String producto) {
        this.producto = producto;
    }

    public BigDecimal getMontoTotalDetalle() {
        return montoTotalDetalle;
    }

    public void setMontoTotalDetalle(BigDecimal montoTotalDetalle) {
        this.montoTotalDetalle = montoTotalDetalle;
    }

    public BigDecimal getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(BigDecimal montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public BigDecimal getMontoDescuentoGeneral() {
        return montoDescuentoGeneral;
    }

    public void setMontoDescuentoGeneral(BigDecimal montoDescuentoGeneral) {
        this.montoDescuentoGeneral = montoDescuentoGeneral;
    }

    public BigDecimal getMontoDescuentoExcedente() {
        return montoDescuentoExcedente;
    }

    public void setMontoDescuentoExcedente(BigDecimal montoDescuentoExcedente) {
        this.montoDescuentoExcedente = montoDescuentoExcedente;
    }

    public BigDecimal getMontoDescuentoDetalle() {
        return montoDescuentoDetalle;
    }

    public void setMontoDescuentoDetalle(BigDecimal montoDescuentoDetalle) {
        this.montoDescuentoDetalle = montoDescuentoDetalle;
    }

    public BigDecimal getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(BigDecimal montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }
    
    public Integer getIdServicio() {
        return idServicio;
    }
    
    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }
    
    public String getServicio() {
        return servicio;
    }
    
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }
    
    public boolean isActual() {
        return actual;
    }
    
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
    public void setActual(boolean actual) {
        this.actual = actual;
    }
    
    public Integer getIdCliente() {
        return idCliente;
    }
    
    public void setDetalles(List<HistoricoLiquidacionDetalle> detalles) {
        this.detalles = detalles;
    }
    
    public List<HistoricoLiquidacionDetalle> getDetalles() {
        return detalles;
    }
//</editor-fold>


    /**
     * Constructor de la clase
     */
    public HistoricoLiquidacion() {
    }

    
    /**
     * Constructor con parametros
     * @param idHistoricoLiquidacion
     * @param fechaProceso
     * @param montoTotalDetalle
     * @param montoTotalExcedente
     * @param montoDescuentoGeneral
     * @param montoDescuentoExcedente
     * @param montoDescuentoDetalle 
     * @param montoTotalLiquidado
     * @param montoMinimo
     * @param estado
     * @param cobroAdelantado
     * @param idLiquidacion
     * @param cantDocRecibidos
     * @param cantDocEnviados
     * @param cantDocExcedidos
     * @param idMoneda
     * @param moneda
     * @param fechaDesde
     * @param fechaHasta
     * @param idContrato
     * @param idProducto
     * @param producto
     * @param idServicio
     * @param servicio
     * @param actual
     * @param idCliente 
     * @param detalles 
     */
    public HistoricoLiquidacion(Integer idHistoricoLiquidacion, Date fechaProceso, BigDecimal montoTotalDetalle, BigDecimal montoTotalExcedente, BigDecimal montoDescuentoGeneral, BigDecimal montoDescuentoExcedente,BigDecimal montoDescuentoDetalle, BigDecimal montoTotalLiquidado, String montoMinimo, String estado, String cobroAdelantado, Integer idLiquidacion, Integer cantDocRecibidos, Integer cantDocEnviados, Integer cantDocExcedidos, Integer idMoneda, String moneda, Date fechaDesde, Date fechaHasta, Integer idContrato, Integer idProducto, String producto, Integer idServicio, String servicio, boolean actual, Integer idCliente, List<HistoricoLiquidacionDetalle> detalles) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.fechaProceso = fechaProceso;
        this.montoTotalDetalle = montoTotalDetalle;
        this.montoTotalExcedente = montoTotalExcedente;
        this.montoDescuentoGeneral = montoDescuentoGeneral;
        this.montoDescuentoExcedente = montoDescuentoExcedente;
        this.montoDescuentoDetalle = montoDescuentoDetalle;
        this.montoTotalLiquidado = montoTotalLiquidado;
        this.montoMinimo = montoMinimo;
        this.estado = estado;
        this.cobroAdelantado = cobroAdelantado;
        this.idLiquidacion = idLiquidacion;
        this.cantDocRecibidos = cantDocRecibidos;
        this.cantDocEnviados = cantDocEnviados;
        this.cantDocExcedidos = cantDocExcedidos;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.idContrato = idContrato;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.actual = actual;
        this.idCliente = idCliente;
        this.detalles = detalles;
    }

    

}
