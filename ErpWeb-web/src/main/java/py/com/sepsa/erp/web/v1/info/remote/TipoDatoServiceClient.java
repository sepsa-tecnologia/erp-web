package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.TipoDatoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TypeFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para los servicios de usuario
 *
 * @author Romina Núñez
 */
public class TipoDatoServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de tipos de clientes
     *
     * @param tipodato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la página de resultado
     * @return
     */
    public TipoDatoListAdapter getTipoDatoList(TipoDato tipodato, Integer page,
            Integer pageSize) {
       
        TipoDatoListAdapter lista = new TipoDatoListAdapter();
        
        Map params = TypeFilter.build(tipodato, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR_TIPO_DATO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoDatoListAdapter.class);

            lista = (TipoDatoListAdapter) response.getPayload();
           
            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de TipoDatoService
     */
    public TipoDatoServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        LISTAR_TIPO_DATO("Listar tipo de dato", "tipo-dato-persona");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
