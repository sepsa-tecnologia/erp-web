/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoFactura;

/**
 * Pojo para FacturaCompra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class FacturaCompra {

    /**
     * Identificador del la factura
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador del tipo factura
     */
    private Integer idTipoFactura;
    /**
     * fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * fecha
     */
    private Date fecha;
    /**
     * Fecha
     */
    private Date fechaDesde;
    /**
     * Fecha
     */
    private Date fechaHasta;
    /**
     * Nro Timbrado
     */
    private String nroTimbrado;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Nro de factura
     */
    private String nroFactura;
    /**
     * Razón Social del cliente
     */
    private String razonSocial;
    /**
     * RUC del cliente
     */
    private String ruc;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * Digital
     */
    private String digital;
    /**
     * Pagado
     */
    private String pagado;
    /**
     * Días de Crédito
     */
    private Integer diasCredito;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total factura
     */
    private BigDecimal montoTotalFactura;
    /**
     * Saldo
     */
    private BigDecimal saldo;
    /**
     * Identificador de motivo anulacion
     */
    private Integer idMotivoAnulacion;
    /**
     * observacion de anulacion
     */
    private String observacionAnulacion;
    /**
     * POJO Tipo Factura
     */
    private TipoFactura tipoFactura;
    /**
     * POJO Moneda
     */
    private Moneda moneda;
    /**
     * Empresa
     */
    private Empresa empresa;
    /**
     * Motivo Anulacion
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Listado de detalles de facturaCompra
     */
    private List<FacturaCompraDetalles> facturaCompraDetalles;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the idEmpresa
     */
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     * @return the idMoneda
     */
    public Integer getIdMoneda() {
        return idMoneda;
    }

    /**
     * @param idMoneda the idMoneda to set
     */
    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    /**
     * @return the idTipoFactura
     */
    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    /**
     * @param idTipoFactura the idTipoFactura to set
     */
    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    /**
     * @return the fechaInsercion
     */
    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * @param fechaInsercion the fechaInsercion to set
     */
    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    /**
     * @return the nroTimbrado
     */
    public String getNroTimbrado() {
        return nroTimbrado;
    }

    /**
     * @param nroTimbrado the nroTimbrado to set
     */
    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    /**
     * @return the fechaVencimientoTimbrado
     */
    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    /**
     * @param fechaVencimientoTimbrado the fechaVencimientoTimbrado to set
     */
    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }

    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    /**
     * @return the anulado
     */
    public String getAnulado() {
        return anulado;
    }

    /**
     * @param anulado the anulado to set
     */
    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    /**
     * @return the digital
     */
    public String getDigital() {
        return digital;
    }

    /**
     * @param digital the digital to set
     */
    public void setDigital(String digital) {
        this.digital = digital;
    }

    /**
     * @return the pagado
     */
    public String getPagado() {
        return pagado;
    }

    /**
     * @param pagado the pagado to set
     */
    public void setPagado(String pagado) {
        this.pagado = pagado;
    }

    /**
     * @return the diasCredito
     */
    public Integer getDiasCredito() {
        return diasCredito;
    }

    /**
     * @param diasCredito the diasCredito to set
     */
    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    /**
     * @return the montoIva5
     */
    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    /**
     * @param montoIva5 the montoIva5 to set
     */
    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    /**
     * @return the montoImponible5
     */
    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    /**
     * @param montoImponible5 the montoImponible5 to set
     */
    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    /**
     * @return the montoTotal5
     */
    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    /**
     * @param montoTotal5 the montoTotal5 to set
     */
    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    /**
     * @return the montoIva10
     */
    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    /**
     * @param montoIva10 the montoIva10 to set
     */
    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    /**
     * @return the montoImponible10
     */
    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    /**
     * @param montoImponible10 the montoImponible10 to set
     */
    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    /**
     * @return the montoTotal10
     */
    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    /**
     * @param montoTotal10 the montoTotal10 to set
     */
    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    /**
     * @return the montoTotalExento
     */
    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    /**
     * @param montoTotalExento the montoTotalExento to set
     */
    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    /**
     * @return the montoIvaTotal
     */
    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    /**
     * @param montoIvaTotal the montoIvaTotal to set
     */
    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    /**
     * @return the montoImponibleTotal
     */
    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    /**
     * @param montoImponibleTotal the montoImponibleTotal to set
     */
    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    /**
     * @return the montoTotalFactura
     */
    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    /**
     * @param montoTotalFactura the montoTotalFactura to set
     */
    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    /**
     * @return the saldo
     */
    public BigDecimal getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the tipoFactura
     */
    public TipoFactura getTipoFactura() {
        return tipoFactura;
    }

    /**
     * @param tipoFactura the tipoFactura to set
     */
    public void setTipoFactura(TipoFactura tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    /**
     * @return the moneda
     */
    public Moneda getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the facturaCompraDetalles
     */
    public List<FacturaCompraDetalles> getFacturaCompraDetalles() {
        return facturaCompraDetalles;
    }

    /**
     * @param facturaCompraDetalles the facturaCompraDetalles to set
     */
    public void setFacturaCompraDetalles(List<FacturaCompraDetalles> facturaCompraDetalles) {
        this.facturaCompraDetalles = facturaCompraDetalles;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    //</editor-fold>
    public FacturaCompra() {

    }

}
