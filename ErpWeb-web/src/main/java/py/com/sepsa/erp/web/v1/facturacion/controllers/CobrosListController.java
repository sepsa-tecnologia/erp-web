package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ArchivoAdjunto;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.notificacion.remote.NotificacionService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;

/**
 * Controlador para lista de cobros
 *
 * @author alext
 */
@SessionScoped
@Named("listarCobros")
public class CobrosListController implements Serializable {

    /**
     * Adaptador de la lista de cobros
     */
    private CobroPojoAdapter adapter;

    /**
     * Pojo para cobros
     */
    private CobroPojo searchData;
    /**
     * Servicio Cobro
     */
    private CobroService serviceCobro;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Email Reenvio
     */
    private String emailReenvio;
    /**
     * Agregar Mail
     */
    private Boolean addEmail;
    /**
     * Pojo para cobros
     */
    private Cobro cobroReenvio;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    
    private Cliente client;
    
    private ClientListAdapter adapterCliente;
    
    private Integer nroFilas;

    private Estado estado;
    
    private EstadoAdapter estadoAdapterList;
    
    private List<CobroPojo> selectedCobros;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    
    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public Integer getNroFilas() {
        return nroFilas;
    }
    
        public void setNroFilas(Integer nroFilas) {    
        this.nroFilas = nroFilas;
    }

    /**
     * @return the adapterMotivoAnulacion
     */
    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    public void setEmailReenvio(String emailReenvio) {
        this.emailReenvio = emailReenvio;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setCobroReenvio(Cobro cobroReenvio) {
        this.cobroReenvio = cobroReenvio;
    }

    public Cobro getCobroReenvio() {
        return cobroReenvio;
    }

    public void setAddEmail(Boolean addEmail) {
        this.addEmail = addEmail;
    }

    public String getEmailReenvio() {
        return emailReenvio;
    }

    public Boolean getAddEmail() {
        return addEmail;
    }

    /**
     * @param adapterMotivoAnulacion the adapterMotivoAnulacion to set
     */
    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    /**
     * @return the motivoAnulacion
     */
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    /**
     * @param motivoAnulacion the motivoAnulacion to set
     */
    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    /**
     * @return the serviceCobro
     */
    public CobroService getServiceCobro() {
        return serviceCobro;
    }

    /**
     * @param serviceCobro the serviceCobro to set
     */
    public void setServiceCobro(CobroService serviceCobro) {
        this.serviceCobro = serviceCobro;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public CobroPojoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(CobroPojoAdapter adapter) {
        this.adapter = adapter;
    }

    public CobroPojo getSearchData() {
        return searchData;
    }

    public void setSearchData(CobroPojo searchData) {
        this.searchData = searchData;
    }

    public List<CobroPojo> getSelectedCobros() {
        return selectedCobros;
    }

    public void setSelectedCobros(List<CobroPojo> selectedCobros) {
        this.selectedCobros = selectedCobros;
    }    
    //</editor-fold>

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectCliente(SelectEvent event) {
        searchData.setIdCliente(((Cliente) event.getObject()).getIdCliente());

    }
    
    /**
     * Método para filtrar cobros
     */
    public void filterCobros() {
        adapter.setPageSize(nroFilas);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        
        this.adapter = new CobroPojoAdapter();
        this.searchData = new CobroPojo();
        filterCobros();
    }

    /**
     * Método para filtrar cobros
     */
    public void refreshCobros() {
        getEstados();
        clear();
    }

    public void validarCorreo(String email) {
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void guardarCobroAReenviar(Integer id, String nro, String cliente) {
        this.cobroReenvio = new Cobro();
        this.cobroReenvio.setId(id);
        this.cobroReenvio.setNroRecibo(nro);
        this.cobroReenvio.setRazonSocial(cliente);
        this.emailReenvio = null;
        //  this.emailReenvio = email;

        if (emailReenvio != null) {
            addEmail = true;
        } else {
            addEmail = false;
        }
    }

    public void reenviar() {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String empresa = session.getNombreEmpresa();
        String cliente = cobroReenvio.getRazonSocial();
        String mailTo = emailReenvio;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su Documento en formato PDF";
        String subjet = "Recibo No. " + cobroReenvio.getNroRecibo();
        String url = "cobro/consulta/" + cobroReenvio.getId();
        byte[] data = fileServiceClient.download(url);
        String fileName = cobroReenvio.getNroRecibo() + ".pdf";

        if (!mailSender.equalsIgnoreCase("N") && !passSender.equalsIgnoreCase("N")) {
            reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, cobroReenvio.getNroRecibo(), host, port, empresa, emailEmpresa, telefonoEmpresa);
        } else {
            reenviarViaNotificacionSepsa(mensaje, subjet, mailTo, data, fileName);
        }

    }
    
    /**
     * Método para reenviar documento por correo
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     * @param from
     * @param passFrom
     * @param cliente
     * @param nroDoc
     */
    public void reenviarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host, port);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado el recibo al dirección confirmada"));
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para reenviar documento via sepsa notificación
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     */
    public void reenviarViaNotificacionSepsa(String msn, String subject, String to, byte[] file, String fileName) {
        Notificacion notificacion = new Notificacion();
        TipoNotificaciones tipoNotificacion = new TipoNotificaciones();
        ArchivoAdjunto archivo = new ArchivoAdjunto();
        NotificacionService servicioNoti = new NotificacionService();
        List<ArchivoAdjunto> notificationAttachments = new ArrayList<>();

        BodyResponse<Notificacion> respuestaNoti = new BodyResponse<>();

        try {
            tipoNotificacion.setCode("MAIL_ALERTAS");

            archivo.setContentType("application/pdf");
            archivo.setFileName(fileName);
            archivo.setData(file);
            notificationAttachments.add(archivo);

            notificacion.setSubject(subject);
            notificacion.setMessage(msn);
            notificacion.setTo(to);
            notificacion.setState("P");
            notificacion.setObservation("--");
            notificacion.setNotificationType(tipoNotificacion);
            notificacion.setNotificationAttachments(notificationAttachments);

            respuestaNoti = servicioNoti.createNotificacion(notificacion);

            if (respuestaNoti.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado el recibo al dirección confirmada"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }
    
    public StreamedContent descargaMasivaCobros(){
        
        List<Integer> idCobros = new ArrayList();
        for (CobroPojo cobro : selectedCobros){
            if (cobro!=null) {
                idCobros.add(cobro.getId());
            }
        }
        if (!idCobros.isEmpty()){
            byte[] data = serviceCobro.descargaMasiva(idCobros);
            String fileName = "listado-cobros.zip";
            String contentType = ("application/zip");
            selectedCobros = new ArrayList<>();
            return new DefaultStreamedContent(new ByteArrayInputStream(data),contentType, fileName);
            
        } else {           
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar al menos un cobro para descargar"));
            return null;
        }          
    }
    
    /**
     * Método para anular el cobro
     *
     * @param cobro
     */
    public void anularCobro(CobroPojo cobro) {
        Cobro co = new Cobro();
        co.setId(cobro.getId());
        co.setIdMotivoAnulacion(cobro.getIdMotivoAnulacion());
        co.setObservacionAnulacion(cobro.getObservacionAnulacion());

        BodyResponse cobroResp = serviceCobro.anularCobro(co);

        if (cobroResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Cobro Anulado!"));
            clear();
        }

    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleCobroURL(Integer id) {

        return String.format("cobro-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

        /**
     * Método para obtener estado
     */
    public void getEstados() {
        estado = new Estado();
        estado.setCodigoTipoEstado("COBRO");
        estadoAdapterList = new EstadoAdapter();
        estadoAdapterList = estadoAdapterList.fillData(estado);
    }

    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            nroFilas=10;
            this.adapter = new CobroPojoAdapter();
            this.searchData = new CobroPojo();
            this.serviceCobro = new CobroService();
            this.estadoAdapterList = new EstadoAdapter();
          
            filterCobros();
            this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
            this.motivoAnulacion = new MotivoAnulacion();

            motivoAnulacion.setActivo('S');
            motivoAnulacion.setCodigoTipoMotivoAnulacion("COBRO");
            adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);

            this.emailReenvio = null;
            this.addEmail = false;
            this.cobroReenvio = new Cobro();
            this.fileServiceClient = new FileServiceClient();
            this.client = new Cliente();
            this.adapterCliente = new ClientListAdapter();
            
            this.selectedCobros = new ArrayList();

            getEstados();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
