package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;
import java.math.BigDecimal;

/**
 * POJO para Monto
 *
 * @author Romina Núñez
 */
public class Cuota {

    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de lugar cobro
     */
    private BigDecimal monto;
    /**
     * Lugar cobro
     */
    private Date fechaVencimiento;

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Constructor de la clase
     */
    public Cuota() {
    }

}
