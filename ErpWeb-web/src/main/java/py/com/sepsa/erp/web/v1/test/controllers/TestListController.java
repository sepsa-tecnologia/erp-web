package py.com.sepsa.erp.web.v1.test.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.test.adapters.TestListAdapter;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;
import py.com.sepsa.erp.web.v1.test.pojos.Test;

/**
 * Controlador para la lista test
 *
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("testList")
public class TestListController implements Serializable {

    /**
     * Adaptador de la lista test
     */
    private TestListAdapter adapter;

    /**
     * Datos utilizados para realizar búsquedas
     */
    private Test searchData;

    private List<Mensaje> listaErrorPrueba = new ArrayList<>();

    public List<Mensaje> getListaErrorPrueba() {
        return listaErrorPrueba;
    }

    public void setListaErrorPrueba(List<Mensaje> listaErrorPrueba) {
        this.listaErrorPrueba = listaErrorPrueba;
    }
    
    

    /**
     * Obtiene los datos de búsqueda
     *
     * @return Datos de búsqueda
     */
    public Test getSearchData() {
        return searchData;
    }

    /**
     * Setea los datos de búsqueda
     *
     * @param searchData Datos de búsqueda
     */
    public void setSearchData(Test searchData) {
        this.searchData = searchData;
    }

    /**
     * Obtiene el adaptador de la lista test
     *
     * @return Adaptador de la lista test
     */
    public TestListAdapter getAdapter() {
        return adapter;
    }

    /**
     * Setea el adaptador de la lista test
     *
     * @param adapter Adaptador de la lista test
     */
    public void setAdapter(TestListAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clear() {
        searchData = new Test();
        search();
    }

    /**
     * Método de búsqueda
     */
    public void search() {

        adapter = new TestListAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Obtiene la URL para la edición
     *
     * @param id Identificador
     * @return URL para la edición
     */
    public String getEditUrl(Integer id) {
        return String.format("test-edit"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    public void fillListaError() {
        for (int i = 0; i < 6; i++) {
            Mensaje msn = new Mensaje();
            msn.setId(i);
            msn.setDescripcion("Error N°: " + i);
            msn.setTipo("ERROR");
            listaErrorPrueba.add(msn);

        }
    }

    /**
     * Inicializa los valores de la lista
     */
    @PostConstruct
    public void init() {
        fillListaError();
        clear();

    }

    /**
     * Constructor de TestListController
     */
    public TestListController() {
    }

}
