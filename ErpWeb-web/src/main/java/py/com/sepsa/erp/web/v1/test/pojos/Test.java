
package py.com.sepsa.erp.web.v1.test.pojos;

/**
 * POJO para test
 * @author Daniel F. Escauriza Arza
 */
public class Test {
    
     /**
     * Identificador del test
     */
    private Integer id;
    
    /**
     * Dato num. 1
     */
    private String data1;
    
    /**
     * Dato num. 2
     */
    private String data2;

    /**
     * Obtiene el identificador del test
     * @return Identificador del test
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador del test
     * @param id Identificador del test
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene el dato num. 1
     * @return Dato num. 1
     */
    public String getData1() {
        return data1;
    }

    /**
     * Setea el dato num. 1
     * @param data1 Dato num. 1
     */
    public void setData1(String data1) {
        this.data1 = data1;
    }

    /**
     * Obtiene el dato num. 2
     * @return Dato num. 2
     */
    public String getData2() {
        return data2;
    }

    /**
     * Setea el dato num. 2
     * @param data2 Dato num. 2
     */
    public void setData2(String data2) {
        this.data2 = data2;
    }

    /**
     * Constructor de Test
     * @param id Identificador del test
     * @param data1 Dato num. 1
     * @param data2 Dato num. 2
     */
    public Test(Integer id, String data1, String data2) {
        this.id = id;
        this.data1 = data1;
        this.data2 = data2;
    }

    /**
     * Constructor de Test
     */
    public Test() {}
    
}
