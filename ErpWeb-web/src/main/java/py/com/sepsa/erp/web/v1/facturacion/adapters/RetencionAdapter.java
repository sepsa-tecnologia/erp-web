/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionService;

/**
 * Adaptador de Retencion
 * @author CristinaInsfrán
 */
public class RetencionAdapter extends DataListAdapter<Retencion>{
     /**
     * Cliente para los servicios de Retencion
     */
    private final RetencionService retencionClient;

    @Override
    public RetencionAdapter fillData(Retencion searchData) {
        return retencionClient.getRetencionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de RetencionAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public RetencionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.retencionClient = new RetencionService();
    }

    /**
     * Constructor de RetencionAdapter
     */
    public RetencionAdapter() {
        super();
        this.retencionClient = new RetencionService();
    } 
}
