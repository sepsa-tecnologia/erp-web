/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.remote.TalonarioService;

/**
 * Adapter para talonario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TalonarioAdapter extends DataListAdapter<Talonario> {

    /**
     * Cliente para el servicio de Talonario
     */
    private final TalonarioService serviceTalonario;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TalonarioAdapter fillData(Talonario searchData) {

        return serviceTalonario.getTalonarioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TalonarioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TalonarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTalonario = new TalonarioService();
    }

    /**
     * Constructor de TalonarioAdapter
     */
    public TalonarioAdapter() {
        super();
        this.serviceTalonario = new TalonarioService();
    }
}
