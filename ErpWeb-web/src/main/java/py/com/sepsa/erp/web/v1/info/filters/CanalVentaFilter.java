/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;

/**
 * Filtro para canal de ventas
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CanalVentaFilter extends Filter {

    /**
     * Agrega el filtro Id
     *
     * @param id Identificador del tipo contacto
     * @return
     */
    public CanalVentaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para la descripción
     *
     * @param descripcion
     * @return
     */
    public CanalVentaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro por código
     *
     * @param codigo código
     * @return
     */
    public CanalVentaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por activo
     *
     * @param activo código
     * @return
     */
    public CanalVentaFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param canalVenta
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(CanalVenta canalVenta, Integer page, Integer pageSize) {
        CanalVentaFilter filter = new CanalVentaFilter();
        
        filter
                .id(canalVenta.getId())
                .descripcion(canalVenta.getDescripcion())
                .codigo(canalVenta.getCodigo())
                .activo(canalVenta.getActivo())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de CargoFilter
     */
    public CanalVentaFilter() {
        super();
    }
    
}
