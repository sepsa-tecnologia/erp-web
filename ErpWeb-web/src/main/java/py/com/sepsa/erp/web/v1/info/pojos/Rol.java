
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de Rol
 * @author Cristina Insfrán
 */
public class Rol {

    /**
     * Identificador del Rol
     */
    private Integer id;
    /**
     * Descripción del Rol
     */
    private String descripcion;
    
     /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Constructor de la clase
     */
    public Rol(){
        
    }
    
    /**
     * Contructor de la clase con parametros
     * @param id
     * @param descripcion
     */
    public Rol(Integer id,String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }
    
}
