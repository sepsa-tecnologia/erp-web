/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * Pojo para Factura Procesamiento
 *
 * @author Romina Núñez
 */
public class DocumentoProcesamiento {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Procesamiento
     */
    private Procesamiento procesamiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Procesamiento getProcesamiento() {
        return procesamiento;
    }

    public void setProcesamiento(Procesamiento procesamiento) {
        this.procesamiento = procesamiento;
    }

    public DocumentoProcesamiento() {
    }
    
    

}
