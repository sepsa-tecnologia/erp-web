package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ToggleEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.CargoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoEmailAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoEmailNotificacionAsociadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoNotificacionAsociadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaEmailAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoContactoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmailListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoNotificacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Cargo;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmailNotificacion;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefonoNotificacion;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.TipoNotificacion;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.ContactoEmailNotificacionService;
import py.com.sepsa.erp.web.v1.info.remote.ContactoEmailService;
import py.com.sepsa.erp.web.v1.info.remote.ContactoServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.ContactoTelefonoNotificacionService;
import py.com.sepsa.erp.web.v1.info.remote.ContactoTelefonoService;
import py.com.sepsa.erp.web.v1.info.remote.EmailServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.PersonaEmailClient;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.PersonaTelefonoClient;
import py.com.sepsa.erp.web.v1.info.remote.TelefonoServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para Contactos
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("contact")
public class ContactController implements Serializable {

    /**
     * Bandera
     */
    private boolean show;
    /**
     * POJO Tipo Telefono
     */
    private TipoTelefono tipoTelefonoFilter;
    /**
     * Adaptador para el tipo de telefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefono;
    /**
     * POJO Telefono
     */
    private Telefono telefonoCreate;
    /**
     * POJO Telefono
     */
    private Telefono telefonoAgregar;
    /**
     * Asociación persona email
     */
    private PersonaEmail personaEmailCreate;
    /**
     * Servicio Persona Email
     */
    private PersonaEmailClient servicePersonaEmail;
    /**
     * Servicio Email
     */
    private EmailServiceClient serviceEmail;
    /**
     * POJO Tipo Email
     */
    private TipoEmail tipoEmailFilter;
    /**
     * Adaptador tipo email
     */
    private TipoEmailListAdapter adapterTipoEmail;
    /**
     * POJO Email
     */
    private Email emailCreate;
    /**
     * POJO Email
     */
    private Email emailAgregar;
    /**
     * Adaptador para la lista de contacto teléfono
     */
    private ContactoTelefonoAdapter adapterContactoTelefono;
    /**
     * Objeto Contacto Teléfono
     */
    private ContactoTelefono contactoTelefonoFilter;
    /**
     * Objeto persona telefono contacto
     */
    private PersonaTelefono personaTelefonoContactoFilter;
    /**
     * Adaptador para la lista de ContactoEmail
     */
    private ContactoEmailAdapter adapterContactoEmail;
    /**
     * Objeto ContactoEmail
     */
    private ContactoEmail contactoEmailFilter;
    /**
     * Objeto TipoContacto
     */
    private TipoContacto tipoContactoFilter;
    /**
     * Adaptador para la lista de tipo de contacto
     */
    private TipoContactoListAdapter adapterTipoContacto;
    /**
     * Objeto Cargo
     */
    private Cargo cargoFilter;
    /**
     * Adaptador para la lista de cargo
     */
    private CargoListAdapter adapterCargo;
    /**
     * Objeto Contacto
     */
    private Contacto contactoFilter;
    /**
     * Adaptador para la lista de contactos
     */
    private ContactoListAdapter adapterContacto;
    /**
     * Objeto Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter adapterPersona;
    /**
     * Dato de contacto
     */
    private Integer id;
    /**
     * Objeto PersonaContacto
     */
    private PersonaContacto personaContactoFilter;
    /**
     * Adaptador para la lista de persona contacto
     */
    private PersonaContactoListAdapter personaContactoAdapter;
    /**
     * Dato del Cliente
     */
    private String idCliente;
    /**
     * Adaptador para la lista de Clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFilter;
    /**
     * Adaptador para la lista de persona email
     */
    private PersonaEmailAdapter adapterPersonaEmail;
    /**
     * Adaptador para lista de persona email contactos
     */
    private PersonaEmailAdapter adapterPersonaEmailContacto;
    /**
     * Adpatado para lista de persona teléfono contactos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefonoContacto;
    /**
     * Objeto PersonaEmail
     */
    private PersonaEmail personaEmailFilter;
    /**
     * Objeto PersonaEmail
     */
    private PersonaEmail personaEmailContactoFilter;
    /**
     * Objeto PersonaTelefono
     */
    private PersonaTelefono personaTelefonoFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Adaptador para la lista de notificaciones
     */
    private ContactoTelefonoNotificacionAsociadoAdapter adapterContactoTelefonoNotificacion;
    /**
     * POJO Contacto Telefono Notificación
     */
    private ContactoTelefonoNotificacion contactoTelefonoNotificacionFilter;
    /**
     * POJO Contacto Email Notificación
     */
    private ContactoEmailNotificacion contactoEmailNotificacionFilter;
    /**
     * Service telefono
     */
    private TelefonoServiceClient serviceTelefono;
    /**
     * Service Persona Teléfono
     */
    private PersonaTelefonoClient servicePersonaTelefono;
    /**
     * POJO Persona Telefono
     */
    private PersonaTelefono personaTelefonoCreate;
    /**
     * Servicio Contacto
     */
    private ContactoServiceClient serviceContacto;
    /**
     * Objeto Persona
     */
    private Persona personaCreate;
    /**
     * Service persona
     */
    private PersonaServiceClient servicePersona;
    /**
     * Objeto contacto
     */
    private Contacto contactoCreate;
    /**
     * Pojo persona contacto
     */
    private PersonaContacto personaContactoCreate;

    /**
     * Servicio Contacto
     */
    private ContactoServiceClient servicePersonaContacto;
    /**
     * Objeto telefono para contacto
     */
    private Telefono telefonoContactoCreate;
    /**
     * Objeto email para contacto
     */
    private Email emailContactoCreate;
    /**
     * Objeto Contacto Telefono
     */
    private ContactoTelefono telefonoContactoAsociacion;
    /**
     * Objeto Contacto Email
     */
    private ContactoEmail emailContactoAsociacion;
    /**
     * Servicio Contacto Email
     */
    private ContactoEmailService contactoEmailService;
    /**
     * Servicio Contacto Telefono
     */
    private ContactoTelefonoService contactoTelefonoService;
    /**
     * POJO Tipo Email
     */
    private TipoEmail tipoEmailContactoFilter;
    /**
     * Adaptador tipo email
     */
    private TipoEmailListAdapter adapterTipoEmailContacto;
    /**
     * POJO Tipo Telefono
     */
    private TipoTelefono tipoTelefonoContactoFilter;
    /**
     * Adaptador para el tipo de telefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefonoContacto;
    /**
     * Identificador de Telefono Contacto
     */
    private Integer idTelefonoContacto = null;
    /**
     * Identificador de Email Contacto
     */
    private Integer idEmailContacto = null;
    /**
     * Adaptador para la lista de notificaciones por contacto
     */
    private ContactoEmailNotificacionAsociadoAdapter adapterContactoEmailNotificacionAsociado;
    /**
     * Servicio Email
     */
    private EmailServiceClient serviceEmailContacto;
    /**
     * Service telefono
     */
    private TelefonoServiceClient serviceTelefonoContacto;
    /**
     * Servicio Contacto Email Notificacion
     */
    private ContactoEmailNotificacionService serviceContactoEmailNotificacion;
    /**
     * Objeto Contacto Email Notificacion
     */
    private ContactoEmailNotificacion contactoEmailNotificacionEdit;
    /**
     * Pojo contactoEmailAsociado
     */
    private ContactoEmail contactoEmailAsociado;
    /**
     * Servicio contacto Telefono Notificacion
     */
    private ContactoTelefonoNotificacionService serviceContactoTelefonoNotificacion;
    /**
     * Objeto ContactoTelefonoNotificacion
     */
    private ContactoTelefonoNotificacion contactoTelefonoNotificacionEdit;
    /**
     * Pojo contacto telefono
     */
    private ContactoTelefono contactoTelefonoAsociado;
    /**
     * POJO PersonaContacto
     */
    private Integer idContacto;

    /**
     * Adaptador para la lista de notificaciones por contacto
     */
    private TipoNotificacionAdapter adapterTipoNotificacion;
    /**
     * POJO Tipo Notificación
     */
    private TipoNotificacion tipoNotificacionFilter;
    /**
     * Lista de notificación telefonos
     */
    private List<TipoNotificacion> listNotificacionTelefono = new ArrayList<>();
    /**
     * Lista de notificación de mails
     */
    private List<TipoNotificacion> listNotificacionMail = new ArrayList<>();
    /**
     * Adaptador para la lista de notificaciones por contacto
     */
    private TipoNotificacionAdapter adapterTipoNotificacionTelefono;
    /**
     * POJO Tipo Notificación
     */
    private TipoNotificacion tipoNotificacionFilterTelefono;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the adapterPersonaTelefonoContacto
     */
    public PersonaTelefonoAdapter getAdapterPersonaTelefonoContacto() {
        return adapterPersonaTelefonoContacto;
    }

    /**
     * @param adapterPersonaTelefonoContacto the adapterPersonaTelefonoContacto
     * to set
     */
    public void setAdapterPersonaTelefonoContacto(PersonaTelefonoAdapter adapterPersonaTelefonoContacto) {
        this.adapterPersonaTelefonoContacto = adapterPersonaTelefonoContacto;
    }

    /**
     * @return the personaTelefonoContactoFilter
     */
    public PersonaTelefono getPersonaTelefonoContactoFilter() {
        return personaTelefonoContactoFilter;
    }

    /**
     * @param personaTelefonoContactoFilter the personaTelefonoContactoFilter to
     * set
     */
    public void setPersonaTelefonoContactoFilter(PersonaTelefono personaTelefonoContactoFilter) {
        this.personaTelefonoContactoFilter = personaTelefonoContactoFilter;
    }

    /**
     * @return the personaEmailContactoFilter
     */
    public PersonaEmail getPersonaEmailContactoFilter() {
        return personaEmailContactoFilter;
    }

    /**
     * @param personaEmailContactoFilter the personaEmailContactoFilter to set
     */
    public void setPersonaEmailContactoFilter(PersonaEmail personaEmailContactoFilter) {
        this.personaEmailContactoFilter = personaEmailContactoFilter;
    }

    /**
     * @return the adapterPersonaEmailContacto
     */
    public PersonaEmailAdapter getAdapterPersonaEmailContacto() {
        return adapterPersonaEmailContacto;
    }

    /**
     * @param adapterPersonaEmailContacto the adapterPersonaEmailContacto to set
     */
    public void setAdapterPersonaEmailContacto(PersonaEmailAdapter adapterPersonaEmailContacto) {
        this.adapterPersonaEmailContacto = adapterPersonaEmailContacto;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setEmailAgregar(Email emailAgregar) {
        this.emailAgregar = emailAgregar;
    }

    public Email getEmailAgregar() {
        return emailAgregar;
    }

    public void setTipoNotificacionFilterTelefono(TipoNotificacion tipoNotificacionFilterTelefono) {
        this.tipoNotificacionFilterTelefono = tipoNotificacionFilterTelefono;
    }

    public void setAdapterTipoNotificacionTelefono(TipoNotificacionAdapter adapterTipoNotificacionTelefono) {
        this.adapterTipoNotificacionTelefono = adapterTipoNotificacionTelefono;
    }

    public TipoNotificacion getTipoNotificacionFilterTelefono() {
        return tipoNotificacionFilterTelefono;
    }

    public TipoNotificacionAdapter getAdapterTipoNotificacionTelefono() {
        return adapterTipoNotificacionTelefono;
    }

    public List<TipoNotificacion> getListNotificacionTelefono() {
        return listNotificacionTelefono;
    }

    public void setListNotificacionTelefono(List<TipoNotificacion> listNotificacionTelefono) {
        this.listNotificacionTelefono = listNotificacionTelefono;
    }

    public List<TipoNotificacion> getListNotificacionMail() {
        return listNotificacionMail;
    }

    public void setListNotificacionMail(List<TipoNotificacion> listNotificacionMail) {
        this.listNotificacionMail = listNotificacionMail;
    }

    public void setTipoNotificacionFilter(TipoNotificacion tipoNotificacionFilter) {
        this.tipoNotificacionFilter = tipoNotificacionFilter;
    }

    public void setAdapterTipoNotificacion(TipoNotificacionAdapter adapterTipoNotificacion) {
        this.adapterTipoNotificacion = adapterTipoNotificacion;
    }

    public TipoNotificacion getTipoNotificacionFilter() {
        return tipoNotificacionFilter;
    }

    public TipoNotificacionAdapter getAdapterTipoNotificacion() {
        return adapterTipoNotificacion;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setServiceContactoTelefonoNotificacion(ContactoTelefonoNotificacionService serviceContactoTelefonoNotificacion) {
        this.serviceContactoTelefonoNotificacion = serviceContactoTelefonoNotificacion;
    }

    public void setContactoTelefonoNotificacionEdit(ContactoTelefonoNotificacion contactoTelefonoNotificacionEdit) {
        this.contactoTelefonoNotificacionEdit = contactoTelefonoNotificacionEdit;
    }

    public void setContactoTelefonoAsociado(ContactoTelefono contactoTelefonoAsociado) {
        this.contactoTelefonoAsociado = contactoTelefonoAsociado;
    }

    public ContactoTelefonoNotificacionService getServiceContactoTelefonoNotificacion() {
        return serviceContactoTelefonoNotificacion;
    }

    public ContactoTelefonoNotificacion getContactoTelefonoNotificacionEdit() {
        return contactoTelefonoNotificacionEdit;
    }

    public ContactoTelefono getContactoTelefonoAsociado() {
        return contactoTelefonoAsociado;
    }

    public void setContactoEmailAsociado(ContactoEmail contactoEmailAsociado) {
        this.contactoEmailAsociado = contactoEmailAsociado;
    }

    public ContactoEmail getContactoEmailAsociado() {
        return contactoEmailAsociado;
    }

    public void setContactoEmailNotificacionEdit(ContactoEmailNotificacion contactoEmailNotificacionEdit) {
        this.contactoEmailNotificacionEdit = contactoEmailNotificacionEdit;
    }

    public ContactoEmailNotificacion getContactoEmailNotificacionEdit() {
        return contactoEmailNotificacionEdit;
    }

    public void setServiceContactoEmailNotificacion(ContactoEmailNotificacionService serviceContactoEmailNotificacion) {
        this.serviceContactoEmailNotificacion = serviceContactoEmailNotificacion;
    }

    public ContactoEmailNotificacionService getServiceContactoEmailNotificacion() {
        return serviceContactoEmailNotificacion;
    }

    public void setAdapterContactoEmailNotificacionAsociado(ContactoEmailNotificacionAsociadoAdapter adapterContactoEmailNotificacionAsociado) {
        this.adapterContactoEmailNotificacionAsociado = adapterContactoEmailNotificacionAsociado;
    }

    public ContactoEmailNotificacionAsociadoAdapter getAdapterContactoEmailNotificacionAsociado() {
        return adapterContactoEmailNotificacionAsociado;
    }

    public void setServiceTelefonoContacto(TelefonoServiceClient serviceTelefonoContacto) {
        this.serviceTelefonoContacto = serviceTelefonoContacto;
    }

    public TelefonoServiceClient getServiceTelefonoContacto() {
        return serviceTelefonoContacto;
    }

    public void setServiceEmailContacto(EmailServiceClient serviceEmailContacto) {
        this.serviceEmailContacto = serviceEmailContacto;
    }

    public EmailServiceClient getServiceEmailContacto() {
        return serviceEmailContacto;
    }

    public void setIdTelefonoContacto(Integer idTelefonoContacto) {
        this.idTelefonoContacto = idTelefonoContacto;
    }

    public Integer getIdTelefonoContacto() {
        return idTelefonoContacto;
    }

    public void setIdEmailContacto(Integer idEmailContacto) {
        this.idEmailContacto = idEmailContacto;
    }

    public Integer getIdEmailContacto() {
        return idEmailContacto;
    }

    public TipoTelefono getTipoTelefonoContactoFilter() {
        return tipoTelefonoContactoFilter;
    }

    public TipoEmail getTipoEmailContactoFilter() {
        return tipoEmailContactoFilter;
    }

    public void setTipoTelefonoContactoFilter(TipoTelefono tipoTelefonoContactoFilter) {
        this.tipoTelefonoContactoFilter = tipoTelefonoContactoFilter;
    }

    public void setTipoEmailContactoFilter(TipoEmail tipoEmailContactoFilter) {
        this.tipoEmailContactoFilter = tipoEmailContactoFilter;
    }

    public void setAdapterTipoTelefonoContacto(TipoTelefonoListAdapter adapterTipoTelefonoContacto) {
        this.adapterTipoTelefonoContacto = adapterTipoTelefonoContacto;
    }

    public void setAdapterTipoEmailContacto(TipoEmailListAdapter adapterTipoEmailContacto) {
        this.adapterTipoEmailContacto = adapterTipoEmailContacto;
    }

    public TipoTelefonoListAdapter getAdapterTipoTelefonoContacto() {
        return adapterTipoTelefonoContacto;
    }

    public TipoEmailListAdapter getAdapterTipoEmailContacto() {
        return adapterTipoEmailContacto;
    }

    public void setContactoTelefonoService(ContactoTelefonoService contactoTelefonoService) {
        this.contactoTelefonoService = contactoTelefonoService;
    }

    public void setContactoEmailService(ContactoEmailService contactoEmailService) {
        this.contactoEmailService = contactoEmailService;
    }

    public ContactoTelefonoService getContactoTelefonoService() {
        return contactoTelefonoService;
    }

    public ContactoEmailService getContactoEmailService() {
        return contactoEmailService;
    }

    public void setTelefonoContactoCreate(Telefono telefonoContactoCreate) {
        this.telefonoContactoCreate = telefonoContactoCreate;
    }

    public void setTelefonoContactoAsociacion(ContactoTelefono telefonoContactoAsociacion) {
        this.telefonoContactoAsociacion = telefonoContactoAsociacion;
    }

    public void setEmailContactoCreate(Email emailContactoCreate) {
        this.emailContactoCreate = emailContactoCreate;
    }

    public Telefono getTelefonoContactoCreate() {
        return telefonoContactoCreate;
    }

    public ContactoTelefono getTelefonoContactoAsociacion() {
        return telefonoContactoAsociacion;
    }

    public Email getEmailContactoCreate() {
        return emailContactoCreate;
    }

    public void setEmailContactoAsociacion(ContactoEmail emailContactoAsociacion) {
        this.emailContactoAsociacion = emailContactoAsociacion;
    }

    public ContactoEmail getEmailContactoAsociacion() {
        return emailContactoAsociacion;
    }

    public void setServicePersonaContacto(ContactoServiceClient servicePersonaContacto) {
        this.servicePersonaContacto = servicePersonaContacto;
    }

    public ContactoServiceClient getServicePersonaContacto() {
        return servicePersonaContacto;
    }

    public void setPersonaContactoCreate(PersonaContacto personaContactoCreate) {
        this.personaContactoCreate = personaContactoCreate;
    }

    public PersonaContacto getPersonaContactoCreate() {
        return personaContactoCreate;
    }

    public void setContactoCreate(Contacto contactoCreate) {
        this.contactoCreate = contactoCreate;
    }

    public Contacto getContactoCreate() {
        return contactoCreate;
    }

    public void setServicePersona(PersonaServiceClient servicePersona) {
        this.servicePersona = servicePersona;
    }

    public void setPersonaCreate(Persona personaCreate) {
        this.personaCreate = personaCreate;
    }

    public PersonaServiceClient getServicePersona() {
        return servicePersona;
    }

    public Persona getPersonaCreate() {
        return personaCreate;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setServiceContacto(ContactoServiceClient serviceContacto) {
        this.serviceContacto = serviceContacto;
    }

    public ContactoServiceClient getServiceContacto() {
        return serviceContacto;
    }

    public void setServicePersonaTelefono(PersonaTelefonoClient servicePersonaTelefono) {
        this.servicePersonaTelefono = servicePersonaTelefono;
    }

    public void setPersonaTelefonoCreate(PersonaTelefono personaTelefonoCreate) {
        this.personaTelefonoCreate = personaTelefonoCreate;
    }

    public void setTelefonoAgregar(Telefono telefonoAgregar) {
        this.telefonoAgregar = telefonoAgregar;
    }

    public Telefono getTelefonoAgregar() {
        return telefonoAgregar;
    }

    public PersonaTelefonoClient getServicePersonaTelefono() {
        return servicePersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefonoCreate() {
        return personaTelefonoCreate;
    }

    public void setServiceTelefono(TelefonoServiceClient serviceTelefono) {
        this.serviceTelefono = serviceTelefono;
    }

    public TelefonoServiceClient getServiceTelefono() {
        return serviceTelefono;
    }

    public void setTipoTelefonoFilter(TipoTelefono tipoTelefonoFilter) {
        this.tipoTelefonoFilter = tipoTelefonoFilter;
    }

    public void setTelefonoCreate(Telefono telefonoCreate) {
        this.telefonoCreate = telefonoCreate;
    }

    public void setAdapterTipoTelefono(TipoTelefonoListAdapter adapterTipoTelefono) {
        this.adapterTipoTelefono = adapterTipoTelefono;
    }

    public TipoTelefono getTipoTelefonoFilter() {
        return tipoTelefonoFilter;
    }

    public Telefono getTelefonoCreate() {
        return telefonoCreate;
    }

    public TipoTelefonoListAdapter getAdapterTipoTelefono() {
        return adapterTipoTelefono;
    }

    public void setServicePersonaEmail(PersonaEmailClient servicePersonaEmail) {
        this.servicePersonaEmail = servicePersonaEmail;
    }

    public PersonaEmailClient getServicePersonaEmail() {
        return servicePersonaEmail;
    }

    public void setTipoEmailFilter(TipoEmail tipoEmailFilter) {
        this.tipoEmailFilter = tipoEmailFilter;
    }

    public void setEmailCreate(Email emailCreate) {
        this.emailCreate = emailCreate;
    }

    public void setPersonaEmailCreate(PersonaEmail personaEmailCreate) {
        this.personaEmailCreate = personaEmailCreate;
    }

    public PersonaEmail getPersonaEmailCreate() {
        return personaEmailCreate;
    }

    public void setAdapterTipoEmail(TipoEmailListAdapter adapterTipoEmail) {
        this.adapterTipoEmail = adapterTipoEmail;
    }

    public TipoEmail getTipoEmailFilter() {
        return tipoEmailFilter;
    }

    public Email getEmailCreate() {
        return emailCreate;
    }

    public TipoEmailListAdapter getAdapterTipoEmail() {
        return adapterTipoEmail;
    }

    public void setContactoEmailNotificacionFilter(ContactoEmailNotificacion contactoEmailNotificacionFilter) {
        this.contactoEmailNotificacionFilter = contactoEmailNotificacionFilter;
    }

    public void setContactoTelefonoNotificacionFilter(ContactoTelefonoNotificacion contactoTelefonoNotificacionFilter) {
        this.contactoTelefonoNotificacionFilter = contactoTelefonoNotificacionFilter;
    }

    public ContactoEmailNotificacion getContactoEmailNotificacionFilter() {
        return contactoEmailNotificacionFilter;
    }

    public ContactoTelefonoNotificacion getContactoTelefonoNotificacionFilter() {
        return contactoTelefonoNotificacionFilter;
    }

    public void setAdapterContactoTelefonoNotificacion(ContactoTelefonoNotificacionAsociadoAdapter adapterContactoTelefonoNotificacion) {
        this.adapterContactoTelefonoNotificacion = adapterContactoTelefonoNotificacion;
    }

    public ContactoTelefonoNotificacionAsociadoAdapter getAdapterContactoTelefonoNotificacion() {
        return adapterContactoTelefonoNotificacion;
    }

    public void setContactoTelefonoFilter(ContactoTelefono contactoTelefonoFilter) {
        this.contactoTelefonoFilter = contactoTelefonoFilter;
    }

    public void setAdapterContactoTelefono(ContactoTelefonoAdapter adapterContactoTelefono) {
        this.adapterContactoTelefono = adapterContactoTelefono;
    }

    public ContactoTelefono getContactoTelefonoFilter() {
        return contactoTelefonoFilter;
    }

    public ContactoTelefonoAdapter getAdapterContactoTelefono() {
        return adapterContactoTelefono;
    }

    public void setContactoEmailFilter(ContactoEmail contactoEmailFilter) {
        this.contactoEmailFilter = contactoEmailFilter;
    }

    public void setAdapterContactoEmail(ContactoEmailAdapter adapterContactoEmail) {
        this.adapterContactoEmail = adapterContactoEmail;
    }

    public ContactoEmail getContactoEmailFilter() {
        return contactoEmailFilter;
    }

    public ContactoEmailAdapter getAdapterContactoEmail() {
        return adapterContactoEmail;
    }

    public void setTipoContactoFilter(TipoContacto tipoContactoFilter) {
        this.tipoContactoFilter = tipoContactoFilter;
    }

    public void setAdapterTipoContacto(TipoContactoListAdapter adapterTipoContacto) {
        this.adapterTipoContacto = adapterTipoContacto;
    }

    public TipoContacto getTipoContactoFilter() {
        return tipoContactoFilter;
    }

    public TipoContactoListAdapter getAdapterTipoContacto() {
        return adapterTipoContacto;
    }

    public void setContactoFilter(Contacto contactoFilter) {
        this.contactoFilter = contactoFilter;
    }

    public void setAdapterContacto(ContactoListAdapter adapterContacto) {
        this.adapterContacto = adapterContacto;
    }

    public void setAdapterCargo(CargoListAdapter adapterCargo) {
        this.adapterCargo = adapterCargo;
    }

    public Contacto getContactoFilter() {
        return contactoFilter;
    }

    public ContactoListAdapter getAdapterContacto() {
        return adapterContacto;
    }

    public CargoListAdapter getAdapterCargo() {
        return adapterCargo;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setPersonaContactoFilter(PersonaContacto personaContactoFilter) {
        this.personaContactoFilter = personaContactoFilter;
    }

    public PersonaContacto getPersonaContactoFilter() {
        return personaContactoFilter;
    }

    public void setPersonaContactoAdapter(PersonaContactoListAdapter personaContactoAdapter) {
        this.personaContactoAdapter = personaContactoAdapter;
    }

    public PersonaContactoListAdapter getPersonaContactoAdapter() {
        return personaContactoAdapter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefonoFilter() {
        return personaTelefonoFilter;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public void setPersonaTelefonoFilter(PersonaTelefono personaTelefonoFilter) {
        this.personaTelefonoFilter = personaTelefonoFilter;
    }

    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public void setServiceEmail(EmailServiceClient serviceEmail) {
        this.serviceEmail = serviceEmail;
    }

    public EmailServiceClient getServiceEmail() {
        return serviceEmail;
    }

    public void setPersonaEmailFilter(PersonaEmail personaEmailFilter) {
        this.personaEmailFilter = personaEmailFilter;
    }

    public void setAdapterPersonaEmail(PersonaEmailAdapter adapterPersonaEmail) {
        this.adapterPersonaEmail = adapterPersonaEmail;
    }

    public PersonaEmail getPersonaEmailFilter() {
        return personaEmailFilter;
    }

    public PersonaEmailAdapter getAdapterPersonaEmail() {
        return adapterPersonaEmail;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public Cargo getCargoFilter() {
        return cargoFilter;
    }

    public void setCargoFilter(Cargo cargoFilter) {
        this.cargoFilter = cargoFilter;
    }

//</editor-fold>
    /**
     * Método para el listado de Cliente
     */
    public void filterCliente() {
        clienteFilter.setIdCliente(Integer.parseInt(idCliente));
        adapterCliente = adapterCliente.fillData(clienteFilter);

        clienteFilter.setNroDocumento(adapterCliente.getData().get(0).getNroDocumento());
        clienteFilter.setRazonSocial(adapterCliente.getData().get(0).getRazonSocial());
    }

    /**
     * Método para el listado de PersonaEmail
     */
    public void filterPersonaEmail() {
        personaEmailFilter.setIdPersona(Integer.parseInt(idCliente));
        personaEmailFilter.setActivo("S");
        adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmailFilter);
    }

    /**
     * Método para el listado de PersonaTelefono
     */
    public void filterPersonaTelefono() {
        personaTelefonoFilter.setIdPersona(Integer.parseInt(idCliente));
        personaTelefonoFilter.setActivo("S");
        adapterPersonaTelefono = adapterPersonaTelefono.fillData(personaTelefonoFilter);
    }

    /**
     * Método para el listado de PersonaContacto
     */
    public void filterPersonaContacto() {
        personaContactoFilter.setIdPersona(Integer.parseInt(idCliente));
        personaContactoAdapter = personaContactoAdapter.fillData(personaContactoFilter);

    }

    /**
     * Método para encontrar la información del contacto
     *
     * @param event
     */
    public void onRowToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Row State " + event.getVisibility(),
                "Contacto:" + ((PersonaContacto) event.getData()).getIdContacto());

        FacesContext.getCurrentInstance().addMessage(null, msg);

        id = ((PersonaContacto) event.getData()).getIdContacto();

        filterContacto();
    }

    /**
     * Método para filtrar el contacto
     */
    public void filterContacto() {

        contactoFilter = new Contacto();

        contactoFilter.setIdContacto(id);

        filterContactoTelefono();
        filterContactoEmail();

    }

    /**
     * Método para filtrar ContactoEmail
     */
    public void filterContactoEmail() {
        personaEmailContactoFilter = new PersonaEmail();
        personaEmailContactoFilter.setIdPersona(id);

        adapterPersonaEmailContacto = adapterPersonaEmailContacto.fillData(personaEmailContactoFilter);

    }

    /**
     * Método para filtar ContactoTelefono
     */
    public void filterContactoTelefono() {

        personaTelefonoContactoFilter = new PersonaTelefono();
        personaTelefonoContactoFilter.setIdPersona(id);

        adapterPersonaTelefonoContacto = adapterPersonaTelefonoContacto.fillData(personaTelefonoContactoFilter);
    }

    /**
     * Método para filtrar tipo contacto
     */
    public void filterTipoContacto() {
        tipoContactoFilter = new TipoContacto();
        adapterTipoContacto = adapterTipoContacto.fillData(tipoContactoFilter);
    }

    /**
     * Método para filtrar cargo
     */
    public void filterCargo() {
        cargoFilter = new Cargo();
        adapterCargo = adapterCargo.fillData(cargoFilter);
    }

    /**
     * Listado de Contacto Telefono Notificación
     *
     * @param contactoTelefono
     */
    public void filterContactoTelefonoNotificacion(ContactoTelefono contactoTelefono) {
        contactoTelefonoNotificacionFilter = new ContactoTelefonoNotificacion();

        contactoTelefonoNotificacionFilter.setIdContacto(contactoTelefono.getIdContacto());
        contactoTelefonoNotificacionFilter.setIdTelefono(contactoTelefono.getIdTelefono());

        adapterContactoTelefonoNotificacion = adapterContactoTelefonoNotificacion.fillData(contactoTelefonoNotificacionFilter);

    }

    /**
     * Método para filtar el contacto email notificación
     *
     * @param contactoEmail
     */
    public void filterContactoEmailNotificacion(ContactoEmail contactoEmail) {

        contactoEmailNotificacionFilter = new ContactoEmailNotificacion();

        contactoEmailNotificacionFilter.setIdContacto(contactoEmail.getIdContacto());
        contactoEmailNotificacionFilter.setIdEmail(contactoEmail.getIdEmail());

        adapterContactoEmailNotificacionAsociado = adapterContactoEmailNotificacionAsociado.fillData(contactoEmailNotificacionFilter);

    }

    /**
     * Método para la lista de tipo email
     */
    public void filterTipoEmail() {
        adapterTipoEmail = adapterTipoEmail.fillData(tipoEmailFilter);
    }

    public void filterTipoNotificacionEmail() {
        tipoNotificacionFilter = new TipoNotificacion();
        tipoNotificacionFilter.setEmail("S");

        adapterTipoNotificacion = adapterTipoNotificacion.fillData(tipoNotificacionFilter);

    }

    public void filterTipoNotificacionTelefono() {
        tipoNotificacionFilterTelefono = new TipoNotificacion();
        tipoNotificacionFilterTelefono.setTelefono("S");

        adapterTipoNotificacionTelefono = adapterTipoNotificacionTelefono.fillData(tipoNotificacionFilterTelefono);

    }

    /**
     * Método para crear contacto persona
     */
    public void createContact() {
        Integer idPersona = null;
        Integer idContacto = null;
        personaCreate.setIdTipoPersona(1);
        personaCreate.setActivo("S");

        //idPersona = servicePersona.setPersona(personaCreate);
        contactoCreate.setIdContacto(idPersona);
        contactoCreate.setActivo("S");

        //idContacto = serviceContacto.setContacto(contactoCreate);
        personaContactoCreate = new PersonaContacto();

        personaContactoCreate.setIdPersona(Integer.parseInt(idCliente));
        personaContactoCreate.setIdContacto(idContacto);

        personaContactoCreate = servicePersonaContacto.createPersonaContacto(personaContactoCreate);

        if (idEmailContacto != null) {
            emailContactoAsociacion.setIdEmail(idEmailContacto);
            emailContactoAsociacion.setIdContacto(idContacto);

            emailContactoAsociacion = contactoEmailService.createContactoEmail(emailContactoAsociacion);

            if (emailContactoAsociacion.getIdEmail() == null && emailContactoAsociacion.getIdContacto() == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el email!"));
            }
        }

        if (idTelefonoContacto != null) {
            telefonoContactoAsociacion.setIdTelefono(idTelefonoContacto);
            telefonoContactoAsociacion.setIdContacto(idContacto);

            telefonoContactoAsociacion = contactoTelefonoService.createContactoTelefono(telefonoContactoAsociacion);

            if (telefonoContactoAsociacion.getIdTelefono() == null && telefonoContactoAsociacion.getIdContacto() == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el teléfono!"));
            }
        }

        if (personaContactoCreate.getIdPersona() != null && personaContactoCreate.getIdContacto() != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contacto agregado correctamente!"));
            filterPersonaContacto();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el contacto!"));
        }

    }

    /**
     * Método para crear email y asociarlo al cliente
     */
    public void createEmail() {

        personaEmailCreate = new PersonaEmail();

        emailCreate.setActivo("S");
        personaEmailCreate.setIdPersona(Integer.parseInt(idCliente));
        personaEmailCreate.setEmail(emailCreate);
        personaEmailCreate.setActivo("S");

        BodyResponse response = servicePersonaEmail.setPersonaEmail(personaEmailCreate);

        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Email agregado correctamente!"));
            filterPersonaEmail();
            emailCreate = new Email();
        }

    }

    /**
     * Método para crear teléfono y asociarlo al cliente
     */
    public void createTelefono() {

        personaTelefonoCreate = new PersonaTelefono();

        telefonoCreate.setActivo("S");
        personaTelefonoCreate.setIdPersona(Integer.parseInt(idCliente));
        personaTelefonoCreate.setTelefono(telefonoCreate);
        personaTelefonoCreate.setActivo("S");

        BodyResponse response = servicePersonaTelefono.setTelefonoPersona(personaTelefonoCreate);

        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Teléfono agregado correctamente!"));
            filterPersonaTelefono();
            telefonoCreate = new Telefono();
        }
    }

    /**
     * Método para guardar el contacto
     *
     * @param contactoEmail
     */
    public void saveContact(Integer id) {
        idContacto = id;

    }

    /**
     * Método para crear email y asociarlo al contacto
     */
    public void agregarEmailContacto() {
        Email email;
        emailContactoAsociacion = new ContactoEmail();

        email = serviceEmail.setEmail(emailAgregar);

        emailContactoAsociacion.setIdEmail(email.getId());
        emailContactoAsociacion.setIdContacto(idContacto);

        emailContactoAsociacion = contactoEmailService.createContactoEmail(emailContactoAsociacion);

        if (!listNotificacionMail.isEmpty()) {
            for (int i = 0; i < listNotificacionMail.size(); i++) {
                contactoEmailNotificacionEdit = new ContactoEmailNotificacion();

                contactoEmailNotificacionEdit.setIdEmail(email.getId());
                contactoEmailNotificacionEdit.setIdContacto(idContacto);
                contactoEmailNotificacionEdit.setIdTipoNotificacion(listNotificacionMail.get(i).getId());
                contactoEmailNotificacionEdit.setEstado("A");

                contactoEmailNotificacionEdit = serviceContactoEmailNotificacion.editContactoEmailNotificacion(contactoEmailNotificacionEdit);

            }
        }

        if (emailContactoAsociacion.getIdContacto() != null && emailContactoAsociacion.getIdEmail() != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Email agregado correctamente!"));
            clearEmail();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el email!"));
        }

    }

    public void clearEmail() {
        emailAgregar = new Email();
        listNotificacionMail = new ArrayList<>();

    }

    /**
     * Método para crear teléfono y asociarlo al contacto
     */
    public void agregarTelefono() {
        Integer idTelefono = null;

        telefonoContactoAsociacion = new ContactoTelefono();

        idTelefono = serviceTelefono.setTelefono(telefonoAgregar);

        telefonoContactoAsociacion.setIdTelefono(idTelefono);
        telefonoContactoAsociacion.setIdContacto(idContacto);

        telefonoContactoAsociacion = contactoTelefonoService.createContactoTelefono(telefonoContactoAsociacion);

        if (!listNotificacionTelefono.isEmpty()) {
            for (int i = 0; i < listNotificacionTelefono.size(); i++) {
                contactoTelefonoNotificacionEdit = new ContactoTelefonoNotificacion();

                contactoTelefonoNotificacionEdit.setIdTelefono(idTelefono);
                contactoTelefonoNotificacionEdit.setIdContacto(idContacto);
                contactoTelefonoNotificacionEdit.setIdTipoNotificacion(listNotificacionTelefono.get(i).getId());
                contactoTelefonoNotificacionEdit.setEstado("A");

                contactoTelefonoNotificacionEdit = serviceContactoTelefonoNotificacion.editContactoTelefonoNotificacion(contactoTelefonoNotificacionEdit);

            }
        }

        if (telefonoContactoAsociacion.getIdContacto() != null && telefonoContactoAsociacion.getIdTelefono() != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Teléfono agregado correctamente!"));
            clearTelefono();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el teléfono!"));
        }
    }

    public void clearTelefono() {
        telefonoCreate = new Telefono();
        listNotificacionTelefono = new ArrayList<>();

    }

    /**
     * Método para eliminar email del cliente
     *
     * @param idPersona
     * @param idEmail
     */
    public void deteleClienteEmail(Integer idPersona, Integer idEmail) {

        PersonaEmail personaEmailDelete = new PersonaEmail();

        personaEmailDelete.setIdEmail(idEmail);
        personaEmailDelete.setIdPersona(idPersona);
        personaEmailDelete.setActivo("N");
        BodyResponse d = servicePersonaEmail.editPersonaEmail(personaEmailDelete);

        if (d.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Email eliminado correctamente!"));
            filterPersonaEmail();
        }

    }

    /**
     * Método para eliminar telefono del cliente
     *
     * @param idPersona
     * @param idTelefono
     */
    public void deteleClienteTelefono(Integer idPersona, Integer idTelefono) {

        PersonaTelefono personaTelefonoDelete = new PersonaTelefono();

        personaTelefonoDelete.setIdTelefono(idTelefono);
        personaTelefonoDelete.setIdPersona(idPersona);
        personaTelefonoDelete.setActivo("N");
        BodyResponse d = servicePersonaTelefono.putTelefonoPersona(personaTelefonoDelete);

        if (d.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Telefono eliminado correctamente!"));
            filterPersonaTelefono();
        }

    }

    /**
     * Método para filtrar tipos de teléfono
     */
    public void filterTipoTelefono() {
        adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefonoFilter);
    }

    /**
     * Método para cambiar el estado del contacto
     *
     * @param personaContactoAux
     */
    public void onChangeStatusContact(PersonaContacto personaContactoAux) {

        Contacto contacto = new Contacto();

        switch (personaContactoAux.getContacto().getActivo()) {
            case "S":
                contacto.setActivo("N");
                break;
            case "N":
                contacto.setActivo("S");
                break;
            default:
                break;
        }

        contacto.setIdContacto(personaContactoAux.getIdContacto());
        contacto.setIdTipoContacto(personaContactoAux.getContacto().getIdTipoContacto());
        contacto.setIdCargo(personaContactoAux.getContacto().getIdCargo());

        BodyResponse response = serviceContacto.editContacto(contacto);

        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio de estado correctamente!"));
            filterPersonaContacto();
        }
    }

    /**
     * Método para mostrar panel
     */
    public void showPanel() {
        show = true;
    }

    /**
     * Método para esconder el panel
     */
    public void hidePanel() {
        show = false;
    }

    /**
     * Método para filtrar los tipos de contacto
     */
    public void filterTiposContacto() {
        adapterTipoTelefonoContacto = adapterTipoTelefonoContacto.fillData(tipoTelefonoContactoFilter);
        adapterTipoEmailContacto = adapterTipoEmailContacto.fillData(tipoEmailContactoFilter);
    }

    /**
     * Método para crear teléfono para el contacto
     */
    public void createTelefonoContacto() {

        idTelefonoContacto = serviceTelefonoContacto.setTelefono(telefonoContactoCreate);

        if (idTelefonoContacto != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Teléfono agregado correctamente!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el teléfono!"));
        }
    }

    /**
     * Método para crear email para contacto
     */
    public void createEmailContacto() {

        Email email = serviceEmailContacto.setEmail(emailContactoCreate);
        idEmailContacto = email.getId();

        if (idEmailContacto != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Email agregado correctamente!"));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al agregar el email!"));
        }
    }

    /**
     * Metodo para editar contacto email notificacion
     *
     * @param contactoEmailNotificacion
     */
    public void editContactoEmailNotificacionAsociado(ContactoEmailNotificacion contactoEmailNotificacion) {
        contactoEmailNotificacionEdit = new ContactoEmailNotificacion();

        if (contactoEmailNotificacion.getEstado().equals("I")) {
            contactoEmailNotificacionEdit.setIdEmail(contactoEmailNotificacionFilter.getIdEmail());
            contactoEmailNotificacionEdit.setIdContacto(contactoEmailNotificacionFilter.getIdContacto());
            contactoEmailNotificacionEdit.setIdTipoNotificacion(contactoEmailNotificacion.getIdTipoNotificacion());
            contactoEmailNotificacionEdit.setEstado("A");

            contactoEmailNotificacionEdit = serviceContactoEmailNotificacion.editContactoEmailNotificacion(contactoEmailNotificacionEdit);

            if (contactoEmailNotificacionEdit != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Notificación activada correctamente!"));
                contactoEmailAsociado = new ContactoEmail();
                contactoEmailAsociado.setIdEmail(contactoEmailNotificacionEdit.getIdEmail());
                contactoEmailAsociado.setIdContacto(contactoEmailNotificacionEdit.getIdContacto());
                filterContactoEmailNotificacion(contactoEmailAsociado);
            }

        } else {
            contactoEmailNotificacionEdit.setIdEmail(contactoEmailNotificacionFilter.getIdEmail());
            contactoEmailNotificacionEdit.setIdContacto(contactoEmailNotificacionFilter.getIdContacto());
            contactoEmailNotificacionEdit.setIdTipoNotificacion(contactoEmailNotificacion.getIdTipoNotificacion());
            contactoEmailNotificacionEdit.setEstado("I");

            contactoEmailNotificacionEdit = serviceContactoEmailNotificacion.editContactoEmailNotificacion(contactoEmailNotificacionEdit);

            if (contactoEmailNotificacionEdit != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Notificación desactivada correctamente!"));
                contactoEmailAsociado = new ContactoEmail();
                contactoEmailAsociado.setIdEmail(contactoEmailNotificacionEdit.getIdEmail());
                contactoEmailAsociado.setIdContacto(contactoEmailNotificacionEdit.getIdContacto());
                filterContactoEmailNotificacion(contactoEmailAsociado);
            }
        }
    }

    /**
     * Método para editar contacto telefono notificacion
     *
     * @param contactoTelefonoNotificacion
     */
    public void editContactoTelefonoNotificacionAsociado(ContactoTelefonoNotificacion contactoTelefonoNotificacion) {
        this.contactoTelefonoNotificacionEdit = new ContactoTelefonoNotificacion();

        if (contactoTelefonoNotificacion.getEstado().equals("I")) {
            contactoTelefonoNotificacionEdit.setIdTelefono(contactoTelefonoNotificacionFilter.getIdTelefono());
            contactoTelefonoNotificacionEdit.setIdContacto(contactoTelefonoNotificacionFilter.getIdContacto());
            contactoTelefonoNotificacionEdit.setIdTipoNotificacion(contactoTelefonoNotificacion.getIdTipoNotificacion());
            contactoTelefonoNotificacionEdit.setEstado("A");

            contactoTelefonoNotificacionEdit = serviceContactoTelefonoNotificacion.editContactoTelefonoNotificacion(contactoTelefonoNotificacionEdit);

            if (contactoTelefonoNotificacionEdit != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Notificación activada correctamente!"));
                contactoTelefonoAsociado = new ContactoTelefono();
                contactoTelefonoAsociado.setIdTelefono(contactoTelefonoNotificacionEdit.getIdTelefono());
                contactoTelefonoAsociado.setIdContacto(contactoTelefonoNotificacionEdit.getIdContacto());

                filterContactoTelefonoNotificacion(contactoTelefonoAsociado);
            }

        } else {
            contactoTelefonoNotificacionEdit.setIdTelefono(contactoTelefonoNotificacionFilter.getIdTelefono());
            contactoTelefonoNotificacionEdit.setIdContacto(contactoTelefonoNotificacionFilter.getIdContacto());
            contactoTelefonoNotificacionEdit.setIdTipoNotificacion(contactoTelefonoNotificacion.getIdTipoNotificacion());
            contactoTelefonoNotificacionEdit.setEstado("I");

            contactoTelefonoNotificacionEdit = serviceContactoTelefonoNotificacion.editContactoTelefonoNotificacion(contactoTelefonoNotificacionEdit);

            if (contactoTelefonoNotificacionEdit != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Notificación desactivada correctamente!"));
                contactoTelefonoAsociado = new ContactoTelefono();
                contactoTelefonoAsociado.setIdTelefono(contactoTelefonoNotificacionEdit.getIdTelefono());
                contactoTelefonoAsociado.setIdContacto(contactoTelefonoNotificacionEdit.getIdContacto());

                filterContactoTelefonoNotificacion(contactoTelefonoAsociado);
            }
        }
    }

    /**
     * Método para editar telefono
     *
     * @param event
     */
    public void onRowEditTelefono(RowEditEvent event) {
        Telefono telefono = new Telefono();
        telefono.setId(((PersonaTelefono) event.getObject()).getTelefono().getId());
        telefono.setIdTipoTelefono(((PersonaTelefono) event.getObject()).getTelefono().getTipoTelefono().getId());
        telefono.setPrefijo(((PersonaTelefono) event.getObject()).getTelefono().getPrefijo());
        telefono.setNumero(((PersonaTelefono) event.getObject()).getTelefono().getNumero());
        telefono.setPrincipal(((PersonaTelefono) event.getObject()).getTelefono().getPrincipal());

        BodyResponse response = serviceTelefono.putTelefono(telefono);
        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Teléfono editado correctamente!"));
            filterPersonaEmail();
        }
    }

    /**
     * Método para editar el cobro
     *
     * @param event
     */
    public void onRowEditEmail(RowEditEvent event) {

        Email emailEdit = new Email();
        emailEdit.setId(((PersonaEmail) event.getObject()).getIdEmail());
        emailEdit.setEmail(((PersonaEmail) event.getObject()).getEmail().getEmail());
        emailEdit.setActivo(((PersonaEmail) event.getObject()).getEmail().getActivo());
        emailEdit.setIdTipoEmail(((PersonaEmail) event.getObject()).getEmail().getIdTipoEmail());
        emailEdit.setPrincipal(((PersonaEmail) event.getObject()).getEmail().getPrincipal());
        BodyResponse d = serviceEmail.putEmail(emailEdit);

        if (d.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Email editado correctamente!"));
            filterPersonaEmail();
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.adapterCliente = new ClientListAdapter();
        this.clienteFilter = new Cliente();
        this.adapterPersonaEmail = new PersonaEmailAdapter();
        this.personaEmailFilter = new PersonaEmail();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.personaTelefonoFilter = new PersonaTelefono();
        this.personaContactoAdapter = new PersonaContactoListAdapter();
        this.personaContactoFilter = new PersonaContacto();
        this.adapterPersona = new PersonaListAdapter();
        this.adapterContacto = new ContactoListAdapter();
        this.adapterCargo = new CargoListAdapter();
        this.adapterTipoContacto = new TipoContactoListAdapter();
        this.adapterContactoEmail = new ContactoEmailAdapter();
        this.adapterContactoTelefono = new ContactoTelefonoAdapter();
        this.adapterContactoTelefonoNotificacion = new ContactoTelefonoNotificacionAsociadoAdapter();
        this.adapterTipoEmail = new TipoEmailListAdapter();
        this.tipoEmailFilter = new TipoEmail();
        this.emailCreate = new Email();
        this.emailAgregar = new Email();
        this.serviceEmail = new EmailServiceClient();
        this.servicePersonaEmail = new PersonaEmailClient();
        this.adapterTipoTelefono = new TipoTelefonoListAdapter();
        this.tipoTelefonoFilter = new TipoTelefono();
        this.telefonoCreate = new Telefono();
        this.telefonoAgregar = new Telefono();
        this.serviceTelefono = new TelefonoServiceClient();
        this.servicePersonaTelefono = new PersonaTelefonoClient();
        this.serviceContacto = new ContactoServiceClient();
        this.show = false;
        this.servicePersona = new PersonaServiceClient();
        this.personaCreate = new Persona();
        this.contactoCreate = new Contacto();
        this.servicePersonaContacto = new ContactoServiceClient();
        this.emailContactoCreate = new Email();
        this.telefonoContactoCreate = new Telefono();
        this.emailContactoAsociacion = new ContactoEmail();
        this.telefonoContactoAsociacion = new ContactoTelefono();
        this.contactoEmailService = new ContactoEmailService();
        this.contactoTelefonoService = new ContactoTelefonoService();
        this.tipoEmailContactoFilter = new TipoEmail();
        this.tipoTelefonoContactoFilter = new TipoTelefono();
        this.adapterTipoTelefonoContacto = new TipoTelefonoListAdapter();
        this.adapterTipoEmailContacto = new TipoEmailListAdapter();
        this.serviceEmailContacto = new EmailServiceClient();
        this.serviceTelefonoContacto = new TelefonoServiceClient();
        this.contactoEmailNotificacionFilter = new ContactoEmailNotificacion();
        this.adapterContactoEmailNotificacionAsociado = new ContactoEmailNotificacionAsociadoAdapter();
        this.serviceContactoEmailNotificacion = new ContactoEmailNotificacionService();
        this.serviceContactoTelefonoNotificacion = new ContactoTelefonoNotificacionService();
        this.adapterTipoNotificacion = new TipoNotificacionAdapter();
        this.adapterTipoNotificacionTelefono = new TipoNotificacionAdapter();
        this.adapterPersonaEmailContacto = new PersonaEmailAdapter();
        this.personaEmailContactoFilter = new PersonaEmail();
        this.personaTelefonoContactoFilter = new PersonaTelefono();
        this.adapterPersonaTelefonoContacto = new PersonaTelefonoAdapter();

        filterCliente();
        filterPersonaEmail();
        filterPersonaTelefono();
        filterPersonaContacto();
        filterTipoEmail();
        filterTipoTelefono();
        /*filterTipoContacto();
        filterCargo();

        filterTiposContacto();
        filterTipoNotificacionEmail();
        filterTipoNotificacionTelefono();*/

    }

    /**
     * Constructor de la clase
     */
    public ContactController() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();

        idCliente = (String) myRequest.getParameter("idCliente");

    }

}
