package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionProcesamientoService;

/**
 * Adaptador de la lista de factura procesamiento
 *
 * @author Williams Vera
 */
public class NotaRemisionProcesamientoAdapter extends DataListAdapter<DocumentoProcesamiento> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final NotaRemisionProcesamientoService serviceNR;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaRemisionProcesamientoAdapter fillData(DocumentoProcesamiento searchData) {

        return serviceNR.getNRProcesamientoList(searchData, getFirstResult(), getPageSize()); 
    }

    /**
     * Constructor de NotaRemisionProcesamientoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaRemisionProcesamientoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNR = new NotaRemisionProcesamientoService();
    }

    /**
     * Constructor de NotaRemisionProcesamientoAdapter
     */
    public NotaRemisionProcesamientoAdapter() {
        super();
        this.serviceNR = new NotaRemisionProcesamientoService();
    }
}
