
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoTelefonoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de contacto email
 * @author Romina Núñez
 */
public class ContactoTelefonoService extends APIErpCore{
    
      /**
     * Obtiene la lista de contacto
     *
     * @param contactoTelefono  
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoTelefonoAdapter getContactoEmailList(ContactoTelefono contactoTelefono, Integer page,
            Integer pageSize) {
       
        ContactoTelefonoAdapter lista = new ContactoTelefonoAdapter();

        Map params = ContactoTelefonoFilter.build(contactoTelefono, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.CONTACTO_TELEFONO.url, 
                ContentType.JSON, params);
  
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoTelefonoAdapter.class);

            lista = (ContactoTelefonoAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }
    
    
    /**
     * Método para crear contacto telefono
     *
     * @param contactoTelefono 
     * @return
     */
    public ContactoTelefono createContactoTelefono(ContactoTelefono contactoTelefono) {

        ContactoTelefono cTelefono = new ContactoTelefono();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CONTACTO_TELEFONO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(contactoTelefono));

            response = BodyResponse.createInstance(conn, ContactoTelefono.class);

            cTelefono = ((ContactoTelefono) response.getPayload());

        }
        return cTelefono;
    }
    
     /**
     * Constructor de ContactoEmailService
     */
    public ContactoTelefonoService() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CONTACTO_TELEFONO("Servicio Contacto Telefono", "contacto-telefono");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
