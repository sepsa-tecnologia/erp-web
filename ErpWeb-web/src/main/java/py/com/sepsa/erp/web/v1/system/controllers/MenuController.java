package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.pojos.MenuPerfil;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;

/**
 * Controlador para listar el menu de otros sistemas
 *
 * @author Cristina Insfrán, Romina Núñez
 */
@SessionScoped
@Named("menuData")
public class MenuController implements Serializable {

    /**
     * Adaptador de la lista de menu
     */
    private MenuListAdapter menuAdapter;
    /**
     * Objeto Menu
     */

    private Menu menu;

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;

    /**
     * @return the menuAdapter
     */
    public MenuListAdapter getMenuAdapter() {
        return menuAdapter;
    }

    /**
     * @param menuAdapter the menuAdapter to set
     */
    public void setMenuAdapter(MenuListAdapter menuAdapter) {
        this.menuAdapter = menuAdapter;
    }

    /**
     * @return the menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String obtenerUrlVistaLocal(String contextPath, String url) {
        return String.format("%s%s", contextPath, url);
    }

    /**
     * Método para obtener la dirección
     *
     * @param idMenu
     * @return
     */
    public String menuDinamicoURL(Integer idMenu) {
        WebLogger.get().debug("entra");
        return String.format("template-dinamico"
                + "?faces-redirect=true"
                + "&idMenu=%d", idMenu);
    }

    /**
     * Método para listar el menu
     */
    public void obtenerListMenu() {
        menu.setTienePadre("false");
        menu.setTieneHijo("true");
        menu.setActivo("S");
        menuAdapter = menuAdapter.fillData(menu);
    }

    /**
     * Inicializa los datos del controlador
     *
     * @param idEmpresa
     */
    @PostConstruct
    public void init() {
        this.menuAdapter = new MenuListAdapter();
        this.menu = new Menu();
        this.menu.setIdEmpresa(session.getUser().getIdEmpresa());

        obtenerListMenu();

    }

    public boolean obtenerPermisoPerfil(List<MenuPerfil> menuPerfiles) {
        boolean rol = false;

        List<String> perfilesDeUsuario = session.getUser().getProfiles();

        for (MenuPerfil menuPerfile : menuPerfiles) {
            if (menuPerfile.getActivo().equals("S")) {
                for (String perfilUsuario : perfilesDeUsuario) {
                    if (menuPerfile.getPerfil().getCodigo().equals(perfilUsuario)) {
                        rol = true;
                    }
                }
            }

        }

        return rol;

    }

    /**
     * Constructor de la clase
     */
    public MenuController() {

    }
}
