/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.remote.LoteService;

/**
 *
 * @author Antonella Lucero
 */
public class LoteAdapter extends DataListAdapter<Lote>{
    private final LoteService serviceProcesamientoLote;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public LoteAdapter fillData(Lote searchData) {
     
        return serviceProcesamientoLote.getLoteList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de LoteAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public LoteAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceProcesamientoLote = new LoteService();
    }

    /**
     * Constructor de LoteAdapter
     */
    public LoteAdapter() {
        super();
        this.serviceProcesamientoLote = new LoteService();
    }
}
