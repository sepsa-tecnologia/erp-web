/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;

/**
 * Controlador para la vista motivo
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoList")
public class MotivoListController implements Serializable {

    /**
     * POJO de Motivo
     */
    private Motivo motivoFilter;
    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoAdapterList;
    /**
     * POJO de tipo motivo
     */
    private TipoMotivo tipoMotivoFilter;
    /**
     * Adaptador para la lista de tipo motivos
     */
    private TipoMotivoAdapter tipoMotivoAdapterList;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoAdapter getMotivoAdapterList() {
        return motivoAdapterList;
    }

    public void setMotivoAdapterList(MotivoAdapter motivoAdapterList) {
        this.motivoAdapterList = motivoAdapterList;
    }

    public Motivo getMotivoFilter() {
        return motivoFilter;
    }

    public void setMotivoFilter(Motivo motivoFilter) {
        this.motivoFilter = motivoFilter;
    }

    public TipoMotivo getTipoMotivoFilter() {
        return tipoMotivoFilter;
    }

    public void setTipoMotivoFilter(TipoMotivo tipoMotivoFilter) {
        this.tipoMotivoFilter = tipoMotivoFilter;
    }

    public TipoMotivoAdapter getTipoMotivoAdapterList() {
        return tipoMotivoAdapterList;
    }

    public void setTipoMotivoAdapterList(TipoMotivoAdapter tipoMotivoAdapterList) {
        this.tipoMotivoAdapterList = tipoMotivoAdapterList;
    }

    //</editor-fold>
    /**
     * Método para obtener motivo
     */
    public void getMotivos() {
        getMotivoAdapterList().setFirstResult(0);
        this.motivoAdapterList = this.motivoAdapterList.fillData(motivoFilter);
    }

    /**
     * Método para obtener tipo motivo
     */
    public void getTipoMotivo() {
        getTipoMotivoAdapterList().setFirstResult(0);
        this.tipoMotivoAdapterList = this.tipoMotivoAdapterList.fillData(tipoMotivoFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.motivoFilter = new Motivo();
        this.tipoMotivoFilter = new TipoMotivo();
        getMotivos();
        getTipoMotivo();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("motivo-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivoFilter = new Motivo();
        this.motivoAdapterList = new MotivoAdapter();
        this.tipoMotivoFilter = new TipoMotivo();
        this.tipoMotivoAdapterList = new TipoMotivoAdapter();
        getMotivos();
        getTipoMotivo();
    }

    /**
     * Constructor
     */
    public MotivoListController() {
        init();
    }

}
