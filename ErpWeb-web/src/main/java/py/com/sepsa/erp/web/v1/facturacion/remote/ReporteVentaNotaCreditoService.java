package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.filters.ReporteVentaNotaCreditoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteNotaCreditoVentaParam;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteVentaNotaCredito;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para reporte de venta nota credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ReporteVentaNotaCreditoService extends APIErpFacturacion {

    /**
     * Método para obtener reporte de venta
     *
     * @param venta
     * @return
     */
    public byte[] getReporteVenta(ReporteVentaNotaCredito venta) {

        byte[] result = null;

        Map params = ReporteVentaNotaCreditoFilter.build(venta);

        String service = String.format(Resource.REPORTE_VENTA_NOTA_CREDITO.url);

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }
    
    public byte[] getReporteComprobanteNcVenta(ReporteComprobanteNotaCreditoVentaParam venta) {

        byte[] result = null;
        Map params = new HashMap<>();
        if (venta.getIdCliente() != null) {
            params.put("IdCliente", venta.getIdCliente());
        }
        Date fecha = venta.getFecha();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String fecha_ = format.format(fecha);
        String anual_ = String.valueOf(venta.getAnual());
        String service
                = String.format(Resource.REPORTE_COMPROBANTE_NC_VENTA.url, fecha_, anual_);

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Recursos de conexión o servicios del APISepsaSet
     */
    public enum Resource {

        //Servicios
        REPORTE_VENTA_NOTA_CREDITO("Reporte de venta nota credito",
            "nota-credito/reporte-venta"),
        REPORTE_COMPROBANTE_NC_VENTA("Reporte de comprobante de venta de nc",
            "nota-credito/comprobante-venta/%s/%s");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
