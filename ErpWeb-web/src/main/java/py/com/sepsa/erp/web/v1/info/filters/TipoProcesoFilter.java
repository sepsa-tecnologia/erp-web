/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;

/**
 * filter para tipo proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoProcesoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoProcesoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public TipoProcesoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoProcesoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoProceso datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoProceso tipoProceso, Integer page, Integer pageSize) {
        TipoProcesoFilter filter = new TipoProcesoFilter();

        filter
                .id(tipoProceso.getId())
                .descripcion(tipoProceso.getDescripcion())
                .codigo(tipoProceso.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
}
