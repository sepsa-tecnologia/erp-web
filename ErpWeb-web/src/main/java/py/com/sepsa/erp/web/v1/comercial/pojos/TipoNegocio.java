
package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO de tipo negocio
 * @author Cristina Insfrán
 */
public class TipoNegocio {

    /**
     * Identificador del tipo de negocio
     */
    private Integer id;
    /**
     * Descripción del tipo de negocio
     */
    private String descripcion;
       /**
     * Bandera que indica si el tipo de negocio esta seleccionado 
     */
    private Boolean selected = false;
    
    /**
     * Bandera que indica si el tipo de negocio es principal
     */
    private Boolean tnPrincipal = false;
    
     /**
     * @return the selected
     */
    public Boolean getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the tnPrincipal
     */
    public Boolean getTnPrincipal() {
        return tnPrincipal;
    }

    /**
     * @param tnPrincipal the tnPrincipal to set
     */
    public void setTnPrincipal(Boolean tnPrincipal) {
        this.tnPrincipal = tnPrincipal;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Contructor de la clase
     */
    public TipoNegocio(){
        
    }
    
    /**
     * Constructor de la clase
     * @param id
     * @param descripcion
     */
    public TipoNegocio(Integer id, String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }
}
