package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MarcaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;

/**
 * Controlador para lista de Marca
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("marcaList")
public class MarcaListController implements Serializable {

    /**
     * Pojo de Marca
     */
    private Marca marcaFilter;

    /**
     * Adaptador para listado de Marca
     */
    private MarcaAdapter adapter;

    /**
     * Pojo para empresa
     */
    private Empresa empresaComplete;

    /**
     * Adapter para empresa
     */
    private EmpresaAdapter adapterEmpresa;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Marca getMarcaFilter() {
        return marcaFilter;
    }

    public void setMarcaFilter(Marca marcaFilter) {
        this.marcaFilter = marcaFilter;
    }

    public MarcaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(MarcaAdapter adapter) {
        this.adapter = adapter;
    }

    public Empresa getEmpresaComplete() {
        return empresaComplete;
    }

    public void setEmpresaComplete(Empresa empresaComplete) {
        this.empresaComplete = empresaComplete;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    //</editor-fold>

    /**
     * Método para obtener Marca
     */
    public void filterMarca() {
        getAdapter().setFirstResult(0);
        this.adapter = this.adapter.fillData(marcaFilter);
    }

    /**
     * Método para limpiar filtros
     */
    public void clear() {
        this.adapter = new MarcaAdapter();
        this.marcaFilter = new Marca();
        filterMarca();
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa empresa = new Empresa();
        empresa.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(empresa);
        return adapterEmpresa.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        empresaComplete.setId(((Empresa) event.getObject()).getId());
    }

    /**
     * Metodo para redirigir a la vista Editar Marca
     *
     * @param id
     * @return vista para la edición de marca
     */
    public String editUrl(Integer id) {
        return String.format("marca-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    private void init() {
        this.adapter = new MarcaAdapter();
        this.marcaFilter = new Marca();
        this.empresaComplete = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
        filterMarca();
    }

    /**
     * Constructor de la clase
     */
    public MarcaListController() {
        init();
    }

}
