package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionCompraService;

/**
 * Adaptador de la lista de retención de compra
 *
 * @author alext
 */
public class RetencionCompraListAdapter extends
        DataListAdapter<RetencionCompra> {

    /**
     * Cliente para el listado de retención de compra
     */
    private final RetencionCompraService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos buscados
     * @return RetencionListAdapter
     */
    @Override
    public RetencionCompraListAdapter fillData(RetencionCompra searchData) {
        return service.getRetencionCompraList(searchData, getFirstResult(),
                getPageSize());
    }

    /**
     * Constructor de RetencionCompraListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public RetencionCompraListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new RetencionCompraService();
    }

    /**
     * Constructor de RetencionCompraListAdapter
     */
    public RetencionCompraListAdapter() {
        super();
        this.service = new RetencionCompraService();
    }
}
