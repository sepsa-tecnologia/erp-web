package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoCompraService;

/**
 * Adaptador de la lista de nota credito compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotaCreditoCompraAdapter extends DataListAdapter<NotaCreditoCompra> {

    /**
     * Cliente para el servicio de cobro
     */
    private final NotaCreditoCompraService serviceNotaCreditoCompra;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoCompraAdapter fillData(NotaCreditoCompra searchData) {

        return serviceNotaCreditoCompra.getNotaCreditoCompraList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de NotaCreditoCompraAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoCompraAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaCreditoCompra = new NotaCreditoCompraService();
    }

    /**
     * Constructor de NotaCreditoCompraAdapter
     */
    public NotaCreditoCompraAdapter() {
        super();
        this.serviceNotaCreditoCompra = new NotaCreditoCompraService();
    }
}
