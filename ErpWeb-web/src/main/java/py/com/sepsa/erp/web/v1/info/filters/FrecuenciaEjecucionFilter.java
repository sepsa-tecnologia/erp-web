/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;

/**
 * filter para frecuencia ejecucion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class FrecuenciaEjecucionFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public FrecuenciaEjecucionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de frecuencia
     *
     * @param frecuencia
     * @return
     */
    public FrecuenciaEjecucionFilter frecuencia(Integer frecuencia) {
        if (frecuencia != null) {
            params.put("frecuencia", frecuencia);
        }
        return this;
    }

    /**
     * Agrega el filtro de hora
     *
     * @param hora
     * @return
     */
    public FrecuenciaEjecucionFilter hora(Integer hora) {
        if (hora != null) {
            params.put("hora", hora);
        }
        return this;
    }

    /**
     * Agrega el filtro de minuto
     *
     * @param minuto
     * @return
     */
    public FrecuenciaEjecucionFilter minuto(Integer minuto) {
        if (minuto != null) {
            params.put("minuto", minuto);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param frecuenciaEjecucion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(FrecuenciaEjecucion frecuenciaEjecucion, Integer page, Integer pageSize) {
        FrecuenciaEjecucionFilter filter = new FrecuenciaEjecucionFilter();

        filter
                .id(frecuenciaEjecucion.getId())
                .frecuencia(frecuenciaEjecucion.getFrecuencia())
                .hora(frecuenciaEjecucion.getHora())
                .minuto(frecuenciaEjecucion.getMinuto())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

}
