/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.proceso.pojos.TipoLote;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.proceso.adapters.TipoLoteAdapter;
import py.com.sepsa.erp.web.v1.proceso.filters.TipoLoteFilter;
import py.com.sepsa.erp.web.v1.remote.API;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 *
 * @author Antonella Lucero
 */
public class TipoLoteService extends APIErpCore {

    /**
     * Obtiene la lista
     *
     * @param tipoLote Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoLoteAdapter getTipoLote(TipoLote tipoLote, Integer page,
            Integer pageSize) {

        TipoLoteAdapter lista = new TipoLoteAdapter();

        Map params = TipoLoteFilter.build(tipoLote, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_LOTE.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoLoteAdapter.class);

            lista = (TipoLoteAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param tipoLote
     * @return
     */
    public BodyResponse<TipoLote> setTipoLote(TipoLote tipoLote) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.TIPO_LOTE.url, API.ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(tipoLote));
            response = BodyResponse.createInstance(conn, TipoLote.class);
        }
        return response;
    }

    /**
     * Constructor de clase
     */
    public TipoLoteService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_LOTE("tipo lote", "tipo-lote");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
