package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.filters.EmpresaFilter;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class EmpresaService extends APIErpCore {

    public EmpresaAdapter getEmpresaList(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    public BodyResponse<Empresa> setEmpresa(Empresa empresa) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.EMPRESA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(empresa));
            response = BodyResponse.createInstance(conn, Empresa.class);
        }
        return response;
    }

    public EmpresaAdapter getEmpresasFacturasPendientes(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA_FACTURA_PENDIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    public EmpresaAdapter getEmpresasAutofacturasPendientes(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA_AUTOFACTURA_PENDIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    public EmpresaAdapter getEmpresasNotaCreditoPendientes(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA_NOTACREDITO_PENDIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    public EmpresaAdapter getEmpresasNotaDebitoPendientes(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA_NOTADEBITO_PENDIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    public EmpresaAdapter getEmpresasNotaRemisionPendientes(Empresa ret, Integer page,
            Integer pageSize) {

        EmpresaAdapter lista = new EmpresaAdapter();

        Map params = EmpresaFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.EMPRESA_NOTAREMISION_PENDIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EmpresaAdapter.class);

            lista = (EmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Recursos de conexión o servicios
     */
    public enum Resource {

        //Servicios
        EMPRESA("Servicio para Empresa", "empresa"),
        EMPRESA_FACTURA_PENDIENTE("Servicio para Empresa", "empresa/facturas-pendientes"),
        EMPRESA_AUTOFACTURA_PENDIENTE("Servicio para Empresa", "empresa/autofacturas-pendientes"),
        EMPRESA_NOTACREDITO_PENDIENTE("Servicio para Empresa", "empresa/nota-credito-pendientes"),
        EMPRESA_NOTADEBITO_PENDIENTE("Servicio para Empresa", "empresa/nota-debito-pendientes"),
        EMPRESA_NOTAREMISION_PENDIENTE("Servicio para Empresa", "empresa/nota-remision-pendientes");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
