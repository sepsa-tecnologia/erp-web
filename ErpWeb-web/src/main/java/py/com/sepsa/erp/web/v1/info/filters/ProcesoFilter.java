/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;

/**
 * Filter para proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ProcesoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public ProcesoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public ProcesoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public ProcesoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public ProcesoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de idTipoProceso
     *
     * @param idTipoProceso
     * @return
     */
    public ProcesoFilter idTipoProceso(Integer idTipoProceso) {
        if (idTipoProceso != null) {
            params.put("idTipoProceso", idTipoProceso);
        }
        return this;
    }

    /**
     * Agrega el filtro de idFrecuenciaEjecucion
     *
     * @param idFrecuenciaEjecucion
     * @return
     */
    public ProcesoFilter idFrecuenciaEjecucion(Integer idFrecuenciaEjecucion) {
        if (idFrecuenciaEjecucion != null) {
            params.put("idFrecuenciaEjecucion", idFrecuenciaEjecucion);
        }
        return this;
    }

    /**
     * Agrega el filtro de frecuenciaEjecucion
     *
     * @param frecuenciaEjecucion
     * @return
     */
    public ProcesoFilter frecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        if (frecuenciaEjecucion != null) {
            params.put("frecuenciaEjecucion", frecuenciaEjecucion);
        }
        return this;
    }

    /**
     * Agrega el filtro de idEmpresa
     *
     * @param idEmpresa
     * @return
     */
    public ProcesoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param proceso datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Proceso proceso, Integer page, Integer pageSize) {
        ProcesoFilter filter = new ProcesoFilter();
        
        filter
                .id(proceso.getId())
                .descripcion(proceso.getDescripcion())
                .codigo(proceso.getCodigo())
                .activo(proceso.getActivo())
                .idTipoProceso(proceso.getIdTipoProceso())
                .idEmpresa(proceso.getIdEmpresa())
                .idFrecuenciaEjecucion(proceso.getIdFrecuenciaEjecucion())
                .frecuenciaEjecucion(proceso.getFrecuenciaEjecucion())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }
    
}
