package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.usuario.adapters.MenuPerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.MenuPerfil;

/**
 * Controlador para listado de menú
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarMenu")
public class MenuListController implements Serializable {

    /**
     * Adaptador de listado de menú
     */
    private MenuListAdapter adapter;
    /**
     * Pojo para menú
     */
    private MenuPojo searchData;
    /**
     * Adaptador de listado de menú
     */
    private MenuListAdapter adapterMenu;
    /**
     * Pojo para menú
     */
    private MenuPojo filtroMenu;
    /**
     * Objeto Menu pojo
     */
    private MenuPojo menu;
    /**
     * Adaptador de la lista de menu perfil
     */
    private MenuPerfilListAdapter adapterMenuPerfil;
    /**
     * Objeto MenuPerfil
     */
    private MenuPerfil menuPerfil;
    /**
     * Cliente remoto para Menu
     */
    private MenuService menuService;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the menu
     */
    public MenuPojo getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(MenuPojo menu) {
        this.menu = menu;
    }

    /**
     * @return the menuService
     */
    public MenuService getMenuService() {
        return menuService;
    }

    /**
     * @param menuService the menuService to set
     */
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    /**
     * @return the menuPerfil
     */
    public MenuPerfil getMenuPerfil() {
        return menuPerfil;
    }

    /**
     * @param menuPerfil the menuPerfil to set
     */
    public void setMenuPerfil(MenuPerfil menuPerfil) {
        this.menuPerfil = menuPerfil;
    }

    /**
     * @return the adapterMenuPerfil
     */
    public MenuPerfilListAdapter getAdapterMenuPerfil() {
        return adapterMenuPerfil;
    }

    /**
     * @param adapterMenuPerfil the adapterMenuPerfil to set
     */
    public void setAdapterMenuPerfil(MenuPerfilListAdapter adapterMenuPerfil) {
        this.adapterMenuPerfil = adapterMenuPerfil;
    }

    public MenuListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(MenuListAdapter adapter) {
        this.adapter = adapter;
    }

    public MenuPojo getSearchData() {
        return searchData;
    }

    public void setSearchData(MenuPojo searchData) {
        this.searchData = searchData;
    }

    public MenuListAdapter getAdapterMenu() {
        return adapterMenu;
    }

    public void setAdapterMenu(MenuListAdapter adapterMenu) {
        this.adapterMenu = adapterMenu;
    }

    public MenuPojo getFiltroMenu() {
        return filtroMenu;
    }

    public void setFiltroMenu(MenuPojo filtroMenu) {
        this.filtroMenu = filtroMenu;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }
    //</editor-fold>

    /**
     * Método para filtrar menú
     */
    public void filterMenu() {
        adapter.setFirstResult(0);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        empresaAutocomplete = new Empresa();
        searchData = new MenuPojo();
        filterMenu();
    }

    /**
     * Metodo para redirigir a la vista editar Menu
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("menu-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Método para listar los perfiles del menu
     *
     * @param mp
     */
    public void getPerfilMenuList(MenuPojo mp) {
        menu = mp;
        getlistPerfilMenu();
    }

    /**
     * Método para actualizar asociación
     */
    public void getlistPerfilMenu() {
        menuPerfil = new MenuPerfil();
        menuPerfil.setIdMenu(menu.getId());
        menuPerfil.setIdEmpresa(menu.getIdEmpresa());
        adapterMenuPerfil = adapterMenuPerfil.fillData(menuPerfil);
    }

    /**
     * Método para obtener menús filtrados
     *
     * @param query
     * @return
     */
    public List<MenuPojo> completeQuery(String query) {

        MenuPojo menu2 = new MenuPojo();
        menu2.setDescripcion(query);

        adapterMenu = adapterMenu.fillData(menu2);

        return adapterMenu.getData();
    }

    /**
     * Selecciona el menu
     *
     * @param event
     */
    public void onItemSelectMenuFilter(SelectEvent event) {
        Integer idPadre = ((MenuPojo) event.getObject()).getId();
        this.filtroMenu.setIdPadre(idPadre);
        this.searchData.setIdPadre(idPadre);
    }

    /**
     * Asociar menu perfil
     *
     * @param menuP
     */
    public void createAssociation(MenuPerfil menuP) {

        menuPerfil = new MenuPerfil();
        menuPerfil.setIdMenu(menuP.getIdMenu());
        menuPerfil.setCodigoMenu(menuP.getCodigoMenu());
        menuPerfil.setIdPerfil(menuP.getIdPerfil());
        menuPerfil.setCodigoPerfil(menuP.getCodigoPerfil());
        menuPerfil.setActivo("S");
        menuPerfil.setIdEmpresa(menu.getIdEmpresa());

        BodyResponse<MenuPerfil> asoMenu = menuService.editMenuPerfil(menuPerfil);

        if (asoMenu.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Actualización correcta!"));
            getlistPerfilMenu();
        }
    }

    /**
     * Editar asociación
     *
     * @param menuP
     */
    public void deleteAssociation(MenuPerfil menuP) {
        menuPerfil = new MenuPerfil();
        menuPerfil.setIdMenu(menuP.getIdMenu());
        menuPerfil.setCodigoMenu(menuP.getCodigoMenu());
        menuPerfil.setIdPerfil(menuP.getIdPerfil());
        menuPerfil.setCodigoPerfil(menuP.getCodigoPerfil());
        menuPerfil.setActivo("N");
        menuPerfil.setIdEmpresa(menu.getIdEmpresa());

        BodyResponse<MenuPerfil> asoMenu = menuService.editMenuPerfil(menuPerfil);

        if (asoMenu.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Actualización correcta!"));
            getlistPerfilMenu();
        }
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        searchData.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    @PostConstruct
    public void init() {
        try {
            this.empresaAutocomplete = new Empresa();
            this.empresaAdapter = new EmpresaAdapter();
            this.adapter = new MenuListAdapter();
            this.searchData = new MenuPojo();
            this.filtroMenu = new MenuPojo();
            this.adapterMenuPerfil = new MenuPerfilListAdapter();
            this.adapterMenu = new MenuListAdapter();
            this.menuPerfil = new MenuPerfil();
            this.menuService = new MenuService();
            this.menu = new MenuPojo();
            filterMenu();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
