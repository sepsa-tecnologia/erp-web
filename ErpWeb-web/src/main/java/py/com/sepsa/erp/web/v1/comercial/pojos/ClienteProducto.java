package py.com.sepsa.erp.web.v1.comercial.pojos;

import java.util.List;

/**
 * POJO de ClienteProducto
 *
 * @author Romina E. Núñez Rojas
 */
public class ClienteProducto {
    /**
     * Identificador del Cliente
     */
    private Integer idCliente;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
    /**
     * Estado del producto
     */
    private String estado;
    /**
     * Facturable
     */
    private String facturable;
    /**
     * Tiene
     */
    private String tiene;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setTiene(String tiene) {
        this.tiene = tiene;
    }

    public String getTiene() {
        return tiene;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFacturable() {
        return facturable;
    }

    public void setFacturable(String facturable) {
        this.facturable = facturable;
    }
    
    
    /**
     * Contructor de la clase
     */
    public ClienteProducto() {

    }
    
    /**
     * Constructor con parámetros
     * @param idCliente
     * @param idProducto
     * @param producto
     * @param estado
     * @param facturable 
     */
    public ClienteProducto(Integer idCliente, Integer idProducto, String producto, String estado, String facturable) {
        this.idCliente = idCliente;
        this.idProducto = idProducto;
        this.producto = producto;
        this.estado = estado;
        this.facturable = facturable;
    }
    
    
    /**
     * Constructor con parámetros
     * @param idCliente
     * @param idProducto
     * @param producto
     * @param estado
     * @param facturable
     * @param tiene 
     */
    public ClienteProducto(Integer idCliente, Integer idProducto, String producto, String estado, String facturable, String tiene) {
        this.idCliente = idCliente;
        this.idProducto = idProducto;
        this.producto = producto;
        this.estado = estado;
        this.facturable = facturable;
        this.tiene = tiene;
    }
    
    
    
   
}
