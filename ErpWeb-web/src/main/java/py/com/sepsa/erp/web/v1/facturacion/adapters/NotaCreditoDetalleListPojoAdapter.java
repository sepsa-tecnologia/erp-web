
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;

/**
 * Adaptador de la lista de nota credito detalle
 * @author Cristina Insfrán
 */
public class NotaCreditoDetalleListPojoAdapter extends DataListAdapter<NotaCreditoDetallePojo> {
  
     /**
     * Cliente para el servicio de cobro
     */
    private final NotaCreditoService serviceNotaCredito;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoDetalleListPojoAdapter fillData(NotaCreditoDetallePojo searchData) {
     
        return serviceNotaCredito.getNotaCreditoDetalleListPojo(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaCreditoDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoDetalleListPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaCredito = new NotaCreditoService();
    }

    /**
     * Constructor de NotaCreditoDetalleListAdapter
     */
    public NotaCreditoDetalleListPojoAdapter() {
        super();
        this.serviceNotaCredito = new NotaCreditoService();
    }
}
