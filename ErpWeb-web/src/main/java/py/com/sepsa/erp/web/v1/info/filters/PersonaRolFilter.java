package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;

/**
 * Filtro utilizado para el servicio de persona rol
 *
 * @author Romina E. Núñez Rojas
 */
public class PersonaRolFilter extends Filter {

    /**
     * Agrega el filtro de identificador persona
     *
     * @param idPersona
     * @return
     */
    public PersonaRolFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del rol
     *
     * @param idRol
     * @return
     */
    public PersonaRolFilter idRol(Integer idRol) {
        if (idRol != null) {
            params.put("idRol", idRol);
        }
        return this;
    }

    /**
     * Filtro por persona.
     *
     * @param persona
     * @return
     */
    public PersonaRolFilter persona(String persona) {
        if (persona != null && !persona.trim().isEmpty()) {
            params.put("persona", persona);
        }
        return this;
    }

    /**
     * Agrega el filtro del estado
     *
     * @param rol
     * @return
     */
    public PersonaRolFilter rol(String rol) {
        if (rol != null) {
            params.put("rol", rol);
        }
        return this;
    }

    /**
     * Agrega el filtro del estado
     *
     * @param estado
     * @return
     */
    public PersonaRolFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro del tiene
     *
     * @param tiene
     * @return
     */
    public PersonaRolFilter tiene(String tiene) {
        if (tiene != null) {
            params.put("tiene", tiene);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param rolPersona
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(PersonaRol rolPersona, Integer page, Integer pageSize) {
        PersonaRolFilter filter = new PersonaRolFilter();

        filter
                .idPersona(rolPersona.getIdPersona())
                .idRol(rolPersona.getIdRol())
                .persona(rolPersona.getPersona())
                .rol(rolPersona.getRol())
                .estado(rolPersona.getEstado())
                .tiene(rolPersona.getTiene())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PersonaRolFilter
     */
    public PersonaRolFilter() {
        super();
    }

}
