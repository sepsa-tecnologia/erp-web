package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;

/**
 * Pojo para el listado de tipo referencia geográfica
 *
 * @author alext
 */
@ViewScoped
@Named("listarTipoReferenciaGeografica")
public class TipoReferenciaGeograficaController implements Serializable {

    /**
     * Adaptador de la lista de tipo de referencia geográfica
     */
    private TipoReferenciaGeograficaAdapter adapter;

    /**
     * Pojo de tipo de referencia geográfica
     */
    private TipoReferenciaGeografica tipoReferenciaGeografica;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoReferenciaGeograficaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(TipoReferenciaGeograficaAdapter adapter) {
        this.adapter = adapter;
    }

    public TipoReferenciaGeografica getTipoReferenciaGeografica() {
        return tipoReferenciaGeografica;
    }

    public void setTipoReferenciaGeografica(TipoReferenciaGeografica tipoReferenciaGeografica) {
        this.tipoReferenciaGeografica = tipoReferenciaGeografica;
    }
    //</editor-fold>

    /**
     * Método para filtrar tipo de referencia geográfica
     */
    public void filterReferencia() {
        adapter = adapter.fillData(tipoReferenciaGeografica);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        tipoReferenciaGeografica = new TipoReferenciaGeografica();
        filterReferencia();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapter = new TipoReferenciaGeograficaAdapter();
        this.tipoReferenciaGeografica = new TipoReferenciaGeografica();
        filterReferencia();
    }

}
