package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.TipoMenuListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;

/**
 * Controlador para la lista de TipoMenu
 *
 * @author alext
 */
@ViewScoped
@Named("listarTipoMenu")
public class TipoMenuListarController implements Serializable {

    /**
     * Adaptador de la lista de tipoMenu
     */
    private TipoMenuListAdapter adapter;

    /**
     * Pojo de tipoMenu
     */
    private TipoMenu searchData;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoMenuListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(TipoMenuListAdapter adapter) {
        this.adapter = adapter;
    }

    public TipoMenu getSearchData() {
        return searchData;
    }

    public void setSearchData(TipoMenu searchData) {
        this.searchData = searchData;
    }
    //</editor-fold>

    /**
     * Método para filtrar tipoMenu
     */
    public void filterTipoMenu() {
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        searchData = new TipoMenu();
        filterTipoMenu();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapter = new TipoMenuListAdapter();
        this.searchData = new TipoMenu();
        filterTipoMenu();
    }

}
