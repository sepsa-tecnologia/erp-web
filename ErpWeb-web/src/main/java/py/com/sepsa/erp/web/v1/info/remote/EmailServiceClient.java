
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Email
 * @author Cristina Insfrán
 */
public class EmailServiceClient extends APIErpCore{
    
     /**
     * Método para crear Email
     *
     * @param email
     * @return
     */
    public Email setEmail(Email email) {

        Email idEmail = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.EMAIL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(email));

            response = BodyResponse.createInstance(conn, Email.class);

            idEmail = (Email) response.getPayload();
           
        }
        return idEmail;
    }
    
       /**
     * Método para crear Email
     *
     * @param email
     * @return
     */
    public BodyResponse putEmail(Email email) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.EMAIL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(email));

            response = BodyResponse.createInstance(conn, Email.class);

           
        }
        return response;
    }
    /**
     * Constructor de EmailServiceClient
     */
    public EmailServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        EMAIL("Servicio email", "email");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
