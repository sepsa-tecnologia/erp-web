package py.com.sepsa.erp.web.v1.info.pojos;

import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;

/**
 * POJO de local
 *
 * @author Sergio D. Riveros Vazquez, Cristina Insfrán
 */
public class Local {

    /**
     * Identificador del local
     */
    private Integer id;
    /**
     * Gln del local
     */
    private String gln;
    /**
     * Id Externo
     */
    private Integer idExterno;
    /**
     * Estado del local
     */
    private String activo;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * Observación sobre el local
     */
    private String observacion;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Persona
     */
    private Persona persona;
    /**
     * Identificador de dirección
     */
    private Integer idDireccion;
    /**
     * Filtro local externo
     */
    private String localExterno;
    /**
     * POJO Cliente
     */
    private Cliente cliente;
    
    /**
     * Identificador de telefono
     */
    private Integer idTelefono;
    /**
     *  POJO Telefono
     */
    private Telefono telefono;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * @return the idDireccion
     */
    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setLocalExterno(String localExterno) {
        this.localExterno = localExterno;
    }

    public String getLocalExterno() {
        return localExterno;
    }

    /**
     * @param idDireccion the idDireccion to set
     */
    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the gln
     */
    public String getGln() {
        return gln;
    }

    /**
     * @param gln the gln to set
     */
    public void setGln(String gln) {
        this.gln = gln;
    }

    public Integer getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(Integer idExterno) {
        this.idExterno = idExterno;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }
 

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Local() {

    }

    public Local(Integer id) {
        this.id = id;
    }
}
