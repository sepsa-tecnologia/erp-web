package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.AutoFacturaDetallePojoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.AutoFacturaFilter;
import py.com.sepsa.erp.web.v1.factura.filters.AutoFacturaPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPendAgrupadosAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LugarCobroListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.FacturaFilter;
import py.com.sepsa.erp.web.v1.factura.filters.CobroDetalleFilter;
import py.com.sepsa.erp.web.v1.factura.filters.LugarCobroFilter;
import py.com.sepsa.erp.web.v1.factura.filters.MotivoAnulacionFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteParam;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaDetallePojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de facturacion
 *
 * @author Romina Núñez, Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class AutoFacturaService extends APIErpFacturacion {

    /**
     * Obtiene la lista de facturas recibidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public FacturaCompraAdapter getFacturaCompraList(Factura factura, Integer page,
            Integer pageSize) {

        FacturaCompraAdapter lista = new FacturaCompraAdapter();

        Map params = FacturaFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_COMPRA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FacturaCompraAdapter.class);

            lista = (FacturaCompraAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas recibidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public AutoFacturaTalonarioAdapter getFacturaTalonarioList(TalonarioPojo talonario, Integer page,
            Integer pageSize) {

        AutoFacturaTalonarioAdapter lista = new AutoFacturaTalonarioAdapter();

        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_TALONARIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutoFacturaTalonarioAdapter.class);

            lista = (AutoFacturaTalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas emitidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public AutoFacturaAdapter getFacturaList(AutoFactura factura, Integer page,
            Integer pageSize) {

        AutoFacturaAdapter lista = new AutoFacturaAdapter();

        Map params = AutoFacturaFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.AUTO_FACTURA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutoFacturaAdapter.class);

            lista = (AutoFacturaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas emitidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public AutoFacturaPojoAdapter getFacturaPojoList(AutoFacturaPojo factura, Integer page,
            Integer pageSize) {

        AutoFacturaPojoAdapter lista = new AutoFacturaPojoAdapter();

        Map params = AutoFacturaPojoFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.AUTO_FACTURA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutoFacturaPojoAdapter.class);

            lista = (AutoFacturaPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas emitidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public AutoFacturaDetallePojoAdapter getFacturaDetallePojoList(AutoFacturaDetallePojo factura, Integer page,
            Integer pageSize) {

        AutoFacturaDetallePojoAdapter lista = new AutoFacturaDetallePojoAdapter();

        Map params = AutoFacturaDetallePojoFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_DETALLE_POJO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutoFacturaDetallePojoAdapter.class);

            lista = (AutoFacturaDetallePojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas compras
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public FacturaMontoCompraAdapter getFacturaMontoComprasList(Factura factura, Integer page,
            Integer pageSize) {

        FacturaMontoCompraAdapter lista = new FacturaMontoCompraAdapter();

        Map params = FacturaFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_COMPRA_MONTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FacturaMontoCompraAdapter.class);

            lista = (FacturaMontoCompraAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public FacturaMontoAdapter getFacturaMontoList(Factura factura, Integer page,
            Integer pageSize) {

        FacturaMontoAdapter lista = new FacturaMontoAdapter();

        Map params = FacturaFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_MONTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FacturaMontoAdapter.class);

            lista = (FacturaMontoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas
     *
     * @param autoFactura Objeto
     * @return
     */
    public AutoFacturaPojo getAutoFacturaDetalle(AutoFacturaPojo autoFactura) {

        AutoFacturaPojo f = new AutoFacturaPojo();

        Map params = new HashMap<>();
        String URL = "autofactura/id/" + autoFactura.getId();
        params.put("listadoPojo", autoFactura.getListadoPojo());

        HttpURLConnection conn = GET(URL,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutoFacturaPojo.class);

            f = (AutoFacturaPojo) response.getPayload();

            conn.disconnect();
        }
        return f;
    }

    /**
     * Obtiene la lista de facturas
     *
     * @param factura Objeto
     * @return
     */
    public Factura getFacturaCompraDetalle(Factura factura) {

        Factura f = new Factura();

        Map params = new HashMap<>();
        String URL = "factura-compra/id/" + factura.getId();

        HttpURLConnection conn = GET(URL,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    Factura.class);

            f = (Factura) response.getPayload();

            conn.disconnect();
        }
        return f;
    }

    /**
     * Datos de Factura
     *
     * @param parametros
     * @return
     */
    public DatosFactura getDatosFactura(Map parametros) {

        DatosFactura df = new DatosFactura();

        Map params = new HashMap<>();

        params = parametros;

        HttpURLConnection conn = GET(Resource.DATOS_FACTURA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DatosFactura.class);

            df = (DatosFactura) response.getPayload();

            conn.disconnect();
        }
        return df;
    }

    /**
     * Método para crear Factura
     *
     * @param factura
     * @return
     */
    public BodyResponse<AutoFactura> createFactura(AutoFactura factura) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CREAR_FACTURA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(factura));

            response = BodyResponse.createInstance(conn, AutoFactura.class);

        }
        return response;
    }

    /**
     * Método para anular la factura
     *
     * @param factura Objeto
     * @return
     */
    public BodyResponse<AutoFactura> anularFactura(AutoFactura factura) {

        BodyResponse response = new BodyResponse();

        String URL = "autofactura/anular/" + factura.getId();

        Map params = new HashMap<>();

        params.put("ignorarPeriodoAnulacion", factura.getIgnorarPeriodoAnulacion());

        HttpURLConnection conn = PUT(URL, ContentType.JSON, params);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(factura));
            response = BodyResponse.createInstance(conn,
                    AutoFactura.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Método para anular la factura
     *
     * @param factura Objeto
     * @return
     */
    public BodyResponse<AutoFactura> regenerarFactura(AutoFactura factura) {

        BodyResponse response = new BodyResponse();

        //String URL = "factura/anular/" + factura.getId();
        HttpURLConnection conn = PUT(Resource.AUTO_FACTURA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(factura));
            response = BodyResponse.createInstance(conn,
                    AutoFactura.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Método para anular la factura
     *
     * @param factura Objeto
     * @return
     */
    public BodyResponse<Factura> anularFacturaCompra(Factura factura) {

        BodyResponse response = new BodyResponse();

        String URL = "factura-compra/anular/" + factura.getId();

        HttpURLConnection conn = PUT(URL, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(factura));
            response = BodyResponse.createInstance(conn,
                    Factura.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Sumatoria de facturas
     *
     * @param factura
     * @return
     */
    public Factura getFacturacionSuma(Factura factura) {

        Factura fac = new Factura();

        Map params = FacturaFilter.build(factura, null, null);

        HttpURLConnection conn = GET(Resource.FACTURA_SUMATORIA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    Factura.class);

            fac = (Factura) response.getPayload();

            conn.disconnect();
        }
        return fac;
    }

    /**
     * Lista de facturas con montos agrupados
     *
     * @param factura
     * @param page
     * @param pageSize
     * @return
     */
    public FacturaPendAgrupadosAdapter getFacturaPendAgrupadoList(Factura factura, Integer page,
            Integer pageSize) {

        FacturaPendAgrupadosAdapter lista = new FacturaPendAgrupadosAdapter();

        Map params = FacturaFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_CLIENTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FacturaPendAgrupadosAdapter.class);

            lista = (FacturaPendAgrupadosAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Detalle de cobro
     *
     * @param factura
     * @param page
     * @param pageSize
     * @return
     */
    public CobroDetalleListAdapter getDetalleCobroList(CobroDetalle factura, Integer page,
            Integer pageSize) {

        CobroDetalleListAdapter lista = new CobroDetalleListAdapter();

        Map params = CobroDetalleFilter.build(factura, page, pageSize);

        HttpURLConnection conn = GET(Resource.DETALLE_COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CobroDetalleListAdapter.class);

            lista = (CobroDetalleListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de lugar cobro
     *
     * @param lugarCobro
     * @param page
     * @param pageSize
     * @return
     */
    public LugarCobroListAdapter getLugarCobroList(LugarCobro lugarCobro, Integer page,
            Integer pageSize) {

        LugarCobroListAdapter lista = new LugarCobroListAdapter();

        Map params = LugarCobroFilter.build(lugarCobro, page, pageSize);

        HttpURLConnection conn = GET(Resource.LUGAR_COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LugarCobroListAdapter.class);

            lista = (LugarCobroListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de facturas
     *
     * @param motivo
     * @param page
     * @param pageSize
     * @return
     */
    public MotivoAnulacionAdapterList getMotivoAnulacionList(MotivoAnulacion motivo, Integer page,
            Integer pageSize) {

        MotivoAnulacionAdapterList lista = new MotivoAnulacionAdapterList();

        Map params = MotivoAnulacionFilter.build(motivo, page, pageSize);

        HttpURLConnection conn = GET(Resource.MOTIVO_ANULACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MotivoAnulacionAdapterList.class);

            lista = (MotivoAnulacionAdapterList) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para obtener reporte de venta
     *
     * @param estadoCuenta
     * @return
     */
    public byte[] getEstadoCuenta(ReporteParam estadoCuenta) {

        byte[] result = null;

        Date inicio = estadoCuenta.getFechaDesde();
        Date fin = estadoCuenta.getFechaHasta();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String inicio_ = format.format(inicio);
        String fin_ = format.format(fin);
        String id_ = estadoCuenta.getIdPersona().toString();
        String service
                = String.format(AutoFacturaService.Resource.ESTADO_CUENTA.url, id_, inicio_, fin_);

        HttpURLConnection conn = GET(service, ContentType.JSON, null);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Método para crear Factura de Compra
     *
     * @param factura
     * @return
     */
    public BodyResponse<Factura> createFacturaCompra(Factura factura) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.FACTURA_COMPRA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(factura));

            response = BodyResponse.createInstance(conn, Factura.class);

        }
        return response;
    }

    /**
     *
     * @param idFactura identificador de factura compra
     * @return
     */
    public Boolean deleteFactura(Integer idFactura) {
        BodyResponse response = new BodyResponse();
        String URL = "factura-compra/" + idFactura;
        HttpURLConnection conn = DELETE(URL, ContentType.JSON);
        if (conn != null) {
            response = BodyResponse.createInstance(conn, Factura.class);
        }
        return response.getSuccess();
    }

    /**
     * Método para crear Motivo Anulacion
     *
     * @param motivoAnulacion
     * @return
     */
    public BodyResponse<MotivoAnulacion> createMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MOTIVO_ANULACION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoAnulacion));
            response = BodyResponse.createInstance(conn, MotivoAnulacion.class);
        }

        return response;
    }
    
    
     /**
     * Método para crear producto
     *
     * @param producto
     * @return
     */
    public byte[] sendFile(Attach cargaFactura) {

        byte[] result = null;

        HttpURLConnection conn = POST(Resource.CARGA_MASIVA.url, ContentType.MULTIPART);

        if (conn != null) {
            try {
                OutputStream os = conn.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(os), true);
                addFormFile(writer, os, "uploadedFile", cargaFactura);
                BodyByteArrayResponse response = BodyByteArrayResponse
                        .createInstance(conn, writer);

                result = response.getPayload();

                conn.disconnect();
                
                String content = new String(result, StandardCharsets.UTF_8);
                
                try {

                     content = String.format("nrofactura;estado;mensaje\r\n%s", content);
                     byte[] arregloBytes = content.getBytes();
                    
                     return arregloBytes;
                     

                } catch(Exception e){
                    byte[] arregloBytes = new byte[0];
                    return arregloBytes;
                }

            } catch (IOException ex) {
                WebLogger.get().fatal(ex);
            }
        }

        return result;
    }

    /**
     * Constructor de FacturacionService
     */
    public AutoFacturaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        FACTURA("Factura", "factura"),
        AUTO_FACTURA("Factura", "autofactura"),
        CARGA_MASIVA("Factura Carga Masiva", "factura/crear-masivo"),
        FACTURA_DETALLE_POJO("Detalle", "autofactura-detalle"),
        FACTURA_TALONARIO("Factura Talonario", "autofactura/talonario"),
        ESTADO_CUENTA("Estado de Cuenta", "factura/estado-cuenta/%s/%s/%s"),
        FACTURA_COMPRA("Factura", "factura-compra"),
        FACTURA_COMPRA_MONTO("Factura", "factura-compra/monto"),
        FACTURA_CLIENTE("Factura", "factura/monto/cliente"),
        FACTURA_MONTO("Factura monto", "autofactura/monto"),
        CREAR_FACTURA("Factura", "autofactura"),
        FACTURA_DETALLE("Detalle", "factura/consulta"),
        DATOS_FACTURA("DatosFactura", "autofactura/datos-crear"),
        FACTURA_SUMATORIA("Servicios para facturas", "factura/monto/sumatoria"),
        DETALLE_COBRO("Servicio para detalle del cobro", "cobro-detalle"),
        LUGAR_COBRO("Lista de lugar cobro", "lugar-cobro"),
        MOTIVO_ANULACION("Lista de motivo anulacion", "motivo-anulacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
