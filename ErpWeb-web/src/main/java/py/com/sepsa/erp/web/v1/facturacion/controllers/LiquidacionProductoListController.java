/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LiquidacionProductoAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.facturacion.pojos.LiquidacionProducto;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("liquidacionProductoList")
public class LiquidacionProductoListController implements Serializable {

    /**
     * Adaptador para la lista de LiquidacionProductos
     */
    private LiquidacionProductoAdapter liquidacionProductoAdapterList;

    /**
     * POJO de LiquidacionProducto
     */
    private LiquidacionProducto liquidacionProductoFilter;

    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;

    /**
     * Datos del cliente
     */
    private Cliente cliente;

    /**
     * Adaptador de la lista de productos
     */
    private ProductoAdapter productoAdapter;

    /**
     * Datos del cliente
     */
    private Producto producto;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public LiquidacionProductoAdapter getLiquidacionProductoAdapterList() {
        return liquidacionProductoAdapterList;
    }

    public void setLiquidacionProductoAdapterList(LiquidacionProductoAdapter liquidacionProductoAdapterList) {
        this.liquidacionProductoAdapterList = liquidacionProductoAdapterList;
    }

    public LiquidacionProducto getLiquidacionProductoFilter() {
        return liquidacionProductoFilter;
    }

    public void setLiquidacionProductoFilter(LiquidacionProducto liquidacionProductoFilter) {
        this.liquidacionProductoFilter = liquidacionProductoFilter;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ProductoAdapter getProductoAdapter() {
        return productoAdapter;
    }

    public void setProductoAdapter(ProductoAdapter productoAdapter) {
        this.productoAdapter = productoAdapter;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    //</editor-fold>
    /**
     * Método para obtener las Retenciones
     */
    public void getLiquidacionProductos() {
        this.setLiquidacionProductoAdapterList(getLiquidacionProductoAdapterList().fillData(getLiquidacionProductoFilter()));
    }

    /**
     * Método para obtener los productos
     */
    public void getProductos() {
        this.setProductoAdapter(getProductoAdapter().fillData(getProducto()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.cliente = new Cliente();
        this.clientAdapter = new ClientListAdapter();
        this.producto = new Producto();
        this.liquidacionProductoFilter = new LiquidacionProducto();
        getProductos();
        getLiquidacionProductos();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        liquidacionProductoFilter.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Metodo para redirigir a la vista Editar LiquidacionProducto
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("liquidacion-producto-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.setLiquidacionProductoFilter(new LiquidacionProducto());
            this.setLiquidacionProductoAdapterList(new LiquidacionProductoAdapter());
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.producto = new Producto();
            this.productoAdapter = new ProductoAdapter();
            getProductos();
            getLiquidacionProductos();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
