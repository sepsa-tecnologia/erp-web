package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturaProcesamientoService;

/**
 * Adaptador de la lista de factura procesamiento
 *
 * @author Romina Núñez
 */
public class AutofacturaProcesamientoAdapter extends DataListAdapter<DocumentoProcesamiento> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final FacturaProcesamientoService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public AutofacturaProcesamientoAdapter fillData(DocumentoProcesamiento searchData) {

        return serviceFactura.getAutofacturaProcesamientoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public AutofacturaProcesamientoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturaProcesamientoService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public AutofacturaProcesamientoAdapter() {
        super();
        this.serviceFactura = new FacturaProcesamientoService();
    }
}
