package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;

/**
 * POJO para Historico Liquidacion Detalle
 *
 * @author Romina Núñez
 */
public class HistoricoLiquidacionDetalle {

    /**
     * Identificador de Historico Liquidacion
     */
    private Integer idHistoricoLiquidacion;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Identificador de Tarifa
     */
    private Integer idTarifa;
    /**
     * Cliente
     */
    private String cliente;
    /**
     * Cliente relacionado
     */
    private String clienteRel;
    /**
     * Identificador de Grupo Cliente
     */
    private Integer idClienteRel;
    /**
     * Identificador Sociedad Cliente Relacionado
     */
    private Integer idSociedadClienteRel;
    /**
     * Monto Tarifa
     */
    private BigDecimal montoTarifa;
    /**
     * Monto Descuento
     */
    private BigDecimal montoDescuento;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdClienteRel(Integer idClienteRel) {
        this.idClienteRel = idClienteRel;
    }

    public void setClienteRel(String clienteRel) {
        this.clienteRel = clienteRel;
    }

    public Integer getIdClienteRel() {
        return idClienteRel;
    }

    public String getClienteRel() {
        return clienteRel;
    }

    public void setIdSociedadClienteRel(Integer idSociedadClienteRel) {
        this.idSociedadClienteRel = idSociedadClienteRel;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getIdSociedadClienteRel() {
        return idSociedadClienteRel;
    }

    public String getCliente() {
        return cliente;
    }
    
    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }
    
    public Integer getNroLinea() {
        return nroLinea;
    }

    public BigDecimal getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigDecimal montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigDecimal getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigDecimal montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }
    
    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }
    
    public Integer getIdTarifa() {
        return idTarifa;
    }
    
    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }
    
    
    
//</editor-fold>

    
    /**
     * Constructor de la clase
     */
    public HistoricoLiquidacionDetalle() {
    }

    /**
     * Constructor con parametros
     * @param idHistoricoLiquidacion
     * @param nroLinea
     * @param idTarifa
     * @param cliente
     * @param clienteRel
     * @param idClienteRel
     * @param idSociedadClienteRel
     * @param montoTarifa
     * @param montoDescuento
     * @param montoTotal 
     */
    public HistoricoLiquidacionDetalle(Integer idHistoricoLiquidacion, Integer nroLinea, Integer idTarifa, String cliente, String clienteRel, Integer idClienteRel, Integer idSociedadClienteRel, BigDecimal montoTarifa, BigDecimal montoDescuento, BigDecimal montoTotal) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.nroLinea = nroLinea;
        this.idTarifa = idTarifa;
        this.cliente = cliente;
        this.clienteRel = clienteRel;
        this.idClienteRel = idClienteRel;
        this.idSociedadClienteRel = idSociedadClienteRel;
        this.montoTarifa = montoTarifa;
        this.montoDescuento = montoDescuento;
        this.montoTotal = montoTotal;
    }

    

    
      

}
