package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.MenuFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.usuario.adapters.MenuPerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.MenuPerfilFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.MenuPerfil;

/**
 * Servicio para listado de menú
 *
 * @author alext, Cristina Insfrán
 */
public class MenuService extends APIErpCore {

    /**
     * Método que obtiene el listado de menu
     *
     * @param menu objeto
     * @param page
     * @param pageSize
     * @return
     */
    public MenuListAdapter getMenuList(MenuPojo menu, Integer page,
            Integer pageSize) {

        MenuListAdapter lista = new MenuListAdapter();

        Map params = MenuFilter.build(menu, page, pageSize);

        HttpURLConnection conn = GET(Resource.MENU.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MenuListAdapter.class);

            lista = (MenuListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear un nuevo menu
     *
     * @param menu
     * @return
     */
    public BodyResponse<MenuPojo> setMenu(MenuPojo menu) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MENU.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(menu));
            response = BodyResponse.createInstance(conn, MenuPojo.class);
        }
        return response;
    }
    
   
    
    /**
     * Método para editar menu perfil
     *
     * @param menu
     * @return
     */
    public BodyResponse<MenuPojo> editMenu(MenuPojo menu) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.MENU.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(menu));
            response = BodyResponse.createInstance(conn, MenuPojo.class);
        }
        return response;
    }
       /**
     * Método para crear un nuevo menu perfil
     *
     * @param menu
     * @return
     */
    public BodyResponse<MenuPerfil> setMenuPerfil(MenuPerfil menu) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MENU_PERFIL.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(menu));
            response = BodyResponse.createInstance(conn, MenuPojo.class);
        }
        return response;
    }
     /**
     * Método para editar menu perfil
     *
     * @param menu
     * @return
     */
    public BodyResponse<MenuPerfil> editMenuPerfil(MenuPerfil menu) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.MENU_PERFIL.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().create();
            this.addBody(conn, gson.toJson(menu));
            response = BodyResponse.createInstance(conn, MenuPojo.class);
        }
        return response;
    }
    
    /**
     * Método que obtiene el listado de menu
     *
     * @param menuperfil
     * @param page
     * @param pageSize
     * @return
     */
    public MenuPerfilListAdapter getMenuPerfilList(MenuPerfil menuperfil, Integer page,
            Integer pageSize) {

        MenuPerfilListAdapter lista = new MenuPerfilListAdapter();

        Map params = MenuPerfilFilter.build(menuperfil, page, pageSize);

        HttpURLConnection conn = GET(Resource.MENU_PERFIL_ASO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MenuPerfilListAdapter.class);

            lista = (MenuPerfilListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Método asociar y desasociar perfil de menu
     * @param menuPerfil
     * @return 
     */

    /*public BodyResponse editAsociacionMenuPerfil(MenuPerfil menuPerfil) {

     
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.MENU_PERFIL_ASO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(menuPerfil));

            response = BodyResponse.createInstance(conn, MenuPerfil.class);

        

        }
        return response;
    }*/
    
    /**
     * Método para obtener detalles del objeto a editar.
     * @param id
     * @return
     */
    public MenuPojo get(Integer id) {
        MenuPojo data = new MenuPojo(id);
        MenuListAdapter list = getMenuList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de la clase
     */
    public MenuService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        MENU("Listado de menu", "menu"),
        MENU_PERFIL("Menu del perfil", "menu-perfil"),
        MENU_PERFIL_ASO("Asociación del perfil","menu-perfil/relacionado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
