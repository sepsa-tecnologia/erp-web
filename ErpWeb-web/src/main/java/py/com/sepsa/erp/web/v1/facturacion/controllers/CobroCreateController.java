package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Cheque;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.EntidadFinancieraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LugarCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaEmailAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;
/**
 *
 * @author SONY
 */
@ViewScoped
@Named("cobroCreate")
public class CobroCreateController implements Serializable {

    /**
     * Variable para visualizar panel
     */
    private boolean viewPanel = false;
    /**
     * Objeto Cliente
     */
    private Cliente client;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto para crear el cobro
     */
    private Cobro cobro;
    /**
     * POJO de factura pendientes
     */
    private Factura facturaPendientes;
    /**
     * POJO de factura canceladas
     */
    private Factura facturaCanceladas;
    /**
     * Adaptador de la lista de facturas
     */
    private FacturaMontoAdapter adapterFacturasPendientes;
    /**
     * Adaptador de la lista de facturas
     */
    private FacturaMontoAdapter adapterFacturasCanceladas;
    /**
     * Seleccion de la lista de cobro
     */
    private List<Factura> selectedFacturas;
    /**
     * Cliente remoto para servicio de cobro
     */
    private CobroService cobroClient;
    /**
     * Lista para cobro
     */
    private List<Cobro> selectedCobros;
    /**
     * Variable global para monto cobro
     */
    private BigDecimal montoCobroTotal = BigDecimal.ZERO;
    /**
     * Lista para lugar cobro
     */
    private List<SelectItem> listLugarCobro = new ArrayList<>();
    /**
     * Adaptador de la lista de lugar cobro
     */
    private LugarCobroListAdapter adapterLugarCobro;
    /**
     * Objeto lugarCobro
     */
    private LugarCobro lugarCobro;
    /**
     * Fecha del recibo
     */
    private Date reciboDate;
    /**
     * Número del recibo
     */
    private String reciboNum = "";
    /**
     * Lugar para el pago seleccionado
     */
    private Integer selectedLugarPago;
    /**
     * Objeto cobro detalle
     */
    private CobroDetalle cobroDetalle;
    /**
     * Lista para tipo de cobro
     */
    private List<SelectItem> listTipoCobro = new ArrayList<>();
    /**
     * Adaptador de la lista de tipo cobro
     */
    private TipoCobroListAdapter adapterTipoCobro;
    /**
     * Pojo tipo cobro
     */
    private TipoCobro tipoCobro;
    /**
     * Adaptador para la lista de entidad financiera
     */
    private EntidadFinancieraListAdapter adapterEntidadFinanciera;
    /**
     * Objeto Entidad Financiera
     */
    private EntidadFinanciera entidadFinanciera;
    /**
     * Variable para guardar el valor de nro de factura
     */
    private String nroFactura;
    /**
     * Adaptador de la lista de cobro detalle
     */
    private CobroDetalleListAdapter adapterCobroDetalle;
    /**
     * Objeto detalle nota de credito
     */
    private NotaCreditoDetalle notaCreditoDetalle;
    /**
     * Adaptador de la lista del detalle de nota de credito
     */
    private NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle;
    private BigDecimal test;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showDescargaPopup;
    /**
     * Dato a ser utilizado para la descarga
     */
    private Integer idCobroCreado;
    /**
     * Dato a ser utilizado para la descarga
     */
    private String nroCobroCreads;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    private EntidadFinanciera entFinanSelected;
    private BigDecimal saldoGeneral;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Cobro Talonario
     */
    private TalonarioPojo cobroTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Adapter Cobro Talonario
     */
    private CobroTalonarioAdapter adapterCobroTalonario;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    private DatosCobro datoCobro;
    private Integer idTalonario;
    /**
     * POJO de PersonaEmail
     */
    private PersonaEmail personaEmail;
    /**
     * Adapter de PersonaEmail
     */
    private PersonaEmailAdapter adapterPersonaEmail;
    /**
     * Email que recibira el recibo
     */
    private String emailNotificacion;
    
    private List<PersonaEmail> emailList;
    /**
     * Lista de Emails que recibiran el recibo
     */
    private List<Email> selectedEmail;
    /**
     * Input para agregar un email a la lista de emails para notificar
     */
    private String addEmail;
    /**
     * Auxiliar para saber si se debe notificar por email
     */
    private String notificar;
    
    private int index;
    
    private boolean cobroPorTotal;
    
        /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * POJO TipoCambio
     */
    private TipoCambio tipoCambio;
    /**
     * Monto total del cobro en Guaraníes
     */
    private BigDecimal montoCobroTotalGs;
    /**
     * Bandera para validar datos de cobro con cheque
     */
    private String omitirValidacionCheque;
    /**
     * Lazy Data Model de factura
     */
    private FacturaDataModel lazyModel;
    /**
     * Bandera para cobros en Guaraníes de facturas con moneda extranjera
     */
    private boolean cobroEnGs;
    
    private ConfiguracionValorServiceClient serviceConfiguracionValor;
    
    private String mail;
    
    private String passMail;
    
    private String port;
    
    private String server;
    
    private String notificado;
    
    private ConfiguracionValorListAdapter notifConfig;
    private ConfiguracionValorListAdapter mailConfig;
    private ConfiguracionValorListAdapter passConfig;
    private ConfiguracionValorListAdapter portConfig;
    private ConfiguracionValorListAdapter serverConfig;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
        
    public boolean isCobroEnGs() {
        return cobroEnGs;
    }
    
    public void setCobroEnGs(boolean cobroEnGs) {
        this.cobroEnGs = cobroEnGs;
    }

    public FacturaDataModel getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(FacturaDataModel lazyModel) {
        this.lazyModel = lazyModel;
    }

    public String getOmitirValidacionCheque() {
        return omitirValidacionCheque;
    }

    public void setOmitirValidacionCheque(String omitirValidacionCheque) {
        this.omitirValidacionCheque = omitirValidacionCheque;
    }

    public BigDecimal getMontoCobroTotalGs() {
        return montoCobroTotalGs;
    }

    public void setMontoCobroTotalGs(BigDecimal montoCobroTotalGs) {
        this.montoCobroTotalGs = montoCobroTotalGs;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

   
    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public boolean isCobroPorTotal() {
        return cobroPorTotal;
    }

    public void setCobroPorTotal(boolean cobroPorTotal) {
        this.cobroPorTotal = cobroPorTotal;
    }
    
    /**
     * @return the adapterNotaCreditoDetalle
     */
    public NotaCreditoDetalleListAdapter getAdapterNotaCreditoDetalle() {
        return adapterNotaCreditoDetalle;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setDatoCobro(DatosCobro datoCobro) {
        this.datoCobro = datoCobro;
    }

    public DatosCobro getDatoCobro() {
        return datoCobro;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public TalonarioPojo getCobroTalonario() {
        return cobroTalonario;
    }

    public void setCobroTalonario(TalonarioPojo cobroTalonario) {
        this.cobroTalonario = cobroTalonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public CobroTalonarioAdapter getAdapterCobroTalonario() {
        return adapterCobroTalonario;
    }

    public void setAdapterCobroTalonario(CobroTalonarioAdapter adapterCobroTalonario) {
        this.adapterCobroTalonario = adapterCobroTalonario;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public void setEntFinanSelected(EntidadFinanciera entFinanSelected) {
        this.entFinanSelected = entFinanSelected;
    }

    public EntidadFinanciera getEntFinanSelected() {
        return entFinanSelected;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public boolean isShowDescargaPopup() {
        return showDescargaPopup;
    }

    public String getNroCobroCreads() {
        return nroCobroCreads;
    }

    public void setShowDescargaPopup(boolean showDescargaPopup) {
        this.showDescargaPopup = showDescargaPopup;
    }

    public void setNroCobroCreads(String nroCobroCreads) {
        this.nroCobroCreads = nroCobroCreads;
    }

    public void setIdCobroCreado(Integer idCobroCreado) {
        this.idCobroCreado = idCobroCreado;
    }

    public Integer getIdCobroCreado() {
        return idCobroCreado;
    }

    public void setTest(BigDecimal test) {
        this.test = test;
    }

    public BigDecimal getTest() {
        return test;
    }

    /**
     * @param adapterNotaCreditoDetalle the adapterNotaCreditoDetalle to set
     */
    public void setAdapterNotaCreditoDetalle(NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle) {
        this.adapterNotaCreditoDetalle = adapterNotaCreditoDetalle;
    }

    /**
     * @return the notaCreditoDetalle
     */
    public NotaCreditoDetalle getNotaCreditoDetalle() {
        return notaCreditoDetalle;
    }

    /**
     * @param notaCreditoDetalle the notaCreditoDetalle to set
     */
    public void setNotaCreditoDetalle(NotaCreditoDetalle notaCreditoDetalle) {
        this.notaCreditoDetalle = notaCreditoDetalle;
    }

    /**
     * @return the adapterCobroDetalle
     */
    public CobroDetalleListAdapter getAdapterCobroDetalle() {
        return adapterCobroDetalle;
    }

    /**
     * @param adapterCobroDetalle the adapterCobroDetalle to set
     */
    public void setAdapterCobroDetalle(CobroDetalleListAdapter adapterCobroDetalle) {
        this.adapterCobroDetalle = adapterCobroDetalle;
    }

    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    /**
     * @return the adapterEntidadFinanciera
     */
    public EntidadFinancieraListAdapter getAdapterEntidadFinanciera() {
        return adapterEntidadFinanciera;
    }

    /**
     * @param adapterEntidadFinanciera the adapterEntidadFinanciera to set
     */
    public void setAdapterEntidadFinanciera(EntidadFinancieraListAdapter adapterEntidadFinanciera) {
        this.adapterEntidadFinanciera = adapterEntidadFinanciera;
    }

    /**
     * @return the entidadFinanciera
     */
    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    /**
     * @param entidadFinanciera the entidadFinanciera to set
     */
    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    /**
     * @return the tipoCobro
     */
    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    /**
     * @param tipoCobro the tipoCobro to set
     */
    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    /**
     * @return the adapterTipoCobro
     */
    public TipoCobroListAdapter getAdapterTipoCobro() {
        return adapterTipoCobro;
    }

    /**
     * @param adapterTipoCobro the adapterTipoCobro to set
     */
    public void setAdapterTipoCobro(TipoCobroListAdapter adapterTipoCobro) {
        this.adapterTipoCobro = adapterTipoCobro;
    }

    /**
     * @return the listTipoCobro
     */
    public List<SelectItem> getListTipoCobro() {
        return listTipoCobro;
    }

    /**
     * @param listTipoCobro the listTipoCobro to set
     */
    public void setListTipoCobro(List<SelectItem> listTipoCobro) {
        this.listTipoCobro = listTipoCobro;
    }

    /**
     * @return the cobroDetalle
     */
    public CobroDetalle getCobroDetalle() {
        return cobroDetalle;
    }

    /**
     * @param cobroDetalle the cobroDetalle to set
     */
    public void setCobroDetalle(CobroDetalle cobroDetalle) {
        this.cobroDetalle = cobroDetalle;
    }

    /**
     * @return the reciboDate
     */
    public Date getReciboDate() {
        return reciboDate;
    }

    /**
     * @param reciboDate the reciboDate to set
     */
    public void setReciboDate(Date reciboDate) {
        this.reciboDate = reciboDate;
    }

    /**
     * @return the reciboNum
     */
    public String getReciboNum() {
        return reciboNum;
    }

    /**
     * @param reciboNum the reciboNum to set
     */
    public void setReciboNum(String reciboNum) {
        this.reciboNum = reciboNum;
    }

    /**
     * @return the selectedLugarPago
     */
    public Integer getSelectedLugarPago() {
        return selectedLugarPago;
    }

    /**
     * @param selectedLugarPago the selectedLugarPago to set
     */
    public void setSelectedLugarPago(Integer selectedLugarPago) {
        this.selectedLugarPago = selectedLugarPago;
    }

    /**
     * @return the lugarCobro
     */
    public LugarCobro getLugarCobro() {
        return lugarCobro;
    }

    /**
     * @param lugarCobro the lugarCobro to set
     */
    public void setLugarCobro(LugarCobro lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    /**
     * @return the adapterLugarCobro
     */
    public LugarCobroListAdapter getAdapterLugarCobro() {
        return adapterLugarCobro;
    }

    /**
     * @param adapterLugarCobro the adapterLugarCobro to set
     */
    public void setAdapterLugarCobro(LugarCobroListAdapter adapterLugarCobro) {
        this.adapterLugarCobro = adapterLugarCobro;
    }

    /**
     * @return the listLugarCobro
     */
    public List<SelectItem> getListLugarCobro() {
        return listLugarCobro;
    }

    /**
     * @param listLugarCobro the listLugarCobro to set
     */
    public void setListLugarCobro(List<SelectItem> listLugarCobro) {
        this.listLugarCobro = listLugarCobro;
    }

    /**
     * @return the montoCobroTotal
     */
    public BigDecimal getMontoCobroTotal() {
        return montoCobroTotal;
    }

    /**
     * @param montoCobroTotal the montoCobroTotal to set
     */
    public void setMontoCobroTotal(BigDecimal montoCobroTotal) {
        this.montoCobroTotal = montoCobroTotal;
    }

    /**
     * @return the selectedCobros
     */
    public List<Cobro> getSelectedCobros() {
        return selectedCobros;
    }

    /**
     * @param selectedCobros the selectedCobros to set
     */
    public void setSelectedCobros(List<Cobro> selectedCobros) {
        this.selectedCobros = selectedCobros;
    }

    /**
     * @return the cobroClient
     */
    public CobroService getCobroClient() {
        return cobroClient;
    }

    /**
     * @param cobroClient the cobroClient to set
     */
    public void setCobroClient(CobroService cobroClient) {
        this.cobroClient = cobroClient;
    }

    /**
     * @return the selectedFacturas
     */
    public List<Factura> getSelectedFacturas() {
        return selectedFacturas;
    }

    /**
     * @param selectedFacturas the selectedFacturas to set
     */
    public void setSelectedFacturas(List<Factura> selectedFacturas) {
        this.selectedFacturas = selectedFacturas;
    }

    public void setAdapterFacturasCanceladas(FacturaMontoAdapter adapterFacturasCanceladas) {
        this.adapterFacturasCanceladas = adapterFacturasCanceladas;
    }

    public FacturaMontoAdapter getAdapterFacturasCanceladas() {
        return adapterFacturasCanceladas;
    }

    /**
     * @return the facturaPendientes
     */
    public Factura getFacturaPendientes() {
        return facturaPendientes;
    }

    /**
     * @param facturaPendientes the facturaPendientes to set
     */
    public void setFacturaPendientes(Factura facturaPendientes) {
        this.facturaPendientes = facturaPendientes;
    }

    /**
     * @return the facturaCanceladas
     */
    public Factura getFacturaCanceladas() {
        return facturaCanceladas;
    }

    /**
     * @param facturaCanceladas the facturaCanceladas to set
     */
    public void setFacturaCanceladas(Factura facturaCanceladas) {
        this.facturaCanceladas = facturaCanceladas;
    }

    public void setAdapterFacturasPendientes(FacturaMontoAdapter adapterFacturasPendientes) {
        this.adapterFacturasPendientes = adapterFacturasPendientes;
    }

    public FacturaMontoAdapter getAdapterFacturasPendientes() {
        return adapterFacturasPendientes;
    }

    /**
     * @return the cobro
     */
    public Cobro getCobro() {
        return cobro;
    }

    /**
     * @param cobro the cobro to set
     */
    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    /**
     * @return the viewPanel
     */
    public boolean isViewPanel() {
        return viewPanel;
    }

    /**
     * @param viewPanel the viewPanel to set
     */
    public void setViewPanel(boolean viewPanel) {
        this.viewPanel = viewPanel;
    }

    /**
     * @return the client
     */
    public Cliente getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Cliente client) {
        this.client = client;
    }

    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public PersonaEmail getPersonaEmail() {
        return personaEmail;
    }

    public void setPersonaEmail(PersonaEmail personaEmail) {
        this.personaEmail = personaEmail;
    }

    public PersonaEmailAdapter getAdapterPersonaEmail() {
        return adapterPersonaEmail;
    }

    public void setAdapterPersonaEmail(PersonaEmailAdapter adapterPersonaEmail) {
        this.adapterPersonaEmail = adapterPersonaEmail;
    }   

    public String getEmailNotificacion() {
        return emailNotificacion;
    }

    public void setEmailNotificacion(String emailNotificacion) {
        this.emailNotificacion = emailNotificacion;
    }
    
    public String getAddEmail() {
        return addEmail;
    }

    public void setAddEmail(String addEmail) {
        this.addEmail = addEmail;
    }

    public List<PersonaEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<PersonaEmail> emailList) {
        this.emailList = emailList;
    }

    public String getNotificar() {
        return notificar;
    }

    public void setNotificar(String notificar) {
        this.notificar = notificar;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Email> getSelectedEmail() {
        return selectedEmail;
    }

    public void setSelectedEmail(List<Email> selectedEmail) {
        this.selectedEmail = selectedEmail;
    }

    public ConfiguracionValorServiceClient getServiceConfiguracionValor() {
        return serviceConfiguracionValor;
    }

    public void setServiceConfiguracionValor(ConfiguracionValorServiceClient serviceConfiguracionValor) {
        this.serviceConfiguracionValor = serviceConfiguracionValor;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassMail() {
        return passMail;
    }

    public void setPassMail(String passMail) {
        this.passMail = passMail;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getNotificado() {
        return notificado;
    }

    public void setNotificado(String notificado) {
        this.notificado = notificado;
    }

    public ConfiguracionValorListAdapter getNotifConfig() {
        return notifConfig;
    }

    public void setNotifConfig(ConfiguracionValorListAdapter notifConfig) {
        this.notifConfig = notifConfig;
    }

    public ConfiguracionValorListAdapter getMailConfig() {
        return mailConfig;
    }

    public void setMailConfig(ConfiguracionValorListAdapter mailConfig) {
        this.mailConfig = mailConfig;
    }

    public ConfiguracionValorListAdapter getPassConfig() {
        return passConfig;
    }

    public void setPassConfig(ConfiguracionValorListAdapter passConfig) {
        this.passConfig = passConfig;
    }

    public ConfiguracionValorListAdapter getPortConfig() {
        return portConfig;
    }

    public void setPortConfig(ConfiguracionValorListAdapter portConfig) {
        this.portConfig = portConfig;
    }

    public ConfiguracionValorListAdapter getServerConfig() {
        return serverConfig;
    }

    public void setServerConfig(ConfiguracionValorListAdapter serverConfig) {
        this.serverConfig = serverConfig;
    }

    
    //</editor-fold>
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }
    
    /**
     * Método autocomplete personaEmail
     *
     * @param query
     * @return
     */
    public List<PersonaEmail> completeQueryEmail(String query) {
        personaEmail = new PersonaEmail();
        personaEmail.setIdPersona(client.getIdCliente());
        personaEmail.setActivo("S");
        adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmail);

        return adapterPersonaEmail.getData();
    }
    
    
    /**
     * Detalle de lista de facturas pendientes y canceladas del cliente
     * seleccionado
     */
    public void onItemSelectCliente() {
        
        Integer idCliente = client == null || client.getIdCliente() == null ? 0 : client.getIdCliente();

        viewPanel = true;
        facturaCanceladas.setIdCliente(idCliente);
        cobro.setIdCliente(idCliente);
        facturaCanceladas.setCobrado("S");
        facturaCanceladas.setAnulado("N");
        facturaCanceladas.setTieneSaldo("false");
        adapterFacturasCanceladas.getFilter().setIdCliente(idCliente);
        adapterFacturasCanceladas.setPageSize(adapterFacturasCanceladas.getPageSize());
        adapterFacturasCanceladas.setFirstResult(0);
        adapterFacturasCanceladas = adapterFacturasCanceladas.fillData(facturaCanceladas);
        adapterFacturasCanceladas.setFilter(facturaCanceladas);
        
        facturaPendientes.setIdCliente(idCliente);
        facturaPendientes.setCobrado("N");
        facturaPendientes.setAnulado("N");
        facturaPendientes.setTieneSaldo("true");
        adapterFacturasPendientes.getFilter().setIdCliente(idCliente);
        adapterFacturasPendientes.setPageSize(adapterFacturasPendientes.getPageSize());
        adapterFacturasPendientes.setFirstResult(0);
        
        selectedFacturas = new ArrayList();
        lazyModel.getFiltro().setIdCliente(idCliente);
        lazyModel.getFiltro().setCobrado("N");
        lazyModel.getFiltro().setAnulado("N");
        lazyModel.getFiltro().setTieneSaldo("true");
        lazyModel.load(0, 10,null, null, null);
        
        filterFacturasPendientes();
        if(notificar.equalsIgnoreCase("S")){
            personaEmail = new PersonaEmail();
            this.adapterPersonaEmail = new PersonaEmailAdapter();
            this.emailList = new ArrayList();
            this.selectedEmail = new ArrayList();
            personaEmail.setIdPersona(client.getIdCliente());
            personaEmail.setActivo("S");
            adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmail);
            this.emailList = adapterPersonaEmail.getData();
        }
    }
    
     public void onItemSelectEmail(){
         this.emailNotificacion = personaEmail.getEmail().getEmail();
     }

    public void obtenerDatosCobro() {
        try {

            Map parametros = new HashMap();
            datoCobro = new DatosCobro();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (cobro.getIdCliente() != null) {
                parametros.put("idCliente", cobro.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoCobro = getCobroClient().getDatoCobro(parametros);

            if (datoCobro != null) {
                cobro.setFecha(today.getTime());
                cobro.setIdTalonario(datoCobro.getIdTalonario());
                cobro.setNroRecibo(datoCobro.getNroRecibo());

                idTalonario = datoCobro.getIdTalonario();
                reciboDate = today.getTime();
                reciboNum = datoCobro.getNroRecibo();

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void filterFacturasPendientes() {
        adapterFacturasPendientes = adapterFacturasPendientes.fillData(facturaPendientes);
        adapterFacturasPendientes.setFilter(facturaPendientes);
    }

    public void clear() {
        Integer idCliente = client == null || client.getIdCliente() == null ? 0 : client.getIdCliente();
        facturaPendientes.setIdCliente(idCliente);
        facturaPendientes.setCobrado("N");
        facturaPendientes.setAnulado("N");
        facturaPendientes.setTieneSaldo("true");
        adapterFacturasPendientes.setFilter(facturaPendientes);
        facturaCanceladas.setIdCliente(idCliente);
        facturaCanceladas.setCobrado("S");
        facturaCanceladas.setAnulado("N");
        facturaCanceladas.setTieneSaldo("false");
        adapterFacturasCanceladas.setFilter(facturaCanceladas);
        lazyModel.getFiltro().setIdCliente(idCliente);
        lazyModel.getFiltro().setCobrado("N");
        lazyModel.getFiltro().setAnulado("N");
        lazyModel.getFiltro().setTieneSaldo("true");
        filterFacturasPendientes();
    }

    /**
     * Método invocado al cambiar el monto a cobrar
     *
     * @param event Evento producido al cambiar el monto a cobrar
     */
    public void onChangeChargeAmount(ValueChangeEvent event) {
        WebLogger.get().debug("ENTRA CHEQUE");

        Map att = event.getComponent().getAttributes();
        String invId = att.get("idFac") == null ? "" : att.get("idFac").toString();

        BigDecimal oldVal = new BigDecimal(event.getOldValue() == null ? "0"
                : event.getOldValue().toString());
        BigDecimal newVal = new BigDecimal(event.getNewValue() == null ? "0"
                : event.getNewValue().toString());

        for (int i = 0; i < selectedCobros.size(); i++) {

            if (selectedCobros.get(i).getIdFactura().equals(new Integer(invId))) {
                selectedCobros.get(i).setSaldoCliente(selectedCobros.get(i).
                        getSaldoCliente().add(oldVal));
                selectedCobros.get(i).setSaldoCliente(selectedCobros.get(i).
                        getSaldoCliente().subtract(newVal));
                
                montoCobroTotal = montoCobroTotal.subtract(oldVal);
                montoCobroTotal = montoCobroTotal.add(newVal);
                 if (!selectedCobros.get(i).getCodigoMoneda().equalsIgnoreCase("PYG")){
                    BigDecimal oldValGs = oldVal.multiply(tipoCambio.getVenta());
                    BigDecimal newValGs = newVal.multiply(tipoCambio.getVenta());
                    
                   montoCobroTotalGs = montoCobroTotalGs.subtract(oldValGs);
                   montoCobroTotalGs = montoCobroTotalGs.add(newValGs);

                }
            }
              
        }
         
    }
    
    public void actualizarDetalleGs(AjaxBehaviorEvent event){
        FacesContext context = FacesContext.getCurrentInstance();
        UIComponent component = event.getComponent();
        Integer idFac = (Integer) component.getAttributes().get("idFac");
        
        for (Cobro cob : selectedCobros){
            if (!cob.getCodigoMoneda().equalsIgnoreCase("PYG")) {
                if (Objects.equals(cob.getIdFactura(), idFac)){
                for (CobroDetalle cd : cob.getCobroDetalles()){
                    cd.setMontoCobroGuaranies(cd.getMontoCobro().multiply(tipoCambio.getVenta()));
                }
            }
            }
            
        }
    }

    /**
     * Método para anular un talonario
     */
    /* public void anularRecibo() {
       
        Cobro cobro_anular = new Cobro();
        cobro_anular.setIdLugarCobro(1);
        cobro_anular.setIdTalonario(cobro.getIdTalonario());
        cobro_anular.setNroRecibo(cobro.getNroRecibo());
        cobro_anular.setFecha(Calendar.getInstance().getTime());
        cobro_anular.setMontoCobro(BigDecimal.ZERO);
        cobro_anular.setEstado("A");

        BodyResponse idCobro = cobroClient.setCobro(cobro_anular);

        if (idCobro != null) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Recibo Anulado!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
       
    }*/
    /**
     * Método para ver el detalle de cobro, retención y nota de crédito
     *
     * @param factura
     */
    public void viewCobroDet(Factura factura) {

        nroFactura = factura.getNroFactura();

        cobroDetalle.setIdFactura(factura.getId());
        //cobroDetalle.setEstado("C");
        Estado estado = new Estado();
        estado.setActivo("C");
        cobroDetalle.setEstado(estado);
        adapterCobroDetalle.setPageSize(adapterCobroDetalle.getPageSize());
        adapterCobroDetalle.setFirstResult(0);
        adapterCobroDetalle = adapterCobroDetalle.fillData(cobroDetalle);

        /*retencionDetalle.setIdFactura(factura.getId());
        adapterRetencionDetalle.setPageSize(adapterRetencionDetalle.getPageSize());
        adapterRetencionDetalle.setFirstResult(0);
        adapterRetencionDetalle = adapterRetencionDetalle.fillData(retencionDetalle);*/
        notaCreditoDetalle.setIdFactura(factura.getId());
        adapterNotaCreditoDetalle.setPageSize(adapterNotaCreditoDetalle.getPageSize());
        adapterNotaCreditoDetalle.setFirstResult(0);
        adapterNotaCreditoDetalle = adapterNotaCreditoDetalle.fillData(notaCreditoDetalle);
    }


    /**
     * Método para cargar datos basicos en el panel cobro
     *
     */
    public void viewCobro() {
        cobroEnGs = false;
        selectedCobros = new ArrayList<>();
        montoCobroTotal = BigDecimal.ZERO;
        montoCobroTotalGs = BigDecimal.ZERO;
        listLugarCobro = new ArrayList<>();

        adapterLugarCobro = adapterLugarCobro.fillData(lugarCobro);

        for (int i = 0; i < adapterLugarCobro.getData().size(); i++) {

            listLugarCobro.add(new SelectItem(adapterLugarCobro.
                    getData().get(i).getId(), adapterLugarCobro.getData()
                    .get(i).getCodigo()));

        }
        selectedLugarPago = (Integer) listLugarCobro.get(0).getValue();

        for (int i = 0; i < selectedFacturas.size(); i++) {
            cobro = new Cobro();
            cobro.setIdTalonario(idTalonario);
            cobro.setFecha(reciboDate);
            cobro.setNroRecibo(reciboNum);
            
            cobro.setNroFactura(selectedFacturas.get(i).getNroFactura());
            cobro.setSaldoCliente(BigDecimal.ZERO);

            cobro.setTotalFactura(selectedFacturas.get(i).getMontoTotalFactura());
            cobro.setIdFactura(selectedFacturas.get(i).getId());
            cobro.setMontoRetenidoTotal(BigDecimal.ZERO);
            cobro.setCodigoMoneda(selectedFacturas.get(i).getCodigoMoneda());
            monedaFilter = new Moneda();
            monedaFilter.setCodigo(selectedFacturas.get(i).getCodigoMoneda());
            adapterMoneda = adapterMoneda.fillData(monedaFilter);
            
            if (!adapterMoneda.getData().isEmpty()){
                monedaFilter = adapterMoneda.getData().get(0);
            }
            else {
                monedaFilter.setDescripcion("Guaranies");
            }
            cobro.setFecha(reciboDate);

            cobro.setCobroDetalles(new ArrayList<>());
            cobroDetalle = new CobroDetalle();
            cobroDetalle.setNroLinea(i + 1);
            cobroDetalle.setIdFactura(selectedFacturas.get(i).getId());
            
            if (!selectedFacturas.get(i).getCodigoMoneda().equalsIgnoreCase("PYG")){
                TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
                TipoCambio tipoCambioFilter = new TipoCambio();

                tipoCambioFilter.setId(selectedFacturas.get(i).getIdTipoCambio());
                adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);

                this.tipoCambio = adapterTipoCambio.getData().get(0);
                
                BigDecimal precioUnitarioCambio = selectedFacturas.get(i).getSaldo().divide(tipoCambio.getVenta(),2,RoundingMode.HALF_UP);
                cobroDetalle.setMontoCobro(precioUnitarioCambio);
                cobroDetalle.setMontoCobroGuaranies(selectedFacturas.get(i).getSaldo());
                cobro.setIdTipoCambio(selectedFacturas.get(i).getIdTipoCambio());
                montoCobroTotalGs = montoCobroTotalGs.add(cobroDetalle.getMontoCobroGuaranies());
            } else {
                cobroDetalle.setMontoCobro(selectedFacturas.get(i).getSaldo());
            }
            montoCobroTotal = montoCobroTotal.add(cobroDetalle.getMontoCobro());
            cobroDetalle.setIdTipoCobro((Integer) listTipoCobro.get(0).getValue());
            cobroDetalle.setCodigoTipoCobro(listTipoCobro.get(0).getLabel());
            cobroDetalle.setCodigoConceptoCobro("OTROS");
            cobroDetalle.setDescripcion(String.format("Pago de Factura Nro. %s ",selectedFacturas.get(i).getNroFactura()));

            cobro.getCobroDetalles().add(cobroDetalle);
            selectedCobros.add(cobro);
        }

    }
    
    /**
     * Método para cargar cobros en guaraníes para facturas en moneda extranjera
     */
    public void cobroEnGuaranies() {
        BigDecimal totalGs = BigDecimal.ZERO;
        for (Cobro c : selectedCobros) {
            c.setCodigoMoneda("PYG");
            c.setIdTipoCambio(null);
            for (CobroDetalle cd : c.getCobroDetalles()) {
                BigDecimal MontoGs = cd.getMontoCobroGuaranies();
                cd.setMontoCobro(MontoGs);
                cd.setMontoCobroGuaranies(null);
                totalGs = totalGs.add(MontoGs);
            }
        }
        monedaFilter = new Moneda();
        monedaFilter.setCodigo("PYG");
        montoCobroTotal = totalGs;
        montoCobroTotalGs = null;
        cobroEnGs = true;
    }

    /**
     * Método para borrar linea de detalle de cobro
     */
    public void codTipoCobro(Integer idFactura, Integer nroLinea) {
        for (int i = 0; i < selectedCobros.size(); i++) {
            if (selectedCobros.get(i).getIdFactura() == idFactura) {
                for (int j = 0; j < selectedCobros.get(i).getCobroDetalles().size(); j++) {
                    CobroDetalle cd = selectedCobros.get(i).getCobroDetalles().get(j);
                    if (cd.getNroLinea() == nroLinea) {
                        cd.setCodigoTipoCobro(getCodTipoCobro(cd.getIdTipoCobro()));
                        if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                            Calendar today = Calendar.getInstance();
                            Cheque ch = new Cheque();
                            EntidadFinanciera ef = new EntidadFinanciera();
                            ch.setOmitirValidacionCheque(omitirValidacionCheque);
                            ch.setMontoCheque(BigDecimal.ZERO);
                            ch.setNroCheque("");
                            ch.setFechaEmision(today.getTime());
                            ch.setFechaPago(today.getTime());
                            cd.setCheque(ch);
                            cd.setEntidadFinanciera(ef);
                            //  cd.setIdEntidadFinanciera((Integer) listEntidadFinanciera.get(0).getValue());
                        } else {
                            cd.setCheque(null);
                        }

                    }
                }

            }
        }
    }
    
    /**
     * Método para borrar linea de detalle de cobro
     */
    public void agregarCheque(Integer idFactura, Integer nroLinea) {
        for (int i = 0; i < selectedCobros.size(); i++) {
            if (selectedCobros.get(i).getIdFactura() == idFactura) {
                for (int j = 0; j < selectedCobros.get(i).getCobroDetalles().size(); j++) {
                    CobroDetalle cd = selectedCobros.get(i).getCobroDetalles().get(j);
                    if (cd.getNroLinea() == nroLinea) {
                        cd.setCodigoTipoCobro(getCodTipoCobro(cd.getIdTipoCobro()));
                    }
                }

            }
        }
    }

    /**
     * Método para obtener el tipo de cobro
     *
     * @param idTipoCobro
     * @return
     */
    public String getCodTipoCobro(Integer idTipoCobro) {
        tipoCobro = new TipoCobro();
        tipoCobro.setId(idTipoCobro);
        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        return adapterTipoCobro.getData().get(0).getCodigo();
    }

    /**
     * Método para borrar linea de detalle de cobro
     */
    public void delLine() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String invId = (String) params.get("idF");
        String nroLinea = (String) params.get("nro");
        for (int i = 0; i < selectedCobros.size(); i++) {
            if (selectedCobros.get(i).getIdFactura() == Integer.parseInt(invId)) {
                if (selectedCobros.get(i).getCobroDetalles().size() == 1) {
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                            "Debe tener al menos un detalle!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                } else if (selectedCobros.get(i).getCobroDetalles().size() > 1) {
                    for (int j = 0; j < selectedCobros.get(i).getCobroDetalles().size(); j++) {
                        CobroDetalle cd = selectedCobros.get(i).getCobroDetalles().get(j);
                        if (cd.getNroLinea() == Integer.parseInt(nroLinea)) {
                            montoCobroTotal = montoCobroTotal.subtract(cd.getMontoCobro());
                            selectedCobros.get(i).setSaldoCliente(selectedCobros.get(i).getSaldoCliente().add(cd.getMontoCobro()));
                            selectedCobros.get(i).getCobroDetalles().remove(cd);
                        }
                    }
                }

            }
        }
    }

    /**
     * Método para agregar linea al detalle de cobro
     */
    public void addLine() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String invId = (String) params.get("idFact");

        for (int i = 0; i < selectedCobros.size(); i++) {
            if (selectedCobros.get(i).getIdFactura() == Integer.parseInt(invId)) {
                cobroDetalle = new CobroDetalle();
                cobroDetalle.setNroLinea(selectedCobros.get(i).getCobroDetalles().size() + 1);
                cobroDetalle.setIdFactura(selectedCobros.get(i).getCobroDetalles().get(0).getIdFactura());
                cobroDetalle.setMontoCobro(BigDecimal.ZERO);
                cobroDetalle.setIdTipoCobro((Integer) listTipoCobro.get(0).getValue());
                cobroDetalle.setCodigoTipoCobro(listTipoCobro.get(0).getLabel());
                Estado estadoDetalle = new Estado();
                estadoDetalle.setActivo("C");
                cobro.setEstado(estadoDetalle);
                cobro.setCodigoEstado("COBRADO");

                cobroDetalle.setEstado(estadoDetalle);
                cobroDetalle.setCodigoEstado("COBRADO");
                cobroDetalle.setCodigoConceptoCobro("OTROS");
                cobroDetalle.setDescripcion(String.format("Pago de Factura Nro. %s ",selectedFacturas.get(i).getNroFactura()));
                selectedCobros.get(i).getCobroDetalles().add(cobroDetalle);
            }
        }
    }
    
    public void obtenerConfiguracionNotificacionEmail() {
        ConfiguracionValor confVal = new ConfiguracionValor();
        confVal.setIdEmpresa(session.getUser().getIdEmpresa());
        

        notifConfig = new ConfiguracionValorListAdapter();
        mailConfig = new ConfiguracionValorListAdapter();
        passConfig = new ConfiguracionValorListAdapter();
        portConfig = new ConfiguracionValorListAdapter();
        serverConfig = new ConfiguracionValorListAdapter();
        boolean save = true;
        confVal.setCodigoConfiguracion("NOTIFICACION_RECIBO_EMAIL");
        notifConfig = notifConfig.fillData(confVal);
        if (notifConfig.getData() != null && notifConfig.getData().size() > 0 && !notifConfig.getData().get(0).getValor().trim().isEmpty()) {
            if (!notifConfig.getData().get(0).getValor().equalsIgnoreCase("N") && !notifConfig.getData().get(0).getActivo().equalsIgnoreCase("N")) {
                confVal.setActivo("S");
                
                confVal.setCodigoConfiguracion("MAIL_SENDER");
                mailConfig = mailConfig.fillData(confVal);
                if (mailConfig.getData() != null && mailConfig.getData().size() > 0 && !mailConfig.getData().get(0).getValor().trim().isEmpty()) {
                    mail = mailConfig.getData().get(0).getValor();
                } else {
                    mail = new String();
                    save = false;
                }

                confVal.setCodigoConfiguracion("PASS_MAIL_SENDER");
                passConfig = passConfig.fillData(confVal);
                if (passConfig.getData() != null && passConfig.getData().size() > 0 && !passConfig.getData().get(0).getValor().trim().isEmpty()) {
                    passMail = passConfig.getData().get(0).getValor();
                } else {
                    passMail = new String();
                    save = false;
                }

                confVal.setCodigoConfiguracion("PUERTO_CORREO_SALIENTE");
                portConfig = portConfig.fillData(confVal);
                if (portConfig.getData() != null && portConfig.getData().size() > 0 && !portConfig.getData().get(0).getValor().trim().isEmpty()) {
                    port = portConfig.getData().get(0).getValor();
                } else {
                    port = new String();
                    save = false;
                }

                confVal.setCodigoConfiguracion("SERVIDOR_CORREO_SALIENTE");
                serverConfig = serverConfig.fillData(confVal);
                if (serverConfig.getData() != null && serverConfig.getData().size() > 0 && !serverConfig.getData().get(0).getValor().trim().isEmpty()) {
                    server = serverConfig.getData().get(0).getValor();
                } else {
                    server = new String();
                    save = false;
                }
            }

        } else {
            ConfiguracionValor confValN = new ConfiguracionValor();
            confValN.setActivo("S");
            confValN.setIdEmpresa(session.getUser().getIdEmpresa());
            confValN.setValor("N");
            confValN.setCodigoConfiguracion("NOTIFICACION_RECIBO_EMAIL");
            serviceConfiguracionValor.createConfiguracionValor(confValN);
        }

        if (!save) {
            PF.current().executeScript("$('#notificar').modal('show');");
        }
    }
    
    public void selectNotificar(String mail, String passMail, String port, String server) {
        ConfiguracionValor confVal = new ConfiguracionValor();
        confVal.setActivo("S");
        confVal.setIdEmpresa(session.getUser().getIdEmpresa());
        ConfiguracionValor confValMail = new ConfiguracionValor();
        ConfiguracionValor confValPass = new ConfiguracionValor();
        ConfiguracionValor confValPort = new ConfiguracionValor();
        ConfiguracionValor confValServer = new ConfiguracionValor();
        
        boolean save = false;
        
        if (mailConfig.getData().size() > 0) {
            mailConfig.getData().get(0).setValor(mail);
            confValMail = serviceConfiguracionValor.editConfiguracionValor(mailConfig.getData().get(0));
            if (!confValMail.getValor().trim().isEmpty() && confValMail.getValor() != null){
              save = true;
            }else{
              save = false;
            }
        }else{
            confVal.setCodigoConfiguracion("MAIL_SENDER");
            confVal.setValor(mail);
            BodyResponse responseMail = serviceConfiguracionValor.createConfiguracionValor(confVal);
            if(responseMail.getSuccess()){
                save = true;
            }else{
              save = false;
            }
        }
        
        if (passConfig.getData().size() > 0) {
            passConfig.getData().get(0).setValor(passMail);
            confValPass = serviceConfiguracionValor.editConfiguracionValor(passConfig.getData().get(0));
            if (!confValPass.getValor().trim().isEmpty() && confValPass.getValor() != null){
              save = true;
            }else{
              save = false;
            }
        }else{
            confVal.setCodigoConfiguracion("PASS_MAIL_SENDER");
            confVal.setValor(passMail);
            BodyResponse responsePass = serviceConfiguracionValor.createConfiguracionValor(confVal);
            if(responsePass.getSuccess()){
                save = true;
            }else{
              save = false;
            }
        }
        
        if (portConfig.getData().size() > 0) {
            portConfig.getData().get(0).setValor(port);
            confValPort = serviceConfiguracionValor.editConfiguracionValor(portConfig.getData().get(0));
            if (!confValPort.getValor().trim().isEmpty() && confValPort.getValor() != null){
              save = true;
            }else{
              save = false;
            }
        }else{
            confVal.setCodigoConfiguracion("PUERTO_CORREO_SALIENTE");
            confVal.setValor(port);
            BodyResponse responsePort = serviceConfiguracionValor.createConfiguracionValor(confVal);
            if(responsePort.getSuccess()){
                save = true;
            }else{
              save = false;
            }
        }

        if (serverConfig.getData().size() > 0) {
            serverConfig.getData().get(0).setValor(server);
            confValServer = serviceConfiguracionValor.editConfiguracionValor(serverConfig.getData().get(0));
            if (!confValServer.getValor().trim().isEmpty() && confValServer.getValor() != null){
              save = true;
            }else{
              save = false;
            }
        }else{
            confVal.setCodigoConfiguracion("SERVIDOR_CORREO_SALIENTE");
            confVal.setValor(server);
            BodyResponse responseServer = serviceConfiguracionValor.createConfiguracionValor(confVal);
            if(responseServer.getSuccess()){
                save = true;
            }else{
              save = false;
            }
        }
        
        if(save){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se actualizarón correctamente las configuraciones de notificaciones"));
        }else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Hubo un error al actualizar las configuraciones de notificaciones"));
        }
    }
    
    public boolean validarFormatoCorreo(String email) {
        boolean addEmail = false;
         
        email = email.trim().replace(" ", "");
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z]{2,})?$");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            String msg = String.format("%s no es un email válido", email);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        return addEmail;
    }

    /**
     * Verifica si la cadena es convertible a un valor numerico
     */
    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
    
//    /**
//     * Metodo para validar que las facturas seleccionadas sean del mismo tipo de moneda
//     */
    public void validarMoneda(SelectEvent event) {
        Factura selectedFactura = (Factura) event.getObject();
       List<Factura> listaAuxiliar = selectedFacturas;
        if (!selectedFacturas.isEmpty()) {
            for (Factura fac : listaAuxiliar) {
                if (!fac.getCodigoMoneda().equalsIgnoreCase(selectedFactura.getCodigoMoneda())) {
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                            "Debe seleccionar facturas con el mismo tipo de moneda.");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    Iterator<Factura> iterator = listaAuxiliar.iterator();
                    while (iterator.hasNext()) {
                        Factura factura = iterator.next();
                        if (Objects.equals(factura.getId(), selectedFactura.getId())) {
                            iterator.remove();
                        }
                    }
                    selectedFacturas = listaAuxiliar;
                    break;
                }
            }          
        }
    }

        /**
     * Metodo para validar que las facturas seleccionadas sean del mismo tipo de moneda
     */
//    public void validarMoneda(Integer rowIndex) {
//        if (!selectedFacturas.isEmpty()) {
//            Factura selectedFactura = selectedFacturas.get(rowIndex);
//            for (Factura fac : selectedFacturas) {
//                if (!fac.getCodigoMoneda().equalsIgnoreCase(selectedFactura.getCodigoMoneda())) {
//                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
//                            "Debe seleccionar facturas con el mismo tipo de moneda.");
//                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
//                    PrimeFaces.current().executeScript("var checkbox = $(\"input[type='checkbox']\").eq(" + rowIndex + "); checkbox.prop('checked', false);");
//                    selectedFacturas.remove(selectedFactura);
//                    break;
//                }
//            }
//        }
//    }
    /**
     * Método para cargar Cobro
     */
    public void addCobro() {

        boolean checkErrorMsn = false;

        for (int i = 0; i < selectedCobros.size(); i++) {

            for (int j = 0; j < selectedCobros.get(i).getCobroDetalles().size(); j++) {

                if (selectedCobros.get(i).getCobroDetalles().get(j).getCodigoTipoCobro().equals("CHEQUE") && (this.omitirValidacionCheque == null || this.omitirValidacionCheque.equalsIgnoreCase("N")) ) {
                    if (selectedCobros.get(i).getCobroDetalles().get(j).getCheque().getNroCheque().equals("")
                            || selectedCobros.get(i).getCobroDetalles().get(j).getCheque().getMontoCheque().
                                    compareTo(BigDecimal.ZERO) <= 0 || selectedCobros.get(i).getCobroDetalles().get(j).getCheque().getFechaEmision() == null
                            || selectedCobros.get(i).getCobroDetalles().get(j).getCheque().getFechaPago() == null) {
                        
                        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                                "El Nº de cheque, el monto, la fecha del cheque y la fecha del pago del cheque son requeridos");
                        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                        checkErrorMsn = true;
                    }
                }
            }
        }

        try {
            List<CobroDetalle> cdFinal = new ArrayList<>();
            if (checkErrorMsn == false) {
                BodyResponse idCobro = null;

                for (int i = 0; i < selectedCobros.size(); i++) {
                    cobro.setFecha(reciboDate);
                    cobro.setIdCliente(client.getIdCliente());
                    if (cobroPorTotal) {
                        cobro.setMontoCobroFicticio(montoCobroTotal);
                        cobro.setMontoCobro(BigDecimal.ZERO);
                    } else {
                        cobro.setMontoCobro(montoCobroTotal);
                        cobro.setMontoCobroFicticio(BigDecimal.ZERO);
                    }
                    
                    cobro.setNroRecibo(reciboNum);
               
                    lugarCobro.setId(cobro.getIdLugarCobro());
                    adapterLugarCobro = adapterLugarCobro.fillData(lugarCobro);
                    if (adapterLugarCobro != null) {
                        cobro.setCodigoLugarCobro(adapterLugarCobro.getData().get(0).getCodigo());
                    }

                    Estado estadoDetalle = new Estado();
                    estadoDetalle.setActivo("C");
                    cobro.setEstado(estadoDetalle);
                    cobro.setEnviado('N');
                    if(digital.equalsIgnoreCase("N")){
                         cobro.setDigital("N");
                    }
                    else {
                        cobro.setDigital("S");
                    }   
                    cobro.setCodigoEstado("COBRADO");
                    for (CobroDetalle cd : cobro.getCobroDetalles()) {
                        if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                            cd.getCheque().setIdEntidadFinanciera(cd.getIdEntidadFinanciera());
                            cd.setEstado(estadoDetalle);
                            cd.setCodigoEstado("COBRADO");
                            cd.setCodigoConceptoCobro("OTROS");
                        } else {
                            cd.setEstado(estadoDetalle);
                            cd.setCodigoEstado("COBRADO");
                            cd.setCodigoConceptoCobro("OTROS");
                        }
                    }
                    for (CobroDetalle cdTest : selectedCobros.get(i).getCobroDetalles()) {
                        cdTest.setCodigoEstado("COBRADO");
                        cdFinal.add(cdTest);
                    }

                    /* Integer concepto = cobro.getSaldoCliente().intValue() > 0 ? 1 : 2;
                    cobroDetalle.setIdConceptoCobro(concepto);*/
                }
                cobro.setCobroDetalles(cdFinal);
                BodyResponse<Cobro> respuestaDeCobro = cobroClient.setCobro(cobro);
                if (respuestaDeCobro.getSuccess() == true) {
                        Cobro cnuevo = new Cobro();
                        cnuevo = (Cobro) respuestaDeCobro.getPayload();
                        idCobroCreado = cnuevo.getId();
                        nroCobroCreads = cnuevo.getNroRecibo();
                    if (obtenerConfiguracionDescarga().equalsIgnoreCase("S")) {
                        mostrarPanelDescarga(); 
                    }
                    if (notificar.equalsIgnoreCase("S")) {
                        if (selectedEmail.size()>0){
                            for(Email email : selectedEmail){
                                notificar(email.getEmail());
                            }
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado la factura a la dirección confirmada"));
                        }
                    }

                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Se realizo el cobro correctamente!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    actualizarFacturas();
                    PF.current().executeScript("$('#modalCobro').modal('hide');");
                    viewPanel = false;
                }

            }

        } catch (Exception ex) {
            WebLogger.get().error("Se produjo un error al agregar el cargo" + ex);
        }
    }

    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }
    
    public void onDateSelect(SelectEvent event) {
        Date selectedDate = (Date) event.getObject();

        // Obtener la hora actual
        Calendar now = Calendar.getInstance();

        // Pasar la fecha seleccionada a un calendario
        Calendar selectedCal = Calendar.getInstance();
        selectedCal.setTime(selectedDate);

        // Asignar la hora, minuto y segundo actuales
        selectedCal.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
        selectedCal.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
        selectedCal.set(Calendar.SECOND, now.get(Calendar.SECOND));

        this.reciboDate = selectedCal.getTime();

    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroCobroCreads + ".pdf";
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("hideStatus();printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     *
     * @return
     */
    public String obtenerConfiguracionDescarga() {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion("DESCARGAR_RECIBO");
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "N";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }

    public void filterEntidadFinanciera() {
        adapterEntidadFinanciera = adapterEntidadFinanciera.fillData(entidadFinanciera);
    }

    /**
     * Método para actualizar lista
     */
    public void actualizarFacturas() {
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.cobro = new Cobro();
        this.selectedFacturas = new ArrayList<>();
        this.cobroClient = new CobroService();
        this.selectedCobros = new ArrayList<>();
        this.adapterLugarCobro = new LugarCobroListAdapter();
        this.lugarCobro = new LugarCobro();
        this.cobroDetalle = new CobroDetalle();
        this.adapterTipoCobro = new TipoCobroListAdapter();
        this.tipoCobro = new TipoCobro();
        this.adapterEntidadFinanciera = new EntidadFinancieraListAdapter();
        this.entidadFinanciera = new EntidadFinanciera();
        this.adapterCobroDetalle = new CobroDetalleListAdapter();
        this.notaCreditoDetalle = new NotaCreditoDetalle();
        this.adapterNotaCreditoDetalle = new NotaCreditoDetalleListAdapter();

        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        for (int i = 0; i < adapterTipoCobro.getData().size(); i++) {
            listTipoCobro.add(new SelectItem(adapterTipoCobro.getData().get(i).
                    getId(), adapterTipoCobro.getData().get(i).getCodigo()));
        }

        filterEntidadFinanciera();
        obtenerDatosTalonario();
        PF.current().ajax().update(":cobro-form:form-data:cliente");
        PF.current().ajax().update(":cobro-form:form-data:grupo");
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<EntidadFinanciera> completeQueryEntidad(String query) {

        entidadFinanciera = new EntidadFinanciera();
        entidadFinanciera.setRazonSocial(query);
        adapterEntidadFinanciera.setPageSize(20);
        adapterEntidadFinanciera = adapterEntidadFinanciera.fillData(entidadFinanciera);

        return adapterEntidadFinanciera.getData();
    }
    
    /**
     * Método para obtener el cliente
     *
     * @param event
     * @param idFactura
     * @param nroLinea
     */
    public void onItemSelectEntidadFinanciera(Integer idFactura, Integer nroLinea) {
        WebLogger.get().debug("ENTRA ENTI");
        for (int i = 0; i < selectedCobros.size(); i++) {
            if (selectedCobros.get(i).getIdFactura() == idFactura) {
                for (int j = 0; j < selectedCobros.get(i).getCobroDetalles().size(); j++) {
                    CobroDetalle cd = selectedCobros.get(i).getCobroDetalles().get(j);
                    if (cd.getNroLinea() == nroLinea) {
                        cd.setIdEntidadFinanciera(selectedCobros.get(i).getCobroDetalles().get(j).getEntidadFinanciera().getIdEntidadFinanciera());
                        if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                            cd.getCheque().setIdEntidadFinanciera(cd.getIdEntidadFinanciera());
                        }
                        int a = cd.getIdEntidadFinanciera();
                        WebLogger.get().debug("id entidad financiera" + a);
                    }
                }

            }
        }

    }
    
    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_RECIBO_DIGITAL");
            configuracionValorFilter.setActivo("S");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            } else {
                this.digital = "N";
                this.digital = adapterConfigValor.getData().get(0).getValor();
            }
            obtenerDatosTalonario();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void obtenerDatosTalonario() {
        cobroTalonario.setDigital(digital);
        //cobroTalonario.setIdUsuario(session.getUser().getId());
        adapterCobroTalonario = adapterCobroTalonario.fillData(cobroTalonario);
        List<TalonarioPojo> talonarios = new ArrayList();
        if (adapterCobroTalonario.getData().size() > 1) {
            LocalDate fechaHoy = LocalDate.now();
            for(TalonarioPojo tp :adapterCobroTalonario.getData()){
                if (tp.getFechaVencimiento() != null){
                    LocalDate fechaAComparar = tp.getFechaVencimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if (fechaAComparar.isAfter(fechaHoy)) {
                        talonarios.add(tp);
                    }
                }
                else {
                    talonarios.add(tp);
                }
            }
            if(talonarios.size()>1) {
                adapterCobroTalonario.setData(talonarios);
                showTalonarioPopup = true;
                PF.current().ajax().update(":cobro-form:form-data:pnl-pop-up");
            }
            else if(talonarios.size()>0) {
                talonario.setId(adapterCobroTalonario.getData().get(0).getId());
                obtenerDatosCobro();
            }
        } else {
            talonario.setId(adapterCobroTalonario.getData().get(0).getId());
            obtenerDatosCobro();
        }
    }

    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }
    
       /**
     * Método para obtener la configuración de Notificación por email
     */
//    public void obtenerConfiguracionNotificacion() {
//        try {
//            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
//            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
//            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
//            configuracionValorFilter.setActivo("S");
//            configuracionValorFilter.setCodigoConfiguracion("NOTIFICACION_RECIBO_EMAIL");
//            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
//
//            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
//                this.notificar = "N";
//            } else {
//                this.notificar = adapterConfigValor.getData().get(0).getValor();
//            }
//
//        } catch (Exception e) {
//            WebLogger.get().fatal(e);
//        }
//    }

    public void notificar(String emailDestino) {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String cliente = client.getRazonSocial();
        String empresa = session.getNombreEmpresa();
        String mailTo = emailDestino;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su recibo en formato PDF";
        String subjet = "Recibo No. " + cobro.getNroRecibo();
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);
        String fileName = nroCobroCreads + ".pdf";


        notificarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, cobro.getNroRecibo(), host, port, empresa, emailEmpresa, telefonoEmpresa);

        
    }
    
    
    public void notificarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host,port);  
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
    
    /**
     * Elimina un email de la lista de emails para notificar
     */
    public void eliminarEmail(int index){
        this.selectedEmail.remove(index);
    }
    
    /**
     * Agrega un email desde la lista de emails
     * @param email 
     */
    public void agregarEmail(Email e){
        if(selectedEmail.contains(e)){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "El email ya está seleccionado para el envío de recibo."));
        }
        else{
            this.selectedEmail.add(e);
        }
    }
    
    /**
     * Agrega un email desde el inputText
     */
    public void agregarNuevoEmail(){
        if (validarCorreo(this.addEmail)){
            Email e = new Email();
            e.setEmail(addEmail);
            this.selectedEmail.add(e);
            this.addEmail="";
        }
    }
    
     public void onRowEditEmail(RowEditEvent event){
         System.out.println(((Email) event.getObject()).getEmail());
          if (!validarCorreo(((Email) event.getObject()).getEmail())){
              FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El formato del email ingresado no es válido.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                PF.current().ajax().update(":message-form:message");
                //throw new ValidatorException(message);
          }
     }
         
    
      public Boolean validarCorreo(String email) {

        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+", Pattern.CASE_INSENSITIVE);

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            
            WebLogger.get().debug("El email ingresado es válido.");
            return true;
        } else {
            
            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            PF.current().ajax().update(":message-form:message");
            return false;
        }
    }
      
    
        /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {
        this.notificar = "S";
        //obtenerConfiguracionNotificacion();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.cobro = new Cobro();
        this.facturaCanceladas = new Factura();
        this.facturaPendientes = new Factura();
        this.adapterFacturasPendientes = new FacturaMontoAdapter();
        this.adapterFacturasCanceladas = new FacturaMontoAdapter();
        this.selectedFacturas = new ArrayList<>();
        this.cobroClient = new CobroService();
        this.selectedCobros = new ArrayList<>();
        this.adapterLugarCobro = new LugarCobroListAdapter();
        this.lugarCobro = new LugarCobro();
        this.cobroDetalle = new CobroDetalle();
        this.adapterTipoCobro = new TipoCobroListAdapter();
        this.tipoCobro = new TipoCobro();
        this.adapterEntidadFinanciera = new EntidadFinancieraListAdapter();
        this.entidadFinanciera = new EntidadFinanciera();
        this.adapterCobroDetalle = new CobroDetalleListAdapter();
        this.notaCreditoDetalle = new NotaCreditoDetalle();
        this.adapterNotaCreditoDetalle = new NotaCreditoDetalleListAdapter();
        this.showDescargaPopup = false;
        this.idCobroCreado = null;
        this.nroCobroCreads = null;
        this.fileServiceClient = new FileServiceClient();
        this.entFinanSelected = new EntidadFinanciera();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        this.monedaFilter.setDescripcion("Guaranies");
        this.mail = null;
        this.passMail = null;
        this.notifConfig = new ConfiguracionValorListAdapter();
        this.mailConfig = new ConfiguracionValorListAdapter();
        this.passConfig = new ConfiguracionValorListAdapter();
        this.portConfig = new ConfiguracionValorListAdapter();
        this.serverConfig = new ConfiguracionValorListAdapter();
        this.serviceConfiguracionValor = new ConfiguracionValorServiceClient();
        this.port = null;
        this.server = null;
        this.notificado = null;
        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        for (int i = 0; i < adapterTipoCobro.getData().size(); i++) {
            listTipoCobro.add(new SelectItem(adapterTipoCobro.getData().get(i).
                    getId(), adapterTipoCobro.getData().get(i).getCodigo()));
        }

        filterEntidadFinanciera();

        this.test = BigDecimal.ONE;

        this.showTalonarioPopup = false;
        obtenerConfiguracionNotificacionEmail();
        this.adapterCobroTalonario = new CobroTalonarioAdapter();
        this.cobroTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();
        this.digital = "N";
        this.addEmail="";
        this.idTalonario = null;
        this.cobroPorTotal = false;
        obtenerConfiguracion();
        this.omitirValidacionCheque = obtenerConfiguracion("OMITIR_VALIDACION_CHEQUE");
        lazyModel = new FacturaDataModel();
        this.cobroEnGs = false;
        clear();
        
    }

}
