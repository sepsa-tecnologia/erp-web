package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetalles;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para Facturas
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("facturaCompraCrear")
public class FacturaCompraCrearController implements Serializable {

    /**
     * DATOS DE PRUEBA
     */
    /**
     * Objeto Cliente
     */
    private Cliente cliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFactura;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFilterPrueba;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterClientePrueba2;
    /**
     * Variable de control
     */
    private boolean show;
    /**
     * Linea
     */
    private Integer linea;
    /**
     * ***DATOS REALES****
     */
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFacturaCancelada;
    /**
     * POJO para Factura
     */
    private Factura facturaFilterCancelada;
    /**
     * POJO DatosFactura
     */
    private DatosFactura datoFactura;
    /**
     * Servicio Facturacion
     */
    private FacturacionService serviceFacturacion;
    /**
     * POJO Factura para crear
     */
    private Factura facturaCreate;
    /**
     * Dato del Cliente
     */
    private String ruc;
    /**
     * Dato del Cliente
     */
    private String direccion;
    /**
     * Dato del Cliente
     */
    private String telefono;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Adapter Historico Liquidación
     */
    private HistoricoLiquidacionAdapter adapterLiquidacion;
    /**
     * Historico Liquidación
     */
    private HistoricoLiquidacion historicoLiquidacionFilter;
    /**
     * Bandera
     */
    private boolean showDetalleCreate;
    /**
     * Lista Detalle
     */
    private List<FacturaDetalle> listaDetalle;
    /**
     * Lista Selected Liquidación
     */
    private List<HistoricoLiquidacion> listSelectedLiquidacion = new ArrayList<>();
    /**
     * Mapa de Datos de Liquidación y nro Linea
     */
    private Map<Integer, Integer> liquidacionDetalle = new HashMap<Integer, Integer>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Producto Adaptador
     */
    private ProductoAdapter adapterProducto;
    /**
     * POJO Producto
     */
    private Producto productoFilter;
    /**
     * Service Factura
     */
    private FacturacionService serviceFactura;
    /**
     * Adaptador parala lista de cliente
     */
    private ClientListAdapter adapter;
    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Identificador de Orden de compra
     */
    private String idOC;
    /**
     * Adaptador para la lista de orden de compra
     */
    private OrdenDeCompraAdapter adapterOrdenCompra;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompra;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Dato para digital
     */
    private String codMoneda;
    private boolean habilitarPanelCotizacion;
    private TipoCambio tipoCambio;
    private TipoCambioService serviceTipoCambio;
    private Integer nroLineaProducto;
    private Integer idDepositoDestino;
    private String generarOC;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompraCreate;
    private String moduloInventario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    
    private String registraInventario;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public String getRegistraInventario() {
        return registraInventario;
    }

    public void setRegistraInventario(String registraInventario) {
        this.registraInventario = registraInventario;
    }
    
    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public void setGenerarOC(String generarOC) {
        this.generarOC = generarOC;
    }

    public String getGenerarOC() {
        return generarOC;
    }

    public void setOrdenDeCompraCreate(OrdenDeCompra ordenDeCompraCreate) {
        this.ordenDeCompraCreate = ordenDeCompraCreate;
    }

    public OrdenDeCompra getOrdenDeCompraCreate() {
        return ordenDeCompraCreate;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setServiceTipoCambio(TipoCambioService serviceTipoCambio) {
        this.serviceTipoCambio = serviceTipoCambio;
    }

    public TipoCambioService getServiceTipoCambio() {
        return serviceTipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setHabilitarPanelCotizacion(boolean habilitarPanelCotizacion) {
        this.habilitarPanelCotizacion = habilitarPanelCotizacion;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public boolean isHabilitarPanelCotizacion() {
        return habilitarPanelCotizacion;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public OrdenDeCompraAdapter getAdapterOrdenCompra() {
        return adapterOrdenCompra;
    }

    public OrdenDeCompra getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void setAdapterOrdenCompra(OrdenDeCompraAdapter adapterOrdenCompra) {
        this.adapterOrdenCompra = adapterOrdenCompra;
    }

    public void setOrdenDeCompra(OrdenDeCompra ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    public void setIdOC(String idOC) {
        this.idOC = idOC;
    }

    public String getIdOC() {
        return idOC;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapter(ClientListAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientListAdapter getAdapter() {
        return adapter;
    }

    public void setClienteFactura(Cliente clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public Cliente getClienteFactura() {
        return clienteFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setProductoFilter(Producto productoFilter) {
        this.productoFilter = productoFilter;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public Producto getProductoFilter() {
        return productoFilter;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListaDetalle(List<FacturaDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<FacturaDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public Map<Integer, Integer> getLiquidacionDetalle() {
        return liquidacionDetalle;
    }

    public void setLiquidacionDetalle(Map<Integer, Integer> liquidacionDetalle) {
        this.liquidacionDetalle = liquidacionDetalle;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListSelectedLiquidacion(List<HistoricoLiquidacion> listSelectedLiquidacion) {
        this.listSelectedLiquidacion = listSelectedLiquidacion;
    }

    public List<HistoricoLiquidacion> getListSelectedLiquidacion() {
        return listSelectedLiquidacion;
    }

    public void setShowDetalleCreate(boolean showDetalleCreate) {
        this.showDetalleCreate = showDetalleCreate;
    }

    public boolean isShowDetalleCreate() {
        return showDetalleCreate;
    }

    public void setHistoricoLiquidacionFilter(HistoricoLiquidacion historicoLiquidacionFilter) {
        this.historicoLiquidacionFilter = historicoLiquidacionFilter;
    }

    public void setAdapterLiquidacion(HistoricoLiquidacionAdapter adapterLiquidacion) {
        this.adapterLiquidacion = adapterLiquidacion;
    }

    public HistoricoLiquidacion getHistoricoLiquidacionFilter() {
        return historicoLiquidacionFilter;
    }

    public HistoricoLiquidacionAdapter getAdapterLiquidacion() {
        return adapterLiquidacion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setFacturaCreate(Factura facturaCreate) {
        this.facturaCreate = facturaCreate;
    }

    public Factura getFacturaCreate() {
        return facturaCreate;
    }

    public void setFacturaFilterCancelada(Factura facturaFilterCancelada) {
        this.facturaFilterCancelada = facturaFilterCancelada;
    }

    public void setAdapterFacturaCancelada(FacturaAdapter adapterFacturaCancelada) {
        this.adapterFacturaCancelada = adapterFacturaCancelada;
    }

    public Factura getFacturaFilterCancelada() {
        return facturaFilterCancelada;
    }

    public FacturaAdapter getAdapterFacturaCancelada() {
        return adapterFacturaCancelada;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setAdapterFactura(FacturaAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Cliente getClienteFilterPrueba() {
        return clienteFilterPrueba;
    }

    public void setClienteFilterPrueba(Cliente clienteFilterPrueba) {
        this.clienteFilterPrueba = clienteFilterPrueba;
    }

    public void setAdapterClientePrueba2(ClientListAdapter adapterClientePrueba2) {
        this.adapterClientePrueba2 = adapterClientePrueba2;
    }

    public ClientListAdapter getAdapterClientePrueba2() {
        return adapterClientePrueba2;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public void setServiceFacturacion(FacturacionService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    public void setDatoFactura(DatosFactura datoFactura) {
        this.datoFactura = datoFactura;
    }

    public FacturacionService getServiceFacturacion() {
        return serviceFacturacion;
    }

    public DatosFactura getDatoFactura() {
        return datoFactura;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public SessionData getSession() {
        return session;
    }

    public void setModuloInventario(String moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public String getModuloInventario() {
        return moduloInventario;
    }

//</editor-fold>
    /**
     * Método para obtener el cliente
     *
     * @param event
     */
    public void onItemSelectClienteFacturaCrear(SelectEvent event) {
        facturaCreate.setIdPersona(((Cliente) event.getObject()).getIdCliente());
        facturaCreate.setRazonSocial(((Cliente) event.getObject()).getRazonSocial());
        facturaCreate.setRuc(((Cliente) event.getObject()).getNroDocumento());
    }

    /**
     * Método para obtener datos de factura
     */
    public void obtenerDatosFacturacion() {
        Calendar today = Calendar.getInstance();
        facturaCreate.setFecha(today.getTime());
        facturaCreate.setFechaVencimiento(today.getTime());

    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryDestino(String query) {
        Local localOrigen = new Local();
        LocalListAdapter localOrigenAdapter = new LocalListAdapter();
        localOrigen.setLocalExterno("N");
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }

    /**
     *
     * @param event
     */
    public void onItemSelectLocalDestinoFilter(SelectEvent event) {
        this.ordenDeCompraCreate.setIdLocalOrigen(((Local) event.getObject()).getId());
    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        Producto producto = new Producto();
        ProductoAdapter adapterProducto = new ProductoAdapter();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto = adapterProducto.fillData(producto);
        producto = new Producto();

        return adapterProducto.getData();
    }

    /**
     * Método para filtrar los datos factura
     */
    public void filterDatosFactura() {
        Map parametros = new HashMap();
        DatosFactura df = new DatosFactura();

        df = serviceFacturacion.getDatosFactura(parametros);

        facturaCreate.setDiasCredito(df.getDiasCredito());
        facturaCreate.setFechaVencimiento(df.getFechaVencimiento());
    }

    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea;
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        boolean save = true;
        FacturaDetalle info = null;
        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), ((Producto) event.getObject()).getId())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }
        if (save == true) {
            info.setIdProducto(((Producto) event.getObject()).getId());
            info.setDescripcion(((Producto) event.getObject()).getDescripcion());
        }

    }

    /**
     * Método para crear la factura
     */
    public String crearFactura() {
        boolean saveFactura = true;
        String r = null;

        if (listaDetalle.isEmpty() || listaDetalle == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe agregar al menos un detalle de factura!"));
            saveFactura = false;
        } else {
            for (int i = 0; i < listaDetalle.size(); i++) {
                int dn = i + 1;

                if (listaDetalle.get(i).getPrecioUnitarioConIva() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el precio unitario con IVA en el detalle N° " + dn));
                    saveFactura = false;
                }

                if (listaDetalle.get(i).getCantidadFacturada() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la cantidad en el detalle N° " + dn));
                    saveFactura = false;
                }

            }
        }

        if (facturaCreate.getRazonSocial().trim().isEmpty() || facturaCreate.getRazonSocial() == null) {
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la razón social del cliente!"));
            saveFactura = false;
        }

        if (facturaCreate.getRuc().trim().isEmpty() || facturaCreate.getRuc() == null) {
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el ruc del cliente!"));
            saveFactura = false;
        }
        if (saveFactura == true) {
            calcularTotalesGenerales();

            facturaCreate.setAnulado("N");
            facturaCreate.setPagado("N");
            facturaCreate.setImpreso("N");
            facturaCreate.setEntregado("N");
            facturaCreate.setDigital(digital);

            for (int i = 0; i < listaDetalle.size(); i++) {
                listaDetalle.get(i).setNroLinea(i + 1);
                listaDetalle.get(i).setIdDepositoLogistico(idDepositoDestino);
                listaDetalle.get(i).setCodigoEstadoInventario("DISPONIBLE");
            }

            facturaCreate.setFacturaCompraDetalles(listaDetalle);

            if (moduloInventario.equalsIgnoreCase("S") && idOC == null) {
                facturaCreate.setOrdenCompra(generarOC(facturaCreate));
                
            }

            BodyResponse<Factura> respuestaDeFactura = serviceFactura.createFacturaCompra(facturaCreate);

            if (respuestaDeFactura.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura creada correctamente!"));

                clearForm();

                if (idOC != null) {
                    r = String.format("/app/orden-de-compra/orden-de-compra-emitida-list.xhtml"
                            + "?faces-redirect=true");
                } 
            }

        }
        return r;
    }

    /**
     * Generar OC
     *
     * @param facturaGenerada
     * @return
     */
    public OrdenDeCompra generarOC(Factura facturaGenerada) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
        String id = sdf.format(new Date());
        ordenDeCompraCreate.setIdClienteOrigen(facturaGenerada.getIdCliente());
        ordenDeCompraCreate.setFechaRecepcion(facturaGenerada.getFecha());
        ordenDeCompraCreate.setNroOrdenCompra("AG-" + id);
        ordenDeCompraCreate.setRegistroInventario(registraInventario);
        List<OrdenCompraDetalles> listaDetalleOC = new ArrayList<>();

        for (FacturaDetalle facturaDetalle : facturaGenerada.getFacturaCompraDetalles()) {
            OrdenCompraDetalles ocDetalle = new OrdenCompraDetalles();
            ocDetalle.setIdProducto(facturaDetalle.getIdProducto());
            ocDetalle.setPrecioUnitarioConIva(facturaDetalle.getPrecioUnitarioConIva());
            ocDetalle.setCantidad(facturaDetalle.getCantidad());
            ocDetalle.setCantidadSolicitada(BigDecimal.ZERO);
            ocDetalle.setCantidadConfirmada(facturaDetalle.getCantidadFacturada());
            ocDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            ocDetalle.setPrecioUnitarioSinIva(facturaDetalle.getPrecioUnitarioSinIva());
            ocDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            ocDetalle.setMontoIva(facturaDetalle.getMontoIva());
            ocDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            ocDetalle.setIdDepositoLogistico(idDepositoDestino);
            ocDetalle.setCodigoEstadoInventario("CONFIRMADO");
            listaDetalleOC.add(ocDetalle);
        }
        ordenDeCompraCreate.setRecibido("N");
        ordenDeCompraCreate.setArchivoEdi("N");
        ordenDeCompraCreate.setGeneradoEdi("N");
        ordenDeCompraCreate.setAnulado("N");
        ordenDeCompraCreate.setCodigoEstado("CONFIRMADO");
        ordenDeCompraCreate.setOrdenCompraDetalles(listaDetalleOC);
        ordenDeCompraCreate.setMontoIva5(facturaGenerada.getMontoIva5());
        ordenDeCompraCreate.setMontoImponible5(facturaGenerada.getMontoImponible5());
        ordenDeCompraCreate.setMontoTotal5(facturaGenerada.getMontoTotal5());
        ordenDeCompraCreate.setMontoIva10(facturaGenerada.getMontoIva10());
        ordenDeCompraCreate.setMontoImponible10(facturaGenerada.getMontoImponible10());
        ordenDeCompraCreate.setMontoTotal10(facturaGenerada.getMontoTotal10());
        ordenDeCompraCreate.setMontoTotalExento(facturaGenerada.getMontoTotalExento());
        ordenDeCompraCreate.setMontoImponibleTotal(facturaGenerada.getMontoImponibleTotal());
        ordenDeCompraCreate.setMontoIvaTotal(facturaGenerada.getMontoIvaTotal());
        ordenDeCompraCreate.setMontoTotalOrdenCompra(facturaGenerada.getMontoTotalFactura());

        return ordenDeCompraCreate;

    }

    /**
     *
     * @param event
     */
    public void onItemSelectLocalOringenFilter(SelectEvent event) {
        this.ordenDeCompraCreate.setIdLocalOrigen(((Local) event.getObject()).getId());
        this.ordenDeCompraCreate.setIdClienteOrigen(((Local) event.getObject()).getIdPersona());
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        init();
        PF.current().ajax().update(":list-factura-create-form:form-data:deposito-destino");
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {

        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setCodigoMoneda(obtenerCodMoneda());
        montoLetrasFilter.setMonto(facturaCreate.getMontoTotalFactura());

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
        facturaCreate.setTotalLetras(montoLetrasFilter.getTotalLetras());
    }

    /**
     * Método para mostrar Datatable de Detalle al crear
     */
    public void showDetalle() {
        showDetalleCreate = true;

        addDetalle();
    }

    /**
     * Método para agregar detalle
     */
    public void addDetalle() {
        FacturaDetalle detalle = new FacturaDetalle();

        if (listaDetalle.isEmpty()) {
            detalle.setNroLinea(linea);
        } else {
            linea++;
            detalle.setNroLinea(linea);
        }

        detalle.setDescripcion("--");
        detalle.setPorcentajeIva(10);
        detalle.setCantidadOrdenCompra(BigDecimal.ZERO);
        detalle.setMontoImponible(BigDecimal.valueOf(0));
        detalle.setMontoIva(BigDecimal.valueOf(0));
        detalle.setMontoTotal(BigDecimal.valueOf(0));
        detalle.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
        detalle.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
        detalle.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
        detalle.setMontoDescuentoParticular(BigDecimal.valueOf(0));

        listaDetalle.add(detalle);
    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        facturaCreate.setMontoIva5(montoIva5Acumulador);
        facturaCreate.setMontoImponible5(montoImponible5Acumulador);
        facturaCreate.setMontoTotal5(montoTotal5Acumulador);
        facturaCreate.setMontoIva10(montoIva10Acumulador);
        facturaCreate.setMontoImponible10(montoImponible10Acumulador);
        facturaCreate.setMontoTotal10(montoTotal10Acumulador);
        facturaCreate.setMontoTotalExento(montoExcentoAcumulador);
        facturaCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        facturaCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
        facturaCreate.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        facturaCreate.setMontoTotalDescuentoParticular(new BigDecimal("0"));
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos(Integer nroLinea) {
        FacturaDetalle info = null;

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        BigDecimal cantidad = info.getCantidadFacturada();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();

        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        BigDecimal iva = new BigDecimal(info.getPorcentajeIva());
        BigDecimal ceroPorc = new BigDecimal("0");
        BigDecimal cincoPorc = new BigDecimal("5");
        BigDecimal diezPorc = new BigDecimal("10");
        BigDecimal prueba = new BigDecimal("0");

        if (iva.compareTo(ceroPorc) == 0) {
            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (iva.compareTo(cincoPorc) == 0) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (iva.compareTo(diezPorc) == 0) {
            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }
        if (info.getMontoTotal().compareTo(prueba) == 0) {
            WebLogger.get().debug("EL MONTO TOTAL ES 0");
        } else {
            calcularTotal();
        }

    }

    /**
     * Método para calcular el total de la factura
     */
    public void calcularTotal() {
        BigDecimal totalFactura = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            BigDecimal montoTotal = listaDetalle.get(i).getMontoTotal();
            totalFactura = totalFactura.add(montoTotal);
        }

        facturaCreate.setMontoTotalFactura(totalFactura);
        facturaCreate.setMontoTotalGuaranies(totalFactura);
        obtenerMontoTotalLetras();
    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param rowKey
     */
    public void deleteDetalle(Integer nroLinea) {
        FacturaDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        BigDecimal monto = facturaCreate.getMontoTotalFactura();
        BigDecimal totalFactura = monto.subtract(subTotal);
        facturaCreate.setMontoTotalFactura(totalFactura);

    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        facturaCreate.setFecha((Date) event.getObject());
        if (facturaCreate.getIdTipoFactura() == 2) {
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
            Date d = facturaCreate.getFechaVencimiento();
            WebLogger.get().debug("FECHA VTO FACTURA ES: " + d);
        }
    }

    /**
     * Método para filtrar el producto
     */
    public void filterProducto() {
        productoFilter = new Producto();
        adapterProducto = adapterProducto.fillData(productoFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        cliente.setProveedor("S");

        adapter = adapter.fillData(cliente);

        return adapter.getData();
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Método para obtener Moneda
     */
    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }

    /**
     * Método para obtener orden de compra
     */
    public void obtenerOC() {
        this.adapterOrdenCompra = new OrdenDeCompraAdapter();
        this.ordenDeCompra = new OrdenDeCompra();
        ordenDeCompra.setId(Integer.valueOf(idOC));
        adapterOrdenCompra = adapterOrdenCompra.fillData(ordenDeCompra);
        showDetalleCreate = true;

        if (adapterOrdenCompra.getData().size() == 0) {
            //Mostrar mensaje de que no hay registro disponible

        } else {
            for (OrdenCompraDetalles detalleOC : adapterOrdenCompra.getData().get(0).getOrdenCompraDetalles()) {
                FacturaDetalle detalle = new FacturaDetalle();

                if (listaDetalle.isEmpty()) {
                    detalle.setNroLinea(linea);
                } else {
                    linea++;
                    detalle.setNroLinea(linea);
                }
                detalle.setIdProducto(detalleOC.getIdProducto());
                detalle.setFechaVencimiento(detalleOC.getFechaVencimiento());
                detalle.setDescripcion(detalleOC.getProducto().getDescripcion());
                detalle.setPorcentajeIva(detalleOC.getProducto().getPorcentajeImpuesto());
                detalle.setCantidadOrdenCompra(detalleOC.getCantidadSolicitada());
                detalle.setCantidadRecibida(detalleOC.getCantidadConfirmada());
                detalle.setPrecioUnitarioConIva(detalleOC.getPrecioUnitarioConIva());
                detalle.setPrecioUnitarioConIvaOc(detalleOC.getPrecioUnitarioConIva());
                detalle.setMontoIva(BigDecimal.valueOf(0));

                listaDetalle.add(detalle);

//           calcularMontos(linea);
            }

            if (adapterOrdenCompra.getData().get(0).getLocalDestino() != null) {
                facturaCreate.setIdPersona(adapterOrdenCompra.getData().get(0).getLocalDestino().getCliente().getIdCliente());
                facturaCreate.setRazonSocial(adapterOrdenCompra.getData().get(0).getLocalDestino().getCliente().getRazonSocial());
                facturaCreate.setRuc(adapterOrdenCompra.getData().get(0).getLocalDestino().getCliente().getNroDocumento());
            } else {
                facturaCreate.setIdPersona(adapterOrdenCompra.getData().get(0).getClienteDestino().getIdCliente());
                facturaCreate.setRazonSocial(adapterOrdenCompra.getData().get(0).getClienteDestino().getRazonSocial());
                facturaCreate.setRuc(adapterOrdenCompra.getData().get(0).getClienteDestino().getNroDocumento());
            }
        }

    }

    /**
     * Autocomplete para depositoDestinoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        DepositoLogistico depositoDestino = new DepositoLogistico();
        DepositoLogisticoAdapter depositoDestinoAdapter = new DepositoLogisticoAdapter();

        depositoDestino.setDescripcion(query);
        depositoDestino.setListadoPojo(true);
        depositoDestinoAdapter = depositoDestinoAdapter.fillData(depositoDestino);

        return depositoDestinoAdapter.getData();
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        this.idDepositoDestino = ((DepositoLogistico) event.getObject()).getId();
    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVto() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(facturaCreate.getFecha());
        cal.add(Calendar.DAY_OF_MONTH, facturaCreate.getDiasCredito());
        Date fechaVencimiento = cal.getTime();

        facturaCreate.setFechaVencimiento(fechaVencimiento);
    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVtoContado() {
        if (facturaCreate.getIdTipoFactura() == 2) {
            Calendar today = Calendar.getInstance();
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
            facturaCreate.setDiasCredito(null);
        }

    }

    /**
     * Método que se dispara en el evento de cambio de moneda
     */
    public void onChangeMoneda() {
        Calendar now = Calendar.getInstance();
        Date date = now.getTime();

        TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
        TipoCambio tipoCambioFilter = new TipoCambio();

        tipoCambioFilter.setIdMoneda(facturaCreate.getIdMoneda());
        tipoCambioFilter.setActivo("S");
        tipoCambioFilter.setFechaInsercion(date);

        adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);

        codMoneda = obtenerCodMoneda();

        if (adapterTipoCambio.getData().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No existe un registro de cotización actual para la moneda seleccionada!"));
            habilitarPanelCotizacion = true;
        } else {
            if (!listaDetalle.isEmpty()) {
                if (codMoneda.equals("PYG")) {
                    for (FacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioVenta(facturaDetalle, adapterTipoCambio.getData().get(0).getVenta());
                    }
                } else {
                    for (FacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioCompra(facturaDetalle, adapterTipoCambio.getData().get(0).getCompra());
                    }
                }
            }
        }
    }

    public void crearTipoCambio() {
        tipoCambio.setActivo("S");
        tipoCambio.setIdMoneda(facturaCreate.getIdMoneda());
        BodyResponse<TipoCambio> respuestaDeTipoCambio = serviceTipoCambio.setTipoCambios(tipoCambio);

        if (respuestaDeTipoCambio.getSuccess()) {
            habilitarPanelCotizacion = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Tipo de cambio creado correctamente!"));
            onChangeMoneda();
        } else {
            for (Mensaje mensaje : respuestaDeTipoCambio.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }

        }
    }

    public String obtenerCodMoneda() {
        Moneda mf = new Moneda();
        MonedaAdapter am = new MonedaAdapter();
        mf.setId(facturaCreate.getIdMoneda());
        am = am.fillData(mf);

        return am.getData().get(0).getCodigo();
    }

    /**
     * Método para calcular el precio unitario con gs de referencia
     *
     * @param facturaDetalle
     * @param precioCompraMoneda
     */
    public void calcularCambioCompra(FacturaDetalle facturaDetalle, BigDecimal precioCompraMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.divide(precioCompraMoneda, 2, RoundingMode.HALF_UP);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para calcular el precio unitario a gs
     *
     * @param facturaDetalle
     * @param precioVentaMoneda
     */
    public void calcularCambioVenta(FacturaDetalle facturaDetalle, BigDecimal precioVentaMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.multiply(precioVentaMoneda);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelectVtoTimbrado(SelectEvent event) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            Date dateFactura = facturaCreate.getFecha();
            Date dateUtil = (Date) event.getObject();

            String dateToday = simpleDateFormat.format(dateFactura);
            String dateUtilFormat = simpleDateFormat.format(dateUtil);

            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(dateToday);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(dateUtilFormat);
            WebLogger.get().debug("FECHA Factura:" + date1);
            WebLogger.get().debug("FECHA Vto Timbrado:" + date2);

            if (date1.compareTo(date2) < 0) {
                facturaCreate.setFechaVencimientoTimbrado(date2);
            } else if (date1.compareTo(date2) > 0) {
                facturaCreate.setFechaVencimientoTimbrado(null);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La Fecha de Vto Timbrado no puede ser inferior a la Fecha de Factura!"));

            }
        } catch (ParseException ex) {
            WebLogger.get().fatal(ex);
        }

    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryOrigen(String query) {
        Local localOrigen = new Local();
        LocalListAdapter localOrigenAdapter = new LocalListAdapter();
        localOrigen.setLocalExterno("N");
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }
    
    /**
     *Obtiene la configuracion de registro 
     */
    public void obtenerConfiguracionRegistroInventario() {
        ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
        ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("REGISTRO_INVENTARIO");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            registraInventario = "N";
        } else {
            registraInventario = adapterConfigValor.getData().get(0).getValor();
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        /**
         * DATOS REALES
         */
        this.codMoneda = null;
        this.digital = "N";
        this.facturaCreate = new Factura();

        this.cliente = new Cliente();
        this.adapter = new ClientListAdapter();
        filterCliente();
        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();

        this.adapterFactura = new FacturaAdapter();

        this.adapterFacturaCancelada = new FacturaAdapter();

        this.serviceFacturacion = new FacturacionService();
        this.adapterProducto = new ProductoAdapter();

        obtenerDatosFacturacion();
        this.ruc = "--";
        this.direccion = "--";
        this.telefono = "--";

        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("idOC") != null) {
            idOC = params.get("idOC");
            obtenerOC();
        }

        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();
        this.facturaCreate.setIdTipoFactura(2);
        this.nroLineaProducto = null;
        this.idDepositoDestino = null;
        this.ordenDeCompraCreate = new OrdenDeCompra();
        this.moduloInventario = session.getUser().getModuloInventario();
        obtenerConfiguracionRegistroInventario();
        // obtenerConfiguracion();

    }

}
