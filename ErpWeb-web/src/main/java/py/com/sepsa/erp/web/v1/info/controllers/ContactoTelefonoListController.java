/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.CargoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoContactoListAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Cargo;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoContacto;

/**
 * Controlador para contacto telefono list
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("contactoTelefonoList")
public class ContactoTelefonoListController implements Serializable {

    /**
     * Adaptador para la lista de Contacto email
     */
    private ContactoTelefonoAdapter contactoTelefonoAdapterList;
    /**
     * POJO de contacto email
     */
    private ContactoTelefono contactoTelefonoFilter;
    /**
     * Adaptador para la lista de tipos de Contacto
     */
    private TipoContactoListAdapter tipoContactoAdapterList;
    /**
     * POJO de tipo contacto
     */
    private TipoContacto tipoContactoFilter;
    /**
     * Adaptador para la lista de cargos
     */
    private CargoListAdapter cargoAdapterList;
    /**
     * POJO de tipo cargo
     */
    private Cargo cargoFilter;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public ContactoTelefonoAdapter getContactoTelefonoAdapterList() {
        return contactoTelefonoAdapterList;
    }

    public void setContactoTelefonoAdapterList(ContactoTelefonoAdapter contactoTelefonoAdapterList) {
        this.contactoTelefonoAdapterList = contactoTelefonoAdapterList;
    }

    public ContactoTelefono getContactoTelefonoFilter() {
        return contactoTelefonoFilter;
    }

    public void setContactoTelefonoFilter(ContactoTelefono contactoTelefonoFilter) {
        this.contactoTelefonoFilter = contactoTelefonoFilter;
    }

    public TipoContactoListAdapter getTipoContactoAdapterList() {
        return tipoContactoAdapterList;
    }

    public void setTipoContactoAdapterList(TipoContactoListAdapter tipoContactoAdapterList) {
        this.tipoContactoAdapterList = tipoContactoAdapterList;
    }

    public TipoContacto getTipoContactoFilter() {
        return tipoContactoFilter;
    }

    public void setTipoContactoFilter(TipoContacto tipoContactoFilter) {
        this.tipoContactoFilter = tipoContactoFilter;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public CargoListAdapter getCargoAdapterList() {
        return cargoAdapterList;
    }

    public void setCargoAdapterList(CargoListAdapter cargoAdapterList) {
        this.cargoAdapterList = cargoAdapterList;
    }

    public Cargo getCargoFilter() {
        return cargoFilter;
    }

    public void setCargoFilter(Cargo cargoFilter) {
        this.cargoFilter = cargoFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener la lista de contacto email
     */
    public void getDatos() {
        this.contactoTelefonoAdapterList = contactoTelefonoAdapterList.fillData(contactoTelefonoFilter);
    }

    /**
     * Método para obtener la lista de tipos de contacto
     */
    public void getTipoContactoList() {
        this.tipoContactoAdapterList = tipoContactoAdapterList.fillData(tipoContactoFilter);
    }

    /**
     * Método para obtener la lista de cargos
     */
    public void getCargosList() {
        this.cargoAdapterList = cargoAdapterList.fillData(cargoFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        contactoTelefonoFilter.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.cliente = new Cliente();
        this.clientAdapter = new ClientListAdapter();
        this.contactoTelefonoFilter = new ContactoTelefono();
        this.tipoContactoFilter = new TipoContacto();
        this.cargoFilter = new Cargo();
        getTipoContactoList();
        getCargosList();
        getDatos();
    }

    /**
     * Obtiene el valor a exportar para el estado
     *
     * @param estado Valor
     * @return Valor a ser exportado
     */
    public String exportState(Character estado) {
        if (estado == null) {
            return "";
        }
        String value = "";
        switch (estado) {
            case 'I':
                value = "Inactivo";
                break;
            case 'A':
                value = "Activo";
                break;
        }
        return value;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.contactoTelefonoFilter = new ContactoTelefono();
            this.contactoTelefonoAdapterList = new ContactoTelefonoAdapter();
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.tipoContactoAdapterList = new TipoContactoListAdapter();
            this.tipoContactoFilter = new TipoContacto();
            this.cargoAdapterList = new CargoListAdapter();
            this.cargoFilter = new Cargo();
            getTipoContactoList();
            getCargosList();
            getDatos();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
