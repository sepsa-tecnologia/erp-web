/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * Filter para empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class EmpresaFilter extends Filter {

    public EmpresaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    public EmpresaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    public EmpresaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    public EmpresaFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    public EmpresaFilter facturadorElectronico(String facturadorElectronico) {
        if (facturadorElectronico != null && !facturadorElectronico.trim().isEmpty()) {
            params.put("facturadorElectronico", facturadorElectronico);
        }
        return this;
    }

    /**
     * Agrega el filtro de idTipoEmpresa
     *
     * @param idTipoEmpresa
     * @return
     */
    public EmpresaFilter idTipoEmpresa(Integer idTipoEmpresa) {
        if (idTipoEmpresa != null) {
            params.put("idTipoEmpresa", idTipoEmpresa);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param empresa datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Empresa empresa, Integer page, Integer pageSize) {
        EmpresaFilter filter = new EmpresaFilter();

        filter
                .id(empresa.getId())
                .descripcion(empresa.getDescripcion())
                .codigo(empresa.getCodigo())
                .activo(empresa.getActivo())
                .facturadorElectronico(empresa.getFacturadorElectronico())
                .idTipoEmpresa(empresa.getIdTipoEmpresa())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

}
