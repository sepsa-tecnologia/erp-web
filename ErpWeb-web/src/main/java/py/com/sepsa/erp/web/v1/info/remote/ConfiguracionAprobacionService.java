package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionAprobacionAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ConfiguracionAprobacionFilter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Servicio para el listado de clientes s/ servicios.
 *
 * @author alext.
 */
public class ConfiguracionAprobacionService extends APIErpCore {

    /**
     * Obtiene la lista de clientes según servicios.
     *
     * @param searchData
     * @param page
     * @param pagesize
     * @return
     */
    public ConfiguracionAprobacionAdapter getList(ConfiguracionAprobacion searchData,
            Integer page, Integer pagesize) {

        ConfiguracionAprobacionAdapter lista = new ConfiguracionAprobacionAdapter();

        Map params = ConfiguracionAprobacionFilter.build(searchData, page, pagesize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionAprobacionAdapter.class);

            lista = (ConfiguracionAprobacionAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    
     /**
      * 
      * @param id
      * @return 
      */
    public ConfiguracionAprobacion get(Integer id) {
        ConfiguracionAprobacion data = new ConfiguracionAprobacion(id);
        ConfiguracionAprobacionAdapter list = getList(data, 0, 1);
        data = list != null 
                && list.getData() != null 
                && !list.getData().isEmpty() 
                ?  list.getData().get(0) 
                : null;
        
        return data;
    }

    /**
     * Método para crear una nueva Configuración Aprobación.
     *
     * @param configuracionAprobacion
     * @return
     */
    public ConfiguracionAprobacion create(ConfiguracionAprobacion configuracionAprobacion) {

        HttpURLConnection conn = POST(Resource.CREAR.url, ContentType.JSON);

        if (conn != null) {
            Gson gson = new Gson();
            WebLogger.get().debug(gson.toJson(configuracionAprobacion));
            this.addBody(conn, gson.toJson(configuracionAprobacion));
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionAprobacion.class);
            configuracionAprobacion = (ConfiguracionAprobacion) response.getPayload();
            conn.disconnect();
        }

        return configuracionAprobacion;
    }

    /**
     * Método para editar una configuración aprobación.
     *
     * @param configuracionAprobacion
     * @return
     */
    public ConfiguracionAprobacion edit(ConfiguracionAprobacion configuracionAprobacion) {

        HttpURLConnection conn = PUT(Resource.EDITAR.url, ContentType.JSON);

        if (conn != null) {
            Gson gson = new Gson();
            WebLogger.get().debug(gson.toJson(configuracionAprobacion));
            this.addBody(conn, gson.toJson(configuracionAprobacion));
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfiguracionAprobacion.class);
            configuracionAprobacion = (ConfiguracionAprobacion) response.getPayload();
            conn.disconnect();
        }

        return configuracionAprobacion;
    }

    /**
     * Constructor de ServiciosService.
     */
    public ConfiguracionAprobacionService() {
        super();
    }

    /**
     * Recursos de conexión.
     */
    public enum Resource {
        //Servicios
        LISTAR("Listado de configuracionAprobracion", "config-aprobacion"),
        CREAR("Creacion de configuracionAprobracion", "config-aprobacion"),
        EDITAR("Editar configuracionAprobracion", "config-aprobacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
