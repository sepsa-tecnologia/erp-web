package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO de ServicioProducto
 *
 * @author Romina E. Núñez Rojas
 */
public class ServicioProducto {

    /**
     * Identificador del servicio
     */
    private Integer id;
    /**
     * Descripción del Servicio
     */
    private String descripcion;
    /**
     * Código del servicio
     */
    private String codigo;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Código Producto
     */
    private String codigoProducto;
    /**
     * Estado del contrato
     */
    private String producto;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getCodigo() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    public Integer getIdProducto() {
        return idProducto;
    }
    
    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }
    
    public String getCodigoProducto() {
        return codigoProducto;
    }
    
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }
    
    public String getProducto() {
        return producto;
    }
    
    public void setProducto(String producto) {
        this.producto = producto;
    }
//</editor-fold>

    /**
     * Contructor de la clase
     */
    public ServicioProducto() {

    }

    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion
     * @param codigo
     * @param idProducto
     * @param codigoProducto
     * @param producto 
     */
    public ServicioProducto(Integer id, String descripcion, String codigo, Integer idProducto, String codigoProducto, String producto) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.idProducto = idProducto;
        this.codigoProducto = codigoProducto;
        this.producto = producto;
    }

    

}
