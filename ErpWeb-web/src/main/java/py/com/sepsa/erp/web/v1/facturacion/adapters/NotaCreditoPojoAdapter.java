package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;

/**
 * Adaptador de la lista de cobro
 *
 * @author Romina Núñez
 */
public class NotaCreditoPojoAdapter extends DataListAdapter<NotaCreditoPojo> {

    /**
     * Cliente para el servicio de cobro
     */
    private final NotaCreditoService serviceNotaCredito;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoPojoAdapter fillData(NotaCreditoPojo searchData) {

        return serviceNotaCredito.getNotaCreditoPojoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de NotaCreditoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaCredito = new NotaCreditoService();
    }

    /**
     * Constructor de NotaCreditoAdapter
     */
    public NotaCreditoPojoAdapter() {
        super();
        this.serviceNotaCredito = new NotaCreditoService();
    }
}
