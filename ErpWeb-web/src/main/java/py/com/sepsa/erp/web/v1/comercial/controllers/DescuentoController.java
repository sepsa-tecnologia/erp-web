package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.DescuentoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.TipoDescuentoListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoDescuento;
import py.com.sepsa.erp.web.v1.comercial.remote.DescuentoService;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * Controlador para Documento de Admin
 *
 * @author Romina Núñez, Cristina Insfrán
 */
@ViewScoped
@Named("descuento")
public class DescuentoController implements Serializable {

    /**
     * Adaptador para la lista de descuentos
     */
    private DescuentoAdapter adapterDescuento;
    /**
     * Objeto Descuento
     */
    private Descuento descuentoFilter;
    /**
     * Adaptador para la lista de descuentos
     */
    private DescuentoAdapter adapterDescuentoPendiente;
    /**
     * Objeto Cliente
     */
    private Cliente client;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;

    /**
     * Objeto Descuento
     */
    private Descuento descuentoPendienteFilter;
    /**
     * Objeto a enviar
     */
    private Descuento descuentoAprobar;
    /**
     * Servicio Descuento
     */
    private DescuentoService serviceDescuento;
    /**
     * SelectItem para tipo descuento
     */
    private List<SelectItem> listaDescuento = new ArrayList<>();
    /**
     * SelectItem para estados
     */
    private List<SelectItem> estados = new ArrayList<>();
    /**
     * Adaptador de la lista tipo descuento
     */
    private TipoDescuentoListAdapter tipoDescuentoAdapter;
    /**
     * Objeto Tipo descuento
     */
    private TipoDescuento tipoDescuento;
    /**
     * Adaptador de la lista de estados
     */
    private EstadoAdapter estadoAdapter;
    /**
     * Objeto Estado
     */
    private Estado estado;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the estados
     */
    public List<SelectItem> getEstados() {
        return estados;
    }

    /**
     * @param estados the estados to set
     */
    public void setEstados(List<SelectItem> estados) {
        this.estados = estados;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    /**
     * @return the estadoAdapter
     */
    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    /**
     * @param estadoAdapter the estadoAdapter to set
     */
    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    /**
     * @return the tipoDescuento
     */
    public TipoDescuento getTipoDescuento() {
        return tipoDescuento;
    }

    /**
     * @param tipoDescuento the tipoDescuento to set
     */
    public void setTipoDescuento(TipoDescuento tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    /**
     * @return the tipoDescuentoAdapter
     */
    public TipoDescuentoListAdapter getTipoDescuentoAdapter() {
        return tipoDescuentoAdapter;
    }

    /**
     * @param tipoDescuentoAdapter the tipoDescuentoAdapter to set
     */
    public void setTipoDescuentoAdapter(TipoDescuentoListAdapter tipoDescuentoAdapter) {
        this.tipoDescuentoAdapter = tipoDescuentoAdapter;
    }

    /**
     * @return the listaDescuento
     */
    public List<SelectItem> getListaDescuento() {
        return listaDescuento;
    }

    /**
     * @param listaDescuento the listaDescuento to set
     */
    public void setListaDescuento(List<SelectItem> listaDescuento) {
        this.listaDescuento = listaDescuento;
    }

    public DescuentoAdapter getAdapterDescuento() {
        return adapterDescuento;
    }

    public void setServiceDescuento(DescuentoService serviceDescuento) {
        this.serviceDescuento = serviceDescuento;
    }

    public void setDescuentoAprobar(Descuento descuentoAprobar) {
        this.descuentoAprobar = descuentoAprobar;
    }

    public DescuentoService getServiceDescuento() {
        return serviceDescuento;
    }

    public Descuento getDescuentoAprobar() {
        return descuentoAprobar;
    }

    public Descuento getDescuentoFilter() {
        return descuentoFilter;
    }

    public void setDescuentoPendienteFilter(Descuento descuentoPendienteFilter) {
        this.descuentoPendienteFilter = descuentoPendienteFilter;
    }

    public void setAdapterDescuentoPendiente(DescuentoAdapter adapterDescuentoPendiente) {
        this.adapterDescuentoPendiente = adapterDescuentoPendiente;
    }

    public Descuento getDescuentoPendienteFilter() {
        return descuentoPendienteFilter;
    }

    public DescuentoAdapter getAdapterDescuentoPendiente() {
        return adapterDescuentoPendiente;
    }

    public void setAdapterDescuento(DescuentoAdapter adapterDescuento) {
        this.adapterDescuento = adapterDescuento;
    }

    public void setDescuentoFilter(Descuento descuentoFilter) {
        this.descuentoFilter = descuentoFilter;
    }

    public Cliente getClient() {
        return client;
    }

    public void setClient(Cliente client) {
        this.client = client;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }
    //</editor-fold>

    public void filter() {
        adapterDescuento.setFirstResult(0);
        this.adapterDescuento = adapterDescuento.fillData(descuentoFilter);
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }

    /**
     * Lista los usuarios de un cliente
     *
     * @param event
     */
    public void onItemSelectClienteDescuento(SelectEvent event) {
        descuentoFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        client = new Cliente();
        descuentoFilter = new Descuento();
        filter();
    }

    /**
     * Método para aprobar el descuento
     *
     * @param descuento
     */
    /*public void aprobarDescuento(Descuento descuento){
        descuentoAprobar.setId(descuento.getId());
        Estado estado = new Estado();
        estado.setCodigo("PENDIENTE");
        descuentoAprobar.setEstado(estado);
        
        descuentoAprobar = serviceDescuento.aprobarDescuento(descuentoAprobar);
        
        filterPendiente();
        
        if(descuentoAprobar!=null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Descuento aprobado!"));    
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error en la aprobación!")); 
        }
    }*/
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.adapterDescuento = new DescuentoAdapter();
        this.descuentoFilter = new Descuento();
        this.adapterDescuentoPendiente = new DescuentoAdapter();
        this.descuentoPendienteFilter = new Descuento();
        this.serviceDescuento = new DescuentoService();
        this.descuentoAprobar = new Descuento();
        this.tipoDescuentoAdapter = new TipoDescuentoListAdapter();
        this.tipoDescuento = new TipoDescuento();
        this.estadoAdapter = new EstadoAdapter();
        this.estado = new Estado();

        tipoDescuentoAdapter = tipoDescuentoAdapter.fillData(tipoDescuento);
        for (int i = 0; i < tipoDescuentoAdapter.getData().size(); i++) {
            listaDescuento.add(new SelectItem(tipoDescuentoAdapter.getData().get(i).getId(),
                    tipoDescuentoAdapter.getData().get(i).getCodigo()));
        }

        estado.setCodigoTipoEstado("DESCUENTO");

        estadoAdapter = estadoAdapter.fillData(estado);
        for (int i = 0; i < estadoAdapter.getData().size(); i++) {
            estados.add(new SelectItem(estadoAdapter.getData().get(i).getId(),
                    estadoAdapter.getData().get(i).getCodigo()));
        }

        filter();
    }
}
