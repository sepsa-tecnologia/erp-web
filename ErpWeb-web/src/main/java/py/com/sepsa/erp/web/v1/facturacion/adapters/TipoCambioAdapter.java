/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;

/**
 * Adapter para TipoCmabio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoCambioAdapter extends DataListAdapter<TipoCambio> {

    /**
     * Cliente para el servicio de TipoCambio
     */
    private final TipoCambioService serviceTipoCambio;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return TipoCambioAdapter
     */
    @Override
    public TipoCambioAdapter fillData(TipoCambio searchData) {

        return serviceTipoCambio.getTipoCambio(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoCambioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoCambioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoCambio = new TipoCambioService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public TipoCambioAdapter() {
        super();
        this.serviceTipoCambio = new TipoCambioService();
    }
}
