package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * Pojo para detalle de operacion inventario
 *
 * @author Alexander Triana
 */
public class OperacionInventarioDetallePojo {

    /**
     * Id
     */
    private Integer id;
    /**
     * Id tipo Operacion
     */
    private Integer idTipoOperacion;
    /**
     * tipo de operacion
     */
    private String tipoOperacion;
    /**
     * Codigo tipo operacion
     */
    private String codigoTipoOperacion;
    /**
     * Orden de compra
     */
    private String ordenDeCompra;
    /**
     * Id Motivo operacion
     */
    private Integer idMotivoOperacion;
    /**
     * motivo de opercion
     */
    private String motivoOperacion;
    /**
     * codigo motivo operacion
     */
    private String codigoMotivoOperacion;
    /**
     * Fecha de Insercion
     */
    private Date fechaInsercion;
    /**
     * Id Estado
     */
    private Integer idEstado;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * codigo Estado
     */
    private String codigoEstado;

    /**
     * listado pojo
     */
    private String listadoPojo;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public String getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void setOrdenDeCompra(String ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    public Integer getIdMotivoOperacion() {
        return idMotivoOperacion;
    }

    public void setIdMotivoOperacion(Integer idMotivoOperacion) {
        this.idMotivoOperacion = idMotivoOperacion;
    }

    public String getMotivoOperacion() {
        return motivoOperacion;
    }

    public void setMotivoOperacion(String motivoOperacion) {
        this.motivoOperacion = motivoOperacion;
    }

    public String getCodigoMotivoOperacion() {
        return codigoMotivoOperacion;
    }

    public void setCodigoMotivoOperacion(String codigoMotivoOperacion) {
        this.codigoMotivoOperacion = codigoMotivoOperacion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(String listadoPojo) {
        this.listadoPojo = listadoPojo;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public OperacionInventarioDetallePojo() {
    }

}
