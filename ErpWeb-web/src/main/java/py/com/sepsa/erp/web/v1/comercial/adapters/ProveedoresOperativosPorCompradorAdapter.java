package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProveedoresOperativosPorComprador;
import py.com.sepsa.erp.web.v1.comercial.remote.ProveedoresOperativosPorCompradorService;

/**
 * Adaptador para porveedores operativos por comprador.
 *
 * @author alext
 */
public class ProveedoresOperativosPorCompradorAdapter extends DataListAdapter<ProveedoresOperativosPorComprador> {

    /**
     * Servicio de listado.
     */
    private final ProveedoresOperativosPorCompradorService service;
    
    @Override
    public ProveedoresOperativosPorCompradorAdapter fillData(ProveedoresOperativosPorComprador searchData) {
        return service.getProveedoresOperativosPorCompradorList(searchData, getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de ProveedoresOperativosPorCompradorAdapter.
     * @param page
     * @param pageSize 
     */
    public ProveedoresOperativosPorCompradorAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new ProveedoresOperativosPorCompradorService();
    }
    
    /**
     * Constructor de ProveedoresOperativosPorCompradorAdapter.
     */
    public ProveedoresOperativosPorCompradorAdapter() {
        super();
        this.service = new ProveedoresOperativosPorCompradorService();
    }
    
}
