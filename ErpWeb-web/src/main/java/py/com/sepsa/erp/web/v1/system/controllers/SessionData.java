package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.system.pojos.Permisos;
import py.com.sepsa.erp.web.v1.system.pojos.User;

/**
 * Bean para los datos de sesión
 *
 * @author Daniel F. Escauriza Arza
 */
@SessionScoped
@Named("sessionData")
public class SessionData implements Serializable {

    //Mensajes del sistema
    private static String JWT_ERROR;
    private static String LOGOUT_MSN;

    /**
     * JWT
     */
    private String jwt;
    /**
     * JWT Analytis
     */
    private String jwtAnalytics;
    /**
     * Datos del usuario
     */
    private User user;

    /**
     * Error asociado a los datos del token
     */
    private String jwtError;

    /**
     * Mensaje de cierre de sistema
     */
    private String logoutMsn;
    /**
     * Nombre de la empresa
     */
    private String nombreEmpresa;

    /**
     * @return the jwtAnalytics
     */
    public String getJwtAnalytics() {
        return jwtAnalytics;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * @param jwtAnalytics the jwtAnalytics to set
     */
    public void setJwtAnalytics(String jwtAnalytics) {
        this.jwtAnalytics = jwtAnalytics;
    }

    /**
     * Obtiene el JWT
     *
     * @return JWT
     */
    public String getJwt() {
        return jwt;
    }

    /**
     * Setea el JWT
     *
     * @param jwt JWT
     */
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    /**
     * Obtiene los datos del usuario
     *
     * @return Datos del usuario
     */
    public User getUser() {
        return user;
    }

    /**
     * Setea los datos del usuario
     *
     * @param user Datos del usuario
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Obtiene el mensaje de error asociado al JWT
     *
     * @return Mensaje de error asociado al JWT
     */
    public String getJwtError() {
        return jwtError;
    }

    /**
     * Setea el mensaje de error asociado al JWT
     *
     * @param jwtError Mensaje de error asociado al JWT
     */
    public void setJwtError(String jwtError) {
        this.jwtError = jwtError;
    }

    /**
     * Obtiene el mensaje de cierre de sistema
     *
     * @return Mensaje de cierre de sistema
     */
    public String getLogoutMsn() {
        return logoutMsn;
    }

    /**
     * Setea el mensaje de cierre de sistema
     *
     * @param logoutMsn Mensaje de cierre de sistema
     */
    public void setLogoutMsn(String logoutMsn) {
        this.logoutMsn = logoutMsn;
    }

    /**
     * Setea el mensaje de error asociado al JWT
     */
    public void jwtError() {
        this.jwtError = JWT_ERROR;
    }

    /**
     * Setea el mensaje de cierre de sistema
     */
    public void logoutMsn() {
        this.logoutMsn = LOGOUT_MSN;
    }

    /**
     * Método para limpiar los mensaje del sistema
     */
    public void clearMsn() {
        this.jwtError = null;
        this.logoutMsn = null;
    }

    /**
     * Constructor e SessionBean
     *
     * @param jwt JWT
     */
    public SessionData(String jwt) {
        this.jwt = jwt;
    }

    /**
     * Constructor de SessionBean
     */
    public SessionData() {
        JWT_ERROR = "No se encuentra el token de sesión, favor vuelva a "
                + "ingresar sus credenciales en el sistema";
        LOGOUT_MSN = "La sesión ha finalizado";
    }

    public void imprimir() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequest();

        String url = request.getRequestURL().toString();
        String uri = request.getRequestURI();

    }
}
