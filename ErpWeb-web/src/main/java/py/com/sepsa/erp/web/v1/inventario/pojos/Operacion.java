/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;
import java.util.List;

/**
 * Pojo para operacion de inventario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Operacion {

    /**
     * Id
     */
    private Integer id;
    /**
     * id usuario
     */
    private Integer idUsuario;
    /**
     * id cliente
     */
    private Integer idCliente;
    /**
     * id proveedor
     */
    private Integer idProveedor;
    /**
     * Id tipo Operacion
     */
    private Integer idTipoOperacion;
    /**
     * Id Estado
     */
    private Integer idEstado;
    /**
     * Id Motivo operacion
     */
    private Integer idMotivo;
    /**
     * Id local Origen
     */
    private Integer idLocalOrigen;
    /**
     * Id local Destino
     */
    private Integer idLocalDestino;
    /**
     * Id deposito origen
     */
    private Integer idDepositoOrigen;
    /**
     * Id deposito Destino
     */
    private Integer idDepositoDestino;
    /**
     * tipo de operacion
     */
    private String tipoOperacion;
    /**
     * Codigo tipo operacion
     */
    private String codigoTipoOperacion;
    /**
     * Orden de compra
     */
    private String ordenDeCompra;
    /**
     * motivo de opercion
     */
    private String motivo;
    /**
     * codigo motivo operacion
     */
    private String codigoMotivo;
    /**
     * Fecha de Insercion
     */
    private Date fechaInsercion;
    /**
     * Fecha de Insercion Desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha de Insercion Hasta
     */
    private Date fechaInsercionHasta;
    /**
     * Estado
     */
    private String estado;
    /**
     * codigo Estado
     */
    private String codigoEstado;
    /**
     * listado pojo
     */
    private String listadoPojo;
    /**
     * local Origen
     */
    private String localOrigen;
    /**
     * local Destino
     */
    private String localDestino;
    /**
     * Nro de documento Asociado
     */
    private String nroDocumentoAsociado;
    /**
     * Detalles de Operacion
     */
    private List<OperacionInventarioDetalle> operacionInventarioDetalles;
    /**
     * Codigo de Control
     */
    private String nroControl;
    /**
     * idControlInventario
     */
    private Integer idControlInventario;
    private String codigoTipoMotivo;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setCodigoTipoMotivo(String codigoTipoMotivo) {
        this.codigoTipoMotivo = codigoTipoMotivo;
    }

    public String getCodigoTipoMotivo() {
        return codigoTipoMotivo;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setNroControl(String nroControl) {
        this.nroControl = nroControl;
    }

    public String getNroControl() {
        return nroControl;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Integer getIdDepositoOrigen() {
        return idDepositoOrigen;
    }

    public void setIdDepositoOrigen(Integer idDepositoOrigen) {
        this.idDepositoOrigen = idDepositoOrigen;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public String getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void setOrdenDeCompra(String ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(String listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(String localOrigen) {
        this.localOrigen = localOrigen;
    }

    public String getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(String localDestino) {
        this.localDestino = localDestino;
    }

    public String getNroDocumentoAsociado() {
        return nroDocumentoAsociado;
    }

    public void setNroDocumentoAsociado(String nroDocumentoAsociado) {
        this.nroDocumentoAsociado = nroDocumentoAsociado;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public List<OperacionInventarioDetalle> getOperacionInventarioDetalles() {
        return operacionInventarioDetalles;
    }

    public void setOperacionInventarioDetalles(List<OperacionInventarioDetalle> operacionInventarioDetalles) {
        this.operacionInventarioDetalles = operacionInventarioDetalles;
    }

    //</editor-fold>
    public Operacion() {
    }

    public Operacion(Integer id) {
        this.id = id;
    }

}
