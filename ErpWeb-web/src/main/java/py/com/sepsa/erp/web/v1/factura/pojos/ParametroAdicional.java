/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 *
 * @author Williams Vera
 */
public class ParametroAdicional { 
    
    private Integer id;
    
    private Integer idEmpresa;
    
    private String codigo;
    
    private String codigoTipoParametroAdicional;
    
    private String codigoTipoDatoParametroAdicional;
    
    private String descripcion;
    
    private String valorDefecto;
    
    private String datosAdicionales;
    
    private String activo;
    
    private Character mandatorio;
    
    private Character visible;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoTipoParametroAdicional() {
        return codigoTipoParametroAdicional;
    }

    public void setCodigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public String getDatosAdicionales() {
        return datosAdicionales;
    }

    public void setDatosAdicionales(String datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Character getMandatorio() {
        return mandatorio;
    }

    public void setMandatorio(Character mandatorio) {
        this.mandatorio = mandatorio;
    }

    public Character getVisible() {
        return visible;
    }

    public void setVisible(Character visible) {
        this.visible = visible;
    }

    public String getCodigoTipoDatoParametroAdicional() {
        return codigoTipoDatoParametroAdicional;
    }

    public void setCodigoTipoDatoParametroAdicional(String codigoTipoDatoParametroAdicional) {
        this.codigoTipoDatoParametroAdicional = codigoTipoDatoParametroAdicional;
    }
    
}
