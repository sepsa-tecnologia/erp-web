package py.com.sepsa.erp.web.v1.factura.pojos;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * POJO para Cobro
 *
 * @author Romina Núñez,Cristina Insfrán
 */
public class Cobro {

  

    /**
     * Identificador de cobro
     */
    private Integer id;
    /**
     * Identificador de la empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador del cliente
     */
    private Integer idCliente;
    /**
     * Dato de Cliente
     */
    private Cliente cliente; 
    /**
     * Identificador de lugar cobro
     */
    private Integer idLugarCobro;
    /**
     * Lugar cobro
     */
    private LugarCobro lugarCobro;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Monto Cobro
     */
    private BigDecimal montoCobro;   
    /**
     * Monto Cobro
     */
    private BigDecimal montoCobroGuaranies;
    /**
     * Número recibo
     */
    private String nroRecibo;
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * Saldo cliente
     */
    private BigDecimal saldoCliente;
    /**
     * Fecha desde
     */
    private Date fechaDesde;
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    /**
     * Identificador de tipo de transaccion red
     */
    private Integer idTransaccionRed;
    /**
     * Fecha de envío
     */
    private Date fechaEnvio;
    /**
     * Enviado
     */
    private Character enviado;
    /**
     * Transaccion red
     */
    private TransaccionRed transaccionRed;
    /**
     * Detalle de cobro
     */
    private List<CobroDetalle> cobroDetalles;
    //Agregados segun necesidad
    /**
     * Número de Factura
     */
    private String nroFactura;
    /**
     * Total factura
     */
    private BigDecimal totalFactura;
    /**
     * Identificador de la factura
     */
    private Integer idFactura;

    //Incluido según necesidad de vista cobro
    /**
     * Indica si se descuenta de la factura o no la retención
     */
    private String tieneRetention;
    /**
     * Fecha Retencion
     */
    private Date fechaRetencion;
    /**
     * Nro de retencion
     */
    private String nroRetencion;
    /**
     * Porcentaje de retencion
     */
    private Integer porcentajeRetencion;
    /**
     * Monto retenido total
     */
    private BigDecimal montoRetenidoTotal;
    /**
     * Campo digital
     */
    private String digital;
    /**
     * Estado del cobro
     */
    private String codigoEstado;
    /**
     * Codigo lugar cobro
     */
    private String codigoLugarCobro;
    /**
     * Identificador del motivo de la anulación
     */
    private Integer idMotivoAnulacion;
    /**
     * Observación anulación
     */
    private String observacionAnulacion;
    /**
     * Estado 
     */
    private Estado estado;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Talonario
     */
    private Talonario talonario;
    /**
     * Empresa
     */
    private Empresa empresa;
    private String razonSocial;
    
    private BigDecimal montoCobroFicticio;
    
    private String codigoMoneda;
    
    private Integer idTipoCambio;
    
    private List<Integer> idCobros;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    
    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }
    
      /**
     * @return the talonario
     */
    public Talonario getTalonario() {
        return talonario;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param talonario the talonario to set
     */
    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the idEstado
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * @param idEstado the idEstado to set
     */
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

   
    /**
     * @return the idMotivoAnulacion
     */
    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    /**
     * @param idMotivoAnulacion the idMotivoAnulacion to set
     */
    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    /**
     * @return the observacionAnulacion
     */
    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    /**
     * @param observacionAnulacion the observacionAnulacion to set
     */
    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    /**
     * @return the idEmpresa
     */
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    /**
     * @return the codigoLugarCobro
     */
    public String getCodigoLugarCobro() {
        return codigoLugarCobro;
    }

    /**
     * @param codigoLugarCobro the codigoLugarCobro to set
     */
    public void setCodigoLugarCobro(String codigoLugarCobro) {
        this.codigoLugarCobro = codigoLugarCobro;
    }

    /**
     * @return the cobroDetalles
     */
    public List<CobroDetalle> getCobroDetalles() {
        return cobroDetalles;
    }

    /**
     * @param cobroDetalles the cobroDetalles to set
     */
    public void setCobroDetalles(List<CobroDetalle> cobroDetalles) {
        this.cobroDetalles = cobroDetalles;
    }

    /**
     * @return the codigoEstado
     */
    public String getCodigoEstado() {
        return codigoEstado;
    }

    /**
     * @param codigoEstado the codigoEstado to set
     */
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    /**
     * @return the digital
     */
    public String getDigital() {
        return digital;
    }

    /**
     * @param digital the digital to set
     */
    public void setDigital(String digital) {
        this.digital = digital;
    }

    /**
     * @return the idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    
    /**
     * @return the fechaRetencion
     */
    public Date getFechaRetencion() {
        return fechaRetencion;
    }

    /**
     * @param fechaRetencion the fechaRetencion to set
     */
    public void setFechaRetencion(Date fechaRetencion) {
        this.fechaRetencion = fechaRetencion;
    }

    /**
     * @return the nroRetencion
     */
    public String getNroRetencion() {
        return nroRetencion;
    }

    /**
     * @param nroRetencion the nroRetencion to set
     */
    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    /**
     * @return the porcentajeRetencion
     */
    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    /**
     * @param porcentajeRetencion the porcentajeRetencion to set
     */
    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    /**
     * @return the montoRetenidoTotal
     */
    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    /**
     * @param montoRetenidoTotal the montoRetenidoTotal to set
     */
    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    /**
     * @return the idFactura
     */
    public Integer getIdFactura() {
        return idFactura;
    }

    /**
     * @param idFactura the idFactura to set
     */
    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    /**
     * @return the tieneRetention
     */
    public String getTieneRetention() {
        return tieneRetention;
    }

    /**
     * @param tieneRetention the tieneRetention to set
     */
    public void setTieneRetention(String tieneRetention) {
        this.tieneRetention = tieneRetention;
    }

    /**
     * @return the lugarCobro
     */
    public LugarCobro getLugarCobro() {
        return lugarCobro;
    }

    /**
     * @param lugarCobro the lugarCobro to set
     */
    public void setLugarCobro(LugarCobro lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    /**
     * @return the totalFactura
     */
    public BigDecimal getTotalFactura() {
        return totalFactura;
    }

    /**
     * @param totalFactura the totalFactura to set
     */
    public void setTotalFactura(BigDecimal totalFactura) {
        this.totalFactura = totalFactura;
    }

  

    /**
     * @return the transaccionRed
     */
    public TransaccionRed getTransaccionRed() {
        return transaccionRed;
    }

    /**
     * @param transaccionRed the transaccionRed to set
     */
    public void setTransaccionRed(TransaccionRed transaccionRed) {
        this.transaccionRed = transaccionRed;
    }

    /**
     * @return the enviado
     */
    public Character getEnviado() {
        return enviado;
    }

    /**
     * @param enviado the enviado to set
     */
    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    /**
     * @return the fechaEnvio
     */
    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    /**
     * @param fechaEnvio the fechaEnvio to set
     */
    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    /**
     * @return the idTransaccionRed
     */
    public Integer getIdTransaccionRed() {
        return idTransaccionRed;
    }

    /**
     * @param idTransaccionRed the idTransaccionRed to set
     */
    public void setIdTransaccionRed(Integer idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public List<Integer> getIdCobros() {
        return idCobros;
    }

    public void setIdCobros(List<Integer> idCobros) {
        this.idCobros = idCobros;
    }
    
    


    /**
     * @return the saldoCliente
     */
    public BigDecimal getSaldoCliente() {
        return saldoCliente;
    }

    /**
     * @param saldoCliente the saldoCliente to set
     */
    public void setSaldoCliente(BigDecimal saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    /**
     * @return the fechaHasta
     */
    public Date getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    /**
     * @return the fechaDesde
     */
    public Date getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }
    
    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getMontoCobroFicticio() {
        return montoCobroFicticio;
    }

    public void setMontoCobroFicticio(BigDecimal montoCobroFicticio) {
        this.montoCobroFicticio = montoCobroFicticio;
    }

//</editor-fold>

    /**
     * Constructor de la clase
     */
    public Cobro() {
        this.cobroDetalles = new ArrayList<>();
    }
    /**
     * Constructor con parametros
     *
     * @param id
     * @param idLugarCobro
     * @param lugarCobro
     * @param fecha
     * @param montoCobro
     * @param nroRecibo
     * @param idTalonario
     * @param estado
     */
    public Cobro(Integer id, Integer idLugarCobro, LugarCobro lugarCobro,
            Date fecha, BigDecimal montoCobro, String nroRecibo,
            Integer idTalonario, Estado estado) {
        this.id = id;
        this.idLugarCobro = idLugarCobro;
        this.lugarCobro = lugarCobro;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idTalonario = idTalonario;
        this.estado = estado;
    }

    /**
     * Constructor con parámetros
     * @param id
     * @param idCliente
     * @param cliente
     * @param idLugarCobro
     * @param lugarCobro
     * @param fecha
     * @param montoCobro
     * @param nroRecibo
     * @param idTalonario
     * @param estado
     * @param saldoCliente
     * @param fechaDesde
     * @param fechaHasta
     * @param idTransaccionRed
     * @param fechaEnvio
     * @param enviado
     * @param transaccionRed
     * @param cobroDetalles
     * @param nroFactura
     * @param totalFactura
     * @param idFactura
     * @param tieneRetention
     * @param fechaRetencion
     * @param nroRetencion
     * @param porcentajeRetencion
     * @param montoRetenidoTotal
     * @param digital 
     */
    public Cobro(Integer id, Integer idCliente, Cliente cliente, Integer idLugarCobro,
            LugarCobro lugarCobro, Date fecha, BigDecimal montoCobro, String nroRecibo, 
            Integer idTalonario, Estado estado, BigDecimal saldoCliente, Date fechaDesde, 
            Date fechaHasta, Integer idTransaccionRed, Date fechaEnvio, Character enviado,
            TransaccionRed transaccionRed, List<CobroDetalle> cobroDetalles, String nroFactura, 
            BigDecimal totalFactura, Integer idFactura, String tieneRetention, Date fechaRetencion,
            String nroRetencion, Integer porcentajeRetencion, BigDecimal montoRetenidoTotal, 
            String digital) {
        this.id = id;
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.idLugarCobro = idLugarCobro;
        this.lugarCobro = lugarCobro;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idTalonario = idTalonario;
        this.estado = estado;
        this.saldoCliente = saldoCliente;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.idTransaccionRed = idTransaccionRed;
        this.fechaEnvio = fechaEnvio;
        this.enviado = enviado;
        this.transaccionRed = transaccionRed;
        this.cobroDetalles = cobroDetalles;
        this.nroFactura = nroFactura;
        this.totalFactura = totalFactura;
        this.idFactura = idFactura;
        this.tieneRetention = tieneRetention;
        this.fechaRetencion = fechaRetencion;
        this.nroRetencion = nroRetencion;
        this.porcentajeRetencion = porcentajeRetencion;
        this.montoRetenidoTotal = montoRetenidoTotal;
        this.digital = digital;
    }


}
