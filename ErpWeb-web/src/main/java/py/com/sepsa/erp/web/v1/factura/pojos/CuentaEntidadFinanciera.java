package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * POJO para CuentaEntidadFinanciera
 *
 * @author Romina Núñez
 */
public class CuentaEntidadFinanciera {

    /**
     * Identificador de cuenta entidad financiera
     */
    private Integer id;
    /**
     * Identificador de entidad financiera
     */
    private Integer idEntidadFinanciera;
    /**
     * Descripción de entidad financiera
     */
    private String entidadFinanciera;
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    /**
     * Descripción de persona
     */
    private String persona;
    /**
     * Número de Cuenta
     */
    private String nroCuenta;
    /**
     * Fecha de Alta
     */
    private Date fechaAlta;
    /**
     * Estado
     */
    private String estado;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }
    
    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }
    
    public String getEntidadFinanciera() {
        return entidadFinanciera;
    }
    
    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }
    
    public Integer getIdPersona() {
        return idPersona;
    }
    
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }
    
    public String getPersona() {
        return persona;
    }
    
    public void setPersona(String persona) {
        this.persona = persona;
    }
    
    public String getNroCuenta() {
        return nroCuenta;
    }
    
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }
    
    public Date getFechaAlta() {
        return fechaAlta;
    }
    
    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
//</editor-fold>


    /**
     * Constructor de la clase
     */
    public CuentaEntidadFinanciera() {
    }

    
    /**
     * Constructor con parámetros
     * @param id
     * @param idEntidadFinanciera
     * @param entidadFinanciera
     * @param idPersona
     * @param persona
     * @param nroCuenta
     * @param fechaAlta
     * @param estado 
     */
    public CuentaEntidadFinanciera(Integer id, Integer idEntidadFinanciera, String entidadFinanciera, Integer idPersona, String persona, String nroCuenta, Date fechaAlta, String estado) {
        this.id = id;
        this.idEntidadFinanciera = idEntidadFinanciera;
        this.entidadFinanciera = entidadFinanciera;
        this.idPersona = idPersona;
        this.persona = persona;
        this.nroCuenta = nroCuenta;
        this.fechaAlta = fechaAlta;
        this.estado = estado;
    }

    

}
