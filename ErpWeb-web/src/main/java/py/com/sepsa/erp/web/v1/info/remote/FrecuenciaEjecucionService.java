/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.FrecuenciaEjecucionAdapter;
import py.com.sepsa.erp.web.v1.info.filters.FrecuenciaEjecucionFilter;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para Frecuencia ejecucion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class FrecuenciaEjecucionService extends APIErpCore {

    /**
     * Obtiene la lista de frecuencia Ejecucion
     *
     * @param frecuenciaEjecucion
     * @param page
     * @param pageSize
     * @return
     */
    public FrecuenciaEjecucionAdapter getFrecuenciaEjecucionList(FrecuenciaEjecucion frecuenciaEjecucion, Integer page,
            Integer pageSize) {

        FrecuenciaEjecucionAdapter lista = new FrecuenciaEjecucionAdapter();

        Map params = FrecuenciaEjecucionFilter.build(frecuenciaEjecucion, page, pageSize);

        HttpURLConnection conn = GET(Resource.FRECUENCIA_EJECUCION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FrecuenciaEjecucionAdapter.class);

            lista = (FrecuenciaEjecucionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     *
     * @param id
     * @return
     */
    public FrecuenciaEjecucion get(Integer id) {
        FrecuenciaEjecucion data = new FrecuenciaEjecucion(id);
        FrecuenciaEjecucionAdapter list = getFrecuenciaEjecucionList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para editar FrecuenciaEjecucion
     *
     * @param frecuenciaEjecucion
     * @return
     */
    public BodyResponse<FrecuenciaEjecucion> editFrecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.FRECUENCIA_EJECUCION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(frecuenciaEjecucion));
            response = BodyResponse.createInstance(conn, FrecuenciaEjecucion.class);
        }
        return response;
    }

    /**
     * Constructor de ProductoService
     */
    public FrecuenciaEjecucionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        FRECUENCIA_EJECUCION("Listado de tipos frecuencia ejecucion", "frecuencia-ejecucion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
