/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * Pojo para motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmision {

    /**
     * Id
     */
    private Integer id;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Descripcion
     */
    private String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Constructor de la clase
     */
    public MotivoEmision() {

    }

    /**
     * Constructor de la clase
     */
    public MotivoEmision(Integer id) {
        this.id = id;
    }

}
