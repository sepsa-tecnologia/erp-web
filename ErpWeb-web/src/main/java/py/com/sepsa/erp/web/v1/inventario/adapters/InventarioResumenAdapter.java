package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.inventario.remote.InventarioService;

/**
 * Adaptador para la lista de inventario
 *
 * @author Romina Núñez
 */
public class InventarioResumenAdapter extends DataListAdapter<InventarioResumen> {

    /**
     * Cliente remoto para inventario
     */
    private final InventarioService inventario;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return InventarioAdapter
     */
    @Override
    public InventarioResumenAdapter fillData(InventarioResumen searchData) {

        return inventario.getInventarioResumen(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de InventarioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public InventarioResumenAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.inventario = new InventarioService();
    }

    /**
     * Constructor de InventarioAdapter
     */
    public InventarioResumenAdapter() {
        super();
        this.inventario = new InventarioService();
    }

}
