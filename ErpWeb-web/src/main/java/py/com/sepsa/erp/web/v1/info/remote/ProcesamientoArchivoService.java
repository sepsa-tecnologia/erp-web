package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ProcesamientoArchivoAdapter;
import py.com.sepsa.erp.web.v1.proceso.filters.ProcesamientoArchivoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.ProcesamientoArchivo;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para procesamiento archivo
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
public class ProcesamientoArchivoService extends APIErpCore {

    public ProcesamientoArchivoAdapter getProcesamientoArchivoList(ProcesamientoArchivo pArchivo, Integer page, Integer pageSize) {

        ProcesamientoArchivoAdapter lista = new ProcesamientoArchivoAdapter();

        Map params = ProcesamientoArchivoFilter.build(pArchivo, page, pageSize);

        HttpURLConnection conn = GET(Resource.PROCESAMIENTO_ARCHIVO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProcesamientoArchivoAdapter.class);

            lista = (ProcesamientoArchivoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para reenviar
     *
     * @param id
     * @return
     */
    public BodyResponse<ProcesamientoArchivo> reenviar(Integer id) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.PROCESAMIENTO_ARCHIVO_REENVIAR.url + id, ContentType.JSON);
        if (conn != null) {
            response = BodyResponse.createInstance(conn, ProcesamientoArchivo.class);
        }
        return response;
    }

    /**
     * Constructor de la clase
     */
    public ProcesamientoArchivoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        PROCESAMIENTO_ARCHIVO("Procesamiento de Archivos", "procesamiento-archivo"),
        PROCESAMIENTO_ARCHIVO_REENVIAR("Procesamiento de Archivos", "procesamiento-archivo/reenviar/");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
