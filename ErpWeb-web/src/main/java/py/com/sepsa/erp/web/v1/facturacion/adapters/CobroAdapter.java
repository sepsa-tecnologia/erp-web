
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;

/**
 * Adaptador de la lista de cobro
 * @author Romina Núñez
 */
public class CobroAdapter extends DataListAdapter<Cobro> {
    
    /**
     * Cliente para el servicio de cobro
     */
    private final CobroService serviceCobro;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CobroAdapter fillData(Cobro searchData) {
     
        return serviceCobro.getCobroList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de CobroAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobroAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceCobro = new CobroService();
    }

    /**
     * Constructor de CobroAdapter
     */
    public CobroAdapter() {
        super();
        this.serviceCobro = new CobroService();
    }
}
