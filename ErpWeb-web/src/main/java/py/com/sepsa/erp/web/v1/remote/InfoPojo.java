/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.remote;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import org.json.JSONObject;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * POJO para el manejo de información de conexión a la API
 *
 * @author Jonathan
 */
public class InfoPojo {

    /**
     * Protocolo
     */
    private API.Scheme protocol;

    /**
     * Host de conexión
     */
    private String host;

    /**
     * Puerto de conexión
     */
    private Integer port;
    /**
     * Host de conexión
     */
    private String user;
    /**
     * Host de conexión
     */
    private String pass;
    /**
     * Token
     */
    private String apiTokenSepsaCore;

    /**
     * Crea una instancia de información de conexión
     *
     * @param api API
     * @return InfoPojo
     */
    public static InfoPojo createInstance(String api) {

        InfoPojo result = new InfoPojo();

        if (api == null || api.trim().isEmpty()) {
            return result;
        }

        try {
            InputStream is = InfoPojo.class
                    .getClassLoader()
                    .getResourceAsStream("connection-info.json");

            //String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
            String text = new BufferedReader(
                    new InputStreamReader(is, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));

            JSONObject object = new JSONObject(text);
            String entorno = object.getString("entorno");
            JSONObject apisInfo = object.getJSONObject(entorno);
            JSONObject apiInfo = apisInfo.getJSONObject(api);

            ObjectMapper objectMapper = new ObjectMapper();

            result = objectMapper.readValue(apiInfo.toString(), InfoPojo.class);

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

        return result;
    }

    /**
     * Crea una instancia de información de url
     *
     * @param api
     * @return
     */
    public static String createInstanceUrl(String api) {

        String result = new String();

        if (api == null || api.trim().isEmpty()) {
            return result;
        }

        try {
            InputStream is = InfoPojo.class
                    .getClassLoader()
                    .getResourceAsStream("connection-info.json");

            //String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
            String text = new BufferedReader(
                    new InputStreamReader(is, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));

            JSONObject object = new JSONObject(text);
            String entorno = object.getString("entorno");
            JSONObject apisInfo = object.getJSONObject(entorno);

            result = apisInfo.getString("api-url");

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

        return result;
    }

    /**
     * Crea una instancia de información de conexión
     *
     * @param api API
     * @return InfoPojo
     */
    public static InfoPojo createInstanceUser(String api) {

        InfoPojo result = new InfoPojo();

        if (api == null || api.trim().isEmpty()) {
            return result;
        }

        try {
            InputStream is = InfoPojo.class
                    .getClassLoader()
                    .getResourceAsStream("connection-info.json");
            //String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
            String text = new BufferedReader(
                    new InputStreamReader(is, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));
            /* JSONObject object = new JSONObject(text);
            JSONObject entorno = object.getJSONObject("login-sistemas");*/
            JSONObject object = new JSONObject(text);
            String entorno = object.getString("entorno");
            JSONObject apisInfo = object.getJSONObject(entorno);
            //JSONObject apiInfo = apisInfo.getJSONObject(api);
            WebLogger.get().debug("apisInfo:" + apisInfo);

            ObjectMapper objectMapper = new ObjectMapper();

            result = objectMapper.readValue(apisInfo.getJSONObject("login-sistemas").toString(), InfoPojo.class);

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

        return result;
    }

    /**
     * Crea una instancia de información de conexión
     *
     * @param api API
     * @return InfoPojo
     */
    public static InfoPojo createInstanceSiediMonitor(String api) {

        ConfiguracionValorListAdapter adapterConfValor = new ConfiguracionValorListAdapter();
        ConfiguracionValor cv = new ConfiguracionValor();
        cv.setActivo("S");
        cv.setCodigoConfiguracion(api);

        adapterConfValor = adapterConfValor.fillData(cv);

        InfoPojo result = new InfoPojo();

        if (adapterConfValor.getData().get(0).getValor() == null || adapterConfValor.getData().get(0).getValor().trim().isEmpty()) {
            return result;
        }

        try {

            ObjectMapper objectMapper = new ObjectMapper();

            result = objectMapper.readValue(adapterConfValor.getData().get(0).getValor(), InfoPojo.class);

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

        return result;
    }

    public API.Scheme getProtocol() {
        return protocol;
    }

    public void setProtocol(API.Scheme protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getApiTokenSepsaCore() {
        return apiTokenSepsaCore;
    }

    public void setApiTokenSepsaCore(String apiTokenSepsaCore) {
        this.apiTokenSepsaCore = apiTokenSepsaCore;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

}
