/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;

import java.io.InputStream;
import java.io.OutputStream;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author SEPSA
 */
public class DownloadUtils {

    /**
     * Permite enviar un archivo como respuesta a una peticion JSF no ajax.
     *
     * @param stream Stream de datos a enviar.
     * @param fileName Nombre de archivo para el lado del cliente.
     * @param contentLength Tamnanho del archivo en disco.
     * @param contentType MIME Type del archivo.
     * @throws Exception Si no se puede enviar el archivo.
     */
    public static void sendFile(InputStream stream, String fileName, int contentLength, String contentType) throws Exception {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try (InputStream input = stream) {
            ExternalContext ec = ctx.getExternalContext();
            ec.responseReset();
            ec.setResponseContentType(contentType);
            ec.setResponseContentLength(contentLength);
            ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            try (OutputStream output = ec.getResponseOutputStream()) {
                byte[] chunk = new byte[4096];
                int read;
                while ((read = input.read(chunk)) >= 0) {
                    output.write(chunk, 0, read);
                    output.flush();
                }
            }
        }
        ctx.responseComplete();
    }

}
