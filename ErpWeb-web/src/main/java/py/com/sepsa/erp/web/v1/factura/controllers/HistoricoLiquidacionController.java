package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;

/**
 * Controlador para Histórico Liquidación
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("liquidacion")
public class HistoricoLiquidacionController implements Serializable {

    /**
     * Adaptador para la lista de liquidacion
     */
    private HistoricoLiquidacionAdapter adapterLiquidacion;
    /**
     * POJO HistoricoLiquidacion
     */
    private HistoricoLiquidacion liquidacionFilter;
    /**
     * Bandera
     */
    private boolean show;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public HistoricoLiquidacionAdapter getAdapterLiquidacion() {
        return adapterLiquidacion;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public void setAdapterLiquidacion(HistoricoLiquidacionAdapter adapterLiquidacion) {
        this.adapterLiquidacion = adapterLiquidacion;
    }

    public void setLiquidacionFilter(HistoricoLiquidacion liquidacionFilter) {
        this.liquidacionFilter = liquidacionFilter;
    }

    public HistoricoLiquidacion getLiquidacionFilter() {
        return liquidacionFilter;
    }

//</editor-fold>
    /**
     * Método para seleccionar el cliente
     *
     * @param event
     */
    public void onItemSelectClienteLiquidacion(SelectEvent event) {
        show = true;
        liquidacionFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        filterLiquidacion();

    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idHistoricoLiquidacion 
     * @return
     */
    public String detalleLiquidacionURL(Integer idHistoricoLiquidacion) {
        return String.format("liquidacion-detalle-list"
                + "?faces-redirect=true"
                + "&idHistoricoLiquidacion=%d", idHistoricoLiquidacion);
    }

    /**
     * Método para llenar el adapter
     */
    public void filterLiquidacion() {
        adapterLiquidacion = adapterLiquidacion.fillData(liquidacionFilter);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        show = false;
        this.liquidacionFilter = new HistoricoLiquidacion();
        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();

    }

}
