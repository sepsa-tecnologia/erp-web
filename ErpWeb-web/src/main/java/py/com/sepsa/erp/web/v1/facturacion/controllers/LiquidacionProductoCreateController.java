/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.facturacion.pojos.LiquidacionProducto;
import py.com.sepsa.erp.web.v1.facturacion.remote.LiquidacionProductoService;

/**
 * Controlador para Crear Liquidacion Producto
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("crearLiquidacionProducto")
public class LiquidacionProductoCreateController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final LiquidacionProductoService service;

    /**
     * Datos del liquidacionProducto
     */
    private LiquidacionProducto liquidacionProducto;

    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;

    /**
     * Datos del cliente
     */
    private Cliente cliente;

    /**
     * Adaptador para la lista de productos
     */
    private ProductoAdapter productoListAdapter;

    /**
     * Pojo de tipo Repositorio
     */
    private Producto producto;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public LiquidacionProducto getLiquidacionProducto() {
        return liquidacionProducto;
    }

    public void setLiquidacionProducto(LiquidacionProducto liquidacionProducto) {
        this.liquidacionProducto = liquidacionProducto;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ProductoAdapter getProductoListAdapter() {
        return productoListAdapter;
    }

    public void setProductoListAdapter(ProductoAdapter productoListAdapter) {
        this.productoListAdapter = productoListAdapter;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    //</editor-fold>

    /**
     * Método para crear Liquidacion Producto
     */
    public void create() {
        Integer idLiquidacionProducto = null;
        idLiquidacionProducto = service.setLiquidacionProducto(this.liquidacionProducto);
        if (idLiquidacionProducto != null) {
            init();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "¡Creado con éxito!", "¡ Creado con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "¡Error al crear!", "¡Error al crear!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }


    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.liquidacionProducto.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Método para obtener los Productos
     */
    public void getProductos() {
        this.setProductoListAdapter(getProductoListAdapter().fillData(getProducto()));
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.liquidacionProducto = new LiquidacionProducto();
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        this.productoListAdapter = new ProductoAdapter();
        this.producto = new Producto();
        getProductos();
    }

    /**
     * Constructor
     */
    public LiquidacionProductoCreateController() {
        init();
        this.service = new LiquidacionProductoService();
    }
}
