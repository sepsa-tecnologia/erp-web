
package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoDescuento;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.filters.TypeFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;

/**
 * Filtro para la lista de tipo descuento
 * @author Cristina Insfrán
 */
public class TipoDescuentoFilter extends Filter{
     /**
     * Agrega el filtro de identificador del tipo de dato
     *
     * @param id Identificador del tipo de dato
     * @return
     */
    public TipoDescuentoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de descripción 
     *
     * @param descripcion
     * @return
     */
    public TipoDescuentoFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
     /**
     * Agrega el filtro código 
     *
     * @param codigo
     * @return
     */
    public TipoDescuentoFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     *
     * @param tipodescuento
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoDescuento tipodescuento, Integer page, Integer pageSize) {
        TipoDescuentoFilter filter = new TipoDescuentoFilter();
        
        filter
                .id(tipodescuento.getId())
                .descripcion(tipodescuento.getDescripcion())
                .codigo(tipodescuento.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoDescuentoFilter
     */
    public TipoDescuentoFilter() {
        super();
    }
}
