package py.com.sepsa.erp.web.v1.remote;

/**
 * APIErpFacturacion para conexiones al servicio.
 * @author Romina Núñez
 */
public class APIErpFacturacion extends API {

    //Parámetros de conexiones.
    private final static String BASE_API = "erp-facturacion-api/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;

    /**
     * Constructor de API
     */
    public APIErpFacturacion() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);
        
        InfoPojo info = InfoPojo.createInstance("api-erp-facturacion");
        
        updateConnInfo(info);
    }
}
