/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;
import py.com.sepsa.erp.web.v1.info.remote.FrecuenciaEjecucionService;
import py.com.sepsa.erp.web.v1.info.remote.ProcesoService;

/**
 * Controlador para la vista editar proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("procesoEdit")

public class ProcesoEditController implements Serializable {

    /**
     * Cliente para el servicio proceso.
     */
    private final ProcesoService service;
    /**
     * Cliente para el servicio proceso.
     */
    private final FrecuenciaEjecucionService service2;
    /**
     * POJO del proceso
     */
    private Proceso proceso;
    /**
     * Adaptador de la lista de tipo procesos
     */
    private TipoProcesoAdapter tipoProcesoAdapter;
    /**
     * Datos del cliente
     */
    private TipoProceso tipoProceso;
    /**
     * Datos de frecuenciaEjecucion
     */
    private FrecuenciaEjecucion frecuenciaEjecucion;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Proceso getProceso() {
        return proceso;
    }

    public void setProceso(Proceso proceso) {
        this.proceso = proceso;
    }

    public TipoProcesoAdapter getTipoProcesoAdapter() {
        return tipoProcesoAdapter;
    }

    public void setTipoProcesoAdapter(TipoProcesoAdapter tipoProcesoAdapter) {
        this.tipoProcesoAdapter = tipoProcesoAdapter;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(TipoProceso tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public FrecuenciaEjecucion getFrecuenciaEjecucion() {
        return frecuenciaEjecucion;
    }

    public void setFrecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        this.frecuenciaEjecucion = frecuenciaEjecucion;
    }

    //</editor-fold>
    /**
     * Método para editar Proceso
     */
    public void edit() {
        BodyResponse<Proceso> respuestaTal = service.editProceso(this.proceso);
        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Proceso editado correctamente!"));
        }

        BodyResponse<FrecuenciaEjecucion> respuestaTal2 = service2.editFrecuenciaEjecucion(this.proceso.getFrecuenciaEjecucion());
        if (respuestaTal2.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Frecuencia De Ejecucion del proceso editado correctamente!"));
        }
    }

    /**
     * Método para obtener las tipoProcesos
     */
    public void getTipoProcesos() {
        this.setTipoProcesoAdapter(getTipoProcesoAdapter().fillData(getTipoProceso()));
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        this.proceso = service.get(id);
        this.tipoProcesoAdapter = new TipoProcesoAdapter();
        this.tipoProceso = new TipoProceso();
        getTipoProcesos();
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        this.proceso.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Constructor
     */
    public ProcesoEditController() {
        this.service2 = new FrecuenciaEjecucionService();
        this.service = new ProcesoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
