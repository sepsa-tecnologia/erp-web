package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;

/**
 * Pojo para inventario.
 *
 * @author Romina Núñez
 */
public class InventarioDetalle {

    /**
     * Identificador de inventario
     */
    private Integer id;

    /**
     * Identificador de depósito
     */
    private Integer idInventario;
    /**
     * Fecha Vto
     */
    private Date fechaVencimiento;
    /**
     * Identificador de depósito
     */
    private Integer idDepositoLogistico;

    /**
     * Cosigo de deposito
     */
    private String codigoDepositoLogistico;

    /**
     * Deposito de almacenamiento
     */
    private String depositoLogistico;

    /**
     * Identificador de producto
     */
    private Integer idProducto;

    /**
     * Producto almacenado
     */
    private String producto;

    /**
     * Codigo Gtin del producto
     */
    private String codigoGtin;

    /**
     * Codigo interno del producto
     */
    private String codigoInterno;

    /**
     * Identificador de estado
     */
    private Integer idEstado;

    /**
     * Estado del producto
     */
    private String estado;

    /**
     * Codigo de estado del producto
     */
    private String codigoEstado;

    /**
     * Parámetro para cantidad
     */
    private Integer cantidad;

    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
        /**
     * Listado Pojo
     */
    private Boolean tieneFechaVencimiento;
    

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getId() {
        return id;
    }

    public void setTieneFechaVencimiento(Boolean tieneFechaVencimiento) {
        this.tieneFechaVencimiento = tieneFechaVencimiento;
    }

    public Boolean getTieneFechaVencimiento() {
        return tieneFechaVencimiento;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getCodigoDepositoLogistico() {
        return codigoDepositoLogistico;
    }

    public void setCodigoDepositoLogistico(String codigoDepositoLogistico) {
        this.codigoDepositoLogistico = codigoDepositoLogistico;
    }

    public String getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(String depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }
    

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public InventarioDetalle() {
    }

}
