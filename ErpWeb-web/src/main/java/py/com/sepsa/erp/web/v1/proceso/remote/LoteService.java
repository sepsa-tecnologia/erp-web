/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.proceso.filters.LoteFilter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.adapters.LoteAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.remote.API;
import py.com.sepsa.erp.web.v1.remote.API.ContentType;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 *
 * @author Antonella Lucero
 */
public class LoteService extends APIErpCore {

    /**
     * Método que obtiene el listado de lotes
     *
     * @param l  objeto
     * @param page
     * @param pageSize
     * @return
     */
    public LoteAdapter getLoteList(Lote l, Integer page,
            Integer pageSize) {

        LoteAdapter lista = new LoteAdapter();

        Map params = LoteFilter.build(l, page, pageSize);

        HttpURLConnection conn = GET(Resource.LOTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LoteAdapter.class);

            lista = (LoteAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Método para crear Lote
     *
     * @param lote
     * @return
     */
    public BodyResponse<Lote> createLote(Lote lote) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.LOTE.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(lote));

            response = BodyResponse.createInstance(conn, Lote.class);

        }
        return response;
    }
    
    public byte[] obtenerResultadoLote(Lote loteCarga) {
        byte[] result = null;
        Map params = new HashMap();

        String url = String.format(Resource.OBTENER_RESULTADO.url,loteCarga.getId().toString());
        HttpURLConnection conn = GET(String.format(url),
                ContentType.JSON, params);

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }
    
    public byte[] obtenerArchivosLote(Lote loteCarga) {
        byte[] result = null;
        Map params = new HashMap();

        String url = String.format(Resource.OBTENER_ARCHIVO_LOTE.url,loteCarga.getId().toString());
        HttpURLConnection conn = GET(String.format(url),
                ContentType.JSON, params);

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }

    /**
     * Constructor de la clase
     */
    public LoteService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LOTE("Lote", "lote"),
        OBTENER_RESULTADO("Obtener Resultado Lote", "lote/resultado/%s"),
        OBTENER_ARCHIVO_LOTE("Obtener Resultado Lote", "lote/archivos-relacionados/%s");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
