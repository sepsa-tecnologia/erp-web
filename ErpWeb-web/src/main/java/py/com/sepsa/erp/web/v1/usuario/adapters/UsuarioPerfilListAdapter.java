
package py.com.sepsa.erp.web.v1.usuario.adapters;



import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfil;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioPerfilServiceClient;

/**
 * Adaptador para la lista de perfiles de usuario
 * @author Cristina Insfrán
 */
public class UsuarioPerfilListAdapter extends DataListAdapter<UsuarioPerfil>{
    
     /**
     * Cliente para los servicios de clientes
     */
    private final UsuarioPerfilServiceClient usuarioPerfilClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioPerfilListAdapter fillData(UsuarioPerfil searchData) {
        return usuarioPerfilClient.getUserPerfilList(searchData, getFirstResult(), getPageSize());

    }

    /**
     * Constructor de UsuarioPerfilListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioPerfilListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.usuarioPerfilClient = new UsuarioPerfilServiceClient();
    }

    /**
     * Constructor de UsuarioPerfilListAdapter
     */
    public UsuarioPerfilListAdapter() {
        super();
        this.usuarioPerfilClient = new UsuarioPerfilServiceClient();
    }

}
