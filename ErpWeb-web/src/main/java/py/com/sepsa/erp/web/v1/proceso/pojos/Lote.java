/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.pojos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Antonella Lucero
 */
public class Lote {
    /**
     * Identificador de procesamiento lote
     */
    private Integer id;
    /**
     * Identificador del tipo lote
     */
    private Integer idTipoLote;
    /**
     * Código del tipo lote
     */
    private String codigoTipoLote;
    
    private String tipoLote;
    /**
     * Identificador del la empresa
     */
    private Integer idEmpresa;
    /**
     * Razón Social
     */
    private String empresa;
    /**
     * Identificador del usuario
     */
    private Integer idUsuario;
    /**
     * Usuario
     */
    private String usuario;
    /**
     * Código del estado del procesamiento lote
     */
    private String codigoEstado;
    /**
     * Código Estado
     */
    private String estado;
    /**
     * Identificador del estado
     */
    private Integer idEstado;
    private Date fechaInsercionDesde;
    private Date fechaInsercionHasta;
    /**
     * Fecha Inserción del procesamiento Lote
     */
    private Date fechaInsercion;
    /**
     * Fecha Procesamiento del procesamiento Lote
     */
    private Date fechaProcesamiento;
    /**
     * Email al que notificar
     */
    private String email;
    
    private ArrayList<Archivo> archivos;
    
    private String nombreArchivo;
    
    private Character notificado;
    
    private String resultado;
    
    private String observacion;
    
    /**
     * Dato para filtro de listado
     */
    private Boolean listadoPojo;
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public String getCodigoTipoLote() {
        return codigoTipoLote;
    }

    public void setCodigoTipoLote(String codigoTipoLote) {
        this.codigoTipoLote = codigoTipoLote;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<Archivo> getArchivos() {
        return archivos;
    }

    public void setArchivos(ArrayList<Archivo> archivos) {
        this.archivos = archivos;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Character getNotificado() {
        return notificado;
    }

    public void setNotificado(Character notificado) {
        this.notificado = notificado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoLote() {
        return idTipoLote;
    }

    public void setIdTipoLote(Integer idTipoLote) {
        this.idTipoLote = idTipoLote;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getTipoLote() {
        return tipoLote;
    }

    public void setTipoLote(String tipoLote) {
        this.tipoLote = tipoLote;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
    
    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Lote() {
        this.archivos = new ArrayList<>();
    }
}
