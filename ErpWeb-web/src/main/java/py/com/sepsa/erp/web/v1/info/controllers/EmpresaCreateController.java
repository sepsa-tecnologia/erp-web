/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmpresa;
import py.com.sepsa.erp.web.v1.info.remote.EmpresaService;



/**
 * Controlador para crear Empresa
 * 
 * @author Williams Vera
 */
@ViewScoped
@Named("empresaCreate")
public class EmpresaCreateController implements Serializable{
    /**
     * Cliente para servicio de Empresa
     */
    private final EmpresaService service;
    /**
     * POJO de Empresa
     */    
    private Empresa empresa;

    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {    
        this.empresa = empresa;
    } 
    //</editor-fold>
   
    /**
     * Método para crear la empresa.
     * Por el momento se setea por defecto tipoEmpresa = "CLIENTE" y activo ="S.
     */
    public void create() {
        TipoEmpresaAdapter adapterTipoEmpresa = new TipoEmpresaAdapter();
        TipoEmpresa tipoEmpresa = new TipoEmpresa();
        tipoEmpresa.setCodigo("CLIENTE");
        adapterTipoEmpresa = adapterTipoEmpresa.fillData(tipoEmpresa);
        
        this.empresa.setIdTipoEmpresa(adapterTipoEmpresa.getData().get(0).getId());
        this.empresa.setTipoEmpresa(adapterTipoEmpresa.getData().get(0));
        this.empresa.setActivo("S");
        
        BodyResponse<Empresa> respuestaEmpresa = service.setEmpresa(this.empresa);
        if (respuestaEmpresa.getSuccess()){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Empresa creada correctamente!"));
            init();
        }
    }
    
    public void init(){
        this.empresa = new Empresa();
        
    }
    public EmpresaCreateController() {
        init();
        this.service = new EmpresaService();
        
    }
    
    
}
