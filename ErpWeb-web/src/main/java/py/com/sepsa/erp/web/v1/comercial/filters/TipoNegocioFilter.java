
package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoNegocio;

/**
 * Filtro utilizado para el servicio de tipo negocio
 * 
 * @author Cristina Insfrán
 */
public class TipoNegocioFilter extends Filter{
  
    /**
     * Agrega el filtro de identificador del tipo negocio
     *
     * @param id Identificador del cliente
     * @return
     */
    public TipoNegocioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion descripción del tipo de negocio
     * @return
     */
    public TipoNegocioFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param tiponegocio datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoNegocio tiponegocio, Integer page, Integer pageSize) {
        TipoNegocioFilter filter = new TipoNegocioFilter();

        filter
                .id(tiponegocio.getId())
                .descripcion(tiponegocio.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TipoNegocioFilter
     */
    public TipoNegocioFilter() {
        super();
    }
}
