package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaMontoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionSncAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.SolicitudNotaCreditoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionSnc;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCreditoDetalles;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.facturacion.remote.SolicitudNotaCreditoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;

/**
 * Controlador para crer nota de crédito compra
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("notaCreditoCompraCrear")
public class NotaCreditoCompraCrearController implements Serializable {

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean create;
    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaCompraAdapter adapterFacturaCompra;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Crédito
     */
    private SolicitudNotaCreditoService serviceNotaCreditoSolicitud;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.C.
     */
    private List<SolicitudNotaCreditoDetalles> listaSolicitudDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Descripción local origen
     */
    private String localOrigen;
    /**
     * Descripción local destino
     */
    private String localDestino;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Dato de factura
     */
    private String idFactura;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Identificador de la solicitud
     */
    private String idSolicitud;
    /**
     * Dato de control
     */
    private boolean showDatatableSolicitud;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listFacturaSolicitud = new ArrayList<>();
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFacturaSolicitud = new ArrayList<>();
    /**
     * Adaptador de la solicitud
     */
    private SolicitudNotaCreditoListAdapter adapterSolicitud;
    /**
     * Objeto de la solicitud nota de credito
     */
    private SolicitudNotaCredito solicitudNC;
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * Adaptador de la lista de motivo solicitud snc
     */
    private MotivoEmisionSncAdapter adapterMotivoEmision;
    /**
     * Objeto del motivo de solicitud
     */
    private MotivoEmisionSnc motivoEmisionSnc;
    /**
     * Lista de tipo de telefonos
     */
    private List<SelectItem> listMotivoSnc;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the localOrigen
     */
    public String getLocalOrigen() {
        return localOrigen;
    }

    /**
     * @param localOrigen the localOrigen to set
     */
    public void setLocalOrigen(String localOrigen) {
        this.localOrigen = localOrigen;
    }

    /**
     * @return the localDestino
     */
    public String getLocalDestino() {
        return localDestino;
    }

    /**
     * @param localDestino the localDestino to set
     */
    public void setLocalDestino(String localDestino) {
        this.localDestino = localDestino;
    }

    /**
     * @return the listMotivoSnc
     */
    public List<SelectItem> getListMotivoSnc() {
        return listMotivoSnc;
    }

    /**
     * @param listMotivoSnc the listMotivoSnc to set
     */
    public void setListMotivoSnc(List<SelectItem> listMotivoSnc) {
        this.listMotivoSnc = listMotivoSnc;
    }

    /**
     * @return the motivoEmisionSnc
     */
    public MotivoEmisionSnc getMotivoEmisionSnc() {
        return motivoEmisionSnc;
    }

    /**
     * @param motivoEmisionSnc the motivoEmisionSnc to set
     */
    public void setMotivoEmisionSnc(MotivoEmisionSnc motivoEmisionSnc) {
        this.motivoEmisionSnc = motivoEmisionSnc;
    }

    /**
     * @return the adapterMotivoEmision
     */
    public MotivoEmisionSncAdapter getAdapterMotivoEmision() {
        return adapterMotivoEmision;
    }

    /**
     * @param adapterMotivoEmision the adapterMotivoEmision to set
     */
    public void setAdapterMotivoEmision(MotivoEmisionSncAdapter adapterMotivoEmision) {
        this.adapterMotivoEmision = adapterMotivoEmision;
    }

    /**
     * @return the listaSolicitudDetalle
     */
    public List<SolicitudNotaCreditoDetalles> getListaSolicitudDetalle() {
        return listaSolicitudDetalle;
    }

    /**
     * @param listaSolicitudDetalle the listaSolicitudDetalle to set
     */
    public void setListaSolicitudDetalle(List<SolicitudNotaCreditoDetalles> listaSolicitudDetalle) {
        this.listaSolicitudDetalle = listaSolicitudDetalle;
    }

    /**
     * @return the serviceNotaCredito
     */
    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    /**
     * @param serviceNotaCredito the serviceNotaCredito to set
     */
    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

    /**
     * @return the adapterFacturaCompra
     */
    public FacturaCompraAdapter getAdapterFacturaCompra() {
        return adapterFacturaCompra;
    }

    /**
     * @param adapterFacturaCompra the adapterFacturaCompra to set
     */
    public void setAdapterFacturaCompra(FacturaCompraAdapter adapterFacturaCompra) {
        this.adapterFacturaCompra = adapterFacturaCompra;
    }

    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setSolicitudNC(SolicitudNotaCredito solicitudNC) {
        this.solicitudNC = solicitudNC;
    }

    public void setAdapterSolicitud(SolicitudNotaCreditoListAdapter adapterSolicitud) {
        this.adapterSolicitud = adapterSolicitud;
    }

    public SolicitudNotaCredito getSolicitudNC() {
        return solicitudNC;
    }

    public SolicitudNotaCreditoListAdapter getAdapterSolicitud() {
        return adapterSolicitud;
    }

    public void setListSelectedFacturaSolicitud(List<Factura> listSelectedFacturaSolicitud) {
        this.listSelectedFacturaSolicitud = listSelectedFacturaSolicitud;
    }

    public void setListFacturaSolicitud(List<Factura> listFacturaSolicitud) {
        this.listFacturaSolicitud = listFacturaSolicitud;
    }

    public List<Factura> getListSelectedFacturaSolicitud() {
        return listSelectedFacturaSolicitud;
    }

    public List<Factura> getListFacturaSolicitud() {
        return listFacturaSolicitud;
    }

    public void setShowDatatableSolicitud(boolean showDatatableSolicitud) {
        this.showDatatableSolicitud = showDatatableSolicitud;
    }

    public boolean isShowDatatableSolicitud() {
        return showDatatableSolicitud;
    }

    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getIdSolicitud() {
        return idSolicitud;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListSelectedFactura(List<Factura> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public List<Factura> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

    /**
     * @return the serviceNotaCreditoSolicitud
     */
    public SolicitudNotaCreditoService getServiceNotaCreditoSolicitud() {
        return serviceNotaCreditoSolicitud;
    }

    /**
     * @param serviceNotaCreditoSolicitud the serviceNotaCreditoSolicitud to set
     */
    public void setServiceNotaCreditoSolicitud(SolicitudNotaCreditoService serviceNotaCreditoSolicitud) {
        this.serviceNotaCreditoSolicitud = serviceNotaCreditoSolicitud;
    }

//</editor-fold>
    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontos(Integer nroLinea) {
        SolicitudNotaCreditoDetalles info = null;
        for (SolicitudNotaCreditoDetalles info0 : listaSolicitudDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotales();

    }

    /**
     * Metodo para eliminar un detalle de la lista de detalles.
     *
     * @param sncd
     */
    public void deleteDetalle(SolicitudNotaCreditoDetalles sncd) {
        this.listaSolicitudDetalle.remove(sncd);
        calcularTotales();
    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");

        for (int i = 0; i < listaSolicitudDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaSolicitudDetalle.get(i).getMontoTotal());
        }

        solicitudNC.setMontoTotalSolicitudNotaCredito(acumTotales);
        solicitudNC.setMontoTotalSolicitudNotaCredito(acumTotales);

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaSolicitudDetalle.size(); i++) {
            if (listaSolicitudDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaSolicitudDetalle.get(i).getMontoImponible());
            } else if (listaSolicitudDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaSolicitudDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaSolicitudDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaSolicitudDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaSolicitudDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaSolicitudDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaSolicitudDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaSolicitudDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaSolicitudDetalle.get(i).getMontoIva());
        }

        solicitudNC.setMontoIva5(montoIva5Acumulador);
        solicitudNC.setMontoImponible5(montoImponible5Acumulador);
        solicitudNC.setMontoTotal5(montoTotal5Acumulador);
        solicitudNC.setMontoIva10(montoIva10Acumulador);
        solicitudNC.setMontoImponible10(montoImponible10Acumulador);
        solicitudNC.setMontoTotal10(montoTotal10Acumulador);
        solicitudNC.setMontoTotalExento(montoExcentoAcumulador);
        solicitudNC.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        solicitudNC.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(solicitudNC.getIdMoneda());
        montoLetrasFilter.setCodigoMoneda("PYG");
        montoLetrasFilter.setMonto(solicitudNC.getMontoTotalSolicitudNotaCredito());

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /**
     * Método para crear Nota de Crédito
     */
    public void createNotaCredito() {
        calcularTotalesGenerales();

        solicitudNC.setAnulado("N");
        solicitudNC.setArchivoEdi("S");
        solicitudNC.setGeneradoEdi("N");
        solicitudNC.setRecibido("N");

        solicitudNC.setSolicitudNotaCreditoDetalles(listaSolicitudDetalle);

        BodyResponse<SolicitudNotaCredito> respuestaDeNC = serviceNotaCreditoSolicitud.createNotaCredito(solicitudNC);

        if (respuestaDeNC.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "NC creada correctamente!"));
        }
    }

    /**
     * Método para filtrar la factura
     *
     * @param idCliente
     */
    public void filterFactura(Integer idCliente) {
        facturaFilter.setIdCliente(idCliente);
        facturaFilter.setCobrado("N");
        facturaFilter.setAnulado("N");

        adapterFacturaCompra = adapterFacturaCompra.fillData(facturaFilter);
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        direccion = "";
        solicitudNC = new SolicitudNotaCredito();
        listSelectedFactura = new ArrayList<>();
        listaSolicitudDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        //datoNotaCredito = new DatosNotaCredito();
    }

    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }

    /**
     * Método para obtener la factura en NC, cuando se redirecciona desde el
     * listado de facturas
     */
    public void obtenerFactura() {
        facturaFilter.setId(Integer.parseInt(idFactura));
        adapterFacturaCompra = adapterFacturaCompra.fillData(facturaFilter);
        listSelectedFactura.add(adapterFacturaCompra.getData().get(0));
        cliente.setRazonSocial(adapterFacturaCompra.getData().get(0).getRazonSocial());

        obtenerDatosNCFactura();
        detalleNC();

    }

    public void obtenerDatosNCFactura() {

        Calendar today = Calendar.getInstance();

        solicitudNC.setRazonSocial(cliente.getRazonSocial());
        ruc = adapterFacturaCompra.getData().get(0).getRuc();
        solicitudNC.setRuc(ruc);
        solicitudNC.setIdCliente(adapterFacturaCompra.getData().get(0).getIdPersona());
        solicitudNC.setFechaInsercion(today.getTime());
        solicitudNC.setIdMoneda(1);
        solicitudNC.setIdLocalDestino(adapterFacturaCompra.getData().get(0).getIdLocalOrigen());
        solicitudNC.setIdLocalOrigen(adapterFacturaCompra.getData().get(0).getIdLocalDestino());
        if (adapterFacturaCompra.getData().get(0).getLocalOrigen() != null) {
            localDestino = adapterFacturaCompra.getData().get(0).getLocalOrigen().getDescripcion();
        }

        if (adapterFacturaCompra.getData().get(0).getLocalDestino() != null) {
            localOrigen = adapterFacturaCompra.getData().get(0).getLocalDestino().getDescripcion();

        }

    }

    public void detalleNC() {
        if (show == false) {
            show = true;
        }
        SolicitudNotaCreditoDetalles notaCreditoDetalle = new SolicitudNotaCreditoDetalles();

        for (int i = 0; i < adapterFacturaCompra.getData().size(); i++) {

            for (int j = 0; j < adapterFacturaCompra.getData().get(i).getFacturaCompraDetalles().size(); j++) {
                notaCreditoDetalle.setNroLinea(j);
                notaCreditoDetalle.setFacturaCompra(adapterFacturaCompra.getData().get(i));
                notaCreditoDetalle.setIdFacturaCompra(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getIdFacturaCompra());
                notaCreditoDetalle.setDescripcion(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getDescripcion());
                notaCreditoDetalle.setPorcentajeIva(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getPorcentajeIva());
                notaCreditoDetalle.setMontoImponible(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getMontoImponible());
                notaCreditoDetalle.setCantidad(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getCantidadFacturada());
                notaCreditoDetalle.setPrecioUnitarioConIva(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getPrecioUnitarioConIva());
                notaCreditoDetalle.setPrecioUnitarioSinIva(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getPrecioUnitarioSinIva());
                notaCreditoDetalle.setMontoIva(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getMontoIva());
                notaCreditoDetalle.setMontoTotal(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getMontoTotal());
                notaCreditoDetalle.setIdProducto(adapterFacturaCompra.getData().get(i).
                        getFacturaCompraDetalles().get(j).getProducto().getId());

                listaSolicitudDetalle.add(notaCreditoDetalle);
            }
        }

        calcularTotales();
    }

    /*public void obtenerFacturasDeSolicitud() {
        solicitudNC.setId(Integer.parseInt(idSolicitud));
        adapterSolicitud = adapterSolicitud.fillData(solicitudNC);

        cliente.setRazonSocial(adapterSolicitud.getData().get(0).getRazonSocial());

        solicitudNC.setRazonSocial(cliente.getRazonSocial());
        ruc = adapterSolicitud.getData().get(0).getRuc();
        solicitudNC.setRuc(ruc);

        facturaFilter.setId(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(0).getIdFactura());
        adapterFacturaCompra = adapterFacturaCompra.fillData(facturaFilter);
        listSelectedFactura.add(adapterFacturaCompra.getData().get(0));
       obtenerDatosNC();
        solicitudDetalleNC();
    }*/
 /*public void solicitudDetalleNC() {
        if (show == false) {
            show = true;
        }

        for (int i = 0; i < adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().size(); i++) {
            SolicitudNotaCreditoDetalles notaCreditoDetalle = new SolicitudNotaCreditoDetalles();

            int nroBonificacion;
            if (listaSolicitudDetalle.isEmpty()) {
                notaCreditoDetalle.setNroLinea(linea);
                nroBonificacion = linea;
            } else {
                linea++;
                nroBonificacion = linea;
                notaCreditoDetalle.setNroLinea(linea);
            }

            notaCreditoDetalle.setIdFactura(listSelectedFactura.get(0).getId());
            String nroFactura = listSelectedFactura.get(0).getNroFactura();
            notaCreditoDetalle.setDescripcion("BONIFICACION " + nroBonificacion + " DE LA FACTURA NRO." + nroFactura);
            notaCreditoDetalle.setPorcentajeIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPorcentajeIva());
            notaCreditoDetalle.setMontoImponible(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoImponible());
            notaCreditoDetalle.setCantidad(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getCantidad());
            notaCreditoDetalle.setPrecioUnitarioConIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPrecioUnitarioConIva());
            notaCreditoDetalle.setPrecioUnitarioSinIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getPrecioUnitarioSinIva());
            notaCreditoDetalle.setMontoIva(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoIva());
            notaCreditoDetalle.setMontoTotal(adapterSolicitud.getData().get(0).getSolicitudNotaCreditoDetalles().get(i).getMontoTotal());

            listaSolicitudDetalle.add(notaCreditoDetalle);
        }

        calcularTotales();
    }*/
    public Factura filtrarFactura(Integer idFactura) {

        FacturaMontoAdapter adapterFactura = new FacturaMontoAdapter();
        Factura facturaFilter = new Factura();
        facturaFilter.setId(idFactura);

        adapterFactura = adapterFactura.fillData(facturaFilter);

        return adapterFactura.getData().get(0);
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void obtenerMotivoEmisionSnc() {

        adapterMotivoEmision = adapterMotivoEmision.fillData(motivoEmisionSnc);

        for (int i = 0; i < adapterMotivoEmision.getData().size(); i++) {

            listMotivoSnc.add(new SelectItem(adapterMotivoEmision.getData().get(i).getId(),
                    adapterMotivoEmision.getData().get(i).getDescripcion()));
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        create = false;
        show = false;
        this.digital = "N";
        showDatatableSolicitud = false;
        this.adapterFacturaCompra = new FacturaCompraAdapter();
        this.facturaFilter = new Factura();
        this.serviceNotaCreditoSolicitud = new SolicitudNotaCreditoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        // this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.adapterSolicitud = new SolicitudNotaCreditoListAdapter();
        this.solicitudNC = new SolicitudNotaCredito();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        this.serviceNotaCredito = new NotaCreditoService();
        this.adapterMotivoEmision = new MotivoEmisionSncAdapter();
        this.motivoEmisionSnc = new MotivoEmisionSnc();
        this.listMotivoSnc = new ArrayList<>();

        filterMoneda();
        obtenerMotivoEmisionSnc();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        if (params.get("id") != null) {
            idFactura = params.get("id");
            showDatatable = true;
            obtenerFactura();
        }

    }

}
