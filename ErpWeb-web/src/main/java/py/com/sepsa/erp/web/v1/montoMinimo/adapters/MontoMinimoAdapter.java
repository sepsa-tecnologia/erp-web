
package py.com.sepsa.erp.web.v1.montoMinimo.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.montoMinimo.pojos.MontoMinimo;
import py.com.sepsa.erp.web.v1.montoMinimo.remote.MontoMinimoService;

/**
 * Adaptador para la lista de monto mínimo
 * @author Romina Núñez
 */
public class MontoMinimoAdapter extends DataListAdapter<MontoMinimo>{
    
    /**
     * Cliente para el servicio monto mínimo
     */
    private final MontoMinimoService montoMinimoService;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MontoMinimoAdapter fillData(MontoMinimo searchData) {
     
        return montoMinimoService.getMontoMinimoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de MontoMinimoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MontoMinimoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.montoMinimoService = new MontoMinimoService();
    }

    /**
     * Constructor de MontoMinimoAdapter
     */
    public MontoMinimoAdapter() {
        super();
        this.montoMinimoService= new MontoMinimoService();
    }
}
