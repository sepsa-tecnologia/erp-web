
package py.com.sepsa.erp.web.v1.producto.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;

/**
 * Adaptador de la lista de producto precio
 * @author Cristina Insfrán
 */
public class ProductoPrecioListAdapter extends DataListAdapter<ProductoPrecio>{
    /**
     * Cliente para el servicio de producto
     */
    private final ProductoService productoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoPrecioListAdapter fillData(ProductoPrecio searchData) {

        return productoClient.getProductoPrecioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ProductoPrecioListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoPrecioListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoClient = new ProductoService();
    }

    /**
     * Constructor de ProductoPrecioListAdapter
     */
    public ProductoPrecioListAdapter() {
        super();
        this.productoClient = new ProductoService();
    }
}
