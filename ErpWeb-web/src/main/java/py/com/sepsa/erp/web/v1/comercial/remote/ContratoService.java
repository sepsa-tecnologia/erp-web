package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ContratoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Contrato
 *
 * @author Romina Núñez
 */
public class ContratoService extends APIErpComercial {

    /**
     * Obtiene la lista de descuentos
     *
     * @param descuento 
     * @param page
     * @param pageSize
     * @return
     */
    public ContratoAdapter getContratoList(Contrato contrato, Integer page,
            Integer pageSize) {

        ContratoAdapter lista = new ContratoAdapter();

        Map params = ContratoFilter.build(contrato, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTRATO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContratoAdapter.class);

            lista = (ContratoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Método para crear Contrato
     *
     * @param contrato 
     * @return
     */
    public Integer createContrato(Contrato contrato) {

        Integer id = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CONTRATO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(contrato));

            response = BodyResponse.createInstance(conn, Contrato.class);

            id = ((Contrato) response.getPayload()).getId();
           
        }
        return id;
    }
    
    /**
     * Método para aprobar contrato
     *
     * @param contrato  
     * @return
     */
    public Contrato aprobarContrato(Contrato contrato) {

        Contrato cont = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.APROBARCONTRATO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(contrato));

            response = BodyResponse.createInstance(conn, Contrato.class);

            cont = ((Contrato) response.getPayload());
           
        }
        return cont;
    }
    
    /**
     * Método para inactivar contrato
     *
     * @param contrato  
     * @return
     */
    public Contrato inactivarContrato(Contrato contrato) {

        Contrato cont = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.INACTIVAR_CONTRATO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(contrato));

            response = BodyResponse.createInstance(conn, Contrato.class);

            cont = ((Contrato) response.getPayload());
           
        }
        return cont;
    }

    
    /**
     * Constructor de ContratoService
     */
    public ContratoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        CONTRATO("Contrato", "contrato"),
        APROBARCONTRATO("AprobarContrato", "contrato/aprobar"),
        INACTIVAR_CONTRATO("Inactivar Contrato", "contrato/inactivar");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
