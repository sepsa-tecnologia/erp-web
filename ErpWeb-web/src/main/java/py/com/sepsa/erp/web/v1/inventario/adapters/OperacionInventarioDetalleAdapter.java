package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.remote.OperacionInventarioDetalleService;

/**
 * Adapter para detalle de operacion de inventario
 *
 * @author Alexander Triana
 */
public class OperacionInventarioDetalleAdapter extends
        DataListAdapter<OperacionInventarioDetalle> {

    /**
     * Cliente remoto para detalles de operacion inventario
     */
    private final OperacionInventarioDetalleService operacionDetalleService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return OperacionInventarioDetalleAdapter
     */
    @Override
    public OperacionInventarioDetalleAdapter fillData(OperacionInventarioDetalle searchData) {

        return operacionDetalleService.getOperacionDetalle(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de OperacionInventarioDetalleAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public OperacionInventarioDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.operacionDetalleService = new OperacionInventarioDetalleService();
    }

    /**
     * Constructor de OperacionInventarioDetalleAdapter
     */
    public OperacionInventarioDetalleAdapter() {
        super();
        this.operacionDetalleService = new OperacionInventarioDetalleService();
    }
}
