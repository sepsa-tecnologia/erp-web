/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.inventario.remote.OperacionService;

/**
 * Adapter para operacion de inventario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OperacionAdapter extends DataListAdapter<Operacion> {

    /**
     * Cliente remoto para operacion de inventario
     */
    private final OperacionService operacionClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return OperacionAdapter
     */
    @Override
    public OperacionAdapter fillData(Operacion searchData) {

        return operacionClient.getOperacion(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de OperacionAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public OperacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.operacionClient = new OperacionService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public OperacionAdapter() {
        super();
        this.operacionClient = new OperacionService();
    }
}
