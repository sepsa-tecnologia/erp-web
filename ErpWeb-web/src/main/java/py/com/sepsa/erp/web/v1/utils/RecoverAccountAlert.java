
package py.com.sepsa.erp.web.v1.utils;

/**
 * Alertas enviada al generar email de recuperación de contraseña
 * @author Néstor E. Reinoso Wood
 */
public class RecoverAccountAlert {
      /**
     * Local
     */
    public static final String URL = "http://localhost:8081";
    /**
     * Pruebas
     */
    //public static final String URL = "http://181.40.81.34:8181";
    /**
     * Producción
     */
    //private static final String URL = "https://mercurio.sepsa.com.py";
    
    /**
     * Envia un recibo de pago a un cliente
     * @param email
     * @param hash Hash del recibo
     */
    public static void receiptMail(String email, String hash ) {
        String msn= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"    <head>\n" +
"        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"        <title>A Simple Responsive HTML Email</title>\n" +
"        <style type=\"text/css\">\n" +
"            body {margin-top: 5px; padding: 0; min-width: 100%!important; font-family: -apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,\"Noto Sans\",sans-serif,\"Apple Color Emoji\",\"Segoe UI Emoji\",\"Segoe UI Symbol\",\"Noto Color Emoji\";}\n" +
"            .content {width: 100%; max-width: 600px;}  \n" +
"            @media only screen and (min-device-width: 601px) {\n" +
"                .content {width: 600px !important;}\n" +
"            }\n" +
"            .header {padding: 0px 0px;}\n" +
"            .text-white {\n" +
"                color: #FFFFFF;\n" +
"                font-size: 1.75rem;\n" +
"                font-weight: 500;\n" +
"            }\n" +
"            .text-center {\n" +
"                text-align: center;\n" +
"            }\n" +
"            .text-left {\n" +
"                text-align: left;\n" +
"            }\n" +
"            .h5, .h4, .h3, .h2, .h1 {\n" +
"                margin-bottom: .5rem;\n" +
"                line-height: 1.2;\n" +
"            }\n" +
"            .rounded {\n" +
"                border-radius: .25rem !important;\n" +
"            }\n" +
"            .table {\n" +
"                width: 100%;\n" +
"                margin-bottom: 1rem;\n" +
"                color: #212529;\n" +
"                border-collapse: collapse;\n" +
"            }\n" +
"            *, ::after, ::before {\n" +
"                box-sizing: border-box;\n" +
"            }\n" +
"            .table-striped tbody tr:nth-of-type(odd) {\n" +
"                background-color: rgba(0, 0, 0, 0.05);\n" +
"            }\n" +
"            .table-dark.table-striped tbody tr:nth-of-type(odd) {\n" +
"                background-color: rgba(255, 255, 255, 0.05);\n" +
"            }\n" +
"            .text-danger {\n" +
"                color: #dc3545 ;\n" +
"            }\n" +
"            .table thead th {\n" +
"                vertical-align: bottom;\n" +
"                border-bottom: 2px solid #dee2e6;\n" +
"            }\n" +
"            .table td, .table th {\n" +
"                border-top: 1px solid #dee2e6;\n" +
"            }\n" +
"            .table-sm td, .table-sm th {\n" +
"                padding: .3rem;\n" +
"            }\n" +
"            .footer-row {\n" +
"                border-top: 2px solid #dee2e6;\n" +
"                text-align: center;\n" +
"                padding: 10px 10px 10px 10px;\n" +
"            }\n" +
"            a {\n" +
"                color: #007bff;\n" +
"                text-decoration: none;\n" +
"                background-color: transparent;\n" +
"                \n" +
"            }\n" +
"            a:hover {\n" +
"                text-decoration: underline;\n" +
"            }\n" +
"        </style>\n" +
"    </head>\n" +
"    <body yahoo bgcolor=\"#f6f8f1\">\n" +
"        <!--[if (gte mso 9)|(IE)]>\n" +
"        <table width=\"100%\" bgcolor=\"#f6f8f1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
"            <tr>\n" +
"                <td>\n" +
"                    <![endif]-->\n" +
"                    <table class=\"content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
"                        <tr>\n" +
"                            <td class=\"header rounded\" bgcolor=\"#fab400\">\n" +
"                                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
"                                    <tr>\n" +
"                                        <td height=\"90\" style=\"padding: 0 0 0 0;\">\n" +
"                                            <h3 class=\"text-white\">SiediWeb | SEPSA</h3>\n" +
"                                        </td>\n" +
"                                    </tr>\n" +
"                                </table>\n" +
"                            </td>\n" +
"                        </tr>\n" +
"                        <tr>\n" +
"                            <td>\n" +
"                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
"                                    <tr>\n" +
"                                        <td height=\"30\" style=\"padding: 10px 10px 10px 10px;\">\n" +
"                                            <h5 class=\"h5\" style=\"font-size:15px;\">Estimado cliente, se ha generado la solicitud de restauración de contraseña:</h5>\n" +
"                                        </td>\n" +
"                                    </tr>\n" +
"                                </table>\n" +
"                            </td>\n" +
"                        </tr>\n" +
"                        <tr>\n" +
"                            <td style=\"padding: 10px 10px 10px 10px;\">\n" +
"                                <p>Para reestablecer su contraseña, haga click <a href=\""+URL+"/SiediWeb-war/recovery.xhtml?h="+hash+"\">aquí</a>.</p>\n" +
"                                \n" +
"                                <p>Equipo SEPSA.</p>\n" +
"                            </td>\n" +
"                        </tr>\n" +
"                        <tr>\n" +
"                            <td class=\"footer-row\">\n" +
"                                <small><a href=\"www.sepsa.com.py\">&copy; 2020 SEPSA</a></small>\n" +
"                            </td>\n" +
"                        </tr>\n" +
"                    </table>\n" +
"                <!--[if (gte mso 9)|(IE)]>\n" +
"                </td>\n" +
"            </tr>\n" +
"        </table>\n" +
"        <![endif]-->\n" +
"    </body>\n" +
"</html>";
        String subject = "Restauración de Contraseña SiediWeb - SEPSA";
        MailUtils.sendMail(msn, subject, email, "text/html; charset=utf-8");
    }
}
