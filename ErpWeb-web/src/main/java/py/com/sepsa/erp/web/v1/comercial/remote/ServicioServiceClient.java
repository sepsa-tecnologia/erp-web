/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServiceListAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ServiceFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicio;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de Servicio
 * @author Cristina Insfrán
 */
public class ServicioServiceClient extends APIErpCore{
    
    
     /**
     * Obtiene la lista de servicios
     *
     * @param servicio Objeto servicio
     * @param page
     * @param pageSize
     * @return
     */
    public ServiceListAdapter getClientList(Servicio servicio, Integer page,
            Integer pageSize) {
        
        ServiceListAdapter lista = new ServiceListAdapter();

        Map params = ServiceFilter.build(servicio, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.LISTAR_SERVICES.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ServiceListAdapter.class);

            lista = (ServiceListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    /**
     * Constructor de la clase ServicioServiceCliente
     */
    public ServicioServiceClient(){
        super();
    }
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LISTAR_SERVICES("Listar servicios", "servicio/***?");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
