/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoEmpresaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmpresa;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para tipo empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEmpresaService extends APIErpCore {

    /**
     * Obtiene la lista de tipo de Empresa.
     *
     * @param tipoEmpresa
     * @param page
     * @param pageSize
     * @return
     */
    public TipoEmpresaAdapter getTipoEmpresaList(TipoEmpresa tipoEmpresa, Integer page,
            Integer pageSize) {

        TipoEmpresaAdapter lista = new TipoEmpresaAdapter();

        Map params = TipoEmpresaFilter.build(tipoEmpresa, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_EMPRESA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoEmpresaAdapter.class);

            lista = (TipoEmpresaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor
     */
    public TipoEmpresaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        TIPO_EMPRESA("Listado de tipos de empresas", "tipo-empresa");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
