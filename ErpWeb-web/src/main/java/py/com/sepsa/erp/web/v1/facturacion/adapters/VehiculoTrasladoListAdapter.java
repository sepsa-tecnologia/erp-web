package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;


/**
 * Adaptador para listado de menú
 *
 * @author Williams Vera
 */
public class VehiculoTrasladoListAdapter extends DataListAdapter<VehiculoTraslado> {

    /**
     * Cliente para el servicio de menu
     */
    private final VehiculoTrasladoService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData datos buscados
     * @return VehiculoTrasladoListAdapter
     */
    @Override
    public VehiculoTrasladoListAdapter fillData(VehiculoTraslado searchData) {
        return service.getVehiculoTrasladoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de VehiculoTrasladoListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public VehiculoTrasladoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new VehiculoTrasladoService();
    }

    /**
     * Constructor de VehiculoTrasladoListAdapter
     */
    public VehiculoTrasladoListAdapter() {
        super();
        this.service = new VehiculoTrasladoService();
    }

}
