/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.notificacion.adapters.ConfigurationAdapter;
import py.com.sepsa.erp.web.v1.notificacion.filters.ConfigurationFilter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Configuration;
import py.com.sepsa.erp.web.v1.remote.APIErpNotificacion;

/**
 * Cliente para el servicio Configuration
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ConfigurationService extends APIErpNotificacion {

    /**
     * Obtiene la lista de configuration.
     *
     * @param configuration
     * @param page
     * @param pageSize
     * @return
     */
    public ConfigurationAdapter getConfigurationList(Configuration configuration, Integer page,
            Integer pageSize) {

        ConfigurationAdapter lista = new ConfigurationAdapter();

        Map params = ConfigurationFilter.build(configuration, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONFIG.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConfigurationAdapter.class);

            lista = (ConfigurationAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear la dirección
     *
     * @param direccion
     * @return
     */
    public Integer createConfiguration(Configuration config) {

        Integer id = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CONFIG.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(config));

            response = BodyResponse.createInstance(conn, Configuration.class);

            id = ((Configuration) response.getPayload()).getId();

        }
        return id;
    }

    /**
     * Método para editar cliente tipo negocio
     *
     * @param tipoNegocio
     * @return
     */
    public Configuration editConfig(Configuration config) {

        Configuration tn = new Configuration();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.CONFIG.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(config));

            response = BodyResponse.createInstance(conn, Configuration.class);

            tn = ((Configuration) response.getPayload());

        }
        return tn;
    }

    /**
     * Constructor de ConfigurationService
     */
    public ConfigurationService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        CONFIG("Listado de Configuracion ", "v1/configuration");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
