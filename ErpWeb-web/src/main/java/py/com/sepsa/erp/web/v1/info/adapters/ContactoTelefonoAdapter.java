
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.ContactoTelefonoService;

/**
 * Adaptador de la lista de contacto email
 * @author Cristina Insfrán
 */
public class ContactoTelefonoAdapter extends DataListAdapter<ContactoTelefono> {
    
    /**
     * Cliente para el servicio de contacto email
     */
    private final ContactoTelefonoService contactoTelefonoService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoTelefonoAdapter fillData(ContactoTelefono searchData) {
     
        return contactoTelefonoService.getContactoEmailList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ContactoTelefonoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoTelefonoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoTelefonoService = new ContactoTelefonoService();
    }

    /**
     * Constructor de ContactoTelefonoAdapter
     */
    public ContactoTelefonoAdapter() {
        super();
        this.contactoTelefonoService = new ContactoTelefonoService();
    }
}
