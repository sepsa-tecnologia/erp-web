
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.TraficoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.TraficoDetalleService;

/**
 * Adaptador de la lista de Trafico Detalle
 * @author Romina Núñez
 */
public class TraficoDetalleAdapter extends DataListAdapter<TraficoDetalle> {
    
    /**
     * Cliente para el servicio de HistoricoLiquidacion
     */
    private final TraficoDetalleService serviceTraficoDetalle;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TraficoDetalleAdapter fillData(TraficoDetalle searchData) {
     
        return serviceTraficoDetalle.getTraficoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de TraficoDetalleAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TraficoDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTraficoDetalle = new TraficoDetalleService();
    }

    /**
     * Constructor de TraficoDetalleAdapter
     */
    public TraficoDetalleAdapter() {
        super();
        this.serviceTraficoDetalle = new TraficoDetalleService();
    }
}
