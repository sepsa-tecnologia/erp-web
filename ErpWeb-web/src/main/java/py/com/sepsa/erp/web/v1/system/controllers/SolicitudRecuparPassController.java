package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.API;
import py.com.sepsa.erp.web.v1.remote.InfoPojo;
import py.com.sepsa.erp.web.v1.system.pojos.Credential;
import py.com.sepsa.erp.web.v1.system.pojos.Token;
import py.com.sepsa.erp.web.v1.system.remote.LoginServiceClient;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;
import py.com.sepsa.erp.web.v1.utils.RecoverAccountAlert;

/**
 * Controlador para la solicitud de recuperación de contraseña
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("solicitudPass")
public class SolicitudRecuparPassController implements Serializable {

    /**
     * Objeto Usuario
     */
    private Usuario usuario;
    /**
     * Servicios para login al sistema
     */
    private LoginServiceClient serviceClient;
    /**
     * Adaptador de usuario
     */
    private UserServiceClient userServiceClient;
    /**
     *
     */
    BodyResponse response;
    /**
     * Clase
     */
    private RecoverAccountAlert recoverAlert;
    protected API api;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the serviceClient
     */
    public LoginServiceClient getServiceClient() {
        return serviceClient;
    }

    /**
     * @param serviceClient the serviceClient to set
     */
    public void setServiceClient(LoginServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    /**
     * @return the userServiceClient
     */
    public UserServiceClient getUserServiceClient() {
        return userServiceClient;
    }

    /**
     * @param userServiceClient the userServiceClient to set
     */
    public void setUserServiceClient(UserServiceClient userServiceClient) {
        this.userServiceClient = userServiceClient;
    }

    /**
     * @return the recoverAlert
     */
    public RecoverAccountAlert getRecoverAlert() {
        return recoverAlert;
    }

    /**
     * @param recoverAlert the recoverAlert to set
     */
    public void setRecoverAlert(RecoverAccountAlert recoverAlert) {
        this.recoverAlert = recoverAlert;
    }
//</editor-fold>

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String paginaSolicitudPass() {

        String respuestas = null;
        if (!usuario.getUsuario().isEmpty()) {

            String urlInfo = InfoPojo.createInstanceUrl("api-url");

            usuario.setUrlDestino(urlInfo + "/web/recuperar_pass.xhtml");

            BodyResponse respuesta = userServiceClient.correoPass(usuario);

            if (respuesta.getSuccess().equals(false)) {

                if (!respuesta.getStatus().getMensajes().isEmpty()) {
                    for (int i = 0; i < respuesta.getStatus().getMensajes().size(); i++) {
                        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Advertencia", respuesta.getStatus().getMensajes().get(i).getDescripcion());
                        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    }
                }
            } else {
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Info", "Enviado exitosamente a su correo!");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }

        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Advertencia", "Debe completar el usuario");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        }
        return respuestas;

    }

    /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {

        this.usuario = new Usuario();
        this.serviceClient = new LoginServiceClient();

        InfoPojo info = InfoPojo.createInstanceUser("login-sistemas");

        Credential credential = new Credential();
        credential.setUsuario(info.getUser());
        credential.setContrasena(info.getPass());

        response = serviceClient.login(credential);

        if (response.getSuccess()) {
            this.userServiceClient = new UserServiceClient() {
                @Override
                protected void setBearer() {
                    super.setJwt(((Token) response.getPayload()).getToken() == null ? null : String.format("Bearer %s",
                            ((Token) response.getPayload()).getToken()));
                }
            };
        }

        // this.recoverAlert = new RecoverAccountAlert();
    }
}
