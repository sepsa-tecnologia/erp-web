/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista listar estado
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("estadoList")
public class EstadoListController implements Serializable {

    /**
     * Adaptador de estado.
     */
    private EstadoAdapter adapter;
    /**
     * Objeto estado.
     */
    private Estado estadoFilter;
    /**
     * Adaptador de la lista de estados
     */
    private TipoEstadoAdapter tipoEstadoAdapter;
    /**
     * Datos del cliente
     */
    private TipoEstado tipoEstado;
    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">

    public EstadoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(EstadoAdapter adapter) {
        this.adapter = adapter;
    }

    public Estado getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(Estado estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

    public TipoEstadoAdapter getTipoEstadoAdapter() {
        return tipoEstadoAdapter;
    }

    public void setTipoEstadoAdapter(TipoEstadoAdapter tipoEstadoAdapter) {
        this.tipoEstadoAdapter = tipoEstadoAdapter;
    }

    public TipoEstado getTipoEstado() {
        return tipoEstado;
    }

    public void setTipoEstado(TipoEstado tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

    //</editor-fold>
    /**
     * Método para filtrar estados.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(estadoFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.estadoFilter = new Estado();
        tipoEstado = new TipoEstado();
        buscar();
    }

    /**
     * Metodo para redirigir a la vista Editar Talonario
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("estado-edit?faces-redirect=true&id=%d", id);
    }

    /* Método para obtener tipo estado filtrados
     *
     * @param query
     * @return
     */
    public List<TipoEstado> completeQuery(String query) {

        TipoEstado tipoEstado = new TipoEstado();
        tipoEstado.setDescripcion(query.toLowerCase());

        tipoEstadoAdapter = tipoEstadoAdapter.fillData(tipoEstado);

        return tipoEstadoAdapter.getData();
    }

    /**
     * Selecciona el tipo estado
     *
     * @param event
     */
    public void onItemSelectTipoEstadoFilter(SelectEvent event) {
        tipoEstado.setId(((TipoEstado) event.getObject()).getId());
        this.estadoFilter.setIdTipoEstado(tipoEstado.getId());
    }

    @PostConstruct
    public void init() {
        try {
            this.adapter = new EstadoAdapter();
            this.estadoFilter = new Estado();
            this.tipoEstadoAdapter = new TipoEstadoAdapter();
            this.tipoEstado = new TipoEstado();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
