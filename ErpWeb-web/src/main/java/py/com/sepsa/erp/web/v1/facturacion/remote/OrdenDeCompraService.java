/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraDetalleAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.OrdenDeCompraFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetallesPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para Orden de compra
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class OrdenDeCompraService extends APIErpFacturacion {

    /**
     * Obtiene la lista de ordenes de compra
     *
     * @param ordenDeCompra Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public OrdenDeCompraAdapter getOrdenDeCompraList(OrdenDeCompra ordenDeCompra, Integer page,
            Integer pageSize) {

        OrdenDeCompraAdapter lista = new OrdenDeCompraAdapter();

        Map params = OrdenDeCompraFilter.build(ordenDeCompra, page, pageSize);

        HttpURLConnection conn = GET(Resource.ORDEN_DE_COMPRA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    OrdenDeCompraAdapter.class);

            lista = (OrdenDeCompraAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de ordenes de compra
     *
     * @param ordenDeCompra Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public OrdenDeCompraDetalleAdapter getOrdenDeCompraDetalleList(OrdenCompraDetallesPojo ordenDeCompra, Integer page,
            Integer pageSize) {

        OrdenDeCompraDetalleAdapter lista = new OrdenDeCompraDetalleAdapter();

        Map params = new HashMap();

        String idOrdenCompra = ordenDeCompra.getIdOrdenCompra().toString();
        String service = String.format(Resource.ORDEN_DE_COMPRA_DETALLE.url, idOrdenCompra);

        HttpURLConnection conn = GET(service,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    OrdenDeCompraDetalleAdapter.class);

            lista = (OrdenDeCompraDetalleAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     *
     * @param id
     * @return
     */
    public OrdenDeCompra get(Integer id) {
        OrdenDeCompra data = new OrdenDeCompra(id);
        OrdenDeCompraAdapter list = getOrdenDeCompraList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para anular la factura
     *
     * @param oc Objeto
     * @return
     */
    public BodyResponse<OrdenDeCompra> editarOrdenCompra(OrdenDeCompra oc) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.ORDEN_DE_COMPRA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(oc));
            response = BodyResponse.createInstance(conn,
                    OrdenDeCompra.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Método para anular la factura
     *
     * @param oc Objeto
     * @return
     */
    public BodyResponse<OrdenDeCompra> anularOrdenCompra(OrdenDeCompra oc) {

        BodyResponse response = new BodyResponse();
        String idOrdenCompra = oc.getId().toString();
        String service = String.format(Resource.ANULAR_ORDEN_DE_COMPRA.url, idOrdenCompra);

        HttpURLConnection conn = PUT(service, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(oc));
            response = BodyResponse.createInstance(conn,
                    OrdenDeCompra.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Método para crear Factura
     *
     * @param factura
     * @return
     */
    public BodyResponse<OrdenDeCompra> createOrdenCompra(OrdenDeCompra ordenCompra) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.ORDEN_DE_COMPRA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(ordenCompra));

            response = BodyResponse.createInstance(conn, OrdenDeCompra.class);

        }
        return response;
    }

    /**
     * Método para descargar un archivo desde el servidor
     *
     * @param id Clave asociada al archivo
     * @return Contenido descargado
     */
    public byte[] download(String url) {

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, null);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }

    /**
     * Constructor de clase
     */
    public OrdenDeCompraService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        ORDEN_DE_COMPRA("Orden De Compra", "orden-compra"),
        ORDEN_DE_COMPRA_DETALLE("Orden De Compra detalle", "orden-compra-detalle/idOrdenCompra/%s"),
        ANULAR_ORDEN_DE_COMPRA("Anular Orden De Compra", "orden-compra/anular/%s");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
