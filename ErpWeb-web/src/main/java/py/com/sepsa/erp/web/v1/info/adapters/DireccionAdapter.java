/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;
import py.com.sepsa.erp.web.v1.info.remote.DireccionServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.TipoDatoServiceClient;
/**
 *
 * @author Romina Núñez
 */

public class DireccionAdapter extends DataListAdapter<Direccion>  {
    
     /**
     * Cliente para los servicios de clientes
     */
    private final DireccionServiceClient direccionService;

    /**
     * Constructor de TipoDatoPersonaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public DireccionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.direccionService = new DireccionServiceClient();
    }

    /**
     * Constructor de TipoDatoListAdapter
     */
    public DireccionAdapter() {
        super();
        this.direccionService = new DireccionServiceClient();
    }

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public DireccionAdapter fillData(Direccion searchData) {
        return direccionService.getDireccionList(searchData, getFirstResult(),getPageSize());
    }
    

}