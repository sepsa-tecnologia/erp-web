package py.com.sepsa.erp.web.v1.comercial.filters;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;

/**
 * Filtro utilizado para el servicio de contrato
 *
 * @author Romina E. Núñez Rojas
 */
public class ContratoServicioFilter extends Filter {

    /**
     * Agrega el filtro de identificador del contrato
     *
     * @param idContrato
     * @return
     */
    public ContratoServicioFilter idContrato(Integer idContrato) {
        if (idContrato != null) {
            params.put("idContrato", idContrato);
        }
        return this;
    }

  

    /**
     * Construye el mapa de parametros
     *
     * @param contrato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ContratoServicio contratoServicio, Integer page, Integer pageSize) {
        ContratoServicioFilter filter = new ContratoServicioFilter();

        filter
                .idContrato(contratoServicio.getIdContrato())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ContratoFilter
     */
    public ContratoServicioFilter() {
        super();
    }
}
