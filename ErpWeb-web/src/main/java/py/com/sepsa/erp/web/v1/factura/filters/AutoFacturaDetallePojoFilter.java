package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetallePojo;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class AutoFacturaDetallePojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public AutoFacturaDetallePojoFilter idAutofactura(Integer idAutofactura) {
        if (idAutofactura != null) {
            params.put("idAutofactura", idAutofactura);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public AutoFacturaDetallePojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

      /**
     * Agrega el filtro de identificador de nroLinea
     *
     * @param nroLinea
     * @return
     */
    public AutoFacturaDetallePojoFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param factura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(AutoFacturaDetallePojo factura, Integer page, Integer pageSize) {
        AutoFacturaDetallePojoFilter filter = new AutoFacturaDetallePojoFilter();

        filter
                .idAutofactura(factura.getIdAutofactura())
                .listadoPojo(factura.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public AutoFacturaDetallePojoFilter() {
        super();
    }
}
