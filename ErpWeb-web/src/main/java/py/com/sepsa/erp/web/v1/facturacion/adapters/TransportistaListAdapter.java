package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.facturacion.remote.TransportistaService;

/**
 * Adaptador para listado de menú
 *
 * @author Williams Vera
 */
public class TransportistaListAdapter extends DataListAdapter<Transportista> {

    /**
     * Cliente para el servicio de menu
     */
    private final TransportistaService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData datos buscados
     * @return MenuListAdapter
     */
    @Override
    public TransportistaListAdapter fillData(Transportista searchData) {
        return service.getTransportistaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TransportistaListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TransportistaListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new TransportistaService();
    }

    /**
     * Constructor de MenuListAdapter
     */
    public TransportistaListAdapter() {
        super();
        this.service = new TransportistaService();
    }

}
