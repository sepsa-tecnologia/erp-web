/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para Tipo Documento proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoProceso {

    /**
     * Identificador de tipo de documento
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Identificador de tipo archivo
     */
    private Integer idTipoArchivo;
    /**
     * tipo archivo
     */
    private TipoArchivo tipoArchivo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public TipoArchivo getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(TipoArchivo tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public TipoDocumentoProceso() {
    }

}
