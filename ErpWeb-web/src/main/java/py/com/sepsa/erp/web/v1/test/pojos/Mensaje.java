
package py.com.sepsa.erp.web.v1.test.pojos;

/**
 * POJO para test
 * @author Daniel F. Escauriza Arza
 */
public class Mensaje {
    
     /**
     * Identificador del test
     */
    private Integer id;
    
    /**
     * Dato num. 1
     */
    private String descripcion;
    
    /**
     * Dato num. 2
     */
    private String tipo;

    /**
     * Obtiene el identificador del test
     * @return Identificador del test
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador del test
     * @param id Identificador del test
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion
     * @param tipo 
     */
    public Mensaje(Integer id, String descripcion, String tipo) {
        this.id = id;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }


    /**
     * Constructor de Test
     */
    public Mensaje() {}
    
}
