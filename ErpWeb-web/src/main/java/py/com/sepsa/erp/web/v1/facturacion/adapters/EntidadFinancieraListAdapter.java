
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.facturacion.remote.EntidadFinancieraServiceClient;

/**
 * Adaptador de la lista de Entidad Financiera
 * @author Cristina Insfrán
 */
public class EntidadFinancieraListAdapter extends DataListAdapter<EntidadFinanciera> {
  
    /**
     * Cliente remoto para tipo cobro
     */
    private final EntidadFinancieraServiceClient financieraClient;
     /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public EntidadFinancieraListAdapter fillData(EntidadFinanciera searchData) {

        return financieraClient.getEntidadFinancieraList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de EntidadFinancieraListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public EntidadFinancieraListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.financieraClient = new EntidadFinancieraServiceClient();
    }

    /**
     * Constructor de EntidadFinancieraListAdapter
     */
    public EntidadFinancieraListAdapter() {
        super();
        this.financieraClient = new EntidadFinancieraServiceClient();
    }
}
