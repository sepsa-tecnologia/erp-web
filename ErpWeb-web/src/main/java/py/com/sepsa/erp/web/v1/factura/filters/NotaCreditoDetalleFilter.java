package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;

/**
 * Filtro para la lista de nota de credito detalle
 *
 * @author Cristina Insfrán
 */
public class NotaCreditoDetalleFilter extends Filter {

    /**
     * Agrega el filtro de identificador de idNotaCredito
     *
     * @param idNotaCredito
     * @return
     */
    public NotaCreditoDetalleFilter idNotaCredito(Integer idNotaCredito) {
        if (idNotaCredito != null) {
            params.put("idNotaCredito", idNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idFactura
     *
     * @param idFactura
     * @return
     */
    public NotaCreditoDetalleFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nroLinea
     *
     * @param nroLinea
     * @return
     */
    public NotaCreditoDetalleFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de porcentajeIva
     *
     * @param porcentajeIva
     * @return
     */
    public NotaCreditoDetalleFilter porcentajeIva(Integer porcentajeIva) {
        if (porcentajeIva != null) {
            params.put("porcentajeIva", porcentajeIva);
        }
        return this;
    }

    /**
     * Agrega el filtro para montoIva
     *
     * @param montoIva
     * @return
     */
    public NotaCreditoDetalleFilter montoIva(BigDecimal montoIva) {
        if (montoIva != null) {
            params.put("montoIva", montoIva.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoImponible
     *
     * @param montoImponible
     * @return
     */
    public NotaCreditoDetalleFilter montoImponible(BigDecimal montoImponible) {
        if (montoImponible != null) {
            params.put("montoImponible", montoImponible.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoTotal
     *
     * @param montoTotal
     * @return
     */
    public NotaCreditoDetalleFilter montoTotal(BigDecimal montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal.toPlainString());
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaCreditoDetalleFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agrega el filtro para anhoMes
     *
     * @param anhoMes
     * @return
     */
    public NotaCreditoDetalleFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param nc
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaCreditoDetalle nc, Integer page, Integer pageSize) {
        NotaCreditoDetalleFilter filter = new NotaCreditoDetalleFilter();

        filter
                .idNotaCredito(nc.getIdNotaCredito())
                .idFactura(nc.getIdFactura())
                .nroLinea(nc.getNroLinea())
                .porcentajeIva(nc.getPorcentajeIva())
                .montoIva(nc.getMontoIva())
                .montoImponible(nc.getMontoImponible())
                .montoTotal(nc.getMontoTotal())
                .anhoMes(nc.getAnhoMes())
                .anulado(nc.getAnulado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoDetalleFilter
     */
    public NotaCreditoDetalleFilter() {
        super();
    }

}
