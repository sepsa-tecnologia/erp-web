/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.api.v1.services;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.erp.api.response.AbstractResponse;
import py.com.sepsa.erp.api.response.OkResponse;
import py.com.sepsa.erp.api.response.ResponseCode;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para PING
 * @author Jonathan D. Bernal Fernández
 */
@Path("/ping")
public class PingService {
    
    /**
     * Método que maneja las peticiones de ping
     * @return Response
     */
    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response ping() {
        
        WebLogger.get().info("Método: ping.");
        
        AbstractResponse resp = new OkResponse(ResponseCode.SERVICE_OK, null);

        return resp.build();
    }
}
