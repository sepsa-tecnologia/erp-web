package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.remote.SolicitudNotaCreditoService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author SONY
 */
@ViewScoped
@Named("solicitudnotaCreditoDet")
public class SolicitudNotaCreditoDetalleController implements Serializable {

    /**
     * Dato de Nota de Crédito
     */
    private String idNotaCredito;
    /**
     * Objeto solicitud nota de crédito detalle
     */
    private SolicitudNotaCredito solicitudNotaCredito;
    /**
     * Cliente para el servicio de cobro
     */
    private SolicitudNotaCreditoService serviceSolicitudNotaCredito;

    /**
     * @return the idNotaCredito
     */
    public String getIdNotaCredito() {
        return idNotaCredito;
    }

    /**
     * @param idNotaCredito the idNotaCredito to set
     */
    public void setIdNotaCredito(String idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    /**
     * @return the solicitudNotaCredito
     */
    public SolicitudNotaCredito getSolicitudNotaCredito() {
        return solicitudNotaCredito;
    }

    /**
     * @param solicitudNotaCredito the solicitudNotaCredito to set
     */
    public void setSolicitudNotaCredito(SolicitudNotaCredito solicitudNotaCredito) {
        this.solicitudNotaCredito = solicitudNotaCredito;
    }

    /**
     * Método para obtener el detalle de solicitud de nota de crédito
     */
    public void obtenerSolicitudNotaCreditoDetalle() {
        solicitudNotaCredito.setId(Integer.parseInt(idNotaCredito));

        solicitudNotaCredito = serviceSolicitudNotaCredito.getNotaCreditoSolicitudDetalle(solicitudNotaCredito);
    }

    /**
     * @return the serviceSolicitudNotaCredito
     */
    public SolicitudNotaCreditoService getServiceSolicitudNotaCredito() {
        return serviceSolicitudNotaCredito;
    }

    /**
     * @param serviceSolicitudNotaCredito the serviceSolicitudNotaCredito to set
     */
    public void setServiceSolicitudNotaCredito(SolicitudNotaCreditoService serviceSolicitudNotaCredito) {
        this.serviceSolicitudNotaCredito = serviceSolicitudNotaCredito;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idNotaCredito = params.get("id");
      

        this.solicitudNotaCredito = new SolicitudNotaCredito();
        this.serviceSolicitudNotaCredito = new SolicitudNotaCreditoService();
   
        obtenerSolicitudNotaCreditoDetalle();
    }
}
