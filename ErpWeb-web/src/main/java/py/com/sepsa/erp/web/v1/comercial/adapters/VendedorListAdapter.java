/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.info.remote.VendedorService;

/**
 *
 * @author Gustavo Benítez
 */
public class VendedorListAdapter extends DataListAdapter<VendedorPojo> {

    /**
     * Cliente para los servicios de vendedor
     */
    private final VendedorService serviceVendedor;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public VendedorListAdapter fillData(VendedorPojo searchData) {
        searchData.setListadoPojo(Boolean.TRUE);
        return serviceVendedor.list(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de VendedorListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public VendedorListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceVendedor = new VendedorService();
    }

    /**
     * Constructor de VendedorListAdapter
     */
    public VendedorListAdapter() {
        super();
        this.serviceVendedor = new VendedorService();
    }
}
