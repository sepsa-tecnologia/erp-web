package py.com.sepsa.erp.web.v1.configuracionAprobacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionAprobacion;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionAprobacionService;

/**
 * Controlador para la edición de Configuración de aprobación.
 *
 * @author alext
 */
@ViewScoped
@Named("editarConfiguracionAprobacion")
public class ConfiguracionAprobacionEditarController implements Serializable {

    /**
     * Cliente para el servicio de configuración de aprobación.
     */
    private final ConfiguracionAprobacionService service;

    /**
     * Datos de la configuración de aprobación.
     */
    private ConfiguracionAprobacion configuracionAprobacion;

    /**
     * Getters y Setters.
     * @return 
     */
    public ConfiguracionAprobacion getConfiguracionAprobacion() {
        return configuracionAprobacion;
    }

    public void setConfiguracionAprobacion(ConfiguracionAprobacion configuracionAprobacion) {
        this.configuracionAprobacion = configuracionAprobacion;
    }

    /**
     * Método para editar una configuración de aprobación.
     */
    public void edit() {
        service.edit(configuracionAprobacion);
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "La configuración se ha editado correctamente!", "La configuración se ha editado correctamente!!");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    /**
     * Inicializa la configuración de aprobación.
     *
     * @param id Identificador de la entidad configuracionAprobacion.
     */
    private void init(int id) {
        
        this.configuracionAprobacion = service.get(id);
        /*this.configuracionAprobacion = new ConfiguracionAprobacion();
        configuracionAprobacion.setId(id);
        configuracionAprobacion.setEstado('A');
        configuracionAprobacion.setIdTipoEtiqueta(10);
        configuracionAprobacion.setIdEtiqueta(1);*/
    }

    /**
     * Constructor de ConfiguracionAprobacionEditarController.
     */
    public ConfiguracionAprobacionEditarController() {
        this.service = new ConfiguracionAprobacionService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
