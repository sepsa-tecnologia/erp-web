
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;

/**
 * Adaptador para la lista de local
 * @author Cristina Insfrán
 */
public class LocalListAdapter extends DataListAdapter<Local> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final LocalService localClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public LocalListAdapter fillData(Local searchData) {
     
        return localClient.getLocalList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public LocalListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.localClient = new LocalService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public LocalListAdapter() {
        super();
        this.localClient = new LocalService();
    }
}
