/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;
import py.com.sepsa.erp.web.v1.info.remote.TelefonoServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para editar local
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("localEdit")
public class LocalEditController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final LocalService service;
    /**
     * POJO del Local
     */
    private Local local;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
        /**
     * Pojo Telefono
     */
    private Telefono telefono;
    /**
     * Cliente del servicio de Telefono
     */
    private TelefonoServiceClient telefonoService;
    /**
     * Lista de tipo de telefonos
     */
    private List<SelectItem> listTipoTelefono;
    /**
     * Adaptador de TipoTelefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefono;
    /**
     * Adaptador de Telefono
     */
    private TelefonoListAdapter adapterTelefono;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Local getLocal() {
        return local;
    }
    
    public void setLocal(Local local) {
        this.local = local;
    }
    
    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }
    
    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public TelefonoServiceClient getTelefonoService() {
        return telefonoService;
    }

    public void setTelefonoService(TelefonoServiceClient telefonoService) {
        this.telefonoService = telefonoService;
    }

    public List<SelectItem> getListTipoTelefono() {
        return listTipoTelefono;
    }

    public void setListTipoTelefono(List<SelectItem> listTipoTelefono) {
        this.listTipoTelefono = listTipoTelefono;
    }

    public TipoTelefonoListAdapter getAdapterTipoTelefono() {
        return adapterTipoTelefono;
    }

    public void setAdapterTipoTelefono(TipoTelefonoListAdapter adapterTipoTelefono) {
        this.adapterTipoTelefono = adapterTipoTelefono;
    }
    
    //</editor-fold>

    /**
     * Método para editar Local
     */
    public void edit() {
        if (!telefono.getNumero().trim().isEmpty() && !telefono.getPrefijo().trim().isEmpty()){
            local.setTelefono(telefono);
        }
        
        BodyResponse<Local> respuestaLocal = service.editLocal(this.local);
        if (respuestaLocal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Local editado correctamente!"));
        } else {
            for (Mensaje mensaje : respuestaLocal.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }
        }
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public void filterCliente() {
        cliente.setIdCliente(local.getIdPersona());
        clientAdapter = clientAdapter.fillData(cliente);
        cliente.setRazonSocial(clientAdapter.getData().get(0).getRazonSocial());
        cliente.setNroDocumento(clientAdapter.getData().get(0).getNroDocumento());
    }
    
    public void obtenerDatosTelefono(){
        Telefono filterTelefono = new Telefono();
        filterTelefono.setId(local.getIdTelefono());
        adapterTelefono.fillData(filterTelefono);
        this.telefono.setPrincipal("S");
        if (!adapterTelefono.getData().isEmpty()){
            this.telefono = adapterTelefono.getData().get(0);
        } 
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        try {
            this.local = service.get(id);
            this.adapterTelefono = new TelefonoListAdapter();
            if(local.getIdTelefono() != null) {
                this.telefono = local.getTelefono();
            } else {
                this.telefono = new Telefono();
            }   
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            filterCliente();
            
            this.listTipoTelefono = new ArrayList();
            TipoTelefono tipoTelefono = new TipoTelefono(); 
            this.adapterTipoTelefono = new TipoTelefonoListAdapter();
            this.adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefono);
            
            if (adapterTipoTelefono.getData() != null ){
                for (TipoTelefono tp : adapterTipoTelefono.getData()){
                    listTipoTelefono.add(new SelectItem(tp.getId(),tp.getCodigo()));
                }
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
        
    }

    /**
     * Constructor
     */
    public LocalEditController() {
        this.service = new LocalService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
    
}
