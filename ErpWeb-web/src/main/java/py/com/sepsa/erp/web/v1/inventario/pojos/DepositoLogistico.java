/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;

/**
 * Pojo para motivo
 *
 * @author Romina Nuñez
 */
public class DepositoLogistico {

    /**
     * Id
     */
    private Integer id;
    /**
     * Id Local
     */
    private Integer idLocal;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * Activo
     */
    private String activo;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercion;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * Codigo
     */
    private String local;
    /**
     * gln
     */
    private String gln;
    /**
     * cliente
     */
    private String cliente;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercionHasta;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    public Integer getId() {
        return id;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setGln(String gln) {
        this.gln = gln;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getLocal() {
        return local;
    }

    public String getGln() {
        return gln;
    }

    public String getCliente() {
        return cliente;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public String getActivo() {
        return activo;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    
   
    /**
     * Constructor de la clase
     */
    public DepositoLogistico() {

    }

    public DepositoLogistico(Integer id) {
        this.id = id;
    }

}
