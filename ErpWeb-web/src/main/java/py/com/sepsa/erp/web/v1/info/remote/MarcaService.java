package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.MarcaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.MarcaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para marca
 *
 * @author Sergio D. Riveros Vazquez & Alexander Triana
 */
public class MarcaService extends APIErpCore {

    /**
     * Obtiene la lista de productos.
     *
     * @param marca
     * @param page
     * @param pageSize
     * @return
     */
    public MarcaAdapter getMarcaList(Marca marca, Integer page,
            Integer pageSize) {

        MarcaAdapter lista = new MarcaAdapter();

        Map params = MarcaFilter.build(marca, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MarcaAdapter.class);

            lista = (MarcaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Método para crear Marca
     *
     * @param marca
     * @return
     */
    public BodyResponse<Marca> setMarca(Marca marca) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CREAR_MARCA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(marca));
            response = BodyResponse.createInstance(conn, Marca.class);
        }
        return response;
    }

    /**
     * Método para editar Marca
     *
     * @param marca
     * @return
     */
    public BodyResponse<Marca> editMarca(Marca marca) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.EDITAR_MARCA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(marca));
            response = BodyResponse.createInstance(conn, Marca.class);
        }
        return response;
    }

    /**
     * Método para obtener Marca a ser editada
     * @param id
     * @return
     */
    public Marca get(Integer id) {
        Marca data = new Marca(id);
        MarcaAdapter list = getMarcaList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor
     */
    public MarcaService() {
        super();
    }
    
    
    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        LISTAR("Listado de Marca", "marca"),
        EDITAR_MARCA("Edición de Marca","marca"),
        CREAR_MARCA("Creación de Marca","marca");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
