/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TransportistaListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.TransportistaService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("transportistaCreate")
public class TransportistaCreateController implements Serializable {
    
    private Transportista transportista;
    
    private TransportistaService transportistaService;
    
    private TransportistaListAdapter adapterTransportista;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public TransportistaListAdapter getAdapterTransportista() {
        return adapterTransportista;
    }

    public void setAdapterTransportista(TransportistaListAdapter adapterTransportista) {
        this.adapterTransportista = adapterTransportista;
    }

    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public TransportistaService getTransportistaService() {
        return transportistaService;
    }

    public void setTransportistaService(TransportistaService transportistaService) {
        this.transportistaService = transportistaService;
    }
    
    //</editor-fold>
    
    /**
     * Método que valida un string a partir de un patrón regex
     * @param cadena
     * @param patronRegex
     * @return 
     */
    public boolean validadorDePatron(String cadena, String patronRegex) {
        try {
            Pattern pattern = Pattern.compile(patronRegex);

            Matcher matcher = pattern.matcher(cadena);

            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
    
    public void formatearCampos() {
        if (transportista != null) {
            if (transportista.getNroDocumento() != null) {
                transportista.setNroDocumento(transportista.getNroDocumento().trim().replace(" ", "").replace(".", ""));
            }
            
            if (transportista.getNroDocumentoChofer() != null) { 
                transportista.setNroDocumentoChofer(transportista.getNroDocumentoChofer().trim().replace(" ", "").replace(".", ""));
            }
            
            if (transportista.getRuc() != null) {   
                String ruc =  transportista.getRuc().trim().replace(" ", "").replace(".", "");
                
                //  Se extrael el digito verificador del ruc si fue ingresado.       
                if (ruc.contains("-")){
                    String[] nroRuc = ruc.split("-");
                    ruc = nroRuc[0];
                }
                transportista.setRuc(ruc);
            }

        }
    }
    
    public boolean validarCampos() {
        formatearCampos();
        boolean saveNr = true;
        if (transportista != null) {
            if (transportista.getIdNaturalezaTransportista() == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de contribuyente!"));
            }
            
            if (transportista.getIdNaturalezaTransportista() == 1) {
                if (!transportista.getRuc().trim().isEmpty() && transportista.getRuc().trim().length() >= 3) {
                    //Validar que el RUC solo tenga dígitos
                    if (!validadorDePatron(transportista.getRuc(), "^[0-9]+$")) {
                        saveNr = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                    }
                } else {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                }
                if (transportista.getVerificadorRuc() != null && (transportista.getVerificadorRuc()< 0 || transportista.getVerificadorRuc()> 9 )) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un dígito verificador de RUC valido!"));
                }
            }

            if (transportista.getRazonSocial().trim().equalsIgnoreCase("") || transportista.getRazonSocial().length() < 4) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre/razon social de transportista válido!"));
            }

            if (transportista.getNombreCompleto().trim().equalsIgnoreCase("") || transportista.getNombreCompleto().length() < 4) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre de chofer válido!"));
            }

            if (transportista.getDireccionChofer().trim().equalsIgnoreCase("") || transportista.getDireccionChofer().length() < 1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección del chofer!"));
            }

            if (transportista.getDomicilioFiscal().trim().equalsIgnoreCase("") || transportista.getDomicilioFiscal().length() < 1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el domicilio fiscal!"));
            }

        }

        return saveNr;
    }
    
    public void clearForm(){
        this.transportista = new Transportista();
    }
    
    public void crearTransportista(){
        if (validarCampos()){          
             BodyResponse response = transportistaService.setTransportista(transportista);
             if (response != null){
                 if (response.getSuccess()){
                     clearForm();
                     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Transportista cargado correctamente!"));
                 }
             }
        } 
    }
    
    @PostConstruct
    public void init(){
        this.transportista = new Transportista();
        this.transportistaService = new TransportistaService();
    }
}
