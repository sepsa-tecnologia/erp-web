/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;

/**
 *
 * @author Romina Núñez
 */
@FacesConverter("configuracionPojoConverter")
public class ConfiguracionPojoConverter implements Converter{
    
      /**
     * Servicios para login al sistema
     */
    private ConfiguracionValorServiceClient serviceConfiguracion;
    
    @Override
    public Configuracion getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceConfiguracion = new ConfiguracionValorServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            Configuracion conf = new Configuracion();
            conf.setId(val);
            List<Configuracion> configuraciones = serviceConfiguracion.getConfiguracionList(conf, 0, 10).getData();
            if(configuraciones != null && !configuraciones.isEmpty()) {
                return configuraciones.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es una configuración válida"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Configuracion) {
                return String.valueOf(((Configuracion)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
