package py.com.sepsa.erp.web.v1.producto.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;

/**
 * Controlador para lista de productos.
 *
 * @author alext
 */
@ViewScoped
@Named("listarProducto")
public class ProductoListarController implements Serializable {

    /**
     * Adaptador de producto.
     */
    private ProductoAdapter adapter;

    /**
     * Objeto producto.
     */
    private Producto searchData;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public ProductoAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ProductoAdapter adapter) {
        this.adapter = adapter;
    }

    public Producto getSearchData() {
        return searchData;
    }

    public void setSearchData(Producto searchData) {
        this.searchData = searchData;
    }
    //</editor-fold>

    /**
     * Método para filtrar productos.
     */
    public void filter(){
        this.adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar (){
        searchData = new Producto();
        filter();
    }

    /**
     * Método para generar lista de clientes según servicios.
     */
    public void search() {
        adapter = new ProductoAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new Producto();
        search();
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public ProductoListarController() {
        adapter = new ProductoAdapter();
        searchData = new Producto();
    }

}
