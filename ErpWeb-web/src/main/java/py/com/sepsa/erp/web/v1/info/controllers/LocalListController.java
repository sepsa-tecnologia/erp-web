package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para Listar Local
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("localList")
public class LocalListController implements Serializable {

    /**
     * Servicio para Local
     */
    private LocalService localService;
    /**
     * Objeto para filtros
     */
    private Local localFilter;
    /**
     * Adaptador de local
     */
    private LocalListAdapter adapter;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public LocalService getLocalService() {
        return localService;
    }

    /**
     * @return the localService
     */
    public void setLocalService(LocalService localService) {
        this.localService = localService;
    }

    /**
     * @return the localFilter
     */
    public Local getLocalFilter() {
        return localFilter;
    }

    /**
     * @param localFilter the localFilter to set
     */
    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    /**
     * @return the adapter
     */
    public LocalListAdapter getAdapter() {
        return adapter;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(LocalListAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    //</editor-fold>
    /**
     * Método para filtrar locales.
     */
    public void filter() {
        getAdapter().setFirstResult(0);
        this.localFilter.setLocalExterno("S");
        this.adapter = adapter.fillData(localFilter);
    }

      /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idCliente
     * @return
     */
    public String direccionURL(Integer idCliente, Integer idDireccion) {
        return String.format("/app/direccion/client-list-direccion.xhtml"
                + "?faces-redirect=true"
                + "&idCliente=%d"
                + "&idDireccion=%d", idCliente, idDireccion);
    }
    
    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clear() {
        localFilter = new Local();
        filter();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        localFilter.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Metodo para redirigir a la vista Editar Talonario
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("local-edit?faces-redirect=true&id=%d", id);
    }
    
     

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.setLocalService(new LocalService());
        this.setLocalFilter(new Local());
        this.setAdapter(new LocalListAdapter());
        this.localFilter = new Local();
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        filter();
    }
}
