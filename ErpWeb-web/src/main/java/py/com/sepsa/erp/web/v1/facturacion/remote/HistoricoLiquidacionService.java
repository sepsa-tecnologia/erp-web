package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.HistoricoLiquidacionFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de cobro
 *
 * @author Romina Núñez
 */
public class HistoricoLiquidacionService extends APIErpFacturacion {

    /**
     * Obtiene la lista de liquidación
     *
     * @param historicoLiquidacion Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public HistoricoLiquidacionAdapter getHistoricoLiquidacionList(HistoricoLiquidacion historicoLiquidacion, Integer page,
            Integer pageSize) {

        HistoricoLiquidacionAdapter lista = new HistoricoLiquidacionAdapter();

        Map params = HistoricoLiquidacionFilter.build(historicoLiquidacion, page, pageSize);

        HttpURLConnection conn = GET(Resource.HISTORICO_LIQUIDACION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    HistoricoLiquidacionAdapter.class);

            lista = (HistoricoLiquidacionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
    /**
     * Obtiene el Histórico Liquidacion + Detalles
     *
     * @param historicoLiquidacion Objeto
     * @return
     */
    public HistoricoLiquidacion getHistoricoLiquidacionDetalle(HistoricoLiquidacion historicoLiquidacion) {
        
        HistoricoLiquidacion h = new HistoricoLiquidacion();

        Map params = new HashMap<>();
        
        params.put("idHistoricoLiquidacion", historicoLiquidacion.getIdHistoricoLiquidacion());

        HttpURLConnection conn = GET(Resource.HISTORICO_LIQUIDACION_DETALLE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    HistoricoLiquidacion.class);

            h = (HistoricoLiquidacion) response.getPayload();

            conn.disconnect();
        }
        return h;
    }


    /**
     * Constructor de FacturacionService
     */
    public HistoricoLiquidacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        HISTORICO_LIQUIDACION("HistoricoLiquidacion", "historico-liquidacion"),
        HISTORICO_LIQUIDACION_DETALLE("HistoricoLiquidacionDetllae", "historico-liquidacion/consulta");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
