package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 * Controlador para cerrar el sistema
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("logout")
public class LogoutController implements Serializable {
    
    /**
     * Datos de la sesión
     */
    @Inject
    private SessionData session;

    /**
     * Método para cerrar el sistema
     * @return Redirección a la página de login
     */
    public String logout() {
        session.clearMsn();
        session.logoutMsn();
        session.setJwt(null);
        FacesContext.getCurrentInstance()
                .getExternalContext()
                .invalidateSession();
        return "/login.xhtml?faces-redirect=true;";
    }
    
    /** 
     * Constructor de LogoutController
     */
    public LogoutController() {}
}