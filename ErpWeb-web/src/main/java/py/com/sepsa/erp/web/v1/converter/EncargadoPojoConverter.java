/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;

/**
 *
 * @author Romina Núñez
 */
@FacesConverter("encargadoPojoConverter")
public class EncargadoPojoConverter implements Converter {

    /**
     * Servicios para Usuario
     */
    private UserServiceClient serviceClient;

    @Override
    public Usuario getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new UserServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            Usuario enc = new Usuario();
            enc.setId(val);

            List<Usuario> encargados = serviceClient.getUserList(enc, 0, 10).getData();
            if (encargados != null && !encargados.isEmpty()) {
                return encargados.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es un encargado válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof Usuario) {
                return String.valueOf(((Usuario) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
