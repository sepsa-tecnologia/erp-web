
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.DatoPersona;

/**
 * Filtro utilizado para dato persona
 * @author Cristina Insfrán
 */
public class DatoPersonaFilter extends Filter{
    
     
     /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public DatoPersonaFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de tipo dato persona
     * @param idTipoDatoPersona Identificador del tipo de dato
     * @return 
     */
    public DatoPersonaFilter idTipoDatoPersona(Integer idTipoDatoPersona){
         if(idTipoDatoPersona != null){
           params.put("idTipoDatoPersona", idTipoDatoPersona);
         }
         return this;
    }
    
    /**
     * Agrega el filtro del valor
     * @param valor Descripcion del valor
     * @return 
     */
    public DatoPersonaFilter valor(String valor){
         if(valor!= null){
           params.put("valor", valor);
         }
         return this;
    }
    
     /**
     * Construye el mapa de parametros
     *
     * @param dato datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(DatoPersona dato, Integer page, Integer pageSize) {
        DatoPersonaFilter filter = new DatoPersonaFilter();

        filter
                .idPersona(dato.getIdPersona())
                .idTipoDatoPersona(dato.getIdTipoDatoPersona())
                .valor(dato.getValor())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DatoPersona
     */
    public DatoPersonaFilter() {
        super();
    }
}
