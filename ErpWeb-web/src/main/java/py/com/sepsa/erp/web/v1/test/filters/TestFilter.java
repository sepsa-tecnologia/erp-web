
package py.com.sepsa.erp.web.v1.test.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.test.pojos.Test;

/**
 * Filtro utilizado en el cliente del servicio test
 * @author Daniel F. Escauriza Arza
 */
public class TestFilter extends Filter {
    
    /**
     * Agrega al filtro el identificador del test
     * @param id Identificador del test
     * @return TestFilter
     */
    public TestFilter id(Integer id) {
        if(id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega al filtro el dato 1
     * @param data1 Dato num. 1
     * @return TestFilter
     */
    public TestFilter data1(String data1) {
        if(data1 != null && !data1.trim().isEmpty()) {
            params.put("data1", data1);
        }
        return this;
    }
    
    /**
     * Agrega al filtro el dato 2
     * @param data2 Dato num. 2
     * @return TestFilter
     */
    public TestFilter data2(String data2) {
        if(data2 != null && !data2.trim().isEmpty()) {
            params.put("data2", data2);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parámetros de filtro
     * @param test Datos del filtro
     * @param page Página buscada en el resultado
     * @param pageSize Tamaño de la página resultado
     * @return Mapa de parámetros dse filtro
     */
    public static Map build(Test test, Integer page, Integer pageSize) {
        TestFilter filter = new TestFilter();
        
        filter
                .id(test.getId())
                .data1(test.getData1())
                .data2(test.getData2())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
                
    }

    /**
     * Constructor de TestFilter
     */
    public TestFilter() {
        super();
    }
}
