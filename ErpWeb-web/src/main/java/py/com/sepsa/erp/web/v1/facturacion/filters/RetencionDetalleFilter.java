/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionDetalle;
import py.com.sepsa.erp.web.v1.filters.Filter;


/**
 * Filtro para Retención Detalle Filter
 * @author Cristina Insfrán
 */
public class RetencionDetalleFilter extends Filter{
   /**
     * Agrega el parametro para el filtro idRetencion
     *
     * @param idRetencion
     * @return
     */
    public RetencionDetalleFilter idRetencion(Integer idRetencion) {
        if (idRetencion != null) {
            params.put("idRetencion", idRetencion);
        }
        return this;
    }
    
    
      /**
     * Agrega el parametro para el filtro idFactura
     *
     * @param idFactura
     * @return
     */
    public RetencionDetalleFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro montoImponible
     *
     * @param montoImponible
     * @return
     */
    public RetencionDetalleFilter montoImponible(BigDecimal montoImponible) {
        if (montoImponible != null) {
            params.put("montoImponible", montoImponible);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro montoIva
     *
     * @param montoIva
     * @return
     */
    public RetencionDetalleFilter montoIva(BigDecimal montoIva) {
        if (montoIva != null) {
            params.put("montoIva", montoIva);
        }
        return this;
    }
    
      /**
     * Agrega el parametro para el filtro montoTotal
     *
     * @param montoTotal
     * @return
     */
    public RetencionDetalleFilter montoTotal(BigDecimal montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal);
        }
        return this;
    }
    
    
     /**
     * Agrega el parametro para el filtro montoRetenido
     *
     * @param montoRetenido
     * @return
     */
    public RetencionDetalleFilter montoRetenido(BigDecimal montoRetenido) {
        if (montoRetenido != null) {
            params.put("montoRetenido", montoRetenido);
        }
        return this;
    }
   
    
     /**
     * Construye el mapa de parametros
     *
     * @param ret
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(RetencionDetalle ret,Integer page, Integer pageSize) {
        RetencionDetalleFilter filter = new RetencionDetalleFilter();

        filter
                .idRetencion(ret.getIdRetencion())
                .idFactura(ret.getIdFactura())
                .montoImponible(ret.getMontoImponible())
                .montoIva(ret.getMontoIva())
                .montoTotal(ret.getMontoTotal())
                .montoRetenido(ret.getMontoRetenido())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de RetencionDetalleFilter
     */
    public RetencionDetalleFilter() {
        super();
    } 
}
