/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 *
 * @author Williams Vera
 */
public class FacturaNotificacion {
    /**
     * Identificador de factura
     */
    private Integer idFactura;

    /**
     * Identificador de tipo de notificación
     */
    private Integer idTipoNotificacion;

    /**
     * Código de tipo de notificación
     */
    private String codigoTipoNotificacion;

    /**
     * Email
     */
    private String email;

    
    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public FacturaNotificacion() {
    }
    
}
