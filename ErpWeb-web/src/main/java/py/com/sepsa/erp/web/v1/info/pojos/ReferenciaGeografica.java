
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para referencia geografica
 * @author Cristina Insfrán
 */
public class ReferenciaGeografica {

   
    /**
     * Identificador 
     */
    private Integer id;
    /**
     * Código de la referencia geografica
     */
    private String codigo;
    /**
     * Codigo tipo referencia geográfica
     */
    private String codigoTipoReferenciaGeografica;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Identificador tipo de referencia
     */
    private Integer idTipoReferenciaGeografica;
    /**
     * Identificador del padre
     */
    private Integer idPadre;
    /**
     * Código padre
     */
    private String codigoPadre;
    /**
     * Tiene Padre
     */
    private String tienePadre;
    /**
     * Objeto Tipo Referencia Geografica
     */
    private TipoReferenciaGeografica tipoReferenciaGeografica;
    /**
     *  Objeto padre
     */
    private ReferenciaGeografica padre;
    
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    
    /**
     * @return the tienePadre
     */
    public String getTienePadre() {
        return tienePadre;
    }

    /**
     * @param tienePadre the tienePadre to set
     */
    public void setTienePadre(String tienePadre) {
        this.tienePadre = tienePadre;
    }

    /**
     * @return the codigoPadre
     */
    public String getCodigoPadre() {
        return codigoPadre;
    }

    /**
     * @param codigoPadre the codigoPadre to set
     */
    public void setCodigoPadre(String codigoPadre) {
        this.codigoPadre = codigoPadre;
    }

    /**
     * @return the codigoTipoReferenciaGeografica
     */
    public String getCodigoTipoReferenciaGeografica() {
        return codigoTipoReferenciaGeografica;
    }

    /**
     * @param codigoTipoReferenciaGeografica the codigoTipoReferenciaGeografica to set
     */
    public void setCodigoTipoReferenciaGeografica(String codigoTipoReferenciaGeografica) {
        this.codigoTipoReferenciaGeografica = codigoTipoReferenciaGeografica;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }
    
    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }
    
    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * @return the idTipoReferenciaGeografica
     */
    public Integer getIdTipoReferenciaGeografica() {
        return idTipoReferenciaGeografica;
    }
    
    /**
     * @param idTipoReferenciaGeografica the idTipoReferenciaGeografica to set
     */
    public void setIdTipoReferenciaGeografica(Integer idTipoReferenciaGeografica) {
        this.idTipoReferenciaGeografica = idTipoReferenciaGeografica;
    }
    /**
     * @return the idPadre
     */
    public Integer getIdPadre() {
        return idPadre;
    }
    
    /**
     * @param idPadre the idPadre to set
     */
    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }
    
    /**
     * @return the tipoReferenciaGeografica
     */
    public TipoReferenciaGeografica getTipoReferenciaGeografica() {
        return tipoReferenciaGeografica;
    }
    
    /**
     * @param tipoReferenciaGeografica the tipoReferenciaGeografica to set
     */
    public void setTipoReferenciaGeografica(TipoReferenciaGeografica tipoReferenciaGeografica) {
        this.tipoReferenciaGeografica = tipoReferenciaGeografica;
    }
    
    /**
     * @return the padre
     */
    public ReferenciaGeografica getPadre() {
        return padre;
    }
    
    /**
     * @param padre the padre to set
     */
    public void setPadre(ReferenciaGeografica padre) {
        this.padre = padre;
    }
//</editor-fold>

    /**
     * Constructor de la clase
     */
    public ReferenciaGeografica(){
        
    }

    /**
     * Constructor con parámetros
     * @param id 
     */
    public ReferenciaGeografica(Integer id) {
        this.id = id;
    }
    
}
