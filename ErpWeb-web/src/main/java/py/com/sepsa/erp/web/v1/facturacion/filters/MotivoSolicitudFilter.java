
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionSnc;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para motivo solicitud
 * @author Cristina Insfrán
 */
public class MotivoSolicitudFilter extends Filter{
    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public MotivoSolicitudFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
     /**
     * Agregar el filtro descripción
     *
     * @param descripcion
     * @return
     */
    public MotivoSolicitudFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
     /**
     * Agregar el filtro codigoXml
     *
     * @param codigoXml
     * @return
     */
    public MotivoSolicitudFilter codigoXml(String codigoXml) {
        if (codigoXml != null && !codigoXml.trim().isEmpty()) {
            params.put("codigoXml", codigoXml);
        }
        return this;
    }
    
     /**
     * Agregar el filtro codigoTxt
     *
     * @param codigoTxt
     * @return
     */
    public MotivoSolicitudFilter codigoTxt(String codigoTxt) {
        if (codigoTxt != null && !codigoTxt.trim().isEmpty()) {
            params.put("codigoTxt", codigoTxt);
        }
        return this;
    }
    
      /**
     * Agregar el filtro activo
     *
     * @param activo
     * @return
     */
    public MotivoSolicitudFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     *
     * @param motivo
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(MotivoEmisionSnc motivo, Integer page, Integer pageSize) {
        MotivoSolicitudFilter filter = new MotivoSolicitudFilter();

        filter
                .id(motivo.getId())
                .descripcion(motivo.getDescripcion())
                .codigoTxt(motivo.getCodigoTxt())
                .codigoXml(motivo.getCodigoXml())
                .activo(motivo.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de MotivoSolicitudFilter
     */
    public MotivoSolicitudFilter() {
        super();
    }
 
}
