package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;

/**
 *
 * @author alext
 */
@FacesConverter("menuPojoConverter")
public class MenuPojoConverter implements Converter {

    /**
     * Servicios para login al sistema
     */
    private MenuService menuService;

    @Override
    public MenuPojo getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            menuService = new MenuService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            MenuPojo menu = new MenuPojo();
            menu.setId(val);
            List<MenuPojo> menus = menuService.getMenuList(menu, 0, 10).getData();
            if (menus != null && !menus.isEmpty()) {
                return menus.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es un menu válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof MenuPojo) {
                return String.valueOf(((MenuPojo) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
