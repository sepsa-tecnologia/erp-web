/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmpresa;
import py.com.sepsa.erp.web.v1.info.remote.TipoEmpresaService;

/**
 * Adapter para tipo empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEmpresaAdapter extends DataListAdapter<TipoEmpresa> {

    /**
     * Cliente para el servicio tipo proceso
     */
    private final TipoEmpresaService tipoEmpresaService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoEmpresaAdapter fillData(TipoEmpresa searchData) {

        return tipoEmpresaService.getTipoEmpresaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoEmpresaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoEmpresaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoEmpresaService = new TipoEmpresaService();
    }

    /**
     * Constructor de TipoEmpresaAdapter
     */
    public TipoEmpresaAdapter() {
        super();
        this.tipoEmpresaService = new TipoEmpresaService();
    }
}
