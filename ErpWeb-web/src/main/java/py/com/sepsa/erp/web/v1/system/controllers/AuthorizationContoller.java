
package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador de authorización
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("authorization")
public class AuthorizationContoller implements Serializable {
    
    // URLs
    private static final String LOGIN = "login.xhtml";
    
    /**
     * Datos de la sesión
     */
    @Inject
    private SessionData session;

    /**
     * Obtiene los datos de la sesión
     * @return Datos de la sesión
     */
    public SessionData getSession() {
        return session;
    }

    /**
     * Setea los datos de la sesión
     * @param session Datos de la sesión
     */
    public void setSession(SessionData session) {
        this.session = session;
    }
    
    /**
     * Inicializa los valores del controlador
     */
    @PostConstruct
    public void init() {
        try {
            
            if(session == null || session.getJwt() == null) {
                session.clearMsn();
                session.jwtError();
                
                FacesContext context = FacesContext.getCurrentInstance();
                String contextPath = context
                        .getExternalContext()
                        .getRequestContextPath();
                context.getExternalContext()
                        .redirect(String.format("%s/%s", contextPath, LOGIN));
            }
        } catch(IOException ex) {
            WebLogger.get().fatal(ex);
        }
    }
    
    /**
     * Constructor de AuthorizationController
     */
    public AuthorizationContoller() {}
    
}
