package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.ReporteEmpresa;
import py.com.sepsa.erp.web.v1.info.remote.ReporteEmpresaService;

/**
 * Converter para reporte empresa
 *
 * @author Jonathan Bernal
 */
@FacesConverter("reporteEmpresaConverter")
public class ReporteEmpresaConverter implements Converter {

    @Override
    public ReporteEmpresa getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            ReporteEmpresaService service = new ReporteEmpresaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            ReporteEmpresa filter = new ReporteEmpresa();
            filter.setId(val);

            List<ReporteEmpresa> list = service.find(filter, 0, 10).getData();
            if (list != null && !list.isEmpty()) {
                return list.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es registro válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof ReporteEmpresa) {
                return String.valueOf(((ReporteEmpresa) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
