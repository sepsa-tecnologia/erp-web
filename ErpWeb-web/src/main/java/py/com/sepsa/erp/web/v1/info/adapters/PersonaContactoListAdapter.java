
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaContacto;
import py.com.sepsa.erp.web.v1.info.remote.ContactoServiceClient;

/**
 * Adaptador para la lista de persona contacto
 * @author Cristina Insfrán
 */
public class PersonaContactoListAdapter extends DataListAdapter<PersonaContacto> {
    
    /**
     * Cliente para los servicios de persona contacto
     * */
    private final ContactoServiceClient personaContactoClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public PersonaContactoListAdapter fillData(PersonaContacto searchData) {
     
        return personaContactoClient.getPersonaContactoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de PersonaContactoListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PersonaContactoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.personaContactoClient = new ContactoServiceClient();
    }

    /**
     * Constructor de PersonaContactoListAdapter
     */
    public PersonaContactoListAdapter() {
        super();
        this.personaContactoClient= new ContactoServiceClient();
    }
}
