package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoServicioAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ContratoServicioFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Contrato
 *
 * @author Romina Núñez
 */
public class ContratoServicioService extends APIErpComercial {

    /**
     * Obtiene la lista de descuentos
     *
     * @param descuento 
     * @param page
     * @param pageSize
     * @return
     */
    public ContratoServicioAdapter getContratoServicioList(ContratoServicio contratoService, Integer page,
            Integer pageSize) {

        ContratoServicioAdapter lista = new ContratoServicioAdapter();

        Map params = ContratoServicioFilter.build(contratoService, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTRATO_SERVICIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContratoServicioAdapter.class);

            lista = (ContratoServicioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    

    
    /**
     * Constructor de ContratoService
     */
    public ContratoServicioService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        CONTRATO_SERVICIO("Contrato Servicio", "contrato-servicio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
