/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para operacion de inventario
 *
 * @author Romina Núñez
 */
public class DevolucionFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public DevolucionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idUsuario
     *
     * @param idUsuario
     * @return
     */
    public DevolucionFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

    /**
     * Agrega el filtro por idCliente
     *
     * @param idCliente
     * @return
     */
    public DevolucionFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProveedor
     *
     * @param idProveedor
     * @return
     */
    public DevolucionFilter idProveedor(Integer idProveedor) {
        if (idProveedor != null) {
            params.put("idProveedor", idProveedor);
        }
        return this;
    }

    /**
     * Agrega el filtro por idTipoOperacion
     *
     * @param idTipoOperacion
     * @return
     */
    public DevolucionFilter idTipoOperacion(Integer idTipoOperacion) {
        if (idTipoOperacion != null) {
            params.put("idTipoOperacion", idTipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro por idMotivo
     *
     * @param idMotivo
     * @return
     */
    public DevolucionFilter idMotivo(Integer idMotivo) {
        if (idMotivo != null) {
            params.put("idMotivo", idMotivo);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de estado
     *
     * @param idEstado
     * @return
     */
    public DevolucionFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por idLocalOrigen
     *
     * @param idLocalOrigen
     * @return
     */
    public DevolucionFilter idLocalOrigen(Integer idLocalOrigen) {
        if (idLocalOrigen != null) {
            params.put("idLocalOrigen", idLocalOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idLocalDestino
     *
     * @param idLocalDestino
     * @return
     */
    public DevolucionFilter idLocalDestino(Integer idLocalDestino) {
        if (idLocalDestino != null) {
            params.put("idLocalDestino", idLocalDestino);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDepositoOrigen
     *
     * @param idDepositoOrigen
     * @return
     */
    public DevolucionFilter idDepositoOrigen(Integer idDepositoOrigen) {
        if (idDepositoOrigen != null) {
            params.put("idDepositoOrigen", idDepositoOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDepositoDestino
     *
     * @param idDepositoDestino
     * @return
     */
    public DevolucionFilter idDepositoDestino(Integer idDepositoDestino) {
        if (idDepositoDestino != null) {
            params.put("idDepositoDestino", idDepositoDestino);
        }
        return this;
    }

    /**
     * Agrega el filtro tipo de Operacion
     *
     * @param tipoOperacion
     * @return
     */
    public DevolucionFilter tipoOperacion(String tipoOperacion) {
        if (tipoOperacion != null && !tipoOperacion.trim().isEmpty()) {
            params.put("tipoOperacion", tipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo tipo de Operacion
     *
     * @param codigoTipoOperacion
     * @return
     */
    public DevolucionFilter codigoTipoOperacion(String codigoTipoOperacion) {
        if (codigoTipoOperacion != null && !codigoTipoOperacion.trim().isEmpty()) {
            params.put("codigoTipoOperacion", codigoTipoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro motivo Operacion
     *
     * @param motivoOperacion
     * @return
     */
    public DevolucionFilter motivoOperacion(String motivoOperacion) {
        if (motivoOperacion != null && !motivoOperacion.trim().isEmpty()) {
            params.put("motivoOperacion", motivoOperacion);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo motivo Operacion
     *
     * @param codigoMotivo
     * @return
     */
    public DevolucionFilter codigoMotivo(String codigoMotivo) {
        if (codigoMotivo != null && !codigoMotivo.trim().isEmpty()) {
            params.put("codigoMotivo", codigoMotivo);
        }
        return this;
    }

    /**
     * Agrega el filtro Estado
     *
     * @param estado
     * @return
     */
    public DevolucionFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public DevolucionFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public DevolucionFilter listadoPojo(String listadoPojo) {
        if (listadoPojo != null && !listadoPojo.trim().isEmpty()) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaInsercion
     * @return
     */
    public DevolucionFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
                
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro nroDocumentoAsociado
     *
     * @param nroDocumentoAsociado
     * @return
     */
    public DevolucionFilter nroDocumentoAsociado(String nroDocumentoAsociado) {
        if (nroDocumentoAsociado != null && !nroDocumentoAsociado.trim().isEmpty()) {
            params.put("nroDocumentoAsociado", nroDocumentoAsociado);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoEstado
     *
     * @param localOrigen
     * @return
     */
    public DevolucionFilter localOrigen(String localOrigen) {
        if (localOrigen != null && !localOrigen.trim().isEmpty()) {
            params.put("localOrigen", localOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idTipoOperacion
     *
     * @param localDestino
     * @return
     */
    public DevolucionFilter localDestino(String localDestino) {
        if (localDestino != null && !localDestino.trim().isEmpty()) {
            params.put("localDestino", localDestino);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public DevolucionFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaInsercionDesde);
                
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionHasta
     * @return
     */
    public DevolucionFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaInsercionHasta);
                
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param devolucion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Devolucion devolucion, Integer page, Integer pageSize) {
        DevolucionFilter filter = new DevolucionFilter();
        
        filter
                .id(devolucion.getId())
                .listadoPojo(devolucion.getListadoPojo())
                .idEstado(devolucion.getIdEstado())
                .fechaInsercionDesde(devolucion.getFechaInsercionDesde())
                .fechaInsercionHasta(devolucion.getFechaInsercionHasta())
                .page(page)
                .pageSize(pageSize);
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public DevolucionFilter() {
        super();
    }
    
}
