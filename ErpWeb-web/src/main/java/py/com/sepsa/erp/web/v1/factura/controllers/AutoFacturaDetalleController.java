package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutoFacturaDetallePojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.AutoFacturaService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;

/**
 * Controlador para Detalles
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("autofacturaDetalle")
public class AutoFacturaDetalleController implements Serializable {

    /**
     * Servicio Facturacion
     */
    private AutoFacturaService serviceFactura;
    /**
     * POJO Factura
     */
    private AutoFacturaPojo facturaDetalle;
    /**
     * Dato de factura
     */
    private String idAutofactura;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    private AutoFacturaDetallePojoAdapter adapterDetalle;
    private AutoFacturaDetallePojo facturaDetalleFilter;
    private Map parametros;
    
            /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    private Boolean siediApiKude = false;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public Boolean getSiediApiKude() {
        return siediApiKude;
    }

    public void setSiediApiKude(Boolean siediApiKude) {
        this.siediApiKude = siediApiKude;
    }
    
    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }

    public Map getParametros() {
        return parametros;
    }

    public AutoFacturaService getServiceFactura() {
        return serviceFactura;
    }

    public void setServiceFactura(AutoFacturaService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public AutoFacturaPojo getFacturaDetalle() {
        return facturaDetalle;
    }

    public void setFacturaDetalle(AutoFacturaPojo facturaDetalle) {
        this.facturaDetalle = facturaDetalle;
    }

    public String getIdAutofactura() {
        return idAutofactura;
    }

    public void setIdAutofactura(String idAutofactura) {
        this.idAutofactura = idAutofactura;
    }

    public AutoFacturaDetallePojoAdapter getAdapterDetalle() {
        return adapterDetalle;
    }

    public void setAdapterDetalle(AutoFacturaDetallePojoAdapter adapterDetalle) {
        this.adapterDetalle = adapterDetalle;
    }

    public AutoFacturaDetallePojo getFacturaDetalleFilter() {
        return facturaDetalleFilter;
    }

    public void setFacturaDetalleFilter(AutoFacturaDetallePojo facturaDetalleFilter) {
        this.facturaDetalleFilter = facturaDetalleFilter;
    }

    

//</editor-fold>
    /**
     * Método para obtener los detalles de la factura
     */
    public void obtenerFacturaDetalle() {
        facturaDetalle.setId(Integer.parseInt(idAutofactura));
        facturaDetalle.setListadoPojo(true);

        facturaDetalle = serviceFactura.getAutoFacturaDetalle(facturaDetalle);

        facturaDetalleFilter.setListadoPojo(true);
        facturaDetalleFilter.setIdAutofactura(Integer.parseInt(idAutofactura));

        adapterDetalle = adapterDetalle.fillData(facturaDetalleFilter);

    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download(Boolean ticket) {
        String contentType = ("application/pdf");
        String fileName = facturaDetalle.getNroAutofactura() + ".pdf";
        String url = "autofactura/consulta/" + idAutofactura;
        if (facturaDetalle.getDigital().equalsIgnoreCase("S")){
            parametros.put("ticket", ticket);
            parametros.put("siediApi", siediApiKude);
        }
        byte[] data = fileServiceClient.downloadFactura(url, parametros);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "factura/consulta/" + idAutofactura;
        byte[] data = fileServiceClient.downloadFactura(url, parametros);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    public String obtenerConfigDescarga(String codigo) {

        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "false";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;
    }

    public void completarFiltrosDescarga() {
        parametros.put("imprimirOriginal", obtenerConfigDescarga("IMPRIMIR_ORIGINAL_FACTURA"));
        parametros.put("imprimirDuplicado", obtenerConfigDescarga("IMPRIMIR_DUPLICADO_FACTURA"));
        parametros.put("imprimirTriplicado", obtenerConfigDescarga("IMPRIMIR_TRIPLICADO_FACTURA"));
    }

     public void obtenerConfigSiediApi() {
        configuracionValorFilter = new ConfiguracionValor();
        adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("ARCHIVOS_DTE_SIEDI_API");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            siediApiKude = false;
        } else {
            siediApiKude = adapterConfigValor.getData().get(0).getValor().equalsIgnoreCase("S"); 
        }
    }
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idAutofactura = params.get("id");

        this.serviceFactura = new AutoFacturaService();
        this.facturaDetalle = new AutoFacturaPojo();

        this.fileServiceClient = new FileServiceClient();

        this.adapterDetalle = new AutoFacturaDetallePojoAdapter();
        this.facturaDetalleFilter = new AutoFacturaDetallePojo();

        this.parametros = new HashMap();
        completarFiltrosDescarga();

        obtenerFacturaDetalle();
        obtenerConfigSiediApi();
    }

}
