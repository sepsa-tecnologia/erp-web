package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO cargo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Cargo {

    /**
     * Identificador cargo
     */
    private Integer id;
    /**
     * Descripcion cargo
     */
    private String descripcion;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Constructor
     */
    public Cargo() {

    }

    /**
     * Constructor del tipo cargo
     *
     * @param id
     * @param descripcion
     */
    public Cargo(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

}
