package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoProcesamientoService;

/**
 * Adaptador de la lista de factura procesamiento
 *
 * @author Romina Núñez
 */
public class NotaCreditoProcesamientoAdapter extends DataListAdapter<DocumentoProcesamiento> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final NotaCreditoProcesamientoService serviceNC;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoProcesamientoAdapter fillData(DocumentoProcesamiento searchData) {

        return serviceNC.getNCProcesamientoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoProcesamientoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNC = new NotaCreditoProcesamientoService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public NotaCreditoProcesamientoAdapter() {
        super();
        this.serviceNC = new NotaCreditoProcesamientoService();
    }
}
