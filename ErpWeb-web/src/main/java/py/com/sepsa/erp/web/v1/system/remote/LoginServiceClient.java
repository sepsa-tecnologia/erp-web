package py.com.sepsa.erp.web.v1.system.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.system.pojos.Token;
import py.com.sepsa.erp.web.v1.system.pojos.Credential;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.system.pojos.UsuarioEmpresa;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Cliente para los servicios de login
 *
 * @author Daniel F. Escauriza Arza
 */
public class LoginServiceClient extends APIErpCore {

    /**
     * Método ping
     *
     * @return Respuesta HTTP
     */
    public String ping() {

        String pong = null;

        HttpURLConnection conn = GET(Resource.PING.url, ContentType.JSON,
                null);

        if (conn != null) {

            BodyResponse response = BodyResponse.createInstance(conn,
                    String.class);

            pong = (String) response.getPayload();

            conn.disconnect();
        }

        return pong;
    }

    /**
     * Método para autenticación de usuario
     *
     * @param credential Credencial de usuario
     * @return Respuesta HTTP
     */
    public BodyResponse login(Credential credential) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.LOGIN.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(credential));

            response = BodyResponse.createInstance(conn, Token.class);

            conn.disconnect();
        }

        return response;
    }

    /**
     * Método para autenticación de usuario
     *
     * @param token token asociado a usuario
     * @return Respuesta HTTP
     */
    public Usuario userInfo() {
        Usuario userInfo = new Usuario();

        Map params = new HashMap<>();

        HttpURLConnection conn = GET(LoginServiceClient.Resource.USER_INFO.url,
                ContentType.JSON, params);

        if (conn != null) {

            BodyResponse response = BodyResponse.createInstance(conn, Usuario.class);
            userInfo = (Usuario) response.getPayload();

            conn.disconnect();
        }

        return userInfo;
    }

    /**
     * Método para obtener el token asociado a la empresa
     *
     * @param idEmpresa 
     * @return Respuesta HTTP
     */
    public BodyResponse tokenEmpresa(Integer idEmpresa) {

        BodyResponse response = new BodyResponse();
        UsuarioEmpresa usuarioEmpresa = new UsuarioEmpresa();
        usuarioEmpresa.setIdEmpresa(idEmpresa);

        HttpURLConnection conn = POST(Resource.TOKEN_EMPRESA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(usuarioEmpresa));

            response = BodyResponse.createInstance(conn, Token.class);

            conn.disconnect();
        }

        return response;
    }

    /**
     * Constructor de LoginClient
     */
    public LoginServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LOGIN("Ingreso al sistema", "usuario/login"),
        TOKEN_EMPRESA("Token asociado a empresa", "usuario/token-empresa"),
        USER_INFO("Servicios para usuario info", "usuario/info"),
        PING("Servicio de ping", "ping"),
        PERMISOS("Servicio para Obtener Permisos","usuario-perfil");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
