
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.DatoPersona;
import py.com.sepsa.erp.web.v1.info.remote.DatoPersonaServiceClient;

/**
 * Adaptador para la lista dato persona
 * @author Cristina Insfrán
 */
public class DatoPersonaListAdapter extends DataListAdapter<DatoPersona>{
    
     /**
     * Cliente para los servicios de dato persona
     */
    private final DatoPersonaServiceClient datoPersonaClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public DatoPersonaListAdapter fillData(DatoPersona searchData) {
     
        return datoPersonaClient.getDatoPersonaList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de DatoPersonaListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public DatoPersonaListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.datoPersonaClient = new DatoPersonaServiceClient();
    }

    /**
     * Constructor de DatoPersonaListAdapter
     */
    public DatoPersonaListAdapter() {
        super();
        this.datoPersonaClient = new DatoPersonaServiceClient();
    }
}
