/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.TalonarioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para la vista de Crear Talonario
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("talonarioCreate")
public class TalonarioCreateController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final TalonarioService service;
    /**
     * POJO del talonario
     */
    private Talonario talonario;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private TipoDocumentoAdapter tipoDocumentoAdapterList;
    /**
     * POJO de tipo documento
     */
    private TipoDocumento tipoDocumentoFilter;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de tipo documento
     */
    private Local localFilter;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapter;
    /**
     * POJO Encargado
     */
    private Usuario encargado;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    /**
     * @return the talonario
     */
    public Talonario getTalonario() {
        return talonario;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    /**
     * @param talonario the talonario to set
     */
    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public void setAdapter(UserListAdapter adapter) {
        this.adapter = adapter;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public UserListAdapter getAdapter() {
        return adapter;
    }

    /**
     * @return the tipoDocumentoAdapterList
     */
    public TipoDocumentoAdapter getTipoDocumentoAdapterList() {
        return tipoDocumentoAdapterList;
    }

    /**
     * @param tipoDocumentoAdapterList the tipoDocumentoAdapterList to set
     */
    public void setTipoDocumentoAdapterList(TipoDocumentoAdapter tipoDocumentoAdapterList) {
        this.tipoDocumentoAdapterList = tipoDocumentoAdapterList;
    }

    /**
     * @return the tipoDocumentoFilter
     */
    public TipoDocumento getTipoDocumentoFilter() {
        return tipoDocumentoFilter;
    }

    /**
     * @param tipoDocumentoFilter the tipoDocumentoFilter to set
     */
    public void setTipoDocumentoFilter(TipoDocumento tipoDocumentoFilter) {
        this.tipoDocumentoFilter = tipoDocumentoFilter;
    }

    /**
     * @return the localAdapterList
     */
    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    /**
     * @param localAdapterList the localAdapterList to set
     */
    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    /**
     * @return the localFilter
     */
    public Local getLocalFilter() {
        return localFilter;
    }

    /**
     * @param localFilter the localFilter to set
     */
    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    //</editor-fold>
    /**
     * Método para crear talonario
     */
    public void create() {

        BodyResponse<Talonario> respuestaTal = service.setTalonario(talonario);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Talonario creado correctamente!"));
            this.clear();
        }

    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQuery(String query) {

        Usuario encargado = new Usuario();
        encargado.setTienePersonaFisica(true);
        encargado.setNombrePersonaFisica(query);

        adapter = adapter.fillData(encargado);

        return adapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectEncargadoFilter(SelectEvent event) {
        encargado.setId(((Usuario) event.getObject()).getId());
        encargado.setIdPersonaFisica(((Usuario) event.getObject()).getIdPersonaFisica());
        this.talonario.setIdPersona(encargado.getId());
        this.talonario.setIdEncargado(encargado.getIdPersonaFisica());
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoDocumento() {
        this.setTipoDocumentoAdapterList(getTipoDocumentoAdapterList().fillData(getTipoDocumentoFilter()));
    }

    /**
     * Método para obtener local
     */
    public void getLocales() {
        this.setLocalAdapterList(getLocalAdapterList().fillData(getLocalFilter()));
    }

    public void clear() {
        this.talonario = new Talonario();
        this.localFilter = new Local();
        this.tipoDocumentoFilter = new TipoDocumento();
    }

    public void filterEncargados() {
        encargado.setTienePersonaFisica(true);
        adapter = adapter.fillData(encargado);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        talonario.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion(String configuracion) {
        configuracionValorFilter.setCodigoConfiguracion(configuracion);
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
        talonario.setDigital(adapterConfigValor.getData().get(0).getValor());
    }

    public void onChangeTipoTalonario() {
        TipoDocumentoAdapter tipoDocumentoAdapter = new TipoDocumentoAdapter();
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(talonario.getIdTipoDocumento());
        tipoDocumentoAdapter = tipoDocumentoAdapter.fillData(tipoDocumento);
        String cod = tipoDocumentoAdapter.getData().get(0).getCodigo();
        if (cod.equalsIgnoreCase("FACTURA")) {
            obtenerConfiguracion("TIPO_TALONARIO_FACTURA_DIGITAL");
        }

        if (cod.equalsIgnoreCase("RECIBO")) {
            obtenerConfiguracion("TIPO_TALONARIO_RECIBO_DIGITAL");
        }

        if (cod.equalsIgnoreCase("NOTA_CREDITO")) {
            obtenerConfiguracion("TIPO_TALONARIO_NC_DIGITAL");
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.talonario = new Talonario();
        this.adapter = new UserListAdapter();
        this.encargado = new Usuario();
        this.tipoDocumentoAdapterList = new TipoDocumentoAdapter();
        this.tipoDocumentoFilter = new TipoDocumento();
        this.localAdapterList = new LocalListAdapter();
        this.localFilter = new Local();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();

        getLocales();
        getTipoDocumento();
        filterEncargados();
    }

    /**
     * Constructor
     */
    public TalonarioCreateController() {
        init();
        this.service = new TalonarioService();
    }
}
