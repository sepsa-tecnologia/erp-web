
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 *
 * @author Romina
 */
public class DetalleProcesamiento {
    
    /**
     * Identificador
     */
    private Integer id;
    /**
     * Codigo Resultado
     */
    private Integer codigoResultado;
    /**
     * Mensaje Resultado
     */
    private String mensajeResultado;
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigoResultado() {
        return codigoResultado;
    }

    public void setCodigoResultado(Integer codigoResultado) {
        this.codigoResultado = codigoResultado;
    }

    public String getMensajeResultado() {
        return mensajeResultado;
    }

    public void setMensajeResultado(String mensajeResultado) {
        this.mensajeResultado = mensajeResultado;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public DetalleProcesamiento() {
    }

    
    
}
