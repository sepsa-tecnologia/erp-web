/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.admin.web.v1.controllers.user;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;

/**
 * Controlador para usuario perfil
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("userperfil")
public class UsuarioPerfilController implements Serializable{
    
    
    /**
     * Método que cambia la asociacion de usuario perfil
     * @param event 
     */
      public void confirmEditUserPerfil(RowEditEvent event) {
          
      }
      /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        
    }
}
