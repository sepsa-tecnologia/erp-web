
package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;

import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfil;

/**
 * Filtro utilizado para usuario perfil
 * @author Cristina Insfrán
 */
public class UsuarioPerfilFilter extends Filter{
    
    /**
     * Agrega el filtro de identificador del usuario
     *
     * @param idUsuario Identificador del usuario
     * @return
     */
    public UsuarioPerfilFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

     /**
     * Agrega el filtro de identificador del perfil
     *
     * @param idPerfil Identificador del usuario
     * @return
     */
    public UsuarioPerfilFilter idPerfil(Integer idPerfil) {
        if (idPerfil != null) {
            params.put("idPerfil", idPerfil);
        }
        return this;
    }
    
      /**
     * Agrega el filtro del estado
     *
     * @param estado
     * @return
     */
    public UsuarioPerfilFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }
    
    
     /**
     * Construye el mapa de parametros
     *
     * @param usuarioperfil
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(UsuarioPerfil usuarioperfil, Integer page, Integer pageSize) {
      
        UsuarioPerfilFilter filter = new UsuarioPerfilFilter();
        
        filter
                .idUsuario(usuarioperfil.getIdUsuario())
                .idPerfil(usuarioperfil.getIdPerfil())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de UsuarioPerfilFilter
     */
    public UsuarioPerfilFilter() {
        super();
    }
    
}
