package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioResumenAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.ControlInventarioService;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para Control de Inventario
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("controlCreate")
public class ControlCreateController implements Serializable {

    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoAdapterList;

    /**
     * POJO de MotivoEmisionInterno
     */
    private Motivo motivoFilter;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioCreate;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterEncargado;
    /**
     * POJO Encargado
     */
    private Usuario encargado;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterSupervisor;
    /**
     * POJO Encargado
     */
    private Usuario supervisor;
    /**
     * Service control inventario
     */
    private ControlInventarioService service;
    /**
     * Objeto depositoLogistico
     */
    private DepositoLogistico deposito;

    /**
     * Adaptador de la lista de depositoLogistico
     */
    private DepositoLogisticoAdapter depositoAdapter;
    /**
     * Adaptador para la lista de inventario
     */
    private InventarioResumenAdapter inventarioResumenAdapter;

    /**
     * Pojo de inventario
     */
    private InventarioResumen inventarioResumen;
    /**
     * Lista para cobro
     */
    private List<InventarioResumen> selectedProductos;
    /**
     * Lista para cobro
     */
    private List<DepositoLogistico> selectedDepositos;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;
    /**
     * POJO de tipo documento
     */
    private Local localFilter;

    //<editor-fold defaultstate="collapsed" desc="GETTERS&SETTERS">
    public MotivoAdapter getMotivoAdapterList() {
        return motivoAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setSelectedDepositos(List<DepositoLogistico> selectedDepositos) {
        this.selectedDepositos = selectedDepositos;
    }

    public List<DepositoLogistico> getSelectedDepositos() {
        return selectedDepositos;
    }

    public void setMotivoAdapterList(MotivoAdapter motivoAdapterList) {
        this.motivoAdapterList = motivoAdapterList;
    }

    public Motivo getMotivoFilter() {
        return motivoFilter;
    }

    public void setMotivoFilter(Motivo motivoFilter) {
        this.motivoFilter = motivoFilter;
    }

    public ControlInventario getControlInventarioCreate() {
        return controlInventarioCreate;
    }

    public void setControlInventarioCreate(ControlInventario controlInventarioCreate) {
        this.controlInventarioCreate = controlInventarioCreate;
    }

    public UserListAdapter getAdapterEncargado() {
        return adapterEncargado;
    }

    public void setAdapterEncargado(UserListAdapter adapterEncargado) {
        this.adapterEncargado = adapterEncargado;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public UserListAdapter getAdapterSupervisor() {
        return adapterSupervisor;
    }

    public void setAdapterSupervisor(UserListAdapter adapterSupervisor) {
        this.adapterSupervisor = adapterSupervisor;
    }

    public Usuario getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Usuario supervisor) {
        this.supervisor = supervisor;
    }

    public ControlInventarioService getService() {
        return service;
    }

    public void setService(ControlInventarioService service) {
        this.service = service;
    }

    public DepositoLogistico getDeposito() {
        return deposito;
    }

    public void setDeposito(DepositoLogistico deposito) {
        this.deposito = deposito;
    }

    public DepositoLogisticoAdapter getDepositoAdapter() {
        return depositoAdapter;
    }

    public void setDepositoAdapter(DepositoLogisticoAdapter depositoAdapter) {
        this.depositoAdapter = depositoAdapter;
    }

    public InventarioResumenAdapter getInventarioResumenAdapter() {
        return inventarioResumenAdapter;
    }

    public void setInventarioResumenAdapter(InventarioResumenAdapter inventarioResumenAdapter) {
        this.inventarioResumenAdapter = inventarioResumenAdapter;
    }

    public InventarioResumen getInventarioResumen() {
        return inventarioResumen;
    }

    public void setInventarioResumen(InventarioResumen inventarioResumen) {
        this.inventarioResumen = inventarioResumen;
    }

    public List<InventarioResumen> getSelectedProductos() {
        return selectedProductos;
    }

    public void setSelectedProductos(List<InventarioResumen> selectedProductos) {
        this.selectedProductos = selectedProductos;
    }
//</editor-fold>

    /**
     * Método para obtener motivoEmisions
     */
    public void getMotivos() {
        getMotivoAdapterList().setFirstResult(0);
        this.motivoAdapterList = this.motivoAdapterList.fillData(motivoFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioEncargado(String query) {

        Usuario encargado = new Usuario();
        encargado.setTienePersonaFisica(true);
        encargado.setNombrePersonaFisica(query);

        adapterEncargado = adapterEncargado.fillData(encargado);

        return adapterEncargado.getData();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioSupervisor(String query) {

        Usuario supervisor = new Usuario();
        supervisor.setTienePersonaFisica(true);
        supervisor.setNombrePersonaFisica(query);

        adapterSupervisor = adapterSupervisor.fillData(supervisor);

        return adapterSupervisor.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectEncargadoFilter(SelectEvent event) {
        encargado.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioCreate.setIdUsuarioEncargado(encargado.getId());
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectSupervisorFilter(SelectEvent event) {
        supervisor.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioCreate.setIdUsuarioSupervisor(supervisor.getId());
    }

    /**
     * Método para crear
     */
    public void create() {
        this.controlInventarioCreate.setCodigoEstado("PENDIENTE");
        List<Integer> idDepositoLogisticos = new ArrayList<>();
        List<Integer> idProductos = new ArrayList<>();

        if (!selectedDepositos.isEmpty() && selectedDepositos.size() == 1) {
            controlInventarioCreate.setIdDepositoLogistico(selectedDepositos.get(0).getId());
        } else if (!selectedDepositos.isEmpty() && selectedDepositos.size() > 1) {
            for (DepositoLogistico selectedDep : selectedDepositos) {
                idDepositoLogisticos.add(selectedDep.getId());
                controlInventarioCreate.setIdDepositoLogisticos(idDepositoLogisticos);
            }
        }

        if (!selectedProductos.isEmpty() && selectedProductos.size() == 1) {
            controlInventarioCreate.setIdProducto(selectedProductos.get(0).getId());
        } else if (!selectedProductos.isEmpty() && selectedProductos.size() > 1) {
            for (InventarioResumen selectedProd : selectedProductos) {
                idProductos.add(selectedProd.getIdProducto());
                controlInventarioCreate.setIdProductos(idProductos);
            }
        }
        BodyResponse<ControlInventario> respuestaDep = service.setControlInventario(controlInventarioCreate);

        if (respuestaDep.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Control de Inventario creado correctamente!"));

            clear();
        }

    }

    /**
     * Método para limpiar formulario
     */
    public void clear() {
        this.controlInventarioCreate = new ControlInventario();
        this.deposito = new DepositoLogistico();
        this.supervisor = new Usuario();
        this.encargado = new Usuario();
        this.selectedDepositos = new ArrayList<>();
        this.selectedProductos = new ArrayList<>();
        PF.current().ajax().update(":list-control-inventario-form:form-data:supervisor");
        PF.current().ajax().update(":list-control-inventario-form:form-data:encargado");
    }

    /**
     * Método para limpiar formulario
     */
    public void clearDeposito() {
        this.deposito = new DepositoLogistico();
        filterDepositoLogistico();
        PF.current().ajax().update(":list-control-create-form:form-data:filter:local");
    }

    /**
     * Método para limpiar formulario
     */
    public void clearResumen() {
        this.inventarioResumen = new InventarioResumen();
        filterResumenInventario();
    }

    /**
     * Método para filtrar resumen de inventario
     */
    public void filterResumenInventario() {
        getInventarioResumenAdapter().setFirstResult(0);
        this.inventarioResumen.setListadoPojo(Boolean.TRUE);
        this.inventarioResumenAdapter = this.inventarioResumenAdapter.fillData(inventarioResumen);

        for (int i = 0; i < inventarioResumenAdapter.getData().size(); i++) {
            inventarioResumenAdapter.getData().get(i).setId(i + 1);
        }
    }

    /**
     * Método para filtrar depósito logístico
     */
    public void filterDepositoLogistico() {
        getDepositoAdapter().setFirstResult(0);
        this.deposito.setListadoPojo(Boolean.TRUE);
        this.depositoAdapter = this.depositoAdapter.fillData(deposito);

    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        deposito.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.motivoFilter = new Motivo();
        this.motivoAdapterList = new MotivoAdapter();

        this.controlInventarioCreate = new ControlInventario();
        this.service = new ControlInventarioService();

        this.adapterEncargado = new UserListAdapter();
        this.adapterSupervisor = new UserListAdapter();
        this.encargado = new Usuario();
        this.supervisor = new Usuario();

        this.deposito = new DepositoLogistico();
        this.depositoAdapter = new DepositoLogisticoAdapter();

        this.inventarioResumen = new InventarioResumen();
        this.inventarioResumenAdapter = new InventarioResumenAdapter();

        this.selectedProductos = new ArrayList<>();
        this.selectedDepositos = new ArrayList<>();

        this.localAdapterList = new LocalListAdapter();
        this.localFilter = new Local();

        filterResumenInventario();
        filterDepositoLogistico();
        getMotivos();
    }

}
