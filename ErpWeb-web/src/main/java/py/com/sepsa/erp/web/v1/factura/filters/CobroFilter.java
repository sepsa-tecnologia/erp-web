package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;

/**
 * Filtro utilizado para el servicio de Cobro
 *
 * @author Romina Núñez
 */
public class CobroFilter extends Filter {

    /**
     * Agrega el filtro de identificador de cobro
     *
     * @param id
     * @return
     */
    public CobroFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de lugar de cobro
     *
     * @param idLugarCobro
     * @return
     */
    public CobroFilter idLugarCobro(Integer idLugarCobro) {
        if (idLugarCobro != null) {
            params.put("idLugarCobro", idLugarCobro);
        }
        return this;
    }

    /**
     * Agregar el filtro de lugar cobro
     *
     * @param lugarCobro
     * @return
     */
    /*public CobroFilter lugarCobro(String lugarCobro) {
        if (lugarCobro != null && !lugarCobro.trim().isEmpty()) {
            params.put("lugarCobro", lugarCobro);
        }
        return this;
    }*/

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public CobroFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public CobroFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public CobroFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro de monto cobro
     *
     * @param montoCobro
     * @return
     */
    public CobroFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro recibo
     *
     * @param nroRecibo
     * @return
     */
    public CobroFilter nroRecibo(String nroRecibo) {
        if (nroRecibo != null && !nroRecibo.trim().isEmpty()) {
            params.put("nroRecibo", nroRecibo);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de talonario
     *
     * @param idTalonario
     * @return
     */
    public CobroFilter idTalonario(Integer idTalonario) {
        if (idTalonario != null) {
            params.put("idTalonario", idTalonario);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public CobroFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idTalonario", idFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de estado
     *
     * @param idEstado
     * @return
     */
    public CobroFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public CobroFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param cobro datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Cobro cobro, Integer page, Integer pageSize) {
        CobroFilter filter = new CobroFilter();

        filter
                .id(cobro.getId())
                .idLugarCobro(cobro.getIdLugarCobro())
                .idFactura(cobro.getIdFactura())
                .fecha(cobro.getFecha())
                .fechaDesde(cobro.getFechaDesde())
                .fechaHasta(cobro.getFechaHasta())
                .montoCobro(cobro.getMontoCobro())
                .nroRecibo(cobro.getNroRecibo())
                .idTalonario(cobro.getIdTalonario())
                .idEstado(cobro.getIdEstado())
                .digital(cobro.getDigital())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de CobroFilter
     */
    public CobroFilter() {
        super();
    }
}
