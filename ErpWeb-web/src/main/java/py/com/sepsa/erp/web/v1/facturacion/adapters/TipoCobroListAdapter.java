
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;

/**
 *
 * @author Cristina Insfrán
 */
public class TipoCobroListAdapter extends DataListAdapter<TipoCobro>{
    
    /**
     * Cliente remoto para tipo cobro
     */
    private final CobroService cobroClient;
     /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoCobroListAdapter fillData(TipoCobro searchData) {

        return cobroClient.getTipoCobroList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoCobroListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.cobroClient = new CobroService();
    }

    /**
     * Constructor de TipoCobroListAdapter
     */
    public TipoCobroListAdapter() {
        super();
        this.cobroClient = new CobroService();
    }
}
