package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;
import py.com.sepsa.erp.web.v1.comercial.remote.ServicioService;

/**
 * Adaptador para lista de servicios.
 *
 * @author alext
 */
public class ServicioAdapter extends DataListAdapter<Servicios> {

    /**
     * Servicio de clientes según servicios.
     */
    private final ServicioService serviciosService;

    /**
     * Método para cargar la lista de servicios.
     *
     * @param searchData
     * @return
     */
    @Override
    public ServicioAdapter fillData(Servicios searchData) {
        return serviciosService.getServiciosList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ServiciosAdapter.
     *
     * @param page
     * @param pageSize
     */
    public ServicioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviciosService = new ServicioService();
    }

    /**
     * Constructor de ServiciosAdapter.
     */
    public ServicioAdapter() {
        super();
        this.serviciosService = new ServicioService();
    }
}
