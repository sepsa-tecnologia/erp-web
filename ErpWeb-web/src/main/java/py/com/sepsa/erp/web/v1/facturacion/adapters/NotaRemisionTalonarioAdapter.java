package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class NotaRemisionTalonarioAdapter extends DataListAdapter<TalonarioPojo> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final NotaRemisionService serviceNR;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaRemisionTalonarioAdapter fillData(TalonarioPojo searchData) {

        return serviceNR.getNRTalonarioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaRemisionTalonarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNR = new NotaRemisionService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public NotaRemisionTalonarioAdapter() {
        super();
        this.serviceNR = new NotaRemisionService();
    }
}
 