/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TipoCambioFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio tipo cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoCambioService extends APIErpFacturacion {

    /**
     * Obtiene la lista
     *
     * @param tipoCambio Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoCambioAdapter getTipoCambio(TipoCambio tipoCambio, Integer page,
            Integer pageSize) {

        TipoCambioAdapter lista = new TipoCambioAdapter();

        Map params = TipoCambioFilter.build(tipoCambio, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_CAMBIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoCambioAdapter.class);

            lista = (TipoCambioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param tipoCambio
     * @return
     */
    public BodyResponse<TipoCambio> setTipoCambios(TipoCambio tipoCambio) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.TIPO_CAMBIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(tipoCambio));
            response = BodyResponse.createInstance(conn, TipoCambio.class);
        }
        return response;
    }
    
    /**
     * Método para crear
     *
     * @param tipoCambio
     * @return
     */
    public BodyResponse<TipoCambio> actualizarTipoCambio(TipoCambio tipoCambio) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.ACTUALIZAR_TIPO_CAMBIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(tipoCambio));
            response = BodyResponse.createInstance(conn, TipoCambio.class);
        }
        return response;
    }

    /**
     * Método para editar tipo cambio
     *
     * @param tipoCambio
     * @return
     */
    public Integer editTipoCambio(TipoCambio tipoCambio) {

        Integer idUsuario = null;
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.TIPO_CAMBIO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(tipoCambio));

            response = BodyResponse.createInstance(conn, TipoCambio.class);

            idUsuario = ((TipoCambio) response.getPayload()).getId();

        }
        return idUsuario;
    }

    /**
     * Constructor de clase
     */
    public TipoCambioService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_CAMBIO("tipo cambio", "tipo-cambio"),
        ACTUALIZAR_TIPO_CAMBIO("tipo cambio", "tipo-cambio/actualizar-tipo-cambio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
