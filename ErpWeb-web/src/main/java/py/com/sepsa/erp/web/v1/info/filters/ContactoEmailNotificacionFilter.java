package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Contacto;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmailNotificacion;

/**
 * Filtro utilizado para el servicio de contacto email
 *
 * @author Romina Núñez
 */
public class ContactoEmailNotificacionFilter extends Filter {

    /**
     * Agrega el filtro de identificador del Contacto
     *
     * @param idContacto Identificador del Contacto
     * @return
     */
    public ContactoEmailNotificacionFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de contacto
     *
     * @param contacto
     * @return
     */
    public ContactoEmailNotificacionFilter contacto(String contacto) {
        if (contacto != null && !contacto.trim().isEmpty()) {
            params.put("contacto", contacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del email
     *
     * @param idEmail
     * @return
     */
    public ContactoEmailNotificacionFilter idEmail(Integer idEmail) {
        if (idEmail != null) {
            params.put("idEmail", idEmail);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de email
     *
     * @param email
     * @return
     */
    public ContactoEmailNotificacionFilter email(String email) {
        if (email != null && !email.trim().isEmpty()) {
            params.put("email", email.trim());
        }
        return this;
    }
    
     /**
     * Agrega el filtro de identificador del telefono
     *
     * @param idTipoNotificacion
     * @return
     */
    public ContactoEmailNotificacionFilter idTipoNotificacion(Integer idTipoNotificacion) {
        if (idTipoNotificacion != null) {
            params.put("idTipoNotificacion", idTipoNotificacion);
        }
        return this;
    }

    /**
     * Agrega el filtro tipo notificación
     *
     * @param tipoNotificacion
     * @return
     */
    public ContactoEmailNotificacionFilter tipoNotificacion(String tipoNotificacion) {
        if (tipoNotificacion != null && !tipoNotificacion.trim().isEmpty()) {
            params.put("tipoNotificacion", tipoNotificacion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro estado
     *
     * @param estado
     * @return
     */
    public ContactoEmailNotificacionFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contactoEmailNotificacion 
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ContactoEmailNotificacion contactoEmailNotificacion, Integer page, Integer pageSize) {
        ContactoEmailNotificacionFilter filter = new ContactoEmailNotificacionFilter();

        filter
                .idContacto(contactoEmailNotificacion.getIdContacto())
                .contacto(contactoEmailNotificacion.getContacto())
                .idEmail(contactoEmailNotificacion.getIdEmail())
                .email(contactoEmailNotificacion.getEmail())
                .idTipoNotificacion(contactoEmailNotificacion.getIdTipoNotificacion())
                .tipoNotificacion(contactoEmailNotificacion.getTipoNotificacion())
                .estado(contactoEmailNotificacion.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ContactoEmailFilter
     */
    public ContactoEmailNotificacionFilter() {
        super();
    }
}
