package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProveedoresOperativosPorCompradorAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaRolAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProveedoresOperativosPorComprador;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;

/**
 * Controlador para proveedores operativos por comprador.
 *
 * @author alext
 */
@ViewScoped
@Named("provOperPorComp")
public class ProveedoresOperativosPorCompradorController implements Serializable {

    private final String ano;

    private final String mes;
    
    private Integer idComprador;

    /**
     * Adaptador de ProveedoresOperativosPorCompradorAdapter.
     */
    private ProveedoresOperativosPorCompradorAdapter adapter;

    /**
     * Adaptador de Persona Rol.
     */
    private PersonaRolAdapter adapterPersonaRol;

    /**
     * Persona Rol.
     */
    private PersonaRol personaRol;

    /**
     * Objeto ProveedoresOperativosPorComprador.
     */
    private ProveedoresOperativosPorComprador searchData;

    /**
     * Cantidad de filas en la tabla.
     */
    Integer rows = 10;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public ProveedoresOperativosPorCompradorAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(ProveedoresOperativosPorCompradorAdapter adapter) {
        this.adapter = adapter;
    }

    public ProveedoresOperativosPorComprador getSearchData() {
        return searchData;
    }

    public void setSearchData(ProveedoresOperativosPorComprador searchData) {
        this.searchData = searchData;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public PersonaRolAdapter getAdapterPersonaRol() {
        return adapterPersonaRol;
    }

    public void setAdapterPersonaRol(PersonaRolAdapter adapterPersonaRol) {
        this.adapterPersonaRol = adapterPersonaRol;
    }

    public PersonaRol getPersonaRol() {
        return personaRol;
    }

    public void setPersonaRol(PersonaRol personaRol) {
        this.personaRol = personaRol;
    }
    
    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }
    //</editor-fold>

    /**
     * Método para generar lista de proveedores.
     */
    public void search() {
        adapter = new ProveedoresOperativosPorCompradorAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new ProveedoresOperativosPorComprador(idComprador, ano, mes);
        search();
    }

    /**
     * Método para filtrar comprador.
     */
    public void filterComprador() {
        personaRol.setIdRol(1);
        adapterPersonaRol = adapterPersonaRol.fillData(personaRol);
    }

    /**
     * Actualiza la lista de proveedores..
     */
    public void loadProvOperPorComp() {
        adapter.setPageSize(rows);
        adapter = adapter.fillData(searchData);
    }

    @PostConstruct
    public void init() {
        this.adapter = new ProveedoresOperativosPorCompradorAdapter();
        this.searchData = new ProveedoresOperativosPorComprador();
        this.adapterPersonaRol = new PersonaRolAdapter();
        this.personaRol = new PersonaRol();
        filterComprador();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public ProveedoresOperativosPorCompradorController() {
        Calendar now = Calendar.getInstance();
        int m = now.get(Calendar.MONTH) + 1;
        int a = now.get(Calendar.YEAR);
        String mes = String.valueOf(m);
        mes = mes.length() == 1 ? "0" + mes : mes;
        this.ano = String.valueOf(a);
        this.mes = mes;
    }

}
