/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;

/**
 * Pojo para motivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Motivo {

    /**
     * Id
     */
    private Integer id;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * Activo
     */
    private String activo;
    /**
     * Codigo tipo motivo
     */
    private String codigoTipoMotivo;
    /**
     * Tipo de Motivo
     */
    private TipoMotivo tipoMotivo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoMotivo getTipoMotivo() {
        return tipoMotivo;
    }

    public void setTipoMotivo(TipoMotivo tipoMotivo) {
        this.tipoMotivo = tipoMotivo;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getCodigoTipoMotivo() {
        return codigoTipoMotivo;
    }

    public void setCodigoTipoMotivo(String codigoTipoMotivo) {
        this.codigoTipoMotivo = codigoTipoMotivo;
    }

    /**
     * Constructor de la clase
     */
    public Motivo() {

    }

    /**
     * Constructor de la clase
     */
    public Motivo(Integer id) {
        this.id = id;
    }

}
