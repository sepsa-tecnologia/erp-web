package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.facturacion.remote.EntidadFinancieraServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;

/**
 * Convertidor para datos de autocomplete
 *
 * @author alext
 */
@FacesConverter("EntidadFinancieraPojoConverter")
public class EntidadFinancieraPojoConverter implements Converter {

    /**
     * Servicios para login al sistema
     */
    private EntidadFinancieraServiceClient financieraClient;

    @Override
    public EntidadFinanciera getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            financieraClient = new EntidadFinancieraServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            EntidadFinanciera per = new EntidadFinanciera();
            per.setId(val);

            List<EntidadFinanciera> entidad
                    = financieraClient.getEntidadFinancieraList(per, 0, 10).getData();
            if (entidad != null && !entidad.isEmpty()) {
                return entidad.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es una entidad válida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof EntidadFinanciera) {
                return String.valueOf(((EntidadFinanciera) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
