/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;

/**
 * Filter para tipo operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoOperacionFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public TipoOperacionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public TipoOperacionFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoOperacionFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro activo
     *
     * @param activo
     * @return
     */
    public TipoOperacionFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param tipoOperacionFilter datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoOperacion tipoOperacionFilter, Integer page, Integer pageSize) {
        TipoOperacionFilter filter = new TipoOperacionFilter();

        filter
                .id(tipoOperacionFilter.getId())
                .codigo(tipoOperacionFilter.getCodigo())
                .descripcion(tipoOperacionFilter.getDescripcion())
                .activo(tipoOperacionFilter.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public TipoOperacionFilter() {
        super();
    }

}
