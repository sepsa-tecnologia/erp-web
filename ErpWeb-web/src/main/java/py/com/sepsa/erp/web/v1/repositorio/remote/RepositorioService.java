/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.repositorio.adapters.RepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.filters.RepositorioFilter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;

/**
 * Cliente para el servicio de repositorio
 *
 * @author Sepsa
 * @author Romina Núñez
 */
public class RepositorioService extends APIErpCore {

    /**
     * Lista de repositorios
     *
     * @param ret
     * @param page
     * @param pageSize
     * @return
     */
    public RepositorioAdapter getRepositorioList(Repositorio ret, Integer page, Integer pageSize) {

        RepositorioAdapter lista = new RepositorioAdapter();

        Map params = RepositorioFilter.build(ret, page, pageSize);

        HttpURLConnection conn = GET(Resource.REPOSITORIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    RepositorioAdapter.class);

            lista = (RepositorioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear Repositorio
     *
     * @param repositorio
     * @return
     */
    public BodyResponse<Repositorio> setRepositorio(Repositorio repositorio) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.REPOSITORIO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(repositorio));

            response = BodyResponse.createInstance(conn, Repositorio.class);

        }
        return response;
    }

    /**
     * Método para editar repositorio
     *
     * @param repositorio
     * @return
     */
    public BodyResponse<Repositorio> editRepositorio(Repositorio repositorio) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.REPOSITORIO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(repositorio));

            response = BodyResponse.createInstance(conn, Repositorio.class);


        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Repositorio get(Integer id) {
        Repositorio data = new Repositorio(id);
        RepositorioAdapter list = getRepositorioList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Recursos de conexión o servicios del APISepsaSet
     */
    public enum Resource {

        //Servicios
        REPOSITORIO("Servicio para Repositorio", "repositorio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
