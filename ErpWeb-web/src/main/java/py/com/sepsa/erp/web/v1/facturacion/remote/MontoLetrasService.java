package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de facturacion
 *
 * @author Romina Núñez
 */
public class MontoLetrasService extends APIErpFacturacion {

   /**
     * Obtiene el monto en letras
     *
     * @param montoLetras  Objeto
     * @return
     */
    public MontoLetras getMontoLetras(MontoLetras montoLetras) {

        MontoLetras ml = new MontoLetras();

        Map params = new HashMap<>();

        params.put("codigoMoneda", montoLetras.getCodigoMoneda());
        params.put("monto", montoLetras.getMonto());

        HttpURLConnection conn = GET(Resource.MONTO_LETRAS.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MontoLetras.class);

            ml = (MontoLetras) response.getPayload();

            conn.disconnect();
        }
        return ml;
    }

    /**
     * Constructor de FacturacionService
     */
    public MontoLetrasService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        MONTO_LETRAS("MontoLetras", "factura/monto-letras");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
