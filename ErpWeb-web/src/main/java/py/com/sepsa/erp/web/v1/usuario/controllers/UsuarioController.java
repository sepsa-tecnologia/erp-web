package py.com.sepsa.erp.web.v1.usuario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.UsuarioLocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.UsuarioLocalRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.UsuarioLocalServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.ChangePass;
import py.com.sepsa.erp.web.v1.usuario.adapters.PerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioPerfilRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioPerfilServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioEmpresaListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioEmpresaRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.pojos.TipoUsuario;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioEmpresa;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfil;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioEmpresaServiceClient;

/**
 * Controlador para crear usuario, Listar y Editar.
 *
 * @author Cristina Insfrán & Romina Núñez , Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("user")
public class UsuarioController implements Serializable {

    private boolean close;
    /**
     * Objeto local
     */
    private Local localFilter;
    /**
     * Objeto de Sesión
     */
    @Inject
    private SessionData session;
    /**
     * Objeto Usuario Perfil
     */
    private UsuarioPerfil userProfileCreate;
    /**
     * Servicio Usuario Perfil
     */
    private UsuarioPerfilServiceClient serviceUserProfile;
    /**
     * Entidad perfil
     */
    private Perfil perfil;
    /**
     * Adaptador de la lista de perfiles
     */
    private PerfilListAdapter adapterPerfil;
    /**
     * Dato para el id del user creado
     */
    private Integer userCreate;
    /**
     * Datos del usuario
     */
    private Usuario user;

    /**
     * Datos del usuario
     */
    private Usuario usuario;
    /**
     * Datos de la persona
     */
    private Persona persona;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Datos del local
     */
    private Local local;
    /**
     * Datos del usuario local
     */
    private UsuarioLocal userLocal;
    /**
     * Lista de tipo de usuario
     */
    private List<TipoUsuario> tipoUsuario = new ArrayList<>();
    /**
     * Lista de tipo de usuario
     */
    private List<Perfil> perfilUsuarioSelected = new ArrayList<>();
    /**
     * Locales Seleccionados
     */
    private List<Local> selectLocal = new ArrayList<>();
    /**
     * Cliente para el servicio persona
     */
    private PersonaServiceClient personaClient;
    /**
     * Adaptador usuario local
     */
    private UsuarioLocalListAdapter adapterUsuarioLocal;
    /**
     * Cliente para el servicio de usuario
     */
    private UserServiceClient userClient;
    /**
     * Cliente para el servicio de usuario local
     */
    private UsuarioLocalServiceClient usuarioLocalClient;
    /**
     * Cliente para el servicio de local
     */
    private LocalService localClient;
    /**
     * Adaptador de la lista de Usuarios
     */
    private UserListAdapter adapter;
    /**
     * Adaptador de la lista Local
     */
    private LocalListAdapter adapterLocal;
    /**
     * Adaptador para la lista de mensajes del servidor
     */
    private MessageListAdapter messageAdapter;
    /**
     * Datos para cambiar contraseña
     */
    private ChangePass changePass;
    /**
     * cantidad de filas
     */
    private Integer rows = 10;
    /**
     * Lista de locales
     */
    private List<Local> locales;
    /**
     * Identificador del usuario Seleccionado
     */
    private Integer idUsuario;
    /**
     * Liste de locales asociados a usuario
     */
    private List<UsuarioLocal> selectedUsuarioLocal = new ArrayList();
    /**
     * Adaptador para la lista de personas
     */
    private PersonaListAdapter adapterPersona;
    /**
     * Objeto Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Usuario Perfil Relacionado
     */
    private UsuarioPerfilRelacionadoAdapter adapterUsuarioPerfilRelacionado;
    /**
     * POJO Usuario Perfil Relacionado
     */
    private UsuarioPerfilRelacionado usuarioPerfilRelacionadoFilter;
    /**
     * Adaptador para la lista de Usuario Local Relacionado
     */
    private UsuarioLocalRelacionadoAdapter adapterUsuarioLocalRelacionado;
    /**
     * POJO Usuario Local
     */
    private UsuarioLocal usuarioLocalFilter;
    /**
     * Email
     */
    private String email;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Lista para tipo de cobro
     */
    private List<SelectItem> listSoftware = new ArrayList<>();
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * POJO Cliente
     */
    private Cliente clienteUsuario;
    /**
     * Adaptador para la lista de local
     */
    private LocalListAdapter adapterLocalList;
    /**
     * POJO Local
     */
    private Local localUsuario;
    /**
     * Identificador de local para filtro
     */
    private Integer idLocal;
    /**
     * Identificador de Persona Usuario
     */
    private Integer idPersonaUsuario;
    /**
     * Idetificador de usuario
     */
    private Integer idUsuarioAsociar;
    /**
     * POJO Email
     */
    private Email emailUsuario;
    /**
     * POJO Telefono
     */
    private Telefono telUsuario;
    /**
     * Adaptador par la lista de estados
     */
    private EstadoAdapter estadoAdapter;
    /**
     * POJO Estado para filtros
     */
    private Estado estadoFilter;
    /**
     * nueva contraseña
     */
    private String nuevaContrasena;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete2;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter2;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete3;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter3;

    /**
     * Adaptador para la lista de Usuario Empresa Relacionado
     */
    private UsuarioEmpresaRelacionadoAdapter adapterUsuarioEmpresaRelacionado;
    /**
     * POJO Usuario para Usuario relacionado a empresa Relacionado
     */
    private UsuarioEmpresa usuarioEmpresaRelacionadoFilter;

    /**
     * Adaptador para la lista de Usuario Empresa Relacionado
     */
    private UsuarioEmpresaListAdapter adapterUsuarioEmpresa;
    /**
     * POJO Usuario para Usuario relacionado a empresa
     */
    private UsuarioEmpresa usuarioEmpresaFilter;
    /**
     * Cliente para el servicio de usuario
     */
    private UsuarioEmpresaServiceClient usuarioEmpresaClient;
    /**
     * Pojo para usuario empresa relacionamiento
     */
    private UsuarioEmpresa userEmpresaRelation;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public Integer getIdPersonaUsuario() {
        return idPersonaUsuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public Empresa getEmpresaAutocomplete2() {
        return empresaAutocomplete2;
    }

    public void setEmpresaAutocomplete2(Empresa empresaAutocomplete2) {
        this.empresaAutocomplete2 = empresaAutocomplete2;
    }

    public EmpresaAdapter getEmpresaAdapter2() {
        return empresaAdapter2;
    }

    public void setEmpresaAdapter2(EmpresaAdapter empresaAdapter2) {
        this.empresaAdapter2 = empresaAdapter2;
    }

    public Empresa getEmpresaAutocomplete3() {
        return empresaAutocomplete3;
    }

    public void setEmpresaAutocomplete3(Empresa empresaAutocomplete3) {
        this.empresaAutocomplete3 = empresaAutocomplete3;
    }

    public EmpresaAdapter getEmpresaAdapter3() {
        return empresaAdapter3;
    }

    public void setEmpresaAdapter3(EmpresaAdapter empresaAdapter3) {
        this.empresaAdapter3 = empresaAdapter3;
    }

    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    public Estado getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(Estado estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

    public UsuarioEmpresaListAdapter getAdapterUsuarioEmpresa() {
        return adapterUsuarioEmpresa;
    }

    public void setAdapterUsuarioEmpresa(UsuarioEmpresaListAdapter adapterUsuarioEmpresa) {
        this.adapterUsuarioEmpresa = adapterUsuarioEmpresa;
    }

    public UsuarioEmpresaRelacionadoAdapter getAdapterUsuarioEmpresaRelacionado() {
        return adapterUsuarioEmpresaRelacionado;
    }

    public void setAdapterUsuarioEmpresaRelacionado(UsuarioEmpresaRelacionadoAdapter adapterUsuarioEmpresaRelacionado) {
        this.adapterUsuarioEmpresaRelacionado = adapterUsuarioEmpresaRelacionado;
    }

    public UsuarioEmpresa getUsuarioEmpresaFilter() {
        return usuarioEmpresaFilter;
    }

    public void setUsuarioEmpresaFilter(UsuarioEmpresa usuarioEmpresaFilter) {
        this.usuarioEmpresaFilter = usuarioEmpresaFilter;
    }

    public UsuarioEmpresaServiceClient getUsuarioEmpresaClient() {
        return usuarioEmpresaClient;
    }

    public void setUsuarioEmpresaClient(UsuarioEmpresaServiceClient usuarioEmpresaClient) {
        this.usuarioEmpresaClient = usuarioEmpresaClient;
    }

    public SessionData getSession() {
        return session;
    }

    public void setSession(SessionData session) {
        this.session = session;
    }

    public Email getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario(Email emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public Telefono getTelUsuario() {
        return telUsuario;
    }

    public void setTelUsuario(Telefono telUsuario) {
        this.telUsuario = telUsuario;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public void setIdPersonaUsuario(Integer idPersonaUsuario) {
        this.idPersonaUsuario = idPersonaUsuario;
    }

    public Integer getIdUsuarioAsociar() {
        return idUsuarioAsociar;
    }

    public void setIdUsuarioAsociar(Integer idUsuarioAsociar) {
        this.idUsuarioAsociar = idUsuarioAsociar;
    }

    public UsuarioEmpresa getUsuarioEmpresaRelacionadoFilter() {
        return usuarioEmpresaRelacionadoFilter;
    }

    public void setUsuarioEmpresaRelacionadoFilter(UsuarioEmpresa usuarioEmpresaRelacionadoFilter) {
        this.usuarioEmpresaRelacionadoFilter = usuarioEmpresaRelacionadoFilter;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public LocalListAdapter getAdapterLocalList() {
        return adapterLocalList;
    }

    public void setAdapterLocalList(LocalListAdapter adapterLocalList) {
        this.adapterLocalList = adapterLocalList;
    }

    public Local getLocalUsuario() {
        return localUsuario;
    }

    public void setLocalUsuario(Local localUsuario) {
        this.localUsuario = localUsuario;
    }

    public void setClienteUsuario(Cliente clienteUsuario) {
        this.clienteUsuario = clienteUsuario;
    }

    public Cliente getClienteUsuario() {
        return clienteUsuario;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    /**
     * @return the listSoftware
     */
    public List<SelectItem> getListSoftware() {
        return listSoftware;
    }

    /**
     * @param listSoftware the listSoftware to set
     */
    public void setListSoftware(List<SelectItem> listSoftware) {
        this.listSoftware = listSoftware;
    }

    /**
     * @return the usuarioLocalClient
     */
    public UsuarioLocalServiceClient getUsuarioLocalClient() {
        return usuarioLocalClient;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setUsuarioLocalFilter(UsuarioLocal usuarioLocalFilter) {
        this.usuarioLocalFilter = usuarioLocalFilter;
    }

    public void setAdapterUsuarioLocalRelacionado(UsuarioLocalRelacionadoAdapter adapterUsuarioLocalRelacionado) {
        this.adapterUsuarioLocalRelacionado = adapterUsuarioLocalRelacionado;
    }

    public UsuarioLocal getUsuarioLocalFilter() {
        return usuarioLocalFilter;
    }

    public UsuarioLocalRelacionadoAdapter getAdapterUsuarioLocalRelacionado() {
        return adapterUsuarioLocalRelacionado;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setPersonaClient(PersonaServiceClient personaClient) {
        this.personaClient = personaClient;
    }

    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }

    public void setUsuarioPerfilRelacionadoFilter(UsuarioPerfilRelacionado usuarioPerfilRelacionadoFilter) {
        this.usuarioPerfilRelacionadoFilter = usuarioPerfilRelacionadoFilter;
    }

    public void setAdapterUsuarioPerfilRelacionado(UsuarioPerfilRelacionadoAdapter adapterUsuarioPerfilRelacionado) {
        this.adapterUsuarioPerfilRelacionado = adapterUsuarioPerfilRelacionado;
    }

    public UsuarioPerfilRelacionado getUsuarioPerfilRelacionadoFilter() {
        return usuarioPerfilRelacionadoFilter;
    }

    public UsuarioPerfilRelacionadoAdapter getAdapterUsuarioPerfilRelacionado() {
        return adapterUsuarioPerfilRelacionado;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public void setUserProfileCreate(UsuarioPerfil userProfileCreate) {
        this.userProfileCreate = userProfileCreate;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public UsuarioPerfil getUserProfileCreate() {
        return userProfileCreate;
    }

    public void setServiceUserProfile(UsuarioPerfilServiceClient serviceUserProfile) {
        this.serviceUserProfile = serviceUserProfile;
    }

    public UsuarioPerfilServiceClient getServiceUserProfile() {
        return serviceUserProfile;
    }

    public void setAdapterPerfil(PerfilListAdapter adapterPerfil) {
        this.adapterPerfil = adapterPerfil;
    }

    public void setPerfilUsuarioSelected(List<Perfil> perfilUsuarioSelected) {
        this.perfilUsuarioSelected = perfilUsuarioSelected;
    }

    public List<Perfil> getPerfilUsuarioSelected() {
        return perfilUsuarioSelected;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public PerfilListAdapter getAdapterPerfil() {
        return adapterPerfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    /**
     * @param usuarioLocalClient the usuarioLocalClient to set
     */
    public void setUsuarioLocalClient(UsuarioLocalServiceClient usuarioLocalClient) {
        this.usuarioLocalClient = usuarioLocalClient;
    }

    public void setUserCreate(Integer userCreate) {
        this.userCreate = userCreate;
    }

    public Integer getUserCreate() {
        return userCreate;
    }

    /**
     * @return the adapterUsuarioLocal
     */
    public UsuarioLocalListAdapter getAdapterUsuarioLocal() {
        return adapterUsuarioLocal;
    }

    /**
     * @param adapterUsuarioLocal the adapterUsuarioLocal to set
     */
    public void setAdapterUsuarioLocal(UsuarioLocalListAdapter adapterUsuarioLocal) {
        this.adapterUsuarioLocal = adapterUsuarioLocal;
    }

    /**
     * @return the selectedUsuarioLocal
     */
    public List<UsuarioLocal> getSelectedUsuarioLocal() {
        return selectedUsuarioLocal;
    }

    /**
     * @param selectedUsuarioLocal the selectedUsuarioLocal to set
     */
    public void setSelectedUsuarioLocal(List<UsuarioLocal> selectedUsuarioLocal) {
        this.selectedUsuarioLocal = selectedUsuarioLocal;
    }

    /**
     * @return the userLocal
     */
    public UsuarioLocal getUserLocal() {
        return userLocal;
    }

    /**
     * @param userLocal the userLocal to set
     */
    public void setUserLocal(UsuarioLocal userLocal) {
        this.userLocal = userLocal;
    }

    /**
     * @return the idUsuario
     */
    public Integer getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the locales
     */
    public List<Local> getLocales() {
        return locales;
    }

    /**
     * @param locales the locales to set
     */
    public void setLocales(List<Local> locales) {
        this.locales = locales;
    }

    /**
     * @return the localClient
     */
    public LocalService getLocalClient() {
        return localClient;
    }

    /**
     * @param localClient the localClient to set
     */
    public void setLocalClient(LocalService localClient) {
        this.localClient = localClient;
    }

    /**
     * @return the adapterLocal
     */
    public LocalListAdapter getAdapterLocal() {
        return adapterLocal;
    }

    /**
     * @param adapterLocal the adapterLocal to set
     */
    public void setAdapterLocal(LocalListAdapter adapterLocal) {
        this.adapterLocal = adapterLocal;
    }

    /**
     * @return the rows
     */
    public Integer getRows() {
        return rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(Integer rows) {
        this.rows = rows;
    }

    /**
     * @return the changePass
     */
    public ChangePass getChangePass() {
        return changePass;
    }

    /**
     * @param changePass the changePass to set
     */
    public void setChangePass(ChangePass changePass) {
        this.changePass = changePass;
    }

    /**
     * @return the messageAdapter
     */
    public MessageListAdapter getMessageAdapter() {
        return messageAdapter;
    }

    /**
     * @param messageAdapter the messageAdapter to set
     */
    public void setMessageAdapter(MessageListAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    /**
     * @return the adapter
     */
    public UserListAdapter getAdapter() {
        return adapter;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(UserListAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the personClient
     */
    public PersonaServiceClient getPersonaClient() {
        return personaClient;
    }

    /**
     * @param personCliente the personClient to set
     */
    public void setPersonClient(PersonaServiceClient personCliente) {
        this.personaClient = personCliente;
    }

    /**
     * @return the userClient
     */
    public UserServiceClient getUserClient() {
        return userClient;
    }

    /**
     * @param userClient the userClient to set
     */
    public void setUserClient(UserServiceClient userClient) {
        this.userClient = userClient;
    }

    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    /**
     * @return the selectLocal
     */
    public List<Local> getSelectLocal() {
        return selectLocal;
    }

    /**
     * @param selectLocal the selectLocal to set
     */
    public void setSelectLocal(List<Local> selectLocal) {
        this.selectLocal = selectLocal;
    }

    /**
     * @return the user
     */
    public Usuario getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Usuario user) {
        this.user = user;
    }

    /**
     * @return the tipoUsuario
     */
    public List<TipoUsuario> getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     * @param tipoUsuario the tipoUsuario to set
     */
    public void setTipoUsuario(List<TipoUsuario> tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public UsuarioEmpresa getUserEmpresaRelation() {
        return userEmpresaRelation;
    }

    public void setUserEmpresaRelation(UsuarioEmpresa userEmpresaRelation) {
        this.userEmpresaRelation = userEmpresaRelation;
    }

//</editor-fold>
    /**
     * Método para crear un usuario
     */
    public void create() {

        if (perfilUsuarioSelected.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar al menos un perfil!"));
        } else {
            persona.setActivo("S");
            persona.setCodigoTipoPersona("FISICA");

            emailUsuario.setPrincipal("S");
            emailUsuario.setCodigoTipoEmail("TRABAJO");

            telUsuario.setPrincipal("S");
            telUsuario.setCodigoTipoTelefono("TRABAJO");

            user.setCodigoEstado("ACTIVO");
            user.setPersonaFisica(persona);
            user.setEmail(emailUsuario);
            user.setTelefono(telUsuario);

            List<UsuarioPerfilRelacionado> usuarioPerfil = new ArrayList<>();
            for (Perfil perfil1 : perfilUsuarioSelected) {
                UsuarioPerfilRelacionado up = new UsuarioPerfilRelacionado();
                up.setIdPerfil(perfil1.getId());
                up.setCodigoEstado("ACTIVO");
                usuarioPerfil.add(up);
            }
            user.setUsuarioPerfiles(usuarioPerfil);

            BodyResponse<Usuario> respuestaTal = userClient.setUsuario(user);
            if (respuestaTal.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Usuario creado correctamente!"));
                clear();
            }
        }

    }

    /**
     * Método para filtrar usuarios perfiles asociados
     */
    public void filterUsuarioPerfil() {
        adapterUsuarioPerfilRelacionado = adapterUsuarioPerfilRelacionado.fillData(usuarioPerfilRelacionadoFilter);
    }

    /**
     * Método para limpiar la vista
     */
    public void clear() {
        empresaAutocomplete2 = new Empresa();
        empresaAutocomplete = new Empresa();
        perfilUsuarioSelected = new ArrayList<>();
        emailUsuario = new Email();
        telUsuario = new Telefono();
        persona = new Persona();
        user = new Usuario();
        filter();
    }

    /**
     * Método para obtener los usuarios
     */
    public void filter() {
        adapter = adapter.fillData(user);
    }

    /**
     * Método para obtener los estados
     */
    public void filterEstado() {
        TipoEstado te = new TipoEstado();
        te.setCodigo("USUARIO");
        estadoFilter.setActivo("S");
        estadoFilter.setTipoEstado(te);
        estadoAdapter = estadoAdapter.fillData(estadoFilter);
    }

    /**
     * Método para editar la contraseña
     *
     * @param event
     */
    public void onRowEditPass(RowEditEvent event) {

        messageAdapter = new MessageListAdapter();

        if (!nuevaContrasena.isEmpty()) {

            this.changePass = new ChangePass(((Usuario) event.getObject()).getUsuario(), nuevaContrasena);

            messageAdapter = userClient.changePassUsers(changePass);
            if (messageAdapter != null
                    && messageAdapter.getMessageList() != null
                    && !messageAdapter.getMessageList().isEmpty()
                    && messageAdapter
                            .getMessageList()
                            .get(0)
                            .getType()
                            .equalsIgnoreCase("OK")) {
                init();
            } else {

                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                                "Contraseña Editada correctamente!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                            "La nueva contraseña no puede ser vacia!"));
        }
    }

    /**
     * Método para cambiar el estado del usuario
     *
     * @param user
     */
    public void onChangeStateUser(Usuario user) {

        switch (user.getEstado().getDescripcion()) {
            case "Activo":
                user.setIdEstado(null);
                user.setCodigoEstado("INACTIVO");
                break;
            case "Inactivo":
                user.setIdEstado(null);
                user.setCodigoEstado("ACTIVO");
                break;
            case "Bloqueado":
                user.setIdEstado(null);
                user.setCodigoEstado("ACTIVO");
                break;
            default:
                break;
        }

        Integer idUsuario = userClient.editUsuario(user);

        if (idUsuario != null) {
            filter();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio de estado correctamente!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Error al cambiar el estado!"));
        }
    }

    /**
     * Metodo para listar los locales del usuario
     *
     */
    public void getListLocal() {

        usuarioLocalFilter = new UsuarioLocal();
        usuarioLocalFilter.setIdUsuario(usuario.getId());

        if (idLocal != null) {
            usuarioLocalFilter.setIdLocal(idLocal);
        }

        adapterUsuarioLocalRelacionado = adapterUsuarioLocalRelacionado.fillData(usuarioLocalFilter);

    }

    /**
     * Método para guardar el usuario
     *
     * @param usuario
     */
    public void saveUser(Usuario usuario) {
        idUsuarioAsociar = usuario.getId();
        idPersonaUsuario = usuario.getIdPersonaFisica();

        this.usuario = usuario;
        getListLocal();
    }

    /**
     * Método para crear la asociación
     *
     * @param usuarioLocal
     */
    public void createAssociation(UsuarioLocal usuarioLocal) {
        localFilter = new Local();
        localFilter = local;

        userLocal = new UsuarioLocal();

        userLocal.setIdLocal(usuarioLocal.getIdLocal());
        userLocal.setIdPersona(usuarioLocal.getIdPersona());
        userLocal.setIdUsuario(usuario.getId());
        userLocal.setActivo("S");

        userLocal = usuarioLocalClient.editAsociacionUsuarioLocal(userLocal);

        if (userLocal != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Local actualizado correctamente!"));
            getListLocal();
        }
    }

    /**
     * Método para eliminar la asociación
     *
     * @param usuarioLocal
     */
    public void deleteAssociation(UsuarioLocal usuarioLocal) {

        userLocal = new UsuarioLocal();

        userLocal.setIdLocal(usuarioLocal.getIdLocal());
        userLocal.setIdPersona(usuarioLocal.getIdPersona());
        userLocal.setIdUsuario(usuario.getId());
        userLocal.setActivo("N");

        userLocal = usuarioLocalClient.editAsociacionUsuarioLocal(userLocal);

        if (userLocal != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Local actualizado correctamente!"));
            getListLocal();

        }
    }

    /**
     * Método para asociar masivamente
     */
    public void asociarMasivamente() {
        UsuarioLocal usuarioLocalMasivo = new UsuarioLocal();
        boolean a = false;

        usuarioLocalMasivo.setIdUsuario(usuario.getId());
        usuarioLocalMasivo.setActivo("S");

        a = usuarioLocalClient.asociarMasivamenteUsuarioLocal(usuarioLocalMasivo);

        if (a == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociaciones creadas!"));
            getListLocal();
        }
    }

    /**
     * Método para eliminar masivamente
     */
    public void eliminarMasivamente() {
        UsuarioLocal usuarioLocalMasivo = new UsuarioLocal();
        boolean a = false;

        usuarioLocalMasivo.setIdUsuario(usuario.getId());
        usuarioLocalMasivo.setActivo("N");

        a = usuarioLocalClient.asociarMasivamenteUsuarioLocal(usuarioLocalMasivo);

        if (a == true) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociaciones eliminadas!"));
            getListLocal();
        }
    }

    public void clearUsuarioLocalFilter() {
        idLocal = null;
        user.setIdPersona(idPersonaUsuario);
        user.setId(idUsuarioAsociar);
    }

    /**
     * Método para reiniciar el formulario de la lista de datos perfiles
     */
    public void clearPerfil() {
        empresaAutocomplete3 = new Empresa();
        perfil = new Perfil();
        filterPerfil();
    }

    /**
     * Metodo para obtener la lista de perfiles
     */
    public void filterPerfil() {
        adapterPerfil = adapterPerfil.fillData(perfil);
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        user.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery2(String query) {
        empresaAutocomplete2 = new Empresa();
        empresaAutocomplete2.setDescripcion(query);
        empresaAdapter2 = empresaAdapter2.fillData(empresaAutocomplete2);
        return empresaAdapter2.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa2(SelectEvent event) {
        user.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery3(String query) {
        empresaAutocomplete3 = new Empresa();
        empresaAutocomplete3.setDescripcion(query);
        empresaAdapter3 = empresaAdapter3.fillData(empresaAutocomplete3);
        return empresaAdapter3.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa3(SelectEvent event) {
        perfil.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Metodo para listar las empresas asociadas a los usuario (ADMIN)
     *
     * @param userAux
     */
    public void getUsuarioEmpresasRelacionadas(Usuario userAux) {
        this.idUsuario = userAux.getId();// se usa en eliminarMasivamenteEmpresas y asociarMasivamenteEmpresas
        usuarioEmpresaRelacionadoFilter = new UsuarioEmpresa(userAux.getId());
        adapterUsuarioEmpresaRelacionado = adapterUsuarioEmpresaRelacionado.fillData(usuarioEmpresaRelacionadoFilter);
    }

    /**
     * Metodo para listar las empresas asociadas a los usuarios mediante el Id
     *
     * @param userId
     */
    public void getUsuarioEmpresasRelacionadas2(Integer userId) {
        usuarioEmpresaRelacionadoFilter = new UsuarioEmpresa(userId);
        adapterUsuarioEmpresaRelacionado = adapterUsuarioEmpresaRelacionado.fillData(usuarioEmpresaRelacionadoFilter);
    }

    /**
     * Metodo para listar las empresas
     *
     * @param userAux
     */
    public void getUsuarioEmpresas(Usuario userAux) {
        usuarioEmpresaFilter = new UsuarioEmpresa(userAux.getId());
        usuarioEmpresaRelacionadoFilter.setActivo("S");
        adapterUsuarioEmpresa = adapterUsuarioEmpresa.fillData(usuarioEmpresaFilter);
    }

    /**
     * Método para crear la asociación entre usuario y empresa
     *
     * @param usuarioEmpresa
     */
    public void createAssociationUsuarioEmpresaRelacionado(UsuarioEmpresa usuarioEmpresa) {
        userEmpresaRelation = new UsuarioEmpresa();
        userEmpresaRelation.setIdEmpresa(usuarioEmpresa.getIdEmpresa());
        userEmpresaRelation.setIdUsuario(usuarioEmpresa.getIdUsuario());
        userEmpresaRelation.setActivo("S");
        BodyResponse<UsuarioEmpresa> respuesta = usuarioEmpresaClient.editAsociacionMasivaUsuarioEmpresaRelacionado(userEmpresaRelation);

        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "usuario actualizado correctamente!"));
            getUsuarioEmpresasRelacionadas2(usuarioEmpresa.getIdUsuario());
        }
    }

    /**
     * Método para eliminar la asociación de un usuario con empresa
     *
     * @param usuarioEmpresa
     */
    public void deleteAssociationUsuarioEmpresaRelacionado(UsuarioEmpresa usuarioEmpresa) {
        userEmpresaRelation = new UsuarioEmpresa();
        userEmpresaRelation.setIdEmpresa(usuarioEmpresa.getIdEmpresa());
        userEmpresaRelation.setIdUsuario(usuarioEmpresa.getIdUsuario());
        userEmpresaRelation.setActivo("N");
        BodyResponse<UsuarioEmpresa> respuesta = usuarioEmpresaClient.editAsociacionMasivaUsuarioEmpresaRelacionado(userEmpresaRelation);

        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "usuario actualizado correctamente!"));
            getUsuarioEmpresasRelacionadas2(usuarioEmpresa.getIdUsuario());
        }
    }

    /**
     * Método para asociar masivamente usuario a empresas
     */
    public void asociarMasivamenteEmpresas() {
        UsuarioEmpresa usuarioEmpresaMasivo = new UsuarioEmpresa();
        usuarioEmpresaMasivo.setIdUsuario(this.idUsuario);
        usuarioEmpresaMasivo.setActivo("S");
        BodyResponse<UsuarioEmpresa> respuesta = usuarioEmpresaClient.editAsociacionMasivaUsuarioEmpresaRelacionado(usuarioEmpresaMasivo);
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociaciones Creadas!"));
            getUsuarioEmpresasRelacionadas2(usuarioEmpresaMasivo.getIdUsuario());
        }
    }

    /**
     * Método para eliminar masivamente usuario de empresas
     */
    public void eliminarMasivamenteEmpresas() {
        UsuarioEmpresa usuarioEmpresaMasivo = new UsuarioEmpresa();
        usuarioEmpresaMasivo.setIdUsuario(this.idUsuario);
        usuarioEmpresaMasivo.setActivo("N");
        BodyResponse<UsuarioEmpresa> respuesta = usuarioEmpresaClient.editAsociacionMasivaUsuarioEmpresaRelacionado(usuarioEmpresaMasivo);
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociaciones eliminadas!"));
            getUsuarioEmpresasRelacionadas2(usuarioEmpresaMasivo.getIdUsuario());
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        try {
            this.nuevaContrasena = "";
            this.emailUsuario = new Email();
            this.telUsuario = new Telefono();
            this.estadoAdapter = new EstadoAdapter();
            this.estadoFilter = new Estado();

            this.idUsuarioAsociar = null;
            this.idPersonaUsuario = null;
            this.idLocal = null;
            this.adapterLocalList = new LocalListAdapter();
            this.adapterCliente = new ClientListAdapter();
            this.idUsuario = null;
            this.persona = new Persona();
            this.user = new Usuario();
            this.usuario = new Usuario();
            this.userLocal = new UsuarioLocal();
            this.personaClient = new PersonaServiceClient();
            this.userClient = new UserServiceClient();
            this.localClient = new LocalService();
            this.adapter = new UserListAdapter();
            this.adapterLocal = new LocalListAdapter();
            this.adapterUsuarioLocal = new UsuarioLocalListAdapter();
            this.usuarioLocalClient = new UsuarioLocalServiceClient();

            this.adapterUsuarioEmpresa = new UsuarioEmpresaListAdapter();
            this.usuarioEmpresaFilter = new UsuarioEmpresa();
            this.usuarioEmpresaClient = new UsuarioEmpresaServiceClient();

            this.adapterUsuarioEmpresaRelacionado = new UsuarioEmpresaRelacionadoAdapter();
            this.usuarioEmpresaRelacionadoFilter = new UsuarioEmpresa();

            this.empresaAutocomplete = new Empresa();
            this.empresaAdapter = new EmpresaAdapter();
            this.empresaAutocomplete2 = new Empresa();
            this.empresaAdapter2 = new EmpresaAdapter();
            this.empresaAutocomplete3 = new Empresa();
            this.empresaAdapter3 = new EmpresaAdapter();

            this.adapterPerfil = new PerfilListAdapter();
            this.perfil = new Perfil();
            filterPerfil();

            this.serviceUserProfile = new UsuarioPerfilServiceClient();
            this.userProfileCreate = new UsuarioPerfil();

            this.personaFilter = new Persona();
            this.adapterPersona = new PersonaListAdapter();

            this.adapterUsuarioPerfilRelacionado = new UsuarioPerfilRelacionadoAdapter();
            this.usuarioPerfilRelacionadoFilter = new UsuarioPerfilRelacionado();

            this.adapterUsuarioLocalRelacionado = new UsuarioLocalRelacionadoAdapter();
            this.userEmpresaRelation = new UsuarioEmpresa();

            filter();
            filterEstado();

        } catch (Exception e) {
            WebLogger.get().debug("Exception: " + e);
        }

    }

}
