package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;

/**
 * Pojo para listado de detalle de devolución
 *
 * @author Romina Núñez
 */
public class DevolucionDetalle {

    /**
     * Identificador
     */
    private Integer id;
    private Integer idDevolucion;
    /**
     * Identificador de detalle de operación
     */
    private Integer idDepositoLogistico;
    /**
     * Codigo Deposito
     */
    private String depositoLogistico;
    /**
     * Identificador de Producto
     */
    private Integer idProducto;
    /**
     * Producto
     */
    private String producto;
        /**
     * Producto
     */
    private String codigoInterno;
        /**
     * Producto
     */
    private String codigoGtin;
    /**
     * Codigo Deposito
     */
    private String codigoEstadoInventario;
        /**
     * Codigo Deposito
     */
    private String estadoInventario;
    /**
     * Codigo Deposito
     */
    private String codigoMotivo;
        /**
     * Codigo Deposito
     */
    private String motivo;
    /**
     * Cantidad Facturada
     */
    private Integer cantidadFacturada;
    /**
     * Cantidad Devolución
     */
    private Integer cantidadDevolucion;
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Listado pojo
     */
    private String listadoPojo;
    /**
     * Identificador de factura detalle
     */
    private Integer idFacturaDetalle;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setIdFacturaDetalle(Integer idFacturaDetalle) {
        this.idFacturaDetalle = idFacturaDetalle;
    }

    public Integer getIdFacturaDetalle() {
        return idFacturaDetalle;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(String depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getEstadoInventario() {
        return estadoInventario;
    }

    public void setEstadoInventario(String estadoInventario) {
        this.estadoInventario = estadoInventario;
    }

    public void setIdDevolucion(Integer idDevolucion) {
        this.idDevolucion = idDevolucion;
    }

    public Integer getIdDevolucion() {
        return idDevolucion;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public Integer getCantidadDevolucion() {
        return cantidadDevolucion;
    }

    public void setCantidadDevolucion(Integer cantidadDevolucion) {
        this.cantidadDevolucion = cantidadDevolucion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(String listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    //</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public DevolucionDetalle() {

    }

}
