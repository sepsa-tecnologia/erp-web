package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefono;

/**
 * Filtro utilizado para el servicio de contacto email
 *
 * @author Romina Núñez
 * @author Sergio D. Riveros Vazquez
 */
public class ContactoTelefonoFilter extends Filter {

    /**
     * Agrega el filtro de identificador del Contacto
     *
     * @param idContacto Identificador del Contacto
     * @return
     */
    public ContactoTelefonoFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de contacto
     *
     * @param contacto
     * @return
     */
    public ContactoTelefonoFilter contacto(String contacto) {
        if (contacto != null && !contacto.trim().isEmpty()) {
            params.put("contacto", contacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del telefono
     *
     * @param idTelefono
     * @return
     */
    public ContactoTelefonoFilter idTelefono(Integer idTelefono) {
        if (idTelefono != null) {
            params.put("idTelefono", idTelefono);
        }
        return this;
    }

    /**
     * Agrega el filtro de la prefijo
     *
     * @param prefijo
     * @return
     */
    public ContactoTelefonoFilter prefijo(String prefijo) {
        if (prefijo != null && !prefijo.trim().isEmpty()) {
            params.put("prefijo", prefijo.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro numero
     *
     * @param numero
     * @return
     */
    public ContactoTelefonoFilter numero(String numero) {
        if (numero != null && !numero.trim().isEmpty()) {
            params.put("numero", numero.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de persona
     *
     * @param idPersona Identificador del Persona
     * @return
     */
    public ContactoTelefonoFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de persona
     *
     * @param persona
     * @return
     */
    public ContactoTelefonoFilter persona(String persona) {
        if (persona != null && !persona.trim().isEmpty()) {
            params.put("persona", persona.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del idTipoContacto
     *
     * @param idTipoContacto Identificador del tipo Contacto
     * @return
     */
    public ContactoTelefonoFilter idTipoContacto(Integer idTipoContacto) {
        if (idTipoContacto != null) {
            params.put("idTipoContacto", idTipoContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de tipoContacto
     *
     * @param tipoContacto
     * @return
     */
    public ContactoTelefonoFilter tipoContacto(String tipoContacto) {
        if (tipoContacto != null && !tipoContacto.trim().isEmpty()) {
            params.put("tipoContacto", tipoContacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de idCargo
     *
     * @param idCargo
     * @return
     */
    public ContactoTelefonoFilter idCargo(Integer idCargo) {
        if (idCargo != null) {
            params.put("idCargo", idCargo);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de cargo
     *
     * @param cargo
     * @return
     */
    public ContactoTelefonoFilter cargo(String cargo) {
        if (cargo != null && !cargo.trim().isEmpty()) {
            params.put("cargo", cargo.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de estado
     *
     * @param estado
     * @return
     */
    public ContactoTelefonoFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contactoTelefono
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ContactoTelefono contactoTelefono, Integer page, Integer pageSize) {
        ContactoTelefonoFilter filter = new ContactoTelefonoFilter();
        
        filter
                .idContacto(contactoTelefono.getIdContacto())
                .contacto(contactoTelefono.getContacto())
                .idTelefono(contactoTelefono.getIdTelefono())
                .prefijo(contactoTelefono.getPrefijo())
                .numero(contactoTelefono.getNumero())
                .idPersona(contactoTelefono.getIdPersona())
                .persona(contactoTelefono.getPersona())
                .idTipoContacto(contactoTelefono.getIdTipoContacto())
                .tipoContacto(contactoTelefono.getTipoContacto())
                .idCargo(contactoTelefono.getIdCargo())
                .cargo(contactoTelefono.getCargo())
                .estado(contactoTelefono.getEstado())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de ContactoEmailFilter
     */
    public ContactoTelefonoFilter() {
        super();
    }
}
