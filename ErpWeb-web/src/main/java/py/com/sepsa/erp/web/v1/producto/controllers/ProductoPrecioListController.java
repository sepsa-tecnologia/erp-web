package py.com.sepsa.erp.web.v1.producto.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.adapters.ProductoPrecioListAdapter;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;

/**
 * Controlador para la vista listar producto precio
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarProductoPrecio")
public class ProductoPrecioListController implements Serializable {

    /**
     * Objeto producto precio
     */
    private ProductoPrecio productoPrecio;
    /**
     * Adaptador de la lista de precio producto
     */
    private ProductoPrecioListAdapter adapterProductoPrecio;
    /**
     * SelectItem para estados
     */
    private List<SelectItem> estados = new ArrayList<>();
    /**
     * Adaptador de la lista de estados
     */
    private EstadoAdapter estadoAdapter;
    /**
     * Objeto Estado
     */
    private Estado estado;
    /**
     * Objeto producto
     */
    private Producto producto;
    /**
     * Adptador de la lista de productos
     */
    private ProductoAdapter adapterProducto;
    /**
     * Objeto canal venta
     */
    private CanalVenta canalVenta;
    /**
     * Adaptador de la lista de canal venta
     */
    private CanalVentaListAdapter adapterCanalVenta;
    /**
     * Lista de Entidad Financiera
     */
    private List<SelectItem> listMonedas = new ArrayList<>();
    /**
     * Adaptador de la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * Objeto Moneda
     */
    private Moneda moneda;
    /**
     * Cliente remoto de producto cliente
     */
    private ProductoService productoClient;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de lista de clientes.
     */
    private ClientListAdapter adapterCliente;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }
    
    
    
    /**
     * @return the productoClient
     */
    public ProductoService getProductoClient() {
        return productoClient;
    }

    /**
     * @param productoClient the productoClient to set
     */
    public void setProductoClient(ProductoService productoClient) {
        this.productoClient = productoClient;
    }

    /**
     * @return the moneda
     */
    public Moneda getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the adapterMoneda
     */
    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    /**
     * @param adapterMoneda the adapterMoneda to set
     */
    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    /**
     * @return the listMonedas
     */
    public List<SelectItem> getListMonedas() {
        return listMonedas;
    }

    /**
     * @param listMonedas the listMonedas to set
     */
    public void setListMonedas(List<SelectItem> listMonedas) {
        this.listMonedas = listMonedas;
    }

    /**
     * @return the canalVenta
     */
    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    /**
     * @param canalVenta the canalVenta to set
     */
    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    /**
     * @return the adapterCanalVenta
     */
    public CanalVentaListAdapter getAdapterCanalVenta() {
        return adapterCanalVenta;
    }

    /**
     * @param adapterCanalVenta the adapterCanalVenta to set
     */
    public void setAdapterCanalVenta(CanalVentaListAdapter adapterCanalVenta) {
        this.adapterCanalVenta = adapterCanalVenta;
    }

    /**
     * @return the productoPrecio
     */
    public ProductoPrecio getProductoPrecio() {
        return productoPrecio;
    }

    /**
     * @param productoPrecio the productoPrecio to set
     */
    public void setProductoPrecio(ProductoPrecio productoPrecio) {
        this.productoPrecio = productoPrecio;
    }

    /**
     * @return the adapterProductoPrecio
     */
    public ProductoPrecioListAdapter getAdapterProductoPrecio() {
        return adapterProductoPrecio;
    }

    /**
     * @param adapterProductoPrecio the adapterProductoPrecio to set
     */
    public void setAdapterProductoPrecio(ProductoPrecioListAdapter adapterProductoPrecio) {
        this.adapterProductoPrecio = adapterProductoPrecio;
    }

    /**
     * @return the estados
     */
    public List<SelectItem> getEstados() {
        return estados;
    }

    /**
     * @param estados the estados to set
     */
    public void setEstados(List<SelectItem> estados) {
        this.estados = estados;
    }

    /**
     * @return the estadoAdapter
     */
    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    /**
     * @param estadoAdapter the estadoAdapter to set
     */
    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    /**
     * @return the producto
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * @return the adapterProducto
     */
    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    /**
     * @param adapterProducto the adapterProducto to set
     */
    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }
    //</editor-fold>

    /**
     * Metodo para obtener la lista de productos
     */
    public void filterPrecioProducto() {
        getAdapterProductoPrecio().setFirstResult(0);
        adapterProductoPrecio = adapterProductoPrecio.fillData(productoPrecio);

    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {
        producto = new Producto();
        producto.setActivo("S");
        producto.setDescripcion(query);
        adapterProducto.setPageSize(25);
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto() {
        productoPrecio.setIdProducto(producto.getId());
    }

    /**
     * Método autocomplete Canal Venta
     *
     * @param query
     * @return
     */
    public List<CanalVenta> completeQueryCanalVenta(String query) {

        canalVenta = new CanalVenta();
        canalVenta.setDescripcion(query);
        adapterCanalVenta = adapterCanalVenta.fillData(canalVenta);

        return adapterCanalVenta.getData();
    }
    
    /**
     * Método autocomplete Cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQueryCliente(String query) {

        cliente = new Cliente();
        cliente.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    /**
     * Método para seleccionar el canal de venta
     */
    public void onItemSelectCanal() {
        productoPrecio.setIdCanalVenta(canalVenta.getId());
    }
    
    /**
     * Método para seleccionar el canal de venta
     */
    public void onItemSelectCliente() {
        productoPrecio.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Método para limpiar
     */
    public void clear() {
        productoPrecio = new ProductoPrecio();
        producto = new Producto();
        canalVenta = new CanalVenta();
        cliente = new Cliente();
        filterPrecioProducto();
    }

    /**
     * Método para editar producto precio
     *
     * @param event
     */
    public void onRowEditProductoPrecio(RowEditEvent event) {

        ProductoPrecio p_precio = new ProductoPrecio();
        p_precio.setId(((ProductoPrecio) event.getObject()).getId());
        p_precio.setIdProducto(((ProductoPrecio) event.getObject()).getProducto().getId());

        if (((ProductoPrecio) event.getObject()).getIdCanalVenta() != null) {
            p_precio.setIdCanalVenta(((ProductoPrecio) event.getObject()).getIdCanalVenta());
        }
        if (((ProductoPrecio) event.getObject()).getIdCliente() != null) {
            p_precio.setIdCliente(((ProductoPrecio) event.getObject()).getIdCliente());
        }

        if (((ProductoPrecio) event.getObject()).getFechaInicio() != null) {
            p_precio.setFechaInicio(((ProductoPrecio) event.getObject()).getFechaInicio());
        }

        if (((ProductoPrecio) event.getObject()).getFechaFin() != null) {
            p_precio.setFechaFin(((ProductoPrecio) event.getObject()).getFechaFin());
        }

        p_precio.setPrecio(((ProductoPrecio) event.getObject()).getPrecio());
        p_precio.setIdMoneda(((ProductoPrecio) event.getObject()).getIdMoneda());
        p_precio.setActivo(((ProductoPrecio) event.getObject()).getActivo());

        BodyResponse<ProductoPrecio> respuestaProducto = productoClient.putProductoPrecio(p_precio);
        if (respuestaProducto.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Info", "Configuración precio producto editado correctamente!"));

        }
    }

    @PostConstruct
    public void init() {

        this.adapterProductoPrecio = new ProductoPrecioListAdapter();
        this.productoPrecio = new ProductoPrecio();
        this.estadoAdapter = new EstadoAdapter();
        this.estado = new Estado();
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.canalVenta = new CanalVenta();
        this.adapterCanalVenta = new CanalVentaListAdapter();
        this.adapterMoneda = new MonedaAdapter();
        this.moneda = new Moneda();
        this.productoClient = new ProductoService();
        this.adapterCliente = new ClientListAdapter();
        this.cliente = new Cliente();
        
        adapterMoneda = adapterMoneda.fillData(moneda);
        for (int i = 0; i < adapterMoneda.getData().size(); i++) {
            listMonedas.add(new SelectItem(adapterMoneda.getData().get(i).getId(),
                    adapterMoneda.getData().get(i).getCodigo()));
        }
        filterPrecioProducto();

    }

}
