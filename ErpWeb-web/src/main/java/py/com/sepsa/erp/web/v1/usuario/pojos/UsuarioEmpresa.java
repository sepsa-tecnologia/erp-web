/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * Pojo para Usuario-empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class UsuarioEmpresa {

    /**
     * Identificador de usuario-Empresa
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    /**
     * Activo
     */
    private String activo;
    /**
     * Local
     */
    private Empresa empresa;
    /**
     * Usuario
     */
    private Usuario usuario;
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    //</editor-fold>

    public UsuarioEmpresa() {
    }

    public UsuarioEmpresa(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

}
