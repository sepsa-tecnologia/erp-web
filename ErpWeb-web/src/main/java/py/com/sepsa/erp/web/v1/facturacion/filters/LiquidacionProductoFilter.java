/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.LiquidacionProducto;

/**
 * Filter para liquidacion producto
 *
 * @author Sergio D. Riveros Vazquez
 */
public class LiquidacionProductoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de liquidacion producto
     *
     * @param id
     * @return
     */
    public LiquidacionProductoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public LiquidacionProductoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param producto
     * @return
     */
    public LiquidacionProductoFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de cliente
     *
     * @param idCliente
     * @return
     */
    public LiquidacionProductoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agregar el filtro de cliente
     *
     * @param cliente
     * @return
     */
    public LiquidacionProductoFilter cliente(String cliente) {
        if (cliente != null && !cliente.trim().isEmpty()) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de plazo
     *
     * @param plazo
     * @return
     */
    public LiquidacionProductoFilter plazo(Integer plazo) {
        if (plazo != null) {
            params.put("plazo", plazo);
        }
        return this;
    }

    /**
     * Agrega el filtro de extension
     *
     * @param extension
     * @return
     */
    public LiquidacionProductoFilter extension(Integer extension) {
        if (extension != null) {
            params.put("extension", extension);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param liquidacionProducto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(LiquidacionProducto liquidacionProducto, Integer page, Integer pageSize) {
        LiquidacionProductoFilter filter = new LiquidacionProductoFilter();
        
        filter
                .id(liquidacionProducto.getId())
                .idProducto(liquidacionProducto.getIdProducto())
                .producto(liquidacionProducto.getProducto())
                .idCliente(liquidacionProducto.getIdCliente())
                .cliente(liquidacionProducto.getProducto())
                .plazo(liquidacionProducto.getPlazo())
                .extension(liquidacionProducto.getExtension())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public LiquidacionProductoFilter() {
        super();
    }
    
}
