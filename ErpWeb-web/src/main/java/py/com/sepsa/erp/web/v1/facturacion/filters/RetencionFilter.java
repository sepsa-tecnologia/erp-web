package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.NotaCreditoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para listado de retención
 *
 * @author alext
 */
public class RetencionFilter extends Filter {

    /**
     * Agrega el filtro por identificador de retención
     *
     * @param id
     * @return
     */
    public RetencionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idCliente
     *
     * @param idCliente
     * @return
     */
    public RetencionFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro por nroRetencion
     *
     * @param nroRetencion
     * @return
     */
    public RetencionFilter nroRetencion(String nroRetencion) {
        if (nroRetencion != null && !nroRetencion.trim().isEmpty()) {
            params.put("nroRetencion", nroRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro por fehcaInsercion
     *
     * @param fechaInsercion
     * @return
     */
    public RetencionFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por fecha
     *
     * @param fecha
     * @return
     */
    public RetencionFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public RetencionFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public RetencionFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public RetencionFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public RetencionFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param retencion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Retencion retencion, Integer page,
            Integer pageSize) {

        RetencionFilter filter = new RetencionFilter();

        filter
                .id(retencion.getId())
                .idCliente(retencion.getIdCliente())
                .idFactura(retencion.getIdFactura())
                .nroRetencion(retencion.getNroRetencion())
                .fechaInsercion(retencion.getFechaInsercion())
                .fecha(retencion.getFecha())
                .fechaDesde(retencion.getFechaDesde())
                .fechaHasta(retencion.getFechaHasta())
                .idEstado(retencion.getIdEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public RetencionFilter() {
        super();
    }

}
