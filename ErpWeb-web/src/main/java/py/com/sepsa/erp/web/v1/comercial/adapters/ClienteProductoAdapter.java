
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteProducto;
import py.com.sepsa.erp.web.v1.comercial.remote.ClienteProductoService;

/**
 * Adaptador para la lista de Descuento
 * @author Romina Núñez
 */

public class ClienteProductoAdapter extends DataListAdapter<ClienteProducto> {
    
    /**
     * Cliente para los servicios de Documento
     */
    private final ClienteProductoService serviceClienteProducto;

    /**
     * Constructor de InstallationListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ClienteProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClienteProducto = new ClienteProductoService();
    }

    /**
     * Constructor de DocumentAdapter
     */
        public ClienteProductoAdapter() {
        super();
        this.serviceClienteProducto= new ClienteProductoService();
    }


    @Override
    public ClienteProductoAdapter fillData(ClienteProducto searchData) {
        return serviceClienteProducto.getClienteProductoList(searchData, getFirstResult(), getPageSize());
    }

    
}
