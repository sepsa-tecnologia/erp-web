package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.usuario.pojos.MenuPerfil;

/**
 * Filtro para la lista de pefiles de menu
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class MenuPerfilFilter extends Filter {

    /**
     * Agrega el filtro por identificador de menú
     *
     * @param idMenu
     * @return
     */
    public MenuPerfilFilter idMenu(Integer idMenu) {
        if (idMenu != null) {
            params.put("idMenu", idMenu);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public MenuPerfilFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoMenu
     *
     * @param codigoMenu
     * @return
     */
    public MenuPerfilFilter codigoMenu(String codigoMenu) {
        if (codigoMenu != null && !codigoMenu.trim().isEmpty()) {
            params.put("codigoMenu", codigoMenu);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de perfil
     *
     * @param idPerfil
     * @return
     */
    public MenuPerfilFilter idPerfil(Integer idPerfil) {
        if (idPerfil != null) {
            params.put("idPerfil", idPerfil);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoPerfil
     *
     * @param codigoPerfil
     * @return
     */
    public MenuPerfilFilter codigoPerfil(String codigoPerfil) {
        if (codigoPerfil != null && !codigoPerfil.trim().isEmpty()) {
            params.put("codigoPerfil", codigoPerfil);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param menu datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la página de resultado
     * @return
     */
    public static Map build(MenuPerfil menu, Integer page, Integer pageSize) {

        MenuPerfilFilter filter = new MenuPerfilFilter();

        filter
                .idMenu(menu.getIdMenu())
                .codigoMenu(menu.getCodigoMenu())
                .idPerfil(menu.getIdPerfil())
                .idEmpresa(menu.getIdEmpresa())
                .codigoPerfil(menu.getCodigoPerfil())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

}
