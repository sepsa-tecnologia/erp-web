package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para configuraión de aprobación.
 *
 * @author alext
 */
public class ConfiguracionAprobacion {

    /**
     * Identificador de configuracion de aprobación.
     */
    private Integer id;
    /**
     * Identificador de id tipo de etiqueta.
     */
    private Integer idTipoEtiqueta;
    /**
     * Tipo de etiqueta.
     */
    private String tipoEtiqueta;
    /**
     * Identificador de etiqueta.
     */
    private Integer idEtiqueta;
    /**
     * Etiqueta
     */
    private String etiqueta;
    /**
     * Orden de configuración.
     */
    private Integer orden;
    /**
     * Estado de configuración.
     */
    private Character estado;

    /**
     * Getters y Setters.
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    public String getTipoEtiqueta() {
        return tipoEtiqueta;
    }

    public void setTipoEtiqueta(String tipoEtiqueta) {
        this.tipoEtiqueta = tipoEtiqueta;
    }

    public Integer getIdEtiqueta() {
        return idEtiqueta;
    }

    public void setIdEtiqueta(Integer idEtiqueta) {
        this.idEtiqueta = idEtiqueta;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    /**
     * Constructor.
     */
    public ConfiguracionAprobacion() {

    }

    /**
     * Constructor con parámetros.
     *
     * @param id
     * @param idTipoEtiqueta
     * @param tipoEtiqueta
     * @param idEtiqueta
     * @param etiqueta
     * @param orden
     * @param estado
     */
    public ConfiguracionAprobacion(Integer id, Integer idTipoEtiqueta, String tipoEtiqueta,
            Integer idEtiqueta, String etiqueta, Integer orden, Character estado) {
        this.id = id;
        this.idTipoEtiqueta = idTipoEtiqueta;
        this.tipoEtiqueta = tipoEtiqueta;
        this.idEtiqueta = idEtiqueta;
        this.etiqueta = etiqueta;
        this.orden = orden;
        this.estado = estado;
    }

    /**
     *
     * @param id
     */
    public ConfiguracionAprobacion(Integer id) {
        this.id = id;
    }

}
