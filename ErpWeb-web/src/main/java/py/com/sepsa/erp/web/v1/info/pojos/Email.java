
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO  de Email
 * @author Cristina Insfrán
 */
public class Email {

    /**
     * Identificador del email
     */
    private Integer id;
    /**
     * Identificador del tipo email
     */
    private Integer idTipoEmail;
    /**
     * Email
     */
    private String email;
    /**
     * Email principal
     */
    private String principal;
  
    /**
     * Objeto Tipo Email
     */
    private TipoEmail tipoEmail;
    /**
     * Código tipo email
     */
    private String codigoTipoEmail;
    /**
     * Activo
     */
    private String activo;
    
    /**
     * @return the codigoTipoEmail
     */
    public String getCodigoTipoEmail() {
        return codigoTipoEmail;
    }

    /**
     * @param codigoTipoEmail the codigoTipoEmail to set
     */
    public void setCodigoTipoEmail(String codigoTipoEmail) {
        this.codigoTipoEmail = codigoTipoEmail;
    }


    /**
     * @return the tipoEmail
     */
    public TipoEmail getTipoEmail() {
        return tipoEmail;
    }

    /**
     * @param tipoEmail the tipoEmail to set
     */
    public void setTipoEmail(TipoEmail tipoEmail) {
        this.tipoEmail = tipoEmail;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    } 

    public void setIdTipoEmail(Integer idTipoEmail) {
        this.idTipoEmail = idTipoEmail;
    }

    public Integer getIdTipoEmail() {
        return idTipoEmail;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    /**
     * @return the principal
     */
    public String getPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    
    /**
     * Constructor de la clase
     */
    public Email(){
        
    }
    
    /**
     * Constructor de la clase
     * @param id
     * @param idTipoEmail
     * @param email
     * @param principal
     */
    public Email(Integer id,Integer idTipoEmail,String email,String principal){
        this.id = id;
        this.idTipoEmail = idTipoEmail;
        this.email = email;
        this.principal = principal;
    }
}
