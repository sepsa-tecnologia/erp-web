package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;
import java.math.BigDecimal;

/**
 * POJO para Monto
 *
 * @author Romina Núñez
 */
public class MontoLetras {

    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de moneda
     */
    private String codigoMoneda;
    /**
     * Identificador de lugar cobro
     */
    private BigDecimal monto;
    /**
     * Lugar cobro
     */
    private String totalLetras;

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public String getTotalLetras() {
        return totalLetras;
    }

    public void setTotalLetras(String totalLetras) {
        this.totalLetras = totalLetras;
    }

    /**
     * Constructor de la clase
     */
    public MontoLetras() {
    }

    /**
     * Constructor con parametros
     *
     * @param idMoneda
     * @param monto
     * @param totalLetras
     */
    public MontoLetras(Integer idMoneda, BigDecimal monto, String totalLetras) {
        this.idMoneda = idMoneda;
        this.monto = monto;
        this.totalLetras = totalLetras;
    }

}
