/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ProcesoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ProcesoService extends APIErpCore {

    /**
     * Obtiene la lista de procesos.
     *
     * @param proceso
     * @param page
     * @param pageSize
     * @return
     */
    public ProcesoAdapter getProcesoList(Proceso proceso, Integer page,
            Integer pageSize) {

        ProcesoAdapter lista = new ProcesoAdapter();

        Map params = ProcesoFilter.build(proceso, page, pageSize);

        HttpURLConnection conn = GET(Resource.PROCESO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProcesoAdapter.class);

            lista = (ProcesoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param proceso
     * @return
     */
    public BodyResponse<Proceso> setProceso(Proceso proceso) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.PROCESO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(proceso));
            response = BodyResponse.createInstance(conn, Proceso.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Proceso get(Integer id) {
        Proceso data = new Proceso(id);
        ProcesoAdapter list = getProcesoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para editar Proceso
     *
     * @param producto
     * @return
     */
    public BodyResponse<Proceso> editProceso(Proceso producto) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.PROCESO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(producto));
            response = BodyResponse.createInstance(conn, Proceso.class);
        }
        return response;
    }

    /**
     * Constructor de Proceso
     */
    public ProcesoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        PROCESO("Listado de procesos", "proceso");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
