package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.MotivoDescuentoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEtiquetaAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.MotivoDescuento;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;
import py.com.sepsa.erp.web.v1.comercial.remote.MotivoDescuentoService;

/**
 * Controlador para Motivo Descuento
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("motivoDescuento")
public class MotivoDescuentoController implements Serializable {

    /**
     * Bandera
     */
    private boolean showPanel;
    /**
     * Adaptador para la lista de motivo Descuento
     */
    private MotivoDescuentoAdapter adapterMotivoDescuento;
    /**
     * POJO Motivo Descuento
     */
    private MotivoDescuento motivoDescuentoFilter;
    /**
     * POJO Motivo Descuento a ser creado
     */
    private MotivoDescuento motivoDescuentoCreate;
    /**
     * POJO TipoEtiqueta
     */
    private TipoEtiqueta tipoEtiquetaFilter;
    /**
     * Adaptador Tipo Etiqueta
     */
    private TipoEtiquetaAdapter adapterTipoEtiqueta;
    /**
     * Servicio Motivo Descuento
     */
    private MotivoDescuentoService serviceMotivoDescuento;
    /**
     * POJO MotivoDescuento
     */
    private MotivoDescuento motivoDescuentoEdit;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoDescuentoAdapter getAdapterMotivoDescuento() {
        return adapterMotivoDescuento;
    }

    public void setMotivoDescuentoEdit(MotivoDescuento motivoDescuentoEdit) {
        this.motivoDescuentoEdit = motivoDescuentoEdit;
    }

    public MotivoDescuento getMotivoDescuentoEdit() {
        return motivoDescuentoEdit;
    }

    public boolean isShowPanel() {
        return showPanel;
    }

    public void setShowPanel(boolean showPanel) {
        this.showPanel = showPanel;
    }

    public void setServiceMotivoDescuento(MotivoDescuentoService serviceMotivoDescuento) {
        this.serviceMotivoDescuento = serviceMotivoDescuento;
    }

    public MotivoDescuentoService getServiceMotivoDescuento() {
        return serviceMotivoDescuento;
    }

    public void setTipoEtiquetaFilter(TipoEtiqueta tipoEtiquetaFilter) {
        this.tipoEtiquetaFilter = tipoEtiquetaFilter;
    }

    public void setAdapterTipoEtiqueta(TipoEtiquetaAdapter adapterTipoEtiqueta) {
        this.adapterTipoEtiqueta = adapterTipoEtiqueta;
    }

    public TipoEtiqueta getTipoEtiquetaFilter() {
        return tipoEtiquetaFilter;
    }

    public TipoEtiquetaAdapter getAdapterTipoEtiqueta() {
        return adapterTipoEtiqueta;
    }

    public void setMotivoDescuentoCreate(MotivoDescuento motivoDescuentoCreate) {
        this.motivoDescuentoCreate = motivoDescuentoCreate;
    }

    public MotivoDescuento getMotivoDescuentoCreate() {
        return motivoDescuentoCreate;
    }

    public MotivoDescuento getMotivoDescuentoFilter() {
        return motivoDescuentoFilter;
    }

    public void setAdapterMotivoDescuento(MotivoDescuentoAdapter adapterMotivoDescuento) {
        this.adapterMotivoDescuento = adapterMotivoDescuento;
    }

    public void setMotivoDescuentoFilter(MotivoDescuento motivoDescuentoFilter) {
        this.motivoDescuentoFilter = motivoDescuentoFilter;
    }
//</editor-fold>

    /**
     * Filtra los datos de la lista de motivo descuento
     */
    public void filterMotivoDescuento() {
        adapterMotivoDescuento = adapterMotivoDescuento.fillData(motivoDescuentoFilter);
    }

    /**
     * Filtra los datos de la lista de tipo etiqueta
     */
    public void filterTipoEtiqueta() {
        adapterTipoEtiqueta = adapterTipoEtiqueta.fillData(tipoEtiquetaFilter);
    }

    /**
     * Método para crear el Motivo Descuento
     */
    public void createMotivoDescuento() {
        Integer idMotivoDescuento = null;
        boolean save = false;

        if (!motivoDescuentoCreate.getDescripcion().trim().isEmpty() && motivoDescuentoCreate.getDescripcion() != null) {
            save = true;
        } else {
            save = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar una DESCRIPCIÓN para el Motivo Descuento!"));
        }

        if (motivoDescuentoCreate.getRangoDesde() != null) {
            save = true;
        } else {
            save = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RANGO DESDE para el Motivo Descuento!"));
        }

        if (motivoDescuentoCreate.getRangoHasta() != null) {
            save = true;
        } else {
            save = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RANGO DESDE para el Motivo Descuento!"));
        }

        if (save != false) {

            idMotivoDescuento = serviceMotivoDescuento.createMotivoDescuento(motivoDescuentoCreate);

            if (idMotivoDescuento != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Motivo Descuento creado!"));
                filterMotivoDescuento();
                cleanForm();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al crear el Motivo Descuento!"));
            }
        }

    }

    /**
     * Método para limpiar formulario
     */
    public void cleanForm() {
        motivoDescuentoCreate = new MotivoDescuento();
    }

    /**
     * Método para guardar los datos
     *
     * @param motivoDescuento
     */
    public void saveData(MotivoDescuento motivoDescuento) {
        motivoDescuentoEdit = motivoDescuento;
        showPanel = true;
    }

    public void editMotivoDescuento() {
        motivoDescuentoEdit = serviceMotivoDescuento.editMotivoDescuento(motivoDescuentoEdit);

        if (motivoDescuentoEdit.getId() != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Motivo Descuento editado!"));
            filterMotivoDescuento();
            showPanel = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al editar el Motivo Descuento!"));
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.motivoDescuentoFilter = new MotivoDescuento();
        this.adapterMotivoDescuento = new MotivoDescuentoAdapter();

        this.tipoEtiquetaFilter = new TipoEtiqueta();
        this.adapterTipoEtiqueta = new TipoEtiquetaAdapter();

        this.serviceMotivoDescuento = new MotivoDescuentoService();
        this.motivoDescuentoCreate = new MotivoDescuento();

        showPanel = false;
        this.motivoDescuentoEdit = new MotivoDescuento();

        filterTipoEtiqueta();
        filterMotivoDescuento();
    }
}
