/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioEmpresa;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioEmpresaServiceClient;

/**
 * Adapter para Usuario-Empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class UsuarioEmpresaRelacionadoAdapter extends DataListAdapter<UsuarioEmpresa> {

    /**
     * Cliente para los servicios de dato persona
     */
    private final UsuarioEmpresaServiceClient usuarioEmpresaClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioEmpresaRelacionadoAdapter fillData(UsuarioEmpresa searchData) {

        return usuarioEmpresaClient.getUsuarioEmpresaRelacionadoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de DatoPersonaListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioEmpresaRelacionadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.usuarioEmpresaClient = new UsuarioEmpresaServiceClient();
    }

    /**
     * Constructor de UsuarioEmpresaListAdapter
     */
    public UsuarioEmpresaRelacionadoAdapter() {
        super();
        this.usuarioEmpresaClient = new UsuarioEmpresaServiceClient();
    }
}
