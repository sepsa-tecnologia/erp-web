package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesPeriodo;
import py.com.sepsa.erp.web.v1.comercial.remote.LiquidacionesPeriodoService;

/**
 * Adaptador para liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionesPeriodoAdapter extends DataListAdapter<LiquidacionesPeriodo> {

    /**
     * Servicio de liquidaciones por periodo.
     */
    private final LiquidacionesPeriodoService liquidacionesPeriodoService;

    /**
     * Método para cargar la lista de liquidaciones por periodo.
     *
     * @param searchData
     * @return
     */
    @Override
    public LiquidacionesPeriodoAdapter fillData(LiquidacionesPeriodo searchData) {
        return liquidacionesPeriodoService.getLiquidacionesPeriodoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de LiquidacionesPeriodoAdapter.
     *
     * @param page
     * @param pageSize
     */
    public LiquidacionesPeriodoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.liquidacionesPeriodoService = new LiquidacionesPeriodoService();
    }

    /**
     * Constructor de LiquidacionesPeriodoAdapter.
     */
    public LiquidacionesPeriodoAdapter() {
        super();
        this.liquidacionesPeriodoService = new LiquidacionesPeriodoService();
    }
}
