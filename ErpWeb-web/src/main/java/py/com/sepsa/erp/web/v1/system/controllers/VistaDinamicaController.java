package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;

/**
 * Controlador para mostrar las vistas dinamicas
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("vistaDinamica")
public class VistaDinamicaController implements Serializable {

    @Inject
    private SessionData session;
    
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Titulo
     */
    private String titulo;
    /**
     * url
     */
    private String url;
    /**
     * Identificador del menu
     */
    private Integer idMenu;
    /**
     * Adaptador de la lista de menu
     */
    private MenuListAdapter menuAdapter;
    /**
     * Objeto Menu
     */
    private Menu menu;

    /**
     * @return the menuAdapter
     */
    public MenuListAdapter getMenuAdapter() {
        return menuAdapter;
    }

    /**
     * @param menuAdapter the menuAdapter to set
     */
    public void setMenuAdapter(MenuListAdapter menuAdapter) {
        this.menuAdapter = menuAdapter;
    }

    /**
     * @return the menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     * @param menu the menu to set
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the idMenu
     */
    public Integer getIdMenu() {
        return idMenu;
    }

    /**
     * @param idMenu the idMenu to set
     */
    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public String obtenerUrl() {
        return String.format("%s?jwt=%s", url, session.getJwt());
    }
    
    public void getMenuDinamico() {
        menuAdapter = menuAdapter.fillData(menu);

        if (!menuAdapter.getData().isEmpty()) {
         
            for (int i = 0; i < menuAdapter.getData().size(); i++) {
                descripcion = menuAdapter.getData().get(i).getDescripcion();
                titulo = menuAdapter.getData().get(i).getTitulo();
                url = menuAdapter.getData().get(i).getUrl();
            }

        }
    }
    


    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        WebLogger.get().debug("iNIT");
        this.menu = new Menu();
        this.menuAdapter = new MenuListAdapter();
         Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        
         menu.setId(Integer.valueOf(params.get("idMenu")));
         getMenuDinamico();

    }

    /**
     * Constructor de la clase
     */
    public VistaDinamicaController() {
        /* WebLogger.get().debug("Entra");
              this.menu = new Menu();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest myRequest = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();

        String id = (String) myRequest.getParameter("idMenu");
        WebLogger.get().debug("id: " + id);
        if (id != null) {
             idMenu = Integer.valueOf(id);
            menu.setId(Integer.valueOf(id));
        }*/

    }
}
