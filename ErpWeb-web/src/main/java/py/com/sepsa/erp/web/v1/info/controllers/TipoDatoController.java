package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.TipoDatoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDato;

/**
 * Controlador para Clientes
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("dataType")
public class TipoDatoController implements Serializable {

    /**
     * Adaptador de tipo de dato
     */
    private TipoDatoListAdapter tipoDatoAdapter;
    /**
     * Adaptador de tipo de dato
     */
    private TipoDatoListAdapter tipoDatoAdapterFilter;
    /**
     * Objeto
     */
    private TipoDato tipoDatoFilter;

    public TipoDatoListAdapter getTipoDatoAdapter() {
        return tipoDatoAdapter;
    }

    public void setTipoDatoAdapterFilter(TipoDatoListAdapter tipoDatoAdapterFilter) {
        this.tipoDatoAdapterFilter = tipoDatoAdapterFilter;
    }
            

    public TipoDatoListAdapter getTipoDatoAdapterFilter() {
        return tipoDatoAdapterFilter;
    }

    public void setTipoDatoAdapter(TipoDatoListAdapter tipoDatoAdapter) {
        this.tipoDatoAdapter = tipoDatoAdapter;
    }

    public TipoDato getTipoDatoFilter() {
        return tipoDatoFilter;
    }

    public void setTipoDatoFilter(TipoDato tipoDatoFilter) {
        this.tipoDatoFilter = tipoDatoFilter;
    }

    /**
     * Obtiene la descripcion del tipo de dato
     *
     * @param id Identificador
     * @return Descripcion
     */
    public String getTipoDato(Integer id) {
        TipoDato tipoDato = new TipoDato();
        tipoDato.setId(id);
        //tipoDatoAdapter = tipoDatoAdapter.fillData(tipoDato);
        tipoDatoAdapter = tipoDatoAdapter.fillData(tipoDato);

        return tipoDatoAdapter.getData() == null
                || tipoDatoAdapter.getData().isEmpty()
                ? "N/A"
                : tipoDatoAdapter.getData().get(0).getTipoDato();
    }
    
    public void filter(){
        this.tipoDatoAdapterFilter=tipoDatoAdapterFilter.fillData(tipoDatoFilter);
    }
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.tipoDatoAdapter = new TipoDatoListAdapter();
        this.tipoDatoAdapterFilter=new TipoDatoListAdapter();
        this.tipoDatoFilter = new TipoDato();
        filter();

    }
}
