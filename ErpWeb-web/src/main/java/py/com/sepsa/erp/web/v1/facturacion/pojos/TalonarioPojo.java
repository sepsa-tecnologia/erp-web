/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;

/**
 * Pojo para Talonario
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TalonarioPojo {

    /**
     * Identificador de talonario
     */
    private Integer id;
    /**
     * Timbrado
     */
    private String timbrado;
    /**
     * Inicio
     */
    private Integer inicio;
    /**
     * Fin
     */
    private Integer fin;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Activo
     */
    private String activo;
    /**
     * Número de Sucursal
     */
    private String nroSucursal;
    /**
     * Número de Punto de Venta
     */
    private String nroPuntoVenta;
    /**
     * Id Local
     */
    private Integer idLocal;
    /**
     * Fecha de Inicio
     */
    private Date fechaInicio;
    /**
     * Digital
     */
    private String digital;
    /**
     * Identificador de tipo documento
     */
    private Integer idTipoDocumento;
    /**
     * POJO Tipo de Documento
     */
    private String tipoDocumento;
    /**
     * Pojo de local
     */
    private Local local;
    /**
     * Id Persona
     */
    private Integer idPersona;
    /**
     * Persona
     */
    private Persona persona;
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    /**
     * Nro Resolución
     */
    private String nroResolucion;
    /**
     * Fecha Resolución
     */
    private Date fechaResolucion;
    /**
     * Identificador de encargado
     */
    private Integer idEncargado;
    
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">


    public Integer getId() {
        return id;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the timbrado
     */
    public String getTimbrado() {
        return timbrado;
    }

    /**
     * @param timbrado the timbrado to set
     */
    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    /**
     * @return the inicio
     */
    public Integer getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(Integer inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fin
     */
    public Integer getFin() {
        return fin;
    }

    /**
     * @param fin the fin to set
     */
    public void setFin(Integer fin) {
        this.fin = fin;
    }

    /**
     * @return the fechaVencimiento
     */
    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * @return the nroSucursal
     */
    public String getNroSucursal() {
        return nroSucursal;
    }

    /**
     * @param nroSucursal the nroSucursal to set
     */
    public void setNroSucursal(String nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    /**
     * @return the nroPuntoVenta
     */
    public String getNroPuntoVenta() {
        return nroPuntoVenta;
    }

    /**
     * @param nroPuntoVenta the nroPuntoVenta to set
     */
    public void setNroPuntoVenta(String nroPuntoVenta) {
        this.nroPuntoVenta = nroPuntoVenta;
    }

    /**
     * @return the idLocal
     */
    public Integer getIdLocal() {
        return idLocal;
    }

    /**
     * @param idLocal the idLocal to set
     */
    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    /**
     * @return the fechaInicio
     */
    public Date getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the digital
     */
    public String getDigital() {
        return digital;
    }

    /**
     * @param digital the digital to set
     */
    public void setDigital(String digital) {
        this.digital = digital;
    }

    /**
     * @return the idTipoDocumento
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * @param idTipoDocumento the idTipoDocumento to set
     */
    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * @param persona the persona to set
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    //</editor-fold>
    
    public TalonarioPojo() {
    }

    public TalonarioPojo(Integer id) {
        this.id = id;
    }
}
