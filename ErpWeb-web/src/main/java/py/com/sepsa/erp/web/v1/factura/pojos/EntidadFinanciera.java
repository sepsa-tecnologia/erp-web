package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 * POJO para la lista de entidad financiera
 *
 * @author Cristina Insfrán
 */
public class EntidadFinanciera {

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Descripción
     */
    private String descripcion;

    /**
     * Código del tipo de tarifa
     */
    private String codigo;

    /**
     * Estado
     */
    private Character estado;

    /**
     * Character como string
     */
    private Boolean charToString;
    /**
     * Razón social
     */
    private String razonSocial;
    /**
     * Entidad Financiera
     */
    private EntidadFinanciera entidadFinanciera;

    /**
     * Identificador
     */
    private Integer idEntidadFinanciera;

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the estado
     */
    public Character getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Character estado) {
        this.estado = estado;
    }

    /**
     * @return the charToString
     */
    public Boolean getCharToString() {
        return charToString;
    }

    /**
     * @param charToString the charToString to set
     */
    public void setCharToString(Boolean charToString) {
        this.charToString = charToString;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public EntidadFinanciera(Integer id, String razonSocial) {
        this.id = id;
        this.razonSocial = razonSocial;
    }

    public EntidadFinanciera() {

    }

}
