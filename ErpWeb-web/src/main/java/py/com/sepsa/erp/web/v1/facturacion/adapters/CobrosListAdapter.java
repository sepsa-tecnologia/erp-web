package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobrosService;

/**
 * Adaptador de la lista de cobros
 *
 * @author alext
 */
public class CobrosListAdapter extends DataListAdapter<Cobro> {

    /**
     * Cliente para el servicio de cobros
     */
    private final CobrosService service;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos buscados
     * @return CobrosListAdapter
     */
    @Override
    public CobrosListAdapter fillData(Cobro searchData) {
        return service.
                getCobrosList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de CobrosListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobrosListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new CobrosService();
    }

    /**
     * Constructor de CobrosListAdapter
     */
    public CobrosListAdapter() {
        super();
        this.service = new CobrosService();
    }

}
