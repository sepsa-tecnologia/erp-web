
package py.com.sepsa.erp.web.v1.controllers.message;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;


/**
 *
 * @author Cristina Insfrán
 */
@SessionScoped
@Named("message")
public class MessageController implements Serializable{
    /**
     * Método para realizar push de mensajes provenientes del servidor
     * @param messageList Lista de mensajes provenientes del servidor
     */
    public void push(List<Mensaje> messageList) {
        if(messageList != null) {
            messageList.stream().map((msn) -> {
                //TODO: Agregar tipo de mensaje para determinar la severidad a mostrar
                Severity severity = FacesMessage.SEVERITY_ERROR;
                String summary = "Error";
                String detail = msn.getDescripcion();
                String formId = ":message-form:message";
                FacesMessage facesMessage = 
                        new FacesMessage(severity, summary, detail);
                FacesContext
                        .getCurrentInstance()
                        .addMessage(formId, facesMessage);
                return formId;
            }).forEachOrdered((formId) -> {
                PrimeFaces.current().ajax().update(formId);
            });
        }
    }
    /** 
     * Constructor de MessageController
     */
    public MessageController() {} 
}
