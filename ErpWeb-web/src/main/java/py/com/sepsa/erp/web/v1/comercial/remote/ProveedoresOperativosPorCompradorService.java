package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProveedoresOperativosPorCompradorAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ProveedoresOperativosPorCompradorFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProveedoresOperativosPorComprador;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para el listado de proveedores operativos por comprador.
 *
 * @author alext
 */
public class ProveedoresOperativosPorCompradorService extends APIErpFacturacion {

    /**
     * Obtiene la lista de proveedores operativos por comprador.
     *
     * @param searchData
     * @param page
     * @param pageSize
     * @return
     */
    public ProveedoresOperativosPorCompradorAdapter getProveedoresOperativosPorCompradorList(ProveedoresOperativosPorComprador searchData,
            Integer page, Integer pageSize) {

        ProveedoresOperativosPorCompradorAdapter lista = new ProveedoresOperativosPorCompradorAdapter();

        Map params = ProveedoresOperativosPorCompradorFilter.build(searchData, page, pageSize);

        HttpURLConnection conn = GET(Resource.REPORTE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProveedoresOperativosPorCompradorAdapter.class);

            lista = (ProveedoresOperativosPorCompradorAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    /**
     * Constructor.
     */
    public ProveedoresOperativosPorCompradorService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaFacturación.
     */
    public enum Resource {

        //Servicios
        REPORTE("Servicios de Proveedores Operativos por Comprador", "reporte/prov-oper-por-comp");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
