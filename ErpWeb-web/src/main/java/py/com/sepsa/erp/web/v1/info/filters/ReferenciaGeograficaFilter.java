package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;

/**
 * Filtro para Referencia Geográfica
 *
 * @author Cristina Insfrán
 */
public class ReferenciaGeograficaFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param idTipoReferenciaGeografica
     * @return
     */
    public ReferenciaGeograficaFilter idTipoReferenciaGeografica
        (Integer idTipoReferenciaGeografica) {
        if (idTipoReferenciaGeografica != null) {
            params.put("idTipoReferenciaGeografica",
                    idTipoReferenciaGeografica);
        }
        return this;
    }

    /**
     *
     * @param tipoReferenciaGeografica
     * @return
     */
    public ReferenciaGeograficaFilter tipoReferenciaGeografica
        (TipoReferenciaGeografica tipoReferenciaGeografica) {
        if (tipoReferenciaGeografica != null) {
            params.put("tipoReferenciaGeografica",
                    tipoReferenciaGeografica);
        }
        return this;
    }

    /**
     * Agrega el filtro del codigoTipoReferenciaGeografica
     *
     * @param codigoTipoReferenciaGeografica
     * @return
     */
    public ReferenciaGeograficaFilter codigoTipoReferenciaGeografica
        (String codigoTipoReferenciaGeografica) {
        if (codigoTipoReferenciaGeografica != null
                && !codigoTipoReferenciaGeografica.trim().isEmpty()) {
            params.put("codigoTipoReferenciaGeografica",
                    codigoTipoReferenciaGeografica.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del padre
     *
     * @param idPadre
     * @return
     */
    public ReferenciaGeograficaFilter idPadre(Integer idPadre) {
        if (idPadre != null) {
            params.put("idPadre", idPadre);
        }
        return this;
    }

    /**
     * Agrega el filtro del codigoPadre
     *
     * @param codigoPadre
     * @return
     */
    public ReferenciaGeograficaFilter codigoPadre(String codigoPadre) {
        if (codigoPadre != null && !codigoPadre.trim().isEmpty()) {
            params.put("codigoPadre", codigoPadre.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro del tienePadre
     *
     * @param tienePadre
     * @return
     */
    public ReferenciaGeograficaFilter tienePadre(String tienePadre) {
        if (tienePadre != null && !tienePadre.trim().isEmpty()) {
            params.put("tienePadre", tienePadre.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public ReferenciaGeograficaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por código
     *
     * @param codigo
     * @return
     */
    public ReferenciaGeograficaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro por descripcion
     *
     * @param descripcion
     * @return
     */
    public ReferenciaGeograficaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param ref
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ReferenciaGeografica ref, Integer page, Integer pageSize) {
        ReferenciaGeograficaFilter filter = new ReferenciaGeograficaFilter();

        filter
                .id(ref.getId())
                .codigo(ref.getCodigo())
                .descripcion(ref.getDescripcion())
                .idTipoReferenciaGeografica(ref.getIdTipoReferenciaGeografica())
                .tipoReferenciaGeografica(ref.getTipoReferenciaGeografica())
                .codigoTipoReferenciaGeografica(ref.getCodigoTipoReferenciaGeografica())
                .idPadre(ref.getIdPadre())
                .codigoPadre(ref.getCodigoPadre())
                .tienePadre(ref.getTienePadre())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ReferenciaGeograficaFilter
     */
    public ReferenciaGeograficaFilter() {
        super();
    }

}
