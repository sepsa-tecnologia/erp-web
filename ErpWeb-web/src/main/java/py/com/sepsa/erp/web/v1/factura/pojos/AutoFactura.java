package py.com.sepsa.erp.web.v1.factura.pojos;

import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;

/**
 * POJO para Factura
 *
 * @author Williams Vera
 */
public class AutoFactura {

    /**
     * Identificador del la factura
     */
    private Integer id;
    
    /**
     * Identificador del naturaleza del vendedor
     */
    private Integer idNaturalezaVendedor;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del la factura
     */
    private Integer idEmpresa;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de la moneda
     */
    private String codigoMoneda;
    /**
     * Nro Factura
     */
    private String nroAutofactura;

    /**
     * Nro Factura
     */
    private String nroDocumentoVendedor;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Fecha Insercion
     */
    private Date fechaInsercion;
    /**
     * Fecha
     */
    private Date fechaDesde;
    /**
     * Fecha
     */
    private Date fechaHasta;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Nombre del vendedor
     */
    private String nombreVendedor;
    /**
     * Nombre del vendedor
     */
    private String razonSocial;
    /**
     * RUC del cliente
     */
    private String ruc;
    /**
     * Dirección del cliente
     */
    private String direccion;
    /**
     * Teléfono del cliente
     */
    private String telefono;
    /**
     * Email del cliente
     */
    private String email;
    /**
     * Digital
     */
    private String digital;
    /**
     * Anulado
     */
    private String anulado;

    /**
     * Cobrado
     */
    private String cobrado;

    /**
     * Impuesto
     */
    private String impreso;
    /**
     * Entregado
     */
    private String entregado;
    /**
     * Días de Crédito
     */
    private Integer diasCredito;
    /**
     * Días de Crédito
     */
    private Integer cantidadCuotas;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto IVA total
     */
    private BigDecimal montoTotalDescuentoParticular;
    /**
     * Monto IVA total
     */
    private BigDecimal montoTotalDescuentoGlobal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total factura
     */
    private BigDecimal montoTotalFactura;
    /**
     * Monto total factura
     */
    private BigDecimal montoTotalGuaranies;
    /**
     * Cantidad
     */
    private Integer idTalonario;
    /**
     * Identificador del tipo factura
     */
    private Integer idTipoFactura;
    /**
     * POJO Moneda
     */
    private Moneda moneda;
    /**
     * POJO Talonario
     */
    private Talonario talonario;
    /**
     * POJO Tipo Factura
     */
    private TipoFactura tipoFactura;
    /**
     * POJO Local
     */
    private Local local;
    /**
     * Local origen
     */
    private Local localOrigen;
    /**
     * Local destino
     */
    private Local localDestino;
    /**
     * Listado de detalles
     */
    private List<AutoFacturaDetalle> facturaDetalles;
    /**
     * Listado de detalles
     */
    private List<Cuota> facturaCuotas;
    /**
     * Total Letras
     */
    private String totalLetras;
    /**
     * Tiene saldo
     */
    private String tieneSaldo;
    /**
     * Saldo
     */
    private BigDecimal saldo;
    /**
     * Motivo anulación
     */
    private String observacionAnulacion;
    /**
     * Identificador del motivo
     */
    private Integer idMotivoAnulacion;
    /**
     * Tiene retención
     */
    private String tieneRetencion;
    /**
     * Monto Retención
     */
    private BigDecimal montoRetencion;
    /**
     * Monto N.C.
     */
    private BigDecimal montoNotaCredito;
    /**
     * Monto Cobro
     */
    private BigDecimal montoCobro;
    /**
     * N° Timbrado
     */
    private String nroTimbrado;
    /**
     * Fecha Vto
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Pagado
     */
    private String pagado;
    /**
     * Listado de detalles
     */
    private List<AutoFacturaDetalle> facturaCompraDetalles;
    /**
     * Identificador local origen
     */
    private Integer idLocalOrigen;
    /**
     * Identificador local destino
     */
    private Integer idLocalDestino;
    /**
     * Codigo tipo de operacion a credito
     */
    private String codigoTipoOperacionCredito;
    /**
     * Codigo tipo de factura
     */
    private String codigoTipoFactura;
    /**
     * Nro casa
     */
    private String nroCasa;
    /**
     * Punto de expedicion
     */
    private String puntoExpedicion;
    /**
     * Establecimiento
     */
    private String establecimiento;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Generado SET
     */
    private String generadoSet;
    /**
     * Generado EDI
     */
    private String generadoEdi;
    /**
     * Archivo EDI
     */
    private String archivoEdi;
    /**
     * Archivo SET
     */
    private String archivoSet;
    /**
     * Estado
     */
    private Estado estado;
    /**
     * Cod Estado
     */
    private String codigoEstado;
    /**
     * Asignar nro factura
     */
    private Boolean asignarNroFactura;
    /**
     * Ignorar periodo anulación
     */
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Tiene OC
     */
    private String tieneOrdenCompra;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;
    /**
     * Registro inventario
     */
    private String registroInventario;
    /**
     * Identificador de tipo de cambio
     */
    private Integer idTipoCambio;
        /**
     * Identificador de tipo de cambio
     */
    private Integer idNaturalezaCliente;
        /**
     * Nro Factura
     */
    private String cdc;
    
    private String compraPublica;

    private FacturaDncp facturaDncp;
    
    private String nroConstancia;
    
    private String nroControlConstancia;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">

    public String getNroConstancia() {
        return nroConstancia;
    }

    public void setNroConstancia(String nroConstancia) {
        this.nroConstancia = nroConstancia;
    }

    public String getNroControlConstancia() {
        return nroControlConstancia;
    }

    public void setNroControlConstancia(String nroControlConstancia) {
        this.nroControlConstancia = nroControlConstancia;
    }
    

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    public Integer getId() {
        return id;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setRegistroInventario(String registroInventario) {
        this.registroInventario = registroInventario;
    }

    public String getRegistroInventario() {
        return registroInventario;
    }

    public Integer getIdNaturalezaVendedor() {
        return idNaturalezaVendedor;
    }

    public void setIdNaturalezaVendedor(Integer idNaturalezaVendedor) {
        this.idNaturalezaVendedor = idNaturalezaVendedor;
    }

    public String getNroDocumentoVendedor() {
        return nroDocumentoVendedor;
    }

    public void setNroDocumentoVendedor(String nroDocumentoVendedor) {
        this.nroDocumentoVendedor = nroDocumentoVendedor;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setTieneOrdenCompra(String tieneOrdenCompra) {
        this.tieneOrdenCompra = tieneOrdenCompra;
    }

    public String getTieneOrdenCompra() {
        return tieneOrdenCompra;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setAsignarNroFactura(Boolean asignarNroFactura) {
        this.asignarNroFactura = asignarNroFactura;
    }

    public Boolean getAsignarNroFactura() {
        return asignarNroFactura;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setGeneradoEdi(String generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public String getGeneradoEdi() {
        return generadoEdi;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getNroAutofactura() {
        return nroAutofactura;
    }

    public void setNroAutofactura(String nroAutofactura) {
        this.nroAutofactura = nroAutofactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }


    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getCobrado() {
        return cobrado;
    }

    public void setCobrado(String cobrado) {
        this.cobrado = cobrado;
    }

    public String getImpreso() {
        return impreso;
    }

    public void setImpreso(String impreso) {
        this.impreso = impreso;
    }

    public String getEntregado() {
        return entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public Integer getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    public Integer getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(Integer cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotalDescuentoParticular() {
        return montoTotalDescuentoParticular;
    }

    public void setMontoTotalDescuentoParticular(BigDecimal montoTotalDescuentoParticular) {
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public TipoFactura getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(TipoFactura tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }



    public List<Cuota> getFacturaCuotas() {
        return facturaCuotas;
    }

    public void setFacturaCuotas(List<Cuota> facturaCuotas) {
        this.facturaCuotas = facturaCuotas;
    }

    public String getTotalLetras() {
        return totalLetras;
    }

    public void setTotalLetras(String totalLetras) {
        this.totalLetras = totalLetras;
    }

    public String getTieneSaldo() {
        return tieneSaldo;
    }

    public void setTieneSaldo(String tieneSaldo) {
        this.tieneSaldo = tieneSaldo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getTieneRetencion() {
        return tieneRetencion;
    }

    public void setTieneRetencion(String tieneRetencion) {
        this.tieneRetencion = tieneRetencion;
    }

    public BigDecimal getMontoRetencion() {
        return montoRetencion;
    }

    public void setMontoRetencion(BigDecimal montoRetencion) {
        this.montoRetencion = montoRetencion;
    }

    public BigDecimal getMontoNotaCredito() {
        return montoNotaCredito;
    }

    public void setMontoNotaCredito(BigDecimal montoNotaCredito) {
        this.montoNotaCredito = montoNotaCredito;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public String getPagado() {
        return pagado;
    }

    public void setPagado(String pagado) {
        this.pagado = pagado;
    }

    public List<AutoFacturaDetalle> getFacturaCompraDetalles() {
        return facturaCompraDetalles;
    }

    public void setFacturaCompraDetalles(List<AutoFacturaDetalle> facturaCompraDetalles) {
        this.facturaCompraDetalles = facturaCompraDetalles;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public String getCodigoTipoOperacionCredito() {
        return codigoTipoOperacionCredito;
    }

    public void setCodigoTipoOperacionCredito(String codigoTipoOperacionCredito) {
        this.codigoTipoOperacionCredito = codigoTipoOperacionCredito;
    }

    public String getCodigoTipoFactura() {
        return codigoTipoFactura;
    }

    public void setCodigoTipoFactura(String codigoTipoFactura) {
        this.codigoTipoFactura = codigoTipoFactura;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(String generadoSet) {
        this.generadoSet = generadoSet;
    }

    public String getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(String archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public String getArchivoSet() {
        return archivoSet;
    }
    
    public void setArchivoSet(String archivoSet) {    
        this.archivoSet = archivoSet;
    }

    public String getCompraPublica() {
        return compraPublica;
    }

    public void setCompraPublica(String compraPublica) {
        this.compraPublica = compraPublica;
    }
    
    public FacturaDncp getFacturaDncp() {
        return facturaDncp;
    }

    public void setFacturaDncp(FacturaDncp facturaDncp) {
        this.facturaDncp = facturaDncp;
    }

    public List<AutoFacturaDetalle> getFacturaDetalles() {
        return facturaDetalles;
    }

    public void setFacturaDetalles(List<AutoFacturaDetalle> facturaDetalles) {
        this.facturaDetalles = facturaDetalles;
    }
    
    
    
    
//</editor-fold>
    /**
     * Constructor de la clase
     */
    public AutoFactura() {
    }

}
