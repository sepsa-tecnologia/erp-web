package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para contacto email
 *
 * @author Cristina Insfrán
 * @author Sergio D. Riveros Vazquez
 */
public class ContactoEmail {

    /**
     * Identificador del Email
     */
    private Integer idEmail;
    /**
     * Email del contacto
     */
    private String email;
    /**
     * Identificador del Email
     */
    private Integer idPersona;
    /**
     * Nombre del persona
     */
    private String persona;
    /**
     * estado
     */
    private String estado;

    /**
     * Identificador del cargo
     */
    private Integer idCargo;
    /**
     * cargo
     */
    private String cargo;
    /**
     * Identificador del tipo contacto
     */
    private Integer idTipoContacto;
    /**
     * tipo contacto
     */
    private String tipoContacto;
    /**
     * Identificador del contacto
     */
    private Integer idContacto;
    /**
     * Nombre del contacto
     */
    private String contacto;
    /**
     * Notificación del documento
     */
    private Character notificacionDocumento;
    /**
     * Notificacion de la factura
     */
    private Character notificacionFactura;
    /**
     * Notificación nota de crédito
     */
    private Character notificacionNotaCredito;
    /**
     * Notificación Factoring
     */
    private Character notificacionFactoring;
    /**
     * Notificación Recibo
     */
    private Character notificacionRecibo;

    /**
     * @return the idEmail
     */
    public Integer getIdEmail() {
        return idEmail;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getEmail() {
        return email;
    }

    public String getContacto() {
        return contacto;
    }

    /**
     * @param idEmail the idEmail to set
     */
    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    /**
     * @return the idContacto
     */
    public Integer getIdContacto() {
        return idContacto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @param idContacto the idContacto to set
     */
    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    /**
     * @return the notificacionDocumento
     */
    public Character getNotificacionDocumento() {
        return notificacionDocumento;
    }

    /**
     * @param notificacionDocumento the notificacionDocumento to set
     */
    public void setNotificacionDocumento(Character notificacionDocumento) {
        this.notificacionDocumento = notificacionDocumento;
    }

    /**
     * @return the notificacionFactura
     */
    public Character getNotificacionFactura() {
        return notificacionFactura;
    }

    /**
     * @param notificacionFactura the notificacionFactura to set
     */
    public void setNotificacionFactura(Character notificacionFactura) {
        this.notificacionFactura = notificacionFactura;
    }

    /**
     * @return the notificacionNotaCredito
     */
    public Character getNotificacionNotaCredito() {
        return notificacionNotaCredito;
    }

    /**
     * @param notificacionNotaCredito the notificacionNotaCredito to set
     */
    public void setNotificacionNotaCredito(Character notificacionNotaCredito) {
        this.notificacionNotaCredito = notificacionNotaCredito;
    }

    /**
     * @return the notificacionFactoring
     */
    public Character getNotificacionFactoring() {
        return notificacionFactoring;
    }

    /**
     * @param notificacionFactoring the notificacionFactoring to set
     */
    public void setNotificacionFactoring(Character notificacionFactoring) {
        this.notificacionFactoring = notificacionFactoring;
    }

    /**
     * @return the notificacionRecibo
     */
    public Character getNotificacionRecibo() {
        return notificacionRecibo;
    }

    /**
     * @param notificacionRecibo the notificacionRecibo to set
     */
    public void setNotificacionRecibo(Character notificacionRecibo) {
        this.notificacionRecibo = notificacionRecibo;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    /**
     * Constructor
     */
    public ContactoEmail() {

    }

    /**
     * Constructor con parametros
     *
     * @param idContacto
     * @param idEmail
     */
    public ContactoEmail(Integer idContacto, Integer idEmail) {
        this.idContacto = idContacto;
        this.idEmail = idEmail;
    }

    /**
     * Constructor
     *
     * @param idEmail
     * @param email
     * @param idContacto
     * @param contacto
     */
    public ContactoEmail(Integer idEmail, String email, Integer idContacto, String contacto) {
        this.idEmail = idEmail;
        this.email = email;
        this.idContacto = idContacto;
        this.contacto = contacto;
    }

}
