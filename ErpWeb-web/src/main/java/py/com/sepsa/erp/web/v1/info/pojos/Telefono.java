
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de telefono
 * @author Sepsa
 */
public class Telefono {

    /**
     * Identificador del telefono
     */
    private Integer id;
    /**
     * Prefijo
     */
    private String prefijo;
    /**
     * Número de telefono
     */
    private String numero;
    /**
     * Telefono principla
     */
    private String principal;

    /**
     * Código tipo telefono
     */
    private String codigoTipoTelefono;
    /**
     * Objeto tipo telefono
     */
    private TipoTelefono tipoTelefono;
    /**
     * Identificador del tipo telefono
     */
    private Integer idTipoTelefono;
    /**
     * Activo
     */
    private String activo;
  
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    
    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * @return the idTipoTelefono
     */
    public Integer getIdTipoTelefono() {
        return idTipoTelefono;
    }

    /**
     * @param idTipoTelefono the idTipoTelefono to set
     */
    public void setIdTipoTelefono(Integer idTipoTelefono) {
        this.idTipoTelefono = idTipoTelefono;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }
    
    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    /**
     * @return the principal
     */
    public String getPrincipal() {
        return principal;
    }
    
    /**
     * @param principal the principal to set
     */
    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
    /**
     * @return the prefijo
     */
    public String getPrefijo() {
        return prefijo;
    }
    
    /**
     * @param prefijo the prefijo to set
     */
    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void setCodigoTipoTelefono(String codigoTipoTelefono) {
        this.codigoTipoTelefono = codigoTipoTelefono;
    }
    
    public String getCodigoTipoTelefono() {
        return codigoTipoTelefono;
    }
    
    /**
     * @return the tipoTelefono
     */
    public TipoTelefono getTipoTelefono() {
        return tipoTelefono;
    }
    
    /**
     * @param tipoTelefono the tipoTelefono to set
     */
    public void setTipoTelefono(TipoTelefono tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }
//</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public Telefono(){
        
    }
    
   
}
