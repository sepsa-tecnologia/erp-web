/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * pojo de producto INFO
 *
 * @author Sergio D. Riveros Vazquez
 */
public class Producto {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    /**
     * Identificador del producto
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Descripción del producto
     */
    private String descripcion;
    /**
     * Código del producto
     */
    private String codigoGtin;
    /**
     * Código interno del producto
     */
    private String codigoInterno;
    /**
     * Activo
     */
    private String activo;
    /**
     * Fecha de insercion
     */
    private Date fechaInsercion;
    /**
     * Fecha de insercion Desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha de insercion Hasta
     */
    private Date fechaInsercionHasta;
    /**
     * porcentaje de impuesto
     */
    private Integer porcentajeImpuesto;
    /**
     * id de marca
     */
    private Integer idMarca;
    /**
     * marca asociada al producto
     */
    private Marca marca;
    /**
     * Idntificador de métrica
     */
    private Integer idMetrica;
        /**
     * Idntificador de métrica
     */
    private String codigoMetrica;
    /**
     * Unidad Mínima de Venta
     */
    private BigDecimal umv;
    /**
     * Múltipo de UMV
     */
    private BigDecimal multiploUmv;
    /**
     * Empresa
     */
    private Empresa empresa;
    /**
     * N° Cuenta Contable
     */
    private String cuentaContable;
    /**
     * Metrica
     */
    private Metrica metrica;
    /**
     * Fecha de vencimiento de lote
     */
    private Date fechaVencimientoLote;
    /**
     * N° de lote
     */
    private String nroLote;
    /**
     * Identificador de tipo de producto
     */
    private Integer idTipoProducto;
    /**
     * Identificador del producto padre
     */
    private Integer idPadre;
    /**
     * Porcentaje de impuestos de productos relacionados
     */
    private BigDecimal porcentajeRelacionado;
    
    private List<Producto> productosRelacionados;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Integer idMetrica) {
        this.idMetrica = idMetrica;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public BigDecimal getMultiploUmv() {
        return multiploUmv;
    }

    public void setMultiploUmv(BigDecimal multiploUmv) {
        this.multiploUmv = multiploUmv;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }
    
    public String getDescripcionLote() {
        String result = descripcion;
        if((nroLote != null && nroLote.trim().isEmpty())
                || fechaVencimientoLote != null) {
            result = String.format("%s (%s%s)", result,
                    nroLote == null ? "" : nroLote,
                    fechaVencimientoLote == null ? ""
                            : (" - " + sdf.format(fechaVencimientoLote)));
        }
        return result;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigoGtin
     */
    public String getCodigoGtin() {
        return codigoGtin;
    }

    /**
     * @param codigoGtin the codigoGtin to set
     */
    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    /**
     * @return the codigoInterno
     */
    public String getCodigoInterno() {
        return codigoInterno;
    }

    /**
     * @param codigoInterno the codigoInterno to set
     */
    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * @return the fechaInsercion
     */
    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    /**
     * @param fechaInsercion the fechaInsercion to set
     */
    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    /**
     * @return the porcentajeImpuesto
     */
    public Integer getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    /**
     * @param porcentajeImpuesto the porcentajeImpuesto to set
     */
    public void setPorcentajeImpuesto(Integer porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    /**
     * @return the idMarca
     */
    public Integer getIdMarca() {
        return idMarca;
    }

    /**
     * @param idMarca the idMarca to set
     */
    public void setIdMarca(Integer idMarca) {
        this.idMarca = idMarca;
    }

    /**
     * @return the marca
     */
    public Marca getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }

    public List<Producto> getProductosRelacionados() {
        return productosRelacionados;
    }

    public void setProductosRelacionados(List<Producto> productosRelacionados) {
        this.productosRelacionados = productosRelacionados;
    }
    
    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Producto() {

    }

    /**
     * Constructor de la clase
     */
    public Producto(int id) {
        this.id = id;
    }
}
