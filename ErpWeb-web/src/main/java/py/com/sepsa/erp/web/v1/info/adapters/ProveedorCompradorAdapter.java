
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ProveedorComprador;
import py.com.sepsa.erp.web.v1.info.remote.ProveedorCompradorService;

/**
 * Adaptador para la lista asociación comprador - proveedor
 * @author Romina E. Núñez Rojas
 */
    public class ProveedorCompradorAdapter extends DataListAdapter<ProveedorComprador> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final ProveedorCompradorService serviceProveedorComprador;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProveedorCompradorAdapter fillData(ProveedorComprador searchData) {
     
        return serviceProveedorComprador.getProveedorComprador(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProveedorCompradorAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceProveedorComprador = new ProveedorCompradorService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public ProveedorCompradorAdapter() {
        super();
        this.serviceProveedorComprador = new ProveedorCompradorService();
    }
}
