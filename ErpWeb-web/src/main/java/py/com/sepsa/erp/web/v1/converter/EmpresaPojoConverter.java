package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.EmpresaService;

/**
 * Converter para empresa
 * @author alext
 */
@FacesConverter("empresaPojoConverter")
public class EmpresaPojoConverter implements Converter{
    
      /**
     * Servicios para cliente
     */
    private EmpresaService serviceClient;
    
    @Override
    public Empresa getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new EmpresaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            Empresa empresa = new Empresa();
            empresa .setId(val);
          
            List<Empresa> empresas = serviceClient.getEmpresaList(empresa , 0, 10).getData();
            if(empresas != null && !empresas.isEmpty()) {
                return empresas.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es una empresa válida"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Empresa) {
                return String.valueOf(((Empresa)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
