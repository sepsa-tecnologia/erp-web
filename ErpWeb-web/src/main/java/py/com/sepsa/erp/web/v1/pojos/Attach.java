/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.pojos;

import java.io.InputStream;

/**
 *
 * @author SEPSA
 */
public class Attach {
    
    /**
     * Nombre del archivo
     */
    private String fileName;
    
    /**
     * Datos del archivo subido
     */
    private InputStream data;
    
    /**
     * ContentType
     */
    private String contentType;

    /**
     * Obtiene el nombre del archivo
     * @return Nombre del archivo
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Setea el nombre del archivo
     * @param fileName Nombre del archivo
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Obtiene los datos del archivo
     * @return Datos del archivo
     */
    public InputStream getData() {
        return data;
    }

    /**
     * Setea los datos del archivo
     * @param data Datos del archivo
     */
    public void setData(InputStream data) {
        this.data = data;
    }

    /**
     * Setea el contentType del archivo
     * @param contentType ContentType del archivo
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Obtiene el contentType del archivo
     * @return ContentType del archivo
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Constructor de Attach
     * @param fileName Nombre del archivo
     * @param data Datos del archivo
     * @param contentType ContentType
     */
    public Attach(String fileName, InputStream data, String contentType) {
        this.fileName = fileName;
        this.data = data;
        this.contentType = contentType;
    }

    public Attach() {
    }
   
    
    
}
