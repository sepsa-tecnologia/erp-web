/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;
import py.com.sepsa.erp.web.v1.pojos.Attach;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("notaRemisionCargaMasiva")
public class NotaRemisionCargaMasivaController implements Serializable {
    /**
     * Archivo de instructivo para la carga de NR
     */
    private StreamedContent loadNotaRemisionCarga;
    /**
     * Objeto Attach
     */
    private Attach attach;
    /**
     * NotaCredito Service
     */
    private NotaRemisionService serviceRemision;
    /**
     * Archivo
     */
    private UploadedFile file;
    /**
     * Datos para descarga
     */
    private byte[] dataDownload;
    /**
     * Bandera para descargar
     */
    private boolean downloadBtn;
    
    private String fileNameDownload = "carga_masiva_nota_remision.csv";
    public UploadedFile getFile() {
        return file;
    }
    public void setDownloadBtn(boolean downloadBtn) {
        this.downloadBtn = downloadBtn;
    }
    public boolean isDownloadBtn() {
        return downloadBtn;
    }
    public void setDataDownload(byte[] dataDownload) {
        this.dataDownload = dataDownload;
    }
    public byte[] getDataDownload() {
        return dataDownload;
    }
    public void setFile(UploadedFile file) {
        this.file = file;
    }
    public NotaRemisionService getServiceRemision() {
        return serviceRemision;
    }
    public void setServiceRemision(NotaRemisionService serviceRemision) {
        this.serviceRemision = serviceRemision;
    }

    public void setLoadNotaRemisionCarga(StreamedContent loadNotaRemisionCarga) {
        this.loadNotaRemisionCarga = loadNotaRemisionCarga;
    } 
    /**
     * Método para descargar el archivo plantilla
     *
     * @return
     */
    public StreamedContent getLoadNotaRemisionCarga() {
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/app/factura/download/nr-matriz-carga-masiva.csv");
        loadNotaRemisionCarga = new DefaultStreamedContent(stream, "text/csv", "nr-matriz-carga-masiva.csv");
        return loadNotaRemisionCarga;
    }
    /**
     * Método para subir y enviar el archivo
     *
     * @throws IOException
     */
    public void upload() throws IOException {
        if (file != null) {
            attach = new Attach(file.getFileName(), file.getInputstream(), file.getContentType());
            byte[] data = serviceRemision.sendFileMassive(attach);
            dataDownload = data;
            if (data != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado el archivo correctamente!"));
                this.downloadBtn = true;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar el archivo!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar un archivo con el formato correcto: (csv)"));
        }
    }
    /**
     * Método para subir y enviar el archivo
     *
     * @return
     */
    public StreamedContent download() {
        String contentType = file.getContentType();
        return new DefaultStreamedContent(new ByteArrayInputStream(dataDownload),
                contentType, fileNameDownload);
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.attach = new Attach();
        this.file = null;
        this.serviceRemision = new NotaRemisionService();
        this.downloadBtn = false;
    }
}