package py.com.sepsa.erp.web.v1.info.filters;

import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.FirmaDigital;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Gustavo Benítez L.
 */
public class FirmaDigitalFilter extends Filter {

    /**
     * Agrega el filtro identificador de producto.
     *
     * @param id Identificador de producto.
     * @return
     */
    public FirmaDigitalFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro activo
     *
     * @param activo Activo.
     * @return
     */
    public FirmaDigitalFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro idCliente
     *
     * @param idCliente Id del Cliente.
     * @return
     */
    public FirmaDigitalFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro idInstalacion
     *
     * @param idInstalacion Id de la instalación.
     * @return
     */
    public FirmaDigitalFilter idInstalacion(Integer idInstalacion) {
        if (idInstalacion != null) {
            params.put("idInstalacion", idInstalacion);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param firmaDigital datos de la firma digital.
     * @param page página buscada en el resultado.
     * @param pageSize tamaño de la página buscada.
     * @return
     */
    public static Map build(FirmaDigital firmaDigital, Integer page, Integer pageSize) {
        FirmaDigitalFilter filter = new FirmaDigitalFilter();

        filter
                .id(firmaDigital.getId())
                .activo(firmaDigital.getActivo())
                .idCliente(firmaDigital.getIdCliente())
                .idInstalacion(firmaDigital.getIdInstalacion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de LocalListFilter.
     */
    public FirmaDigitalFilter() {
        super();
    }
}
