
package py.com.sepsa.erp.web.v1.logger;

/**
 * Manejador de log para el modulo WEB
 * @author Daniel F. Escauriza Arza
 */
public class WebLogger extends Logger {
    
    //Nombre del log
    private final static String LOG_NAME = "admin.web.logger";

    /**
     * Obtiene el Logger
     * @return Logger
     */
    public static Logger get() {
        return new WebLogger();
    }
    
    /**
     * Constructor de WebLogger
     */
    public WebLogger() {
        super(LOG_NAME);
    }
    
}
