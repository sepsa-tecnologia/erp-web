package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.RetencionCompra;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para listado de retención de compra
 *
 * @author alext
 */
public class RetencionCompraFilter extends Filter {

    /**
     * Agrega el filtro por identificador de retención de compra
     *
     * @param id
     * @return
     */
    public RetencionCompraFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idPersona
     *
     * @param idPersona
     * @return
     */
    public RetencionCompraFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro por nroRetencion
     *
     * @param nroRetencion
     * @return
     */
    public RetencionCompraFilter nroRetencion(String nroRetencion) {
        if (nroRetencion != null && !nroRetencion.trim().isEmpty()) {
            params.put("nroRetencion", nroRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro por fechaInsercion
     *
     * @param fechaInsercion
     * @return
     */
    public RetencionCompraFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por fecha
     *
     * @param fecha
     * @return
     */
    public RetencionCompraFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));
                
            } catch (Exception e) {
                
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public RetencionCompraFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaDesde);
                
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public RetencionCompraFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public RetencionCompraFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por idFacturaCompra
     *
     * @param idFacturaCompra
     * @return
     */
    public RetencionCompraFilter idFacturaCompra(Integer idFacturaCompra) {
        if (idFacturaCompra != null) {
            params.put("idFacturaCompra", idFacturaCompra);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param retencionCompra datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(RetencionCompra retencionCompra, Integer page,
            Integer pageSize) {
        
        RetencionCompraFilter filter = new RetencionCompraFilter();
        
        filter
                .id(retencionCompra.getId())
                .idPersona(retencionCompra.getIdPersona())
                .idFacturaCompra(retencionCompra.getIdFacturaCompra())
                .nroRetencion(retencionCompra.getNroRetencion())
                .fechaInsercion(retencionCompra.getFechaInsercion())
                .fecha(retencionCompra.getFecha())
                .fechaDesde(retencionCompra.getFechaDesde())
                .fechaHasta(retencionCompra.getFechaHasta())
                .idEstado(retencionCompra.getIdEstado())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public RetencionCompraFilter() {
        super();
    }
    
}
