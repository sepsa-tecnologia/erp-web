package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.CuentaEntidadFinanciera;

/**
 * Filtro utilizado para el servicio de Cuenta Entidad Financiera
 *
 * @author Romina Núñez
 */
public class CuentaEntidadFinancieraFilter extends Filter {

    /**
     * Agrega el filtro de identificador de entidad financiera
     *
     * @param idEntidadFinanciera 
     * @return
     */
    public CuentaEntidadFinancieraFilter idEntidadFinanciera(Integer idEntidadFinanciera) {
        if (idEntidadFinanciera != null) {
            params.put("idEntidadFinanciera", idEntidadFinanciera);
        }
        return this;
    }

   

    /**
     * Construye el mapa de parametros
     *
     * @param cuentaEntidadFinaciera datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(CuentaEntidadFinanciera cuentaEntidadFinaciera, Integer page, Integer pageSize) {
        CuentaEntidadFinancieraFilter filter = new CuentaEntidadFinancieraFilter();

        filter
                .idEntidadFinanciera(cuentaEntidadFinaciera.getIdEntidadFinanciera())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de CuentaEntidadFinancieraFilter
     */
    public CuentaEntidadFinancieraFilter() {
        super();
    }
}
