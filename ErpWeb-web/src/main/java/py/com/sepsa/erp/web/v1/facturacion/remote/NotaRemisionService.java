package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.NotaRemisionDetallePojoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaRemisionFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaRemision;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemision;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaRemisionTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Nota de Remision
 *
 * @author Williams Vera
 */
public class NotaRemisionService extends APIErpFacturacion {

    /**
     * Obtiene la lista de nota de Remision
     *
     * @param notaRemision Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaRemisionAdapter getNotaRemisionList(NotaRemision notaRemision, Integer page,
            Integer pageSize) {

        NotaRemisionAdapter lista = new NotaRemisionAdapter();

        Map params = NotaRemisionFilter.build(notaRemision, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_REMISION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaRemisionAdapter.class);

            lista = (NotaRemisionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de talonarios
     *
     * @param talonario Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaRemisionTalonarioAdapter getNRTalonarioList(TalonarioPojo talonario, Integer page,
            Integer pageSize) {

        NotaRemisionTalonarioAdapter lista = new NotaRemisionTalonarioAdapter();

        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_REMISION_TALONARIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaRemisionTalonarioAdapter.class);

            lista = (NotaRemisionTalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Datos de Factura
     *
     * @param parametros
     * @return
     */
    public DatosNotaRemision getDatosNotaRemision(Map parametros) {

        DatosNotaRemision dnc = new DatosNotaRemision();

        Map params = new HashMap<>();

        params = parametros;

        HttpURLConnection conn = GET(Resource.DATOS_NOTA_REMISION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DatosNotaRemision.class);

            dnc = (DatosNotaRemision) response.getPayload();

            conn.disconnect();
        }
        return dnc;
    }
    
    /**
     * Obtiene detalles de la nota de nota de remision
     *
     * @param notaRemision Objeto
     * @return
     */
    public NotaRemisionPojo getNotaRemisionDetalle(NotaRemisionPojo notaRemision) {

        NotaRemisionPojo nc = new NotaRemisionPojo();

        Map params = new HashMap<>();

        params.put("listadoPojo", notaRemision.getListadoPojo());

        String url = "nota-remision/id/" + notaRemision.getId();

        HttpURLConnection conn = GET(url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaRemisionPojo.class);

            nc = (NotaRemisionPojo) response.getPayload();

            conn.disconnect();
        }
        return nc;
    }

    /**
     * Método para crear notaRemision
     *
     * @param notaRemision
     * @return
     */
    public BodyResponse<NotaRemision> createNotaRemision(NotaRemision notaRemision) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.NOTA_REMISION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaRemision));

            response = BodyResponse.createInstance(conn, NotaRemision.class);

        }
        return response;
    }
    
    /**
     * Método para crear nota de remisión de forma masiva
     *
     * @param cargaNotaRemision
     * @return
     */
    public byte[] sendFileMassive(Attach cargaNotaRemision) {
        byte[] result = null;
        HttpURLConnection conn = POST(Resource.CARGA_MASIVA.url, ContentType.MULTIPART);
        if (conn != null) {
            try {
                OutputStream os = conn.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(os), true);
                addFormFile(writer, os, "uploadedFile", cargaNotaRemision);
                BodyByteArrayResponse response = BodyByteArrayResponse
                        .createInstance(conn, writer);
                result = response.getPayload();
                conn.disconnect();
            } catch (IOException ex) {
                WebLogger.get().fatal(ex);
            }
        }
        return result;
    }


    /**
     * Obtiene la lista del detalle de nota de credito VERIFICAR DONDE SE USA
     * ESTE SERVICIO
     *
     * @param notaCredito Objeto
     * @param page
     * @param pageSize
     * @return
     */
//    public NotaCDetalleListAdapter getNotaCreditoDetalleList(NotaCreditoDetalle notaCredito, Integer page,
//            Integer pageSize) {
//
//        NotaCreditoDetalleListAdapter lista = new NotaCreditoDetalleListAdapter();
//
//        Map params = NotaCreditoDetalleFilter.build(notaCredito, page, pageSize);
//
//        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_DETALLE.url,
//                ContentType.JSON, params);
//
//        if (conn != null) {
//            BodyResponse response = BodyResponse.createInstance(conn,
//                    NotaCreditoDetalleListAdapter.class);
//
//            lista = (NotaCreditoDetalleListAdapter) response.getPayload();
//
//            conn.disconnect();
//        }
//        return lista;
//    }
    
    /**
     * Obtiene la lista del detalle de nota de remisión
     *
     * @param notaRemision  Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaRemisionDetalleListPojoAdapter getNotaRemisionDetalleListPojo(NotaRemisionDetallePojo notaRemision, Integer page,
            Integer pageSize) {

        NotaRemisionDetalleListPojoAdapter lista = new NotaRemisionDetalleListPojoAdapter();

        Map params = NotaRemisionDetallePojoFilter.build(notaRemision, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_REMISION_DETALLE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaRemisionDetalleListPojoAdapter.class);

            lista = (NotaRemisionDetalleListPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para anular nota de credito
     *
     * @param nota
     * @return
     */
    public BodyResponse anularNotaCredito(NotaRemision nota) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "nota-remision/anular/" + nota.getId();

        Map params = new HashMap<>();
        params.put("ignorarPeriodoAnulacion", nota.getIgnorarPeriodoAnulacion());

        HttpURLConnection conn = PUT(ANULAR_URL, ContentType.JSON, params);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(nota));

            response = BodyResponse.createInstance(conn, NotaRemision.class);

        }
        return response;
    }

    /**
     * Método para regenerar la NR
     *
     * @param notaRemision Objeto
     * @return
     */
    public BodyResponse<NotaRemision> regenerarNR(NotaRemision notaRemision) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.NOTA_REMISION.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaRemision));
            response = BodyResponse.createInstance(conn,
                    NotaRemision.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Obtiene la lista de facturas recibidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
//    public NotaCreditoTalonarioAdapter getNCTalonarioList(TalonarioPojo talonario, Integer page,
//            Integer pageSize) {
//
//        NotaCreditoTalonarioAdapter lista = new NotaCreditoTalonarioAdapter();
//
//        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);
//
//        HttpURLConnection conn = GET(Resource.NOTA_CREDITO_TALONARIO.url,
//                ContentType.JSON, params);
//
//        if (conn != null) {
//            BodyResponse response = BodyResponse.createInstance(conn,
//                    NotaCreditoTalonarioAdapter.class);
//
//            lista = (NotaCreditoTalonarioAdapter) response.getPayload();
//
//            conn.disconnect();
//        }
//        return lista;
//    }

    /**
     * Constructor de FacturacionService
     */
    public NotaRemisionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        NOTA_REMISION("NotaRemision","nota-remision"),
        CARGA_MASIVA("NotaRemisionCargaMasiva", "nota-remision/crear-masivo"),
        DATOS_NOTA_REMISION("DatosNotaRemision", "nota-remision/datos-crear"),
        NOTA_REMISION_TALONARIO("NotaRemisionTalonario","nota-remision/talonario"),
        NOTA_REMISION_DETALLE("nota remision detalle", "nota-remision-detalle");
        

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
