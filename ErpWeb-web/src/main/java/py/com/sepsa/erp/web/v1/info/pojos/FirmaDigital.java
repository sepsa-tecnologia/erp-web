package py.com.sepsa.erp.web.v1.info.pojos;

import java.util.Date;

/**
 *
 * @author Gustavo Benítez L.
 */
public class FirmaDigital {

    private Integer id;

    private Integer idCliente;

    private Date fechaVencimiento;

    private String tipoKs;

    private String clave;

    private String alias;

    private String claveAlias;

    private String contenido;

    private String activo;

    private String claveUsuario;

    private Integer idInstalacion;

    private String razonSocial;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getTipoKs() {
        return tipoKs;
    }

    public void setTipoKs(String tipoKs) {
        this.tipoKs = tipoKs;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getClaveAlias() {
        return claveAlias;
    }

    public void setClaveAlias(String claveAlias) {
        this.claveAlias = claveAlias;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public Integer getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(Integer idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /*
    Constructor
     */
    public FirmaDigital() {
    }
}
