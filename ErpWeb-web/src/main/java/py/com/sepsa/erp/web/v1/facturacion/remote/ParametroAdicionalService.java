/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.ParametroAdicional;
import py.com.sepsa.erp.web.v1.facturacion.adapters.ParametroAdicionalAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.ParametroAdicionalFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 *
 * @author Williams Vera
 */
public class ParametroAdicionalService extends APIErpFacturacion{
    
    
    /**
     * Obtiene la lista de parametros adicionales
     *
     * @param param Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public ParametroAdicionalAdapter getParametroAdicionalList(ParametroAdicional param, Integer page,
            Integer pageSize) {

        ParametroAdicionalAdapter lista = new ParametroAdicionalAdapter();

        Map params = ParametroAdicionalFilter.build(param, page, pageSize);

        HttpURLConnection conn = GET(ParametroAdicionalService.Resource.PARAMETRO_ADICIONAL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ParametroAdicionalAdapter.class);

            lista = (ParametroAdicionalAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
        /**
     * Método para editar parametro adicional
     *
     * @param cobro
     * @return
     */
    public ParametroAdicional editParametroAdicional (ParametroAdicional param) {

        ParametroAdicional parametroEdit = new ParametroAdicional();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PARAMETRO_ADICIONAL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(param));

            response = BodyResponse.createInstance(conn, ParametroAdicional.class);

            param = ((ParametroAdicional) response.getPayload());

        }
        return parametroEdit;
    }
    
    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        PARAMETRO_ADICIONAL("Parametro adicional", "parametro-adicional");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
