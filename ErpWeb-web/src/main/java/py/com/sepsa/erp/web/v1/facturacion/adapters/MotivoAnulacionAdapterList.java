
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista de motivos
 * @author Cristina Insfrán
 */
public class MotivoAnulacionAdapterList extends DataListAdapter<MotivoAnulacion> {
    /**
     * Cliente para el servicio de facturacion
     */
    private final FacturacionService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MotivoAnulacionAdapterList fillData(MotivoAnulacion searchData) {

        return serviceFactura.getMotivoAnulacionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoAnulacionAdapterList(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturacionService();
    }

    /**
     * Constructor de MotivoAnulacionAdapterList
     */
    public MotivoAnulacionAdapterList() {
        super();
        this.serviceFactura = new FacturacionService();
    }

}
