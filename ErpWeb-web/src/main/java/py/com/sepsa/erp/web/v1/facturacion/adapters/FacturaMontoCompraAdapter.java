
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
* Adaptador de la lista de facturas compras
 * @author Cristina Insfrán
 */
public class FacturaMontoCompraAdapter extends DataListAdapter<Factura> {
    
     /**
     * Cliente para el servicio de facturacion
     */
    private final FacturacionService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public FacturaMontoCompraAdapter fillData(Factura searchData) {

        return serviceFactura.getFacturaMontoComprasList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaMontoCompraAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FacturaMontoCompraAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturacionService();
    }

    /**
     * Constructor de FacturaMontoCompraAdapter
     */
    public FacturaMontoCompraAdapter() {
        super();
        this.serviceFactura = new FacturacionService();
    }
}
