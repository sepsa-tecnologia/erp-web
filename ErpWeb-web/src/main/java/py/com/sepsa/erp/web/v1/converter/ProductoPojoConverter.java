
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;

/**
 *
 * @author Cristina Insfrán
 */
@FacesConverter("productoPojoConverter")
public class ProductoPojoConverter implements Converter{
    
    /**
     * Servicio para producto
     */
    private ProductoService serviceProducto;
    
     @Override
    public Producto getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceProducto = new ProductoService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
          
            Producto prod=new Producto();
            prod.setId(val);
          
            List<Producto> productos = serviceProducto.getProductoList(prod, 0, 10).getData();
            if(productos != null && !productos.isEmpty()) {
                return productos.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un producto válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Producto) {
                return String.valueOf(((Producto)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
