package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class CobroTalonarioAdapter extends DataListAdapter<TalonarioPojo> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final CobroService serviceCobro;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CobroTalonarioAdapter fillData(TalonarioPojo searchData) {

        return serviceCobro.getCobroTalonarioList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobroTalonarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceCobro = new CobroService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public CobroTalonarioAdapter() {
        super();
        this.serviceCobro = new CobroService();
    }
}
