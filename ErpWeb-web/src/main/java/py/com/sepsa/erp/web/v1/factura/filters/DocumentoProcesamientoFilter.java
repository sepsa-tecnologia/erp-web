package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class DocumentoProcesamientoFilter extends Filter {
    
    /**
     * Agrega el filtro de idProcesamiento
     *
     * @param idProcesamiento 
     * @return
     */
    public DocumentoProcesamientoFilter idProcesamiento(Integer idProcesamiento) {
        if (idProcesamiento != null) {
            params.put("idProcesamiento", idProcesamiento);
        }
        return this;
    }


    /**
     * Construye el mapa de parametros
     *
     * @param fp  datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(DocumentoProcesamiento fp, Integer page, Integer pageSize) {
        DocumentoProcesamientoFilter filter = new DocumentoProcesamientoFilter();

        filter
                .idProcesamiento(fp.getIdProcesamiento())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public DocumentoProcesamientoFilter() {
        super();
    }
}
