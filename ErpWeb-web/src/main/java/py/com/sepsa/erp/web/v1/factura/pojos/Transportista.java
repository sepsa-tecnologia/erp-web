/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 *
 * @author Williams Vera
 */
public class Transportista {
    
    private Integer id;
    
    private Integer idEmpresa;
    
    private Integer idNaturalezaTransportista;
    
    private String razonSocial;
    
    private String ruc;
    
    private Integer verificadorRuc;
    
    private Integer idTipoDocumento;
    
    private String nroDocumento;
    
    private String nombreCompleto;
    
    private String domicilioFiscal;
    
    private String direccionChofer;
    
    private String nroDocumentoChofer;
    
    private String activo;

    public Transportista() {
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
   
    public Integer getIdNaturalezaTransportista() {
        return idNaturalezaTransportista;
    }

    public void setIdNaturalezaTransportista(Integer idNaturalezaTransportista) {
        this.idNaturalezaTransportista = idNaturalezaTransportista;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getVerificadorRuc() {
        return verificadorRuc;
    }

    public void setVerificadorRuc(Integer verificadorRuc) {
        this.verificadorRuc = verificadorRuc;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDireccionChofer() {
        return direccionChofer;
    }

    public void setDireccionChofer(String direccionChofer) {
        this.direccionChofer = direccionChofer;
    }

    public String getNroDocumentoChofer() {
        return nroDocumentoChofer;
    }

    public void setNroDocumentoChofer(String nroDocumentoChofer) {
        this.nroDocumentoChofer = nroDocumentoChofer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }
    
}
