package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoMotivoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para la vista crear motivo emision
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("tipoMotivoCreate")
public class TipoMotivoCreateController implements Serializable {

    /**
     * Cliente para el servicio de tipoMotivo
     */
    private final TipoMotivoService service;
    /**
     * POJO del motivoEmision
     */
    private TipoMotivo tipoMotivo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoMotivo getTipoMotivo() {
        return tipoMotivo;
    }

    public void setTipoMotivo(TipoMotivo tipoMotivo) {
        this.tipoMotivo = tipoMotivo;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {

        BodyResponse<TipoMotivo> respuestaTal = 
                service.setTipoMotivo(tipoMotivo);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Tipo de Motivo creado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.tipoMotivo = new TipoMotivo();
    }

    /**
     * Constructor
     */
    public TipoMotivoCreateController() {
        init();
        this.service = new TipoMotivoService();
    }

}
