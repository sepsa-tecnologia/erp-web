/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;

import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoDocumentoProcesoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoDocumentoProcesoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDocumentoProceso;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para Tipo Documento proceso
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoDocumentoProcesoService extends APIErpCore {

    /**
     * Obtiene la lista de objetos
     *
     * @param tipoDocumentoProceso Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoDocumentoProcesoListAdapter getTipoDocumentoProcesoList(TipoDocumentoProceso tipoDocumentoProceso, Integer page,
            Integer pageSize) {

        TipoDocumentoProcesoListAdapter lista = new TipoDocumentoProcesoListAdapter();

        Map params = TipoDocumentoProcesoFilter.build(tipoDocumentoProceso, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_DOCUMENTO_PROCESO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoDocumentoProcesoListAdapter.class);

            lista = (TipoDocumentoProcesoListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de clase
     */
    public TipoDocumentoProcesoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios
     */
    public enum Resource {

        //Servicios
        TIPO_DOCUMENTO_PROCESO("Tipo Documento proceso", "tipo-documento-proceso");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
