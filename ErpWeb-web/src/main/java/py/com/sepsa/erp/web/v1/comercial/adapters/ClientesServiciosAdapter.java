package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientesServicios;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientesServiciosService;

/**
 * Adaptador para lista de clientes según servicios.
 *
 * @author alext
 * @author Sergio D. Riveros Vazquez
 */
public class ClientesServiciosAdapter extends DataListAdapter<ClientesServicios> {

    /**
     * Cliente para clientes según servicios.
     */
    private final ClientesServiciosService clientesServiciosService;

    /**
     * Método para cargar la lista de clientes según servicios.
     *
     * @param searchData
     * @return
     */
    @Override
    public ClientesServiciosAdapter fillData(ClientesServicios searchData) {
        return clientesServiciosService.getClientesServiciosList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ClientesServiciosAdapter.
     *
     * @param page
     * @param pageSize
     */
    public ClientesServiciosAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.clientesServiciosService = new ClientesServiciosService();
    }

    /**
     * Constructor de ClientesServiciosAdapter.
     */
    public ClientesServiciosAdapter() {
        super();
        this.clientesServiciosService = new ClientesServiciosService();
    }
}
