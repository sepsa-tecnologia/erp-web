/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ProductoRelacionado;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;

/**
 * Adapter para producto relacionado
 *
 * @author Williams Vera
 */
public class ProductoRelacionadoAdapter extends DataListAdapter<ProductoRelacionado> {

    /**
     * Cliente para el servicio producto
     */
    private final ProductoService productoService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoRelacionadoAdapter fillData(ProductoRelacionado searchData) {

        return productoService.getProductoRelacionadoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ProductoRelacionadoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoRelacionadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoService();
    }

    /**
     * Constructor de ProductoRelacionadoAdapter
     */
    public ProductoRelacionadoAdapter() {
        super();
        this.productoService = new ProductoService();
    }
}
