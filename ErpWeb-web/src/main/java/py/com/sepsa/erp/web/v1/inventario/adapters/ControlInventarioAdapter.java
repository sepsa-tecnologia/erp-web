/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.remote.ControlInventarioService;

/**
 * Adapter para Control Inventario
 *
 * @author Romina Núñez
 */
public class ControlInventarioAdapter extends DataListAdapter<ControlInventario> {

    /**
     * Cliente remoto para tipo motivoEmision
     */
    private final ControlInventarioService controlInventario;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ControlInventarioAdapter fillData(ControlInventario searchData) {

        return controlInventario.getControlInventario(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ControlInventarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.controlInventario = new ControlInventarioService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public ControlInventarioAdapter() {
        super();
        this.controlInventario = new ControlInventarioService();
    }
}
