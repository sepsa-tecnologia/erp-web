/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.adapters;

import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * Clase para 
 * @author Jonathan
 * @param <T> Clase de Resultado
 * @param <U> Clase de Filtro
 */
public abstract class AbstractDataModel<T,U> extends LazyDataModel<T>{
    
    /**
     * Filtro
     */
    protected U filtro;

    /**
     * Obtiene el objeto de filtro
     * @param filtro objeto de filtro
     */
    public void setFiltro(U filtro) {
        this.filtro = filtro;
    }

    /**
     * Setea el objeto de filtro
     * @return objeto de filtro
     */
    public U getFiltro() {
        return filtro;
    }
    
    @Override
    public abstract List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters);
    
    /**
     * Define la llamada al servicio
     * @param filtro Filtro
     * @param first Primer resultado
     * @param pageSize Cantidad máxima de resultados
     * @return List Adapter
     */
    public abstract DataListAdapter<T> callService(U filtro, Integer first, Integer pageSize);
    
    /**
     * Realiza la búsqueda de elementos
     * @param filtro Filtro
     * @param first Primer resultado
     * @param pageSize Cantidad máxima de resultados
     * @return Lista
     */
    public List<T> find(U filtro, Integer first, Integer pageSize) {
        DataListAdapter<T> result = callService(filtro, first, pageSize);
        return result.getData();
    }
    
    /**
     * Realiza la búsqueda del primer elemento
     * @param filtro Filtro
     * @return Lista
     */
    public T findFirst(U filtro) {
        DataListAdapter<T> result = callService(filtro, 0, 1);
        return result.getData() == null || result.getData().isEmpty()
                ? null
                : result.getData().get(0);
    }
}
