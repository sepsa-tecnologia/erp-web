package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.inventario.adapters.OperacionInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;

/**
 * Controlador para lista de detalle de operación de inventario
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("operacionInventarioDetalle")
public class OperacionInventarioDetalleController implements Serializable {

    /**
     * Adaptador para lista de detalle de operaciond de inventario
     */
    private OperacionInventarioDetalleAdapter adapter;

    /**
     * Pojo de detalle de operación de inventario
     */
    private OperacionInventarioDetalle operacionDetalle;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public OperacionInventarioDetalleAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(OperacionInventarioDetalleAdapter adapter) {
        this.adapter = adapter;
    }

    public OperacionInventarioDetalle getOperacionDetalle() {
        return operacionDetalle;
    }

    public void setOperacionDetalle(OperacionInventarioDetalle operacionDetalle) {
        this.operacionDetalle = operacionDetalle;
    }
    //</editor-fold>

    /**
     * Método para obetner lista de inventario
     */
    public void getListaOperacionDetalle() {
        getAdapter().setFirstResult(0);
        this.setAdapter(getAdapter().fillData(getOperacionDetalle()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.operacionDetalle = new OperacionInventarioDetalle();
        getListaOperacionDetalle();
    }

    /**
     * Inicializa los datos del controlador
     */
    private void init() {
        this.adapter = new OperacionInventarioDetalleAdapter();
        this.operacionDetalle = new OperacionInventarioDetalle();
        getListaOperacionDetalle();
    }

    /**
     * Consturctor de la clase
     */
    public OperacionInventarioDetalleController() {
        init();
    }

}
