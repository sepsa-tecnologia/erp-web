package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.TraficoDetalle;

/**
 * Filtro utilizado para el servicio de Trafico Detalle
 *
 * @author Romina Núñez
 */
public class TraficoDetalleFilter extends Filter {

    /**
     * Agrega el filtro de identificador de historico liquidacion
     *
     * @param idHistoricoLiquidacion
     * @return
     */
    public TraficoDetalleFilter idHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        if (idHistoricoLiquidacion != null) {
            params.put("idHistoricoLiquidacion", idHistoricoLiquidacion);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de numero de línea
     *
     * @param nroLinea
     * @return
     */
    public TraficoDetalleFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param trafico datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TraficoDetalle trafico, Integer page, Integer pageSize) {
        TraficoDetalleFilter filter = new TraficoDetalleFilter();

        filter
                .idHistoricoLiquidacion(trafico.getIdHistoricoLiquidacion())
                .nroLinea(trafico.getNroLinea())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de TraficoDetalleFilter
     */
    public TraficoDetalleFilter() {
        super();
    }
}
