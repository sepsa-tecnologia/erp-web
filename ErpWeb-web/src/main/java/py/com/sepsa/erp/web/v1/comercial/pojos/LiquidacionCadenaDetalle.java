package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para listar detalle de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionCadenaDetalle {

    /**
     * Identificador de cliente.
     */
    private Integer idCliente;
    /**
     * Cliente.
     */
    private String cliente;
    /**
     * Identificador de contrato.
     */
    private Integer idContrato;
    /**
     * Identificador del producto.
     */
    private Integer idProducto;
    /**
     * Producto.
     */
    private String producto;
    /**
     * Año
     */
    private String ano;
    /**
     * Mes
     */
    private String mes;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    //</editor-fold>

    /**
     * Constructor sin parámetros.
     */
    public LiquidacionCadenaDetalle() {

    }

    public LiquidacionCadenaDetalle(String ano, String mes) {
        this.ano = ano;
        this.mes = mes;
    }

    /**
     * Constructor con parámetros.
     * 
     * @param idCliente
     * @param cliente
     * @param idContrato
     * @param idProducto
     * @param producto
     * @param ano
     * @param mes 
     */
    public LiquidacionCadenaDetalle(Integer idCliente, String cliente, Integer idContrato,
            Integer idProducto, String producto, String ano, String mes) {
        
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.idContrato = idContrato;
        this.idProducto = idProducto;
        this.producto = producto;
        this.ano = ano;
        this.mes = mes;
    }

}
