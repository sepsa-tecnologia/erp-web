package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para Vendedor
 *
 * @author Gustavo Benítez
 */
public class VendedorPojo {
    
    /**
     * Identificador del vendedor
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Empresa
     */
    private String empresa;
    /**
     * Identificador de tipo de persona
     */
    private Integer idTipoPersona;

    /**
     * Identificador de persona
     */
    private Integer idPersona;
    /**
     * Nombre del Vendedor
     */
    private String nombre;
    /**
     * Apellido
     */
    private String apellido;
    /**
     * activo
     */
    private Character activo;
    /**
     * Listado pojo
     */
    private Boolean listadoPojo;
    /**
     * Persona
     */
    private Persona persona;

    public VendedorPojo() {
    }

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }
     //</editor-fold>
    
}
