package py.com.sepsa.erp.web.v1.usuario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.system.pojos.ChangePass;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.UserFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Cliente para los servicios de usuario
 *
 * @author Daniel F. Escauriza Arza, Sergio D. Riveros Vazquez
 */
public class UserServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de usuarios
     *
     * @param usuario
     * @param page
     * @param pageSize
     * @return
     */
    public UserListAdapter getUserList(Usuario usuario, Integer page,
            Integer pageSize) {

        UserListAdapter lista = new UserListAdapter();

        Map params = UserFilter.build(usuario, page, pageSize);

        HttpURLConnection conn = GET(Resource.USER.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UserListAdapter.class);

            lista = (UserListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Metodo para obtener un usuario segun su Id
     *
     * @param id
     * @return Usuario
     */
    public Usuario get(Integer id) {
        Usuario data = new Usuario(id);
        UserListAdapter list = getUserList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para crear usuario
     *
     * @param usuario
     * @return
     */
    public BodyResponse<Usuario> setUsuario(Usuario usuario) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.USER.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(usuario));
            response = BodyResponse.createInstance(conn, Usuario.class);
        }
        return response;
    }

    /**
     * Método para editar un usuario
     *
     * @param usuario
     * @return
     */
    public Integer editUsuario(Usuario usuario) {

        Integer idUsuario = null;
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.USER.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuario));

            response = BodyResponse.createInstance(conn, Usuario.class);

            idUsuario = ((Usuario) response.getPayload()).getId();

        }
        return idUsuario;
    }

    /**
     * Método para setear userlocal
     *
     * @param userlocal
     * @return
     */
    public UsuarioLocal setUserlocal(UsuarioLocal userlocal) {

        UsuarioLocal ul = new UsuarioLocal();

        HttpURLConnection conn = POST(Resource.USER_LOCAL.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(userlocal));

            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioLocal.class);

            ul = (UsuarioLocal) response.getPayload();

            conn.disconnect();
        }
        return ul;
    }

    /**
     * Método para cambiar contraseña de los usuarios
     *
     * @param changePass Datos para cambiar la contraseña
     * @return
     */
    public MessageListAdapter changePassUsers(ChangePass changePass) {
        MessageListAdapter adapter = new MessageListAdapter();

        HttpURLConnection conn = PUT(Resource.CHANGE_PASS_USERS.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(changePass));

            BodyResponse response = BodyResponse.createInstance(conn,
                    MessageListAdapter.class);

            adapter = (MessageListAdapter) response.getPayload();

            conn.disconnect();
        }

        return adapter;
    }

    /**
     * Método para cambiar la contraseña del usuario
     *
     * @param changePass Datos para cambiar contraseña
     * @return Mensaje de respuesta del servidor
     */
    public MessageListAdapter changePass(ChangePass changePass) {

        MessageListAdapter adapter = new MessageListAdapter();

        HttpURLConnection conn = PUT(Resource.CHANGE_PASS.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(changePass));

            BodyResponse response = BodyResponse.createInstance(conn,
                    MessageListAdapter.class);

            adapter = (MessageListAdapter) response.getPayload();

            conn.disconnect();
        }

        return adapter;
    }

    /**
     * Método para enviar correo de recuperación
     *
     * @param usuario
     * @return Mensaje de respuesta del servidor
     */
    public BodyResponse correoPass(Usuario usuario) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.RECUP_PASS_CORREO.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(usuario));

            response = BodyResponse.createInstance(conn,
                    MessageListAdapter.class);

            // adapter = (MessageListAdapter) response.getPayload();
            conn.disconnect();
        }

        return response;
    }

    /**
     * Método para enviar correo de recuperación
     *
     * @param usuario
     * @return Mensaje de respuesta del servidor
     */
    public BodyResponse recuperarPass(Usuario usuario) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.RECUP_PASS.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(usuario));

            response = BodyResponse.createInstance(conn,
                    MessageListAdapter.class);

            //  adapter = (MessageListAdapter) response.getPayload();
            conn.disconnect();
        }

        return response;
    }
    
    public BodyResponse validarHash(Usuario usuario){
        
        BodyResponse response = new BodyResponse();
        
        HttpURLConnection conn = POST(Resource.VALIDAR_HASH.url, ContentType.JSON);
        
        if (conn != null) {
            Gson gson = new Gson();
            
            this.addBody(conn,gson.toJson(usuario));
            
            response = BodyResponse.createInstance(conn,
                    Usuario.class);
                    
            conn.disconnect();
 
        }
        
        return response;
    }

    /**
     * Constructor de UserServiceClient
     */
    public UserServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        CHANGE_PASS("Cambiar contraseña", "usuario/editar-password"),
        CHANGE_PASS_USERS("Cambiar contraseñas de usuarios", "usuario/editar-password-super-usuario"),
        USER("Servicios para usuario", "usuario"),
        RECUP_PASS_CORREO("Recuperación de pass correo", "usuario/enviar-correo-recuperacion"),
        RECUP_PASS("Recuperacion de pass", "usuario/recuperar-contrasena"),
        VALIDAR_HASH("Validar Hash de usuario", "usuario/validar-hash"),
        USER_LOCAL("Set User local", "usuario/usuario-local");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
