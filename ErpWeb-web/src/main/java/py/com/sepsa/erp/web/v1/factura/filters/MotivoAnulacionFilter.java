package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para el motivo anulación
 *
 * @author Cristina Insfrán
 */
public class MotivoAnulacionFilter extends Filter {

    /**
     * Agregar el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public MotivoAnulacionFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public MotivoAnulacionFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idTipoMotivoAnulacion
     *
     * @param idTipoMotivoAnulacion
     * @return
     */
    public MotivoAnulacionFilter idTipoMotivoAnulacion(Integer idTipoMotivoAnulacion) {
        if (idTipoMotivoAnulacion != null) {
            params.put("idTipoMotivoAnulacion", idTipoMotivoAnulacion);
        }
        return this;
    }

    /**
     * Agregar el filtro de codigoTipoMotivoAnulacion
     *
     * @param codigoTipoMotivoAnulacion
     * @return
     */
    public MotivoAnulacionFilter codigoTipoMotivoAnulacion(String codigoTipoMotivoAnulacion) {
        if (codigoTipoMotivoAnulacion != null && !codigoTipoMotivoAnulacion.trim().isEmpty()) {
            params.put("codigoTipoMotivoAnulacion", codigoTipoMotivoAnulacion);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idEstado
     *
     * @param activo
     * @return
     */
    public MotivoAnulacionFilter activo(Character activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param motivo
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(MotivoAnulacion motivo, Integer page, Integer pageSize) {
        MotivoAnulacionFilter filter = new MotivoAnulacionFilter();

        filter
                .idTipoMotivoAnulacion(motivo.getIdTipoMotivoAnulacion())
                .codigoTipoMotivoAnulacion(motivo.getCodigoTipoMotivoAnulacion())
                .activo(motivo.getActivo())
                .codigo(motivo.getCodigo())
                .descripcion(motivo.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de MotivoAnulacionFilter
     */
    public MotivoAnulacionFilter() {
        super();
    }
}
