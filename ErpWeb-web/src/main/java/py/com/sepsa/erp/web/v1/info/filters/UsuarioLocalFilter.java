
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;

/**
 * Filtro para el servicio Usuario Local
 * @author Cristina Insfrán
 */
public class UsuarioLocalFilter extends Filter{
    
     /**
     * Agrega el filtro de identificador del usuario
     *
     * @param idUsuario Identificador del usuario
     * @return
     */
    public UsuarioLocalFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public UsuarioLocalFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }
    
      /**
     * Agrega el filtro de identificador del local
     *
     * @param idLocal Identificador del usuario
     * @return
     */
    public UsuarioLocalFilter idLocal(Integer idLocal) {
        if (idLocal != null) {
            params.put("idLocal", idLocal);
        }
        return this;
    }
    
      /**
     * Agrega el filtro del estado
     *
     * @param estado Identificador del usuario
     * @return
     */
    public UsuarioLocalFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }
    
    
    /**
     * Construye el mapa de parametros
     *
     * @param usuariolocal datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(UsuarioLocal usuariolocal, Integer page, Integer pageSize) {
        UsuarioLocalFilter filter = new UsuarioLocalFilter();

        filter
                .idUsuario(usuariolocal.getIdUsuario())
                .idPersona(usuariolocal.getIdPersona())
                .idLocal(usuariolocal.getIdLocal())                
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor UsuarioLocalFilter
     */
    public UsuarioLocalFilter() {
        super();
    }
}
