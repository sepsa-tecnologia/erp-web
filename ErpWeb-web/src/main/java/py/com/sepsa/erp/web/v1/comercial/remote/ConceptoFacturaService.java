package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ConceptoFacturaAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ConceptoFacturaFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ConceptoFactura;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de ConceptoFactura
 *
 * @author Romina Núñez
 */
public class ConceptoFacturaService extends APIErpComercial {

    /**
     * Obtiene la lista de tipo tarifa
     *
     * @param conceptoFactura  
     * @param page
     * @param pageSize
     * @return
     */
    public ConceptoFacturaAdapter getConceptoFacturaList(ConceptoFactura conceptoFactura, Integer page,
            Integer pageSize) {

        ConceptoFacturaAdapter lista = new ConceptoFacturaAdapter();

        Map params = ConceptoFacturaFilter.build(conceptoFactura, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONCEPTOFACTURA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ConceptoFacturaAdapter.class);

            lista = (ConceptoFacturaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
    /**
     * Constructor de ConceptoFacturaService
     */
    public ConceptoFacturaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        CONCEPTOFACTURA("ConceptoFactura", "concepto-factura");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
