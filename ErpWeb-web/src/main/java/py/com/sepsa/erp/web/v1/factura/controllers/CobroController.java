package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez, Cristina Insfrán
 */
@ViewScoped
@Named("cobro")
public class CobroController implements Serializable {

    /**
     * Adaptador para la lista de cobros
     */
    private CobroAdapter adapterCobro;
    /**
     * POJO Cobro
     */
    private Cobro cobroFilter;
    /**
     * POJO Cobro
     */
    private Cobro cobroEdit;
    /**
     * Servicio Cobro
     */
    private CobroService serviceCobro;
    /**
     * Dato del Recibo
     */
    private String idRecibo;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private FileServiceClient fileServiceClient;
    private boolean error = false;
    
     /**
     * Obtiene el valor a exportar para el estado
     *
     * @param estado Valor
     * @return Valor a ser exportado
     */
    public String exportState(String estado) {
        String value = "--";
        if (estado == null) {
            return value;
        } else if (estado.equals("R")) {
            value = "Reserva";
        } else if (estado.equals("A")) {
            value = "Anulado";
        } else if (estado.equals("C")) {
            value = "Confirmado";
        } else {
            value = "Antiguo";
        }
        return value;
    }

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @return the fileServiceClient
     */
    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    /**
     * @param fileServiceClient the fileServiceClient to set
     */
    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    /**
     * @return the idRecibo
     */
    public String getIdRecibo() {
        return idRecibo;
    }

    /**
     * @param idRecibo the idRecibo to set
     */
    public void setIdRecibo(String idRecibo) {
        this.idRecibo = idRecibo;
    }

    public CobroAdapter getAdapterCobro() {
        return adapterCobro;
    }

    public void setServiceCobro(CobroService serviceCobro) {
        this.serviceCobro = serviceCobro;
    }

    public CobroService getServiceCobro() {
        return serviceCobro;
    }

    public void setCobroEdit(Cobro cobroEdit) {
        this.cobroEdit = cobroEdit;
    }

    public Cobro getCobroEdit() {
        return cobroEdit;
    }

    public Cobro getCobroFilter() {
        return cobroFilter;
    }

    public void setAdapterCobro(CobroAdapter adapterCobro) {
        this.adapterCobro = adapterCobro;
    }

    public void setCobroFilter(Cobro cobroFilter) {
        this.cobroFilter = cobroFilter;
    }

//</editor-fold>
    /**
     * Método para llenar el adapter
     */
    public void filterCobro() {

        if (cobroFilter.getFechaDesde() == null || cobroFilter.getFechaHasta() == null) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                    "Se debe seleccionar el rango de fechas de los cobros");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            setError(true);
        } else {
            Long dif = (cobroFilter.getFechaDesde().getTime() - cobroFilter.getFechaHasta().getTime()) / (24 * 60 * 60 * 1000);
            if (dif >= 31) {
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                        "La máxima diferencia entre las fechas inicio y fin debe ser de 1 mes");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                setError(true);
            }
        }
        if (isError() == false) {
            adapterCobro = adapterCobro.fillData(cobroFilter);
        }

    }

    /**
     * Método para editar el cobro
     *
     * @param event
     */
    public void onRowEditCobro(RowEditEvent event) {
        cobroEdit = new Cobro();

        cobroEdit.setId(((Cobro) event.getObject()).getId());
        cobroEdit.setNroRecibo(((Cobro) event.getObject()).getNroRecibo());
        cobroEdit.setFecha(((Cobro) event.getObject()).getFecha());

        Cobro id = serviceCobro.editCobro(cobroEdit);
        if (id != null) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Error al realizar el cambio!"));
        }
    }

    

    public void downloadRecibo() {
        byte[] pdf = null;

        pdf = fileServiceClient.download(idRecibo);
    }

    /**
     * Método para descargar recibo en pdf
     *
     * @param cobro
     * @return
     */
    public StreamedContent download(Cobro cobro) {
        String contentType = ("application/pdf");
        String fileName = cobro.getNroRecibo() + ".pdf";
        String url = "cobro/consulta/" + cobro.getId();
        byte[] data = fileServiceClient.download(url);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.cobroFilter = new Cobro();
        this.adapterCobro = new CobroAdapter();
        this.serviceCobro = new CobroService();
        this.fileServiceClient = new FileServiceClient();

        Calendar today = Calendar.getInstance();
        cobroFilter.setFechaHasta(today.getTime());

        today.set(Calendar.DAY_OF_MONTH, 1);

        cobroFilter.setFechaDesde(today.getTime());

        filterCobro();
    }

}
