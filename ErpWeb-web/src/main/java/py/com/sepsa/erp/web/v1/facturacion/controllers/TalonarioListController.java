/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioLocalRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioLocal;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.TalonarioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para la vista Talonario
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("talonarioList")
public class TalonarioListController implements Serializable {

    /**
     * Adaptador para la lista de talonario
     */
    private TalonarioAdapter talonarioAdapterList;
    /**
     * POJO de Talonario
     */
    private Talonario talonarioFilter;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private TipoDocumentoAdapter tipoDocumentoAdapterList;
    /**
     * POJO de tipo documento
     */
    private TipoDocumento tipoDocumentoFilter;
    /**
     * Adaptador para la lista de local
     */
    private LocalListAdapter localAdapterList;
    /**
     * Adaptador de la lista de local para autocomplete
     */
    private LocalListAdapter localAdapterListAutoComplete;
    /**
     * POJO de local
     */
    private Local localFilter;
    /**
     * Pojo de local para autocomplete
     */
    private Local localComplete;
    /**
     * Datos del talonario para autocomplete
     */
    private Talonario talonarioComplete;
    /**
     * Adaptador para la lista de talonario local
     */
    private TalonarioLocalRelacionadoAdapter adapterTalonarioLocalRelacionado;
    /**
     * Talonario Local
     */
    private TalonarioLocal talonarioLocalFilter;
    /**
     * Talonario service
     */
    private TalonarioService serviceTalonario;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the talonarioAdapterList
     */
    public TalonarioAdapter getTalonarioAdapterList() {
        return talonarioAdapterList;
    }
    
    public void setServiceTalonario(TalonarioService serviceTalonario) {
        this.serviceTalonario = serviceTalonario;
    }
    
    public TalonarioService getServiceTalonario() {
        return serviceTalonario;
    }
    
    public TalonarioLocal getTalonarioLocalFilter() {
        return talonarioLocalFilter;
    }
    
    public void setTalonarioLocalFilter(TalonarioLocal talonarioLocalFilter) {
        this.talonarioLocalFilter = talonarioLocalFilter;
    }
    
    public void setAdapterTalonarioLocalRelacionado(TalonarioLocalRelacionadoAdapter adapterTalonarioLocalRelacionado) {
        this.adapterTalonarioLocalRelacionado = adapterTalonarioLocalRelacionado;
    }
    
    public TalonarioLocalRelacionadoAdapter getAdapterTalonarioLocalRelacionado() {
        return adapterTalonarioLocalRelacionado;
    }

    /**
     * @param talonarioAdapterList the talonarioAdapterList to set
     */
    public void setTalonarioAdapterList(TalonarioAdapter talonarioAdapterList) {
        this.talonarioAdapterList = talonarioAdapterList;
    }

    /**
     * @return the talonarioFilter
     */
    public Talonario getTalonarioFilter() {
        return talonarioFilter;
    }

    /**
     * @param talonarioFilter the talonarioFilter to set
     */
    public void setTalonarioFilter(Talonario talonarioFilter) {
        this.talonarioFilter = talonarioFilter;
    }

    /**
     * @return the clientAdapter
     */
    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    /**
     * @param clientAdapter the clientAdapter to set
     */
    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public TipoDocumentoAdapter getTipoDocumentoAdapterList() {
        return tipoDocumentoAdapterList;
    }
    
    public void setTipoDocumentoAdapterList(TipoDocumentoAdapter tipoDocumentoAdapterList) {
        this.tipoDocumentoAdapterList = tipoDocumentoAdapterList;
    }
    
    public TipoDocumento getTipoDocumentoFilter() {
        return tipoDocumentoFilter;
    }
    
    public void setTipoDocumentoFilter(TipoDocumento tipoDocumentoFilter) {
        this.tipoDocumentoFilter = tipoDocumentoFilter;
    }
    
    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }
    
    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }
    
    public Local getLocalFilter() {
        return localFilter;
    }
    
    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }
    
    public Local getLocalComplete() {
        return localComplete;
    }
    
    public void setLocalComplete(Local localComplete) {
        this.localComplete = localComplete;
    }
    
    public Talonario getTalonarioComplete() {
        return talonarioComplete;
    }
    
    public void setTalonarioComplete(Talonario talonarioComplete) {
        this.talonarioComplete = talonarioComplete;
    }
    
    public LocalListAdapter getLocalAdapterListAutoComplete() {
        return localAdapterListAutoComplete;
    }
    
    public void setLocalAdapterListAutoComplete(LocalListAdapter localAdapterListAutoComplete) {
        this.localAdapterListAutoComplete = localAdapterListAutoComplete;
    }

    //</editor-fold>
    /**
     * Método para obtener talonarios
     */
    public void getTalonario() {
        this.setTalonarioAdapterList(getTalonarioAdapterList().fillData(getTalonarioFilter()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.localComplete = new Local();
        this.cliente = new Cliente();
        this.clientAdapter = new ClientListAdapter();
        this.talonarioFilter = new Talonario();
        getTalonario();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        
        clientAdapter = clientAdapter.fillData(cliente);
        
        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        talonarioFilter.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoDocumento() {
        this.setTipoDocumentoAdapterList(getTipoDocumentoAdapterList().fillData(getTipoDocumentoFilter()));
    }

    /**
     * Método para obtener local
     */
    public void getLocales() {
        this.setLocalAdapterList(getLocalAdapterList().fillData(getLocalFilter()));
    }

    /**
     * Metodo para redirigir a la vista Editar Talonario
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("talonario-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local localComplete = new Local();
        localComplete.setDescripcion(query);
        localAdapterListAutoComplete = localAdapterListAutoComplete.fillData(localComplete);
        return localAdapterListAutoComplete.getData();
    }

    /**
     * Selecciona el local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        talonarioFilter.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Método para filtrar locales por talonario
     *
     * @param idTalonario
     */
    public void filterLocalTalonario(Integer idTalonario) {
        talonarioLocalFilter.setIdTalonario(idTalonario);
        adapterTalonarioLocalRelacionado = adapterTalonarioLocalRelacionado.fillData(talonarioLocalFilter);
        
    }

    /**
     * Método para editar la asociación
     *
     * @param idTalonario
     * @param idLocal
     * @param activo
     */
    public void editarAsociacion(Integer idTalonario, Integer idLocal, String activo) {
        TalonarioLocal talLocalEdit = new TalonarioLocal();
        
        if (activo.equalsIgnoreCase("S")) {
            talLocalEdit.setActivo("N");
        } else {
            talLocalEdit.setActivo("S");
        }
        
        talLocalEdit.setIdLocal(idLocal);
        talLocalEdit.setIdTalonario(idTalonario);
        
        BodyResponse<TalonarioLocal> respuestaTalonarioLocal = serviceTalonario.editAsociacionTalonarioLocal(talLocalEdit);
        if (respuestaTalonarioLocal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Asociacion editada correctamente!"));
            filterLocalTalonario(idTalonario);
        }
        
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.setTalonarioFilter(new Talonario());
            this.setTalonarioAdapterList(new TalonarioAdapter());
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.tipoDocumentoAdapterList = new TipoDocumentoAdapter();
            this.tipoDocumentoFilter = new TipoDocumento();
            this.localAdapterList = new LocalListAdapter();
            this.localFilter = new Local();
            this.localComplete = new Local();
            this.localAdapterListAutoComplete = new LocalListAdapter();
            this.talonarioComplete = new Talonario();
            this.adapterTalonarioLocalRelacionado = new TalonarioLocalRelacionadoAdapter();
            this.talonarioLocalFilter = new TalonarioLocal();
            this.serviceTalonario = new TalonarioService();
            getTalonario();
            getTipoDocumento();
            getLocales();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
