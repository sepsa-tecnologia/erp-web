package py.com.sepsa.erp.web.v1.usuario.remote;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.usuario.adapters.PerfilListAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.PerfilFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Perfil;
import py.com.sepsa.erp.web.v1.usuario.pojos.PerfilConverter;

/**
 * Cliente para el servicio de Perfil
 *
 * @author Cristina Insfrán, Romina Núñez
 */
public class PerfilServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de perfiles
     *
     * @param perfil
     * @param page
     * @param pageSize
     * @return
     */
    public PerfilListAdapter getPerfilList(Perfil perfil, Integer page,
            Integer pageSize) {

        PerfilListAdapter lista = new PerfilListAdapter();

        Map params = PerfilFilter.build(perfil, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERFIL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PerfilListAdapter.class);

            lista = (PerfilListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Método para setea la configuracion valor
     *
     * @param perfil
     * @return
     */
    public Perfil editPerfil(Perfil perfil) {

        Perfil perfilEdit = new Perfil();
        
        PerfilConverter convertedPerfil = new PerfilConverter();
        convertedPerfil.convert(perfil);
        
        HttpURLConnection conn = PUT(Resource.PERFIL.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(convertedPerfil));

            BodyResponse response = BodyResponse.createInstance(conn,
                    Perfil.class);

            perfilEdit = (Perfil) response.getPayload();

            conn.disconnect();
        }
        return perfilEdit;
    }

    /**
     * Método para setear configuración
     *
     * @param perfil
     * @return
     */
    public BodyResponse<Perfil> createPerfil(Perfil perfil) {
        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERFIL.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(perfil));

            response = BodyResponse.createInstance(conn,
                    Perfil.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Constructor de PerfilServiceClient
     */
    public PerfilServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        PERFIL("Servicio Perfil", "perfil");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
