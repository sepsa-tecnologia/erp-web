
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Rol;
import py.com.sepsa.erp.web.v1.info.remote.RolServiceClient;

/**
 * Adaptador de la lista de Roles
 * @author Cristina Insfrán
 */
public class RolListAdapter extends DataListAdapter<Rol> {
    
    /**
     * Cliente para el servicio de Rol
     */
    private final RolServiceClient rolClient;
 
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public RolListAdapter fillData(Rol searchData) {
     
        return rolClient.getRolList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public RolListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.rolClient = new RolServiceClient();
    }

    /**
     * Constructor de RolListAdapter
     */
    public RolListAdapter() {
        super();
        this.rolClient = new RolServiceClient();
    }
}
