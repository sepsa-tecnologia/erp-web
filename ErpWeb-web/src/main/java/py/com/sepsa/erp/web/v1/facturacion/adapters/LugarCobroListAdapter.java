
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista lugar cobro
 * @author Cristina Insfrán
 */
public class LugarCobroListAdapter extends DataListAdapter<LugarCobro> {
      /**
     * Cliente para los servicios de facturación
     */
    private final FacturacionService facturacionClient;
   
    
    @Override
    public LugarCobroListAdapter fillData(LugarCobro searchData) {
        return facturacionClient.getLugarCobroList(searchData,getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de LugarCobroAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public LugarCobroListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.facturacionClient = new FacturacionService();
    }

    /**
     * Constructor de LugarCobroAdapter
     */
    public LugarCobroListAdapter() {
        super();
        this.facturacionClient = new FacturacionService();
    }
}
