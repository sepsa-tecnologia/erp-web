package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para Factura
 *
 * @author Romina Núñez,Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class AutoFacturaPojo {

    /**
     * Identificador del la factura
     */
    private Integer id;
       /**
     * Identificador del la factura
     */
    private Integer idEstado;
    /**
     * Identificador del la factura
     */
    private Integer idEmpresa;
    /**
     * Nro Factura
     */
    private String empresa;
    /**
     * Nro Factura
     */
    private String cdc;
    /**
     * Cantidad
     */
    private Integer idTalonario;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaInicioVigenciaTimbrado;
    /**
     * Identificador de la moneda
     */
    private String timbrado;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de la moneda
     */
    private Integer idProcesamiento;
    /**
     * Identificador de la moneda
     */
    private String moneda;
    /**
     * Identificador de la moneda
     */
    private String codigoMoneda;
    /**
     * Identificador del tipo factura
     */
    private Integer idTipoFactura;
    /**
     * Identificador de la moneda
     */
    private String tipoFactura;
    /**
     * Identificador de la moneda
     */
    private String codigoTipoFactura;
    /**
     * Nro Factura
     */
    private String nroAutofactura;
    /**
     * Fecha
     */
    private Date fecha;
    /**
     * Nombre del vendedor
     */
    private String nombreVendedor;
    /**
     * Anulado
     */
    private String anulado;

    /**
     * Cobrado
     */
    private String cobrado;

    /**
     * Impuesto
     */
    private String impreso;
    /**
     * Digital
     */
    private String digital;

    /**
     * Entregado
     */
    private String entregado;
    /**
     * Generado SET
     */
    private String generadoSet;
    /**
     * Generado EDI
     */
    private String generadoEdi;
    /**
     * Archivo EDI
     */
    private String archivoEdi;
    /**
     * Archivo SET
     */
    private String archivoSet;
    /**
     * Archivo SET
     */
    private String estadoSincronizado;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto IVA total
     */
    private BigDecimal montoTotalDescuentoParticular;
    /**
     * Monto IVA total
     */
    private BigDecimal montoTotalDescuentoGlobal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total factura
     */
    private BigDecimal montoTotalFactura;
    /**
     * Monto total factura
     */
    private BigDecimal montoTotalGuaranies;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * Fecha
     */
    private Date fechaDesde;
    /**
     * Fecha
     */
    private Date fechaHasta;
    /**
     * Cod Estado
     */
    private String codigoEstado;
    /**
     * Identificador del motivo
     */
    private Integer idMotivoAnulacion;
    /**
     * Motivo anulación
     */
    private String observacionAnulacion;
    /**
     * Nro Factura
     */
    private String direccion;
    /**
     * Nro Factura
     */
    private String telefono;
    /**
     * Nro Factura
     */
    private String email;
    /**
     * Saldo
     */
    private BigDecimal saldo;
    
    private String razonSocial;

    private String ruc;

    private BigDecimal montoTotalAutofactura;
    
    private String nroDocumentoVendedor;
    
    

    public String getNroDocumentoVendedor() {
        return nroDocumentoVendedor;
    }

    public void setNroDocumentoVendedor(String nroDocumentoVendedor) {
        this.nroDocumentoVendedor = nroDocumentoVendedor;
    }
    
  
    public BigDecimal getMontoTotalAutofactura() {
        return montoTotalAutofactura;
    }

    public void setMontoTotalAutofactura(BigDecimal montoTotalAutofactura) {
        this.montoTotalAutofactura = montoTotalAutofactura;
    }
    
    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    
    
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getDireccion() {
        return direccion;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaInicioVigenciaTimbrado() {
        return fechaInicioVigenciaTimbrado;
    }

    public void setFechaInicioVigenciaTimbrado(Date fechaInicioVigenciaTimbrado) {
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public String getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public String getCodigoTipoFactura() {
        return codigoTipoFactura;
    }

    public void setCodigoTipoFactura(String codigoTipoFactura) {
        this.codigoTipoFactura = codigoTipoFactura;
    }

    public String getNroAutofactura() {
        return nroAutofactura;
    }

    public void setNroAutofactura(String nroAutofactura) {
        this.nroAutofactura = nroAutofactura;
    }


    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }


    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }


    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getCobrado() {
        return cobrado;
    }

    public void setCobrado(String cobrado) {
        this.cobrado = cobrado;
    }

    public String getImpreso() {
        return impreso;
    }

    public void setImpreso(String impreso) {
        this.impreso = impreso;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getEntregado() {
        return entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public String getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(String generadoSet) {
        this.generadoSet = generadoSet;
    }

    public String getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(String generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public String getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(String archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public String getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(String archivoSet) {
        this.archivoSet = archivoSet;
    }

    public String getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setEstadoSincronizado(String estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotalDescuentoParticular() {
        return montoTotalDescuentoParticular;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public void setMontoTotalDescuentoParticular(BigDecimal montoTotalDescuentoParticular) {
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }
    

    /**
     * Constructor de la clase
     */
    public AutoFacturaPojo() {
    }

}
