/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.MetricaAdapter;
import py.com.sepsa.erp.web.v1.info.filters.MetricaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio producto info
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class MetricaService extends APIErpCore {

   
    /**
     * Obtiene la lista de metricas.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public MetricaAdapter getMetricaList(Metrica metrica, Integer page,
            Integer pageSize) {

        MetricaAdapter lista = new MetricaAdapter();

        Map params = MetricaFilter.build(metrica, page, pageSize);

        HttpURLConnection conn = GET(Resource.METRICA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MetricaAdapter.class);

            lista = (MetricaAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

//    /**
//     * Método para crear producto
//     *
//     * @param producto
//     * @return
//     */
//    public BodyResponse<Producto> setProducto(Producto producto) {
//        BodyResponse response = new BodyResponse();
//        HttpURLConnection conn = POST(Resource.PRODUCTO.url, ContentType.JSON);
//        if (conn != null) {
//            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
//            this.addBody(conn, gson.toJson(producto));
//            response = BodyResponse.createInstance(conn, Producto.class);
//        }
//        return response;
//    }
//
//
//    /**
//     *
//     * @param id
//     * @return
//     */
//    public Producto get(Integer id) {
//        Producto data = new Producto(id);
//        ProductoAdapter list = getProductoList(data, 0, 1);
//        data = list != null
//                && list.getData() != null
//                && !list.getData().isEmpty()
//                ? list.getData().get(0)
//                : null;
//
//        return data;
//    }
//
//    /**
//     * Método para editar Producto
//     *
//     * @param producto
//     * @return
//     */
//    public BodyResponse<Producto> editProducto(Producto producto) {
//        BodyResponse response = new BodyResponse();
//        HttpURLConnection conn = PUT(Resource.PRODUCTO.url, ContentType.JSON);
//        if (conn != null) {
//            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
//            this.addBody(conn, gson.toJson(producto));
//            response = BodyResponse.createInstance(conn, Producto.class);
//        }
//        return response;
//    }


    /**
     * Constructor de MetricaService
     */
    public MetricaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        METRICA("Metrica", "metrica");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
