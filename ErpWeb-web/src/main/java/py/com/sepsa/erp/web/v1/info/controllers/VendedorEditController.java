/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.VendedorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.info.remote.VendedorService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Gustavo Benítez
 */
@ViewScoped
@Named("vendedorEdit")
public class VendedorEditController implements Serializable {

    /**
     * Vendedor Filter
     */
    private VendedorPojo vendedorfilter;
    /**
     * Vendedor
     */
    private VendedorPojo vendedorEdit;
    /**
     * Adaptador de vendedor
     */
    private VendedorListAdapter vendedorAdapter;
    /**
     * Servicio Cliente
     */
    private VendedorService serviceVendedor;
    /**
     * Adaptador de empresa.
     */
    private EmpresaAdapter adapterEmpresa;
    /**
     * Objeto empresa.
     */
    private Empresa empresa;
    /**
     * Objeto persona
     */
    private Persona persona;
    /**
     * Adapter Persona
     */
    private PersonaListAdapter adapterPersona;
    /**
     * Dato del vendedor
     */
    private String idVendedor;
    /**
     * Dato para habilitación de panel
     */
    private boolean habilitarEditVendedor;

//<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }

    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public boolean isHabilitarEditVendedor() {
        return habilitarEditVendedor;
    }

    public void setHabilitarEditVendedor(boolean habilitarEditVendedor) {
        this.habilitarEditVendedor = habilitarEditVendedor;
    }

    public VendedorPojo getVendedorfilter() {
        return vendedorfilter;
    }

    public void setVendedorfilter(VendedorPojo vendedorfilter) {
        this.vendedorfilter = vendedorfilter;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public VendedorPojo getVendedorEdit() {
        return vendedorEdit;
    }

    public void setVendedorEdit(VendedorPojo vendedorEdit) {
        this.vendedorEdit = vendedorEdit;
    }

    public VendedorListAdapter getVendedorAdapter() {
        return vendedorAdapter;
    }

    public void setVendedorAdapter(VendedorListAdapter vendedorAdapter) {
        this.vendedorAdapter = vendedorAdapter;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }

    public VendedorService getServiceVendedor() {
        return serviceVendedor;
    }

    public void setServiceVendedor(VendedorService serviceVendedor) {
        this.serviceVendedor = serviceVendedor;
    }
//</editor-fold>

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa emp = new Empresa();
        emp.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(emp);
        return adapterEmpresa.getData();
    }

    /* Método para obtener personas
     *
     * @param query
     * @return
     */
    public List<Persona> completeQueryPersona(String query) {
        Persona per = new Persona();
        per.setNombre(query);

        this.adapterPersona = this.adapterPersona.fillData(per);
        return this.adapterPersona.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaEdit(SelectEvent event) {
        empresa.setId(((Empresa) event.getObject()).getId());
        this.vendedorfilter.setIdEmpresa(empresa.getId());
    }

    public void habilitarEdicionVendedorPanel() {
        this.habilitarEditVendedor = true;
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectPersonaEdit(SelectEvent event) {
        persona.setId(((Persona) event.getObject()).getId());
        this.vendedorfilter.setIdPersona(persona.getId());
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clearFilter() {
        this.vendedorEdit = new VendedorPojo();
        this.vendedorfilter = new VendedorPojo();
        this.vendedorAdapter = new VendedorListAdapter();
        this.empresa = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
        this.persona = new Persona();
        this.adapterPersona = new PersonaListAdapter();
    }

    /**
     * Metodo para editar Vendedor
     */
    public void editarVendedor() {
        vendedorEdit.setId(vendedorfilter.getId());
        vendedorEdit.setIdEmpresa(vendedorfilter.getIdEmpresa());
        vendedorEdit.setIdPersona(vendedorfilter.getIdPersona());
        vendedorEdit.setActivo(vendedorfilter.getActivo());

        BodyResponse<VendedorPojo> respuestaOC = serviceVendedor.edit(vendedorEdit);
        if (respuestaOC.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Edicion Correcta!"));
            habilitarEditVendedor = false;

        } else {
            respuestaOC.getStatus().getMensajes().forEach(mensaje -> {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", mensaje.getDescripcion()));
            });
        }
    }

    /**
     * Metodo para obtener la lista de vendedores
     */
    public void filterVendedor() {
        this.vendedorfilter.setId(Integer.parseInt(idVendedor));
        this.vendedorAdapter = vendedorAdapter.fillData(vendedorfilter);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.habilitarEditVendedor = false;
            this.vendedorEdit = new VendedorPojo();
            this.vendedorfilter = new VendedorPojo();
            this.vendedorAdapter = new VendedorListAdapter();
            this.empresa = new Empresa();
            this.adapterEmpresa = new EmpresaAdapter();
            this.persona = new Persona();
            this.adapterPersona = new PersonaListAdapter();
            this.serviceVendedor = new VendedorService();

            Map<String, String> params = FacesContext.getCurrentInstance().
                    getExternalContext().getRequestParameterMap();

            this.idVendedor = params.get("idVendedor");
            filterVendedor();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
