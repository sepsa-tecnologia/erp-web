package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.remote.CanalVentaService;

/**
 *
 * @author Cristina Insfrán
 */
@FacesConverter("canalVentaPojoConverter")
public class CanalVentaPojoConverter implements Converter {
    
    /**
     * Servicio para canal de venta
     */
    private CanalVentaService serviceCanal;
    
     @Override
    public CanalVenta getAsObject(FacesContext fc, UIComponent uic, String value) {

        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceCanal = new CanalVentaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}

            CanalVenta canal=new CanalVenta();
            canal.setId(val);
          
            List<CanalVenta> canales = serviceCanal.getCanalVentaList(canal, 0, 10).getData();
            if(canales != null && !canales.isEmpty()) {
                return canales.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "No es un canal de ventas válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof CanalVenta) {
                return String.valueOf(((CanalVenta) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }   
}
