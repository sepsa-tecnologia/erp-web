
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;

/**
 * Adaptador para la lista de Cliente
 * @author Romina Nuñez
 */
public class ClientPojoAdapter extends DataListAdapter<ClientePojo> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final ClientServiceClient serviceClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ClientPojoAdapter fillData(ClientePojo searchData) {
     
        return serviceClient.getClientPojoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ClientPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClient = new ClientServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public ClientPojoAdapter() {
        super();
        this.serviceClient = new ClientServiceClient();
    }
    
}
