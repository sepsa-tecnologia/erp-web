
package py.com.sepsa.erp.web.v1.usuario.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioPerfilRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.UsuarioPerfilRelacionadoFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;

/**
 * Cliente para el servico de Usuario Perfil Relacionado
 * @author Romina Núñez
 */
public class UsuarioPerfilRelacionadoService extends APIErpCore{
    
    
    /**
     * Obtiene la lista de usuario perfiles
     *
     * @param up
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioPerfilRelacionadoAdapter getUsuarioPerfilRelacionadoList(UsuarioPerfilRelacionado up, Integer page,
            Integer pageSize) {
       
        UsuarioPerfilRelacionadoAdapter lista = new UsuarioPerfilRelacionadoAdapter();
        
        Map params = UsuarioPerfilRelacionadoFilter.build(up, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_PERFIL_RELACIONADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioPerfilRelacionadoAdapter.class);

            lista = (UsuarioPerfilRelacionadoAdapter) response.getPayload();
           
            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Constructor de UsuarioPerfilRelacionadoService
     */
    public UsuarioPerfilRelacionadoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        USUARIO_PERFIL_RELACIONADO("Usuario Perfil Relacionado", "usuario-perfil/relacionado");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
