
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.CuentaEntidadFinanciera;
import py.com.sepsa.erp.web.v1.facturacion.remote.CuentaEntidadFinancieraService;

/**
 * Adaptador de la lista de cuenta entidad financiera
 * @author Romina Núñez
 */
public class CuentaEntidadFinancieraAdapter extends DataListAdapter<CuentaEntidadFinanciera> {
    
    /**
     * Cliente para el servicio de cuenta entidad financiera
     */
    private final CuentaEntidadFinancieraService serviceCuentaEntidadFinanciera;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CuentaEntidadFinancieraAdapter fillData(CuentaEntidadFinanciera searchData) {
     
        return serviceCuentaEntidadFinanciera.getCuentaEntidadFinancieraList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de CuentaEntidadFinancieraAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CuentaEntidadFinancieraAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceCuentaEntidadFinanciera = new CuentaEntidadFinancieraService();
    }

    /**
     * Constructor de CuentaEntidadFinancieraAdapter
     */
    public CuentaEntidadFinancieraAdapter() {
        super();
        this.serviceCuentaEntidadFinanciera = new CuentaEntidadFinancieraService();
    }
}
