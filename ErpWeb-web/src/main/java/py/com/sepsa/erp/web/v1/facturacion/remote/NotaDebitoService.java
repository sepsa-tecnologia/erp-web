/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.NotaDebitoDetalleFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaDebitoDetallePojoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaDebitoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.NotaDebitoPojoFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaDebitoTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.remote.API;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoService extends APIErpFacturacion {

    /**
     * Obtiene la lista de nota de debito
     *
     * @param notaDebito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaDebitoAdapter getNotaDebitoList(NotaDebito notaDebito, Integer page,
            Integer pageSize) {

        NotaDebitoAdapter lista = new NotaDebitoAdapter();

        Map params = NotaDebitoFilter.build(notaDebito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_DEBITO.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoAdapter.class);

            lista = (NotaDebitoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de nota de debito
     *
     * @param notaDebito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaDebitoPojoAdapter getNotaDebitoPojoList(NotaDebitoPojo notaDebito, Integer page,
            Integer pageSize) {

        NotaDebitoPojoAdapter lista = new NotaDebitoPojoAdapter();

        Map params = NotaDebitoPojoFilter.build(notaDebito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_DEBITO.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoPojoAdapter.class);

            lista = (NotaDebitoPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene detalles de la nota de débito
     *
     * @param notaDebito Objeto
     * @return
     */
    public NotaDebitoPojo getNotaDebitoDetalle(NotaDebitoPojo notaDebito) {

        NotaDebitoPojo nd = new NotaDebitoPojo();

        Map params = new HashMap<>();

        params.put("listadoPojo", notaDebito.getListadoPojo());

        String url = "nota-debito/id/" + notaDebito.getId();

        HttpURLConnection conn = GET(url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoPojo.class);

            nd = (NotaDebitoPojo) response.getPayload();

            conn.disconnect();
        }
        return nd;
    }

    /**
     * Datos de ND
     *
     * @param digital
     * @return
     */
    public DatosNotaDebito getDatosNotaDebito(Map parametros) {

        DatosNotaDebito dnd = new DatosNotaDebito();

        Map params = new HashMap<>();

        params = parametros;

        HttpURLConnection conn = GET(Resource.DATOS_NOTA_DEBITO.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DatosNotaDebito.class);

            dnd = (DatosNotaDebito) response.getPayload();

            conn.disconnect();
        }
        return dnd;
    }

    /**
     * Método para crear Nota Débito
     *
     * @param notaDebito
     * @return
     */
    public BodyResponse<NotaDebito> createNotaDebito(NotaDebito notaDebito) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.NOTA_DEBITO.url, API.ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaDebito));

            response = BodyResponse.createInstance(conn, NotaDebito.class);

        }
        return response;
    }
    
    /**
     * Método para crear nota de débito de forma masiva
     *
     * @param notaDebito
     * @return
     */
    public byte[] sendFileMassive(Attach cargaNotaDebito) {

        byte[] result = null;

        HttpURLConnection conn = POST(Resource.CARGA_MASIVA.url, API.ContentType.MULTIPART);

        if (conn != null) {
            try {
                OutputStream os = conn.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(os), true);
                addFormFile(writer, os, "uploadedFile", cargaNotaDebito);
                BodyByteArrayResponse response = BodyByteArrayResponse
                        .createInstance(conn, writer);

                result = response.getPayload();

                conn.disconnect();

            } catch (IOException ex) {
                WebLogger.get().fatal(ex);
            }
        }

        return result;
    }

    /**
     * Obtiene la lista del detalle de nota de Debito
     * ESTE SERVICIO
     *
     * @param notaDebito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaDebitoDetalleListAdapter getNotaDebitoDetalleList(NotaDebitoDetalle notaDebito, Integer page,
            Integer pageSize) {

        NotaDebitoDetalleListAdapter lista = new NotaDebitoDetalleListAdapter();

        Map params = NotaDebitoDetalleFilter.build(notaDebito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_DEBITO_DETALLE.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoDetalleListAdapter.class);

            lista = (NotaDebitoDetalleListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Obtiene la lista del detalle de nota de crédito
     * ESTE SERVICIO
     *
     * @param notaDebito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public NotaDebitoDetalleListPojoAdapter getNotaDebitoDetalleListPojo(NotaDebitoDetallePojo notaDebito, Integer page,
            Integer pageSize) {

        NotaDebitoDetalleListPojoAdapter lista = new NotaDebitoDetalleListPojoAdapter();

        Map params = NotaDebitoDetallePojoFilter.build(notaDebito, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_DEBITO_DETALLE.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoDetalleListPojoAdapter.class);

            lista = (NotaDebitoDetalleListPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para anular nota de débito
     *
     * @param nota
     * @return
     */
    public BodyResponse anularNotaDebito(NotaDebito nota) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "nota-debito/anular/" + nota.getId();

        Map params = new HashMap<>();
        params.put("ignorarPeriodoAnulacion", nota.getIgnorarPeriodoAnulacion());

        HttpURLConnection conn = PUT(ANULAR_URL, API.ContentType.JSON, params);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(nota));

            response = BodyResponse.createInstance(conn, NotaDebito.class);

        }
        return response;
    }

    /**
     * Método para regenerar la ND
     *
     * @param notaDebito Objeto
     * @return
     */
    public BodyResponse<NotaDebito> regenerarND(NotaDebito notaDebito) {

        BodyResponse response = new BodyResponse();

        //String URL = "factura/anular/" + factura.getId();
        HttpURLConnection conn = PUT(Resource.NOTA_DEBITO.url, API.ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(notaDebito));
            response = BodyResponse.createInstance(conn,
                    NotaDebito.class);

            conn.disconnect();
        }
        return response;
    }

    /**
     * Obtiene la lista de ND recibidas
     *
     * @param page
     * @param pageSize
     * @return
     */
    public NotaDebitoTalonarioAdapter getNDTalonarioList(TalonarioPojo talonario, Integer page,
            Integer pageSize) {

        NotaDebitoTalonarioAdapter lista = new NotaDebitoTalonarioAdapter();

        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.NOTA_DEBITO_TALONARIO.url,
                API.ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    NotaDebitoTalonarioAdapter.class);

            lista = (NotaDebitoTalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de FacturacionService
     */
    public NotaDebitoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        NOTA_DEBITO("NotaDebito", "nota-debito"),
        NOTA_DEBITO_TALONARIO("NotaDebito Talonario", "nota-debito/talonario"),
        CARGA_MASIVA("NotaDebitoCargaMasiva", "nota-debito/crear-masivo"),
        DATOS_NOTA_DEBITO("DatosNotaDebito", "nota-debito/datos-crear"),
        NOTA_DEBITO_DETALLE("nota debito detalle", "nota-debito-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
