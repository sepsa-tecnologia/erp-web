
package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 * POJO de encargados
 * @author Cristina Insfrán
 */
public class Encargado {

    

   
     /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código del tipo de tarifa
     */
    private String codigo;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Character como string
     */
    private Boolean charToString;
    /**
     * Razón Social 
     */
    private String razonSocial;
    
     /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the charToString
     */
    public Boolean getCharToString() {
        return charToString;
    }

    /**
     * @param charToString the charToString to set
     */
    public void setCharToString(Boolean charToString) {
        this.charToString = charToString;
    }
    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
    /**
     * Constructor
     */
    public Encargado(){
        
    }
}
