/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;

/**
 * Adapter para producto info
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ProductoAdapter extends DataListAdapter<Producto> {

    /**
     * Cliente para el servicio producto
     */
    private final ProductoService productoService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoAdapter fillData(Producto searchData) {

        return productoService.getProductoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ProductoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoService();
    }

    /**
     * Constructor de ProductoAdapter
     */
    public ProductoAdapter() {
        super();
        this.productoService = new ProductoService();
    }
}
