package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.filters.ReporteVentaNotaDebitoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteVentaNotaDebito;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para reporte de venta nota debito
 *
 * @author Antonella Lucero
 */
public class ReporteVentaNotaDebitoService extends APIErpFacturacion {

    /**
     * Método para obtener reporte de venta
     *
     * @param venta
     * @return
     */
    public byte[] getReporteVenta(ReporteVentaNotaDebito venta) {

        byte[] result = null;

        Map params = ReporteVentaNotaDebitoFilter.build(venta);

        String service = String.format(Resource.REPORTE_VENTA_NOTA_DEBITO.url);

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }
        return result;
    }
    

    /**
     * Recursos de conexión o servicios del APISepsaSet
     */
    public enum Resource {

        //Servicios
        REPORTE_VENTA_NOTA_DEBITO("Reporte de venta nota debito",
            "nota-debito/reporte-venta");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
