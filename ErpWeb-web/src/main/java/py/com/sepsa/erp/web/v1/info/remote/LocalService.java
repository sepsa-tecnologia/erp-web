package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.LocalFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de local
 *
 * @author Cristina Insfrán
 */
public class LocalService extends APIErpCore {

    /**
     * Método para crear un local
     *
     * @param local
     * @return
     */
    public BodyResponse<Local> setLocal(Local local) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.LOCAL.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(local));
            response = BodyResponse.createInstance(conn, Local.class);
        }
        return response;
    }

    /**
     * Método para editar local
     *
     * @param local
     * @return
     */
    public BodyResponse<Local> editLocal(Local local) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.LOCAL.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(local));
            response = BodyResponse.createInstance(conn, Local.class);
        }
        return response;
    }

    /**
     * Obtiene la lista de clientes
     *
     * @param local Objeto local
     * @param page
     * @param pageSize
     * @return
     */
        public LocalListAdapter getLocalList(Local local, Integer page,
            Integer pageSize) {

        LocalListAdapter lista = new LocalListAdapter();

        Map params = LocalFilter.build(local, page, pageSize);

        HttpURLConnection conn = GET(Resource.LOCAL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LocalListAdapter.class);

            lista = (LocalListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     *
     * @param id
     * @return
     */
    public Local get(Integer id) {
        Local data = new Local(id);
        LocalListAdapter list = getLocalList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de UserServiceClient
     */
    public LocalService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        LOCAL("Servicio Local", "local");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
