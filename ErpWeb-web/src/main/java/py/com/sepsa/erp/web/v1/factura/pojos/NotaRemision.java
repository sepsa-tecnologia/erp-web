/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Williams Vera
 */
public class NotaRemision {
    
    private Integer id;
    
    private String nroNotaRemision;
    
    private Integer idNaturalezaCliente;
    
    private Integer idTalonario;
    
    private Integer idCliente;
    
    private Integer idMotivoEmisionNr;
    
    private String ruc;
    
    private String razonSocial;
    
    private String direccion;
    
    private String nroCasa;
    
    private Integer idDepartamento;
    
    private Integer idDistrito;
    
    private Integer idCiudad;
    
    private Character generadoSet;
    
    private String archivoSet;
    
    private Character anulado;
    
    private String digital;
    
    private Character estadoSincronizado;
    
    private Date fecha;
    
    private Date fechaDesde;
    
    private Date fechaHasta;
    
    private String observacion;
    
    private String cdc;
    
    private String codSeguridad;
   
    private String serie;
    
    private Integer tipoTransporte;   

    private Integer modalidadTransporte;   
  
    private Integer responsableCostoFlete; 

    private String nroDespachoImportacion;

    private Date fechaInicioTraslado;
    
    private Date fechaFinTraslado;
    
    private Integer responsableEmision;
    
    private Date fechaFuturaEmision;
    
    private String cdcFactura;
    
    private String timbradoFactura;
    
    private String nroFactura;
    
    private Date fechaFactura;
    
    private Character tieneFactura;
    
    //DATOS TRANSPORTISTA
    private Transportista transportista;
    
    //VEHICULO
    private VehiculoTraslado vehiculoTraslado;
    
    //DATOS TRASLADO
    private LocalSalidaEntrega localSalida;
    
    private LocalSalidaEntrega localEntrega;
    
    private Integer km;
    
    private List<NotaRemisionDetalle> notaRemisionDetalles;
    
    private String descripcionMotivoEmisionNr;
    
    private String codigoEstado;
    
    private Integer idMotivoAnulacion;
    
    private String observacionAnulacion;
    
    private Boolean ignorarPeriodoAnulacion;
    
    private Boolean listadoPojo;
    
    private Integer idProcesamiento;
    
    private Integer idEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }
    

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public List<NotaRemisionDetalle> getNotaRemisionDetalles() {
        return notaRemisionDetalles;
    }

    public void setNotaRemisionDetalles(List<NotaRemisionDetalle> notaRemisionDetalles) {
        this.notaRemisionDetalles = notaRemisionDetalles;
    }

    public String getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(String archivoSet) {
        this.archivoSet = archivoSet;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public Integer getTipoTransporte() {
        return tipoTransporte;
    }

    public void setTipoTransporte(Integer tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    public Integer getModalidadTransporte() {
        return modalidadTransporte;
    }

    public void setModalidadTransporte(Integer modalidadTransporte) {
        this.modalidadTransporte = modalidadTransporte;
    }

    public Integer getResponsableCostoFlete() {
        return responsableCostoFlete;
    }

    public void setResponsableCostoFlete(Integer responsableCostoFlete) {
        this.responsableCostoFlete = responsableCostoFlete;
    }

    public String getNroDespachoImportacion() {
        return nroDespachoImportacion;
    }

    public void setNroDespachoImportacion(String nroDespachoImportacion) {
        this.nroDespachoImportacion = nroDespachoImportacion;
    }

    public Date getFechaInicioTraslado() {
        return fechaInicioTraslado;
    }

    public void setFechaInicioTraslado(Date fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    public Date getFechaFinTraslado() {
        return fechaFinTraslado;
    }

    public void setFechaFinTraslado(Date fechaFinTraslado) {
        this.fechaFinTraslado = fechaFinTraslado;
    }

    public Integer getResponsableEmision() {
        return responsableEmision;
    }

    public void setResponsableEmision(Integer responsableEmision) {
        this.responsableEmision = responsableEmision;
    }

    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    public VehiculoTraslado getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTraslado vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    public LocalSalidaEntrega getLocalSalida() {
        return localSalida;
    }

    public void setLocalSalida(LocalSalidaEntrega localSalida) {
        this.localSalida = localSalida;
    }

    public LocalSalidaEntrega getLocalEntrega() {
        return localEntrega;
    }

    public void setLocalEntrega(LocalSalidaEntrega localEntrega) {
        this.localEntrega = localEntrega;
    }

    public Date getFechaFuturaEmision() {
        return fechaFuturaEmision;
    }

    public void setFechaFuturaEmision(Date fechaFuturaEmision) {
        this.fechaFuturaEmision = fechaFuturaEmision;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Character getTieneFactura() {
        return tieneFactura;
    }

    public void setTieneFactura(Character tieneFactura) {
        this.tieneFactura = tieneFactura;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getDescripcionMotivoEmisionNr() {
        return descripcionMotivoEmisionNr;
    }

    public void setDescripcionMotivoEmisionNr(String descripcionMotivoEmisionNr) {
        this.descripcionMotivoEmisionNr = descripcionMotivoEmisionNr;
    }

    public NotaRemision() {
    }
    
}
