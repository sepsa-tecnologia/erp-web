package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Romina Núñez
 */
public class FacturaPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param id
     * @return
     */
    public FacturaPojoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
        /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public FacturaPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agrega el filtro de idPersona
     *
     * @param idPersona
     * @return
     */
    public FacturaPojoFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public FacturaPojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public FacturaPojoFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public FacturaPojoFilter moneda(String moneda) {
        if (moneda != null && !moneda.trim().isEmpty()) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de tipo factura
     *
     * @param idTipoFactura
     * @return
     */
    public FacturaPojoFilter idTipoFactura(Integer idTipoFactura) {
        if (idTipoFactura != null) {
            params.put("idTipoFactura", idTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de tipo factura
     *
     * @param tipoFactura
     * @return
     */
    public FacturaPojoFilter tipoFactura(String tipoFactura) {
        if (tipoFactura != null && !tipoFactura.trim().isEmpty()) {
            params.put("tipoFactura", tipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro factura
     *
     * @param nroFactura
     * @return
     */
    public FacturaPojoFilter nroFactura(String nroFactura) {
        if (nroFactura != null && !nroFactura.trim().isEmpty()) {
            params.put("nroFactura", nroFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de nroOrdenCompra
     *
     * @param nroOrdenCompra
     * @return
     */
    public FacturaPojoFilter nroOrdenCompra(String nroOrdenCompra) {
        if (nroOrdenCompra != null && !nroOrdenCompra.trim().isEmpty()) {
            params.put("nroOrdenCompra", nroOrdenCompra);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public FacturaPojoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha vencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public FacturaPojoFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de razón social
     *
     * @param razonSocial
     * @return
     */
    public FacturaPojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agregar el filtro de direccion
     *
     * @param direccion
     * @return
     */
    public FacturaPojoFilter direccion(String direccion) {
        if (direccion != null && !direccion.trim().isEmpty()) {
            params.put("direccion", direccion);
        }
        return this;
    }

    /**
     * Agregar el filtro de RUC
     *
     * @param ruc
     * @return
     */
    public FacturaPojoFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public FacturaPojoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public FacturaPojoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public FacturaPojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de cobrado
     *
     * @param cobrado
     * @return
     */
    public FacturaPojoFilter cobrado(String cobrado) {
        if (cobrado != null && !cobrado.trim().isEmpty()) {
            params.put("cobrado", cobrado);
        }
        return this;
    }

    /**
     * Agregar el filtro de impreso
     *
     * @param impreso
     * @return
     */
    public FacturaPojoFilter impreso(String impreso) {
        if (impreso != null && !impreso.trim().isEmpty()) {
            params.put("impreso", impreso);
        }
        return this;
    }

    /**
     * Agregar el filtro de entregado
     *
     * @param entregado
     * @return
     */
    public FacturaPojoFilter entregado(String entregado) {
        if (entregado != null && !entregado.trim().isEmpty()) {
            params.put("entregado", entregado);
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public FacturaPojoFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de días crédito
     *
     * @param diasCredito
     * @return
     */
    public FacturaPojoFilter diasCredito(Integer diasCredito) {
        if (diasCredito != null) {
            params.put("diasCredito", diasCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 5
     *
     * @param montoIva5
     * @return
     */
    public FacturaPojoFilter montoIva5(BigDecimal montoIva5) {
        if (montoIva5 != null) {
            params.put("montoIva5", montoIva5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 5
     *
     * @param montoImponible5
     * @return
     */
    public FacturaPojoFilter montoImponible5(BigDecimal montoImponible5) {
        if (montoImponible5 != null) {
            params.put("montoImponible5", montoImponible5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 5
     *
     * @param montoTotal5
     * @return
     */
    public FacturaPojoFilter montoTotal5(BigDecimal montoTotal5) {
        if (montoTotal5 != null) {
            params.put("montoTotal5", montoTotal5);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva 10
     *
     * @param montoIva10
     * @return
     */
    public FacturaPojoFilter montoIva10(BigDecimal montoIva10) {
        if (montoIva10 != null) {
            params.put("montoIva10", montoIva10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible 10
     *
     * @param montoImponible10
     * @return
     */
    public FacturaPojoFilter montoImponible10(BigDecimal montoImponible10) {
        if (montoImponible10 != null) {
            params.put("montoImponible10", montoImponible10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total 10
     *
     * @param montoTotal10
     * @return
     */
    public FacturaPojoFilter montoTotal10(BigDecimal montoTotal10) {
        if (montoTotal10 != null) {
            params.put("montoTotal10", montoTotal10);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total excento
     *
     * @param montoTotalExento
     * @return
     */
    public FacturaPojoFilter montoTotalExento(BigDecimal montoTotalExento) {
        if (montoTotalExento != null) {
            params.put("montoTotalExento", montoTotalExento);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto iva total
     *
     * @param montoIvaTotal
     * @return
     */
    public FacturaPojoFilter montoIvaTotal(BigDecimal montoIvaTotal) {
        if (montoIvaTotal != null) {
            params.put("montoIvaTotal", montoIvaTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto imponible total
     *
     * @param montoImponibleTotal
     * @return
     */
    public FacturaPojoFilter montoImponibleTotal(BigDecimal montoImponibleTotal) {
        if (montoImponibleTotal != null) {
            params.put("montoImponibleTotal", montoImponibleTotal);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto total factura
     *
     * @param montoTotalFactura
     * @return
     */
    public FacturaPojoFilter montoTotalFactura(BigDecimal montoTotalFactura) {
        if (montoTotalFactura != null) {
            params.put("montoTotalFactura", montoTotalFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public FacturaPojoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nota de credito
     *
     * @param montoNotaCredito
     * @return
     */
    public FacturaPojoFilter montoNotaCredito(BigDecimal montoNotaCredito) {
        if (montoNotaCredito != null) {
            params.put("montoNotaCredito", montoNotaCredito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de montoCobro
     *
     * @param montoCobro
     * @return
     */
    public FacturaPojoFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de retencion
     *
     * @param montoRetencion
     * @return
     */
    public FacturaPojoFilter montoRetencion(BigDecimal montoRetencion) {
        if (montoRetencion != null) {
            params.put("montoRetencion", montoRetencion);
        }
        return this;
    }

    /**
     * Agrega el filtro de saldo
     *
     * @param saldo
     * @return
     */
    public FacturaPojoFilter saldo(BigDecimal saldo) {
        if (saldo != null) {
            params.put("saldo", saldo);
        }
        return this;
    }

    /**
     * Agrega el filtro de detalles
     *
     * @param detalles
     * @return
     */
    public FacturaPojoFilter detalles(List<FacturaDetalle> detalles) {
        if (detalles != null && !detalles.isEmpty()) {
            params.put("detalles", detalles);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene retencion, true o false
     *
     * @param tieneRetencion
     * @return
     */
    public FacturaPojoFilter tieneRetencion(String tieneRetencion) {
        if (tieneRetencion != null && !tieneRetencion.trim().isEmpty()) {
            params.put("tieneRetencion", tieneRetencion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro tiene saldo
     *
     * @param tieneSaldo
     * @return
     */
    public FacturaPojoFilter tieneSaldo(String tieneSaldo) {
        if (tieneSaldo != null && !tieneSaldo.trim().isEmpty()) {
            params.put("tieneSaldo", tieneSaldo.trim());
        }
        return this;
    }

    /**
     * Agregar el filtro de anhoMes
     *
     * @param anhoMes
     * @return
     */
    public FacturaPojoFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agregar el filtro de pagado
     *
     * @param pagado
     * @return
     */
    public FacturaPojoFilter pagado(String pagado) {
        if (pagado != null && !pagado.trim().isEmpty()) {
            params.put("pagado", pagado);
        }
        return this;
    }

    /**
     * Agregar el filtro codigoTipoFactura
     *
     * @param codigoTipoFactura
     * @return
     */
    public FacturaPojoFilter codigoTipoFactura(String codigoTipoFactura) {
        if (codigoTipoFactura != null && !codigoTipoFactura.trim().isEmpty()) {
            params.put("codigoTipoFactura", codigoTipoFactura);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param producto
     * @return
     */
    public FacturaPojoFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param tieneOc
     * @return
     */
    public FacturaPojoFilter tieneOrdenCompra(String tieneOrdenCompra) {
        if (tieneOrdenCompra != null && !tieneOrdenCompra.trim().isEmpty()) {
            params.put("tieneOrdenCompra", tieneOrdenCompra);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de cdc
     *
     * @param cdc
     * @return
     */
    public FacturaPojoFilter cdc(String cdc) {
        if (cdc != null && !cdc.trim().isEmpty()) {
            params.put("cdc", cdc);
        }
        return this;
    }
    
        /**
     * Agregar el filtro de codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public FacturaPojoFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param factura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(FacturaPojo factura, Integer page, Integer pageSize) {
        FacturaPojoFilter filter = new FacturaPojoFilter();

        filter
                .id(factura.getId())
                .nroFactura(factura.getNroFactura())
                .ruc(factura.getRuc())
                .nroOrdenCompra(factura.getNroOrdenCompra())
                .digital(factura.getDigital())
                .fechaDesde(factura.getFechaDesde())
                .fechaHasta(factura.getFechaHasta())
                .razonSocial(factura.getRazonSocial())
                .cobrado(factura.getCobrado())
                .codigoTipoFactura(factura.getCodigoTipoFactura())
                .anulado(factura.getAnulado())
                .idCliente(factura.getIdCliente())
                .idMoneda(factura.getIdMoneda())
                .tieneOrdenCompra(factura.getTieneOrdenCompra())
                .listadoPojo(factura.getListadoPojo())
                .cdc(factura.getCdc())
                .codigoEstado(factura.getCodigoEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public FacturaPojoFilter() {
        super();
    }
}
