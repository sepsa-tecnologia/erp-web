package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.remote.InventarioService;

/**
 * Adaptador para la lista de inventario
 *
 * @author Romina Núñez
 */
public class InventarioDetalleAdapter extends DataListAdapter<InventarioDetalle> {

    /**
     * Cliente remoto para inventario
     */
    private final InventarioService inventario;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return InventarioAdapter
     */
    @Override
    public InventarioDetalleAdapter fillData(InventarioDetalle searchData) {

        return inventario.getInventarioDetalle(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de InventarioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public InventarioDetalleAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.inventario = new InventarioService();
    }

    /**
     * Constructor de InventarioAdapter
     */
    public InventarioDetalleAdapter() {
        super();
        this.inventario = new InventarioService();
    }

}
