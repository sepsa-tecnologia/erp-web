/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.remote.CanalVentaService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista listar canal de Venta
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("canalVentaList")
public class CanalVentaListController implements Serializable {

    /**
     * Servicio canalVenta.
     */
    private CanalVentaService service;
    /**
     * Adaptador de canalVenta.
     */
    private CanalVentaListAdapter adapter;
    /**
     * Objeto canalVenta.
     */
    private CanalVenta canalVentaFilter;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public CanalVentaListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(CanalVentaListAdapter adapter) {
        this.adapter = adapter;
    }

    public CanalVenta getCanalVentaFilter() {
        return canalVentaFilter;
    }

    public void setCanalVentaFilter(CanalVenta canalVentaFilter) {
        this.canalVentaFilter = canalVentaFilter;
    }

    //</editor-fold>
    /**
     * Método para filtrar canalVentas.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(canalVentaFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.canalVentaFilter = new CanalVenta();
        buscar();
    }

    /**
     * Método para cambiar el estado
     *
     * @param aux
     */
    public void onChangeStatus(CanalVenta aux) {

        CanalVenta newCanalVenta = new CanalVenta();

        switch (aux.getActivo()) {
            case "S":
                newCanalVenta.setActivo("N");
                break;
            case "N":
                newCanalVenta.setActivo("S");
                break;
            default:
                break;
        }

        newCanalVenta.setId(aux.getId());
        newCanalVenta.setCodigo(aux.getCodigo());
        newCanalVenta.setDescripcion(aux.getDescripcion());
        BodyResponse response = service.editCanalVenta(newCanalVenta);

        if (response.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio de estado correctamente!"));
            buscar();
        }
    }

    @PostConstruct
    public void init() {
        try {
            this.service = new CanalVentaService();
            this.adapter = new CanalVentaListAdapter();
            this.canalVentaFilter = new CanalVenta();
            buscar();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
