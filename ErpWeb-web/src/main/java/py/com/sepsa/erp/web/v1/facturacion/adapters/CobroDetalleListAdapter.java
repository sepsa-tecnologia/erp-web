
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista del detalle de cobro
 * @author Cristina Insfrán
 */
public class CobroDetalleListAdapter extends DataListAdapter<CobroDetalle>{
   
     /**
     * Cliente para los servicios de facturación
     */
    private final FacturacionService facturacionClient;
   
    
    @Override
    public CobroDetalleListAdapter fillData(CobroDetalle searchData) {
        return facturacionClient.getDetalleCobroList(searchData,getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de CobroDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobroDetalleListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.facturacionClient = new FacturacionService();
    }

    /**
     * Constructor de CobroDetalleListAdapter
     */
    public CobroDetalleListAdapter() {
        super();
        this.facturacionClient = new FacturacionService();
    }
}
