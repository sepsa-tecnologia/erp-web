package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro utilizado para el servicio de Nota Crédito
 *
 * @author Romina Núñez,Sergio D. Riveros Vazquez
 */
public class NotaCreditoPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaCreditoPojoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    public NotaCreditoPojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public NotaCreditoPojoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }
    
    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public NotaCreditoPojoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public NotaCreditoPojoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nc
     *
     * @param nroNotaCredito
     * @return
     */
    public NotaCreditoPojoFilter nroNotaCredito(String nroNotaCredito) {
        if (nroNotaCredito != null && !nroNotaCredito.trim().isEmpty()) {
            params.put("nroNotaCredito", nroNotaCredito);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaCreditoPojoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaCreditoPojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo emision interno
     *
     * @param idMotivoEmisionInterno
     * @return
     */
    public NotaCreditoPojoFilter idMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        if (idMotivoEmisionInterno != null) {
            params.put("idMotivoEmisionInterno", idMotivoEmisionInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public NotaCreditoPojoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de cdc
     *
     * @param cdc
     * @return
     */
    public NotaCreditoPojoFilter cdc(String cdc) {
        if (cdc != null && !cdc.trim().isEmpty()) {
            params.put("cdc", cdc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public NotaCreditoPojoFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public NotaCreditoPojoFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }
    
        /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaCreditoPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notaCredito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaCreditoPojo notaCredito, Integer page, Integer pageSize) {
        NotaCreditoPojoFilter filter = new NotaCreditoPojoFilter();

        filter
                .id(notaCredito.getId())
                .idCliente(notaCredito.getIdCliente())
                .razonSocial(notaCredito.getRazonSocial())
                .fecha(notaCredito.getFecha())
                .fechaDesde(notaCredito.getFechaDesde())
                .fechaHasta(notaCredito.getFechaHasta())
                .nroNotaCredito(notaCredito.getNroNotaCredito())
                .anulado(notaCredito.getAnulado())
                .idMotivoEmisionInterno(notaCredito.getIdMotivoEmisionInterno())
                .idFactura(notaCredito.getIdFactura())
                .listadoPojo(notaCredito.getListadoPojo())
                .cdc(notaCredito.getCdc())
                .ruc(notaCredito.getRuc())
                .codigoEstado(notaCredito.getCodigoEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoFilter
     */
    public NotaCreditoPojoFilter() {
        super();
    }
}
