package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesPeriodo;

/**
 * Filtro para liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionesPeriodoFilter extends Filter {

    /**
     * Filtro por identificador de histórico de liquidaciones.
     *
     * @param idHistoricoLiquidacion
     * @return
     */
    public LiquidacionesPeriodoFilter idHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        if (idHistoricoLiquidacion != null) {
            params.put("idHistoricoLiquidacion", idHistoricoLiquidacion);
        }
        return this;
    }

    /**
     * Filtro por fecha de proceso.
     *
     * @param fechaProceso
     * @return
     */
    public LiquidacionesPeriodoFilter fechaProceso(Date fechaProceso) {
        if (fechaProceso != null) {
            params.put("fechaProceso", fechaProceso);
        }
        return this;
    }

    /**
     * Filtro por monto total excedente.
     *
     * @param montoTotalExcedente
     * @return
     */
    public LiquidacionesPeriodoFilter montoTotalExcedente(Integer montoTotalExcedente) {
        if (montoTotalExcedente != null) {
            params.put("montoTotalExcedente", montoTotalExcedente);
        }
        return this;
    }

    /**
     * Filtro por monto total liquidado.
     *
     * @param montoTotalLiquidado
     * @return
     */
    public LiquidacionesPeriodoFilter montoTotalLiquidado(Integer montoTotalLiquidado) {
        if (montoTotalLiquidado != null) {
            params.put("montoTotalLiquidado", montoTotalLiquidado);
        }
        return this;
    }

    /**
     * Filtro por monto mínimo.
     *
     * @param montoMinimo
     * @return
     */
    public LiquidacionesPeriodoFilter montoMinimo(Character montoMinimo) {
        if (montoMinimo != null) {
            params.put("montoMinimo", montoMinimo);
        }
        return this;
    }

    /**
     * Filtro por estado.
     *
     * @param estado
     * @return
     */
    public LiquidacionesPeriodoFilter estado(Character estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Filtro por identificador de liquidación.
     *
     * @param idLiquidacion
     * @return
     */
    public LiquidacionesPeriodoFilter idLiquidacion(Integer idLiquidacion) {
        if (idLiquidacion != null) {
            params.put("idLiquidacion", idLiquidacion);
        }
        return this;
    }

    /**
     * Filtro por cantidad de documentos recibidos.
     *
     * @param cantDocRecibidos
     * @return
     */
    public LiquidacionesPeriodoFilter cantDocRecibidos(Integer cantDocRecibidos) {
        if (cantDocRecibidos != null) {
            params.put("cantDocRecibidos", cantDocRecibidos);
        }
        return this;
    }

    /**
     * Filtro por cantidad de documentos recibidos.
     *
     * @param cantDocEnviados
     * @return
     */
    public LiquidacionesPeriodoFilter cantDocEnviados(Integer cantDocEnviados) {
        if (cantDocEnviados != null) {
            params.put("cantDocEnviados", cantDocEnviados);
        }
        return this;
    }

    /**
     * Filtro por fecha desde.
     *
     * @param fechaDesde
     * @return
     */
    public LiquidacionesPeriodoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            params.put("fechaDesde", fechaDesde);
        }
        return this;
    }

    /**
     * Filtro por fecha hasta.
     *
     * @param fechaHasta
     * @return
     */
    public LiquidacionesPeriodoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            params.put("fechaHasta", fechaHasta);
        }
        return this;
    }

    /**
     * Filtro por identificador de producto.
     *
     * @param idProducto
     * @return
     */
    public LiquidacionesPeriodoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Filtro por producto.
     *
     * @param producto
     * @return
     */
    public LiquidacionesPeriodoFilter producto(String producto) {
        if (producto != null) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Filtro por identificador de servicio.
     *
     * @param idServicio
     * @return
     */
    public LiquidacionesPeriodoFilter idServicio(Integer idServicio) {
        if (idServicio != null) {
            params.put("idServicio", idServicio);
        }
        return this;
    }

    /**
     * Filtro por servicio.
     *
     * @param servicio
     * @return
     */
    public LiquidacionesPeriodoFilter servicio(String servicio) {
        if (servicio != null) {
            params.put("servicio", servicio);
        }
        return this;
    }

    /**
     * Filtro por identificador de cliente.
     *
     * @param idCliente
     * @return
     */
    public LiquidacionesPeriodoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Filtro por cliente.
     *
     * @param cliente
     * @return
     */
    public LiquidacionesPeriodoFilter cliente(String cliente) {
        if (cliente != null) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Filtro por agente comercial.
     *
     * @param comercial
     * @return
     */
    public LiquidacionesPeriodoFilter comercial(String comercial) {
        if (comercial != null) {
            params.put("comercial", comercial);
        }
        return this;
    }

    public LiquidacionesPeriodoFilter cantLineas(Integer cantLineas) {
        if (cantLineas != null) {
            params.put("cantLineas", cantLineas);
        }
        return this;
    }

    /**
     * Filtro por año.
     *
     * @param ano
     * @return
     */
    public LiquidacionesPeriodoFilter ano(String ano) {
        if (ano != null && !ano.trim().isEmpty()) {
            params.put("ano", ano);
        }
        return this;
    }

    /**
     * Filtro por mes.
     *
     * @param mes
     * @return
     */
    public LiquidacionesPeriodoFilter mes(String mes) {
        if (mes != null && !mes.trim().isEmpty()) {
            params.put("mes", mes);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros.
     *
     * @param liquidacionesPeriodo datos del filtro.
     * @param page página buscada en el resultado.
     * @param pageSize tamaño de la página buscada en el resultado.
     * @return
     */
    public static Map build(LiquidacionesPeriodo liquidacionesPeriodo, Integer page, Integer pageSize) {
        LiquidacionesPeriodoFilter filter = new LiquidacionesPeriodoFilter();

        filter
                .idHistoricoLiquidacion(liquidacionesPeriodo.getIdHistoricoLiquidacion())
                .fechaProceso(liquidacionesPeriodo.getFechaProceso())
                .montoTotalExcedente(liquidacionesPeriodo.getMontoTotalExcedente())
                .montoTotalLiquidado(liquidacionesPeriodo.getMontoTotalLiquidado())
                .montoMinimo(liquidacionesPeriodo.getMontoMinimo())
                .estado(liquidacionesPeriodo.getEstado())
                .idLiquidacion(liquidacionesPeriodo.getIdLiquidacion())
                .cantDocRecibidos(liquidacionesPeriodo.getCantDocRecibidos())
                .cantDocEnviados(liquidacionesPeriodo.getCantDocEnviados())
                .fechaDesde(liquidacionesPeriodo.getFechaDesde())
                .fechaHasta(liquidacionesPeriodo.getFechaHasta())
                .idProducto(liquidacionesPeriodo.getIdProducto())
                .producto(liquidacionesPeriodo.getProducto())
                .idServicio(liquidacionesPeriodo.getIdServicio())
                .servicio(liquidacionesPeriodo.getServicio())
                .idCliente(liquidacionesPeriodo.getIdCliente())
                .cliente(liquidacionesPeriodo.getCliente())
                .comercial(liquidacionesPeriodo.getComercial())
                .cantLineas(liquidacionesPeriodo.getCantLineas())
                .ano(liquidacionesPeriodo.getAno())
                .mes(liquidacionesPeriodo.getMes())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor.
     */
    public LiquidacionesPeriodoFilter() {
        super();
    }

}
