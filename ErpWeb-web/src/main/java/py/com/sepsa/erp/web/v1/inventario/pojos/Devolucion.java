/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;
import java.util.List;

/**
 * Pojo para devolución
 *
 * @author Romina Núñez
 */
public class Devolucion {

    /**
     * Id
     */
    private Integer id;
    /**
     * Id Estado
     */
    private Integer idEstado;
    /**
     * Estado
     */
    private String estado;
    /**
     * codigo Estado
     */
    private String codigoEstado;
    /**
     * listado pojo
     */
    private String listadoPojo;
    /**
     * Fecha de Insercion
     */
    private Date fechaInsercion;
    /**
     * Detalles de Devolución
     */
    private List<DevolucionDetalle> devolucionDetalles;
    /**
     * Fecha de Insercion Desde
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha de Insercion Hasta
     */
    private Date fechaInsercionHasta;
    /**
     * Identificador de factura
     */
    private Integer idFactura;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(String listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public List<DevolucionDetalle> getDevolucionDetalles() {
        return devolucionDetalles;
    }

    public void setDevolucionDetalles(List<DevolucionDetalle> devolucionDetalles) {
        this.devolucionDetalles = devolucionDetalles;
    }

    //</editor-fold>
    public Devolucion() {
    }

    public Devolucion(Integer id) {
        this.id = id;
    }

}
