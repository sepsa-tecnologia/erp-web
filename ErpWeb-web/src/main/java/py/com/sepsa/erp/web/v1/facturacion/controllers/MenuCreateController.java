package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * Pojo para la creación de Menu
 *
 * @author alext, Sergio D. Riveros Vazquez.
 */
@ViewScoped
@Named("menuCreate")
public class MenuCreateController implements Serializable {

    /**
     * Cliente para el servicio de menu
     */
    private final MenuService service;
    /**
     * Adaptador de la lista de menu
     */
    MenuListAdapter adapter;
    /**
     * Pojo de menu
     */
    private MenuPojo menu;
    /**
     * Pojo de menu
     */
    private MenuPojo menuAutoComplete;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MenuPojo getMenu() {
        return menu;
    }

    public void setMenuAutoComplete(MenuPojo menuAutoComplete) {
        this.menuAutoComplete = menuAutoComplete;
    }

    public MenuPojo getMenuAutoComplete() {
        return menuAutoComplete;
    }

    public void setMenu(MenuPojo menu) {
        this.menu = menu;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public MenuListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(MenuListAdapter adapter) {
        this.adapter = adapter;
    }
    //</editor-fold>

    /* Método para obtener los padres filtrados
     *
     * @param query
     * @return
     */
    public List<MenuPojo> completeQuery(String query) {

        MenuPojo menuAutocomplete = new MenuPojo();
        menuAutocomplete.setDescripcion(query);

        adapter = adapter.fillData(menuAutocomplete);

        return adapter.getData();
    }

    /**
     * Selecciona el padre
     *
     * @param event
     */
    public void onItemSelectPadreFilter(SelectEvent event) {
        menu.setIdPadre(((MenuPojo) event.getObject()).getId());
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        menu.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método para crear menu
     */
    public void crearMenu() {
        BodyResponse<MenuPojo> respuestaMenu = service.setMenu(this.menu);
        if (respuestaMenu.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Menu creado correctamente!"));
            init();
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.menu = new MenuPojo();
        this.adapter = new MenuListAdapter();
        this.menuAutoComplete = new MenuPojo();
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
    }

    /**
     * Constructor de la clase
     */
    public MenuCreateController() {
        init();
        this.service = new MenuService();
    }

}
