
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemisionDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;

/**
 * Adaptador de la lista de nota credito detalle
 * @author Cristina Insfrán
 */
public class NotaRemisionDetalleListPojoAdapter extends DataListAdapter<NotaRemisionDetallePojo> {
  
     /**
     * Cliente para el servicio de cobro
     */
    private final NotaRemisionService serviceNotaRemision;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaRemisionDetalleListPojoAdapter fillData(NotaRemisionDetallePojo searchData) {
     
        return serviceNotaRemision.getNotaRemisionDetalleListPojo(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaCreditoDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaRemisionDetalleListPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaRemision = new NotaRemisionService();
    }

    /**
     * Constructor de NotaCreditoDetalleListAdapter
     */
    public NotaRemisionDetalleListPojoAdapter() {
        super();
        this.serviceNotaRemision = new NotaRemisionService();
    }
}
