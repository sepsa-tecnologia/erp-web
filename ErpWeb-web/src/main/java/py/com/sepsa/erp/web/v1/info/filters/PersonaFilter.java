package py.com.sepsa.erp.web.v1.info.filters;

import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoPersona;

/**
 * Filtro utilizado para Persona
 *
 * @author Sergio D. Riveros Vazquez
 */
public class PersonaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param id Identificador de la persona
     * @return
     */
    public PersonaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;

    }

    public PersonaFilter idTipoPersona(Integer idTipoPersona) {
        if (idTipoPersona != null) {
            params.put("idTipoPersona", idTipoPersona);
        }
        return this;
    }

    public PersonaFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    public PersonaFilter nombre(String nombre) {
        if (nombre != null) {
            params.put("nombre", nombre);
        }
        return this;
    }

    public PersonaFilter apellido(String apellido) {
        if (apellido != null) {
            params.put("apellido", apellido);
        }
        return this;
    }

    public PersonaFilter tipoPersona(TipoPersona tipoPersona) {
        if (tipoPersona != null) {
            params.put("tipoPersona", tipoPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de personaTelefonos
     *
     * @param personaTelefonos
     * @return
     */
    public PersonaFilter personaTelefonos(List<PersonaTelefono> personaTelefonos) {
        if (personaTelefonos != null && !personaTelefonos.isEmpty()) {
            params.put("personaTelefonos", personaTelefonos);
        }
        return this;
    }

    /**
     * Agrega el filtro de personaEmails
     *
     * @param personaEmails
     * @return
     */
    public PersonaFilter personaEmails(List<PersonaEmail> personaEmails) {
        if (personaEmails != null && !personaEmails.isEmpty()) {
            params.put("personaEmails", personaEmails);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param persona datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Persona persona, Integer page, Integer pageSize) {
        PersonaFilter filter = new PersonaFilter();

        filter
                .id(persona.getId())
                .idTipoPersona(persona.getIdTipoPersona())
                .activo(persona.getActivo())
                .nombre(persona.getNombre())
                .apellido(persona.getApellido())
                .tipoPersona(persona.getTipoPersona())
                .personaEmails(persona.getPersonaEmails())
                .personaTelefonos(persona.getPersonaTelefonos())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DatoPersona
     */
    public PersonaFilter() {
        super();
    }
}
