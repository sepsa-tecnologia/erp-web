package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MenuListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MenuPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.MenuService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * Pojo para la edición de Menu
 *
 * @author alext, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("menuEdit")
public class MenuEditController implements Serializable {

    /**
     * Cliente para el servicio de menu
     */
    private final MenuService service;
    /**
     * Adaptador de la lista de menu
     */
    private MenuListAdapter adapter;
    /**
     * Pojo de menu
     */
    private MenuPojo menu;
    /**
     * Pojo de menu
     */
    private MenuPojo menuAutoComplete;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MenuPojo getMenu() {
        return menu;
    }

    public void setMenu(MenuPojo menu) {
        this.menu = menu;
    }

    public MenuPojo getMenuAutoComplete() {
        return menuAutoComplete;
    }

    public void setMenuAutoComplete(MenuPojo menuAutoComplete) {
        this.menuAutoComplete = menuAutoComplete;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public MenuListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(MenuListAdapter adapter) {
        this.adapter = adapter;
    }
    //</editor-fold>

    /* Método para obtener los padres filtrados
     *
     * @param query
     * @return
     */
    public List<MenuPojo> completeQuery(String query) {

        MenuPojo menuAutocomplete = new MenuPojo();
        menuAutocomplete.setDescripcion(query);

        adapter = adapter.fillData(menuAutocomplete);

        return adapter.getData();
    }

    /**
     * Selecciona el padre
     *
     * @param event
     */
    public void onItemSelectPadreFilter(SelectEvent event) {
        menu.setIdPadre(((MenuPojo) event.getObject()).getId());

    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        menu.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método para editar menu
     */
    public void editarMenu() {
        BodyResponse<MenuPojo> respuestaMenu = service.editMenu(menu);
        if (respuestaMenu.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Menu editado correctamente!"));
        }
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        menu = service.get(id);
        // Autocompletar el campo Padre
        menuAutoComplete = new MenuPojo();
        if (menu.getPadre() != null) {
            menuAutoComplete.setDescripcion(menu.getPadre().getDescripcion());
        }

        this.adapter = new MenuListAdapter();
        this.completeQuery(menuAutoComplete.getDescripcion());

        // AutoCompletar el campo Empresa
        this.empresaAutocomplete = new Empresa();
        if (menu.getEmpresa() != null) {
            empresaAutocomplete.setDescripcion(menu.getEmpresa().getDescripcion());
        }

        this.empresaAdapter = new EmpresaAdapter();
        this.completeEmpresaQuery(empresaAutocomplete.getDescripcion());
    }

    /**
     * Constructor de la clase
     */
    public MenuEditController() {
        this.service = new MenuService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
