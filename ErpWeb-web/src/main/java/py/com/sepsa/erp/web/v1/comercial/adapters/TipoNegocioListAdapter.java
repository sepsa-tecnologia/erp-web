
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoNegocio;
import py.com.sepsa.erp.web.v1.comercial.remote.TipoNegocioServiceClient;

/**
 * Adaptador para la lista de tipo negocio
 * @author Cristina Insfrán
 */
public class TipoNegocioListAdapter extends DataListAdapter<TipoNegocio>{
    
    /**
     * Cliente para el servicio tipo negocio
     */
    private final TipoNegocioServiceClient tipoNegocioClient;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoNegocioListAdapter fillData(TipoNegocio searchData) {
     
        return tipoNegocioClient.getTipoNegocioList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoNegocioListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoNegocioClient = new TipoNegocioServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public TipoNegocioListAdapter() {
        super();
        this.tipoNegocioClient = new TipoNegocioServiceClient();
    }
}
