/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoTransaccion;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para tipo transaccion
 *
 * @author Williams Vera
 */
public class TipoTransaccionFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoTransaccionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoTransaccionFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
        /**
     * Agregar el filtro de código
     *
     * @param codigo
     * @return
     */
    public TipoTransaccionFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoTransaccion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoTransaccion tipoTransaccion, Integer page, Integer pageSize) {
        TipoTransaccionFilter filter = new TipoTransaccionFilter();

        filter
                .id(tipoTransaccion.getId())
                .descripcion(tipoTransaccion.getDescripcion())
                .codigo(tipoTransaccion.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoTransaccionFilter() {
        super();
    }

}
