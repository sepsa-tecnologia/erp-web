/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.pojos;

/**
 *
 * @author Jonathan
 */
public class FrecuenciaEjecucion {
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Frecuencia
     */
    private Integer frecuencia;
    
    /**
     * Hora
     */
    private Integer hora;
    
    /**
     * Minuto
     */
    private Integer minuto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(Integer frecuencia) {
        this.frecuencia = frecuencia;
    }

    public Integer getHora() {
        return hora;
    }

    public void setHora(Integer hora) {
        this.hora = hora;
    }

    public Integer getMinuto() {
        return minuto;
    }

    public void setMinuto(Integer minuto) {
        this.minuto = minuto;
    }
}
