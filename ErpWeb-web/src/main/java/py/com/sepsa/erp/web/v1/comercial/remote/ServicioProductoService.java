package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServicioProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ServicioProductoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ServicioProducto;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio Producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ServicioProductoService extends APIErpComercial {

    /**
     * Obtiene la lista de clientes
     *
     * @param servicioProducto
     * @param page
     * @param pageSize
     * @return
     */
    public ServicioProductoAdapter getServicioProductoList(ServicioProducto servicioProducto, Integer page,
            Integer pageSize) {

        ServicioProductoAdapter lista = new ServicioProductoAdapter();

        Map params = ServicioProductoFilter.build(servicioProducto, page, pageSize);

        HttpURLConnection conn = GET(Resource.SERVICIOPRODUCTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ServicioProductoAdapter.class);

            lista = (ServicioProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de ServicioProductoService
     */
    public ServicioProductoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        SERVICIOPRODUCTO("Servicio", "servicio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
