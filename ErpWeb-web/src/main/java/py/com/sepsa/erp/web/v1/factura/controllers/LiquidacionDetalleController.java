package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TraficoDetalleAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.factura.pojos.TraficoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.HistoricoLiquidacionService;

/**
 * Controlador para Liquidación Detalle
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("liquidacionDetalle")
public class LiquidacionDetalleController implements Serializable {
    /**
     * Identificador de liquidación
     */
    private String idLiquidacion;
    /**
     * POJO Historico Liquidacion
     */
    private HistoricoLiquidacion historicoLiquidacionFilter;
    /**
     * Historico Liquidacion Service
     */
    private HistoricoLiquidacionService serviceHistoricoLiquidacion;
    /**
     * Adaptador de Cliente
     */
    private ClientListAdapter adapterCliente;
    /**
     * POJO Cliente
     */
    private Cliente clienteFilter;
    /**
     * POJO TraficoDetalle
     */
    private TraficoDetalle traficoFilter;
    /**
     * Adaptador para la lista de trafico
     */
    private TraficoDetalleAdapter adapterTraficoDetalle;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public String getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setTraficoFilter(TraficoDetalle traficoFilter) {
        this.traficoFilter = traficoFilter;
    }

    public TraficoDetalle getTraficoFilter() {
        return traficoFilter;
    }

    public void setAdapterTraficoDetalle(TraficoDetalleAdapter adapterTraficoDetalle) {
        this.adapterTraficoDetalle = adapterTraficoDetalle;
    }

    public TraficoDetalleAdapter getAdapterTraficoDetalle() {
        return adapterTraficoDetalle;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setServiceHistoricoLiquidacion(HistoricoLiquidacionService serviceHistoricoLiquidacion) {
        this.serviceHistoricoLiquidacion = serviceHistoricoLiquidacion;
    }

    public void setHistoricoLiquidacionFilter(HistoricoLiquidacion historicoLiquidacionFilter) {
        this.historicoLiquidacionFilter = historicoLiquidacionFilter;
    }

    public HistoricoLiquidacionService getServiceHistoricoLiquidacion() {
        return serviceHistoricoLiquidacion;
    }

    public HistoricoLiquidacion getHistoricoLiquidacionFilter() {
        return historicoLiquidacionFilter;
    }
    
    public void setIdLiquidacion(String idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }
//</editor-fold>
    
    /**
     * Método para obtener Histórico Liquidación + Detalles
     */
    public void obtenerDetalleHistoricoLiquidacion(){
        historicoLiquidacionFilter.setIdHistoricoLiquidacion(Integer.parseInt(idLiquidacion));
        
        historicoLiquidacionFilter = serviceHistoricoLiquidacion.getHistoricoLiquidacionDetalle(historicoLiquidacionFilter);
        
        clienteFilter = new Cliente();
        
        clienteFilter.setIdCliente(historicoLiquidacionFilter.getIdCliente());
        adapterCliente = adapterCliente.fillData(clienteFilter);
        
        clienteFilter.setIdCliente(adapterCliente.getData().get(0).getIdCliente());
        clienteFilter.setRazonSocial(adapterCliente.getData().get(0).getRazonSocial());
        clienteFilter.setNroDocumento(adapterCliente.getData().get(0).getNroDocumento());
    }

    
    /**
     * Método para filtrar el tráfico detalle
     * @param idHistoricoLiquidacion 
     */
    public void filterTrafico(Integer idHistoricoLiquidacion, Integer nroLinea){
        traficoFilter = new TraficoDetalle();
        
        traficoFilter.setIdHistoricoLiquidacion(idHistoricoLiquidacion);
        traficoFilter.setNroLinea(nroLinea);
        
        adapterTraficoDetalle = adapterTraficoDetalle.fillData(traficoFilter);
    }
    
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
     Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idLiquidacion = params.get("idHistoricoLiquidacion");
        
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();
        this.serviceHistoricoLiquidacion = new HistoricoLiquidacionService();
        this.adapterCliente = new ClientListAdapter();
        
        this.adapterTraficoDetalle = new TraficoDetalleAdapter();
        
        obtenerDetalleHistoricoLiquidacion();

    }

}
