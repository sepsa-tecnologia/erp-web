
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de persona
 * @author Cristina Insfrán
 */
public class PersonaServiceClient extends APIErpCore{
    
     /**
     * Método para crear usuario
     *
     * @param persona
     * @return
     */
    public BodyResponse<Persona> setPersona(Persona persona) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERSONA.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(persona));

            response = BodyResponse.createInstance(conn, Persona.class);
           
        }
        return response;
    }
    
    
     /**
     * Método para crear/editar paesona
     *
     * @param persona
     * @return
     */
    public BodyResponse<Persona> putPersona(Persona persona) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PERSONA.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(persona));

            response = BodyResponse.createInstance(conn, Persona.class);
           
        }
        return response;
    }
     /**
     * Obtiene la lista de personas
     *
     * @param persona Objeto cliente
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaListAdapter getPersonaList(Persona persona, Integer page,
            Integer pageSize) {
       
        PersonaListAdapter lista = new PersonaListAdapter();

        Map params = PersonaFilter.build(persona, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.PERSONA.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaListAdapter.class);

            lista = (PersonaListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
      /**
     * Constructor de PersonaServiceClient
     */
    public PersonaServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        PERSONA("Servicio para Persona", "persona");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
