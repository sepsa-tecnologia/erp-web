package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.remote.DepositoLogisticoService;

/**
 * Converter para depositoLogistico
 *
 * @author Alexander Triana, Romina Núñez
 */
@FacesConverter("depositoPojoConverter")
public class DepositoPojoConverter implements Converter {

    /**
     * Servicio para depositoLogistico
     */
    private DepositoLogisticoService depositoService;
    
    @Override
    public DepositoLogistico getAsObject(FacesContext fc, UIComponent uic, String value) {
        
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            depositoService = new DepositoLogisticoService();
            
            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }
            
            DepositoLogistico depositoLogistico = new DepositoLogistico();
            depositoLogistico.setListadoPojo(true);
            depositoLogistico.setId(val);
            
            List<DepositoLogistico> depositoLogistico1 = depositoService.getDepositoMotivoList(depositoLogistico, 0, 10).getData();
            if (depositoLogistico1 != null && !depositoLogistico1.isEmpty()) {
                return depositoLogistico1.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "No es un depósito logísitico válido"));
            }
        } else {
            return null;
        }
    }
    
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof DepositoLogistico) {
                return String.valueOf(((DepositoLogistico) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
