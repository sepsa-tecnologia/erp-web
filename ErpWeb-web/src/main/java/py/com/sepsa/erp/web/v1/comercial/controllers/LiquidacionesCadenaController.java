package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionesCadenaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesCadena;

/**
 * Controlador para lista de liquidaciones por cadena.
 *
 * @author alext
 */
@ViewScoped
@Named("liquidacionesCadena")
public class LiquidacionesCadenaController implements Serializable {

    private final String ano;

    private final String mes;

    /**
     * Adaptador de Liquidaciones por cadena.
     */
    private LiquidacionesCadenaAdapter adapter;

    /**
     * Objeto LiquidacionesCadena.
     */
    private LiquidacionesCadena searchData;

    /**
     * Cantidad de filas en la tabla.
     */
    Integer rows = 10;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public LiquidacionesCadenaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LiquidacionesCadenaAdapter adapter) {
        this.adapter = adapter;
    }

    public LiquidacionesCadena getSearchData() {
        return searchData;
    }

    public void setSearchData(LiquidacionesCadena searchData) {
        this.searchData = searchData;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }
    //</editor-fold>

    /**
     * Método para generar lista de liquidaciones por cadena.
     */
    public void search() {
        adapter = new LiquidacionesCadenaAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new LiquidacionesCadena(ano, mes);
        search();
    }

    /**
     * Actualiza la lista de documentos.
     */
    public void loadLiquidaciones() {
        adapter.setPageSize(rows);
        adapter = adapter.fillData(searchData);
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public LiquidacionesCadenaController() {
        adapter = new LiquidacionesCadenaAdapter();
        searchData = new LiquidacionesCadena();
        Calendar now = Calendar.getInstance();
        int m = now.get(Calendar.MONTH) + 1;
        int a = now.get(Calendar.YEAR);
        String mes = String.valueOf(m);
        mes = mes.length() == 1 ? "0" + mes : mes;
        this.mes = mes;
        this.ano = String.valueOf(a);

    }

}
