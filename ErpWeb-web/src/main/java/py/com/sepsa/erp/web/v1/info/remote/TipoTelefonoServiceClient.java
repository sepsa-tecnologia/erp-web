
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoTelefonoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de tipo telefono
 * @author Cristina Insfrán
 */
public class TipoTelefonoServiceClient extends  APIErpCore{
    
    /**
     * Obtiene la lista de tipo telefono
     *
     * @param tipo Objeto tipo telefono
     * @param page
     * @param pageSize
     * @return
     */
    public TipoTelefonoListAdapter getTipoTelefonoList(TipoTelefono tipo, Integer page,
            Integer pageSize) {
       
        TipoTelefonoListAdapter lista = new TipoTelefonoListAdapter();
        
        Map params = TipoTelefonoFilter.build(tipo, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.TIPO_TELEFONO.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoTelefonoListAdapter.class);

            lista = (TipoTelefonoListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        return lista;
    
    }
     /**
     * Constructor de TipoTelefonoServiceClient
     */
    public TipoTelefonoServiceClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        TIPO_TELEFONO("Tipo Telefono", "tipo-telefono");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
