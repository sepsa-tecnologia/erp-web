
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;

/**
 * Adaptador de la lista de cobro
 * @author Romina Núñez
 */
public class NotaCreditoAdapter extends DataListAdapter<NotaCredito> {
    
    /**
     * Cliente para el servicio de cobro
     */
    private final NotaCreditoService serviceNotaCredito;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaCreditoAdapter fillData(NotaCredito searchData) {
     
        return serviceNotaCredito.getNotaCreditoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaCreditoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaCreditoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaCredito = new NotaCreditoService();
    }

    /**
     * Constructor de NotaCreditoAdapter
     */
    public NotaCreditoAdapter() {
        super();
        this.serviceNotaCredito = new NotaCreditoService();
    }
}
