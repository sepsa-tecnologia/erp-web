package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para persona telefono
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class PersonaTelefono {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador del idPersona
     */
    private Integer idPersona;
    /**
     * Identificador del idtelefono
     */
    private Integer idTelefono;
    /**
     * Activo
     */
    private String activo;
    /**
     * Objeto Telefono
     */
    private Telefono telefono;
    /**
     * Objeto Persona
     */
    private Persona persona;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS/**">

    public Integer getId() {
        return id;

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

//</editor-fold>
    /**
     * Constructor de la clase
     */
    public PersonaTelefono() {
    }

    /**
     * Constructor de la clase con parametros
     *
     * @param idTelefono
     * @param idPersona
     */
    public PersonaTelefono(Integer idTelefono, Integer idPersona) {
        this.idTelefono = idTelefono;
        this.idPersona = idPersona;
    }

}
