package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.FirmaDigitalAdapter;
import py.com.sepsa.erp.web.v1.info.filters.FirmaDigitalFilter;
import py.com.sepsa.erp.web.v1.info.pojos.FirmaDigital;
import py.com.sepsa.erp.web.v1.remote.APISepsaCore;

/**
 *
 * @author Gustavo Benítez L.
 */
public class FirmaDigitalService extends APISepsaCore{

    public FirmaDigitalAdapter getFirmaDigitalList(FirmaDigital firmaDigital, Integer page, Integer pageSize) {

        FirmaDigitalAdapter lista = new FirmaDigitalAdapter();
        Map params = FirmaDigitalFilter.build(firmaDigital, page, pageSize);
        HttpURLConnection conn = GET(Resource.FIRMADIGITAL.url,ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,FirmaDigitalAdapter.class);
            try {
                lista = (FirmaDigitalAdapter) response.getPayload();

            } catch (Exception ex) {
                lista = new FirmaDigitalAdapter();
            }
            conn.disconnect();
        }
        return lista;
    }

    public FirmaDigitalService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        FIRMADIGITAL("Servicio Firma Digital", "firma-digital");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
