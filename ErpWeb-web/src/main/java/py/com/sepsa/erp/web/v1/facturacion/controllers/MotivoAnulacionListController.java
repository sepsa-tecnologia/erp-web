/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAnulacionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivoAnulacion;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista listar Motivo Anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoAnulacionList")
public class MotivoAnulacionListController implements Serializable {

    /**
     * Adaptador para la lista de Motivo Anulacion
     */
    private MotivoAnulacionAdapterList motivoAnulacionAdapterList;
    /**
     * POJO de Motivo Anulacion
     */
    private MotivoAnulacion motivoAnulacionFilter;
    /**
     * Adaptador para la lista de Motivo anulacion
     */
    private TipoMotivoAnulacionAdapter tipoMotivoAnulacionAdapterList;
    /**
     * POJO de tipo Motivo anulacion
     */
    private TipoMotivoAnulacion tipoMotivoAnulacionFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoAnulacionAdapterList getMotivoAnulacionAdapterList() {
        return motivoAnulacionAdapterList;
    }

    public void setMotivoAnulacionAdapterList(MotivoAnulacionAdapterList motivoAnulacionAdapterList) {
        this.motivoAnulacionAdapterList = motivoAnulacionAdapterList;
    }

    public MotivoAnulacion getMotivoAnulacionFilter() {
        return motivoAnulacionFilter;
    }

    public void setMotivoAnulacionFilter(MotivoAnulacion motivoAnulacionFilter) {
        this.motivoAnulacionFilter = motivoAnulacionFilter;
    }

    public TipoMotivoAnulacionAdapter getTipoMotivoAnulacionAdapterList() {
        return tipoMotivoAnulacionAdapterList;
    }

    public void setTipoMotivoAnulacionAdapterList(TipoMotivoAnulacionAdapter tipoMotivoAnulacionAdapterList) {
        this.tipoMotivoAnulacionAdapterList = tipoMotivoAnulacionAdapterList;
    }

    public TipoMotivoAnulacion getTipoMotivoAnulacionFilter() {
        return tipoMotivoAnulacionFilter;
    }

    public void setTipoMotivoAnulacionFilter(TipoMotivoAnulacion tipoMotivoAnulacionFilter) {
        this.tipoMotivoAnulacionFilter = tipoMotivoAnulacionFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener motivo Anulaciones
     */
    public void getMotivosAnulacion() {
        this.motivoAnulacionAdapterList.setFirstResult(0);
         this.motivoAnulacionAdapterList = this.motivoAnulacionAdapterList.fillData(motivoAnulacionFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.motivoAnulacionFilter = new MotivoAnulacion();
        getMotivosAnulacion();
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoMotivoAnulacion() {
        this.tipoMotivoAnulacionAdapterList.setFirstResult(0);
        this.tipoMotivoAnulacionAdapterList = this.tipoMotivoAnulacionAdapterList.fillData(tipoMotivoAnulacionFilter);
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("motivo-anulacion-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            motivoAnulacionFilter = new MotivoAnulacion();
            motivoAnulacionAdapterList = new MotivoAnulacionAdapterList();
            tipoMotivoAnulacionFilter = new TipoMotivoAnulacion();
            tipoMotivoAnulacionAdapterList = new TipoMotivoAnulacionAdapter();
            getTipoMotivoAnulacion();
            getMotivosAnulacion();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
