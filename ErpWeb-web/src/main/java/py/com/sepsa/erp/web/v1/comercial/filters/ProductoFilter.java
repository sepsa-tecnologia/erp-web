package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;

/**
 * Filtro utilizado para el servicio de producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ProductoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public ProductoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param descripcion
     * @return
     */
    public ProductoFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de la código del producto
     *
     * @param codigo
     * @return
     */
    public ProductoFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de bonificable
     *
     * @param bonificable
     * @return
     */
    public ProductoFilter bonificable(String bonificable) {
        if (bonificable != null) {
            params.put("bonificable", bonificable);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public ProductoFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Producto producto, Integer page, Integer pageSize) {
        ProductoFilter filter = new ProductoFilter();

        filter
                .id(producto.getId())
                .descripcion(producto.getDescripcion())
                .codigo(producto.getCodigo())
                .bonificable(producto.getBonificable())
                .activo(producto.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ProductoFilter
     */
    public ProductoFilter() {
        super();
    }
}
