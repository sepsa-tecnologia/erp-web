/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.DepositoLogisticoFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Cliente para el servicio de Motivo
 *
 * @author Romina Núñez
 */
public class DepositoLogisticoService extends APIErpInventario {

    /**
     * Obtiene la lista
     *
     * @param deposito Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public DepositoLogisticoAdapter getDepositoMotivoList(DepositoLogistico deposito, Integer page,
            Integer pageSize) {
        DepositoLogisticoAdapter lista = new DepositoLogisticoAdapter();
        try {
            Map params = DepositoLogisticoFilter.build(deposito, page, pageSize);
            HttpURLConnection conn = GET(Resource.DEPOSITO_LOGISTICO.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        DepositoLogisticoAdapter.class);
                lista = (DepositoLogisticoAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param deposito
     * @return
     */
    public BodyResponse<DepositoLogistico> setDeposito(DepositoLogistico deposito) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.DEPOSITO_LOGISTICO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(deposito));
            response = BodyResponse.createInstance(conn, DepositoLogistico.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param deposito
     * @return
     */
    public BodyResponse<DepositoLogistico> editDeposito(DepositoLogistico deposito) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.DEPOSITO_LOGISTICO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(deposito));
            response = BodyResponse.createInstance(conn, DepositoLogistico.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
         */
    public DepositoLogistico get(Integer id) {
        DepositoLogistico data = new DepositoLogistico(id);
        data.setListadoPojo(true);
        DepositoLogisticoAdapter list = getDepositoMotivoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public DepositoLogisticoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        DEPOSITO_LOGISTICO("Deposito Logistico", "deposito-logistico");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
