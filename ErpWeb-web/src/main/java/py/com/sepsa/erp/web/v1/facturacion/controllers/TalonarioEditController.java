/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoDocumentoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoDocumento;
import py.com.sepsa.erp.web.v1.facturacion.remote.TalonarioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para Editar Talonario
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("talonarioEdit")
public class TalonarioEditController implements Serializable {

    /**
     * Cliente para el servicio de talonario.
     */
    private final TalonarioService service;
    /**
     * Datos del talonario
     */
    private Talonario talonario;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private TipoDocumentoAdapter tipoDocumentoAdapterList;
    /**
     * POJO de tipo documento
     */
    private TipoDocumento tipoDocumentoFilter;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private LocalListAdapter localAdapterList;

    /**
     * POJO de tipo documento
     */
    private Local localFilter;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapter;
    /**
     * POJO Encargado
     */
    private Usuario encargado;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Talonario getTalonario() {
        return talonario;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public void setAdapter(UserListAdapter adapter) {
        this.adapter = adapter;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public UserListAdapter getAdapter() {
        return adapter;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoDocumentoAdapter getTipoDocumentoAdapterList() {
        return tipoDocumentoAdapterList;
    }

    public void setTipoDocumentoAdapterList(TipoDocumentoAdapter tipoDocumentoAdapterList) {
        this.tipoDocumentoAdapterList = tipoDocumentoAdapterList;
    }

    public TipoDocumento getTipoDocumentoFilter() {
        return tipoDocumentoFilter;
    }

    public void setTipoDocumentoFilter(TipoDocumento tipoDocumentoFilter) {
        this.tipoDocumentoFilter = tipoDocumentoFilter;
    }

    public LocalListAdapter getLocalAdapterList() {
        return localAdapterList;
    }

    public void setLocalAdapterList(LocalListAdapter localAdapterList) {
        this.localAdapterList = localAdapterList;
    }

    public Local getLocalFilter() {
        return localFilter;
    }

    public void setLocalFilter(Local localFilter) {
        this.localFilter = localFilter;
    }
    //</editor-fold>

    /**
     * Método para editar Talonario
     */
    public void edit() {
        BodyResponse<Talonario> respuestaTalonario = service.editTalonario(this.talonario);
        if (respuestaTalonario.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Talonario editado correctamente!"));
        } else {
            for (Mensaje mensaje : respuestaTalonario.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }
        }
    }

    /**
     * Método para obtener tipo documento
     */
    public void getTipoDocumento() {
        this.setTipoDocumentoAdapterList(getTipoDocumentoAdapterList().fillData(getTipoDocumentoFilter()));
    }

    /**
     * Método para obtener local
     */
    public void getLocales() {
        this.setLocalAdapterList(getLocalAdapterList().fillData(getLocalFilter()));
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQuery(String query) {

        Usuario encargado = new Usuario();
        encargado.setTienePersonaFisica(true);
        encargado.setNombrePersonaFisica(query);

        adapter = adapter.fillData(encargado);

        return adapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectEncargadoFilter(SelectEvent event) {
        encargado.setId(((Usuario) event.getObject()).getId());
        this.talonario.setIdEncargado(((Usuario) event.getObject()).getIdPersonaFisica());
        System.out.println(talonario.getIdEncargado());
    }

    public void filterEncargados() {
        encargado.setTienePersonaFisica(true);
        adapter = adapter.fillData(encargado);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryLocal(String query) {
        Local local = new Local();
        local.setDescripcion(query);
        localAdapterList = localAdapterList.fillData(local);
        return localAdapterList.getData();
    }

    public void onChangeTipoTalonario() {
        TipoDocumentoAdapter tipoDocumentoAdapter = new TipoDocumentoAdapter();
        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setId(talonario.getIdTipoDocumento());
        tipoDocumentoAdapter = tipoDocumentoAdapter.fillData(tipoDocumento);
        String cod = tipoDocumentoAdapter.getData().get(0).getCodigo();
        if (cod.equalsIgnoreCase("FACTURA")) {
            obtenerConfiguracion("TIPO_TALONARIO_FACTURA_DIGITAL");
        }

        if (cod.equalsIgnoreCase("RECIBO")) {
            obtenerConfiguracion("TIPO_TALONARIO_RECIBO_DIGITAL");
        }

        if (cod.equalsIgnoreCase("NOTA_CREDITO")) {
            obtenerConfiguracion("TIPO_TALONARIO_NC_DIGITAL");
        }
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion(String configuracion) {
        configuracionValorFilter.setCodigoConfiguracion(configuracion);
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);
        talonario.setDigital(adapterConfigValor.getData().get(0).getValor());
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalFilter(SelectEvent event) {
        talonario.setIdLocal(((Local) event.getObject()).getId());
    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.talonario = service.get(id);
        this.adapter = new UserListAdapter();
        this.encargado = new Usuario();
        this.localAdapterList = new LocalListAdapter();
        this.tipoDocumentoAdapterList = new TipoDocumentoAdapter();
        this.localFilter = new Local();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        if (talonario.getLocal() != null) {
            this.localFilter = talonario.getLocal();
        }
        
        if(talonario.getIdEncargado() != null){
            Usuario encargado = new Usuario();
            encargado.setTienePersonaFisica(true);
            encargado.setIdPersonaFisica(talonario.getIdEncargado());

            adapter = adapter.fillData(encargado);
            this.encargado = adapter.getData().get(0);
        }
        
            
        this.tipoDocumentoFilter = new TipoDocumento();
        getTipoDocumento();
        getLocales();
        filterEncargados();
    }

    /**
     * Constructor
     */
    public TalonarioEditController() {
        this.service = new TalonarioService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
