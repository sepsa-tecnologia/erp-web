/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;
import py.com.sepsa.erp.web.v1.info.remote.TipoEstadoService;

/**
 * Adaptador para tipo tipoEstado
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEstadoAdapter extends DataListAdapter<TipoEstado> {

    /**
     * Cliente para los servicios de tipoEstado
     */
    private final TipoEstadoService tipoEstadoService;

    /**
     * Constructor de ClientListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoEstadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoEstadoService = new TipoEstadoService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public TipoEstadoAdapter() {
        super();
        this.tipoEstadoService = new TipoEstadoService();
    }

    @Override
    public TipoEstadoAdapter fillData(TipoEstado searchData) {
        return tipoEstadoService.getTipoEstadoList(searchData, getFirstResult(), getPageSize());
    }
}
