package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProductoPojo;

/**
 * Filtro utilizado para el servicio de producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ProductoPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public ProductoPojoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param descripcion
     * @return
     */
    public ProductoPojoFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de la código del producto
     *
     * @param codigo
     * @return
     */
    public ProductoPojoFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de bonificable
     *
     * @param bonificable
     * @return
     */
    public ProductoPojoFilter bonificable(String bonificable) {
        if (bonificable != null) {
            params.put("bonificable", bonificable);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public ProductoPojoFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public ProductoPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ProductoPojo producto, Integer page, Integer pageSize) {
        ProductoPojoFilter filter = new ProductoPojoFilter();

        filter
                .id(producto.getId())
                .descripcion(producto.getDescripcion())
                .activo(producto.getActivo())
                .listadoPojo(producto.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ProductoFilter
     */
    public ProductoPojoFilter() {
        super();
    }
}
