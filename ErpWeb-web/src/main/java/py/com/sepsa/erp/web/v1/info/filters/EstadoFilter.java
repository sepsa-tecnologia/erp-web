package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * Filtro utilizado para el servicio de estado
 *
 * @author Romina Núñez
 * @author Sergio D. Riveros Vazquez
 */
public class EstadoFilter extends Filter {

    /**
     * Agrega el filtro de identificador estado
     *
     * @param id Identificador del rol
     * @return
     */
    public EstadoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion descripción del estado
     * @return
     */
    public EstadoFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo
     *
     * @param codigo del estado
     * @return
     */
    public EstadoFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo bandera
     * @return
     */
    public EstadoFilter activo(String activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo de tipo estado
     *
     * @param codigoTipoEstado codigo de tipo estado
     * @return
     */
    public EstadoFilter codigoTipoEstado(String codigoTipoEstado) {
        if (codigoTipoEstado != null) {
            params.put("codigoTipoEstado", codigoTipoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador
     *
     * @param idTipoEstado
     * @return
     */
    public EstadoFilter idTipoEstado(Integer idTipoEstado) {
        if (idTipoEstado != null) {
            params.put("idTipoEstado", idTipoEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param estado datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Estado estado, Integer page, Integer pageSize) {
        EstadoFilter filter = new EstadoFilter();
        
        filter
                .id(estado.getId())
                .descripcion(estado.getDescripcion())
                .codigo(estado.getCodigo())
                .activo(estado.getActivo())
                .codigoTipoEstado(estado.getCodigoTipoEstado())
                .idTipoEstado(estado.getIdTipoEstado())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de RolFilter
     */
    public EstadoFilter() {
        super();
    }
    
}
