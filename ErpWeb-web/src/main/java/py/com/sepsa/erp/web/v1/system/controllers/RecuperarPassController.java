package py.com.sepsa.erp.web.v1.system.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.remote.InfoPojo;
import py.com.sepsa.erp.web.v1.system.pojos.Credential;
import py.com.sepsa.erp.web.v1.system.pojos.Token;
import py.com.sepsa.erp.web.v1.system.remote.LoginServiceClient;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;

/**
 * Controlador para recupera la contraseña
 *
 * @author Cristina Insfrán
 */
@ViewScoped
@Named("passRecUser")
public class RecuperarPassController implements Serializable {

    /**
     * Objeto Usuario
     */
    private Usuario usuario;
    /**
     * Adaptador de usuario
     */
    private UserServiceClient userServiceClient;
    /**
     *
     */
    BodyResponse response;
    /**
     * Servicios para login al sistema
     */
    private LoginServiceClient serviceClient;
    /**
     * Contraseña nueva
     */
    private String pass;
    /**
     * Confirmar contraseña nueva
     */
    private String passConfirmation;
    /**
     * Variable para mostrar mensaje
     */
    private Boolean cambio = false;
    /**
     * hash de la url
     */
    private String hash;
    /**
     * Bandera de hash válido
     */
    private Boolean hashValido;
    

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the cambio
     */
    public Boolean getCambio() {
        return cambio;
    }

    /**
     * @param cambio the cambio to set
     */
    public void setCambio(Boolean cambio) {
        this.cambio = cambio;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the passConfirmation
     */
    public String getPassConfirmation() {
        return passConfirmation;
    }

    /**
     * @param passConfirmation the passConfirmation to set
     */
    public void setPassConfirmation(String passConfirmation) {
        this.passConfirmation = passConfirmation;
    }

    /**
     * @return the serviceClient
     */
    public LoginServiceClient getServiceClient() {
        return serviceClient;
    }

    /**
     * @param serviceClient the serviceClient to set
     */
    public void setServiceClient(LoginServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the userServiceClient
     */
    public UserServiceClient getUserServiceClient() {
        return userServiceClient;
    }

    /**
     * @param userServiceClient the userServiceClient to set
     */
    public void setUserServiceClient(UserServiceClient userServiceClient) {
        this.userServiceClient = userServiceClient;
    }

    public Boolean getHashValido() {
        return hashValido;
    }

    public void setHashValido(Boolean hashValido) {
        this.hashValido = hashValido;
    }
    
//</editor-fold>
    /**
     * Método para recupera contraseña
     */
    public void setCorreoRecuperacion() {

        if (pass != null && passConfirmation != null) {

            if (pass.equals(passConfirmation)) {

                usuario.setContrasena(pass);

                usuario.setHash(hash);

                BodyResponse resp = new BodyResponse();
                try {
                    resp = userServiceClient.recuperarPass(usuario);
                } catch (Exception e) {
                    WebLogger.get().fatal(e);
                }

                if (resp.getSuccess().equals(false)) {

                    if (!resp.getStatus().getMensajes().isEmpty()) {
                        for (int i = 0; i < resp.getStatus().getMensajes().size(); i++) {
                            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                                    "Advertencia", resp.getStatus().getMensajes().get(i).getDescripcion());
                            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                        }
                    }
                } else {
                    cambio = true;
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Contraseña Actualizada exitosamente");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                }

            } else {
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Advertencia", "No coincide las contraseñas");
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            }
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Advertencia", "Debe completar los campos");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }

    }

    /**
     * Obtiene el parámetro desde la URL
     */
    private String getRequestParameteer() {
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();

        try {
            Map<String, String> params = ctx.getRequestParameterMap();
            if (params == null) {
                throw new IllegalStateException("No se puede obtener el mapa de parametros.");
            }
            return mapGet(params, "h", "");
        } catch (Throwable thr) {
            WebLogger.get().error("No se ha obtener el parámetro desde el enlace");
            return "";
        }
    }

    private static <K, V> V mapGet(Map<K, V> map, K key, V fallback) {
        // Control de seguridad.
        if (map == null) {
            return fallback;
        }

        if (map.containsKey(key)) {
            V value = map.get(key);
            if (value == null) {
                return fallback;
            } else {
                return value;
            }
        } else {
            return fallback;
        }
    }
    
    /**
     * 
     */
    public void validarHash(){
        
        usuario.setHash(hash);
        usuario.setContrasena("SoloParaValidacion");
        BodyResponse response = userServiceClient.validarHash(usuario);
        if (response.getSuccess()){
            hashValido = true;
        }
        
    }

    /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {
        this.usuario = new Usuario();
        this.serviceClient = new LoginServiceClient();

        InfoPojo info = InfoPojo.createInstanceUser("login-sistemas");
        Credential credential = new Credential();
        credential.setUsuario(info.getUser());
        credential.setContrasena(info.getPass());
        response = serviceClient.login(credential);

        if (response.getSuccess()) {
            this.userServiceClient = new UserServiceClient() {
                @Override
                protected void setBearer() {
                    super.setJwt(((Token) response.getPayload()).getToken() == null ? null : String.format("Bearer %s",
                            ((Token) response.getPayload()).getToken()));
                }
            };
        }
        this.hashValido = false;
        hash = getRequestParameteer();
        if (!hash.isEmpty() || !hash.equalsIgnoreCase("")){
            validarHash();
        }
    }

}
