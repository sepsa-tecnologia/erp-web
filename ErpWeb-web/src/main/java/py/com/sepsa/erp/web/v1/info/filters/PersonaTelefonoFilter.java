package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;

/**
 * Filtro para el servicio persona telefono
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
public class PersonaTelefonoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id Identificador
     * @return
     */
    public PersonaTelefonoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public PersonaTelefonoFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de telefono
     *
     * @param idTelefono
     * @return
     */
    public PersonaTelefonoFilter idTelefono(Integer idTelefono) {
        if (idTelefono != null) {
            params.put("idTelefono", idTelefono);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public PersonaTelefonoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param personaTelefono datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(PersonaTelefono personaTelefono, Integer page, Integer pageSize) {
        PersonaTelefonoFilter filter = new PersonaTelefonoFilter();

        filter
                .id(personaTelefono.getId())
                .idPersona(personaTelefono.getIdPersona())
                .idTelefono(personaTelefono.getIdTelefono())
                .activo(personaTelefono.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PersonaTelefonoFilter
     */
    public PersonaTelefonoFilter() {
        super();
    }

}
