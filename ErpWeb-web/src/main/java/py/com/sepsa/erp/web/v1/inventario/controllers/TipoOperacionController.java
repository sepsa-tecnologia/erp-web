/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.inventario.adapters.TipoOperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;

/**
 * Controlador para la vista tipo operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("tipoOperacionList")
public class TipoOperacionController implements Serializable {

    /**
     * POJO de tipo oepracion
     */
    private TipoOperacion tipoOperacionFilter;
    /**
     * Adaptador para la lista de tipo operacion
     */
    private TipoOperacionAdapter tipoOperacionAdapterList;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoOperacion getTipoOperacionFilter() {
        return tipoOperacionFilter;
    }

    public void setTipoOperacionFilter(TipoOperacion tipoOperacionFilter) {
        this.tipoOperacionFilter = tipoOperacionFilter;
    }

    public TipoOperacionAdapter getTipoOperacionAdapterList() {
        return tipoOperacionAdapterList;
    }

    public void setTipoOperacionAdapterList(TipoOperacionAdapter tipoOperacionAdapterList) {
        this.tipoOperacionAdapterList = tipoOperacionAdapterList;
    }
    //</editor-fold>
    /**
     * Método para obtener tipo operacion
     */
    public void getTipoOperaciones() {
        getTipoOperacionAdapterList().setFirstResult(0);
        this.tipoOperacionAdapterList = this.tipoOperacionAdapterList.fillData(tipoOperacionFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.tipoOperacionFilter = new TipoOperacion();
        getTipoOperaciones();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("tipo-operacion-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.tipoOperacionFilter = new TipoOperacion();
        this.tipoOperacionAdapterList = new TipoOperacionAdapter();
        getTipoOperaciones();
    }

    /**
     * Constructor
     */
    public TipoOperacionController() {
        init();
    }

}
