package py.com.sepsa.erp.web.v1.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

/**
 * Utilidades para JWT
 * @author Daniel F. Escauriza Arza
 */
public class JWTUtils {
    
    /**
     * Obtiene los datos del token
     * @param jwt Token JWT
     * @return Datos del token //TODO: Posibles otros usos
     * @throws org.jose4j.lang.JoseException 
     */
    public static JwtClaims getJwtClaims(String jwt) throws Exception {
        
        JwtConsumerBuilder jwtConsumerBuilder = new JwtConsumerBuilder()
                .setSkipSignatureVerification();
         
        JwtConsumer jwtConsumer = jwtConsumerBuilder.build();

        return jwtConsumer.processToClaims(jwt);
    }
    
    /**
     * Obtiene los datos del token
     * @param jwt Token JWT
     * @return Datos del token //TODO: Posibles otros usos
     * @throws org.jose4j.lang.JoseException 
     */
    public static JsonObject getJwtData(String jwt) throws Exception {
        
        JsonObject result = null;
        try {
            // The shared secret or shared symmetric key represented as a octet sequence JSON Web Key (JWK)
            String jwkJson = "{\"kty\":\"oct\",\"k\":\"Fdh9u8rINbbnairbvifxVT1u232VQBZYKx1PGAGHt2I\"}";
            JsonWebKey jwk = JsonWebKey.Factory.newJwk(jwkJson);

            // That other party, the receiver, can then use JsonWebEncryption to decrypt the message.
            JsonWebEncryption receiverJwe = new JsonWebEncryption();
            // Set the algorithm constraints based on what is agreed upon or expected from the sender
            AlgorithmConstraints algConstraints = new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, KeyManagementAlgorithmIdentifiers.DIRECT);
            receiverJwe.setAlgorithmConstraints(algConstraints);
            AlgorithmConstraints encConstraints = new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
            receiverJwe.setContentEncryptionAlgorithmConstraints(encConstraints);
            // Set the compact serialization on new Json Web Encryption object
            receiverJwe.setCompactSerialization(jwt);
            // Symmetric encryption, like we are doing here, requires that both parties have the same key.
            // The key will have had to have been securely exchanged out-of-band somehow.
            receiverJwe.setKey(jwk.getKey());
            // Get the message that was encrypted in the JWE. This step performs the actual decryption steps.
            String plaintext = receiverJwe.getPlaintextString();
            
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(plaintext);
            result = element.getAsJsonObject();
        } catch(Exception ex) {}
        
        return result;
    }
}
