/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;
import java.util.List;

/**
 * Pojo para Control Inventario
 *
 * @author Romina Nuñez
 */
public class ControlInventario {

    /**
     * Id
     */
    private Integer id;
    /**
     * Id Local
     */
    private Integer idUsuarioEncargado;
    /**
     * Codigo
     */
    private String usuarioEncargado;
    /**
     * Id Local
     */
    private Integer idUsuarioSupervisor;
    /**
     * Descripcion
     */
    private String usuarioSupervisor;
    /**
     * Id Local
     */
    private Integer idMotivo;
    /**
     * Activo
     */
    private String motivo;
    /**
     * Activo
     */
    private String codigoMotivo;
    /**
     * Id Local
     */
    private Integer idEstado;
    /**
     * Activo
     */
    private String estado;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercion;
    /**
     * Listado Pojo
     */
    private Boolean listadoPojo;
    /**
     * cliente
     */
    private String codigoEstado;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercionDesde;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercionHasta;
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
        /**
     * Identificador de deposito logistico
     */
    private Integer idProducto;
        /**
     * Lista para cobro
     */
    private List<Integer> idDepositoLogisticos;
    /**
     * Lista para cobro
     */
    private List<Integer> idProductos;
    /**
     * Codigo de control
     */
    private String codigo;
        /**
     * Lista Detalle
     */
    private List<ControlInventarioDetalle> controlInventarioDetalles;

    public Integer getId() {
        return id;
    }

    public void setControlInventarioDetalles(List<ControlInventarioDetalle> controlInventarioDetalles) {
        this.controlInventarioDetalles = controlInventarioDetalles;
    }

    public List<ControlInventarioDetalle> getControlInventarioDetalles() {
        return controlInventarioDetalles;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public List<Integer> getIdDepositoLogisticos() {
        return idDepositoLogisticos;
    }

    public void setIdDepositoLogisticos(List<Integer> idDepositoLogisticos) {
        this.idDepositoLogisticos = idDepositoLogisticos;
    }

    public List<Integer> getIdProductos() {
        return idProductos;
    }

    public void setIdProductos(List<Integer> idProductos) {
        this.idProductos = idProductos;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuarioEncargado() {
        return idUsuarioEncargado;
    }

    public void setIdUsuarioEncargado(Integer idUsuarioEncargado) {
        this.idUsuarioEncargado = idUsuarioEncargado;
    }

    public String getUsuarioEncargado() {
        return usuarioEncargado;
    }

    public void setUsuarioEncargado(String usuarioEncargado) {
        this.usuarioEncargado = usuarioEncargado;
    }

    public Integer getIdUsuarioSupervisor() {
        return idUsuarioSupervisor;
    }

    public void setIdUsuarioSupervisor(Integer idUsuarioSupervisor) {
        this.idUsuarioSupervisor = idUsuarioSupervisor;
    }

    public String getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(String usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    

    /**
     * Constructor de la clase
     */
    public ControlInventario() {

    }

    public ControlInventario(Integer id) {
        this.id = id;
    }

}
