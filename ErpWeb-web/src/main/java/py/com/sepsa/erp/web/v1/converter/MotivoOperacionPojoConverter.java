/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.MotivoService;

/**
 * Converter para motivo operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
@FacesConverter("motivoOperacionPojoConverter")
public class MotivoOperacionPojoConverter implements Converter {

    /**
     * Servicio para motivo estado
     */
    private MotivoService motivoOperacionService;

    @Override
    public Motivo getAsObject(FacesContext fc, UIComponent uic, String value) {

        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            motivoOperacionService = new MotivoService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            Motivo motivoOperacion = new Motivo();
            motivoOperacion.setId(val);

            List<Motivo> motivoOperacions = motivoOperacionService.getMotivo(motivoOperacion, 0, 10).getData();
            if (motivoOperacions != null && !motivoOperacions.isEmpty()) {
                return motivoOperacions.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "No es un Motivo Operacion válido"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof Motivo) {
                return String.valueOf(((Motivo) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
