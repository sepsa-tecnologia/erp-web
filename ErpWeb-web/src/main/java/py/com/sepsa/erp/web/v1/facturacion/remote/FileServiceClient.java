package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para los servicios de archivos
 *
 * @author Daniel F. Escauriza Arza
 */
public class FileServiceClient extends APIErpFacturacion {

    /**
     * Método para descargar un archivo desde el servidor
     *
     * @param url
     * @return Contenido descargado
     */
    public byte[] download(String url) {

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, null);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }
    
    public byte[] downloadNotaCredito(String url, Map parametros) {
        
        Map params = new HashMap();
        params = parametros;

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, params);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }
    
    public byte[] downloadNotaDebito(String url, Map parametros) {
        
        Map params = new HashMap();
        params = parametros;

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, params);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }

    /**
     * Método para descargar un archivo desde el servidor
     *
     * @param url
     * @return Contenido descargado
     */
    public byte[] downloadFactura(String url, Map parametros) {

        Map params = new HashMap();
        params = parametros;

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, params);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }
    
    /**
     * Constructor de FileServiceClient
     */
    public FileServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del API
     */
    public enum Resource {

        //Servicios
        UPLOAD("Subir archivo", "upload/%s"),
        DOWNLOAD_NOTA_CREDITO("Descargar Nota Credito", "nota-credito/consulta/%s");
        //DOWNLOAD_NOTA_DEBITO("Descargar Nota Debito", "nota-debito/consulta/%s");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
