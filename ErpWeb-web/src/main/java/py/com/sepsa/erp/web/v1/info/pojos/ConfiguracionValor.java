package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de Configuración valor
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class ConfiguracionValor {

    /**
     * Identificador de la configuración
     */
    private Integer id;
    /**
     * Identificador del atributo
     */
    private String valor;
    /**
     * Etiqueta
     */
    private String activo;
    /**
     * Descripcion
     */
    private Integer idConfiguracion;
    /**
     * Código de configuración
     */
    private String codigoConfiguracion;
    /**
     * Valor
     */
    private Configuracion configuracion;
    /**
     * Identificador de la empresa
     */
    private Integer idEmpresa;

    /**
     * Identificador de la empresa
     */
    private Empresa empresa;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getId() {
        return id;
    }

    public void setCodigoConfiguracion(String codigoConfiguracion) {
        this.codigoConfiguracion = codigoConfiguracion;
    }

    public String getCodigoConfiguracion() {
        return codigoConfiguracion;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(Integer idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public Configuracion getConfiguracion() {
        return configuracion;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void setConfiguracion(Configuracion configuracion) {
        this.configuracion = configuracion;
    }

    //</editor-fold>
    /**
     * Constructor
     */
    public ConfiguracionValor() {

    }

}
