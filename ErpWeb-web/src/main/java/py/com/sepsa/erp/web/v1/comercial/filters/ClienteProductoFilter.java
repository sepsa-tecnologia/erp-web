package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteProducto;

/**
 * Filtro utilizado para el servicio de Cliente Producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ClienteProductoFilter extends Filter {

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param idCliente
     * @return
     */
    public ClienteProductoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public ClienteProductoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del producto
     *
     * @param producto
     * @return
     */
    public ClienteProductoFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro de estado
     *
     * @param estado
     * @return
     */
    public ClienteProductoFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro de estado
     *
     * @param estado
     * @return
     */
    public ClienteProductoFilter facturable(String facturable) {
        if (facturable != null && !facturable.trim().isEmpty()) {
            params.put("facturable", facturable);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene
     *
     * @param tiene 
     * @return
     */
    public ClienteProductoFilter tiene(String tiene) {
        if (tiene != null && !tiene.trim().isEmpty()) {
            params.put("tiene", tiene);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param productos datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ClienteProducto clienteProducto, Integer page, Integer pageSize) {
        ClienteProductoFilter filter = new ClienteProductoFilter();

        filter
                .idCliente(clienteProducto.getIdCliente())
                .idProducto(clienteProducto.getIdProducto())
                .producto(clienteProducto.getProducto())
                .estado(clienteProducto.getEstado())
                .facturable(clienteProducto.getFacturable())
                .tiene(clienteProducto.getTiene())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClienteProductoFilter
     */
    public ClienteProductoFilter() {
        super();
    }
}
