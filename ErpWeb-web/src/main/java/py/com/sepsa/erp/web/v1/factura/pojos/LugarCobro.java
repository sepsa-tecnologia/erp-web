package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 * POJO para la lista de lugar cobro
 *
 * @author Cristina Insfrán
 */
public class LugarCobro {

   
    /**
     * Identificador
     */
    private Integer id;

    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Codigo 
     */
    private String codigo;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
     /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    public LugarCobro(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public LugarCobro() {

    }

}
