package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.MotivoDescuentoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.MotivoDescuentoFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.MotivoDescuento;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio de Motivo Descuento
 *
 * @author Romina Núñez
 */
public class MotivoDescuentoService extends APIErpComercial {

    /**
     * Obtiene la lista de descuentos
     *
     * @param motivoDescuento 
     * @param page
     * @param pageSize
     * @return
     */
    public MotivoDescuentoAdapter getMotivoDescuentoList(MotivoDescuento motivoDescuento, Integer page,
            Integer pageSize) {

        MotivoDescuentoAdapter lista = new MotivoDescuentoAdapter();

        Map params = MotivoDescuentoFilter.build(motivoDescuento, page, pageSize);

        HttpURLConnection conn = GET(Resource.MOTIVODESCUENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MotivoDescuentoAdapter.class);

            lista = (MotivoDescuentoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
     /**
     * Método para crear Motivo Descuento
     *
     * @param motivoDescuento  
     * @return
     */
    public Integer createMotivoDescuento(MotivoDescuento motivoDescuento) {

        Integer id = null;

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.MOTIVODESCUENTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoDescuento));

            response = BodyResponse.createInstance(conn, MotivoDescuento.class);

            id = ((MotivoDescuento) response.getPayload()).getId();
           
        }
        return id;
    }
    
    /**
     * Método para editar motivo descuento
     *
     * @param motivoDescuento   
     * @return
     */
    public MotivoDescuento editMotivoDescuento(MotivoDescuento motivoDescuento) {

        MotivoDescuento motivoDescuentoEdit = new MotivoDescuento();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.MOTIVODESCUENTO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoDescuento));

            response = BodyResponse.createInstance(conn, MotivoDescuento.class);

            motivoDescuentoEdit = ((MotivoDescuento) response.getPayload());
           
        }
        return motivoDescuentoEdit;
    }
    
    /**
     * Constructor de DescuentoService
     */
    public MotivoDescuentoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicios
        MOTIVODESCUENTO("MotivoDescuento", "motivo-descuento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
