package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionCadenaDetalleAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionCadenaDetalle;

/**
 * Controlador para lista de detalle de liquidaciones por cadena.
 *
 * @author alext
 */
@ViewScoped
@Named("liquidacionCadenaDetalle")
public class LiquidacionCadenaDetalleController implements Serializable {

    private String cliente;
    
    /**
     * Adaptador.
     */
    private LiquidacionCadenaDetalleAdapter adapter;
    /**
     * Objeto.
     */
    private LiquidacionCadenaDetalle searchData;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public LiquidacionCadenaDetalleAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LiquidacionCadenaDetalleAdapter adapter) {
        this.adapter = adapter;
    }

    public LiquidacionCadenaDetalle getSearchData() {
        return searchData;
    }

    public void setSearchData(LiquidacionCadenaDetalle searchData) {
        this.searchData = searchData;
    }
    
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    //</editor-fold>

    /**
     * Método para generar la lista.
     */
    public void search(Integer idCliente, String mes, String ano, String cliente) {
        searchData.setIdCliente(idCliente);
        searchData.setMes(mes);
        searchData.setAno(ano);
        this.cliente = cliente;
        adapter = new LiquidacionCadenaDetalleAdapter();
        adapter = adapter.fillData(searchData);
    }


    /**
     * Inicializa los datos del controlador.
     */
    public LiquidacionCadenaDetalleController() {
        adapter = new LiquidacionCadenaDetalleAdapter();
        searchData = new LiquidacionCadenaDetalle();
    }
}
