
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO  dato persona
 * @author Cristina Insfrán
 */
public class DatoPersona {

    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del tipo de dato
     */
    private Integer idTipoDatoPersona;
    /**
     * Valor
     */
    private String valor;
    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the idTipoDatoPersona
     */
    public Integer getIdTipoDatoPersona() {
        return idTipoDatoPersona;
    }

    /**
     * @param idTipoDatoPersona the idTipoDatoPersona to set
     */
    public void setIdTipoDatoPersona(Integer idTipoDatoPersona) {
        this.idTipoDatoPersona = idTipoDatoPersona;
    }

    /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }
    
    /**
     * Contructor de la clase
     */
    public DatoPersona(){
        
    }
    /**
     * Constructor de la clase
     * @param idPersona
     * @param idTipoDatoPersona
     * @param valor
     */
    public DatoPersona(Integer idPersona, Integer idTipoDatoPersona,
            String valor){
        this.idPersona =  idPersona;
        this.idTipoDatoPersona = idTipoDatoPersona;
        this.valor = valor;
    }
    
}
