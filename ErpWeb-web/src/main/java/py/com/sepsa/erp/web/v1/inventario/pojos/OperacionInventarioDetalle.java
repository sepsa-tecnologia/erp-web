package py.com.sepsa.erp.web.v1.inventario.pojos;

import java.util.Date;

/**
 * Pojo para listado de detalle de operación de inventario
 *
 * @author Alexander Triana
 */
public class OperacionInventarioDetalle {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de detalle de operación
     */
    private Integer idOperacionInventario;
    /**
     * Identificador de detalle de operación
     */
    private Integer idDepositoOrigen;
      /**
     * Identificador de detalle de operación
     */
    private Integer idDepositoDestino;
          /**
     * Identificador de detalle de operación
     */
    private Integer idDepositoLogistico;
    /**
     * Codigo Deposito
     */
    private String codigoDeposito;
    /**
     * Identificador de estado
     */
    private Integer idEstadoInventarioOrigen;
        /**
     * Identificador de estado
     */
    private Integer idEstadoInventarioDestino;
           /**
     * Pojo de estado
     */
    private String estadoInventarioOrigen;
    /**
     * Pojo de estado
     */
    private String codigoEstadoInventarioOrigen;
           /**
     * Pojo de estado
     */
    private String estadoInventarioDestino;
    /**
     * Pojo de estado
     */
    private String codigoEstadoInventarioDestino;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Parámetro cantidad
     */
    private Integer cantidadInicial;
    /**
     * Parámetro cantidad
     */
    private Integer cantidadFinal;
    /**
     * Parámetro cantidad
     */
    private Integer cantidad;
    /**
     * Parámetro condigo gtin
     */
    private String codigoGtin;
    /**
     * Parámetro codigo interno
     */
    private String codigoInterno;
    /**
     * Pojo de producto
     */
    private String producto;
    /**
     * Pojo de estado
     */
    private String estadoOperacion;
            /**
     * Pojo de estado
     */
    private String codigoEstadoOperacion;

    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Listado pojo
     */
    private String listadoPojo;
    private Integer cantidadControl;
    private Integer idControlInventarioDetalle;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public Integer getIdEstadoInventarioOrigen() {
        return idEstadoInventarioOrigen;
    }

    public void setIdEstadoInventarioOrigen(Integer idEstadoInventarioOrigen) {
        this.idEstadoInventarioOrigen = idEstadoInventarioOrigen;
    }

    public Integer getIdEstadoInventarioDestino() {
        return idEstadoInventarioDestino;
    }

    public void setIdEstadoInventarioDestino(Integer idEstadoInventarioDestino) {
        this.idEstadoInventarioDestino = idEstadoInventarioDestino;
    }

    public String getEstadoInventarioOrigen() {
        return estadoInventarioOrigen;
    }

    public void setEstadoInventarioOrigen(String estadoInventarioOrigen) {
        this.estadoInventarioOrigen = estadoInventarioOrigen;
    }

    public String getCodigoEstadoInventarioOrigen() {
        return codigoEstadoInventarioOrigen;
    }

    public void setCodigoEstadoInventarioOrigen(String codigoEstadoInventarioOrigen) {
        this.codigoEstadoInventarioOrigen = codigoEstadoInventarioOrigen;
    }

    public String getEstadoInventarioDestino() {
        return estadoInventarioDestino;
    }

    public void setEstadoInventarioDestino(String estadoInventarioDestino) {
        this.estadoInventarioDestino = estadoInventarioDestino;
    }

    public String getCodigoEstadoInventarioDestino() {
        return codigoEstadoInventarioDestino;
    }

    public void setCodigoEstadoInventarioDestino(String codigoEstadoInventarioDestino) {
        this.codigoEstadoInventarioDestino = codigoEstadoInventarioDestino;
    }

    public void setIdControlInventarioDetalle(Integer idControlInventarioDetalle) {
        this.idControlInventarioDetalle = idControlInventarioDetalle;
    }

    public void setEstadoOperacion(String estadoOperacion) {
        this.estadoOperacion = estadoOperacion;
    }

    public void setCodigoEstadoOperacion(String codigoEstadoOperacion) {
        this.codigoEstadoOperacion = codigoEstadoOperacion;
    }

    public String getEstadoOperacion() {
        return estadoOperacion;
    }

    public String getCodigoEstadoOperacion() {
        return codigoEstadoOperacion;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public Integer getIdControlInventarioDetalle() {
        return idControlInventarioDetalle;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCodigoDeposito(String codigoDeposito) {
        this.codigoDeposito = codigoDeposito;
    }

    public String getCodigoDeposito() {
        return codigoDeposito;
    }

    public void setIdDepositoOrigen(Integer idDepositoOrigen) {
        this.idDepositoOrigen = idDepositoOrigen;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public Integer getIdDepositoOrigen() {
        return idDepositoOrigen;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setCantidadControl(Integer cantidadControl) {
        this.cantidadControl = cantidadControl;
    }

    public Integer getCantidadControl() {
        return cantidadControl;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOperacionInventario() {
        return idOperacionInventario;
    }

    public void setIdOperacionInventario(Integer idOperacionInventario) {
        this.idOperacionInventario = idOperacionInventario;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getListadoPojo() {
        return listadoPojo;
    }

    public void setListadoPojo(String listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public OperacionInventarioDetalle() {

    }

}
