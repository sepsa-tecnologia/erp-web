
package py.com.sepsa.erp.web.v1.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Formatea hora inicio y fin
 * @author Cristina Insfran
 */
public class FormaDateTime {
    
    public static Date truncateTime(Date date) {
        if(date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTime();
        }
        return null;
    }

/**
     * Trunca el tiempo de una fecha
     * @param date Fecha
     * @return Fecha con tiempo 0
     */
    public static Date set24HourTime(Date date) {
        if(date != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 999);
            return cal.getTime();
        }
        return null;
    }
}
