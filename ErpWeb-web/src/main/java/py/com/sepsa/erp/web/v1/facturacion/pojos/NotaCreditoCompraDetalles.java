/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;

/**
 * Pojo para Nota de credito compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotaCreditoCompraDetalles {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de nota credito compra
     */
    private Integer idNotaCreditoCompra;
    /**
     * Identificador factura compra
     */
    private Integer idFacturaCompra;
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * porcentaje Iva
     */
    private Integer porcentajeIva;
    /**
     * cantidad
     */
    private Integer cantidad;
    /**
     * Precio unitario con Iva
     */
    private BigDecimal precioUnitarioConIva;
    /**
     * Precio unitario sin Iva
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * monto Iva
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Factura compra
     */
    private FacturaCompra facturaCompra;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdNotaCreditoCompra() {
        return idNotaCreditoCompra;
    }

    public void setIdNotaCreditoCompra(Integer idNotaCreditoCompra) {
        this.idNotaCreditoCompra = idNotaCreditoCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public FacturaCompra getFacturaCompra() {
        return facturaCompra;
    }

    public void setFacturaCompra(FacturaCompra facturaCompra) {
        this.facturaCompra = facturaCompra;
    }
    //</editor-fold>

    public NotaCreditoCompraDetalles() {
    }

}
