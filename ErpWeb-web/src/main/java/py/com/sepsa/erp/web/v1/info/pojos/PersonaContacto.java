package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para persona contacto
 *
 * @author Cristina Insfrán, Sergio D. Riveros Vazquez
 */
public class PersonaContacto {


    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Identificador del tipo contacto
     */
    private Integer idTipoContacto;
    /**
     * Identificador del cargo
     */
    private Integer idCargo;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador del contacto
     */
    private Contacto contacto;
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    /**
     * Estado 
     */
    private Estado estado;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getIdPersona() {
        return idPersona;
    }
    
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }
    
    public String getActivo() {
        return activo;
    }
    
    public void setActivo(String activo) {
        this.activo = activo;
    }
    
    public Contacto getContacto() {
        return contacto;
    }
    
    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }
    
    public Integer getIdContacto() {
        return idContacto;
    }
    
    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }
    
    /**
     * @return the idTipoContacto
     */
    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }
    
    /**
     * @param idTipoContacto the idTipoContacto to set
     */
    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }
    
    /**
     * @return the idCargo
     */
    public Integer getIdCargo() {
        return idCargo;
    }
    
    /**
     * @param idCargo the idCargo to set
     */
    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }
    
    /**
     * @return the estado
     */
    public Estado getEstado() {
        return estado;
    }
    
    /**
     * @param estado the estado to set
     */
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
//</editor-fold>

    /**
     * Constructor
     */
    public PersonaContacto() {

    }

    /**
     * Constructor con parametros
     *
     * @param idPersona
     * @param idContacto
     */
    public PersonaContacto(Integer idPersona, Integer idContacto) {
        this.idPersona = idPersona;
    }

}
