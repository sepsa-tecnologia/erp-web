
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.UsuarioLocal;
import py.com.sepsa.erp.web.v1.info.remote.UsuarioLocalServiceClient;

/**
 * Adaptador para la lista de usuario local relacionado
 * @author Romina Núñez
 */
public class UsuarioLocalRelacionadoAdapter extends DataListAdapter<UsuarioLocal>{
    
    /**
     * Cliente para los servicios de dato persona
     */
    private final UsuarioLocalServiceClient usuarioLocalClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioLocalRelacionadoAdapter fillData(UsuarioLocal searchData) {
     
        return usuarioLocalClient.getUsuarioLocalRelacionadoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de DatoPersonaListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioLocalRelacionadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.usuarioLocalClient = new UsuarioLocalServiceClient();
    }

    /**
     * Constructor de UsuarioLocalListAdapter
     */
    public UsuarioLocalRelacionadoAdapter() {
        super();
        this.usuarioLocalClient= new UsuarioLocalServiceClient();
    }
}
