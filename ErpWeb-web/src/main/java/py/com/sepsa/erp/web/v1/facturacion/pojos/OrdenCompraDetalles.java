/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * pojo para detalles de orden de compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OrdenCompraDetalles {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de orden de compra
     */
    private Integer idOrdenCompra;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidadSolicitada;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidadConfirmada;
    /**
     * Producto asociado
     */
    private Producto producto;
    /**
     *
     */
    private BigDecimal precioUnitarioConIva;
    /**
     *
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     *
     */
    private BigDecimal montoIva;
    /**
     *
     */
    private BigDecimal montoImponible;
    /**
     *
     */
    private BigDecimal montoTotal;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidad;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimiento;
    /**
     * Identificador de depósito logístico
     */
    private Integer idDepositoLogistico;
    /**
     * Codigo de Estado de Inventario
     */
    private String codigoEstadoInventario;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public Integer getId() {
        return id;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public BigDecimal getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(BigDecimal cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public BigDecimal getCantidadConfirmada() {
        return cantidadConfirmada;
    }

    public void setCantidadConfirmada(BigDecimal cantidadConfirmada) {
        this.cantidadConfirmada = cantidadConfirmada;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    //</editor-fold>
    public OrdenCompraDetalles() {

    }

}
