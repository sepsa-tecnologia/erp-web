package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemision;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro utilizado para el servicio de Nota Crédito
 *
 * @author Romina Núñez,Sergio D. Riveros Vazquez
 */
public class NotaRemisionFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaRemisionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public NotaRemisionFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }
    
    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public NotaRemisionFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public NotaRemisionFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nr
     *
     * @param nroNotaRemision
     * @return
     */
    public NotaRemisionFilter nroNotaRemision(String nroNotaRemision) {
        if (nroNotaRemision != null && !nroNotaRemision.trim().isEmpty()) {
            params.put("nroNotaRemision", nroNotaRemision);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaRemisionFilter anulado(Character anulado) {
        if (anulado != null && !anulado.toString().trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaRemisionFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo emision interno
     *
     * @param idMotivoEmisionNr
     * @return
     */
    public NotaRemisionFilter idMotivoEmisionNr(Integer idMotivoEmisionNr) {
        if (idMotivoEmisionNr != null) {
            params.put("idMotivoEmisionNr", idMotivoEmisionNr);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de cdc
     *
     * @param cdc
     * @return
     */
    public NotaRemisionFilter cdc(String cdc) {
        if (cdc != null && !cdc.trim().isEmpty()) {
            params.put("cdc", cdc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public NotaRemisionFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    /**public NotaRemisionFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }**/
    
    
    
        /**
     * Agrega el filtro de listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaRemisionFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param notaRemision datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaRemision notaRemision, Integer page, Integer pageSize) {
        NotaRemisionFilter filter = new NotaRemisionFilter();

        filter
                .id(notaRemision.getId())
                .razonSocial(notaRemision.getRazonSocial())
                .fecha(notaRemision.getFecha())
                .fechaDesde(notaRemision.getFechaDesde())
                .fechaHasta(notaRemision.getFechaHasta())
                .nroNotaRemision(notaRemision.getNroNotaRemision())
                .anulado(notaRemision.getAnulado())
                .idMotivoEmisionNr(notaRemision.getIdMotivoEmisionNr())
                .listadoPojo(notaRemision.getListadoPojo())
                .cdc(notaRemision.getCdc())
                //.codigoEstado(notaRemision.getCodigoEstado())
                .ruc(notaRemision.getRuc())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoFilter
     */
    public NotaRemisionFilter() {
        super();
    }
}
