/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.remote.CanalVentaService;

/**
 * Controlador para crear canalVenta
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("canalVentaCreate")
public class CanalVentaCreateController implements Serializable {

    /**
     * Cliente para el servicio de canal Venta.
     */
    private final CanalVentaService service;
    /**
     * POJO de canal venta
     */
    private CanalVenta canalVenta;

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {
        BodyResponse<CanalVenta> respuestaTal = service.setCanalVenta(this.canalVenta);
        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Canal de Venta creado correctamente!"));
            init();
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.canalVenta = new CanalVenta();
    }

    /**
     * Constructor
     */
    public CanalVentaCreateController() {
        init();
        this.service = new CanalVentaService();
    }
}
