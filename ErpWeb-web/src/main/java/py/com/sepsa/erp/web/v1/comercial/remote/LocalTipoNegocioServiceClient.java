
package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import py.com.sepsa.erp.web.v1.comercial.pojos.LocalTipoNegocio;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 *
 * @author SONY
 */
public class LocalTipoNegocioServiceClient extends APIErpCore{
    
     /**
     * Método para setear local tipo negocio
     * @param localtipo
     * @return 
     */
    public LocalTipoNegocio setLocalTipoNegocio(LocalTipoNegocio localtipo){
     
        LocalTipoNegocio ltn=new LocalTipoNegocio();
        
         HttpURLConnection conn = POST(Resource.LOCAL_TIPO_NEGOCIO.url,
                ContentType.JSON);
    
        if (conn != null) {
        
            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(localtipo));
     
            BodyResponse response = BodyResponse.createInstance(conn,
                    LocalTipoNegocio.class);
            
            ltn = (LocalTipoNegocio) response.getPayload();
      
            conn.disconnect();
        }
        return ltn;
    }
    
      /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicio
        LOCAL_TIPO_NEGOCIO("Local tipo de negocio","local-tipo-negocio");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
