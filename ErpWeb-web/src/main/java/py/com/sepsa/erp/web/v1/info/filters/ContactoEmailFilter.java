package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;

/**
 * Filtro utilizado para el servicio de contacto email
 *
 * @author Romina Núñez
 * @author Sergio D. Riveros Sergio
 */
public class ContactoEmailFilter extends Filter {

    /**
     * Agrega el filtro de identificador del Contacto
     *
     * @param idContacto Identificador del Contacto
     * @return
     */
    public ContactoEmailFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de contacto
     *
     * @param contacto
     * @return
     */
    public ContactoEmailFilter contacto(String contacto) {
        if (contacto != null && !contacto.trim().isEmpty()) {
            params.put("contacto", contacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del idTipoContacto
     *
     * @param idTipoContacto Identificador del tipo Contacto
     * @return
     */
    public ContactoEmailFilter idTipoContacto(Integer idTipoContacto) {
        if (idTipoContacto != null) {
            params.put("idTipoContacto", idTipoContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de tipoContacto
     *
     * @param tipoContacto
     * @return
     */
    public ContactoEmailFilter tipoContacto(String tipoContacto) {
        if (tipoContacto != null && !tipoContacto.trim().isEmpty()) {
            params.put("tipoContacto", tipoContacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de persona
     *
     * @param idPersona Identificador del Persona
     * @return
     */
    public ContactoEmailFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de persona
     *
     * @param persona
     * @return
     */
    public ContactoEmailFilter persona(String persona) {
        if (persona != null && !persona.trim().isEmpty()) {
            params.put("persona", persona.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del email
     *
     * @param idEmail
     * @return
     */
    public ContactoEmailFilter idEmail(Integer idEmail) {
        if (idEmail != null) {
            params.put("idEmail", idEmail);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de email
     *
     * @param email
     * @return
     */
    public ContactoEmailFilter email(String email) {
        if (email != null && !email.trim().isEmpty()) {
            params.put("email", email.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de estado
     *
     * @param estado
     * @return
     */
    public ContactoEmailFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de cargo
     *
     * @param cargo
     * @return
     */
    public ContactoEmailFilter cargo(String cargo) {
        if (cargo != null && !cargo.trim().isEmpty()) {
            params.put("cargo", cargo.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de idCargo
     *
     * @param idCargo
     * @return
     */
    public ContactoEmailFilter idCargo(Integer idCargo) {
        if (idCargo != null) {
            params.put("idCargo", idCargo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contactoEmail
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ContactoEmail contactoEmail, Integer page, Integer pageSize) {
        ContactoEmailFilter filter = new ContactoEmailFilter();
        
        filter
                .idContacto(contactoEmail.getIdContacto())
                .contacto(contactoEmail.getContacto())
                .idTipoContacto(contactoEmail.getIdTipoContacto())
                .tipoContacto(contactoEmail.getTipoContacto())
                .idPersona(contactoEmail.getIdPersona())
                .persona(contactoEmail.getPersona())
                .idEmail(contactoEmail.getIdEmail())
                .estado(contactoEmail.getEstado())
                .cargo(contactoEmail.getCargo())
                .idCargo(contactoEmail.getIdCargo())
                .email(contactoEmail.getEmail())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de ContactoEmailFilter
     */
    public ContactoEmailFilter() {
        super();
    }
}
