package py.com.sepsa.erp.web.v1.servicio.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicios;
import py.com.sepsa.erp.web.v1.comercial.remote.ServicioService;

@ViewScoped
@Named("crearServicio")
public class ServicioCrearController implements Serializable {

    /**
     * Cliente para el servicio de servicios
     */
    private final ServicioService service;
    
    /**
     * Datos del servicio
     */
    private Servicios servicios;

    /**
     * Getter y Setters.
     */
    public Servicios getServicios() {
        return servicios;
    }
    
    public void setServicios(Servicios servicios) {    
        this.servicios = servicios;
    }

    /**
     * Método para crear un servicio
     */
    public void create() {
        servicios = service.create(servicios);
        if (servicios != null && servicios.getId() != null) {
            init();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Servicio creado con éxito!", "Servicio creado con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear el servicio!", "Error al crear el servicio!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.servicios = new Servicios();
    }

    /**
     * Método para reiniciar el formulario
    
    public void clear() {
        init();
    }*/

    /**
     * Constructor de ServiciosControllerCrear
     */
    public ServicioCrearController() {
        init();
        this.service = new ServicioService();
    }

}
