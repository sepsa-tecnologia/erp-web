/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoDetalleListPojoAdapter extends DataListAdapter<NotaDebitoDetallePojo> {
  
     /**
     * Cliente para el servicio de cobro
     */
    private final NotaDebitoService serviceNotaDebito;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaDebitoDetalleListPojoAdapter fillData(NotaDebitoDetallePojo searchData) {
     
        return serviceNotaDebito.getNotaDebitoDetalleListPojo(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaDebitoDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaDebitoDetalleListPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaDebito = new NotaDebitoService();
    }

    /**
     * Constructor de NotaCreditoDetalleListAdapter
     */
    public NotaDebitoDetalleListPojoAdapter() {
        super();
        this.serviceNotaDebito = new NotaDebitoService();
    }
}
