/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivoAnulacion;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para tipo motivo anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoMotivoAnulacionFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoMotivoAnulacionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoMotivoAnulacionFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agregar el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoMotivoAnulacionFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agregar el filtro de activo
     *
     * @param activo
     * @return
     */
    public TipoMotivoAnulacionFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoMotivoAnulacion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoMotivoAnulacion tipoMotivoAnulacion, Integer page, Integer pageSize) {
        TipoMotivoAnulacionFilter filter = new TipoMotivoAnulacionFilter();

        filter
                .id(tipoMotivoAnulacion.getId())
                .descripcion(tipoMotivoAnulacion.getDescripcion())
                .codigo(tipoMotivoAnulacion.getCodigo())
                .activo(tipoMotivoAnulacion.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoMotivoAnulacionFilter() {
        super();
    }

}
