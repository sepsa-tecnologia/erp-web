/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.TipoArchivoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.TipoArchivoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoArchivo;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para Tipo Archivo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoArchivoService extends APIErpCore {

    /**
     * Obtiene la lista de objetos
     *
     * @param tipoArchivo Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoArchivoAdapter getTipoArchivoList(TipoArchivo tipoArchivo, Integer page,
            Integer pageSize) {

        TipoArchivoAdapter lista = new TipoArchivoAdapter();

        Map params = TipoArchivoFilter.build(tipoArchivo, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_ARCHIVO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoArchivoAdapter.class);

            lista = (TipoArchivoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de clase
     */
    public TipoArchivoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_ARCHIVO("Tipo Archivo", "tipo-archivo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
