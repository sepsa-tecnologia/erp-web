/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ConfigurationValue;

/**
 * Filter para configurationValue
 *
 * @author Romina Núñez
 */
public class ConfigurationValueFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public ConfigurationValueFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de value para configurationFilter
     *
     * @param value
     * @return
     */
    public ConfigurationValueFilter value(String value) {
        if (value != null) {
            params.put("value", value);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipoNotificacion de configurationValue
     *
     * @param idTipoNotificacion
     * @return
     */
    public ConfigurationValueFilter idTipoNotificacion(Integer idTipoNotificacion) {
        if (idTipoNotificacion != null) {
            params.put("notificationTypeId", idTipoNotificacion);
        }
        return this;
    }

    /**
     * Agrega el filtro de idConfiguration de configurationValue
     *
     * @param idConfiguration
     * @return
     */
    public ConfigurationValueFilter idConfiguration(Integer idConfiguration) {
        if (idConfiguration != null) {
            params.put("configurationId", idConfiguration);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param configurationValue datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ConfigurationValue configurationValue, Integer page, Integer pageSize) {
        ConfigurationValueFilter filter = new ConfigurationValueFilter();

        filter
                .id(configurationValue.getId())
                .idTipoNotificacion(configurationValue.getNotificationType() != null ? configurationValue.getNotificationType().getId() : null)
                .idConfiguration(configurationValue.getConfiguration() != null ? configurationValue.getConfiguration().getId() : null)
                .value(configurationValue.getValue())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ConfigurationValueFilter
     */
    public ConfigurationValueFilter() {
        super();
    }
}
