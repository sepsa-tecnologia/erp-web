package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Inventario;
import py.com.sepsa.erp.web.v1.inventario.remote.InventarioService;

/**
 * Adaptador para la lista de inventario
 *
 * @author Alexander Triana
 */
public class InventarioAdapter extends DataListAdapter<Inventario> {

    /**
     * Cliente remoto para inventario
     */
    private final InventarioService inventario;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return InventarioAdapter
     */
    @Override
    public InventarioAdapter fillData(Inventario searchData) {

        return inventario.getInventario(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de InventarioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public InventarioAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.inventario = new InventarioService();
    }

    /**
     * Constructor de InventarioAdapter
     */
    public InventarioAdapter() {
        super();
        this.inventario = new InventarioService();
    }

}
