package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de contacto teléfono
 *
 * @author Cristina Insfrán
 * @author Sergio D. Riveros Vazquez
 */
public class ContactoTelefono {

    /**
     * Identificador del contacto
     */
    private Integer idContacto;
    /**
     * Contacto
     */
    private String contacto;
    /**
     * identificador del telefono
     */
    private Integer idTelefono;
    /**
     * Notificacion del documento
     */
    private String notificacionDocumento;
    /**
     * Prefijo
     */
    private String prefijo;
    /**
     * Número
     */
    private String numero;
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    /**
     * Persona
     */
    private String persona;
    /**
     * Identificador de contacto
     */
    private Integer idTipoContacto;
    /**
     * Tipo de contacto
     */
    private String tipoContacto;
    /**
     * Identificador de cargo
     */
    private Integer idCargo;
    /**
     * Cargo
     */
    private String cargo;
    /**
     * Estado
     */
    private String estado;

    /**
     * @return the idContacto
     */
    public Integer getIdContacto() {
        return idContacto;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public String getContacto() {
        return contacto;
    }

    /**
     * @param idContacto the idContacto to set
     */
    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    /**
     * @return the idTelefono
     */
    public Integer getIdTelefono() {
        return idTelefono;
    }

    /**
     * @param idTelefono the idTelefono to set
     */
    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    /**
     * @return the notificacionDocumento
     */
    public String getNotificacionDocumento() {
        return notificacionDocumento;
    }

    /**
     * @param notificacionDocumento the notificacionDocumento to set
     */
    public void setNotificacionDocumento(String notificacionDocumento) {
        this.notificacionDocumento = notificacionDocumento;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Constructor
     */
    public ContactoTelefono() {

    }

    /**
     * Constructor con parametros
     *
     * @param idContacto
     * @param idTelefono
     */
    public ContactoTelefono(Integer idContacto, Integer idTelefono) {
        this.idContacto = idContacto;
        this.idTelefono = idTelefono;
    }

    /**
     * Constructor con parámetros
     *
     * @param idContacto
     * @param contacto
     * @param idTelefono
     * @param prefijo
     * @param numero
     */
    public ContactoTelefono(Integer idContacto, String contacto, Integer idTelefono, String prefijo, String numero) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idTelefono = idTelefono;
        this.prefijo = prefijo;
        this.numero = numero;
    }

}
