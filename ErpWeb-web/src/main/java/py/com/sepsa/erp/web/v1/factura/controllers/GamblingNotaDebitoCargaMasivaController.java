package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Archivo;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.remote.LoteService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("gamblingMasivaND")
public class GamblingNotaDebitoCargaMasivaController implements Serializable {
    /**
     * Archivo
     */
    private UploadedFile file;
    private UploadedFile fileClient;
    private UploadedFile fileFacturas;
    private UploadedFile fileFacturasExternas;
    
    private String email;
    private LoteService serviceLote;
    
    @Inject
    private SessionData session;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFileClient() {
        return fileClient;
    }

    public void setFileClient(UploadedFile fileClient) {
        this.fileClient = fileClient;
    }

    public UploadedFile getFileFacturas() {
        return fileFacturas;
    }

    public void setFileFacturas(UploadedFile fileFacturas) {
        this.fileFacturas = fileFacturas;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LoteService getServiceLote() {
        return serviceLote;
    }

    public void setServiceLote(LoteService serviceLote) {
        this.serviceLote = serviceLote;
    }

    public UploadedFile getFileFacturasExternas() {
        return fileFacturasExternas;
    }

    public void setFileFacturasExternas(UploadedFile fileFacturasExternas) {
        this.fileFacturasExternas = fileFacturasExternas;
    }
    
    
    
    
    /**
     * Método para subir y enviar el archivo
     *
     * @throws IOException
     */
    public void upload() throws IOException {
        if (validarArchivos()) {
            Archivo archivoCreate = new Archivo();
            archivoCreate.setNombreArchivo(file.getFileName());
            String contentBase64 = Base64.getEncoder().encodeToString(file.getContents());
            archivoCreate.setContenido(contentBase64);
            archivoCreate.setCodigo("NOTA_DEBITO");

            Archivo archivoCreate2 = new Archivo();
            archivoCreate2.setNombreArchivo(fileClient.getFileName());
            String contentBase64__ = Base64.getEncoder().encodeToString(fileClient.getContents());
            archivoCreate2.setContenido(contentBase64__);
            archivoCreate2.setCodigo("CLIENTES");

            Archivo archivoCreate3 = new Archivo();
            archivoCreate3.setNombreArchivo(fileFacturas.getFileName());
            String contentBase64_ = Base64.getEncoder().encodeToString(fileFacturas.getContents());
            archivoCreate3.setContenido(contentBase64_);
            archivoCreate3.setCodigo("FACTURAS");
            
            
            Archivo archivoCreate4 = new Archivo();
            archivoCreate4.setNombreArchivo(fileFacturasExternas.getFileName());
            String contentBase64___ = Base64.getEncoder().encodeToString(fileFacturasExternas.getContents());
            archivoCreate4.setContenido(contentBase64___);
            archivoCreate4.setCodigo("FACTURAS_EXTERNAS");
            
            ArrayList<Archivo> archivos = new ArrayList<>(Arrays.asList(archivoCreate, archivoCreate2, archivoCreate3,archivoCreate4));
            
            Lote loteCreate = new Lote();
            loteCreate.setIdEmpresa(session.getUser().getIdEmpresa());
            loteCreate.setFechaInsercion(Date.from(Instant.now()));
            loteCreate.setNombreArchivo(file.getFileName());
            loteCreate.setEmail(email);
            loteCreate.setIdUsuario(session.getUser().getId());
            loteCreate.setCodigoTipoLote("CARGA_MASIVA_NOTAS_DEBITO_GAMBLING");
            loteCreate.setArchivos(archivos);
            BodyResponse<Lote> respuesta = serviceLote.createLote(loteCreate);
            if (respuesta.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se han enviado los archivos correctamente!"));
            }

        }
    }
    
    public boolean validarArchivos(){
        boolean valid = true;
        
        if (file == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de notas de débito con el formato correcto: (xlsx)"));
        }
        if (fileClient == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de los clientes con el formato correcto: (xlsx)"));
        }
        if (fileFacturas == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de facturas con el formato correcto: (xlsx)"));
        }
        if (email == null || email.trim().isEmpty()) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el correo para enviar los resultados"));
        }else{
            if(!validarFormatoCorreo(email)){
                valid = false;
            }
        }
        
        return valid;
    }
    
    public boolean validarFormatoCorreo(String email) {
        boolean addEmail = false;
         
        email = email.trim().replace(" ", "");
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z]{2,})?$");

        // El email a validar
        String correo = email;

        Matcher matcher = pattern.matcher(correo);

        if (matcher.find() == true) {
            addEmail = true;

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            String msg = String.format("%s no es un email válido", email);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        return addEmail;
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.file = null;
        this.fileClient = null;
        this.fileFacturas = null;
        this.serviceLote = new LoteService();
        this.email = null;
    }
}
