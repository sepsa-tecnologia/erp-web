/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.remote.MarcaService;

/**
 * Converter para Marca
 *
 * @author Sergio D. Riveros Vazquez
 */
@FacesConverter("marcaPojoConverter")
public class MarcaPojoConverter implements Converter {

    /**
     * Servicios para marca
     */
    private MarcaService serviceClient;

    @Override
    public Marca getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new MarcaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch (Exception ex) {
            }

            Marca cli = new Marca();
            cli.setId(val);

            List<Marca> marcas = serviceClient.getMarcaList(cli, 0, 10).getData();
            if (marcas != null && !marcas.isEmpty()) {
                return marcas.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No es una marca válida"));
            }
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof Marca) {
                return String.valueOf(((Marca) object).getId());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
