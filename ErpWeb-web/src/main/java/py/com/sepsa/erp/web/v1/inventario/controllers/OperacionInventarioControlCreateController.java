/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.pojos.Operacion;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.remote.OperacionService;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("crearOperacionInventarioControl")
public class OperacionInventarioControlCreateController implements Serializable {

    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioAdapter adapterControlInventario;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioFilter;
    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioDetalleAdapter adapterControlInventarioDetalle;
    /**
     * Pojo Control Inventario
     */
    private ControlInventarioDetalle controlInventarioDetalleFilter;
    /**
     * Dato bandera
     */
    private Boolean show;
    /**
     * Dato bandera
     */
    private Boolean showFilter;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioEdit;
    /**
     * Lista Detalle
     */
    private List<ControlInventarioDetalle> listaDetalle;
    /**
     * POJO de operacion
     */
    private Operacion operacion;
    /**
     * Objeto con lista de detalles de operaciones
     */
    private List<OperacionInventarioDetalle> operacionInventarioDetalleList;

    /**
     * Cliente para el servicio de operacion
     */
    private final OperacionService service;
    /**
     * Adapter para motivo de operacion
     */
    private MotivoAdapter adapterMotivo;
    /**
     * POJO de motivo
     */
    private Motivo motivo;
    /**
     * Adapter para Estado
     */
    private EstadoAdapter adapterEstado;
    /**
     * Pojo para estado
     */
    private Estado estado;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setControlInventarioDetalleFilter(ControlInventarioDetalle controlInventarioDetalleFilter) {
        this.controlInventarioDetalleFilter = controlInventarioDetalleFilter;
    }

    public MotivoAdapter getAdapterMotivo() {
        return adapterMotivo;
    }

    public void setAdapterMotivo(MotivoAdapter adapterMotivo) {
        this.adapterMotivo = adapterMotivo;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public EstadoAdapter getAdapterEstado() {
        return adapterEstado;
    }

    public void setAdapterEstado(EstadoAdapter adapterEstado) {
        this.adapterEstado = adapterEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void setOperacionInventarioDetalleList(List<OperacionInventarioDetalle> operacionInventarioDetalleList) {
        this.operacionInventarioDetalleList = operacionInventarioDetalleList;
    }

    public List<OperacionInventarioDetalle> getOperacionInventarioDetalleList() {
        return operacionInventarioDetalleList;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setListaDetalle(List<ControlInventarioDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<ControlInventarioDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setControlInventarioEdit(ControlInventario controlInventarioEdit) {
        this.controlInventarioEdit = controlInventarioEdit;
    }

    public ControlInventario getControlInventarioEdit() {
        return controlInventarioEdit;
    }

    public void setShowFilter(Boolean showFilter) {
        this.showFilter = showFilter;
    }

    public Boolean getShowFilter() {
        return showFilter;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Boolean getShow() {
        return show;
    }

    public void setAdapterControlInventarioDetalle(ControlInventarioDetalleAdapter adapterControlInventarioDetalle) {
        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle;
    }

    public ControlInventarioDetalle getControlInventarioDetalleFilter() {
        return controlInventarioDetalleFilter;
    }

    public ControlInventarioDetalleAdapter getAdapterControlInventarioDetalle() {
        return adapterControlInventarioDetalle;
    }

    public void setControlInventarioFilter(ControlInventario controlInventarioFilter) {
        this.controlInventarioFilter = controlInventarioFilter;
    }

    public void setAdapterControlInventario(ControlInventarioAdapter adapterControlInventario) {
        this.adapterControlInventario = adapterControlInventario;
    }

    public ControlInventario getControlInventarioFilter() {
        return controlInventarioFilter;
    }

    public ControlInventarioAdapter getAdapterControlInventario() {
        return adapterControlInventario;
    }

    //</editor-fold>
    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.controlInventarioFilter = new ControlInventario();
        filterControlInventario();
    }

    public void filterControlInventario() {
        getAdapterControlInventario().setFirstResult(0);
        this.controlInventarioFilter.setListadoPojo(true);
        if (this.controlInventarioFilter.getCodigo() != null && !this.controlInventarioFilter.getCodigo().trim().isEmpty()) {
            this.adapterControlInventario = adapterControlInventario.fillData(controlInventarioFilter);
            if (!this.adapterControlInventario.getData().isEmpty()) {
                this.show = true;
                this.showFilter = false;
                this.controlInventarioEdit = this.adapterControlInventario.getData().get(0);
                this.operacion.setCodigoMotivo(controlInventarioEdit.getCodigoMotivo());
                this.operacion.setMotivo(controlInventarioEdit.getMotivo());
                this.operacion.setCodigoEstado(controlInventarioEdit.getCodigoEstado());
                this.operacion.setEstado(controlInventarioEdit.getEstado());
                this.operacion.setIdControlInventario(controlInventarioEdit.getId());
                filterControlInventarioDetalle(controlInventarioEdit.getId());
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Advertencia", "No hay registros relacionados al N° ingresado!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Advertencia", "Debe ingresar el N° de Control!"));
        }

    }

    /**
     * Metodo para crear Operacion
     */
    public void create() {
        operacion.setCodigoTipoOperacion("AJUSTE_CONTROL_INVENTARIO");
        operacion.setCodigoEstado("CONFIRMADO");
        operacion.setCodigoTipoMotivo("OPERACION_INVENTARIO_CONTROL");
        for (OperacionInventarioDetalle operacionInventarioDetalle : operacionInventarioDetalleList) {
            operacionInventarioDetalle.setCantidad(operacionInventarioDetalle.getCantidadFinal());
        }

        operacion.setOperacionInventarioDetalles(operacionInventarioDetalleList);
        BodyResponse<Operacion> respuesta = service.setOperacion(operacion);
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Ajuste de Inventario Por Control generado correctamente!"));
            init();
        }
    }

    /**
     * Metodo para eliminar un detalle de la lista de detalles.
     *
     * @param opD
     */
    public void deleteDetalle(OperacionInventarioDetalle opD) {
        this.operacionInventarioDetalleList.remove(opD);
    }

    /**
     * Método para filtrar los detalles de control de inventario
     *
     * @param idControl
     */
    public void filterControlInventarioDetalle(Integer idControl) {
        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();

        this.controlInventarioDetalleFilter.setListadoPojo(Boolean.TRUE);
        this.controlInventarioDetalleFilter.setIdControlInventario(idControl);

        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle.fillData(controlInventarioDetalleFilter);
        this.listaDetalle = this.adapterControlInventarioDetalle.getData();

        for (ControlInventarioDetalle controlInventarioDetalle : listaDetalle) {
            OperacionInventarioDetalle oid = new OperacionInventarioDetalle();
            oid.setIdProducto(controlInventarioDetalle.getIdProducto());
            oid.setProducto(controlInventarioDetalle.getProducto());
            oid.setEstadoInventarioDestino("Disponible");
            oid.setCodigoEstadoInventarioDestino("DISPONIBLE");
            oid.setFechaVencimiento(controlInventarioDetalle.getFechaVencimiento());
            oid.setCantidadControl(controlInventarioDetalle.getCantidadFinal());
            oid.setCantidadFinal(controlInventarioDetalle.getCantidadFinal());
            oid.setIdDepositoDestino(controlInventarioDetalle.getIdDepositoLogistico());
            oid.setCodigoDeposito(controlInventarioDetalle.getCodigoDeposito());
            oid.setIdControlInventarioDetalle(controlInventarioDetalle.getId());

            //Se busca si existen datos asociados al producto en el detalle de inventario
            InventarioDetalleAdapter inventarioDetalleAdapter = new InventarioDetalleAdapter();
            InventarioDetalle inventarioDetalle = new InventarioDetalle();
            inventarioDetalle.setListadoPojo(true);
            inventarioDetalle.setIdProducto(oid.getIdProducto());
            inventarioDetalle.setCodigoEstado("DISPONIBLE");
            inventarioDetalle.setFechaVencimiento(controlInventarioDetalle.getFechaVencimiento());
            inventarioDetalleAdapter = inventarioDetalleAdapter.fillData(inventarioDetalle);

            if (inventarioDetalleAdapter.getData().size() == 0) {
                oid.setCantidadFinal(0);
            } else {
                oid.setCantidadInicial(inventarioDetalleAdapter.getData().get(0).getCantidad());
            }

            operacionInventarioDetalleList.add(oid);
        }
    }

    /**
     * Método para obtener el listado de motivos para operacion
     */
    public void filterMotivoOperacion() {
        this.motivo.setCodigoTipoMotivo("OPERACION_INVENTARIO_CONTROL");
        adapterMotivo = adapterMotivo.fillData(motivo);
    }

    /**
     * Método para obtener el listado de estados
     */
    public void getEstadoList() {
        estado.setCodigoTipoEstado("OPERACION_INVENTARIO");
        adapterEstado = adapterEstado.fillData(estado);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.operacion = new Operacion();
        this.operacionInventarioDetalleList = new ArrayList<>();

        this.adapterControlInventario = new ControlInventarioAdapter();
        this.controlInventarioFilter = new ControlInventario();

        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();

        this.show = false;
        this.showFilter = true;

        this.controlInventarioEdit = new ControlInventario();
        this.listaDetalle = new ArrayList<>();

        this.motivo = new Motivo();
        this.adapterMotivo = new MotivoAdapter();
        this.estado = new Estado();
        this.adapterEstado = new EstadoAdapter();

        this.filterMotivoOperacion();
        this.getEstadoList();
    }

    /**
     * Constructor
     */
    public OperacionInventarioControlCreateController() {
        init();
        this.service = new OperacionService();
    }

}
