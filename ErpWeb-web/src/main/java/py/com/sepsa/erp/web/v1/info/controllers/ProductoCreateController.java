/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MarcaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MetricaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProducto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para crear producto
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("productoCreate")

public class ProductoCreateController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final ProductoService service;
    /**
     * POJO del talonario
     */
    private Producto producto;
    /**
     * Adaptador para la lista de Marca para autocomplete
     */
    private MarcaAdapter marcaAdapterListAux;
    /**
     * POJO de Marca para autocomplete
     */
    private Marca marcaFilterAux;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfiguracionValor;
    /**
     * Pojo conf valor
     */
    private ConfiguracionValor confValor;
    /**
     * Adaptador para lista de metricas
     */
    private MetricaAdapter metricaAdapter;
    /**
     * POJO de metrica
     */
    private Metrica metrica;
    /**
     * POJO tipoProducto
     */
    private TipoProducto tipoProducto;
    /**
     * Adapter de TipoProducto
     */
    private TipoProductoAdapter tipoProductoAdapter;
    /**
     * Listado de productos relacionados
     */
    private List<Producto> productosRelacionados;
    /**
     * Addapter de producto
     */
    private ProductoAdapter adapterProducto;
    /**
     * 
     */
    private Producto selectedProducto;
    
    private BigDecimal porcentajeRelacionado;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }
    
    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public Producto getSelectedProducto() {
        return selectedProducto;
    }

    public void setSelectedProducto(Producto selectedProducto) {
        this.selectedProducto = selectedProducto;
    }

    public List<Producto> getProductosRelacionados() {
        return productosRelacionados;
    }

    public void setProductosRelacionados(List<Producto> productosRelacionados) {
        this.productosRelacionados = productosRelacionados;
    }
    
    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public TipoProductoAdapter getTipoProductoAdapter() {
        return tipoProductoAdapter;
    }

    public void setTipoProductoAdapter(TipoProductoAdapter tipoProductoAdapter) {
        this.tipoProductoAdapter = tipoProductoAdapter;
    }
    
    public Producto getProducto() {
        return producto;
    }

    public void setConfValor(ConfiguracionValor confValor) {
        this.confValor = confValor;
    }

    public ConfiguracionValor getConfValor() {
        return confValor;
    }

    public void setAdapterConfiguracionValor(ConfiguracionValorListAdapter adapterConfiguracionValor) {
        this.adapterConfiguracionValor = adapterConfiguracionValor;
    }

    public ConfiguracionValorListAdapter getAdapterConfiguracionValor() {
        return adapterConfiguracionValor;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public MarcaAdapter getMarcaAdapterListAux() {
        return marcaAdapterListAux;
    }

    public void setMarcaAdapterListAux(MarcaAdapter marcaAdapterListAux) {
        this.marcaAdapterListAux = marcaAdapterListAux;
    }

    public Marca getMarcaFilterAux() {
        return marcaFilterAux;
    }

    public void setMarcaFilterAux(Marca marcaFilterAux) {
        this.marcaFilterAux = marcaFilterAux;
    }

    public MetricaAdapter getMetricaAdapter() {
        return metricaAdapter;
    }

    public void setMetricaAdapter(MetricaAdapter metricaAdapter) {
        this.metricaAdapter = metricaAdapter;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }
    
    //</editor-fold>
    /**
     * Método para crear Producto
     */
    public void create() {
        boolean create = false;
        if (producto.getCodigoGtin() != null) {
            if ((producto.getCodigoGtin().length() == 1 && producto.getCodigoGtin().equalsIgnoreCase("0")) ||  producto.getCodigoGtin().length() == 8 || producto.getCodigoGtin().length() == 12 || producto.getCodigoGtin().length() == 13 || producto.getCodigoGtin().length() == 14) {
                create = true;
            } else {
                if(producto.getCodigoInterno() == null || producto.getCodigoInterno().isEmpty()){
                    create = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El Código GTIN debe contar con 8, 12, 13 o 14 dígitos! En caso de no tener código, ingresar 0, o ingresar el código interno."));
                } else {
                    producto.setCodigoGtin("0");
                    create = true;
                }
            }
        }
        if (producto.getIdMetrica() == null){
             create = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar una Unidad de Medida"));
        }
        
        if (producto.getNroLote() != null && !producto.getNroLote().trim().isEmpty()){
            String formateado = producto.getNroLote().trim().toUpperCase().replace(" ", "");
            if (!formateado.isEmpty()) {
                producto.setNroLote(formateado);
            }
            
        }else {
                producto.setNroLote(null);
            }

        if (producto.getIdTipoProducto() == 2) {
            try {       
                if (porcentajeRelacionado == null  || (!(porcentajeRelacionado.compareTo(BigDecimal.ZERO) == 1 && porcentajeRelacionado.compareTo(new BigDecimal("100")) == -1)) ) {
                    create = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje relacionado debe ser entre 0 y 100!"));
                }  
            } catch (NumberFormatException e) {
                create = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se debe ingresar un porcentaje relacionado válido"));
            }
            
            if (productosRelacionados.isEmpty()){
                create = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se debe relacionar al menos un producto para poder crear el nuevo producto"));
            } else {
                for (Producto p :productosRelacionados) {
                    p.setPorcentajeRelacionado(porcentajeRelacionado);
                }
                producto.setProductosRelacionados(productosRelacionados);
            }
        }
        
        if (create == true) {
            BodyResponse<Producto> respuestaProducto = service.setProducto(this.producto);
            if (respuestaProducto.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Producto creado correctamente!"));
                init();
            }
        }

    }

    /**
     * Método para obtener el listado de configuración valor para impuesto de
     * producto
     */
    public void filterConfiguracionValor() {
        ConfiguracionListAdapter adapterCong = new ConfiguracionListAdapter();
        Configuracion config = new Configuracion();
        config.setCodigo("PORCENTAJE_IMPUESTO_PRODUCTO");
        adapterCong = adapterCong.fillData(config);

        confValor.setIdConfiguracion(adapterCong.getData().get(0).getId());
        adapterConfiguracionValor = adapterConfiguracionValor.fillData(confValor);
    }

    /* Método para obtener las marcas filtrados
     *
     * @param query
     * @return
     */
    public List<Marca> completeQuery(String query) {
        Marca marcaFilterAux = new Marca();
        marcaFilterAux.setDescripcion(query);
        marcaAdapterListAux = marcaAdapterListAux.fillData(marcaFilterAux);

        return marcaAdapterListAux.getData();
    }
    
    /* Método para obtener las marcas filtrados
     *
     * @param query
     * @return
     */
    public List<Metrica> completeQueryMetrica(String query) {
        Metrica metricaFilter = new Metrica();
        metricaFilter.setCodigo(query);
        metricaAdapter.setFirstResult(0);
        metricaAdapter.setPageSize(10000);
        metricaAdapter = metricaAdapter.fillData(metricaFilter);

        return metricaAdapter.getData();
    }

    /**
     * Selecciona marca
     *
     * @param event
     */
    public void onItemSelectMarcaFilter(SelectEvent event) {
        marcaFilterAux.setId(((Marca) event.getObject()).getId());
    }
    
    /**
     * Selecciona metrica
     *
     * @param event
     */
    public void onItemSelectMetricaFilter(SelectEvent event) {
        producto.setIdMetrica(((Metrica) event.getObject()).getId());
    }
    
        /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        selectedProducto = new Producto();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto.setPageSize(100);
        adapterProducto = adapterProducto.fillData(producto);
        producto = new Producto();

        return adapterProducto.getData();
    }
    
    /**
     * Método para agregar el producto al listado de productos relacionados
     */
    public void agregarProducto() {
        boolean addProd = true;
        for (Producto p : productosRelacionados){
            if (p.getId().equals(selectedProducto.getId())){
                addProd = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Atención", "Este producto ya se encuentra en la lista de relacionados!"));
                break;
            }
        }
        if (addProd){
            this.productosRelacionados.add(selectedProducto); 
            this.selectedProducto = new Producto();
        }
       
    }
    
     /**
     * Método para eliminar el productos del listado
     *
     * @param id
     */
    public void deleteProducto(Integer id) {
        Producto p = null;
        for (Producto prod : productosRelacionados) {
            if (Objects.equals(prod.getId(), id)) {
                p = prod;
                productosRelacionados.remove(p);
                break;
            }
        }
    }
    
    /**
     * Inicializa los datos
     */
    private void init() {
        try {
            this.producto = new Producto();
            this.marcaAdapterListAux = new MarcaAdapter();
            this.marcaFilterAux = new Marca();
            this.metricaAdapter = new MetricaAdapter();
            this.metrica = new Metrica();
            this.adapterConfiguracionValor = new ConfiguracionValorListAdapter();
            this.confValor = new ConfiguracionValor();
            this.tipoProducto = new TipoProducto();
            this.tipoProductoAdapter = new TipoProductoAdapter();
            tipoProductoAdapter = tipoProductoAdapter.fillData(tipoProducto);
            this.adapterProducto = new ProductoAdapter();
            this.selectedProducto = new Producto();
            this.productosRelacionados = new ArrayList();
            filterConfiguracionValor();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Constructor
     */
    public ProductoCreateController() {
        init();
        this.service = new ProductoService();
    }

}
