package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;

/**
 * POJO para Datos de Nota de Crédito
 * @author Romina Núñez
 */
public class DatosNotaRemision {

    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    /**
     * N° Nota Credito
     */
    private String nroNotaRemision;
    /**
     * Timbrado
     */
    private Integer timbrado;
    /**
     * Fecha Vto Timbrado
     */
    private Date fechaVencimientoTimbrado;
        /**
     * telefono
     */
    private String telefono;
    /**
     * direccion
     */
    private String direccion;
    /**
     * email
     */
    private String email;
    /**
     * Nro de casa
     */
    private String nroCasa;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Identificador de departamento 
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Fecha 
     */
    private Date fecha;

    public Integer getIdTalonario() {
        return idTalonario;
    }
    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getTimbrado() {
        return timbrado;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }
    
    
    /**
     * Constructor de la clase
     */
    public DatosNotaRemision() {
    }

    /**
     * Constructor con parametros
     * @param idTalonario
     * @param nroNotaCredito
     * @param timbrado
     * @param fechaVencimientoTimbrado
     * @param fecha 
     */
    public DatosNotaRemision(Integer idTalonario, String nroNotaRemision, Integer timbrado, Date fechaVencimientoTimbrado, Date fecha) {
        this.idTalonario = idTalonario;
        this.nroNotaRemision = nroNotaRemision;
        this.timbrado = timbrado;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fecha = fecha;
    }
    
    

    

    
    
       

}
