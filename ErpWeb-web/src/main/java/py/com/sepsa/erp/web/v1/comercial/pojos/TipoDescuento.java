
package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * POJO para la lista de tipo descuento
 * @author Cristina Insfrán
 */
public class TipoDescuento {
   /**
    * Identificador 
    */ 
    private Integer id;
    /**
     * Descripción del tipo de descuento
     */
    private String descripcion;
    /**
     * Código del tipo de descuento
     */
    private String codigo;
    
     /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    /**
     * Constructor de la clase
     */
    public TipoDescuento(){
        
    }
}
