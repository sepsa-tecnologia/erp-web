
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.util.Date;

/**
 * Clase para el manejo de los parámetros de transaccion red
 * @author Cristina Insfrán
 */
public class TransaccionRed {

    
    /**
     * Identificador de red
     */
    private Integer idRed;
    
    /**
     * Identificador de operación
     */
    private Integer idOperacion;
    
    /**
     * Identificador de mensaje
     */
    private Integer idMensaje;
    
    /**
     * Identificador de transaccion
     */
    private String idTransaccion;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * @return the idRed
     */
    public Integer getIdRed() {
        return idRed;
    }

    /**
     * @param idRed the idRed to set
     */
    public void setIdRed(Integer idRed) {
        this.idRed = idRed;
    }

    /**
     * @return the idOperacion
     */
    public Integer getIdOperacion() {
        return idOperacion;
    }

    /**
     * @param idOperacion the idOperacion to set
     */
    public void setIdOperacion(Integer idOperacion) {
        this.idOperacion = idOperacion;
    }

    /**
     * @return the idMensaje
     */
    public Integer getIdMensaje() {
        return idMensaje;
    }

    /**
     * @param idMensaje the idMensaje to set
     */
    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    /**
     * @return the idTransaccion
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * @param idTransaccion the idTransaccion to set
     */
    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
    /**
     * Constructor 
     */
    public TransaccionRed(){
        
    }
    
}
