package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para lista de inventario
 *
 * @author Romina Núñez
 */
public class InventarioResumenFilter extends Filter {

    /**
     * Agrega el filtro por id de inventario
     *
     * @param id
     * @return
     */
    public InventarioResumenFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDeposito
     *
     * @param idDeposito
     * @return
     */
    public InventarioResumenFilter idDeposito(Integer idDeposito) {
        if (idDeposito != null) {
            params.put("idDeposito", idDeposito);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoDeposito
     *
     * @param codigoDeposito
     * @return
     */
    public InventarioResumenFilter codigoDeposito(String codigoDeposito) {
        if (codigoDeposito != null && codigoDeposito.trim().isEmpty()) {
            params.put("codigoDeposito", codigoDeposito);
        }
        return this;
    }

    /**
     * Agrega el filtro por deposito
     *
     * @param deposito
     * @return
     */
    public InventarioResumenFilter deposito(String deposito) {
        if (deposito != null && deposito.trim().isEmpty()) {
            params.put("deposito", deposito);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProducto
     *
     * @param idProducto
     * @return
     */
    public InventarioResumenFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro por producto
     *
     * @param producto
     * @return
     */
    public InventarioResumenFilter producto(String producto) {
        if (producto != null && producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoGtin
     *
     * @param codigoGtin
     * @return
     */
    public InventarioResumenFilter codigoGtin(String codigoGtin) {
        if (codigoGtin != null && codigoGtin.trim().isEmpty()) {
            params.put("codigoGtin", codigoGtin);
        }
        return this;
    }
    
        /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public InventarioResumenFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaVencimiento);

                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoInterno
     *
     * @param codigoInterno
     * @return
     */
    public InventarioResumenFilter codigoInterno(String codigoInterno) {
        if (codigoInterno != null && codigoInterno.trim().isEmpty()) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public InventarioResumenFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado
     *
     * @param estado
     * @return
     */
    public InventarioResumenFilter estado(String estado) {
        if (estado != null && estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public InventarioResumenFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por cantidad
     *
     * @param cantidad
     * @return
     */
    public InventarioResumenFilter cantidad(Integer cantidad) {
        if (cantidad != null) {
            params.put("cantidad", cantidad);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public InventarioResumenFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param inventario datos del filtro
     * @param page página consultada
     * @param pageSize tamaño de la página consultada
     * @return
     */
    public static Map build(InventarioResumen inventario, Integer page,
            Integer pageSize) {

        InventarioResumenFilter filter = new InventarioResumenFilter();

        filter
                .id(inventario.getId())
                .idProducto(inventario.getIdProducto())
                .producto(inventario.getProducto())
                .codigoGtin(inventario.getCodigoGtin())
                .codigoInterno(inventario.getCodigoInterno())
                .cantidad(inventario.getCantidad())
                .listadoPojo(inventario.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public InventarioResumenFilter() {
        super();
    }

}
