package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * Pojo para tipoMotivo
 *
 * @author Alexander Triana
 */
public class TipoMotivo {

    /**
     * Identificador de tipoMotivo
     */
    private Integer id;
    /**
     * Código de tipoMotivo
     */
    private String codigo;
    /**
     * Descripción de tipoMotivo
     */
    private String descripcion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public TipoMotivo() {
    }

    /**
     * Constructor de la clase con parámetro id
     * @param id 
     */
    public TipoMotivo(Integer id) {
        this.id = id;
    }
    
}
