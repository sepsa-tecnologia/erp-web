package py.com.sepsa.erp.web.v1.remote;

/**
 * APIErpNotificacion para conexión a servicios
 *
 * @author Jonathan D. Bernal Fernández, Romina Núñez
 */
public class APIErpNotificacion extends API {

    //Parámetros de conexións
    private final static String BASE_API = "rest";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;

    public APIErpNotificacion() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);

        InfoPojo info = InfoPojo.createInstance("api-erp-notificacion");

        updateConnInfo(info);
    }

    public APIErpNotificacion(String token) {
        super(null, null, 0, BASE_API, CONN_TIMEOUT, token);

        InfoPojo info = InfoPojo.createInstance("api-erp-notificacion");

        updateConnInfo(info);
    }
}
