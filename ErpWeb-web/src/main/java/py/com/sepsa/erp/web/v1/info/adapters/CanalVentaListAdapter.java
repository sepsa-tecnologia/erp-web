/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.remote.CanalVentaService;

/**
 * Adaptador para Canal Venta
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CanalVentaListAdapter extends DataListAdapter<CanalVenta> {

    /**
     * Cliente para el servicio de Canal Venta
     */
    private final CanalVentaService canalVentaClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CanalVentaListAdapter fillData(CanalVenta searchData) {

        return canalVentaClient.getCanalVentaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de CanalVentaListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CanalVentaListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.canalVentaClient = new CanalVentaService();
    }

    /**
     * Constructor de CanalVentaListAdapter
     */
    public CanalVentaListAdapter() {
        super();
        this.canalVentaClient = new CanalVentaService();
    }
}
