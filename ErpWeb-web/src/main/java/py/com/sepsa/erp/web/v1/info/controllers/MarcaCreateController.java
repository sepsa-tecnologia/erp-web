package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.remote.MarcaService;

/**
 * Controlador para creación de Marca
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("marcaCreate")
public class MarcaCreateController implements Serializable {

    /**
     * Servicio para la creación de Marca
     */
    private final MarcaService service;

    /**
     * Pojo de Marca
     */
    private Marca marca;

    /**
     * Pojo para empresa
     */
    private Empresa empresaComplete;

    /**
     * Adapter para empresa
     */
    private EmpresaAdapter adapterEmpresa;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Empresa getEmpresaComplete() {
        return empresaComplete;
    }

    public void setEmpresaComplete(Empresa empresaComplete) {
        this.empresaComplete = empresaComplete;
    }

    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }

    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    //</editor-fold>

    /**
     * Método para crear Marca
     */
    public void create() {

        BodyResponse<Marca> respuesta = service.setMarca(marca);

        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Marca creada correctamente!"));
        }
        this.marca = new Marca();
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa empresa = new Empresa();
        empresa.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(empresa);
        return adapterEmpresa.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        empresaComplete.setId(((Empresa) event.getObject()).getId());
    }

    /**
     * Inicializa los datos del controlador
     */
    private void init() {
        this.marca = new Marca();
        this.empresaComplete = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
    }

    /**
     * Constructor de la clase
     */
    public MarcaCreateController() {
        init();
        this.service = new MarcaService();
    }

}
