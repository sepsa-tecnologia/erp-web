package py.com.sepsa.erp.web.v1.inventario.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.OperacionInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.OperacionInventarioDetalleFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Cliente para el servicio de Detalle de operación de inventario
 *
 * @author Alexander Triana
 */
public class OperacionInventarioDetalleService extends APIErpInventario {

    /**
     * Obtiene la lista detalle de operacion de inventario 
     *
     * @param operacionDetalle Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public OperacionInventarioDetalleAdapter getOperacionDetalle(
            OperacionInventarioDetalle operacionDetalle, Integer page,
            Integer pageSize) {
        
        OperacionInventarioDetalleAdapter lista = new 
        OperacionInventarioDetalleAdapter();
        try {
            Map params = OperacionInventarioDetalleFilter.build(
                    operacionDetalle, page, pageSize);
            HttpURLConnection conn = GET(Resource.OPERACION_DETALLE.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        OperacionInventarioDetalleAdapter.class);
                lista = (OperacionInventarioDetalleAdapter) 
                        response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Constructor de clase
     */
    public OperacionInventarioDetalleService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpInventario
     */
    public enum Resource {

        //Servicios
        OPERACION_DETALLE("OperacionInventarioDetalle", 
            "operacion-inventario-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
