
package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;

/**
 *
 * @author Cristina Insfrán
 */
@FacesConverter("GeoPojoConverter.Erp")
public class GeoLocalizacionPojoConverter implements Converter{
    
      /**
     * Cliente remoto para Referencia Geografica
     */
    private  ReferenciaGeograficaService refGeoClient;
    
    @Override
    public ReferenciaGeografica getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            refGeoClient = new ReferenciaGeograficaService();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            ReferenciaGeografica geo=new ReferenciaGeografica();
            geo.setId(val);
          
            List<ReferenciaGeografica> clientes = refGeoClient.getRefGeograficaList(geo, 0, 10).getData();
            if(clientes != null && !clientes.isEmpty()) {
                return clientes.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es un cliente válido"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof ReferenciaGeografica) {
                return String.valueOf(((ReferenciaGeografica)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    } 
}
