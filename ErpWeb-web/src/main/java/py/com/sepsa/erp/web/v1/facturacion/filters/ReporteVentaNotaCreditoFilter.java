package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteVentaNotaCredito;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filtro para reporte de venta nota credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ReporteVentaNotaCreditoFilter extends Filter {

    /**
     * Agrega el filtro de identificador local talonario
     *
     * @param idLocalTalonario
     * @return
     */
    public ReporteVentaNotaCreditoFilter idLocalTalonario(Integer idLocalTalonario) {
        if (idLocalTalonario != null) {
            params.put("idLocalTalonario", idLocalTalonario);
        }
        return this;
    }
    
    public ReporteVentaNotaCreditoFilter idCliente(Integer idCliente) {
        
        if (idCliente != null) {
            params.put("idCliente", idCliente); 
        }
        return this;  
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public ReporteVentaNotaCreditoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public ReporteVentaNotaCreditoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public ReporteVentaNotaCreditoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param venta
     * @return
     */
    public static Map build(ReporteVentaNotaCredito venta) {
        ReporteVentaNotaCreditoFilter filter = new ReporteVentaNotaCreditoFilter();

        filter
                .idCliente(venta.getIdCliente())
                .fechaDesde(venta.getFechaDesde())
                .fechaHasta(venta.getFechaHasta())
                .anulado(venta.getAnulado())
                .idLocalTalonario(venta.getIdLocalTalonario());

        return filter.getParams();

    }

    /**
     * Constructor de la clase
     */
    public ReporteVentaNotaCreditoFilter() {
        super();
    }

}
