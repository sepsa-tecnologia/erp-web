/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import java.util.List;
import py.com.sepsa.utils.info.adapters.ConfiguracionValorAdapter;
import py.com.sepsa.utils.info.adapters.ConfiguracionValorOpenAdapter;
import py.com.sepsa.utils.info.pojos.ConfiguracionValor;
import py.com.sepsa.utils.misc.Lists;

/**
 *
 * @author Jonathan
 */
public class ConfiguracionValorUtils {
    
    private static final String USER = "USER";
    private static final String PASS = "PASS";
    private static final String API_TOKEN = "API_TOKEN";
    private static final String TIPO_NOTIFICACION = "TIPO_NOTIFICACION";
    
    public static List<String> getStrings(Integer idEmpresa, String code) {
        
        ConfiguracionValorOpenAdapter adapter = new ConfiguracionValorOpenAdapter(null, 0, 100);
        adapter.getFilter().setActivo('S');
        adapter.getFilter().setIdEmpresa(idEmpresa);
        adapter.getFilter().setCodigoConfiguracion(code);
        adapter.fillData();
        
        List<String> result = Lists.empty();
        
        for (ConfiguracionValor configuracionValor : adapter.getData()) {
            result.add(configuracionValor.getValor());
        }
        
        return result;
    }
    
    public static String getString(Integer idEmpresa, String code) {
        List<String> list = getStrings(idEmpresa, code);
        return list == null || list.isEmpty() ? null : list.get(0);
    }
    
    public static String getUser(Integer idEmpresa) {
        return getString(idEmpresa, USER);
    }
    
    public static String getPass(Integer idEmpresa) {
        return getString(idEmpresa, PASS);
    }
    
    public static String getTipoNotificacion(Integer idEmpresa) {
        return getString(idEmpresa, TIPO_NOTIFICACION);
    }
    
     public static String getApiToken(Integer idEmpresa) {
        return getString(idEmpresa, API_TOKEN);
    }
}
