package py.com.sepsa.erp.web.v1.factura.pojos;

/**
 * POJO para Trafico Detalle
 *
 * @author Romina Núñez
 */
public class TraficoDetalle {

    /**
     * Identificador de Historico Liquidacion
     */
    private Integer idHistoricoLiquidacion;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Identificador de Tarifa
     */
    private Integer idTipoDocumento;
    /**
     * Tipo Documento
     */
    private String tipoDocumento;
    /**
     * Documentos Recibidos
     */
    private Integer cantDocRecibidos;
    /**
     * Documentos Enviados
     */
    private Integer cantDocEnviados;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }

    
    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

//</editor-fold>
    /**
     * Constructor de la clase
     */
    public TraficoDetalle() {
    }

    
    /**
     * Constructor con parametros
     * @param idHistoricoLiquidacion
     * @param nroLinea
     * @param idTipoDocumento
     * @param tipoDocumento
     * @param cantDocRecibidos
     * @param cantDocEnviados 
     */
    public TraficoDetalle(Integer idHistoricoLiquidacion, Integer nroLinea, Integer idTipoDocumento, String tipoDocumento, Integer cantDocRecibidos, Integer cantDocEnviados) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.nroLinea = nroLinea;
        this.idTipoDocumento = idTipoDocumento;
        this.tipoDocumento = tipoDocumento;
        this.cantDocRecibidos = cantDocRecibidos;
        this.cantDocEnviados = cantDocEnviados;
    }

        

}
