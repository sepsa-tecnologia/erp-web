
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;

/**
 * Adaptador para la lista de Cliente
 * @author Daniel F. Escauriza Arza
 */
public class ClientListAdapter extends DataListAdapter<Cliente> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final ClientServiceClient serviceClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ClientListAdapter fillData(Cliente searchData) {
     
        return serviceClient.getClientList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ClientListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClient = new ClientServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public ClientListAdapter() {
        super();
        this.serviceClient = new ClientServiceClient();
    }
    
}
