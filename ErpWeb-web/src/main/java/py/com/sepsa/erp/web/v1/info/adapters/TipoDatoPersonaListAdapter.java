/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoDatoPersona;
import py.com.sepsa.erp.web.v1.info.remote.TipoDatoPersonaService;

/**
 * Adapter para tipo dato persona
 *
 * @author Sergio D. Riveros Vazquez
 */

public class TipoDatoPersonaListAdapter extends DataListAdapter<TipoDatoPersona> {

    /**
     * Cliente para los servicios de clientes
     */
    private final TipoDatoPersonaService tipoDatoPersonaClient;

    /**
     * Constructor de TipoDatoPersonaPersonaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoDatoPersonaListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoDatoPersonaClient = new TipoDatoPersonaService();
    }

    /**
     * Constructor de TipoDatoPersonaListAdapter
     */
    public TipoDatoPersonaListAdapter() {
        super();
        this.tipoDatoPersonaClient = new TipoDatoPersonaService();
    }

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoDatoPersonaListAdapter fillData(TipoDatoPersona searchData) {
        return tipoDatoPersonaClient.getTipoDatoPersonaList(searchData, getFirstResult(), getPageSize());
    }

}
