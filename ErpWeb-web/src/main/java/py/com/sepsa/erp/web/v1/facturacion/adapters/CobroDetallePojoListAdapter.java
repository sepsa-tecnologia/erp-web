/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 *
 * @author Antonella Lucero
 */
public class CobroDetallePojoListAdapter extends DataListAdapter<CobroDetallePojo>{
   
     /**
     * Cliente para los servicios de facturación
     */
    private final FacturacionService facturacionClient;
   
    
    @Override
    public CobroDetallePojoListAdapter fillData(CobroDetallePojo searchData) {
        return facturacionClient.getDetalleCobroPojoList(searchData,getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de CobroDetallePojoListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobroDetallePojoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.facturacionClient = new FacturacionService();
    }

    /**
     * Constructor de CobroDetallePojoListAdapter
     */
    public CobroDetallePojoListAdapter() {
        super();
        this.facturacionClient = new FacturacionService();
    }
}