/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.filters.CanalVentaFilter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Servicio para Canal Venta
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CanalVentaService extends APIErpCore {

    /**
     * Obtiene la lista de canal de venta
     *
     * @param dato
     * @param page
     * @param pageSize
     * @return
     */
    public CanalVentaListAdapter getCanalVentaList(CanalVenta dato, Integer page,
            Integer pageSize) {

        CanalVentaListAdapter lista = new CanalVentaListAdapter();

        Map params = CanalVentaFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.CANAL_VENTA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CanalVentaListAdapter.class);

            lista = (CanalVentaListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para crear
     *
     * @param dato
     * @return
     */
    public BodyResponse<CanalVenta> setCanalVenta(CanalVenta dato) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CANAL_VENTA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(dato));
            response = BodyResponse.createInstance(conn, CanalVenta.class);
        }
        return response;
    }

    /**
     * Método para editar canal venta
     *
     * @param dato
     * @return
     */
    public BodyResponse editCanalVenta(CanalVenta dato) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.CANAL_VENTA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(dato));
            response = BodyResponse.createInstance(conn, CanalVenta.class);
        }
        return response;
    }

    /**
     * Constructor de CanalVentaServiceClient
     */
    public CanalVentaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CANAL_VENTA("Servicio para canal de venta", "canal-venta");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
