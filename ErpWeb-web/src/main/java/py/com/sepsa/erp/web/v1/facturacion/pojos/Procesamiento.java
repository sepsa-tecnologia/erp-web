package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Romina
 */
public class Procesamiento {
    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de envío
     */
    private Integer idEnvio;
    /**
     * Nro Transaccion
     */
    private String nroTransaccion;
    /**
     * Fecha insercion
     */
    private Date fechaInsercion;
    /**
     * Fecha procesamiento
     */
    private Date fechaProcesamiento;
    /**
     * Estado resultado
     */
    private String estadoResultado;
    /**
     * Listado de detalles de procesamiento
     */
    private List<DetalleProcesamiento> detalleProcesamientos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public String getNroTransaccion() {
        return nroTransaccion;
    }

    public void setNroTransaccion(String nroTransaccion) {
        this.nroTransaccion = nroTransaccion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getEstadoResultado() {
        return estadoResultado;
    }

    public void setEstadoResultado(String estadoResultado) {
        this.estadoResultado = estadoResultado;
    }

    public List<DetalleProcesamiento> getDetalleProcesamientos() {
        return detalleProcesamientos;
    }

    public void setDetalleProcesamientos(List<DetalleProcesamiento> detalleProcesamientos) {
        this.detalleProcesamientos = detalleProcesamientos;
    }

    public Procesamiento() {
    }

    
    
}
