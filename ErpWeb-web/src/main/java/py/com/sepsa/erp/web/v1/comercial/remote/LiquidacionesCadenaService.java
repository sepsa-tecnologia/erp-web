package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionesCadenaAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.LiquidacionesCadenaFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesCadena;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para el listado de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionesCadenaService extends APIErpFacturacion {

    /**
     * Obtiene la lista de liquidaciones por cadena.
     *
     * @param searchData
     * @param page
     * @param pagesize
     * @return
     */
    public LiquidacionesCadenaAdapter getLiquidacionesCadenaList(LiquidacionesCadena searchData,
            Integer page, Integer pagesize) {

        LiquidacionesCadenaAdapter lista = new LiquidacionesCadenaAdapter();

        Map params = LiquidacionesCadenaFilter.build(searchData, page, pagesize);

        HttpURLConnection conn = GET(Resource.LIQUIDACIONES_CADENA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LiquidacionesCadenaAdapter.class);

            lista = (LiquidacionesCadenaAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;
    }

    /**
     * Constructor de LiquidacionesCadenaService
     */
    public LiquidacionesCadenaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaFacturación
     */
    public enum Resource {

        //Servicios
        LIQUIDACIONES_CADENA("Servicios de Liquidaciones por cadena", "historico-liquidacion/cadena");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
