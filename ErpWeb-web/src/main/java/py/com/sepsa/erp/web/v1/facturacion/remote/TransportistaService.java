/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioLocalRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TransportistaListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioFilter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioLocalFilter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TransportistaFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioLocal;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Talonario
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class TransportistaService extends APIErpFacturacion {

    /**
     * Obtiene la lista de talonarios
     *
     * @param param Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TransportistaListAdapter getTransportistaList(Transportista param, Integer page,
            Integer pageSize) {

        TransportistaListAdapter lista = new TransportistaListAdapter();

        Map params = TransportistaFilter.build(param, page, pageSize);

        HttpURLConnection conn = GET(Resource.TRANSPORTISTA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TransportistaListAdapter.class);

            lista = (TransportistaListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param transportista
     * @return
     */
    public BodyResponse<Transportista> setTransportista(Transportista transportista) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.TRANSPORTISTA.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(transportista));

            response = BodyResponse.createInstance(conn, Transportista.class);

        }
        return response;
    }

    /**
     * Método para editar transportista
     *
     * @param transportista
     * @return
     */
    public BodyResponse<Transportista> editTransportista(Transportista transportista) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.TRANSPORTISTA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(transportista));
            response = BodyResponse.createInstance(conn, Transportista.class);
        }
        return response;
    }

     /**
     *
     * @param id
     * @return
     */
    public Transportista get(Integer id) {
        Transportista data = new Transportista();
        data.setId(id);
        TransportistaListAdapter list = getTransportistaList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public TransportistaService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TRANSPORTISTA("transportista", "transportista");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
