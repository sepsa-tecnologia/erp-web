package py.com.sepsa.erp.web.v1.system.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.MenuPerfil;
import java.util.List;
import py.com.sepsa.erp.web.v1.info.pojos.TipoMenu;

/**
 * POJO para la lista de menu
 *
 * @author Cristina Insfrán
 */
public class Menu {

    /**
     * Identificador del menu
     */
    private Integer id;
    /**
     * Código
     */
    private String codigo;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Titulo
     */
    private String titulo;
    /**
     * Url
     */
    private String url;
    /**
     * Activo
     */
    private String activo;
    /**
     * Hijos del menu
     */
    private List<Menu> hijos;
    /**
     * Identificador de tipo de menú
     */
    private Integer idTipoMenu;
    /**
     * Identificador del idPadre
     */
    private Integer idPadre;
    /**
     * Tiene padre
     */
    private String tienePadre;
    /**
     * Tiene hijos
     */
    private String tieneHijo;
    /**
     * Tipo menu
     */
    private TipoMenu tipoMenu;
    /**
     * Lista de MenuPerfil
     */
    private List<MenuPerfil> menuPerfiles;
    private Integer idEmpresa;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the tieneHijo
     */
    public String getTieneHijo() {
        return tieneHijo;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param tieneHijo the tieneHijo to set
     */
    public void setTieneHijo(String tieneHijo) {
        this.tieneHijo = tieneHijo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoMenu getTipoMenu() {
        return tipoMenu;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * @return the hijos
     */
    public List<Menu> getHijos() {
        return hijos;
    }

    /**
     * @param hijos the hijos to set
     */
    public void setHijos(List<Menu> hijos) {
        this.hijos = hijos;
    }

    public void setTipoMenu(TipoMenu tipoMenu) {
        this.tipoMenu = tipoMenu;
    }

    public String getTienePadre() {
        return tienePadre;
    }

    public void setIdTipoMenu(Integer idTipoMenu) {
        this.idTipoMenu = idTipoMenu;
    }

    public Integer getIdTipoMenu() {
        return idTipoMenu;
    }

    /**
     * @return the idPadre
     */
    public Integer getIdPadre() {
        return idPadre;
    }

    /**
     * @param idPadre the idPadre to set
     */
    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    /**
     * @return the tienePadre
     */
    public String isTienePadre() {
        return tienePadre;
    }

    /**
     * @param tienePadre the tienePadre to set
     */
    public void setTienePadre(String tienePadre) {
        this.tienePadre = tienePadre;
    }

    /**
     *
     * @param menuPerfiles
     */
    public void setMenuPerfiles(List<MenuPerfil> menuPerfiles) {
        this.menuPerfiles = menuPerfiles;
    }

    /**
     *
     * @return
     */
    public List<MenuPerfil> getMenuPerfiles() {
        return menuPerfiles;
    }

//</editor-fold>
    /**
     * Constructor de la clase
     *
     * @param id
     * @param codigo
     * @param descripcion
     * @param titulo
     * @param url
     * @param activo
     * @param hijos
     * @param idPadre
     * @param tienePadre
     * @param tieneHijo
     */
    public Menu(Integer id, String codigo, String descripcion, String titulo,
            String url, String activo, List<Menu> hijos, Integer idPadre,
            String tienePadre, String tieneHijo) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.titulo = titulo;
        this.url = url;
        this.activo = activo;
        this.hijos = hijos;
        this.idPadre = idPadre;
        this.tienePadre = tienePadre;
        this.tieneHijo = tieneHijo;

    }

    /**
     * Constructor con parametros
     */
    public Menu() {

    }

}
