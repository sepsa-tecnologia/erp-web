package py.com.sepsa.erp.web.v1.inventario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.OperacionInventarioDetalle;

/**
 * Filter para lista de detalle de operación de inventario
 *
 * @author Alexander Triana
 */
public class OperacionInventarioDetalleFilter extends Filter {

    /**
     * Agrega el filtro por id
     *
     * @param id
     * @return
     */
    public OperacionInventarioDetalleFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idOperacionInventario
     *
     * @param idOperacionInventario
     * @return
     */
    public OperacionInventarioDetalleFilter idOperacionInventario(Integer idOperacionInventario) {
        if (idOperacionInventario != null) {
            params.put("idOperacionInventario", idOperacionInventario);
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public OperacionInventarioDetalleFilter idEstadoOrigen(Integer idEstadoOrigen) {
        if (idEstadoOrigen != null) {
            params.put("idEstadoOrigen", idEstadoOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProducto
     *
     * @param idProducto
     * @return
     */
    public OperacionInventarioDetalleFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoGtin
     *
     * @param codigoGtin
     * @return
     */
    public OperacionInventarioDetalleFilter codigoGtin(String codigoGtin) {
        if (codigoGtin != null && !codigoGtin.trim().isEmpty()) {
            params.put("codigoGtin", codigoGtin);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoInterno
     *
     * @param codigoInterno
     * @return
     */
    public OperacionInventarioDetalleFilter codigoInterno(String codigoInterno) {
        if (codigoInterno != null && !codigoInterno.trim().isEmpty()) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro por producto
     *
     * @param producto
     * @return
     */
    public OperacionInventarioDetalleFilter producto(String producto) {
        if (producto != null && !producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro por cantidad
     *
     * @param cantidad
     * @return
     */
    public OperacionInventarioDetalleFilter cantidad(Integer cantidad) {
        if (cantidad != null) {
            params.put("cantidad", cantidad);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado
     *
     * @param estado
     * @return
     */
    public OperacionInventarioDetalleFilter estadoOPeracion(String estadoOPeracion) {
        if (estadoOPeracion != null && !estadoOPeracion.trim().isEmpty()) {
            params.put("estadoOPeracion", estadoOPeracion);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado
     *
     * @param codigoEstado
     * @return
     */
    public OperacionInventarioDetalleFilter codigoEstadoInventarioOrigen(String codigoEstadoInventarioOrigen) {
        if (codigoEstadoInventarioOrigen != null && !codigoEstadoInventarioOrigen.trim().isEmpty()) {
            params.put("codigoEstadoInventarioOrigen", codigoEstadoInventarioOrigen);
        }
        return this;
    }

    /**
     * Agrega el filtro listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public OperacionInventarioDetalleFilter listadoPojo(String listadoPojo) {
        if (listadoPojo != null && !listadoPojo.trim().isEmpty()) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param operacionDetalle
     * @param page
     * @param pageSize
     * @return
     */
    public static Map build(OperacionInventarioDetalle operacionDetalle,
            Integer page, Integer pageSize) {

        OperacionInventarioDetalleFilter filter = new OperacionInventarioDetalleFilter();

        filter
                .id(operacionDetalle.getId())
                .idOperacionInventario(operacionDetalle.getIdOperacionInventario())
                .idProducto(operacionDetalle.getIdProducto())
                .idEstadoOrigen(operacionDetalle.getIdEstadoInventarioOrigen())
                .codigoGtin(operacionDetalle.getCodigoGtin())
                .codigoInterno(operacionDetalle.getCodigoInterno())
                .listadoPojo(operacionDetalle.getListadoPojo())
                .producto(operacionDetalle.getProducto())
                .estadoOPeracion(operacionDetalle.getEstadoOperacion())
                .codigoEstadoInventarioOrigen(operacionDetalle.getCodigoEstadoInventarioOrigen())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();

    }

    /**
     * Constructor de la clase
     */
    public OperacionInventarioDetalleFilter() {
        super();
    }

}
