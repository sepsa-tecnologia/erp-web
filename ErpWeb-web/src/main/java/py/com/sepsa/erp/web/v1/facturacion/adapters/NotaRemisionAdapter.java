/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaRemision;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaRemisionService;

/**
 *
 * @author Williams Vera
 */
public class NotaRemisionAdapter extends DataListAdapter<NotaRemision> {
    
    private final NotaRemisionService serviceNotaRemision;

 /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaRemisionAdapter fillData(NotaRemision searchData) {
     
        return serviceNotaRemision.getNotaRemisionList(searchData,getFirstResult(),getPageSize());
    }
    
       /**
     * Constructor de NotaRemisionAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaRemisionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaRemision = new NotaRemisionService();
    }

    /**
     * Constructor de NotaRemisionAdapter
     */
    public NotaRemisionAdapter() {
        super();
        this.serviceNotaRemision = new NotaRemisionService();
    }
    
    
}
