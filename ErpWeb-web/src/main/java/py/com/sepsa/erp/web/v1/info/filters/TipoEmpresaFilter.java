/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmpresa;

/**
 * filter para tipo tipoEmpresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEmpresaFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoEmpresaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion
     * @return
     */
    public TipoEmpresaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoEmpresaFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoEmpresa datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoEmpresa tipoEmpresa, Integer page, Integer pageSize) {
        TipoEmpresaFilter filter = new TipoEmpresaFilter();

        filter
                .id(tipoEmpresa.getId())
                .descripcion(tipoEmpresa.getDescripcion())
                .codigo(tipoEmpresa.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
}
