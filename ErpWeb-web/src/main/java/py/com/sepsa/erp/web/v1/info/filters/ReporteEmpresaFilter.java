package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ReporteEmpresa;

/**
 * Filtro utilizado para el servicio de reporte empresa
 *
 * @author Jonathan Bernal
 */
public class ReporteEmpresaFilter extends Filter {

    public ReporteEmpresaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    public ReporteEmpresaFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    public ReporteEmpresaFilter activo(Character activo) {
        if (activo != null) {
            params.put("activo", activo);
        }
        return this;
    }

    public ReporteEmpresaFilter codigoReporte(String codigoReporte) {
        if (codigoReporte != null) {
            params.put("codigoReporte", codigoReporte);
        }
        return this;
    }

    public ReporteEmpresaFilter descripcionReporte(String descripcionReporte) {
        if (descripcionReporte != null) {
            params.put("descripcionReporte", descripcionReporte);
        }
        return this;
    }

    public ReporteEmpresaFilter idReporte(Integer idReporte) {
        if (idReporte != null) {
            params.put("idReporte", idReporte);
        }
        return this;
    }

    public static Map build(ReporteEmpresa param, Integer page, Integer pageSize) {
        ReporteEmpresaFilter filter = new ReporteEmpresaFilter();

        filter
                .id(param.getId())
                .idEmpresa(param.getIdEmpresa())
                .activo(param.getActivo())
                .codigoReporte(param.getCodigoReporte())
                .idReporte(param.getIdReporte())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

}
