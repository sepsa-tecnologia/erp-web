/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionInternoService;

/**
 * Adapter para motivo emision interno
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionInternoAdapter extends DataListAdapter<MotivoEmisionInterno> {

    /**
     *
     * Cliente remoto para tipo motivoEmisionInterno
     *
     */
    private final MotivoEmisionInternoService motivoEmisionInternoClient;

    /**
     *
     * Método para cargar la lista de datos
     *
     *
     *
     * @param searchData Datos de búsqueda
     *
     * @return ProductListAdapter
     *
     */
    @Override

    public MotivoEmisionInternoAdapter fillData(MotivoEmisionInterno searchData) {

        return motivoEmisionInternoClient.getMotivoEmisionInterno(searchData, getFirstResult(), getPageSize());

    }

    /**
     *
     * Constructor de TipoDocumentoSAdapter
     *
     *
     *
     * @param page Página de la lista
     *
     * @param pageSize Tamaño de la página
     *
     */
    public MotivoEmisionInternoAdapter(Integer page, Integer pageSize) {

        super(page, pageSize);

        this.motivoEmisionInternoClient = new MotivoEmisionInternoService();

    }

    /**
     *
     * Constructor de MotivoEmisionInternoAdapter
     *
     */
    public MotivoEmisionInternoAdapter() {

        super();

        this.motivoEmisionInternoClient = new MotivoEmisionInternoService();

    }

}
