/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 *
 * @author Romina Núñez
 */
public class Estado {

    /**
     * Identificador de estado
     */
    private Integer id;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Activo
     */
    private String activo;
    /**
     * codigo
     */
    private String codigo;
    /**
     * Identificador de tipo estado
     */
    private Integer idTipoEstado;
    /**
     * POJO Tipo Estado
     */
    private TipoEstado tipoEstado;
    /**
     * Codigo tipo estado
     */
    private String codigoTipoEstado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public Integer getIdTipoEstado() {
        return idTipoEstado;
    }

    public void setIdTipoEstado(Integer idTipoEstado) {
        this.idTipoEstado = idTipoEstado;
    }

    public TipoEstado getTipoEstado() {
        return tipoEstado;
    }

    public void setTipoEstado(TipoEstado tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

    public String getCodigoTipoEstado() {
        return codigoTipoEstado;
    }

    public void setCodigoTipoEstado(String codigoTipoEstado) {
        this.codigoTipoEstado = codigoTipoEstado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Estado() {
    }

    public Estado(Integer id) {
        this.id = id;
    }

    public Estado(TipoEstado tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

}
