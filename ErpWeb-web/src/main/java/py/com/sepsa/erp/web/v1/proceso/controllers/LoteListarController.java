/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.shaded.commons.io.FilenameUtils;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.pojos.TipoLote;
import py.com.sepsa.erp.web.v1.proceso.adapters.LoteAdapter;
import py.com.sepsa.erp.web.v1.proceso.adapters.TipoLoteAdapter;
import py.com.sepsa.erp.web.v1.proceso.remote.LoteService;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("loteListar")
public class LoteListarController implements Serializable {
     /**
     * Adaptador para la lista de nota de debito
     */
    private LoteAdapter adapterLote;
    /**
     * POJO NotaDebito
     */
    private Lote loteFilter;
    
    private TipoLoteAdapter tipoLoteAdapter;
    
    private TipoLote tipoLoteFilter; 
    
    private Empresa empresaAutocomplete;
    
    private EmpresaAdapter empresaAdapter;
    
    private LoteService serviceLote;
    
    public LoteAdapter getAdapterLote() {
        return adapterLote;
    }

    public void setAdapterLote(LoteAdapter adapterLote) {
        this.adapterLote = adapterLote;
    }

    public Lote getLoteFilter() {
        return loteFilter;
    }

    public void setLoteFilter(Lote loteFilter) {
        this.loteFilter = loteFilter;
    }

    public TipoLoteAdapter getTipoLoteAdapter() {
        return tipoLoteAdapter;
    }

    public void setTipoLoteAdapter(TipoLoteAdapter tipoLoteAdapter) {
        this.tipoLoteAdapter = tipoLoteAdapter;
    }

    public TipoLote getTipoLoteFilter() {
        return tipoLoteFilter;
    }

    public void setTipoLoteFilter(TipoLote tipoLoteFilter) {
        this.tipoLoteFilter = tipoLoteFilter;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    public LoteService getServiceLote() {
        return serviceLote;
    }

    public void setServiceLote(LoteService serviceLote) {
        this.serviceLote = serviceLote;
    }
    
    /**
     * Método para llenar el adapter
     */
    public void filterLote() {
        adapterLote.setFirstResult(0);
        loteFilter.setListadoPojo(true);
        this.adapterLote = adapterLote.fillData(loteFilter);
    }

    public void clear() {
        loteFilter = new Lote();
        empresaAutocomplete = new Empresa();
        tipoLoteFilter = new TipoLote();
        filterLote();
        filterTipoLote();
        
    }
    
    public void filterTipoLote() { 
        this.tipoLoteAdapter = tipoLoteAdapter.fillData(tipoLoteFilter);
    }
    
    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        loteFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }
    
    public StreamedContent download(Lote lote) {
        byte[] data = serviceLote.obtenerResultadoLote(lote);
        String contentType = "text/csv";
        String fileName = "Resultado_"+FilenameUtils.removeExtension(lote.getNombreArchivo())+".csv";
        return new DefaultStreamedContent(new ByteArrayInputStream(data),contentType,fileName);
    }
    
    
    public StreamedContent downloadArchivos(Lote lote) throws IOException {
        byte[] data = serviceLote.obtenerArchivosLote(lote);
        String contentType = "";
        String fileName = "";
        if (isZipFile(data)) {
          contentType = "application/zip";
          fileName = FilenameUtils.removeExtension(lote.getNombreArchivo())+".zip";
          return new DefaultStreamedContent(new ByteArrayInputStream(data),contentType,fileName);
        } else {
          contentType = "text/csv";
          fileName = FilenameUtils.removeExtension(lote.getNombreArchivo())+".csv";
          return new DefaultStreamedContent(new ByteArrayInputStream(data),contentType,fileName);
        }
       
    }
    
    public static boolean isZipFile(byte[] data) {
        try (ZipArchiveInputStream zipStream = new ZipArchiveInputStream(new ByteArrayInputStream(data))) {
            return zipStream.getNextEntry() != null;
        } catch (IOException e) {
            return false;
        }
    }
    
  
    @PostConstruct
    public void init() {
        this.loteFilter = new Lote();
        this.adapterLote = new LoteAdapter();
        this.serviceLote = new LoteService();
        this.tipoLoteFilter = new TipoLote();
        this.tipoLoteAdapter = new TipoLoteAdapter();
        this.empresaAdapter = new EmpresaAdapter();
        this.empresaAutocomplete = new Empresa();
        filterTipoLote();
        filterLote();
        
    }
    
}
