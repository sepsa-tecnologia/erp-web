/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.pojos.SolicitudNotaCredito;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para solicitud nota credito
 *
 * @author Sergio D. Riveros Vazquez
 */
public class SolicitudNotaCreditoFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public SolicitudNotaCreditoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public SolicitudNotaCreditoFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de cliente
     *
     * @param idCliente
     * @return
     */
    public SolicitudNotaCreditoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public SolicitudNotaCreditoFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaInsercionDesde
     * @return
     */
    public SolicitudNotaCreditoFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionDesde);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fechaInsercionHasta
     * @return
     */
    public SolicitudNotaCreditoFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaInsercionHasta);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public SolicitudNotaCreditoFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }

    /**
     * Agregar el filtro de nroDocumento
     *
     * @param nroDocumento
     * @return
     */
    public SolicitudNotaCreditoFilter nroDocumento(String nroDocumento) {
        if (nroDocumento != null && !nroDocumento.trim().isEmpty()) {
            params.put("nroDocumento", nroDocumento);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public SolicitudNotaCreditoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public SolicitudNotaCreditoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de recibido
     *
     * @param recibido
     * @return
     */
    public SolicitudNotaCreditoFilter recibido(String recibido) {
        if (recibido != null && !recibido.trim().isEmpty()) {
            params.put("recibido", recibido);
        }
        return this;
    }

    /**
     * Agregar el filtro de generadoEdi
     *
     * @param generadoEdi
     * @return
     */
    public SolicitudNotaCreditoFilter generadoEdi(String generadoEdi) {
        if (generadoEdi != null && !generadoEdi.trim().isEmpty()) {
            params.put("generadoEdi", generadoEdi);
        }
        return this;
    }

    /**
     * Agregar el filtro de archivoEdi
     *
     * @param archivoEdi
     * @return
     */
    public SolicitudNotaCreditoFilter archivoEdi(String archivoEdi) {
        if (archivoEdi != null && !archivoEdi.trim().isEmpty()) {
            params.put("archivoEdi", archivoEdi);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param solicitudNotaCredito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(SolicitudNotaCredito solicitudNotaCredito, Integer page, Integer pageSize) {
        SolicitudNotaCreditoFilter filter = new SolicitudNotaCreditoFilter();

        filter
                .id(solicitudNotaCredito.getId())
                .idEmpresa(solicitudNotaCredito.getIdEmpresa())
                .idCliente(solicitudNotaCredito.getIdCliente())
                .idMoneda(solicitudNotaCredito.getIdMoneda())
                .fechaInsercionDesde(solicitudNotaCredito.getFechaInsercionDesde())
                .fechaInsercionHasta(solicitudNotaCredito.getFechaInsercionHasta())
                .ruc(solicitudNotaCredito.getRuc())
                .nroDocumento(solicitudNotaCredito.getNroDocumento())
                .razonSocial(solicitudNotaCredito.getRazonSocial())
                .anulado(solicitudNotaCredito.getAnulado())
                .recibido(solicitudNotaCredito.getRecibido())
                .generadoEdi(solicitudNotaCredito.getGeneradoEdi())
                .archivoEdi(solicitudNotaCredito.getArchivoEdi())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaCreditoFilter
     */
    public SolicitudNotaCreditoFilter() {
        super();
    }

}
