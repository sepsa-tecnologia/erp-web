
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;

/**
 * Filtro para el servicio de tipo telefono
 * @author Cristina Insfrán
 */
public class TipoTelefonoFilter extends Filter{
    
    /**
     * Agrega el filtro de identificador del tipo telefono
     *
     * @param id Identificador del rol
     * @return
     */
    public TipoTelefonoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro para el tipo
     * @param tipo descripción del tipo
     * @return 
     */
    public TipoTelefonoFilter tipo(String tipo){
         if(tipo!= null){
           params.put("tipo", tipo);
         }
         return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param tipotelefono datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(TipoTelefono tipotelefono, Integer page, Integer pageSize) {
        TipoTelefonoFilter filter = new TipoTelefonoFilter();

        filter
                .id(tipotelefono.getId())
                .tipo(tipotelefono.getTipo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor TipoTelefonoFilter
     */
    public TipoTelefonoFilter() {
        super();
    }
    
}
