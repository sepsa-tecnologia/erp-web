/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

/**
 * pojo para tipo motivo anulacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoMotivoAnulacion {

    /**
     * Id
     */
    private Integer id;
    /**
     * Descripcion
     */
    private String descripcion;
    /**
     * Codigo
     */
    private String codigo;
    /**
     * Activo
     */
    private String activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public TipoMotivoAnulacion() {

    }
}
