package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para listado de cobros
 *
 * @author alext
 */
public class CobrosFilter extends Filter {

    /**
     * Agrega el filtro por identificador de cobros
     *
     * @param id
     * @return
     */
    public CobrosFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public CobrosFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro fecha
     *
     * @param fecha
     * @return
     */
    public CobrosFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat
                        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }
    
    /**
     * Agregar el filtro por fechaDesde
     *
     * @param fechaDesde
     * @return
     */
    public CobrosFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = 
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaDesde);
                
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                 WebLogger.get().error("Exception: "+e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro por fechaHasta
     *
     * @param fechaHasta
     * @return
     */
    public CobrosFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = 
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                 WebLogger.get().error("Exception: "+e);
            }
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro montoCobro
     *
     * @param montoCobro
     * @return
     */
    public CobrosFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro nroRecibo
     *
     * @param nroRecibo
     * @return
     */
    public CobrosFilter nroRecibo(String nroRecibo) {
        if (nroRecibo != null && !nroRecibo.trim().isEmpty()) {
            params.put("nroRecibo", nroRecibo);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro digital
     *
     * @param digital
     * @return
     */
    public CobrosFilter digital(String digital) {
        if (digital != null) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro enviado
     *
     * @param enviado
     * @return
     */
    public CobrosFilter enviado(Character enviado) {
        if (enviado != null) {
            params.put("enviado", enviado);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro idLugarCobro
     *
     * @param idLugarCobro
     * @return
     */
    public CobrosFilter idLugarCobro(Integer idLugarCobro) {
        if (idLugarCobro != null) {
            params.put("idLugarCobro", idLugarCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro idTalonario
     *
     * @param idTalonario
     * @return
     */
    public CobrosFilter idTalonario(Integer idTalonario) {
        if (idTalonario != null) {
            params.put("idTalonario", idTalonario);
        }
        return this;
    }
    
        /**
     * Agrega el filtro por parámetro idEstado
     *
     * @param estado
     * @return
     */
    public CobrosFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro estado
     *
     * @param estado
     * @return
     */
    public CobrosFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro lugarCobro
     *
     * @param lugarCobro
     * @return
     */
    public CobrosFilter lugarCobro(String lugarCobro) {
        if (lugarCobro != null && !lugarCobro.trim().isEmpty()) {
            params.put("lugarCobro", lugarCobro);
        }
        return this;
    }

    /**
     * Agrega el filtro por parámetro empresa
     *
     * @param empresa
     * @return
     */
    public CobrosFilter empresa(String empresa) {
        if (empresa != null && !empresa.trim().isEmpty()) {
            params.put("empresa", empresa);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parámetros
     * @param cobro datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    
            /**
     * Agrega el filtro por parámetro idEstado
     *
     * @param estado
     * @return
     */
    public CobrosFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }
    
    public static Map build(Cobro cobro, Integer page, Integer pageSize) {
        CobrosFilter filter = new CobrosFilter();
        
        filter
                .id(cobro.getId())
                .idEmpresa(cobro.getIdEmpresa())
                .fecha(cobro.getFecha())
                .fechaDesde(cobro.getFechaDesde())
                .fechaHasta(cobro.getFechaHasta())
                .montoCobro(cobro.getMontoCobro())
                .nroRecibo(cobro.getNroRecibo())
                .digital(cobro.getDigital())
                .enviado(cobro.getEnviado())
                .idLugarCobro(cobro.getIdLugarCobro())
                .idTalonario(cobro.getIdTalonario())
                .idEstado(cobro.getIdEstado())
                .idCliente(cobro.getIdCliente())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public CobrosFilter() {
        super();
    }
    
    
}
