/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.notificacion.adapters.TipoNotificacionesAdapter;
import py.com.sepsa.erp.web.v1.notificacion.filters.TipoNotificacionesFilter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.remote.APIErpNotificacion;

/**
 * Cliente para el servicio Tipo Notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoNotificacionesService extends  APIErpNotificacion {

    /**
     * Obtiene la lista de Tipo Notificaciones.
     *
     * @param tipoNotificaciones
     * @param page
     * @param pageSize
     * @return
     */
    public TipoNotificacionesAdapter getTipoNotificacionList(TipoNotificaciones tipoNotificaciones, Integer page,
            Integer pageSize) {

        TipoNotificacionesAdapter lista = new TipoNotificacionesAdapter();

        Map params = TipoNotificacionesFilter.build(tipoNotificaciones, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoNotificacionesAdapter.class);

            lista = (TipoNotificacionesAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de TipoNotificacionService
     */
    public TipoNotificacionesService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        LISTAR("Listado de Tipo de Notificaciones", "v1/notificationType");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
