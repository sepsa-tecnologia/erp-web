package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteCompraParam;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ReporteComprobanteNCCompraParam;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteCompraServiceClient;

/**
 * Controlador para reporte de comprobante de nota credito compra
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("reporteComprobanteNCCompra")
public class ReporteComprobanteNotaCreditoCompraController implements Serializable {

    /**
     * Objeto
     */
    private ReporteComprobanteCompraParam parametro;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private ReporteCompraServiceClient reporteCliente;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public ReporteComprobanteCompraParam getParametro() {
        return parametro;
    }

    public void setParametro(ReporteComprobanteCompraParam parametro) {
        this.parametro = parametro;
    }

    public ReporteCompraServiceClient getReporteCliente() {
        return reporteCliente;
    }

    public void setReporteCliente(ReporteCompraServiceClient reporteCliente) {
        this.reporteCliente = reporteCliente;
    }

    //</editor-fold>
    /**
     * Método para descargar reporte
     *
     * @return
     */
    public StreamedContent download() {

        String contentType = ("application/zip");
        String fileName = "reporte-comprobante-nota-credito-compra.zip";
        byte[] data = reporteCliente.getReporteComprobanteNotaCreditoCompra(parametro);
        if (data != null) {
            return new DefaultStreamedContent(new ByteArrayInputStream(data),
                    contentType, fileName);
        }

        return null;
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.parametro = new ReporteComprobanteCompraParam();
        this.reporteCliente = new ReporteCompraServiceClient();

        Calendar today = Calendar.getInstance();
        parametro.setFecha(today.getTime());
    }

}
