/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.adapters.VehiculoTrasladoListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("vehiculoTrasladoList")
public class VehiculoTrasladoListController implements Serializable {
    
    private VehiculoTrasladoListAdapter adapterVehiculoTraslado;
    
    private VehiculoTraslado vehiculoTrasladoFilter;
    
    private VehiculoTrasladoService vehiculoTrasladoService;

    
    
    public VehiculoTrasladoService getVehiculoTrasladoService() {
        return vehiculoTrasladoService;
    }

    public void setVehiculoTrasladoService(VehiculoTrasladoService vehiculoTrasladoService) {
        this.vehiculoTrasladoService = vehiculoTrasladoService;
    }
    
    public VehiculoTrasladoListAdapter getAdapterVehiculoTraslado() {
        return adapterVehiculoTraslado;
    }

    public void setAdapterVehiculoTraslado(VehiculoTrasladoListAdapter adapterVehiculoTraslado) {
        this.adapterVehiculoTraslado = adapterVehiculoTraslado;
    }

    public VehiculoTraslado getVehiculoTrasladoFilter() {
        return vehiculoTrasladoFilter;
    }

    public void setVehiculoTrasladoFilter(VehiculoTraslado vehiculoTrasladoFilter) {
        this.vehiculoTrasladoFilter = vehiculoTrasladoFilter;
    }

    
    /**
     * Método para filtrar vehiculos.
     */
    public void buscar() {
        adapterVehiculoTraslado.setFirstResult(0);
        this.adapterVehiculoTraslado = adapterVehiculoTraslado.fillData(vehiculoTrasladoFilter);
    }
    
    /**
     * Método para limpiar filtros
     */
    public void clear() {
        this.vehiculoTrasladoFilter = new VehiculoTraslado();
        buscar();
    }
    
    
    /**
     * Método para redireccionar a la página de edición
     * @param id
     * @return 
     */
    public String editUrl(Integer id) {
        return String.format("vehiculo-traslado-edit?faces-redirect=true&id=%d", id);
    }
    
    
    /**
     * Método para cambiar el estado del vehículo
     *
     * @param vt
     */
    public void onChangeStateConfig(VehiculoTraslado vt) {

        BodyResponse response = new BodyResponse();
        
        if (vt.getActivo() == null) {
            vt.setActivo("S");
        } else {
            switch (vt.getActivo()) {
                case "S":
                    vt.setActivo("N");
                    break;
                case "N":
                    vt.setActivo("S");
                    break;
                default:
                    break;
            }
        }
        
        response = vehiculoTrasladoService.editVehiculoTraslado(vt);

        if (response != null) {
            buscar();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio correcto!"));
        }
    }
    
    @PostConstruct()
    public void init(){
        this.adapterVehiculoTraslado = new VehiculoTrasladoListAdapter();
        this.vehiculoTrasladoFilter = new VehiculoTraslado();
        this.vehiculoTrasladoService = new VehiculoTrasladoService();
        buscar();
    }
}
