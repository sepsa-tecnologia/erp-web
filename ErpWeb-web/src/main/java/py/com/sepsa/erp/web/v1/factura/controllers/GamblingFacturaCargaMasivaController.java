package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import org.primefaces.model.UploadedFile;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Archivo;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.proceso.remote.LoteService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 *
 * @author Antonella Lucero
 */
@ViewScoped
@Named("gamblingMasiva")
public class GamblingFacturaCargaMasivaController implements Serializable {
    /**
     * Archivo
     */
    private UploadedFile file;
    private UploadedFile fileClient;
    
    private String email;
    
    private LoteService serviceLote;
    
    @Inject
    private SessionData session;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFileClient() {
        return fileClient;
    }

    public void setFileClient(UploadedFile fileClient) {
        this.fileClient = fileClient;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LoteService getServiceLote() {
        return serviceLote;
    }

    public void setServiceLote(LoteService serviceLote) {
        this.serviceLote = serviceLote;
    }
    
    
    /**
     * Método para subir y enviar el archivo
     *
     * @throws IOException
     */
    public void upload() throws IOException {
        if (validarArchivos()) {
            Archivo archivoCreate = new Archivo();
            archivoCreate.setNombreArchivo(file.getFileName());
            String contentBase64 = Base64.getEncoder().encodeToString(file.getContents());
            archivoCreate.setContenido(contentBase64);
            archivoCreate.setCodigo("FACTURAS");

            Archivo archivoCreate2 = new Archivo();
            archivoCreate2.setNombreArchivo(fileClient.getFileName());
            String contentBase64__ = Base64.getEncoder().encodeToString(fileClient.getContents());
            archivoCreate2.setContenido(contentBase64__);
            archivoCreate2.setCodigo("CLIENTES");

            ArrayList<Archivo> archivos = new ArrayList<>(Arrays.asList(archivoCreate, archivoCreate2));
            Lote loteCreate = new Lote();
            loteCreate.setIdEmpresa(session.getUser().getIdEmpresa());
            loteCreate.setFechaInsercion(Date.from(Instant.now()));
            loteCreate.setNombreArchivo(file.getFileName());
            loteCreate.setEmail(email);
            loteCreate.setIdUsuario(session.getUser().getId());
            loteCreate.setArchivos(archivos);
            loteCreate.setCodigoTipoLote("CARGA_MASIVA_FACTURAS_GAMBLING");
            BodyResponse<Lote> respuesta = serviceLote.createLote(loteCreate);
            if (respuesta.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se han enviado los archivos correctamente!"));
            }

        }
    }
        
    public boolean validarArchivos(){
        boolean valid = true;
        
        if (file == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de facturas con el formato correcto: (xlsx)"));
        }
        if (fileClient == null) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el archivo de los clientes con el formato correcto: (xlsx)"));
        }
        if (email == null || email.trim().isEmpty()) {
            valid = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Favor seleccionar el correo para enviar los resultados"));
        }else{
            if(!validarFormatoCorreo(email)){
                valid = false;
            }
        }
        
        return valid;
    }
    
    public boolean validarFormatoCorreo(String email) {
        boolean addEmail = false;
         
        email = email.trim().replace(" ", "");
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z]{2,})?$");

        // El email a validar
        String correo = email;

        Matcher matcher = pattern.matcher(correo);

        if (matcher.find() == true) {
            addEmail = true;

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            String msg = String.format("%s no es un email válido", email);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        return addEmail;
    }
    
    /**
     * Ejecuta la conversión del archivo XLSX a CSV.
     *
     * @param file
     * @param fileClient
     * @return 
     * @throws IOException Si ocurre un error durante la conversión.
     */
    public String convert(UploadedFile file, UploadedFile fileClient) throws IOException {
        String timbrado = obtenerConfiguracionGeneral("TIMBRADO_FACTURA_MASIVA");
        if (timbrado == null || timbrado.trim().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La configuración para el timbrado de carga de factura masiva es inexistente"));
            return null;
        }

        List<Cliente> clientes = getListClientes(fileClient);
        if (clientes == null || clientes.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra ningún cliente valido en el archivo indicado"));
            return null;
        }
        try (Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(file.getContents())); StringWriter writer = new StringWriter()) {
            int numSheets = workbook.getNumberOfSheets();
            for (int sheetNumber = 0; sheetNumber < numSheets; sheetNumber++) {

                Sheet sheet = workbook.getSheetAt(sheetNumber);

                int rowIndex = 0;
                int linea = 1; // Contador de línea
                for (Row row : sheet) {
                    if (rowIndex < 1) { // Omitir las 2 primeras filas
                        rowIndex++;
                        continue;
                    }

                    String nro = getCellValueAsString(row.getCell(1)).trim();
                    while (nro.length() != 7) {
                        nro = "0" + nro;
                    }
                    if (getCellValueAsString(row.getCell(29)) != null && !getCellValueAsString(row.getCell(29)).trim().isEmpty()) {
                        String sucursalCode = getSucursales(Integer.parseInt(getCellValueAsString(row.getCell(29))), timbrado);
                        if (sucursalCode != null && !sucursalCode.trim().isEmpty()) {
                            String nroFactura = sucursalCode + nro;
                            String fecha = getCellValueAsString(row.getCell(3));
                            String codigoIsoMoneda = "PYG";
                            int idcli = Integer.parseInt(getCellValueAsString(row.getCell(12)));
                            String rucCi = "";
                            for (Cliente cli : clientes) {
                                if (idcli == cli.getIdCliente()) {
                                    rucCi = cli.getNroDocumento();
                                    break;
                                }
                            }
                            String razonSocial = getCellValueAsString(row.getCell(31)).replace(",", "");
                            String codSeguridad = generarNumeroRandom();
                            String tipoCambio = "";
                            String tipoFactura = "CONTADO";
                            String tipoCredito = "";
                            String diasCredito = "";
                            String cantidadCuotas = "";
                            String codigoProducto = "001";
                            String porcentajeImpuesto = "10";
                            String porcentajeGravada = "100";
                            String precioUnitarioConIva = getCellValueAsString(row.getCell(7));
                            String descuentoUnitario = calculoDescuento(row.getCell(7), row.getCell(8));
                            String cantidad = getCellValueAsString(row.getCell(11));
                            String descripcion = "Carton de sorteo";

                            // Primera línea
                            String linea1 = 1 + "," + timbrado + "," + nroFactura + "," + fecha + "," + codigoIsoMoneda + "," + rucCi + "," + razonSocial + "," + codSeguridad + "," + tipoCambio + "," + tipoFactura + "," + tipoCredito + "," + diasCredito + "," + cantidadCuotas;
                            writer.write(linea1 + "\n");

                            // Segunda línea
                            String linea2 = 2 + "," + timbrado + "," + nroFactura + "," + codigoProducto + "," + porcentajeImpuesto + "," + porcentajeGravada + "," + precioUnitarioConIva + "," + descuentoUnitario + "," + cantidad + "," + descripcion;
                            writer.write(linea2 + "\n");
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra ningún talonario registrado para la sucursal (su_id) = " + getCellValueAsString(row.getCell(29))));
                            return null;
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No todos los registros cuentan con el número de sucursal (su_id)"));
                        return null;
                    }

                    linea++;
                    rowIndex++;
                }
            }

            return writer.toString();
        } catch (IOException e) {
            return null;
        }
    }
    /**
     * obtiene los clientes id y nroDocumento
     *
     * @param fileClient
     * @return 
     * @throws java.io.IOException */
    public List<Cliente> getListClientes(UploadedFile fileClient) throws IOException{
        List<Cliente> clientes = new ArrayList<>();
        try (Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(fileClient.getContents())); StringWriter writer = new StringWriter()) {
            Sheet sheet = workbook.getSheetAt(0);
            int rowIndex = 0;
            for (Row row : sheet) {
                if (rowIndex < 1) {
                    rowIndex++;
                    continue;
                }
                try{
                    Cliente c = new Cliente();
                    if (getCellValueAsString(row.getCell(0)) != null && !getCellValueAsString(row.getCell(0)).trim().isEmpty() && getCellValueAsString(row.getCell(4)) != null && !getCellValueAsString(row.getCell(4)).trim().isEmpty()) {
                        c.setIdCliente(Integer.parseInt(getCellValueAsString(row.getCell(0))));
                        c.setNroDocumento(getCellValueAsString(row.getCell(4)));
                        if (!clientes.contains(c)) {
                            clientes.add(c);
                        }
                    }
                }catch (Exception e){
                }
                rowIndex++;
            }
            return clientes;
        } catch (Exception e) {
            WebLogger.get().fatal(e);
            return null;
        }
    }

    private static String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    return sdf.format(cell.getDateCellValue());
                } else {
                    return String.valueOf((long) cell.getNumericCellValue());
                }
            default:
                return "";
        }
    }

    private String getSucursales(int idSuc, String timbrado) {
        String sucursales = "";

        Talonario t = new Talonario();
        t.setIdEmpresa(session.getUser().getIdEmpresa());
        t.setActivo("S");
        t.setTimbrado(timbrado);
        t.setIdTipoDocumento(1);
        t.setCodigoInterno(idSuc);
        TalonarioAdapter talonarioAdapter = new TalonarioAdapter();
        talonarioAdapter = talonarioAdapter.fillData(t);
        if(!talonarioAdapter.getData().isEmpty()){
            Talonario talonario = talonarioAdapter.getData().get(0);
            sucursales = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta()+ "-"; 
        }

        return sucursales;
    }
    
    /**
     * Método para obtener la configuración
     * @param codigo
     * @return 
     */
    public String obtenerConfiguracionGeneral(String codigo) {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setActivo("S");
        confVal.setIdEmpresa(session.getUser().getIdEmpresa());
        a = a.fillData(confVal);

        if (a.getData() != null && !a.getData().isEmpty()) {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }

    private static String generarNumeroRandom() {
        Random random = new Random();
        return String.format("%09d", random.nextInt(1000000000));
    }

    private static String calculoDescuento(Cell precioCell, Cell descuentoPorcentajeCell) {
        if (precioCell == null || descuentoPorcentajeCell == null) {
            return "0";
        }
        double price = precioCell.getNumericCellValue();
        double descuentoPorcentaje = descuentoPorcentajeCell.getNumericCellValue();
        return String.valueOf((long) (price * descuentoPorcentaje / 100));
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.file = null;
        this.fileClient = null;
        this.email = null;
        this.serviceLote = new LoteService();
    }
}
