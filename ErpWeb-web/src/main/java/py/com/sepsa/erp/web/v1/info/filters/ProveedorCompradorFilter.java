package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ProveedorComprador;

/**
 * Filtro utilizado para el servicio de proveedor comprador
 *
 * @author Romina E. Núñez Rojas
 */
public class ProveedorCompradorFilter extends Filter {

    /**
     * Agrega el filtro de identificador del comprador
     *
     * @param idComprador
     * @return
     */
    public ProveedorCompradorFilter idComprador(Integer idComprador) {
        if (idComprador != null) {
            params.put("idComprador", idComprador);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del proveedor
     *
     * @param idProveedor
     * @return
     */
    public ProveedorCompradorFilter idProveedor(Integer idProveedor) {
        if (idProveedor != null) {
            params.put("idProveedor", idProveedor);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del producto
     *
     * @param idProducto
     * @return
     */
    public ProveedorCompradorFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de estado
     *
     * @param estado
     * @return
     */
    public ProveedorCompradorFilter estado(String estado) {
        if (estado != null) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro de fecha inserción
     *
     * @param fechaInsercion
     * @return
     */
    public ProveedorCompradorFilter fechaInsercion(String fechaInsercion) {
        if (fechaInsercion != null) {
            params.put("fechaInsercion", fechaInsercion);
        }
        return this;
    }

    /**
     * Agrega el filtro de fecha modificación
     *
     * @param estado
     * @return
     */
    public ProveedorCompradorFilter fechaModificacion(String fechaModificacion) {
        if (fechaModificacion != null) {
            params.put("fechaModificacion", fechaModificacion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param proveedorComprador datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ProveedorComprador proveedorComprador, Integer page, Integer pageSize) {
        ProveedorCompradorFilter filter = new ProveedorCompradorFilter();

        filter
                .idComprador(proveedorComprador.getIdComprador())
                .idProveedor(proveedorComprador.getIdProveedor())
                .idProducto(proveedorComprador.getIdProducto())
                .estado(proveedorComprador.getEstado())
                .fechaInsercion(proveedorComprador.getFechaInsercion())
                .fechaModificacion(proveedorComprador.getFechaModificacion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ProveedorCompradorFilter
     */
    public ProveedorCompradorFilter() {
        super();
    }

}
