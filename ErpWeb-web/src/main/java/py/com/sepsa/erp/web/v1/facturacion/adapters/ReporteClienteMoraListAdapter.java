
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import java.util.Calendar;
import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.ReporteClienteMora;
import py.com.sepsa.erp.web.v1.facturacion.remote.ReporteClienteMoraService;

/**
 * Adaptador de la lista de reporte de mora
 * @author Cristina Insfrán
 */
public class ReporteClienteMoraListAdapter extends DataListAdapter<ReporteClienteMora>{

     /**
     * Cliente para los servicios de reportes de clientes
     */
    private final ReporteClienteMoraService reporteClient;
    

    
    @Override
    public ReporteClienteMoraListAdapter fillData(ReporteClienteMora searchData) {
        return reporteClient.getReporteList(searchData,getFirstResult(), getPageSize());
    }
    
    /**
     * Constructor de ReporteClienteMoraListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ReporteClienteMoraListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        Calendar fecha = Calendar.getInstance();
        this.reporteClient = new ReporteClienteMoraService();
        this.filter = new ReporteClienteMora( fecha.get(Calendar.MONTH),  fecha.get(Calendar.YEAR));
        
    }

    /**
     * Constructor de ReporteClienteMoraListAdapter
     */
    public ReporteClienteMoraListAdapter() {
        super();
        Calendar fecha = Calendar.getInstance();
        this.reporteClient = new ReporteClienteMoraService();
        this.filter = new ReporteClienteMora(fecha.get(Calendar.MONTH),  fecha.get(Calendar.YEAR));
    }
   
}
