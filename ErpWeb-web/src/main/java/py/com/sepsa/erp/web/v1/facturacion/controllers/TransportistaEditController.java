/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.factura.pojos.VehiculoTraslado;
import py.com.sepsa.erp.web.v1.facturacion.remote.TransportistaService;
import py.com.sepsa.erp.web.v1.facturacion.remote.VehiculoTrasladoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para editar vehiculo de traslado
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("transportistaEdit")

public class TransportistaEditController implements Serializable {

    /**
     * Cliente para el servicio de transportista.
     */
    private final TransportistaService service;
    /**
     * POJO del producto
     */
    private Transportista transportista;

    
    public Transportista getTransportista() {
        return transportista;
    }

    public void setTransportista(Transportista transportista) {
        this.transportista = transportista;
    }

    /**
     * Método que valida un string a partir de un patrón regex
     * @param cadena
     * @param patronRegex
     * @return 
     */
    public boolean validadorDePatron(String cadena, String patronRegex) {
        try {
            Pattern pattern = Pattern.compile(patronRegex);

            Matcher matcher = pattern.matcher(cadena);

            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
    
    public void formatearCampos() {
        if (transportista != null) {
            if (transportista.getNroDocumento() != null) {
                transportista.setNroDocumento(transportista.getNroDocumento().trim().replace(" ", "").replace(".", ""));
            }
            
            if (transportista.getNroDocumentoChofer() != null) { 
                transportista.setNroDocumentoChofer(transportista.getNroDocumentoChofer().trim().replace(" ", "").replace(".", ""));
            }
            
            if (transportista.getRuc() != null) {   
                String ruc =  transportista.getRuc().trim().replace(" ", "").replace(".", "");
                
                //  Se extrael el digito verificador del ruc si fue ingresado.       
                if (ruc.contains("-")){
                    String[] nroRuc = ruc.split("-");
                    ruc = nroRuc[0];
                }
                transportista.setRuc(ruc);
            }

        }
    }
    
    /**
     * Método para validar inputs
     * @return 
     */
     public boolean validarCampos() {
        formatearCampos();
        boolean saveNr = true;
        if (transportista != null) {
            if (transportista.getIdNaturalezaTransportista() == null) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar el tipo de contribuyente!"));
            }
            
            if (transportista.getIdNaturalezaTransportista() == 1) {
                if (!transportista.getRuc().trim().isEmpty() && transportista.getRuc().trim().length() >= 3) {
                    //Validar que el RUC solo tenga dígitos
                    if (!validadorDePatron(transportista.getRuc(), "^[0-9]+$")) {
                        saveNr = false;
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                    }
                } else {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un RUC de transportista valido!"));
                }
                if (transportista.getVerificadorRuc() != null && (transportista.getVerificadorRuc()< 0 || transportista.getVerificadorRuc()> 9 )) {
                    saveNr = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un dígito verificador de RUC valido!"));
                }
            }

            if (transportista.getRazonSocial().trim().equalsIgnoreCase("") || transportista.getRazonSocial().length() < 4) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre/razon social de transportista válido!"));
            }

            if (transportista.getNombreCompleto().trim().equalsIgnoreCase("") || transportista.getNombreCompleto().length() < 4) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un nombre de chofer válido!"));
            }

            if (transportista.getDireccionChofer().trim().equalsIgnoreCase("") || transportista.getDireccionChofer().length() < 1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección del chofer!"));
            }

            if (transportista.getDomicilioFiscal().trim().equalsIgnoreCase("") || transportista.getDomicilioFiscal().length() < 1) {
                saveNr = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el domicilio fiscal!"));
            }

        }

        return saveNr;
    }

    //</editor-fold>
    /**
     * Método para editar Producto
     */
    public void edit() {
        boolean create = validarCampos();
        
        if (create){
            BodyResponse<Transportista> respuestaTransportista = service.editTransportista(transportista);
            if (respuestaTransportista.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Transportista editado correctamente!"));
            } else {
                for (Mensaje mensaje : respuestaTransportista.getStatus().getMensajes()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
                }
            }
        }
    }

   
    /**
     * Inicializa los datos
     */
    private void init(int id) {
        try {
            this.transportista = service.get(id);
        } catch (Exception e) {
            System.err.print(e);
        }
    }

    /**
     * Constructor
     */
    public TransportistaEditController() {
        this.service = new TransportistaService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
}
