
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefonoNotificacion;
import py.com.sepsa.erp.web.v1.info.remote.ContactoTelefonoNotificacionService;

/**
 * Adaptador de la lista de contacto telefono notificación
 * @author Romina Núñez
 */
public class ContactoTelefonoNotificacionAdapter extends DataListAdapter<ContactoTelefonoNotificacion> {
    
    /**
     * Cliente para el servicio de contacto email
     */
    private final ContactoTelefonoNotificacionService contactoTelefonoNotificacionService;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ContactoTelefonoNotificacionAdapter fillData(ContactoTelefonoNotificacion searchData) {
     
        return contactoTelefonoNotificacionService.getContactoTelefonoNotificacionList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ContactoTelefonoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ContactoTelefonoNotificacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.contactoTelefonoNotificacionService = new ContactoTelefonoNotificacionService();
    }

    /**
     * Constructor de ContactoTelefonoAdapter
     */
    public ContactoTelefonoNotificacionAdapter() {
        super();
        this.contactoTelefonoNotificacionService = new ContactoTelefonoNotificacionService();
    }
}
