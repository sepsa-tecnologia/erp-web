/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Pojo para nota credito compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotaCreditoCompra {

    /**
     * Identificador del la factura
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador de persona/cliente
     */
    private Integer idPersona;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de motivo emision
     */
    private Integer idMotivoEmision;
    /**
     * Identificador de motivo Emision Interno
     */
    private Integer idMotivoEmisionInterno;
    /**
     * idFacturaCompra
     */
    private Integer idFacturaCompra;
    /**
     * fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * fecha
     */
    private Date fecha;
    /**
     * Fecha
     */
    private Date fechaDesde;
    /**
     * Fecha
     */
    private Date fechaHasta;
    /**
     * Nro Timbrado
     */
    private String nroTimbrado;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Nro de nota de credito
     */
    private String nroNotaCredito;
    /**
     * Razón Social
     */
    private String razonSocial;
    /**
     * RUC del cliente
     */
    private String ruc;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * Digital
     */
    private String digital;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total nota de credito
     */
    private BigDecimal montoTotalNotaCredito;
    /**
     * Empresa
     */
    private Empresa empresa;
    /**
     * Listado de detalles de facturaCompra
     */
    private List<NotaCreditoCompraDetalles> notaCreditoCompraDetalles;

    /**
     * Identificador del tipo de dato
     */
    private Integer idMotivoAnulacion;
    /**
     * Motivo Anulación
     */
    private String observacionAnulacion;
    /**
     * Motivo Emision
     */
    private MotivoEmision motivoEmision;
    /**
     * Motivo Emision Interno
     */
    private MotivoEmisionInterno motivoEmisionInterno;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<NotaCreditoCompraDetalles> getNotaCreditoCompraDetalles() {
        return notaCreditoCompraDetalles;
    }

    public void setNotaCreditoCompraDetalles(List<NotaCreditoCompraDetalles> notaCreditoCompraDetalles) {
        this.notaCreditoCompraDetalles = notaCreditoCompraDetalles;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public MotivoEmisionInterno getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    //</editor-fold>
    public NotaCreditoCompra() {
    }
}
