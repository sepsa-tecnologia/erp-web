package py.com.sepsa.erp.web.v1.factura.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.facturacion.pojos.ConceptoCobro;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * POJO para detalle del cobro
 *
 * @author Cristina Insfrán
 */
public class CobroDetallePojo implements Serializable {

    /**
     * Identificador del cobro
     */
    private Integer id;
    /**
     * Identificador de cobro
     */
    private Integer idCobro;
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    /**
     * Nro de factura
     */
    private String nroFactura;  
    /**
     * Número de recibo
     */
    private String nroRecibo;
    /**
     * Fecha
     */
    private Date fechaFactura;
    /**
     * Identificador de tipo de cobro
     */
    private Integer idTipoCobro;
    /**
     * Tipo de cobro
     */
    private String tipoCobro;
    /**
     * Codigo tipo cobro
     */
    private String codigoTipoCobro;
    /**
     * Monto de cobro
     */
    private BigDecimal montoCobro;
    /**
     * id Estado
     */
    private Integer idEstado;
    /**
     * Estado
     */
    private String estado;
    /**
     * Codigo Concepto Cobro
     */
    private String codigoConceptoCobro;
    /**
     * Código estado
     */
    private String codigoEstado;
    /**
     * id concepto Cobro
     */
    private Integer idConceptoCobro;
    /**
     * Parámetro conceptoCobro
     */
    private String conceptoCobro;
    /**
     * Dato para filtro de listado
     */
    private Boolean listadoPojo;
    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getIdCobro() {
            return idCobro;
        }

        public void setIdCobro(Integer idCobro) {
            this.idCobro = idCobro;
        }

        public Integer getNroLinea() {
            return nroLinea;
        }

        public void setNroLinea(Integer nroLinea) {
            this.nroLinea = nroLinea;
        }

        public Integer getIdFactura() {
            return idFactura;
        }

        public void setIdFactura(Integer idFactura) {
            this.idFactura = idFactura;
        }

        public String getNroFactura() {
            return nroFactura;
        }

        public void setNroFactura(String nroFactura) {
            this.nroFactura = nroFactura;
        }

        public String getNroRecibo() {
            return nroRecibo;
        }

        public void setNroRecibo(String nroRecibo) {
            this.nroRecibo = nroRecibo;
        }

        public Date getFechaFactura() {
            return fechaFactura;
        }

        public void setFechaFactura(Date fechaFactura) {
            this.fechaFactura = fechaFactura;
        }

        public Integer getIdTipoCobro() {
            return idTipoCobro;
        }

        public void setIdTipoCobro(Integer idTipoCobro) {
            this.idTipoCobro = idTipoCobro;
        }

        public String getTipoCobro() {
            return tipoCobro;
        }

        public void setTipoCobro(String tipoCobro) {
            this.tipoCobro = tipoCobro;
        }

        public String getCodigoTipoCobro() {
            return codigoTipoCobro;
        }

        public void setCodigoTipoCobro(String codigoTipoCobro) {
            this.codigoTipoCobro = codigoTipoCobro;
        }

        public BigDecimal getMontoCobro() {
            return montoCobro;
        }

        public void setMontoCobro(BigDecimal montoCobro) {
            this.montoCobro = montoCobro;
        }

        public Integer getIdEstado() {
            return idEstado;
        }

        public void setIdEstado(Integer idEstado) {
            this.idEstado = idEstado;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public String getCodigoConceptoCobro() {
            return codigoConceptoCobro;
        }

        public void setCodigoConceptoCobro(String codigoConceptoCobro) {
            this.codigoConceptoCobro = codigoConceptoCobro;
        }

        public String getCodigoEstado() {
            return codigoEstado;
        }

        public void setCodigoEstado(String codigoEstado) {
            this.codigoEstado = codigoEstado;
        }

        public Integer getIdConceptoCobro() {
            return idConceptoCobro;
        }

        public void setIdConceptoCobro(Integer idConceptoCobro) {
            this.idConceptoCobro = idConceptoCobro;
        }

        public String getConceptoCobro() {
            return conceptoCobro;
        }

        public void setConceptoCobro(String conceptoCobro) {
            this.conceptoCobro = conceptoCobro;
        }   

        public Boolean getListadoPojo() {
            return listadoPojo;
        }

        public void setListadoPojo(Boolean listadoPojo) {
            this.listadoPojo = listadoPojo;
        }
    //</editor-fold>
    /**
     * Constructor
     */
    public CobroDetallePojo() {

    }

    public CobroDetallePojo(Integer id, Integer idCobro, Integer nroLinea, Integer idFactura,
            String nroFactura, String nroRecibo, Date fechaFactura, Integer idTipoCobro,
            String tipoCobro, String codigoTipoCobro, BigDecimal montoCobro, Integer idEstado,
            String estado, String codigoConceptoCobro, String codigoEstado, Integer idConceptoCobro, String conceptoCobro, Boolean listadoPojo) {
        this.id = id;
        this.idCobro = idCobro;
        this.nroLinea = nroLinea;
        this.idFactura = idFactura;
        this.nroFactura = nroFactura;
        this.nroRecibo = nroRecibo;
        this.fechaFactura = fechaFactura;
        this.idTipoCobro = idTipoCobro;
        this.tipoCobro = tipoCobro;
        this.codigoTipoCobro = codigoTipoCobro;
        this.montoCobro = montoCobro;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoConceptoCobro = codigoConceptoCobro;
        this.codigoEstado = codigoEstado;
        this.idConceptoCobro = idConceptoCobro;
        this.conceptoCobro = conceptoCobro;
        this.listadoPojo = listadoPojo;
    }
}
