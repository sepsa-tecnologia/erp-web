/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;
import java.io.IOException;
import java.util.Properties;
import py.com.sepsa.erp.web.v1.logger.WebLogger;


/**
 * Utilidades para la lectura de parametros del sistema
 * @author Cristina Insfrán
 */
public class ParamsReader {
     /**
     * Path del archivo de parametros
     */
    private static final String PARAMS_PATH = "META-INF/data.properties";
    /**
     * Objeto de parametros del sistema
     */
    public static final Params PARAMS = new Params();

    /**
     * Constructor de ParameterUtils
     * @param parameterPath Path del archivo de parametros
     */
    /*public static void setPARAMS_PATH(String parameterPath) {
        ParamsReader.PARAMS_PATH = parameterPath;
    }*/

    /**
     * Inicialización de parametros del sistema
     */
    static {
        Properties properties = new Properties();
        try {
            WebLogger.get().info("Inicializando parametros del sistema");
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(PARAMS_PATH));
            
            PARAMS.setMailAlert(String.valueOf(properties.get("mail_alert")));
            WebLogger.get().info("mailAlert: " + String.valueOf(properties.get("mail_alert")));
            
            PARAMS.setMailPass(String.valueOf(properties.get("mail_pass")));
            WebLogger.get().info("mailPass: " + String.valueOf(properties.get("mail_pass")));
            
            PARAMS.setMailServer(String.valueOf(properties.get("mail_server")));
            WebLogger.get().info("mailServer: " + String.valueOf(properties.get("mail_server")));
            
            PARAMS.setMailPort(Integer.parseInt(String.valueOf(properties.get("mail_port"))));
            WebLogger.get().info("mailPort: " + String.valueOf(properties.get("mail_port")));
            
            PARAMS.setMailOper(String.valueOf(properties.get("mail_oper")));
            WebLogger.get().info("mailOper: " + String.valueOf(properties.get("mail_oper")));
            
            PARAMS.setMailTI(String.valueOf(properties.get("mail_tic")));
            WebLogger.get().info("mailOper: " + String.valueOf(properties.get("mail_tic")));
            
            PARAMS.setSmbUser(String.valueOf(properties.get("sambaUser")));
            WebLogger.get().info("sambaUser: " + String.valueOf(properties.get("sambaUser")));
           
            PARAMS.setSmbUserPass(String.valueOf(properties.get("sambaUserPass")));
            
        } catch (IOException ex) {
            WebLogger.get().error("No se encuentra el archivo de parametros del sistema");
            ex.printStackTrace();
        } catch (Exception ex) {
            WebLogger.get().error("Se ha producido el siguiente error: " + ex);
            ex.printStackTrace();
        }
    }
}
