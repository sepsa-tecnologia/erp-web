package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Cargo;
import py.com.sepsa.erp.web.v1.info.remote.CargoService;

/**
 * Adaptador para la lista de cargo
 *
 * @author Sergio D. Riveros Vazquez
 */
public class CargoListAdapter extends DataListAdapter<Cargo> {

    /**
     * Cliente para el servicio de tipo contacto
     */
    private final CargoService cargoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CargoListAdapter fillData(Cargo searchData) {

        return cargoClient.getCargoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de CargoListAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CargoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.cargoClient = new CargoService();
    }

    /**
     * Constructor de CargoListAdapter
     */
    public CargoListAdapter() {
        super();
        this.cargoClient = new CargoService();
    }
}
