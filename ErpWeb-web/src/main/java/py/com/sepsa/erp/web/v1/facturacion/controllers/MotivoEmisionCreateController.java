/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para la vista crear motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionCreate")
public class MotivoEmisionCreateController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final MotivoEmisionService service;
    /**
     * POJO del motivoEmision
     */
    private MotivoEmision motivoEmision;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public MotivoEmision getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(MotivoEmision motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {

        BodyResponse<MotivoEmision> respuestaTal = service.setMotivoEmision(motivoEmision);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, 
                            "Info", "Motivo de Emision creado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivoEmision = new MotivoEmision();
    }

    /**
     * Constructor
     */
    public MotivoEmisionCreateController() {
        init();
        this.service = new MotivoEmisionService();
    }

}
