package py.com.sepsa.erp.web.v1.info.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * POJO de Tipo Notificación
 *
 * @author Romina Núñez
 */
public class TipoNotificacion {

    /**
     * Identificador 
     */
    private Integer id;
    /**
     * Descripción del tipo de notificación
     */
    private String descripcion;
    /**
     * Código del tipo de notificación
     */
    private String codigo;
    /**
     * Bandera email
     */
    private String email;
    /**
     * Bandera teléfono
     */
    private String telefono;
    
    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">

    public Integer getId() {
        return id;

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

//</editor-fold>
    /**
     * Constructor
     */
    public TipoNotificacion() {
    }

    
    /**
     * Constructor con parámetros
     * @param id
     * @param descripcion
     * @param codigo
     * @param email
     * @param telefono 
     */
    public TipoNotificacion(Integer id, String descripcion, String codigo, String email, String telefono) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.email = email;
        this.telefono = telefono;
    }

    

}
