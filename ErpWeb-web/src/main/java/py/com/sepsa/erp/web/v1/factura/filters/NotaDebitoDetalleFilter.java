/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetalle;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoDetalleFilter extends Filter {

    /**
     * Agrega el filtro de identificador de idNotaDebito
     *
     * @param idNotaDebito
     * @return
     */
    public NotaDebitoDetalleFilter idNotaDebito(Integer idNotaDebito) {
        if (idNotaDebito != null) {
            params.put("idNotaDebito", idNotaDebito);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de idFactura
     *
     * @param idFactura
     * @return
     */
    public NotaDebitoDetalleFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de nroLinea
     *
     * @param nroLinea
     * @return
     */
    public NotaDebitoDetalleFilter nroLinea(Integer nroLinea) {
        if (nroLinea != null) {
            params.put("nroLinea", nroLinea);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de porcentajeIva
     *
     * @param porcentajeIva
     * @return
     */
    public NotaDebitoDetalleFilter porcentajeIva(Integer porcentajeIva) {
        if (porcentajeIva != null) {
            params.put("porcentajeIva", porcentajeIva);
        }
        return this;
    }

    /**
     * Agrega el filtro para montoIva
     *
     * @param montoIva
     * @return
     */
    public NotaDebitoDetalleFilter montoIva(BigDecimal montoIva) {
        if (montoIva != null) {
            params.put("montoIva", montoIva.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoImponible
     *
     * @param montoImponible
     * @return
     */
    public NotaDebitoDetalleFilter montoImponible(BigDecimal montoImponible) {
        if (montoImponible != null) {
            params.put("montoImponible", montoImponible.toPlainString());
        }
        return this;
    }

    /**
     * Agrega el filtro para montoTotal
     *
     * @param montoTotal
     * @return
     */
    public NotaDebitoDetalleFilter montoTotal(BigDecimal montoTotal) {
        if (montoTotal != null) {
            params.put("montoTotal", montoTotal.toPlainString());
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaDebitoDetalleFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agrega el filtro para anhoMes
     *
     * @param anhoMes
     * @return
     */
    public NotaDebitoDetalleFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param nd
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaDebitoDetalle nd, Integer page, Integer pageSize) {
        NotaDebitoDetalleFilter filter = new NotaDebitoDetalleFilter();

        filter
                .idNotaDebito(nd.getIdNotaDebito())
                .idFactura(nd.getIdFactura())
                .nroLinea(nd.getNroLinea())
                .porcentajeIva(nd.getPorcentajeIva())
                .montoIva(nd.getMontoIva())
                .montoImponible(nd.getMontoImponible())
                .montoTotal(nd.getMontoTotal())
                .anhoMes(nd.getAnhoMes())
                .anulado(nd.getAnulado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaDebitoDetalleFilter
     */
    public NotaDebitoDetalleFilter() {
        super();
    }
}
