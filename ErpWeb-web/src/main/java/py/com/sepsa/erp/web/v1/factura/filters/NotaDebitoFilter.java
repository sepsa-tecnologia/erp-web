/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de nota
     *
     * @param id
     * @return
     */
    public NotaDebitoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public NotaDebitoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de nro nd
     *
     * @param nroNotaDebito
     * @return
     */
    public NotaDebitoFilter nroNotaDebito(String nroNotaDebito) {
        if (nroNotaDebito != null && !nroNotaDebito.trim().isEmpty()) {
            params.put("nroNotaDebito", nroNotaDebito);
        }
        return this;
    }

    /**
     * Agregar el filtro de anulado
     *
     * @param anulado
     * @return
     */
    public NotaDebitoFilter anulado(String anulado) {
        if (anulado != null && !anulado.trim().isEmpty()) {
            params.put("anulado", anulado);
        }
        return this;
    }

    /**
     * Agregar el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public NotaDebitoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de motivo emision interno
     *
     * @param idMotivoEmisionInterno
     * @return
     */
    public NotaDebitoFilter idMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        if (idMotivoEmisionInterno != null) {
            params.put("idMotivoEmisionInterno", idMotivoEmisionInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de factura
     *
     * @param idFactura
     * @return
     */
    public NotaDebitoFilter idFactura(Integer idFactura) {
        if (idFactura != null) {
            params.put("idFactura", idFactura);
        }
        return this;
    }
    
        /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public NotaDebitoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notaDebito datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(NotaDebito notaDebito, Integer page, Integer pageSize) {
        NotaDebitoFilter filter = new NotaDebitoFilter();

        filter
                .id(notaDebito.getId())
                .razonSocial(notaDebito.getRazonSocial())
                .fecha(notaDebito.getFecha())
                .nroNotaDebito(notaDebito.getNroNotaDebito())
                .anulado(notaDebito.getAnulado())
                .idMotivoEmisionInterno(notaDebito.getIdMotivoEmisionInterno())
                .idFactura(notaDebito.getIdFactura())
                .listadoPojo(notaDebito.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de NotaDebitoFilter
     */
    public NotaDebitoFilter() {
        super();
    }
}
