/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 *
 * @author Romina
 */
public class TipoEtiqueta {
    /**
     * Identificador del Tipo Etiqueta
     */
    private Integer id;
    /**
     * Código
     */
    private String codigo;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Configuración Aprobación
     */
    private Character configAprobacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getConfigAprobacion() {
        return configAprobacion;
    }

    public void setConfigAprobacion(Character configAprobacion) {
        this.configAprobacion = configAprobacion;
    }
    /**
     * Constructor de la clase
     */
    public TipoEtiqueta() {
    }

    /**
     * Constructor con parámetros
     * @param id
     * @param codigo
     * @param descripcion
     * @param configAprobacion 
     */
    public TipoEtiqueta(Integer id, String codigo, String descripcion, Character configAprobacion) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.configAprobacion = configAprobacion;
    }
    
    
}
