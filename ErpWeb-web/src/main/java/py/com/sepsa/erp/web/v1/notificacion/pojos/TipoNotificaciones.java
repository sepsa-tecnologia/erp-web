/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.pojos;

/**
 * Pojo de tipo notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoNotificaciones {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Codigo
     */
    private String code;
    /**
     * Descripcion
     */
    private String description;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //</editor-fold>

    /**
     * Contructor de la clase
     */
    public TipoNotificaciones() {

    }
}
