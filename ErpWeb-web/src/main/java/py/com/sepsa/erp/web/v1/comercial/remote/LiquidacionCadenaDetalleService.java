package py.com.sepsa.erp.web.v1.comercial.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionCadenaDetalleAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.LiquidacionCadenaDetalleFilter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionCadenaDetalle;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para el listado de detalle de liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionCadenaDetalleService extends APIErpFacturacion {

    /**
     * Obtiene la lista de detalle de liquidaciones por cadena.
     *
     * @param searchData
     * @param page
     * @param pageSize
     * @return
     */
    public LiquidacionCadenaDetalleAdapter getLiquidacionesCadenaDetalle(LiquidacionCadenaDetalle searchData,
            Integer page, Integer pageSize) {

        LiquidacionCadenaDetalleAdapter lista = new LiquidacionCadenaDetalleAdapter();

        Map params = LiquidacionCadenaDetalleFilter.build(searchData, page, pageSize);

        HttpURLConnection conn = GET(Resource.DETALLE.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    LiquidacionCadenaDetalleAdapter.class);

            lista = (LiquidacionCadenaDetalleAdapter) response.getPayload();

            conn.disconnect();
        }

        return lista;

    }

    /**
     * Constructor de LiquidacionPeriodoDetalleService.
     */
    public LiquidacionCadenaDetalleService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaFacturación
     */
    public enum Resource {

        //Servicios
        DETALLE("Servicio de liquidaciones por cadena", "liquidacion-detalle/cadena");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
