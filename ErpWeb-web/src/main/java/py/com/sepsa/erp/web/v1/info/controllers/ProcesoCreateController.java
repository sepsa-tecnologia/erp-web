/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProcesoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.FrecuenciaEjecucion;
import py.com.sepsa.erp.web.v1.info.pojos.Proceso;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProceso;
import py.com.sepsa.erp.web.v1.info.remote.ProcesoService;

/**
 * Controlador para crear procesos
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("procesoCreate")
public class ProcesoCreateController implements Serializable {

    /**
     * Cliente para el servicio de proceso.
     */
    private final ProcesoService service;
    /**
     * POJO del proceso
     */
    private Proceso proceso;
    /**
     * Adaptador de la lista de tipo procesos
     */
    private TipoProcesoAdapter tipoProcesoAdapter;
    /**
     * Datos del cliente
     */
    private TipoProceso tipoProceso;
    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;
    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Proceso getProceso() {
        return proceso;
    }

    public void setProceso(Proceso proceso) {
        this.proceso = proceso;
    }

    public TipoProcesoAdapter getTipoProcesoAdapter() {
        return tipoProcesoAdapter;
    }

    public void setTipoProcesoAdapter(TipoProcesoAdapter tipoProcesoAdapter) {
        this.tipoProcesoAdapter = tipoProcesoAdapter;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(TipoProceso tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    //</editor-fold>
    /**
     * Método para crear Proceso
     */
    public void create() {
        BodyResponse<Proceso> respuestaTal = service.setProceso(this.proceso);
        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                    "Proceso creado correctamente!"));
        }
    }

    /**
     * Método para obtener las tipoProcesos
     */
    public void getTipoProcesos() {
        this.setTipoProcesoAdapter(getTipoProcesoAdapter().fillData(getTipoProceso()));
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        this.proceso.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.empresaAutocomplete = new Empresa();
        this.empresaAdapter = new EmpresaAdapter();
        this.proceso = new Proceso();
        this.tipoProcesoAdapter = new TipoProcesoAdapter();
        this.tipoProceso = new TipoProceso();
        FrecuenciaEjecucion frecuenciaEjecucion = new FrecuenciaEjecucion();
        proceso.setFrecuenciaEjecucion(frecuenciaEjecucion);
        getTipoProcesos();
    }

    /**
     * Constructor
     */
    public ProcesoCreateController() {
        init();
        this.service = new ProcesoService();
    }

}
