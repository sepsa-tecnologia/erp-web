
package py.com.sepsa.erp.web.v1.test.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.test.adapters.TestListAdapter;
import py.com.sepsa.erp.web.v1.test.filters.TestFilter;
import py.com.sepsa.erp.web.v1.test.pojos.Test;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para los servicios de TEST
 * @author Daniel F. Escauriza Arza
 */
public class TestServiceClient extends APIErpCore {
    
    /**
     * Método para obtener un objeto test por identificador
     * @param id Identificador del test
     * @return Test
     */
    public Test getTest(Integer id) {
        
        Test test = null;
        
        HttpURLConnection conn = GET(String.format(Resource.SINGLE.url, id), 
                ContentType.JSON, null);
        
        if(conn != null) {
            
            BodyResponse response = BodyResponse.createInstance(conn, 
                    Test.class);
            
            test = (Test) response.getPayload();
            
            conn.disconnect();
        }
        
        return test;
    }
    
    /**
     * Método para obtener el adaptador de la lista de test
     * @param searchData Datos utilizados para la búsqueda
     * @param page Página buscada
     * @param pageSize Tamaño de la página resultado
     * @return Adaptador de la lista de test
     */
    public TestListAdapter getTestList(Test searchData, Integer page, 
            Integer pageSize) {
        
        TestListAdapter adapter = new TestListAdapter();
        
        Map params = TestFilter.build(searchData, page, pageSize);
        
        HttpURLConnection conn = GET(Resource.LIST.url, 
                ContentType.JSON, params);
        
        if(conn != null) {
            
            BodyResponse response = BodyResponse.createInstance(conn, 
                    TestListAdapter.class);
            
            adapter = (TestListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        
        return adapter;
    }
    
    /**
     * Método para crear un nuevo test en el sistema
     * @param test Datos del test
     * @return Mensaje de respuesta del servidor
     */
    public MessageListAdapter createTest(Test test) {
        
        MessageListAdapter adapter = new MessageListAdapter();
        
        HttpURLConnection conn = POST(Resource.CREATE.url, ContentType.JSON);
        
        if(conn != null) {
            
            Gson gson = new Gson();
        
            this.addBody(conn, gson.toJson(test));
            
            BodyResponse response = BodyResponse.createInstance(conn, 
                    MessageListAdapter.class);
            
            adapter = (MessageListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        
        return adapter;
    }
    
    /**
     * Método para editar un test en el sistema
     * @param test Datos del test
     * @return Mensaje de respuesta del servidor
     */
    public MessageListAdapter editTest(Test test) {
        
        MessageListAdapter adapter = new MessageListAdapter();
        
        HttpURLConnection conn = PUT(Resource.EDIT.url, ContentType.JSON);
        
        if(conn != null) {
            
            Gson gson = new Gson();
        
            this.addBody(conn, gson.toJson(test));
            
            BodyResponse response = BodyResponse.createInstance(conn, 
                    MessageListAdapter.class);
            
            adapter = (MessageListAdapter) response.getPayload();
            
            conn.disconnect();
        }
        
        return adapter;
    }
    
    /**
     * Constructor de TestServiceClient
     */
    public TestServiceClient() {
        super();
    }
    
    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {
        
        //Servicios
        SINGLE("Obtiene un producto", "test/%d"),
        LIST("Listar productos", "test"),
        CREATE("Crear producto", "test"),
        EDIT("Editar producto", "test");
        
        /**
         * Nombre del recurso
         */
        private String resource;
        
        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }
        
        /**
         * Constructor de Resource
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}