package py.com.sepsa.erp.web.v1.comercial.filters;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;

/**
 * Filtro utilizado para el servicio de descuento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class DescuentoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de Descuento
     *
     * @param id
     * @return
     */
    public DescuentoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro para de identificador de tipo Descuento
     *
     * @param idTipoDescuento
     * @return
     */
    public DescuentoFilter idTipoDescuento(Integer idTipoDescuento) {
        if (idTipoDescuento != null) {
            params.put("idTipoDescuento", idTipoDescuento);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de Cliente
     *
     * @param idCliente
     * @return
     */
    public DescuentoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de Servicio
     *
     * @param idServicio
     * @return
     */
    public DescuentoFilter idServicio(Integer idServicio) {
        if (idServicio != null) {
            params.put("idServicio", idServicio);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de Contrato
     *
     * @param idContrato
     * @return
     */
    public DescuentoFilter idContrato(Integer idContrato) {
        if (idContrato != null) {
            params.put("idContrato", idContrato);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de Estado
     *
     * @param idEstado
     * @return
     */
    public DescuentoFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro de id Aprobacion
     *
     * @param idAprobacion
     * @return
     */
    public DescuentoFilter idAprobacion(Integer idAprobacion) {
        if (idAprobacion != null) {
            params.put("idAprobacion", idAprobacion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigoTipoDescuento
     *
     * @param codigoTipoDescuento
     * @return
     */
    public DescuentoFilter codigoTipoDescuento(String codigoTipoDescuento) {
        if (codigoTipoDescuento != null && !codigoTipoDescuento.trim().isEmpty()) {
            params.put("codigoTipoDescuento", codigoTipoDescuento);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public DescuentoFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agregar el filtro de fechaInsercion
     *
     * @param fechaInsercion
     * @return
     */
    public DescuentoFilter fechaInsercion(Date fechaInsercion) {

        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaDesde(Date fechaDesde) {

        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaDesde);
                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaDesdeDesde(Date fechaDesdeDesde) {

        if (fechaDesdeDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaDesdeDesde);
                params.put("fechaDesdeDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaDesdeHasta(Date fechaDesdeHasta) {

        if (fechaDesdeHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaDesdeHasta);
                params.put("fechaDesdeHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaHasta(Date fechaHasta) {

        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaHastaDesde(Date fechaHastaDesde) {

        if (fechaHastaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHastaDesde);
                params.put("fechaHastaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    public DescuentoFilter fechaHastaHasta(Date fechaHastaHasta) {

        if (fechaHastaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHastaHasta);
                params.put("fechaHastaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro de valor Descuento
     *
     * @param valorDescuento
     * @return
     */
    public DescuentoFilter valorDescuento(BigDecimal valorDescuento) {
        if (valorDescuento != null) {
            params.put("valorDescuento", valorDescuento);
        }
        return this;
    }

    /**
     * Agrega el filtro de porcentual
     *
     * @param porcentual
     * @return
     */
    public DescuentoFilter porcentual(String porcentual) {
        if (porcentual != null && !porcentual.trim().isEmpty()) {
            params.put("porcentual", porcentual);
        }
        return this;
    }

    /**
     * Agrega el filtro de excedente
     *
     * @param excedente
     * @return
     */
    public DescuentoFilter excedente(String excedente) {
        if (excedente != null && !excedente.trim().isEmpty()) {
            params.put("excedente", excedente);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param descuento
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Descuento descuento, Integer page, Integer pageSize) {
        DescuentoFilter filter = new DescuentoFilter();

        filter
                .id(descuento.getId())
                .idTipoDescuento(descuento.getIdTipoDescuento())
                .idCliente(descuento.getIdCliente())
                .idServicio(descuento.getIdServicio())
                .idContrato(descuento.getIdContrato())
                .idEstado(descuento.getIdEstado())
                .idAprobacion(descuento.getIdAprobacion())
                .fechaDesde(descuento.getFechaDesde())
                .fechaDesdeDesde(descuento.getFechaDesdeDesde())
                .fechaDesdeHasta(descuento.getFechaDesdeHasta())
                .fechaHasta(descuento.getFechaHasta())
                .fechaHastaDesde(descuento.getFechaHastaDesde())
                .fechaHastaHasta(descuento.getFechaHastaHasta())
                .valorDescuento(descuento.getValorDescuento())
                .porcentual(descuento.getPorcentual())
                .excedente(descuento.getExcedente())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DescuentoFilter
     */
    public DescuentoFilter() {
        super();
    }
}
