package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.EstadoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de local
 *
 * @author Romina Núñez
 */
public class EstadoService extends APIErpCore {

    /**
     * Obtiene la lista de estados
     *
     * @param estado
     * @param page
     * @param pageSize
     * @return
     */
    public EstadoAdapter getEstadoList(Estado estado, Integer page,
            Integer pageSize) {

        EstadoAdapter lista = new EstadoAdapter();

        Map params = EstadoFilter.build(estado, page, pageSize);

        HttpURLConnection conn = GET(Resource.ESTADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    EstadoAdapter.class);

            lista = (EstadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para crear
     *
     * @param estado
     * @return
     */
    public BodyResponse<Estado> setEstado(Estado estado) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.ESTADO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(estado));
            response = BodyResponse.createInstance(conn, Estado.class);
        }
        return response;
    }

    /**
     * Método para editar Estado
     *
     * @param estado
     * @return
     */
    public BodyResponse<Estado> editEstado(Estado estado) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.ESTADO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(estado));
            response = BodyResponse.createInstance(conn, Estado.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Estado get(Integer id) {
        Estado data = new Estado(id);
        EstadoAdapter list = getEstadoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de EstadoService
     */
    public EstadoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        ESTADO("Servicio Estado", "estado");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
