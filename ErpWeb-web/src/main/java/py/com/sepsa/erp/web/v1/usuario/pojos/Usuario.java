package py.com.sepsa.erp.web.v1.usuario.pojos;

import java.util.ArrayList;
import java.util.List;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.system.pojos.UsuarioEmpresa;

/**
 * POJO para usuario
 *
 * @author Cristina Insfrán, Romina Núñez
 * @author Sergio D. Riveros Vazquez
 */
public class Usuario {

    /**
     * Identificador del usuario
     */
    private Integer id;
    /**
     * Nombre de Usuario
     */
    private String usuario;
    /**
     * Dato para filtro
     */
    private Boolean tienePersonaFisica;
    /**
     * Nombre persona fisica
     */
    private String nombrePersonaFisica;
    /**
     * Estado del usuario
     */
    private String codigoEstado;
    /**
     * Hash
     */
    private String hash;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Persona
     */
    private String persona;
    /**
     * Identificador de la persona fisica
     */
    private Integer idPersonaFisica;
    /**
     * Persona Física
     */
    private Persona personaFisica;
    /**
     * Contraseña
     */
    private String contrasena;
    /**
     * Email del usuario
     */
    private Email email;
    /**
     * Telefono del usuario
     */
    private Telefono telefono;
    /**
     * Validación del usuario
     */
    private String validacion;
    /**
     * Comercial
     */
    private String comercial;
    /**
     * Comercial administrador
     */
    private String comercialAdministrador;
    /**
     * POJO Estado
     */
    private Estado estado;
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    /**
     * Usuario locales
     */
    private List<UsuarioLocal> usuarioLocales = new ArrayList<>();
    /**
     * Usuario perfiles
     */
    private List<UsuarioPerfilRelacionado> usuarioPerfiles = new ArrayList<>();
    /**
     * URL
     */
    private String urlDestino;
    /**
     * Usuario empresa relacionado
     */
    private List<UsuarioEmpresa> usuarioEmpresas;
    /**
     * Codigo Tipo Empresa
     */
    private String codigoTipoEmpresa;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    /**
     * @return the idPersonaFisica
     */
    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setNombrePersonaFisica(String nombrePersonaFisica) {
        this.nombrePersonaFisica = nombrePersonaFisica;
    }

    public void setTienePersonaFisica(Boolean tienePersonaFisica) {
        this.tienePersonaFisica = tienePersonaFisica;
    }

    public Boolean getTienePersonaFisica() {
        return tienePersonaFisica;
    }

    public String getNombrePersonaFisica() {
        return nombrePersonaFisica;
    }

    public void setUsuarioEmpresas(List<UsuarioEmpresa> usuarioEmpresas) {
        this.usuarioEmpresas = usuarioEmpresas;
    }

    public List<UsuarioEmpresa> getUsuarioEmpresas() {
        return usuarioEmpresas;
    }

    public void setUrlDestino(String urlDestino) {
        this.urlDestino = urlDestino;
    }

    public String getUrlDestino() {
        return urlDestino;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setUsuarioPerfiles(List<UsuarioPerfilRelacionado> usuarioPerfiles) {
        this.usuarioPerfiles = usuarioPerfiles;
    }

    public List<UsuarioPerfilRelacionado> getUsuarioPerfiles() {
        return usuarioPerfiles;
    }

    public List<UsuarioLocal> getUsuarioLocales() {
        return usuarioLocales;
    }

    public void setUsuarioLocales(List<UsuarioLocal> usuarioLocales) {
        this.usuarioLocales = usuarioLocales;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getPersona() {
        return persona;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public String getComercial() {
        return comercial;
    }

    /**
     * @param idPersonaFisica the idPersonaFisica to set
     */
    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     *
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the validacion
     */
    public String getValidacion() {
        return validacion;
    }

    /**
     * @param validacion the validacion to set
     */
    public void setValidacion(String validacion) {
        this.validacion = validacion;
    }

    public String getComercialAdministrador() {
        return comercialAdministrador;
    }

    public void setComercialAdministrador(String comercialAdministrador) {
        this.comercialAdministrador = comercialAdministrador;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setPersonaFisica(Persona personaFisica) {
        this.personaFisica = personaFisica;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Persona getPersonaFisica() {
        return personaFisica;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public Email getEmail() {
        return email;
    }

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Usuario() {

    }

    /**
     * Contructor
     *
     * @param id
     */
    public Usuario(Integer id) {
        this.id = id;
    }
}
