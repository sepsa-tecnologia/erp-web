/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoDetalle {
    /**
     * Identificador de Nota de Débito
     */
    private Integer idNotaDebito;
    /**
     * Nro nota de débito
     */
    private String nroNotaDebito;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Fecha nota de débito
     */
    private Date fechaNotaDebito;

    /**
     * Razón social
     */
    private String razonSocial;

    /**
     * RUC
     */
    private String ruc;
    /**
     * Nro de factura
     */
    private String nroFactura;

    /**
     * Fecha factura
     */
    private Date fechaFactura;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * Monto IVA
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * anhoMes
     */
    private String anhoMes;
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    /**
     * Precio unitario
     */
    private BigDecimal precioUnitarioConIva;
        /**
     * Precio unitario
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * Identificador de factura de compra
     */
    private Integer idFacturaCompra;
    
    private Character facturaDigital;
    
    private String timbradoFactura;
    
    private String cdcFactura;
    
     private BigDecimal descuentoParticularUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoParticularUnitarioAux;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoGlobalUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoParticular;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoGlobal;
    
    private BigDecimal subTotalSinDescuento;
    
    private String lote;
    
    private String fechaVencimientoLote;
    
    
 //<editor-fold defaultstate="collapsed" desc="***GETTER & SETTER***">
    
    public String getLote() {
        return lote;
    }
   
    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }
    
    public void setFechaVencimientoLote(String fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public Integer getIdNotaDebito() {
        return idNotaDebito;
    }

    public void setIdNotaDebito(Integer idNotaDebito) {
        this.idNotaDebito = idNotaDebito;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }
    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }
    public BigDecimal getMontoImponible() {
        return montoImponible;
    }
    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }
    public BigDecimal getMontoTotal() {
        return montoTotal;
    }
    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }
    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }
    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }
    /**
     * @return the fechaFactura
     */
    public Date getFechaFactura() {
        return fechaFactura;
    }
    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }
    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }
    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNroNotaDebito() {
        return nroNotaDebito;
    }

    public void setNroNotaDebito(String nroNotaDebito) {
        this.nroNotaDebito = nroNotaDebito;
    }

    public Date getFechaNotaDebito() {
        return fechaNotaDebito;
    }

    public void setFechaNotaDebito(Date fechaNotaDebito) {
        this.fechaNotaDebito = fechaNotaDebito;
    }

    /**
     * @return anulado
     */
    public String getAnulado() {
        return anulado;
    }

    /**
     * @param anulado the anulado to set
     */
    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getAnhoMes() {
        return anhoMes;
    }

    public void setAnhoMes(String anhoMes) {
        this.anhoMes = anhoMes;
    }

    public Character getFacturaDigital() {
        return facturaDigital;
    }

    public void setFacturaDigital(Character facturaDigital) {
        this.facturaDigital = facturaDigital;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoParticularUnitarioAux() {
        return descuentoParticularUnitarioAux;
    }

    public void setDescuentoParticularUnitarioAux(BigDecimal descuentoParticularUnitarioAux) {
        this.descuentoParticularUnitarioAux = descuentoParticularUnitarioAux;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getSubTotalSinDescuento() {
        return subTotalSinDescuento;
    }

    public void setSubTotalSinDescuento(BigDecimal subTotalSinDescuento) {
        this.subTotalSinDescuento = subTotalSinDescuento;
    }

//</editor-fold>
    
    /**
     * Constructor de la clase
     */
    public NotaDebitoDetalle() {
    }
}
