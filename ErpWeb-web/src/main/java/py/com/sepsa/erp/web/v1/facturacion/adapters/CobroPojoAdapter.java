package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroPojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class CobroPojoAdapter extends DataListAdapter<CobroPojo> {

    /**
     * Cliente para el servicio de cobro
     */
    private final CobroService serviceCobro;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public CobroPojoAdapter fillData(CobroPojo searchData) {

        return serviceCobro.getCobroPojoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de CobroPojoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public CobroPojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceCobro = new CobroService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public CobroPojoAdapter() {
        super();
        this.serviceCobro = new CobroService();
    }
}
