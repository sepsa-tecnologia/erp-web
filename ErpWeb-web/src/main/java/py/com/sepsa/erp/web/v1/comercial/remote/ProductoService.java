package py.com.sepsa.erp.web.v1.comercial.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.filters.ProductoFilter;
import py.com.sepsa.erp.web.v1.comercial.filters.ProductoPojoFilter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.comercial.pojos.ProductoPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio Producto
 *
 * @author Romina E. Núñez Rojas
 */
public class ProductoService extends APIErpComercial {

    /**
     * Obtiene la lista de productos.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoAdapter getProductoList(Producto producto, Integer page,
            Integer pageSize) {

        ProductoAdapter lista = new ProductoAdapter();

        Map params = ProductoFilter.build(producto, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoAdapter.class);

            lista = (ProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
        /**
     * Obtiene la lista de productos.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoPojoAdapter getProductoPojoList(ProductoPojo producto, Integer page,
            Integer pageSize) {

        ProductoPojoAdapter lista = new ProductoPojoAdapter();

        Map params = ProductoPojoFilter.build(producto, page, pageSize);

        HttpURLConnection conn = GET(Resource.LISTAR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoPojoAdapter.class);

            lista = (ProductoPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear un nuevo producto.
     *
     * @param producto
     * @return
     */
    public Producto create(Producto producto) {

        HttpURLConnection conn = POST(Resource.CREAR.url, ContentType.JSON);

        if (conn != null) {
            Gson gson = new Gson();
            WebLogger.get().debug(gson.toJson(producto));
            this.addBody(conn, gson.toJson(producto));
            BodyResponse response = BodyResponse.createInstance(conn,
                    Producto.class);
            producto = (Producto) response.getPayload();
            conn.disconnect();
        }

        return producto;
    }

    /**
     * Constructor de ProductoService
     */
    public ProductoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        LISTAR("Listado de prodcutos", "producto"),
        CREAR("Creación de prodcutos", "producto");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
