package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de contacto teléfono
 *
 * @author Romina Núñez
 */
public class ContactoTelefonoNotificacion {

    /**
     * Identificador del contacto
     */
    private Integer idContacto;
    /**
     * Contacto
     */
    private String contacto;
    /**
     * identificador del telefono
     */
    private Integer idTelefono;
    /**
     * Prefijo
     */
    private String prefijo;
    /**
     * Número
     */
    private String numero;
    /**
     * Identificador de notificación
     */
    private Integer idTipoNotificacion;
    /**
     * Tipo de Notificación
     */
    private String tipoNotificacion;
    /**
     * Estado
     */
    private String estado;

    /**
     * @return the idContacto
     */
    public Integer getIdContacto() {
        return idContacto;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public String getContacto() {
        return contacto;
    }

    /**
     * @param idContacto the idContacto to set
     */
    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    /**
     * @return the idTelefono
     */
    public Integer getIdTelefono() {
        return idTelefono;
    }

    /**
     * @param idTelefono the idTelefono to set
     */
    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Constructor
     */
    public ContactoTelefonoNotificacion() {

    }

    /**
     * Constructor con parámetros
     * @param idContacto
     * @param contacto
     * @param idTelefono
     * @param prefijo
     * @param numero
     * @param idTipoNotificacion
     * @param tipoNotificacion
     * @param estado 
     */
    public ContactoTelefonoNotificacion(Integer idContacto, String contacto, Integer idTelefono, String prefijo, String numero, Integer idTipoNotificacion, String tipoNotificacion, String estado) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idTelefono = idTelefono;
        this.prefijo = prefijo;
        this.numero = numero;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.estado = estado;
    }
    
    

}
