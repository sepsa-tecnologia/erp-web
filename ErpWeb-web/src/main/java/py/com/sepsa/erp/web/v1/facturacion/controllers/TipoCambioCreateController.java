/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para la vista de Crear Tipo Cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("tipoCambioCreate")
public class TipoCambioCreateController implements Serializable {

    /**
     * Cliente para el servicio Tipo cambio
     */
    private final TipoCambioService service;
    /**
     * POJO del tipoCambio
     */
    private TipoCambio tipoCambio;
    /**
     * Adaptador para la lista de monedas
     */
    private MonedaAdapter monedaAdapterList;
    /**
     * POJO de moneda
     */
    private Moneda monedaFilter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public MonedaAdapter getMonedaAdapterList() {
        return monedaAdapterList;
    }

    public void setMonedaAdapterList(MonedaAdapter monedaAdapterList) {
        this.monedaAdapterList = monedaAdapterList;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    //</editor-fold>
    /**
     * Método para crear tipo cambio
     */
    public void create() {

        BodyResponse<TipoCambio> respuestaTal = service.setTipoCambios(tipoCambio);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Tipo de Cambio creado correctamente!"));
            init();
        }
    }

    /**
     * Método para obtener moneda
     */
    public void getMonedaes() {
        this.setMonedaAdapterList(getMonedaAdapterList().fillData(getMonedaFilter()));
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.tipoCambio = new TipoCambio();
        this.monedaAdapterList = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        getMonedaes();
    }

    /**
     * Constructor
     */
    public TipoCambioCreateController() {
        init();
        this.service = new TipoCambioService();
    }
}
