
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.remote.EstadoService;

/**
 * Adaptador para persona
 * @author Romina Núñez
 */
public class EstadoAdapter extends DataListAdapter<Estado> {
    
    /**
     * Cliente para los servicios de estado
     */
    private final EstadoService estadoService;
   
    
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public EstadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.estadoService = new EstadoService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public EstadoAdapter() {
        super();
        this.estadoService = new EstadoService();
    }

    @Override
    public EstadoAdapter fillData(Estado searchData) {
        return estadoService.getEstadoList(searchData,getFirstResult(),getPageSize());
    }
}



