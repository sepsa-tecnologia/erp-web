/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.Transportista;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para transportista
 *
 * @author Williams Vera
 */
public class TransportistaFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TransportistaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }



    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public TransportistaFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    
    /**
     * Agrega el filtro de ruc
     *
     * @param ruc
     * @return
     */
    public TransportistaFilter ruc(String ruc) {
        if (ruc != null && !ruc.trim().isEmpty()) {
            params.put("ruc", ruc);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de razonSocial
     *
     * @param razonSocial
     * @return
     */
    public TransportistaFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }
    
    
        /**
     * Agrega el filtro de nombreCompleto
     *
     * @param nombreCompleto
     * @return
     */
    public TransportistaFilter nombreCompleto(String nombreCompleto) {
        if (nombreCompleto != null && !nombreCompleto.trim().isEmpty()) {
            params.put("nombreCompleto", nombreCompleto);
        }
        return this;
    }
    
            /**
     * Agrega el filtro de nroDocumentoChofer
     *
     * @param nroDocumentoChofer
     * @return
     */
    public TransportistaFilter nroDocumentoChofer(String nroDocumentoChofer) {
        if (nroDocumentoChofer != null && !nroDocumentoChofer.trim().isEmpty()) {
            params.put("nroDocumentoChofer", nroDocumentoChofer);
        }
        return this;
    }


    /**
     * Agrega el filtro de idTipoDocumento
     *
     * @param idTipoDocumento
     * @return
     */
    public TransportistaFilter idTipoDocumento(Integer idTipoDocumento) {
        if (idTipoDocumento != null) {
            params.put("idTipoDocumento", idTipoDocumento);
        }
        return this;
    }




    /**
     * Construye el mapa de parametros
     *
     * @param transportista datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Transportista transportista, Integer page, Integer pageSize) {
        TransportistaFilter filter = new TransportistaFilter();

        filter
                .id(transportista.getId())
                .ruc(transportista.getRuc())
                .activo(transportista.getActivo())
                .razonSocial(transportista.getRazonSocial())
                .nombreCompleto(transportista.getNombreCompleto())
                .nroDocumentoChofer(transportista.getNroDocumentoChofer())
                .idTipoDocumento(transportista.getIdTipoDocumento())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TransportistaFilter() {
        super();
    }

}
