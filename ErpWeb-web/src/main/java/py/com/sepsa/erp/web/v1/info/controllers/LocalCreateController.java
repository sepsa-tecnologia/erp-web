/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.ReverseGeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoTelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.pojos.TipoReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.DireccionServiceClient;
import py.com.sepsa.erp.web.v1.info.remote.LocalService;
import py.com.sepsa.erp.web.v1.info.remote.TelefonoServiceClient;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para crear local
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("localCreate")
public class LocalCreateController implements Serializable {

    /**
     * Cliente para el servicio de local.
     */
    private final LocalService service;
    /**
     * POJO del Local
     */
    private Local local;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Latitud de la direccion del local
     */
    private double lat = -25.273884;
    /**
     * Longitud de la direccion del local
     */
    private double lng = -57.634671;

    /**
     *
     */
    private MapModel draggableModel;
    /**
     *
     */
    private Marker marker;
    /**
     * Cliente remote para dirección
     */
    private DireccionServiceClient direccionClient;
    /**
     * Objeto Dirección
     */
    private Direccion direccion;
    /**
     * Adaptador de la lista de tipo de referencia geografica
     */
    private TipoReferenciaGeograficaAdapter adapterTipoRefGeo;
    /**
     * Objeto tipo referencia geo
     */
    private TipoReferenciaGeografica tipoRefGeo;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeo;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Lista para tipo de cobro
     */
    private List<SelectItem> listDepartamentos = new ArrayList<>();
    /**
     * Lista de Distritos
     */
    private List<SelectItem> listDistritos = new ArrayList<>();
    /**
     * Lista de Ciudades
     */
    private List<SelectItem> listCiudades = new ArrayList<>();
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Pojo Telefono
     */
    private Telefono telefono;
    /**
     * Cliente del servicio de Telefono
     */
    private TelefonoServiceClient telefonoService;
    /**
     * Lista de tipo de telefonos
     */
    private List<SelectItem> listTipoTelefono;
    /**
     * Adaptador de TipoTelefono
     */
    private TipoTelefonoListAdapter adapterTipoTelefono;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public TelefonoServiceClient getTelefonoService() {
        return telefonoService;
    }

    public void setTelefonoService(TelefonoServiceClient telefonoService) {
        this.telefonoService = telefonoService;
    }

    public List<SelectItem> getListTipoTelefono() {
        return listTipoTelefono;
    }

    public void setListTipoTelefono(List<SelectItem> listTipoTelefono) {
        this.listTipoTelefono = listTipoTelefono;
    }
    
    /**
     * @return the referenciaGeoDepartamento
     */
    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    /**
     * @param referenciaGeoDepartamento the referenciaGeoDepartamento to set
     */
    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    /**
     * @return the referenciaGeoCiudad
     */
    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    /**
     * @param referenciaGeoCiudad the referenciaGeoCiudad to set
     */
    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    /**
     * @return the listDistritos
     */
    public List<SelectItem> getListDistritos() {
        return listDistritos;
    }

    /**
     * @param listDistritos the listDistritos to set
     */
    public void setListDistritos(List<SelectItem> listDistritos) {
        this.listDistritos = listDistritos;
    }

    /**
     * @return the listCiudades
     */
    public List<SelectItem> getListCiudades() {
        return listCiudades;
    }

    /**
     * @param listCiudades the listCiudades to set
     */
    public void setListCiudades(List<SelectItem> listCiudades) {
        this.listCiudades = listCiudades;
    }

    /**
     * @return the referenciaGeoDistrito
     */
    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    /**
     * @param referenciaGeoDistrito the referenciaGeoDistrito to set
     */
    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    /**
     * @return the listDepartamentos
     */
    public List<SelectItem> getListDepartamentos() {
        return listDepartamentos;
    }

    /**
     * @param listDepartamentos the listDepartamentos to set
     */
    public void setListDepartamentos(List<SelectItem> listDepartamentos) {
        this.listDepartamentos = listDepartamentos;
    }

    /**
     * @return the adapterRefGeo
     */
    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    /**
     * @param adapterRefGeo the adapterRefGeo to set
     */
    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    /**
     * @return the referenciaGeo
     */
    public ReferenciaGeografica getReferenciaGeo() {
        return referenciaGeo;
    }

    /**
     * @param referenciaGeo the referenciaGeo to set
     */
    public void setReferenciaGeo(ReferenciaGeografica referenciaGeo) {
        this.referenciaGeo = referenciaGeo;
    }

    /**
     * @return the tipoRefGeo
     */
    public TipoReferenciaGeografica getTipoRefGeo() {
        return tipoRefGeo;
    }

    /**
     * @param tipoRefGeo the tipoRefGeo to set
     */
    public void setTipoRefGeo(TipoReferenciaGeografica tipoRefGeo) {
        this.tipoRefGeo = tipoRefGeo;
    }

    /**
     * @return the adapterTipoRefGeo
     */
    public TipoReferenciaGeograficaAdapter getAdapterTipoRefGeo() {
        return adapterTipoRefGeo;
    }

    /**
     * @param adapterTipoRefGeo the adapterTipoRefGeo to set
     */
    public void setAdapterTipoRefGeo(TipoReferenciaGeograficaAdapter adapterTipoRefGeo) {
        this.adapterTipoRefGeo = adapterTipoRefGeo;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return the lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * @param lng the lng to set
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the direccionClient
     */
    public DireccionServiceClient getDireccionClient() {
        return direccionClient;
    }

    /**
     * @param direccionClient the direccionClient to set
     */
    public void setDireccionClient(DireccionServiceClient direccionClient) {
        this.direccionClient = direccionClient;
    }

    /**
     * @return the draggableModel
     */
    public MapModel getDraggableModel() {
        return draggableModel;
    }

    /**
     * @param draggableModel the draggableModel to set
     */
    public void setDraggableModel(MapModel draggableModel) {
        this.draggableModel = draggableModel;
    }

    /**
     * @return the marker
     */
    public Marker getMarker() {
        return marker;
    }

    /**
     * @param marker the marker to set
     */
    public void setMarker(Marker marker) {
        this.marker = marker;
    }
    
    public Local getLocal() {
        return local;
    }
    
    public void setLocal(Local local) {
        this.local = local;
    }
    
    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }
    
    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public Integer getIdDepartamento() {
        return idDepartamento;
    }
    
    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    
    public Integer getIdDistrito() {
        return idDistrito;
    }
    
    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }
    
    public Integer getIdCiudad() {
        return idCiudad;
    }
    
    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }
    
    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }
    
    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }
    
    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }
    
    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    //</editor-fold>
    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccion.setIdDepartamento(empyInt);
        direccion.setIdDistrito(empyInt);
        direccion.setIdCiudad(empyInt);
        
        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }
        
        PF.current().ajax().update(":local-form:form-data:distrito");
        PF.current().ajax().update(":local-form:form-data:ciudad");
        
        return datos;
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {

        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccion.setIdDistrito(empyInt);
        direccion.setIdCiudad(empyInt);
        
        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccion.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }
        
        PF.current().ajax().update(":local-form:form-data:ciudad");
        
        return datos;
        
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {

        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        
        ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
        referenciaGeoDistrito.setIdPadre(direccion.getIdDistrito());
        referenciaGeoDistrito.setDescripcion(query);
        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
        
        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccion.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     *
     * @param event
     */
    public void onMarkerDrag(MarkerDragEvent event) {
        marker = event.getMarker();
        lat = marker.getLatlng().getLat();
        lng = marker.getLatlng().getLng();
    }

    /**
     * Enfoca el mapa en la direccion indicada
     *
     * @param event Evento que contiene la direccion
     */
    public void onGeocode(GeocodeEvent event) {
        GeocodeResult result = event.getResults().get(0);
        if (result != null) {
            LatLng center = result.getLatLng();
            lat = center.getLat();
            lng = center.getLng();
            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setDraggable(true);
                premarker.setLatlng(center);
                premarker.setTitle(result.getAddress());
            });
        }
    }

    /**
     * Obtiene la direccion exacta de la coordenada indicada
     *
     * @param event Evento que contiene las coordenadas
     */
    public void onReverseGeocode(ReverseGeocodeEvent event) {
        
        String address = event.getAddresses().get(0);
        LatLng center = event.getLatlng();
        if (address != null && center != null) {
            lat = center.getLat();
            lng = center.getLng();
            direccion.setDireccion(address);
            
            draggableModel.getMarkers().stream().forEach((premarker) -> {
                premarker.setLatlng(center);
                premarker.setTitle(address);
            });
        }
        PrimeFaces.current().executeScript("initAutocomplete();");
    }

    /**
     * Método para crear local
     */
    public void create() {
        Boolean saveLocal = true; 
        Integer idTelefono = null;
        direccion.setLatitud(getLat() + "");
        direccion.setLongitud(getLng() + "");
        direccion.setIdDepartamento(idDepartamento);
        direccion.setIdDistrito(idDistrito);
        direccion.setIdCiudad(idCiudad);
        BodyResponse setDireccion = direccionClient.setDireccion(direccion);
        if (setDireccion.getSuccess() != true){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Verifique los datos de dirección!"));
             saveLocal = false;
        } 
        
        if (telefono!= null && !telefono.getPrefijo().trim().equalsIgnoreCase("") && !telefono.getNumero().trim().equalsIgnoreCase("")){
            telefono.setPrincipal("S");
            idTelefono = telefonoService.setTelefono(telefono);
            if (idTelefono == null ){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Verifique los datos de teléfono!"));
                saveLocal = false;
            }
        }
        
        if (saveLocal == true) {
            local.setIdDireccion(((Direccion) setDireccion.getPayload()).getId());
            local.setIdTelefono(idTelefono);
            
            BodyResponse<Local> respuestaLocal = service.setLocal(local);
            
            if (respuestaLocal.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Local creado correctamente!"));
                init();
            }
        }
        
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        
        clientAdapter = clientAdapter.fillData(cliente);
        
        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.local.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Muestra la lista de distritos
     */
    public void listaDistritos() {
        listDistritos = new ArrayList<>();
        listCiudades = new ArrayList<>();
        adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDepartamento(referenciaGeoDepartamento.getId());
        
        referenciaGeoDepartamento = new ReferenciaGeografica();
        referenciaGeoDepartamento.setIdPadre(direccion.getIdDepartamento());
        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDepartamento);
        
        if (!adapterRefGeo.getData().isEmpty()) {
            for (int i = 0; i < adapterRefGeo.getData().size(); i++) {
                listDistritos.add(new SelectItem(adapterRefGeo.getData().get(i).getId(),
                        adapterRefGeo.getData().get(i).getDescripcion()));
            }
        }
    }

    /**
     * Muesta la lista de ciudades
     */
    public void listaCiudades() {
        listCiudades = new ArrayList<>();
        adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccion.setIdDistrito(referenciaGeoDistrito.getId());
        referenciaGeoDistrito = new ReferenciaGeografica();
        referenciaGeoDistrito.setIdPadre(direccion.getIdDistrito());
        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
        
        if (!adapterRefGeo.getData().isEmpty()) {
            for (int i = 0; i < adapterRefGeo.getData().size(); i++) {
                listCiudades.add(new SelectItem(adapterRefGeo.getData().get(i).getId(),
                        adapterRefGeo.getData().get(i).getDescripcion()));
            }
        }
    }
    
    public void filterDepartamento() {
        WebLogger.get().debug("FILTRO DEP");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }
    
    public void filterDistrito() {
        WebLogger.get().debug("FILTRO DISTRITO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }
    
    public void filterCiudad() {
        WebLogger.get().debug("FILTRO CIUDAD");
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }
    
    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }

    /**
     * Inicializa los datos
     */
    @PostConstruct
    private void init() {
        this.local = new Local();
        this.local.setLocalExterno("S");
        this.clientAdapter = new ClientListAdapter();
        this.cliente = new Cliente();
        this.direccionClient = new DireccionServiceClient();
        this.direccion = new Direccion();
        this.adapterTipoRefGeo = new TipoReferenciaGeograficaAdapter();
        this.tipoRefGeo = new TipoReferenciaGeografica();
        this.referenciaGeo = new ReferenciaGeografica();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();
        this.telefono = new Telefono();
        this.telefonoService = new TelefonoServiceClient();
        this.listTipoTelefono = new ArrayList();
        TipoTelefono tipoTelefono = new TipoTelefono(); 
        this.adapterTipoTelefono = new TipoTelefonoListAdapter();
        this.adapterTipoTelefono = adapterTipoTelefono.fillData(tipoTelefono);
        
        if (adapterTipoTelefono.getData() != null ){
            for (TipoTelefono tp : adapterTipoTelefono.getData()){
                listTipoTelefono.add(new SelectItem(tp.getId(),tp.getCodigo()));
            }
        }
        
        adapterTipoRefGeo = adapterTipoRefGeo.fillData(tipoRefGeo);
        
        if (!adapterTipoRefGeo.getData().isEmpty()) {
            referenciaGeo.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeo);
            for (int i = 0; i < adapterRefGeo.getData().size(); i++) {
                listDepartamentos.add(new SelectItem(adapterRefGeo.getData().get(i).getId(),
                        adapterRefGeo.getData().get(i).getDescripcion()));
            }
        }
        lat = -25.286534173063284;
        lng = -57.63632285404583;
        draggableModel = new DefaultMapModel();
        LatLng coord = new LatLng(lat, lng);
        draggableModel.addOverlay(new Marker(coord));
        for (Marker premarker : draggableModel.getMarkers()) {
            premarker.setDraggable(true);
        }
        
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;
        
        filterDepartamento();
        filterDistrito();
        filterCiudad();
        
    }

    /**
     * Constructor
     */
    public LocalCreateController() {
        init();
        this.service = new LocalService();
    }
}
