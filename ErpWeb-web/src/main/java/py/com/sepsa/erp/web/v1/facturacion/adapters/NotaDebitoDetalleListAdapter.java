/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebitoDetalle;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoDetalleListAdapter extends DataListAdapter<NotaDebitoDetalle> {
  
     /**
     * Cliente para el servicio de cobro
     */
    private final NotaDebitoService serviceNotaDebito;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaDebitoDetalleListAdapter fillData(NotaDebitoDetalle searchData) {
     
        return serviceNotaDebito.getNotaDebitoDetalleList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaCreditoDetalleListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaDebitoDetalleListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaDebito = new NotaDebitoService();
    }

    /**
     * Constructor de NotaCreditoDetalleListAdapter
     */
    public NotaDebitoDetalleListAdapter() {
        super();
        this.serviceNotaDebito = new NotaDebitoService();
    }
}
