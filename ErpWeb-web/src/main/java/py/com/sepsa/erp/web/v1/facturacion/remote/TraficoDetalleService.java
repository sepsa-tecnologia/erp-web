package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TraficoDetalleAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.TraficoDetalleFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.TraficoDetalle;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de cobro
 *
 * @author Romina Núñez
 */
public class TraficoDetalleService extends APIErpFacturacion {

    /**
     * Obtiene la lista de Trafico
     *
     * @param trafico Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TraficoDetalleAdapter getTraficoList(TraficoDetalle trafico, Integer page,
            Integer pageSize) {

        TraficoDetalleAdapter lista = new TraficoDetalleAdapter();

        Map params = TraficoDetalleFilter.build(trafico, page, pageSize);

        HttpURLConnection conn = GET(Resource.TRAFICO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TraficoDetalleAdapter.class);

            lista = (TraficoDetalleAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
   


    /**
     * Constructor de FacturacionService
     */
    public TraficoDetalleService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TRAFICO("Trafico", "trafico-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
