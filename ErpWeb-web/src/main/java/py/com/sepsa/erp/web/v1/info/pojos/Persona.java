package py.com.sepsa.erp.web.v1.info.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * POJO para persona
 *
 * @author Sergio D. Riveros Vazquez, Cristina Insfrán
 */
public class Persona {

    /**
     * Identificador de la persona
     */
    private Integer id;
    /**
     * Identificador del tipo de persona
     */
    private Integer idTipoPersona;
    /**
     * Activo
     */
    private String activo;
    /**
     * Nombre
     */
    private String nombre;
    /**
     * Razón social
     */
    private String razonSocial;
    /**
     * Nombre fantasía
     */
    private String nombreFantasia;
    /**
     * Apellido
     */
    private String apellido;

    /**
     * Tipo Persona
     */
    private TipoPersona tipoPersona;
    /**
     * Número de Documento
     */
    private String nroDocumento;
    /**
     * Código tipo persona
     */
    private String codigoTipoPersona;
    /**
     * Identificador de la dirección
     */
    private Integer idDireccion;
    /**
     * Objeto Contacto
     */
    private Contacto contacto;
    /**
     * Objeto Dirección
     */
    private Direccion direccion;
    /**
     * Objeto Email
     */
    private Email email;
    /**
     * Telefono
     */
    private Telefono telefono;
    /**
     * Lista de email
     */
    private List<PersonaEmail> personaEmails = new ArrayList<>();
    /**
     * Lista de teléfonos
     */
    private List<PersonaTelefono> personaTelefonos = new ArrayList<>();

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * @return the personaEmails
     */
    public List<PersonaEmail> getPersonaEmails() {
        return personaEmails;
    }

    /**
     * @param personaEmails the personaEmails to set
     */
    public void setPersonaEmails(List<PersonaEmail> personaEmails) {
        this.personaEmails = personaEmails;
    }

    /**
     * @return the personaTelefonos
     */
    public List<PersonaTelefono> getPersonaTelefonos() {
        return personaTelefonos;
    }

    /**
     * @param personaTelefonos the personaTelefonos to set
     */
    public void setPersonaTelefonos(List<PersonaTelefono> personaTelefonos) {
        this.personaTelefonos = personaTelefonos;
    }

    /**
     * @return the contacto
     */
    public Contacto getContacto() {
        return contacto;
    }

    /**
     * @param contacto the contacto to set
     */
    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the email
     */
    public Email getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(Email email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public Telefono getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the idDireccion
     */
    public Integer getIdDireccion() {
        return idDireccion;
    }

    /**
     * @param idDireccion the idDireccion to set
     */
    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Integer getId() {
        return id;

    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public void setCodigoTipoPersona(String codigoTipoPersona) {
        this.codigoTipoPersona = codigoTipoPersona;
    }

    public String getCodigoTipoPersona() {
        return codigoTipoPersona;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    //</editor-fold>
    /**
     * Constructor de la clase
     */
    public Persona() {
    }
}
