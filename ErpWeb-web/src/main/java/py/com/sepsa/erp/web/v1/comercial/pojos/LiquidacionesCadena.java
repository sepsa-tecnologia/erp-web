package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para Liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionesCadena {

    /**
     * Identificador del Cliente.
     */
    private Integer idCliente;
    /**
     * Cliente.
     */
    private String cliente;
    /**
     * Linea de crédito.
     */
    private Integer linea;
    /**
     * Descuento.
     */
    private Integer descuento;
    /**
     * Monto total.
     */
    private Integer total;
    /**
     * Año
     */
    private String ano;
    /**
     * Mes
     */
    private String mes;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    //</editor-fold>

    /**
     * Constructor.
     */
    public LiquidacionesCadena() {

    }

    public LiquidacionesCadena(String ano, String mes) {
        this.ano = ano;
        this.mes = mes;
    }

    /**
     * Constructor de parámetros.
     *
     * @param idCliente
     * @param cliente
     * @param linea
     * @param descuento
     * @param total
     * @param ano
     * @param mes
     */
    public LiquidacionesCadena(Integer idCliente, String cliente, Integer linea,
            Integer descuento, Integer total, String ano, String mes) {
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.linea = linea;
        this.descuento = descuento;
        this.total = total;
        this.ano = ano;
        this.mes = mes;
    }

}
