
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.remote.PersonaService;

/**
 * Adaptador para persona
 * @author Romina Núñez
 */
public class PersonaListAdapter extends DataListAdapter<Persona> {
    
    /**
     * Cliente para los servicios de clientes
     */
    private final PersonaService personaClient;
   
    
    
    /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public PersonaListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.personaClient = new PersonaService();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public PersonaListAdapter() {
        super();
        this.personaClient = new PersonaService();
    }

    @Override
    public PersonaListAdapter fillData(Persona searchData) {
        return personaClient.getPersonaList(searchData,getFirstResult(),getPageSize());
    }
}



