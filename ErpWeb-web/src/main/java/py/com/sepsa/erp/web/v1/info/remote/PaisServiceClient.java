package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.PaisAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PaisFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Pais;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de configuracion valor
 *
 * @author Williams Vera
 */
public class PaisServiceClient extends APIErpCore {

    /**
     * Método para editar pais
     *
     * @param pais
     * @return
     */
    public Pais editPais(Pais pais) {

        Pais paisEdit = new Pais();

        HttpURLConnection conn = PUT(Resource.PAIS.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(pais));

            BodyResponse response = BodyResponse.createInstance(conn,
                    Pais.class);

            paisEdit = (Pais) response.getPayload();

            conn.disconnect();
        }
        return paisEdit;
    }


    /**
     * Método para setear pais
     *
     * @param pais
     * @return
     */
    public Integer createPais(Pais pais) {

        Integer idPais = null;

        HttpURLConnection conn = POST(Resource.PAIS.url,
                ContentType.JSON);

        if (conn != null) {

            Gson gson = new Gson();

            this.addBody(conn, gson.toJson(pais));

            BodyResponse response = BodyResponse.createInstance(conn,
                    Pais.class);

            idPais = ((Pais) response.getPayload()).getId();

            conn.disconnect();
        }
        return idPais;
    }



    /**
     * Obtiene la lista de Pais
     *
     * @param pais
     * @param page
     * @param pageSize
     * @return
     */
    public PaisAdapter getPaisList(Pais pais, Integer page,
            Integer pageSize) {

        PaisAdapter lista = new PaisAdapter(page, pageSize);

        Map params = PaisFilter.build(pais, page, pageSize);

        HttpURLConnection conn = GET(Resource.PAIS.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PaisAdapter.class);

            lista = (PaisAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Constructor de ConfiguracionValorServiceClient
     */
    public PaisServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicios
        PAIS("Servicio pais", "pais");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
