
package py.com.sepsa.erp.web.v1.test.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.test.pojos.Test;
import py.com.sepsa.erp.web.v1.test.remote.TestServiceClient;

/**
 * Adaptador para la lista de test
 * @author Daniel F. Escauriza Arza
 */
public class TestListAdapter extends DataListAdapter<Test> {
    
    /**
     * Cliente para los servicios de producto
     */
    private final TestServiceClient serviceClient;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TestListAdapter fillData(Test searchData) {
        
        TestListAdapter adapter = new TestListAdapter();
        
        Test test1 = new Test(1, "PRUEBA 1", "DETALLE DE PRUEBA 1");
        Test test2 = new Test(2, "PRUEBA 2", "DETALLE DE PRUEBA 2");
        Test test3 = new Test(3, "PRUEBA 3", "DETALLE DE PRUEBA 3");
        
        adapter.getData().add(test1);
        adapter.getData().add(test2);
        adapter.getData().add(test3);        
          
        
        return adapter;
        
        /*return serviceClient.getTestList(searchData, getPage(), 
                getPageSize());*/
    }
    
    /**
     * Constructor de ProductListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TestListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClient = new TestServiceClient();
    }

    /**
     * Constructor de ProductListAdapter
     */
    public TestListAdapter() {
        super();
        this.serviceClient = new TestServiceClient();
    }
    
}
