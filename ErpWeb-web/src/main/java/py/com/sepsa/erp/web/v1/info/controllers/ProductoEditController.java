/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MarcaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.MetricaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Marca;
import py.com.sepsa.erp.web.v1.info.pojos.Metrica;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProducto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para editar producto
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("productoEdit")

public class ProductoEditController implements Serializable {

    /**
     * Cliente para el servicio de liquidacion producto.
     */
    private final ProductoService service;
    /**
     * POJO del producto
     */
    private Producto producto;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfiguracionValor;
    /**
     * Pojo conf valor
     */
    private ConfiguracionValor confValor;
    /**
     * Adaptador para la lista de Marca para autocomplete
     */
    private MarcaAdapter marcaAdapterListAux;
    /**
     * POJO de Marca para autocomplete
     */
    private Marca marcaFilterAux;
        /**
     * Adaptador para lista de metricas
     */
    private MetricaAdapter metricaAdapter;
    /**
     * POJO de metrica
     */
    private Metrica metrica;
    
    /**
     * Porcentaje de producto relacionado
     */
    private BigDecimal porcentajeRelacionado;
    
    /**
     * POJO tipoProducto
     */
    private TipoProducto tipoProducto;
    
    /**
     * Adapter de TipoProducto
     */
    private TipoProductoAdapter tipoProductoAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }

    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public TipoProductoAdapter getTipoProductoAdapter() {
        return tipoProductoAdapter;
    }

    public void setTipoProductoAdapter(TipoProductoAdapter tipoProductoAdapter) {
        this.tipoProductoAdapter = tipoProductoAdapter;
    }
    
    public Producto getProducto() {
        return producto;
    }

    public void setConfValor(ConfiguracionValor confValor) {
        this.confValor = confValor;
    }

    public void setAdapterConfiguracionValor(ConfiguracionValorListAdapter adapterConfiguracionValor) {
        this.adapterConfiguracionValor = adapterConfiguracionValor;
    }

    public ConfiguracionValor getConfValor() {
        return confValor;
    }

    public ConfiguracionValorListAdapter getAdapterConfiguracionValor() {
        return adapterConfiguracionValor;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public MarcaAdapter getMarcaAdapterListAux() {
        return marcaAdapterListAux;
    }

    public void setMarcaAdapterListAux(MarcaAdapter marcaAdapterListAux) {
        this.marcaAdapterListAux = marcaAdapterListAux;
    }

    public Marca getMarcaFilterAux() {
        return marcaFilterAux;
    }

    public void setMarcaFilterAux(Marca marcaFilterAux) {
        this.marcaFilterAux = marcaFilterAux;
    }

    public MetricaAdapter getMetricaAdapter() {
        return metricaAdapter;
    }

    public void setMetricaAdapter(MetricaAdapter metricaAdapter) {
        this.metricaAdapter = metricaAdapter;
    }

    public Metrica getMetrica() {
        return metrica;
    }

    public void setMetrica(Metrica metrica) {
        this.metrica = metrica;
    }

    //</editor-fold>
    /**
     * Método para editar Producto
     */
    public void edit() {
        boolean create = true;
        if (producto.getIdMetrica() == null){
            create = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar una Unidad de Medida"));
        }
        if (producto.getCodigoGtin() != null) {
            if ((producto.getCodigoGtin().length() == 1 && producto.getCodigoGtin().equalsIgnoreCase("0")) ||  producto.getCodigoGtin().length() == 8 || producto.getCodigoGtin().length() == 12 || producto.getCodigoGtin().length() == 13 || producto.getCodigoGtin().length() == 14) {
                create = true;
            } else {
                if(producto.getCodigoInterno() == null || producto.getCodigoInterno().isEmpty()){
                    create = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El Código GTIN debe contar con 8, 12, 13 o 14 dígitos! En caso de no tener código, debe ingresar el código interno."));
                } else {
                    producto.setCodigoGtin("0");
                    create = true;
                }
            }
        }
        
        if (producto.getIdTipoProducto() == 2) {
            try {       
                if (porcentajeRelacionado == null  || (!(porcentajeRelacionado.compareTo(BigDecimal.ZERO) == 1 && porcentajeRelacionado.compareTo(new BigDecimal("100")) == -1)) ) {
                    create = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje relacionado debe ser entre 0 y 100!"));
                }  
            } catch (NumberFormatException e) {
                create = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se debe ingresar un porcentaje relacionado válido"));
            }
            
        } else if (producto.getIdTipoProducto() == null){
            producto.setIdTipoProducto(1);
        }
        
        if (create){
            BodyResponse<Producto> respuestaProducto = service.editProducto(this.producto);
            if (respuestaProducto.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Producto editado correctamente!"));
            } else {
                for (Mensaje mensaje : respuestaProducto.getStatus().getMensajes()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
                }
            }
        }
    }

    /**
     * Método para obtener el listado de configuración valor para impuesto de
     * producto
     */
    public void filterConfiguracionValor() {
        ConfiguracionListAdapter adapterCong = new ConfiguracionListAdapter();
        Configuracion config = new Configuracion();
        config.setCodigo("PORCENTAJE_IMPUESTO_PRODUCTO");
        adapterCong = adapterCong.fillData(config);
        confValor.setIdConfiguracion(adapterCong.getData().get(0).getId());
        adapterConfiguracionValor = adapterConfiguracionValor.fillData(confValor);
    }

    /* Método para obtener las marcas filtrados
     *
     * @param query
     * @return
     */
    public List<Marca> completeQuery(String query) {
        Marca marcaFilterAux = new Marca();
        marcaFilterAux.setDescripcion(query);
        marcaAdapterListAux = marcaAdapterListAux.fillData(marcaFilterAux);

        return marcaAdapterListAux.getData();
    }
    
      /* Método para obtener las marcas filtrados
     *
     * @param query
     * @return
     */
    public List<Metrica> completeQueryMetrica(String query) {
        Metrica metricaFilter = new Metrica();
        metricaFilter.setCodigo(query);
        metricaAdapter.setFirstResult(0);
        metricaAdapter.setPageSize(10000);
        metricaAdapter = metricaAdapter.fillData(metricaFilter);

        return metricaAdapter.getData();
    }


    /**
     * Selecciona marca
     *
     * @param event
     */
    public void onItemSelectMarcaFilter(SelectEvent event) {
        this.producto.setIdMarca(((Marca) event.getObject()).getId());
    }
    
      /**
     * Selecciona metrica
     *
     * @param event
     */
    public void onItemSelectMetricaFilter(SelectEvent event) {
        producto.setIdMetrica(((Metrica) event.getObject()).getId());
    }


    /**
     * Inicializa los datos
     */
    private void init(int id) {
        try {
            this.producto = service.get(id);
            this.adapterConfiguracionValor = new ConfiguracionValorListAdapter();
            this.marcaAdapterListAux = new MarcaAdapter();
            this.marcaFilterAux = new Marca();
            this.metricaAdapter = new MetricaAdapter();
            this.metrica = new Metrica();
            if (producto.getMetrica()!= null){
                this.metrica = producto.getMetrica();
            }
            if(producto.getCodigoGtin().equalsIgnoreCase("0")){
                producto.setCodigoGtin(null);
            }
            this.tipoProducto = new TipoProducto();
            this.tipoProductoAdapter = new TipoProductoAdapter();
            tipoProductoAdapter = tipoProductoAdapter.fillData(tipoProducto);
            this.confValor = new ConfiguracionValor();
            filterConfiguracionValor();
        } catch (Exception e) {
            System.err.print(e);
        }
    }

    /**
     * Constructor
     */
    public ProductoEditController() {
        this.service = new ProductoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }
}
