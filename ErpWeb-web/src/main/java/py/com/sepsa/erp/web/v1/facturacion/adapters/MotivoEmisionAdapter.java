/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.remote.MotivoEmisionService;

/**
 * Adapter para motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionAdapter extends DataListAdapter<MotivoEmision> {

    /**
     * Cliente remoto para tipo motivoEmision
     */
    private final MotivoEmisionService motivoEmisionClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MotivoEmisionAdapter fillData(MotivoEmision searchData) {

        return motivoEmisionClient.getMotivoEmision(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoEmisionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.motivoEmisionClient = new MotivoEmisionService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public MotivoEmisionAdapter() {
        super();
        this.motivoEmisionClient = new MotivoEmisionService();
    }
}
