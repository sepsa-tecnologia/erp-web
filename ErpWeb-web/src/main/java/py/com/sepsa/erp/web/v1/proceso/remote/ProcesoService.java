package py.com.sepsa.erp.web.v1.proceso.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.proceso.adapters.ProcesoAdapter;
import py.com.sepsa.erp.web.v1.proceso.filters.ProcesoFilter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 *
 * @author Jonathan Bernal
 */
public class ProcesoService extends APIErpCore {

    public ProcesoAdapter listar(Proceso proceso, Integer page, Integer pageSize) {

        ProcesoAdapter lista = new ProcesoAdapter(null);

        Map params = ProcesoFilter.build(proceso, page, pageSize);

        HttpURLConnection conn = GET(Resource.PROCESO.url, ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn, ProcesoAdapter.class);

            lista = (ProcesoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    public ProcesoService(String token) {
        super(token);
    }

    public enum Resource {

        //Servicio
        PROCESO("Proceso", "proceso");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
