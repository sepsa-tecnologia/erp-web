package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteTipoNegocio;
import py.com.sepsa.erp.web.v1.comercial.pojos.TipoNegocio;

/**
 * Filtro utilizado para el servicio de cliente tipo negocio
 *
 * @author Romina Núñez
 */
public class ClienteTipoNegocioFilter extends Filter {

    /**
     * Agrega el filtro de identificador del tipo negocio
     *
     * @param idTipoNegocio
     * @return
     */
    public ClienteTipoNegocioFilter idTipoNegocio(Integer idTipoNegocio) {
        if (idTipoNegocio != null) {
            params.put("idTipoNegocio", idTipoNegocio);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipo negocio
     *
     * @param tipoNegocio
     * @return
     */
    public ClienteTipoNegocioFilter tipoNegocio(String tipoNegocio) {
        if (tipoNegocio != null && !tipoNegocio.trim().isEmpty()) {
            params.put("tipoNegocio", tipoNegocio);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param idCliente
     * @return
     */
    public ClienteTipoNegocioFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de principal
     *
     * @param principal
     * @return
     */
    public ClienteTipoNegocioFilter principal(String principal) {
        if (principal != null && !principal.trim().isEmpty()) {
            params.put("principal", principal);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de principal
     *
     * @param tiene
     * @return
     */
    public ClienteTipoNegocioFilter tiene(String tiene) {
        if (tiene != null && !tiene.trim().isEmpty()) {
            params.put("tiene", tiene);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param clienteTipoNegocio datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ClienteTipoNegocio clienteTipoNegocio, Integer page, Integer pageSize) {
        ClienteTipoNegocioFilter filter = new ClienteTipoNegocioFilter();

        filter
                .idTipoNegocio(clienteTipoNegocio.getIdTipoNegocio())
                .tipoNegocio(clienteTipoNegocio.getTipoNegocio())
                .idCliente(clienteTipoNegocio.getIdCliente())
                .principal(clienteTipoNegocio.getPrincipal())
                .tiene(clienteTipoNegocio.getTiene())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ClienteTipoNegocioFilter
     */
    public ClienteTipoNegocioFilter() {
        super();
    }
}
