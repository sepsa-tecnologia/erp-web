/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.VendedorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.VendedorPojo;
import py.com.sepsa.erp.web.v1.info.remote.VendedorService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 *
 * @author Gustavo Benítez
 */
@ViewScoped
@Named("vendedorList")
public class VendedorListController implements Serializable {

    /**
     * Vendedor Filter
     */
    private VendedorPojo vendedorfilter;
    /**
     * Vendedor
     */
    private VendedorPojo vendedorNuevo;
    /**
     * Adaptador de vendedor
     */
    private VendedorListAdapter vendedorAdapter;
    /**
     * Servicio Cliente
     */
    private VendedorService serviceVendedor;
    /**
     * Adaptador de empresa.
     */
    private EmpresaAdapter adapterEmpresa;
    /**
     * Objeto empresa.
     */
    private Empresa empresa;
    /**
     * Objeto persona
     */
    private Persona persona;
    /**
     * Objeto persona nueva
     */
    private Persona personaNueva;
    /**
     * Adapter Persona
     */
    private PersonaListAdapter adapterPersona;

//<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public VendedorPojo getVendedorfilter() {
        return vendedorfilter;
    }
    
    public void setVendedorfilter(VendedorPojo vendedorfilter) {
        this.vendedorfilter = vendedorfilter;
    }
    
    public Persona getPersonaNueva() {
        return personaNueva;
    }
    
    public void setPersonaNueva(Persona personaNueva) {
        this.personaNueva = personaNueva;
    }

    public Persona getPersona() {
        return persona;
    }
    
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public PersonaListAdapter getAdapterPersona() {
        return adapterPersona;
    }
    
    public void setAdapterPersona(PersonaListAdapter adapterPersona) {
        this.adapterPersona = adapterPersona;
    }
    
    public Empresa getEmpresa() {
        return empresa;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public VendedorPojo getVendedorNuevo() {
        return vendedorNuevo;
    }
    
    public void setVendedorNuevo(VendedorPojo vendedorNuevo) {
        this.vendedorNuevo = vendedorNuevo;
    }
    
    public VendedorListAdapter getVendedorAdapter() {
        return vendedorAdapter;
    }
    
    public void setVendedorAdapter(VendedorListAdapter vendedorAdapter) {
        this.vendedorAdapter = vendedorAdapter;
    }
    
    public EmpresaAdapter getAdapterEmpresa() {
        return adapterEmpresa;
    }
    
    public void setAdapterEmpresa(EmpresaAdapter adapterEmpresa) {
        this.adapterEmpresa = adapterEmpresa;
    }
    
    public VendedorService getServiceVendedor() {
        return serviceVendedor;
    }
    
    public void setServiceVendedor(VendedorService serviceVendedor) {
        this.serviceVendedor = serviceVendedor;
    }
//</editor-fold>

    /**
     * Metodo para obtener la lista de vendedores
     */
    public void filterVendedor() {
        this.vendedorAdapter.setFirstResult(0);
        this.vendedorAdapter = vendedorAdapter.fillData(vendedorfilter);
    }

    /**
     * Método para obtener las empresas filtradas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeQueryEmpresa(String query) {
        Empresa emp = new Empresa();
        emp.setDescripcion(query);
        adapterEmpresa = adapterEmpresa.fillData(emp);
        return adapterEmpresa.getData();
    }

    /* Método para obtener personas
     *
     * @param query
     * @return
     */
    public List<Persona> completeQueryPersona(String query) {
        Persona per = new Persona();
        per.setNombre(query);
        
        this.adapterPersona = this.adapterPersona.fillData(per);
        return this.adapterPersona.getData();
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaFilter(SelectEvent event) {
        empresa.setId(((Empresa) event.getObject()).getId());
        this.vendedorfilter.setIdEmpresa(empresa.getId());
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectEmpresaCreate(SelectEvent event) {
        empresa.setId(((Empresa) event.getObject()).getId());
        this.vendedorNuevo.setIdEmpresa(empresa.getId());
    }

    /**
     * Selecciona la empresa
     *
     * @param event
     */
    public void onItemSelectPersonaFilter(SelectEvent event) {
        persona.setId(((Persona) event.getObject()).getId());
        this.vendedorfilter.setIdPersona(persona.getId());
    }

    /**
     * Método para reiniciar el formulario de la lista de datos
     */
    public void clearFilter() {
        this.vendedorfilter = new VendedorPojo();
        this.vendedorAdapter = new VendedorListAdapter();
        this.empresa = new Empresa();
        this.adapterEmpresa = new EmpresaAdapter();
        this.persona = new Persona();
        this.adapterPersona = new PersonaListAdapter();
        
        this.serviceVendedor = new VendedorService();
        filterVendedor();
    }

    /**
     * Método para crear Cliente
     */
    public void create() {
        this.personaNueva.setActivo("S");
        this.personaNueva.setCodigoTipoPersona("FISICA");
        
        this.vendedorNuevo.setActivo('S');
        this.vendedorNuevo.setPersona(personaNueva);
        this.vendedorNuevo.setNombre(personaNueva.getNombre());
        this.vendedorNuevo.setApellido(personaNueva.getApellido());
        
        BodyResponse<VendedorPojo> respuesta = serviceVendedor.create(vendedorNuevo);
        
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Vendedor creado correctamente!"));
            clearForm();
        }
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param idVendedor
     * @return
     */
    public String vendedorEditURL(Integer idVendedor) {
        return String.format("/app/vendedor/vendedor-edit.xhtml"
                + "?faces-redirect=true"
                + "&idVendedor=%d", idVendedor);
    }
    
    public void clearForm() {
        this.empresa = new Empresa();
        this.persona = new Persona();
        this.vendedorNuevo = new VendedorPojo();
        this.personaNueva = new Persona();
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.vendedorNuevo = new VendedorPojo();
            this.vendedorfilter = new VendedorPojo();
            this.vendedorAdapter = new VendedorListAdapter();
            this.empresa = new Empresa();
            this.adapterEmpresa = new EmpresaAdapter();
            this.persona = new Persona();
            this.personaNueva = new Persona();
            this.adapterPersona = new PersonaListAdapter();
            this.serviceVendedor = new VendedorService();
            filterVendedor();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
