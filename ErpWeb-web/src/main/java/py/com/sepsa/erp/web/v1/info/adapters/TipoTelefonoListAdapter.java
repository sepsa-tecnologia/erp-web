
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoTelefono;
import py.com.sepsa.erp.web.v1.info.remote.TipoTelefonoServiceClient;

/**
 * Adaptador de la lista Tipo telefono
 * @author Cristina Insfrán
 */
public class TipoTelefonoListAdapter extends DataListAdapter<TipoTelefono>{
   /**
     * Cliente para el servicio de tipo telefono
     */
    private final TipoTelefonoServiceClient tipoTelefonoClient;
 
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoTelefonoListAdapter fillData(TipoTelefono searchData) {
     
        return tipoTelefonoClient.getTipoTelefonoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de TipoTelefonoListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoTelefonoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoTelefonoClient = new TipoTelefonoServiceClient();
    }

    /**
     * Constructor de TipoTelefonoListAdapter
     */
    public TipoTelefonoListAdapter() {
        super();
        this.tipoTelefonoClient = new TipoTelefonoServiceClient();
    } 
}
