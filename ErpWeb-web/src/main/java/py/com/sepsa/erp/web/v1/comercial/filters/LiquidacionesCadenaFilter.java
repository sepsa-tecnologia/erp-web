package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionesCadena;

/**
 * Filtros para Liquidaciones por cadena.
 *
 * @author alext
 */
public class LiquidacionesCadenaFilter extends Filter {

    /**
     * Filtro por identificador de cliente.
     *
     * @param idCliente
     * @return
     */
    public LiquidacionesCadenaFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Filtro por cliente.
     *
     * @param cliente
     * @return
     */
    public LiquidacionesCadenaFilter cliente(String cliente) {
        if (cliente != null) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Filtro por linea.
     *
     * @param linea
     * @return
     */
    public LiquidacionesCadenaFilter linea(Integer linea) {
        if (linea != null) {
            params.put("linea", linea);
        }
        return this;
    }

    /**
     * Filtro por descuento.
     *
     * @param descuento
     * @return
     */
    public LiquidacionesCadenaFilter descuento(Integer descuento) {
        if (descuento != null) {
            params.put("descuento", descuento);
        }
        return this;
    }

    /**
     * Filtro por monto total.
     *
     * @param total
     * @return
     */
    public LiquidacionesCadenaFilter total(Integer total) {
        if (total != null) {
            params.put("total", total);
        }
        return this;
    }
    /**
     * Filtro por año.
     * @param ano
     * @return 
     */
    public LiquidacionesCadenaFilter ano(String ano) {
        if (ano != null && !ano.trim().isEmpty()) {
            params.put("ano", ano);
        }
        return this;
    }
    /**
     * Filtro por mes.
     * @param mes
     * @return 
     */
    public LiquidacionesCadenaFilter mes(String mes) {
        if (mes != null && !mes.trim().isEmpty()) {
            params.put("mes", mes);
        }
        return this;
    }
    
    

    /**
     * Construye el mapa de parámetros.
     *
     * @param liquidacionesCadena datos del filtro.
     * @param page página buscada en el resultado.
     * @param pageSize tamaño de la página de resultado.
     * @return
     */
    public static Map build(LiquidacionesCadena liquidacionesCadena, Integer page, Integer pageSize) {
        LiquidacionesCadenaFilter filter = new LiquidacionesCadenaFilter();

        filter
                .idCliente(liquidacionesCadena.getIdCliente())
                .cliente(liquidacionesCadena.getCliente())
                .linea(liquidacionesCadena.getLinea())
                .descuento(liquidacionesCadena.getDescuento())
                .total(liquidacionesCadena.getTotal())
                .ano(liquidacionesCadena.getAno())
                .mes(liquidacionesCadena.getMes())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
    
    /**
     * Constructor de LiquidacionesCadenaFilter.
     */
    public LiquidacionesCadenaFilter(){
        super();
    }

}
