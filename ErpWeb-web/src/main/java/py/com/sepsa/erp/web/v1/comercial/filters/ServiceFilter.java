
package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicio;

/**
 * Filtro utilizado para el servicio
 * @author Cristina Insfrán
 */
public class ServiceFilter extends Filter{
   
    /**
     * Agrega el filtro de identificador del servicio
     *
     * @param id Identificador del servicio
     * @return
     */
    public ServiceFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
      /**
     * Agrega el filtro para la descripción
     *
     * @param descripcion
     * @return
     */
    public ServiceFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
     /**
     * Construye el mapa de parametros
     *
     * @param service datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(Servicio service, Integer page, Integer pageSize) {
        ServiceFilter filter = new ServiceFilter();

        filter
                .id(service.getId())
                .descripcion(service.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }
    
     /**
     * Constructor de ServiceFilter
     */
    public ServiceFilter() {
        super();
    }

}
