package py.com.sepsa.erp.web.v1.tipoEtiqueta.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.TipoEtiquetaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;

/**
 * Controlador para tipo etiqueta.
 *
 * @author alext
 */
@ViewScoped
@Named("listarTipoEtiqueta")
public class TipoEtiquetaListarController implements Serializable {

    /**
     * Adaptador de servicios.
     */
    private TipoEtiquetaAdapter adapter;

    /**
     * Objeto Servicios.
     */
    private TipoEtiqueta searchData;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public TipoEtiquetaAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(TipoEtiquetaAdapter adapter) {
        this.adapter = adapter;
    }

    public TipoEtiqueta getSearchData() {
        return searchData;
    }

    public void setSearchData(TipoEtiqueta searchData) {
        this.searchData = searchData;
    }
    //</editor-fold>
    
    /**
     * Método para filtrar etiqueta.
     */
    public void filter() {
        this.adapter = adapter.fillData(searchData);
    }
    
    /**
     * Método para limpiar filtros.
     */
    public void limpiar(){
        searchData = new TipoEtiqueta();
        filter();
    }

    /**
     * Método para generar lista de etiquetas.
     */
    public void search() {
        adapter = new TipoEtiquetaAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para reiniciar la lista de datos.
     */
    public void clear() {
        searchData = new TipoEtiqueta();
        search();
    }

    @PostConstruct
    public void init() {
        clear();
    }

    /**
     * Inicializa los datos del controlador.
     */
    public TipoEtiquetaListarController() {
        adapter = new TipoEtiquetaAdapter();
        searchData = new TipoEtiqueta();
    }

}
