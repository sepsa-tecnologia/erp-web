package py.com.sepsa.erp.web.v1.converter;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.remote.PersonaServiceClient;

/**
 * Convertidor para datos de autocomplete
 * @author alext
 */
@FacesConverter("personaPojoConverter")
public class PersonaPojoConverter implements Converter{
    
      /**
     * Servicios para login al sistema
     */
    private PersonaServiceClient serviceClient;
    
    @Override
    public Persona getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && !value.trim().isEmpty() && !value.trim().equalsIgnoreCase("null")) {
            serviceClient = new PersonaServiceClient();

            Integer val = null;
            try {
                val = Integer.valueOf(value);
            } catch(Exception ex) {}
            
            Persona per=new Persona();
            per.setId(val);
          
            List<Persona> persona = 
                    serviceClient.getPersonaList(per, 0, 10).getData();
            if(persona != null && !persona.isEmpty()) {
                return persona.get(0);
            } else {
                throw new ConverterException(new FacesMessage(FacesMessage
                        .SEVERITY_ERROR, "Error", "No es una persona válida"));
            }
        } else {
            return null;
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            if(object instanceof Persona) {
                return String.valueOf(((Persona)object).getId());
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }   
}
