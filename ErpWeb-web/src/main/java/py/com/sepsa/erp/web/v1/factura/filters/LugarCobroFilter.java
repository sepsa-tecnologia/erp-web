
package py.com.sepsa.erp.web.v1.factura.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;

/**
 * Filtro para la lista de lugar de cobro
 * @author Cristina Insfrán
 */
public class LugarCobroFilter extends Filter{
    
     /**
     * Agrega el filtro de identificador de producto
     *
     * @param id
     * @return
     */
    public LugarCobroFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de tipo tarifa
     *
     * @param descripcion
     * @return
     */
    public LugarCobroFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param lugarCobro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(LugarCobro lugarCobro, Integer page, Integer pageSize) {
        LugarCobroFilter filter = new LugarCobroFilter();

        filter
                .id(lugarCobro.getId())
                .descripcion(lugarCobro.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de LugarCobroFilter
     */
    public LugarCobroFilter() {
        super();
    }
}
