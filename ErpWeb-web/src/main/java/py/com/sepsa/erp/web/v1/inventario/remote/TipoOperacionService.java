/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.TipoOperacionAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.TipoOperacionFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Cliente para el servicio de Tipo Operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoOperacionService extends APIErpInventario {

    /**
     * Obtiene la lista
     *
     * @param tipoOperacion Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public TipoOperacionAdapter getTipoOperacion(TipoOperacion tipoOperacion, Integer page,
            Integer pageSize) {
        TipoOperacionAdapter lista = new TipoOperacionAdapter();
        try {
            Map params = TipoOperacionFilter.build(tipoOperacion, page, pageSize);
            HttpURLConnection conn = GET(Resource.TIPO_OPERACION.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        TipoOperacionAdapter.class);
                lista = (TipoOperacionAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return lista;
    }

    /**
     * Método para crear
     *
     * @param tipoOperacion
     * @return
     */
    public BodyResponse<TipoOperacion> setTipoOperacion(TipoOperacion tipoOperacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.TIPO_OPERACION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(tipoOperacion));
            response = BodyResponse.createInstance(conn, TipoOperacion.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param tipoOperacion
     * @return
     */
    public BodyResponse<TipoOperacion> editTipoOperacion(TipoOperacion tipoOperacion) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.TIPO_OPERACION.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(tipoOperacion));
            response = BodyResponse.createInstance(conn, TipoOperacion.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public TipoOperacion get(Integer id) {
        TipoOperacion data = new TipoOperacion(id);
        TipoOperacionAdapter list = getTipoOperacion(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public TipoOperacionService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        TIPO_OPERACION("TipoOperacion", "tipo-operacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
