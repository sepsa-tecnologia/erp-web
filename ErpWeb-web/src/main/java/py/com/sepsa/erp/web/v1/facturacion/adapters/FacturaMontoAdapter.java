
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista de factura
 * @author Cristina Insfrán
 */
public class FacturaMontoAdapter extends DataListAdapter<Factura>{
   
     /**
     * Cliente para el servicio de facturacion
     */
    private final FacturacionService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public FacturaMontoAdapter fillData(Factura searchData) {

        return serviceFactura.getFacturaMontoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaMontoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FacturaMontoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturacionService();
    }

    /**
     * Constructor de FacturaMontoAdapter
     */
    public FacturaMontoAdapter() {
        super();
        this.serviceFactura = new FacturacionService();
    }
}
