/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para la vista Listar orden de compra
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("ordenDeCompraList")
public class OrdenDeCompraListController implements Serializable {

    /**
     * Adaptador para la lista de orden de compra
     */
    private OrdenDeCompraAdapter ordenDeCompraAdapterList;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompraFilter;
    /**
     * Adaptador para estado
     */
    private EstadoAdapter estadoAdapterList;
    /**
     * Pojo de estado
     */
    private Estado estado;
    /**
     * Service Orden de Compra
     */
    private OrdenDeCompraService serviceOrdenCompra;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the ordenDeCompraAdapterList
     */
    public OrdenDeCompraAdapter getOrdenDeCompraAdapterList() {
        return ordenDeCompraAdapterList;
    }

    public void setServiceOrdenCompra(OrdenDeCompraService serviceOrdenCompra) {
        this.serviceOrdenCompra = serviceOrdenCompra;
    }

    public OrdenDeCompraService getServiceOrdenCompra() {
        return serviceOrdenCompra;
    }

    /**
     * @param ordenDeCompraAdapterList the ordenDeCompraAdapterList to set
     */
    public void setOrdenDeCompraAdapterList(OrdenDeCompraAdapter ordenDeCompraAdapterList) {
        this.ordenDeCompraAdapterList = ordenDeCompraAdapterList;
    }

    /**
     * @return the ordenDeCompraFilter
     */
    public OrdenDeCompra getOrdenDeCompraFilter() {
        return ordenDeCompraFilter;
    }

    /**
     * @param ordenDeCompraFilter the ordenDeCompraFilter to set
     */
    public void setOrdenDeCompraFilter(OrdenDeCompra ordenDeCompraFilter) {
        this.ordenDeCompraFilter = ordenDeCompraFilter;
    }

    public EstadoAdapter getEstadoAdapterList() {
        return estadoAdapterList;
    }

    public void setEstadoAdapterList(EstadoAdapter estadoAdapterList) {
        this.estadoAdapterList = estadoAdapterList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    //</editor-fold>
    /**
     * Método para obtener lista de ordenes de Compra
     */
    public void getList() {
        ordenDeCompraFilter.setRecibido("S");
        this.ordenDeCompraAdapterList = ordenDeCompraAdapterList.fillData(ordenDeCompraFilter);
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.ordenDeCompraFilter = new OrdenDeCompra();
        getList();
    }

    /**
     * Metodo para obtener la lista de estados
     */
    public void getEstados() {
        this.setEstadoAdapterList(getEstadoAdapterList().fillData(getEstado()));
    }

    /**
     * Metodo para redirigir a la vista de detalles
     *
     * @param id
     * @return
     */
    public String viewDetail(Integer id) {
        return String.format("orden-de-compra-detalles?faces-redirect=true&id=%d", id);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String facturaURL(Integer idOrdenCompra) {
        return String.format("/app/factura/factura-create.xhtml"
                + "?faces-redirect=true"
                + "&idOC=%d", idOrdenCompra);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String facturaCompraURL(Integer idOrdenCompra) {
        return String.format("/app/factura/factura-compra-create.xhtml"
                + "?faces-redirect=true"
                + "&idOC=%d", idOrdenCompra);
    }

    /**
     * Método para rechazar orden de compra
     *
     * @param id
     */
    public void rechazarOrdenCompra(OrdenDeCompra orden) {
        OrdenDeCompra oc = new OrdenDeCompra();

        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoOC = new Estado();
        estadoOC.setCodigo("RECHAZADO");
        estadoOC.setCodigoTipoEstado("ORDEN_COMPRA");
        adapterEstado = adapterEstado.fillData(estadoOC);

        oc = orden;
        oc.setIdEstado(adapterEstado.getData().get(0).getId());

        BodyResponse<OrdenDeCompra> respuestaOC = serviceOrdenCompra.editarOrdenCompra(oc);
        if (respuestaOC.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Orden de Compra rechazada!"));
            clear();

        } else {
            for (Mensaje mensaje : respuestaOC.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", mensaje.getDescripcion()));
            }
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.serviceOrdenCompra = new OrdenDeCompraService();
            this.ordenDeCompraFilter = new OrdenDeCompra();
            this.setOrdenDeCompraAdapterList(new OrdenDeCompraAdapter());
            this.estadoAdapterList = new EstadoAdapter();
            this.estado = new Estado();
            this.estado.setCodigoTipoEstado("ORDEN_COMPRA");
            getEstados();
            clear();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
