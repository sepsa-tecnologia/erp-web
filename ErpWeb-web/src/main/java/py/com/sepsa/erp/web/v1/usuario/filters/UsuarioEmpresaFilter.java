/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioEmpresa;

/**
 * Filter Usuario-Empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class UsuarioEmpresaFilter extends Filter {

    /**
     * Agrega el filtro de identificador del cliente
     *
     * @param id Identificador del usuario
     * @return
     */
    public UsuarioEmpresaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del usuario
     *
     * @param idUsuario Identificador del usuario
     * @return
     */
    public UsuarioEmpresaFilter idUsuario(Integer idUsuario) {
        if (idUsuario != null) {
            params.put("idUsuario", idUsuario);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la empresa
     *
     * @param idEmpresa Identificador de la empresa
     * @return
     */
    public UsuarioEmpresaFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo indicador de activo
     * @return
     */
    public UsuarioEmpresaFilter activo(String activo) {
        if (activo != null && !activo.isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param usuarioEmpresa
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(UsuarioEmpresa usuarioEmpresa, Integer page, Integer pageSize) {

        UsuarioEmpresaFilter filter = new UsuarioEmpresaFilter();

        filter
                .idUsuario(usuarioEmpresa.getIdUsuario())
                .idEmpresa(usuarioEmpresa.getIdEmpresa())
                .id(usuarioEmpresa.getId())
                .activo(usuarioEmpresa.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de UsuarioPerfilFilter
     */
    public UsuarioEmpresaFilter() {
        super();
    }

}
