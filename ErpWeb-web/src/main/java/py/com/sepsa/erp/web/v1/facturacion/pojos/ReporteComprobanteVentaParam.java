package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;

/**
 * POJO para reporte de comprobante de venta
 *
 * @author Romina Núñez
 */
public class ReporteComprobanteVentaParam {

    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    
    private Boolean anual;
    /**
     * Fecha
     */
    private Date fecha;

    public Boolean getAnual() {
        return anual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setAnual(Boolean anual) {
        this.anual = anual;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
   
    /**
     * Constructor
     */
    public ReporteComprobanteVentaParam() {

    }
}
