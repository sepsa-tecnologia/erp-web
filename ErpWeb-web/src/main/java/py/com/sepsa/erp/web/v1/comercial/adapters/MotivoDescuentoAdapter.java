
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.MotivoDescuento;
import py.com.sepsa.erp.web.v1.comercial.remote.MotivoDescuentoService;

/**
 * Adaptador para la lista de Descuento
 * @author Romina Núñez
 */

public class MotivoDescuentoAdapter extends DataListAdapter<MotivoDescuento> {
    
    /**
     * Cliente para los servicios de Documento
     */
    private final MotivoDescuentoService serviceMotivoDescuento;

    /**
     * Constructor de InstallationListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoDescuentoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceMotivoDescuento = new MotivoDescuentoService();
    }

    /**
     * Constructor de DocumentAdapter
     */
        public MotivoDescuentoAdapter() {
        super();
        this.serviceMotivoDescuento= new MotivoDescuentoService();
    }


    @Override
    public MotivoDescuentoAdapter fillData(MotivoDescuento searchData) {
        return serviceMotivoDescuento.getMotivoDescuentoList(searchData, getFirstResult(), getPageSize());
    }

    
}
