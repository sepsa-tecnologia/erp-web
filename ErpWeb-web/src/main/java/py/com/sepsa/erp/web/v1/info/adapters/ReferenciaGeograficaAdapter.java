
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.remote.ReferenciaGeograficaService;

/**
 * Adaptador de la referencia Geografica
 * @author Cristina Insfrán
 */
public class ReferenciaGeograficaAdapter extends DataListAdapter<ReferenciaGeografica>{
     /**
     * Cliente remoto para Referencia Geografica
     */
    private final ReferenciaGeograficaService service;

     /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ReferenciaGeograficaAdapter fillData(ReferenciaGeografica searchData) {

        return service.getRefGeograficaList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de ReferenciaGeograficaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ReferenciaGeograficaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new ReferenciaGeograficaService();
    }

    /**
     * Constructor de ReferenciaGeograficaAdapter
     */
    public ReferenciaGeograficaAdapter() {
        super();
        this.service = new ReferenciaGeograficaService();
    } 
}
