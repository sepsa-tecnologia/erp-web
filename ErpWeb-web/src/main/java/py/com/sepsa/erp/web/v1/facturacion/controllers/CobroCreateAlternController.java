package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Cheque;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.EntidadFinancieraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LugarCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaEmailAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Email;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;

/**
 *
 * @author SONY
 */
@ViewScoped
@Named("cobroCreateAltern")
public class CobroCreateAlternController implements Serializable {

    /**
     * Variable para visualizar panel
     */
    private boolean viewPanel = false;
    /**
     * Objeto Cliente
     */
    private Cliente client;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto para crear el cobro
     */
    private Cobro cobro;
    /**
     * Cliente remoto para servicio de cobro
     */
    private CobroService cobroClient;
    /**
     * Lista para cobro
     */
    //private List<Cobro> selectedCobros;
    /**
     * Variable global para monto cobro
     */
    private BigDecimal montoCobroTotal = BigDecimal.ZERO;
    /**
     * Lista para lugar cobro
     */
    private List<SelectItem> listLugarCobro = new ArrayList<>();
    /**
     * Adaptador de la lista de lugar cobro
     */
    private LugarCobroListAdapter adapterLugarCobro;
    /**
     * Objeto lugarCobro
     */
    private LugarCobro lugarCobro;
    /**
     * Fecha del recibo
     */
    private Date reciboDate;
    /**
     * Número del recibo
     */
    private String reciboNum = "";
    /**
     * Lugar para el pago seleccionado
     */
    private Integer selectedLugarPago;
    /**
     * Objeto cobro detalle
     */
    private CobroDetalle cobroDetalle;
    /**
     * Lista para tipo de cobro
     */
    private List<SelectItem> listTipoCobro = new ArrayList<>();
    /**
     * Adaptador de la lista de tipo cobro
     */
    private TipoCobroListAdapter adapterTipoCobro;
    /**
     * Pojo tipo cobro
     */
    private TipoCobro tipoCobro;
    /**
     * Adaptador para la lista de entidad financiera
     */
    private EntidadFinancieraListAdapter adapterEntidadFinanciera;
    /**
     * Objeto Entidad Financiera
     */
    private EntidadFinanciera entidadFinanciera;
    /**
     * Variable para guardar el valor de nro de factura
     */
    private String nroFactura;
    /**
     * Adaptador de la lista de cobro detalle
     */
    private CobroDetalleListAdapter adapterCobroDetalle;
    /**
     * Objeto detalle nota de credito
     */
    private NotaCreditoDetalle notaCreditoDetalle;
    /**
     * Adaptador de la lista del detalle de nota de credito
     */
    private NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle;
    private BigDecimal test;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showDescargaPopup;
    /**
     * Dato a ser utilizado para la descarga
     */
    private Integer idCobroCreado;
    /**
     * Dato a ser utilizado para la descarga
     */
    private String nroCobroCreads;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    private EntidadFinanciera entFinanSelected;
    private BigDecimal saldoGeneral;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Cobro Talonario
     */
    private TalonarioPojo cobroTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Adapter Cobro Talonario
     */
    private CobroTalonarioAdapter adapterCobroTalonario;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    private DatosCobro datoCobro;
    private Integer idTalonario;
    /**
     * POJO de PersonaEmail
     */
    private PersonaEmail personaEmail;
    /**
     * Adapter de PersonaEmail
     */
    private PersonaEmailAdapter adapterPersonaEmail;
    /**
     * Email que recibira el recibo
     */
    private String emailNotificacion;

    private List<PersonaEmail> emailList;
    /**
     * Lista de Emails que recibiran el recibo
     */
    private List<Email> selectedEmail;
    /**
     * Input para agregar un email a la lista de emails para notificar
     */
    private String addEmail;
    /**
     * Auxiliar para saber si se debe notificar por email
     */
    private String notificar;

    private int index;

    private boolean cobroPorTotal;

    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * POJO TipoCambio
     */
    private TipoCambio tipoCambio;
    /**
     * Monto total del cobro en Guaraníes
     */
    private BigDecimal montoCobroTotalGs;
    /**
     * Bandera para validar datos de cobro con cheque
     */
    private String omitirValidacionCheque;
    /**
     * Lazy Data Model de factura
     */
    private FacturaDataModel lazyModel;
    /**
     * Bandera para cobros en Guaraníes de facturas con moneda extranjera
     */
    private boolean cobroEnGs;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public boolean isCobroEnGs() {
        return cobroEnGs;
    }

    public void setCobroEnGs(boolean cobroEnGs) {
        this.cobroEnGs = cobroEnGs;
    }

    public FacturaDataModel getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(FacturaDataModel lazyModel) {
        this.lazyModel = lazyModel;
    }

    public String getOmitirValidacionCheque() {
        return omitirValidacionCheque;
    }

    public void setOmitirValidacionCheque(String omitirValidacionCheque) {
        this.omitirValidacionCheque = omitirValidacionCheque;
    }

    public BigDecimal getMontoCobroTotalGs() {
        return montoCobroTotalGs;
    }

    public void setMontoCobroTotalGs(BigDecimal montoCobroTotalGs) {
        this.montoCobroTotalGs = montoCobroTotalGs;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public boolean isCobroPorTotal() {
        return cobroPorTotal;
    }

    public void setCobroPorTotal(boolean cobroPorTotal) {
        this.cobroPorTotal = cobroPorTotal;
    }

    /**
     * @return the adapterNotaCreditoDetalle
     */
    public NotaCreditoDetalleListAdapter getAdapterNotaCreditoDetalle() {
        return adapterNotaCreditoDetalle;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setDatoCobro(DatosCobro datoCobro) {
        this.datoCobro = datoCobro;
    }

    public DatosCobro getDatoCobro() {
        return datoCobro;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public TalonarioPojo getCobroTalonario() {
        return cobroTalonario;
    }

    public void setCobroTalonario(TalonarioPojo cobroTalonario) {
        this.cobroTalonario = cobroTalonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public CobroTalonarioAdapter getAdapterCobroTalonario() {
        return adapterCobroTalonario;
    }

    public void setAdapterCobroTalonario(CobroTalonarioAdapter adapterCobroTalonario) {
        this.adapterCobroTalonario = adapterCobroTalonario;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public void setEntFinanSelected(EntidadFinanciera entFinanSelected) {
        this.entFinanSelected = entFinanSelected;
    }

    public EntidadFinanciera getEntFinanSelected() {
        return entFinanSelected;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public boolean isShowDescargaPopup() {
        return showDescargaPopup;
    }

    public String getNroCobroCreads() {
        return nroCobroCreads;
    }

    public void setShowDescargaPopup(boolean showDescargaPopup) {
        this.showDescargaPopup = showDescargaPopup;
    }

    public void setNroCobroCreads(String nroCobroCreads) {
        this.nroCobroCreads = nroCobroCreads;
    }

    public void setIdCobroCreado(Integer idCobroCreado) {
        this.idCobroCreado = idCobroCreado;
    }

    public Integer getIdCobroCreado() {
        return idCobroCreado;
    }

    public void setTest(BigDecimal test) {
        this.test = test;
    }

    public BigDecimal getTest() {
        return test;
    }

    /**
     * @param adapterNotaCreditoDetalle the adapterNotaCreditoDetalle to set
     */
    public void setAdapterNotaCreditoDetalle(NotaCreditoDetalleListAdapter adapterNotaCreditoDetalle) {
        this.adapterNotaCreditoDetalle = adapterNotaCreditoDetalle;
    }

    /**
     * @return the notaCreditoDetalle
     */
    public NotaCreditoDetalle getNotaCreditoDetalle() {
        return notaCreditoDetalle;
    }

    /**
     * @param notaCreditoDetalle the notaCreditoDetalle to set
     */
    public void setNotaCreditoDetalle(NotaCreditoDetalle notaCreditoDetalle) {
        this.notaCreditoDetalle = notaCreditoDetalle;
    }

    /**
     * @return the adapterCobroDetalle
     */
    public CobroDetalleListAdapter getAdapterCobroDetalle() {
        return adapterCobroDetalle;
    }

    /**
     * @param adapterCobroDetalle the adapterCobroDetalle to set
     */
    public void setAdapterCobroDetalle(CobroDetalleListAdapter adapterCobroDetalle) {
        this.adapterCobroDetalle = adapterCobroDetalle;
    }

    /**
     * @return the nroFactura
     */
    public String getNroFactura() {
        return nroFactura;
    }

    /**
     * @param nroFactura the nroFactura to set
     */
    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    /**
     * @return the adapterEntidadFinanciera
     */
    public EntidadFinancieraListAdapter getAdapterEntidadFinanciera() {
        return adapterEntidadFinanciera;
    }

    /**
     * @param adapterEntidadFinanciera the adapterEntidadFinanciera to set
     */
    public void setAdapterEntidadFinanciera(EntidadFinancieraListAdapter adapterEntidadFinanciera) {
        this.adapterEntidadFinanciera = adapterEntidadFinanciera;
    }

    /**
     * @return the entidadFinanciera
     */
    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    /**
     * @param entidadFinanciera the entidadFinanciera to set
     */
    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    /**
     * @return the tipoCobro
     */
    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    /**
     * @param tipoCobro the tipoCobro to set
     */
    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    /**
     * @return the adapterTipoCobro
     */
    public TipoCobroListAdapter getAdapterTipoCobro() {
        return adapterTipoCobro;
    }

    /**
     * @param adapterTipoCobro the adapterTipoCobro to set
     */
    public void setAdapterTipoCobro(TipoCobroListAdapter adapterTipoCobro) {
        this.adapterTipoCobro = adapterTipoCobro;
    }

    /**
     * @return the listTipoCobro
     */
    public List<SelectItem> getListTipoCobro() {
        return listTipoCobro;
    }

    /**
     * @param listTipoCobro the listTipoCobro to set
     */
    public void setListTipoCobro(List<SelectItem> listTipoCobro) {
        this.listTipoCobro = listTipoCobro;
    }

    /**
     * @return the cobroDetalle
     */
    public CobroDetalle getCobroDetalle() {
        return cobroDetalle;
    }

    /**
     * @param cobroDetalle the cobroDetalle to set
     */
    public void setCobroDetalle(CobroDetalle cobroDetalle) {
        this.cobroDetalle = cobroDetalle;
    }

    /**
     * @return the reciboDate
     */
    public Date getReciboDate() {
        return reciboDate;
    }

    /**
     * @param reciboDate the reciboDate to set
     */
    public void setReciboDate(Date reciboDate) {
        this.reciboDate = reciboDate;
    }

    /**
     * @return the reciboNum
     */
    public String getReciboNum() {
        return reciboNum;
    }

    /**
     * @param reciboNum the reciboNum to set
     */
    public void setReciboNum(String reciboNum) {
        this.reciboNum = reciboNum;
    }

    /**
     * @return the selectedLugarPago
     */
    public Integer getSelectedLugarPago() {
        return selectedLugarPago;
    }

    /**
     * @param selectedLugarPago the selectedLugarPago to set
     */
    public void setSelectedLugarPago(Integer selectedLugarPago) {
        this.selectedLugarPago = selectedLugarPago;
    }

    /**
     * @return the lugarCobro
     */
    public LugarCobro getLugarCobro() {
        return lugarCobro;
    }

    /**
     * @param lugarCobro the lugarCobro to set
     */
    public void setLugarCobro(LugarCobro lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    /**
     * @return the adapterLugarCobro
     */
    public LugarCobroListAdapter getAdapterLugarCobro() {
        return adapterLugarCobro;
    }

    /**
     * @param adapterLugarCobro the adapterLugarCobro to set
     */
    public void setAdapterLugarCobro(LugarCobroListAdapter adapterLugarCobro) {
        this.adapterLugarCobro = adapterLugarCobro;
    }

    /**
     * @return the listLugarCobro
     */
    public List<SelectItem> getListLugarCobro() {
        return listLugarCobro;
    }

    /**
     * @param listLugarCobro the listLugarCobro to set
     */
    public void setListLugarCobro(List<SelectItem> listLugarCobro) {
        this.listLugarCobro = listLugarCobro;
    }

    /**
     * @return the montoCobroTotal
     */
    public BigDecimal getMontoCobroTotal() {
        return montoCobroTotal;
    }

    /**
     * @param montoCobroTotal the montoCobroTotal to set
     */
    public void setMontoCobroTotal(BigDecimal montoCobroTotal) {
        this.montoCobroTotal = montoCobroTotal;
    }

    /**
     * @return the cobroClient
     */
    public CobroService getCobroClient() {
        return cobroClient;
    }

    /**
     * @param cobroClient the cobroClient to set
     */
    public void setCobroClient(CobroService cobroClient) {
        this.cobroClient = cobroClient;
    }

    /**
     * @return the cobro
     */
    public Cobro getCobro() {
        return cobro;
    }

    /**
     * @param cobro the cobro to set
     */
    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    /**
     * @return the viewPanel
     */
    public boolean isViewPanel() {
        return viewPanel;
    }

    /**
     * @param viewPanel the viewPanel to set
     */
    public void setViewPanel(boolean viewPanel) {
        this.viewPanel = viewPanel;
    }

    /**
     * @return the client
     */
    public Cliente getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Cliente client) {
        this.client = client;
    }

    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public PersonaEmail getPersonaEmail() {
        return personaEmail;
    }

    public void setPersonaEmail(PersonaEmail personaEmail) {
        this.personaEmail = personaEmail;
    }

    public PersonaEmailAdapter getAdapterPersonaEmail() {
        return adapterPersonaEmail;
    }

    public void setAdapterPersonaEmail(PersonaEmailAdapter adapterPersonaEmail) {
        this.adapterPersonaEmail = adapterPersonaEmail;
    }

    public String getEmailNotificacion() {
        return emailNotificacion;
    }

    public void setEmailNotificacion(String emailNotificacion) {
        this.emailNotificacion = emailNotificacion;
    }

    public String getAddEmail() {
        return addEmail;
    }

    public void setAddEmail(String addEmail) {
        this.addEmail = addEmail;
    }

    public List<PersonaEmail> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<PersonaEmail> emailList) {
        this.emailList = emailList;
    }

    public String getNotificar() {
        return notificar;
    }

    public void setNotificar(String notificar) {
        this.notificar = notificar;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<Email> getSelectedEmail() {
        return selectedEmail;
    }

    public void setSelectedEmail(List<Email> selectedEmail) {
        this.selectedEmail = selectedEmail;
    }

//</editor-fold>
    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        client = new Cliente();
        client.setRazonSocial(query);
        adapterCliente = adapterCliente.fillData(client);

        return adapterCliente.getData();
    }

    /**
     * Método autocomplete personaEmail
     *
     * @param query
     * @return
     */
    public List<PersonaEmail> completeQueryEmail(String query) {
        personaEmail = new PersonaEmail();
        personaEmail.setIdPersona(client.getIdCliente());
        personaEmail.setActivo("S");
        adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmail);

        return adapterPersonaEmail.getData();
    }

    /**
     * Detalle de lista de facturas pendientes y canceladas del cliente
     * seleccionado
     */
    public void onItemSelectCliente() {
        viewPanel = true;
        cobro.setIdCliente(client.getIdCliente());
        cobro.setSaldoCliente(BigDecimal.ZERO);
        cobro.setCodigoMoneda("PYG");
        
        adapterLugarCobro = adapterLugarCobro.fillData(lugarCobro);
        for (int i = 0; i < adapterLugarCobro.getData().size(); i++) {
            listLugarCobro.add(new SelectItem(adapterLugarCobro.getData().get(i).getId(), adapterLugarCobro.getData().get(i).getCodigo()));

        }
        selectedLugarPago = (Integer) listLugarCobro.get(0).getValue();
        
        lazyModel.getFiltro().setIdCliente(client.getIdCliente());
        lazyModel.getFiltro().setCobrado("N");
        lazyModel.getFiltro().setAnulado("N");
        lazyModel.getFiltro().setTieneSaldo("true");
        lazyModel.load(0, 10, null, null, null);

        if (notificar.equalsIgnoreCase("S")) {
            personaEmail = new PersonaEmail();
            this.adapterPersonaEmail = new PersonaEmailAdapter();
            this.emailList = new ArrayList();
            this.selectedEmail = new ArrayList();
            personaEmail.setIdPersona(client.getIdCliente());
            personaEmail.setActivo("S");
            adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmail);
            this.emailList = adapterPersonaEmail.getData();
        }

    }

    public void onItemSelectEmail() {
        this.emailNotificacion = personaEmail.getEmail().getEmail();
    }

    public void obtenerDatosCobro() {
        try {

            Map parametros = new HashMap();
            datoCobro = new DatosCobro();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", digital);
            if (cobro.getIdCliente() != null) {
                parametros.put("idCliente", cobro.getIdCliente());
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoCobro = getCobroClient().getDatoCobro(parametros);

            if (datoCobro != null) {
                cobro.setFecha(today.getTime());
                cobro.setIdTalonario(datoCobro.getIdTalonario());
                cobro.setNroRecibo(datoCobro.getNroRecibo());

                idTalonario = datoCobro.getIdTalonario();
                reciboDate = today.getTime();
                reciboNum = datoCobro.getNroRecibo();

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método invocado al cambiar el monto a cobrar
     *
     * @param event Evento producido al cambiar el monto a cobrar
     */
    public void onChangeChargeAmount(ValueChangeEvent event) {
        WebLogger.get().debug("ENTRA CHEQUE");

        BigDecimal oldVal = new BigDecimal(event.getOldValue() == null ? "0"
                : event.getOldValue().toString());
        BigDecimal newVal = new BigDecimal(event.getNewValue() == null ? "0"
                : event.getNewValue().toString());

        cobro.setSaldoCliente(cobro.
                getSaldoCliente().add(oldVal));
        cobro.setSaldoCliente(cobro.
                getSaldoCliente().subtract(newVal));

        montoCobroTotal = montoCobroTotal.subtract(oldVal);
        montoCobroTotal = montoCobroTotal.add(newVal);
        if (!cobro.getCodigoMoneda().equalsIgnoreCase("PYG")) {
            BigDecimal oldValGs = oldVal.multiply(tipoCambio.getVenta());
            BigDecimal newValGs = newVal.multiply(tipoCambio.getVenta());

            montoCobroTotalGs = montoCobroTotalGs.subtract(oldValGs);
            montoCobroTotalGs = montoCobroTotalGs.add(newValGs);

        }
    }

    /**
     * Método para borrar linea de detalle de cobro
     */
    public void codTipoCobro(Integer nroLinea) {
        for (int j = 0; j < cobro.getCobroDetalles().size(); j++) {
            CobroDetalle cd = cobro.getCobroDetalles().get(j);
            if (cd.getNroLinea() == nroLinea) {
                cd.setCodigoTipoCobro(getCodTipoCobro(cd.getIdTipoCobro()));
                if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                    Calendar today = Calendar.getInstance();
                    Cheque ch = new Cheque();
                    EntidadFinanciera ef = new EntidadFinanciera();
                    ch.setOmitirValidacionCheque(omitirValidacionCheque);
                    ch.setMontoCheque(BigDecimal.ZERO);
                    ch.setNroCheque("");
                    ch.setFechaEmision(today.getTime());
                    ch.setFechaPago(today.getTime());
                    cd.setCheque(ch);
                    cd.setEntidadFinanciera(ef);
                    //  cd.setIdEntidadFinanciera((Integer) listEntidadFinanciera.get(0).getValue());
                } else {
                    cd.setCheque(null);
                }

            }
        }
    }


    public void agregarCheque(Integer nroLinea) {
        for (int j = 0; j < cobro.getCobroDetalles().size(); j++) {
            CobroDetalle cd = cobro.getCobroDetalles().get(j);
            if (cd.getNroLinea() == nroLinea) {
                cd.setCodigoTipoCobro(getCodTipoCobro(cd.getIdTipoCobro()));
            }
        }
    }

    /**
     * Método para obtener el tipo de cobro
     *
     * @param idTipoCobro
     * @return
     */
    public String getCodTipoCobro(Integer idTipoCobro) {
        tipoCobro = new TipoCobro();
        tipoCobro.setId(idTipoCobro);
        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        return adapterTipoCobro.getData().get(0).getCodigo();
    }

    /**
     * Método para borrar linea de detalle de cobro
     */
    public void delLine() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String nroLinea = (String) params.get("nro");

        for (CobroDetalle cobDetalle : cobro.getCobroDetalles()) {
            if (cobDetalle.getNroLinea() == Integer.parseInt(nroLinea)) {
                if (cobro.getCobroDetalles().size() == 1) {
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                            "Debe tener al menos un detalle!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                } else if (cobro.getCobroDetalles().size() > 1) {
                    for (int j = 0; j < cobro.getCobroDetalles().size(); j++) {
                        CobroDetalle cd = cobro.getCobroDetalles().get(j);
                        if (cd.getNroLinea() == Integer.parseInt(nroLinea)) {
                            montoCobroTotal = montoCobroTotal.subtract(cd.getMontoCobro());
                            cobro.setSaldoCliente(cobro.getSaldoCliente().add(cd.getMontoCobro()));
                            cobro.getCobroDetalles().remove(cd);
                        }
                    }
                }
                break;
            }
        }

    }

    /**
     * Método para agregar linea al detalle de cobro
     */
    public void addLine() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();

        cobroDetalle = new CobroDetalle();
        cobroDetalle.setNroLinea(cobro.getCobroDetalles().size() + 1);
        cobroDetalle.setMontoCobro(BigDecimal.ZERO);
        cobroDetalle.setIdTipoCobro((Integer) listTipoCobro.get(0).getValue());
        cobroDetalle.setCodigoTipoCobro(listTipoCobro.get(0).getLabel());
        Estado estadoDetalle = new Estado();
        estadoDetalle.setActivo("C");
        cobro.setEstado(estadoDetalle);
        cobro.setCodigoEstado("COBRADO");

        cobroDetalle.setEstado(estadoDetalle);
        cobroDetalle.setCodigoEstado("COBRADO");
        cobroDetalle.setCodigoConceptoCobro("OTROS");
        cobro.getCobroDetalles().add(cobroDetalle);

    }

    /**
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Date dateUtil = (Date) event.getObject();
        this.reciboDate = dateUtil;
    }

    /**
     * Verifica si la cadena es convertible a un valor numerico
     */
    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Método para cargar Cobro
     */
    public void addCobro() {

        boolean checkErrorMsn = false;

        for (int j = 0; j < cobro.getCobroDetalles().size(); j++) {

            if (cobro.getCobroDetalles().get(j).getCodigoTipoCobro().equals("CHEQUE") && (this.omitirValidacionCheque == null || this.omitirValidacionCheque.equalsIgnoreCase("N"))) {
                if (cobro.getCobroDetalles().get(j).getCheque().getNroCheque().equals("")
                        || cobro.getCobroDetalles().get(j).getCheque().getMontoCheque().
                                compareTo(BigDecimal.ZERO) <= 0 || cobro.getCobroDetalles().get(j).getCheque().getFechaEmision() == null
                        || cobro.getCobroDetalles().get(j).getCheque().getFechaPago() == null) {

                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                            "El Nº de cheque, el monto, la fecha del cheque y la fecha del pago del cheque son requeridos");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    checkErrorMsn = true;
                }
            }
        }

        try {
            List<CobroDetalle> cdFinal = new ArrayList<>();
            if (checkErrorMsn == false) {
                BodyResponse idCobro = null;

                cobro.setFecha(reciboDate);
                cobro.setIdCliente(client.getIdCliente());
                cobro.setMontoCobro(montoCobroTotal);
                cobro.setNroRecibo(reciboNum);

                lugarCobro.setId(cobro.getIdLugarCobro());
                adapterLugarCobro = adapterLugarCobro.fillData(lugarCobro);
                if (adapterLugarCobro != null) {
                    cobro.setCodigoLugarCobro(adapterLugarCobro.getData().get(0).getCodigo());
                }

                Estado estadoDetalle = new Estado();
                estadoDetalle.setActivo("C");
                cobro.setEstado(estadoDetalle);
                cobro.setEnviado('N');
                if (digital.equalsIgnoreCase("N")) {
                    cobro.setDigital("N");
                } else {
                    cobro.setDigital("S");
                }
                cobro.setCodigoEstado("COBRADO");
                for (CobroDetalle cd : cobro.getCobroDetalles()) {
                    if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                        cd.getCheque().setIdEntidadFinanciera(cd.getIdEntidadFinanciera());
                        cd.setEstado(estadoDetalle);
                        cd.setCodigoEstado("COBRADO");
                        cd.setCodigoConceptoCobro("OTROS");
                    } else {
                        cd.setEstado(estadoDetalle);
                        cd.setCodigoEstado("COBRADO");
                        cd.setCodigoConceptoCobro("OTROS");
                    }
                }
                for (CobroDetalle cdTest : cobro.getCobroDetalles()) {
                    cdTest.setCodigoEstado("COBRADO");
                    cdFinal.add(cdTest);
                }

                /* Integer concepto = cobro.getSaldoCliente().intValue() > 0 ? 1 : 2;
                cobroDetalle.setIdConceptoCobro(concepto);*/
                cobro.setCobroDetalles(cdFinal);
                BodyResponse<Cobro> respuestaDeCobro = cobroClient.setCobro(cobro);
                if (respuestaDeCobro.getSuccess() == true) {
                    Cobro cnuevo = new Cobro();
                    cnuevo = (Cobro) respuestaDeCobro.getPayload();
                    idCobroCreado = cnuevo.getId();
                    nroCobroCreads = cnuevo.getNroRecibo();
                    if (obtenerConfiguracionDescarga().equalsIgnoreCase("S")) {
                        mostrarPanelDescarga();
                    }
                    if (notificar.equalsIgnoreCase("S")) {
                        if (selectedEmail.size() > 0) {
                            for (Email email : selectedEmail) {
                                notificar(email.getEmail());
                            }
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado la factura a la dirección confirmada"));
                        }
                    }

                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                            "Se realizó el cobro correctamente!");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    actualizarFacturas();
                    PF.current().executeScript("$('#modalCobro').modal('hide');");
                    viewPanel = false;
                }

            }

        } catch (Exception ex) {
            WebLogger.get().error("Se produjo un error al agregar el cargo" + ex);
        }
    }

    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroCobroCreads + ".pdf";
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try (FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("hideStatus();printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     *
     * @return
     */
    public String obtenerConfiguracionDescarga() {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion("DESCARGAR_RECIBO");
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "N";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }

    public void filterEntidadFinanciera() {
        adapterEntidadFinanciera = adapterEntidadFinanciera.fillData(entidadFinanciera);
    }

    /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<EntidadFinanciera> completeQueryEntidad(String query) {

        entidadFinanciera = new EntidadFinanciera();
        entidadFinanciera.setRazonSocial(query);
        adapterEntidadFinanciera.setPageSize(20);
        adapterEntidadFinanciera = adapterEntidadFinanciera.fillData(entidadFinanciera);

        return adapterEntidadFinanciera.getData();
    }

    /**
     * Método para obtener el cliente
     *
     * @param event
     * @param nroLinea
     */
    public void onItemSelectEntidadFinanciera(Integer nroLinea) {
        WebLogger.get().debug("ENTRA ENTI");
        for (int j = 0; j < cobro.getCobroDetalles().size(); j++) {
            CobroDetalle cd = cobro.getCobroDetalles().get(j);
            if (cd.getNroLinea() == nroLinea) {
                cd.setIdEntidadFinanciera(cobro.getCobroDetalles().get(j).getEntidadFinanciera().getIdEntidadFinanciera());
                if (cd.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
                    cd.getCheque().setIdEntidadFinanciera(cd.getIdEntidadFinanciera());
                }
                int a = cd.getIdEntidadFinanciera();
                WebLogger.get().debug("id entidad financiera" + a);
            }
        }
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_RECIBO_DIGITAL");
            configuracionValorFilter.setActivo("S");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            } else {
                this.digital = "N";
                this.digital = adapterConfigValor.getData().get(0).getValor();
            }
            obtenerDatosTalonario();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void obtenerDatosTalonario() {
        cobroTalonario.setDigital(digital);
        //cobroTalonario.setIdUsuario(session.getUser().getId());
        adapterCobroTalonario = adapterCobroTalonario.fillData(cobroTalonario);
        List<TalonarioPojo> talonarios = new ArrayList();
        if (adapterCobroTalonario.getData().size() > 1) {
            LocalDate fechaHoy = LocalDate.now();
            for (TalonarioPojo tp : adapterCobroTalonario.getData()) {
                if (tp.getFechaVencimiento() != null) {
                    LocalDate fechaAComparar = tp.getFechaVencimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    if (fechaAComparar.isAfter(fechaHoy)) {
                        talonarios.add(tp);
                    }
                } else {
                    talonarios.add(tp);
                }
            }
            if (talonarios.size() > 1) {
                adapterCobroTalonario.setData(talonarios);
                showTalonarioPopup = true;
                PF.current().ajax().update(":cobro-form:form-data:pnl-pop-up");
            } else if (talonarios.size() > 0) {
                talonario.setId(adapterCobroTalonario.getData().get(0).getId());
                obtenerDatosCobro();
            }
        } else {
            talonario.setId(adapterCobroTalonario.getData().get(0).getId());
            obtenerDatosCobro();
        }
    }

    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }

    /**
     * Método para obtener la configuración de Notificación por email
     */
    public void obtenerConfiguracionNotificacion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("NOTIFICACION_RECIBO_EMAIL");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.notificar = "N";
            } else {
                this.notificar = adapterConfigValor.getData().get(0).getValor();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public void notificar(String emailDestino) {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String cliente = client.getRazonSocial();
        String empresa = session.getNombreEmpresa();
        String mailTo = emailDestino;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su recibo en formato PDF";
        String subjet = "Recibo No. " + cobro.getNroRecibo();
        String url = "cobro/consulta/" + idCobroCreado;
        byte[] data = fileServiceClient.download(url);
        String fileName = nroCobroCreads + ".pdf";

        notificarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, cobro.getNroRecibo(), host, port, empresa, emailEmpresa, telefonoEmpresa);

    }

    public void notificarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host, port);
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Elimina un email de la lista de emails para notificar
     */
    public void eliminarEmail(int index) {
        this.selectedEmail.remove(index);
    }

    /**
     * Agrega un email desde la lista de emails
     *
     * @param email
     */
    public void agregarEmail(Email e) {
        if (selectedEmail.contains(e)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "El email ya está seleccionado para el envío de recibo."));
        } else {
            this.selectedEmail.add(e);
        }
    }

    /**
     * Agrega un email desde el inputText
     */
    public void agregarNuevoEmail() {
        if (validarCorreo(this.addEmail)) {
            Email e = new Email();
            e.setEmail(addEmail);
            this.selectedEmail.add(e);
            this.addEmail = "";
        }
    }

    public void onRowEditEmail(RowEditEvent event) {
        System.out.println(((Email) event.getObject()).getEmail());
        if (!validarCorreo(((Email) event.getObject()).getEmail())) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El formato del email ingresado no es válido.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            throw new ValidatorException(message);
        }
    }

    public Boolean validarCorreo(String email) {

        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+", Pattern.CASE_INSENSITIVE);

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {

            WebLogger.get().debug("El email ingresado es válido.");
            return true;
        } else {

            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            PF.current().ajax().update(":message-form:message");
            return false;
        }
    }
    
    public void actualizarFacturas() {
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.cobro = new Cobro();
        this.cobroClient = new CobroService();
        this.adapterLugarCobro = new LugarCobroListAdapter();
        this.lugarCobro = new LugarCobro();
        this.cobroDetalle = new CobroDetalle();
        this.adapterTipoCobro = new TipoCobroListAdapter();
        this.tipoCobro = new TipoCobro();
        this.adapterEntidadFinanciera = new EntidadFinancieraListAdapter();
        this.entidadFinanciera = new EntidadFinanciera();
        this.adapterCobroDetalle = new CobroDetalleListAdapter();
        this.notaCreditoDetalle = new NotaCreditoDetalle();
        this.adapterNotaCreditoDetalle = new NotaCreditoDetalleListAdapter();

        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        for (int i = 0; i < adapterTipoCobro.getData().size(); i++) {
            listTipoCobro.add(new SelectItem(adapterTipoCobro.getData().get(i).
                    getId(), adapterTipoCobro.getData().get(i).getCodigo()));
        }

        filterEntidadFinanciera();
        obtenerDatosTalonario();
        PF.current().ajax().update(":cobro-form:form-data:cliente");
        PF.current().ajax().update(":cobro-form:form-data:grupo");
    }

    /**
     * Inicializa los valores de la página
     */
    @PostConstruct
    public void init() {
        this.notificar = "N";
        obtenerConfiguracionNotificacion();
        this.client = new Cliente();
        this.adapterCliente = new ClientListAdapter();
        this.cobro = new Cobro();
        this.cobroClient = new CobroService();
        this.adapterLugarCobro = new LugarCobroListAdapter();
        this.lugarCobro = new LugarCobro();
        this.cobroDetalle = new CobroDetalle();
        this.adapterTipoCobro = new TipoCobroListAdapter();
        this.tipoCobro = new TipoCobro();
        this.adapterEntidadFinanciera = new EntidadFinancieraListAdapter();
        this.entidadFinanciera = new EntidadFinanciera();
        this.adapterCobroDetalle = new CobroDetalleListAdapter();
        this.notaCreditoDetalle = new NotaCreditoDetalle();
        this.adapterNotaCreditoDetalle = new NotaCreditoDetalleListAdapter();
        this.showDescargaPopup = false;
        this.idCobroCreado = null;
        this.nroCobroCreads = null;
        this.fileServiceClient = new FileServiceClient();
        this.entFinanSelected = new EntidadFinanciera();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        this.monedaFilter.setDescripcion("Guaranies");

        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        for (int i = 0; i < adapterTipoCobro.getData().size(); i++) {
            listTipoCobro.add(new SelectItem(adapterTipoCobro.getData().get(i).
                    getId(), adapterTipoCobro.getData().get(i).getCodigo()));
        }

        filterEntidadFinanciera();

        this.test = BigDecimal.ONE;

        this.showTalonarioPopup = false;
        this.adapterCobroTalonario = new CobroTalonarioAdapter();
        this.cobroTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();
        this.digital = "N";
        this.idTalonario = null;
        this.cobroPorTotal = false;
        obtenerConfiguracion();
        this.omitirValidacionCheque = obtenerConfiguracion("OMITIR_VALIDACION_CHEQUE");
        lazyModel = new FacturaDataModel();
        this.cobroEnGs = false;

    }

}
