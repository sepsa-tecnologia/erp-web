
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * Pojo para solicitud nota credito detalles
 *
 * @author Sergio D. Riveros Vazquez
 */
public class SolicitudNotaCreditoDetalles {


    /**
     * Identificador
     */
    private Integer id;
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    /**
     * Identificador factura
     */
    private Integer idFactura;
    /**
     * Identificador de la factura compra
     */
    private Integer idFacturaCompra;
    /**
     * Identificador producto
     */
    private Integer idProducto;
    /**
     * Identificador de solicitud nota credito
     */
    private Integer idSolicitudNotaCredito;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * porcentaje Iva
     */
    private Integer porcentajeIva;
    /**
     * cantidad
     */
    private BigDecimal cantidad;
    /**
     * Precio unitario con Iva
     */
    private BigDecimal precioUnitarioConIva;
    /**
     * Precio unitario sin Iva
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * monto Iva
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Motivo
     */
    private String motivo;
    /**
     * Identificador del motivo de la emisión snc
     */
    private Integer idMotivoEmisionSnc;
    /**
     * producto
     */
    private Producto producto;
    /**
     * Objeto Factura compra
     */
    private Factura facturaCompra;
    
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    
    /**
     * @return the facturaCompra
     */
    public Factura getFacturaCompra() {
        return facturaCompra;
    }

    /**
     * @param facturaCompra the facturaCompra to set
     */
    public void setFacturaCompra(Factura facturaCompra) {
        this.facturaCompra = facturaCompra;
    }


    /**
     * @return the idMotivoEmisionSnc
     */
    public Integer getIdMotivoEmisionSnc() {
        return idMotivoEmisionSnc;
    }

    /**
     * @param idMotivoEmisionSnc the idMotivoEmisionSnc to set
     */
    public void setIdMotivoEmisionSnc(Integer idMotivoEmisionSnc) {
        this.idMotivoEmisionSnc = idMotivoEmisionSnc;
    }

    /**
     * @return the idFacturaCompra
     */
    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    /**
     * @param idFacturaCompra the idFacturaCompra to set
     */
    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdSolicitudNotaCredito() {
        return idSolicitudNotaCredito;
    }

    public void setIdSolicitudNotaCredito(Integer idSolicitudNotaCredito) {
        this.idSolicitudNotaCredito = idSolicitudNotaCredito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    //</editor-fold>
    
    public SolicitudNotaCreditoDetalles() {
    }

}
