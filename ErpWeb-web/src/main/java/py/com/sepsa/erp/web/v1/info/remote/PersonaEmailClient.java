
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaEmailAdapter;
import py.com.sepsa.erp.web.v1.info.filters.PersonaEmailFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio Persona Email
 * @author Cristina Insfrán
 */
public class PersonaEmailClient extends APIErpCore{
   
     /**
     * Método para crear persona email
     *
     * @param personaEmail
     * @return
     */
    public BodyResponse setPersonaEmail(PersonaEmail  personaEmail) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.PERSONA_EMAIL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaEmail));

            response = BodyResponse.createInstance(conn, PersonaEmail.class);
         
        }
        return response;
    }
    /**
     * Método para editar el email
     * @param personaEmail
     * @return 
     */
     public BodyResponse editPersonaEmail(PersonaEmail  personaEmail) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.PERSONA_EMAIL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(personaEmail));

            response = BodyResponse.createInstance(conn, PersonaEmail.class);
         
        }
        return response;
    }
    
     /**
     * Obtiene la lista de persona email
     *
     * @param personaEmail 
     * @param page
     * @param pageSize
     * @return
     */
    public PersonaEmailAdapter getPersonaEmail(PersonaEmail personaEmail, Integer page,
            Integer pageSize) {

        PersonaEmailAdapter lista = new PersonaEmailAdapter();

        Map params = PersonaEmailFilter.build(personaEmail, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_EMAIL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaEmailAdapter.class);

            lista = (PersonaEmailAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }
    
    public PersonaEmailAdapter getEmailList(PersonaEmail pesonaEmail, Integer page,
            Integer pageSize) {

        PersonaEmailAdapter lista = new PersonaEmailAdapter();

        Map params = PersonaEmailFilter.build(pesonaEmail, page, pageSize);

        HttpURLConnection conn = GET(Resource.PERSONA_EMAIL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    PersonaEmailAdapter.class);

            lista = (PersonaEmailAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    /**
     * Constructor de PersonaEmailClient
     */
    public PersonaEmailClient() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        PERSONA_EMAIL("Servicio email persona", "persona-email");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
