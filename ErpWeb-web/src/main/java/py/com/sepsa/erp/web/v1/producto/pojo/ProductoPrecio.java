
package py.com.sepsa.erp.web.v1.producto.pojo;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;




/**
 * POJO para producto precio
 * @author Cristina Insfrán
 */
public class ProductoPrecio {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Objeto producto
     */
    private Producto producto;
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Objeto Moneda
     */
    private Moneda moneda;
    /**
     * Precio
     */
    private BigDecimal precio;
    /**
     * Fecha Inserción
     */
    private Date fechaInsercion;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador del canal de venta
     */
    private Integer idCanalVenta;
    /**
     * Objeto Canal Venta
     */
    private CanalVenta canalVenta;
    /**
     * Producto Descripción
     */
    private String productoDescripcion;
    /**
     * Canal venta descripción
     */
    private String canalVentaDescripcion;
    /**
     * Fecha inicio
     */
    private Date fechaInicio;
    /**
     * Fecha Fin
     */
    private Date fechaFin;
    /**
     * filtro para rango de fecha
     */
    private Date fecha;
    /**
     * Cliente
     */
    private Cliente cliente;
    
    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * 
     * @param cliente 
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * 
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }
   
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * @return the idProducto
     */
    public Integer getIdProducto() {
        return idProducto;
    }
    
    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }
    
    /**
     * @return the producto
     */
    public Producto getProducto() {
        return producto;
    }
    
    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    /**
     * @return the idMoneda
     */
    public Integer getIdMoneda() {
        return idMoneda;
    }
    
    /**
     * @param idMoneda the idMoneda to set
     */
    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }
    
    /**
     * @return the moneda
     */
    public Moneda getMoneda() {
        return moneda;
    }
    
    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }
    
    /**
     * @return the precio
     */
    public BigDecimal getPrecio() {
        return precio;
    }
    
    /**
     * @param precio the precio to set
     */
    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }
    
    /**
     * @return the fechaInsercion
     */
    public Date getFechaInsercion() {
        return fechaInsercion;
    }
    
    /**
     * @param fechaInsercion the fechaInsercion to set
     */
    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }
    
    /**
     * @return the activo
     */
    public String getActivo() {
        return activo;
    }
    
    /**
     * @param activo the activo to set
     */
    public void setActivo(String activo) {
        this.activo = activo;
    }
    
    /**
     * @return the idCanalVenta
     */
    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }
    
    /**
     * @param idCanalVenta the idCanalVenta to set
     */
    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }
    
    /**
     * @return the canalVenta
     */
    public CanalVenta getCanalVenta() {
        return canalVenta;
    }
    
    /**
     * @param canalVenta the canalVenta to set
     */
    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }
    
    /**
     * @return the productoDescripcion
     */
    public String getProductoDescripcion() {
        return productoDescripcion;
    }
    
    /**
     * @param productoDescripcion the productoDescripcion to set
     */
    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }
    
    /**
     * @return the canalVentaDescripcion
     */
    public String getCanalVentaDescripcion() {
        return canalVentaDescripcion;
    }
    
    /**
     * @param canalVentaDescripcion the canalVentaDescripcion to set
     */
    public void setCanalVentaDescripcion(String canalVentaDescripcion) {
        this.canalVentaDescripcion = canalVentaDescripcion;
    }
    
    /**
     * @return the fechaInicio
     */
    public Date getFechaInicio() {
        return fechaInicio;
    }
    
    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
    /**
     * @return the fechaFin
     */
    public Date getFechaFin() {
        return fechaFin;
    }
    
    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
//</editor-fold>
 

    
}
