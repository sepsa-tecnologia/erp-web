/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.math.BigDecimal;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ProductoRelacionado;

/**
 * Filter para producto info
 *
 * @author Williams Vera
 */
public class ProductoRelacionadoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de registro
     *
     * @param id
     * @return
     */
    public ProductoRelacionadoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

     /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public ProductoRelacionadoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }
        
    /**
     * Agrega el filtro de identificador del producto relacionado
     *
     * @param idProductoRelacionado
     * @return
     */
    public ProductoRelacionadoFilter idProductoRelacionado(Integer idProductoRelacionado) {
        if (idProductoRelacionado != null) {
            params.put("idProductoRelacionado", idProductoRelacionado);
        }
        return this;
    }

    /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public ProductoRelacionadoFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ProductoRelacionado producto, Integer page, Integer pageSize) {
        ProductoRelacionadoFilter filter = new ProductoRelacionadoFilter();

        filter
                .id(producto.getId())
                .activo(producto.getActivo())
                .idProducto(producto.getIdProducto())
                .idProductoRelacionado(producto.getIdProductoRelacionado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public ProductoRelacionadoFilter() {
        super();
    }
}
