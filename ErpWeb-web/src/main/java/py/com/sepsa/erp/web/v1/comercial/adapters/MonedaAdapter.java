
package py.com.sepsa.erp.web.v1.comercial.adapters;


import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.comercial.remote.MonedaService;

/**
 * Adaptador para la lista de Moneda
 * @author Romina Núñez
 */

public class MonedaAdapter extends DataListAdapter<Moneda> {
    
    /**
     * Cliente para los servicios de Moneda
     */
    private final MonedaService serviceMoneda;

    /**
     * Constructor de MonedaAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MonedaAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceMoneda = new MonedaService();
    }

    /**
     * Constructor de MonedaAdapter
     */
        public MonedaAdapter() {
        super();
        this.serviceMoneda= new MonedaService();
    }


    @Override
    public MonedaAdapter fillData(Moneda searchData) {
        return serviceMoneda.getMonedaList(searchData, getFirstResult(), getPageSize());
    }

    
}
