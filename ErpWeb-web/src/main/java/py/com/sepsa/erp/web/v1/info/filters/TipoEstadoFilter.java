/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEstado;

/**
 * Filter para tipo tipoEstado
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoEstadoFilter extends Filter {

    /**
     * Agrega el filtro de identificador tipoEstado
     *
     * @param id Identificador del rol
     * @return
     */
    public TipoEstadoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción
     *
     * @param descripcion descripción del tipoEstado
     * @return
     */
    public TipoEstadoFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de codigo de tipo tipoEstado
     *
     * @param codigo codigo de tipo tipoEstado
     * @return
     */
    public TipoEstadoFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param tipoEstado datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoEstado tipoEstado, Integer page, Integer pageSize) {
        TipoEstadoFilter filter = new TipoEstadoFilter();

        filter
                .id(tipoEstado.getId())
                .descripcion(tipoEstado.getDescripcion())
                .codigo(tipoEstado.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de RolFilter
     */
    public TipoEstadoFilter() {
        super();
    }

}
