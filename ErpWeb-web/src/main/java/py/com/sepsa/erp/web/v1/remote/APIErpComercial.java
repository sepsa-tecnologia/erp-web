package py.com.sepsa.erp.web.v1.remote;

/**
 * APIErpComercial para conexión a servicios
 *
 * @author Jonathan D. Bernal Fernández, Romina Núñez
 */
public class APIErpComercial extends API {

    //Parámetros de conexións
    private final static String BASE_API = "erp-comercial-api/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;

    /**
     * Constructor de API
     */
    public APIErpComercial() {
        super(null, null, 0, BASE_API, CONN_TIMEOUT);

        InfoPojo info = InfoPojo.createInstance("api-erp-comercial");

        updateConnInfo(info);
    }
}
