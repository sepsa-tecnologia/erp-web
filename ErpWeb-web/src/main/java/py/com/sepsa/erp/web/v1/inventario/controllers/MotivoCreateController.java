/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.MotivoService;

/**
 * Controlador para la vista crear motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoCreate")
public class MotivoCreateController implements Serializable {

    /**
     * Cliente para el servicio
     */
    private final MotivoService service;
    /**
     * POJO del motivo
     */
    private Motivo motivo;
    /**
     * Adaptador para la lista de motivos
     */
    private TipoMotivoAdapter tipoMotivoAdapterList;
    /**
     * POJO de tipo motivo
     */
    private TipoMotivo tipoMotivoFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public TipoMotivoAdapter getTipoMotivoAdapterList() {
        return tipoMotivoAdapterList;
    }

    public void setTipoMotivoAdapterList(TipoMotivoAdapter tipoMotivoAdapterList) {
        this.tipoMotivoAdapterList = tipoMotivoAdapterList;
    }

    public TipoMotivo getTipoMotivoFilter() {
        return tipoMotivoFilter;
    }

    public void setTipoMotivoFilter(TipoMotivo tipoMotivoFilter) {
        this.tipoMotivoFilter = tipoMotivoFilter;
    }

    //</editor-fold>
    /**
     * Método para crear
     */
    public void create() {

        BodyResponse<Motivo> respuestaTal = service.setMotivo(motivo);

        if (respuestaTal.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Motivo creado correctamente!"));
        }

    }

    /**
     * Método para obtener tipo motivo
     */
    public void getTipoMotivo() {
        getTipoMotivoAdapterList().setFirstResult(0);
        this.tipoMotivoAdapterList = this.tipoMotivoAdapterList.fillData(tipoMotivoFilter);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivo = new Motivo();
        this.tipoMotivoAdapterList = new TipoMotivoAdapter();
        this.tipoMotivoFilter = new TipoMotivo();
        getTipoMotivo();
    }

    /**
     * Constructor
     */
    public MotivoCreateController() {
        init();
        this.service = new MotivoService();
    }

}
