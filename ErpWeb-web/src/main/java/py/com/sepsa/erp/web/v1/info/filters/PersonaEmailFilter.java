package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;

/**
 * Filtro para el servicio persona email
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
public class PersonaEmailFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id Identificador
     * @return
     */
    public PersonaEmailFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de la persona
     *
     * @param idPersona Identificador de la persona
     * @return
     */
    public PersonaEmailFilter idPersona(Integer idPersona) {
        if (idPersona != null) {
            params.put("idPersona", idPersona);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de mail
     * @param idEmail
     * @return
     */
    public PersonaEmailFilter idEmail(Integer idEmail){
        if (idEmail != null) {
            params.put("idEmail", idEmail);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de principal
     *
     * @param principal
     * @return
     */
    public PersonaEmailFilter principal(String principal) {
        if (principal != null && !principal.trim().isEmpty()) {
            params.put("principal", principal);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de tipo email
     * @param idTipoEmail 
     * @return 
     */
    public PersonaEmailFilter idTipoEmail(Integer idTipoEmail){
        if (idTipoEmail != null) {
            params.put("idTipoEmail", idTipoEmail);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de tipoEmail
     *
     * @param tipoEmail
     * @return
     */
    public PersonaEmailFilter tipoEmail(String tipoEmail) {
        if (tipoEmail != null && !tipoEmail.trim().isEmpty()) {
            params.put("tipoEmail", tipoEmail);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de activo
     *
     * @param activo
     * @return
     */
    public PersonaEmailFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }
    
    
    
    /**
     * Construye el mapa de parametros
     *
     * @param personaEmail datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(PersonaEmail personaEmail, Integer page, Integer pageSize) {
        PersonaEmailFilter filter = new PersonaEmailFilter();

        filter
                .id(personaEmail.getId())
                .idPersona(personaEmail.getIdPersona())
                .idEmail(personaEmail.getIdEmail())
                .activo(personaEmail.getActivo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de PersonaEmailFilter
     */
    public PersonaEmailFilter() {
        super();
    }

}
