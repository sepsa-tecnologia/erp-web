/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para Control Inventario
 *
 * @author Romina Núñez
 */
public class ControlInventarioFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public ControlInventarioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador
     *
     * @param idMotivo
     * @return
     */
    public ControlInventarioFilter idMotivo(Integer idMotivo) {
        if (idMotivo != null) {
            params.put("idMotivo", idMotivo);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador
     *
     * @param idEstado
     * @return
     */
    public ControlInventarioFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public ControlInventarioFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agrega el filtro codigo
     *
     * @param codigo
     * @return
     */
    public ControlInventarioFilter codigo(String codigo) {
        if (codigo != null && !codigo.trim().isEmpty()) {
            params.put("codigo", codigo);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public ControlInventarioFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);

                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionHasta
     * @return
     */
    public ControlInventarioFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);

                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param control datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ControlInventario control, Integer page, Integer pageSize) {
        ControlInventarioFilter filter = new ControlInventarioFilter();

        filter
                .id(control.getId())
                .idMotivo(control.getIdMotivo())
                .idEstado(control.getIdEstado())
                .listadoPojo(control.getListadoPojo())
                .codigo(control.getCodigo())
                .fechaInsercionDesde(control.getFechaInsercionDesde())
                .fechaInsercionHasta(control.getFechaInsercionHasta())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public ControlInventarioFilter() {
        super();
    }

}
