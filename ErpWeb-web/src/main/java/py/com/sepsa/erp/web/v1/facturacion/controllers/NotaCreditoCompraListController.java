/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoCompraService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar nota credito compra
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
@ViewScoped
@Named("notaCreditoCompraList")
public class NotaCreditoCompraListController implements Serializable {

    /**
     * Adaptador de notaCreditoCompra.
     */
    private NotaCreditoCompraAdapter adapter;
    /**
     * Objeto notaCreditoCompra.
     */
    private NotaCreditoCompra notaCreditoCompraFilter;
    /**
     * Adaptador de la lista de moneda
     */
    private MonedaAdapter monedaAdapter;
    /**
     * Datos del moneda
     */
    private Moneda moneda;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;
    /**
     * Datos del cliente
     */
    private Cliente cliente;
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    /**
     * Adaptador de la lista de motivo EmisionInterno
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapter;
    /**
     * Datos del motivoEmisionInterno
     */
    private MotivoEmisionInterno motivoEmisionInterno;
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoCompraService serviceNotaCreditoCompra;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    /**
     * @return the adapter
     */
    public NotaCreditoCompraAdapter getAdapter() {
        return adapter;
    }

    public void setServiceNotaCreditoCompra(NotaCreditoCompraService serviceNotaCreditoCompra) {
        this.serviceNotaCreditoCompra = serviceNotaCreditoCompra;
    }

    public NotaCreditoCompraService getServiceNotaCreditoCompra() {
        return serviceNotaCreditoCompra;
    }

    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    /**
     * @param adapter the adapter to set
     */
    public void setAdapter(NotaCreditoCompraAdapter adapter) {
        this.adapter = adapter;
    }

    /**
     * @return the notaCreditoCompraFilter
     */
    public NotaCreditoCompra getNotaCreditoCompraFilter() {
        return notaCreditoCompraFilter;
    }

    /**
     * @param notaCreditoCompraFilter the notaCreditoCompraFilter to set
     */
    public void setNotaCreditoCompraFilter(NotaCreditoCompra notaCreditoCompraFilter) {
        this.notaCreditoCompraFilter = notaCreditoCompraFilter;
    }

    public MonedaAdapter getMonedaAdapter() {
        return monedaAdapter;
    }

    public void setMonedaAdapter(MonedaAdapter monedaAdapter) {
        this.monedaAdapter = monedaAdapter;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapter() {
        return motivoEmisionInternoAdapter;
    }

    public void setMotivoEmisionInternoAdapter(MotivoEmisionInternoAdapter motivoEmisionInternoAdapter) {
        this.motivoEmisionInternoAdapter = motivoEmisionInternoAdapter;
    }

    public MotivoEmisionInterno getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    //</editor-fold>
    /**
     * Método para filtrar notaCreditoCompras.
     */
    public void buscar() {
        getAdapter().setFirstResult(0);
        this.adapter = adapter.fillData(notaCreditoCompraFilter);
    }

    /**
     * Método para limpiar el filtro.
     */
    public void limpiar() {
        this.notaCreditoCompraFilter = new NotaCreditoCompra();
        this.moneda = new Moneda();
        getMonedas();
        getMotivoEmisionInternoList();
        buscar();
    }

    /**
     * Método para obtener las monedas
     */
    public void getMonedas() {
        this.setMonedaAdapter(getMonedaAdapter().fillData(getMoneda()));
    }

    /**
     * Método para obtener las monedas
     */
    public void getMotivoEmisionInternoList() {
        this.setMotivoEmisionInternoAdapter(getMotivoEmisionInternoAdapter().fillData(getMotivoEmisionInterno()));
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {
        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        clientAdapter = clientAdapter.fillData(cliente);
        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        notaCreditoCompraFilter.setIdPersona(cliente.getIdCliente());
    }

    /**
     * Método para anular nota de crédito
     *
     * @param notaCredito
     */
    public void anularNotaCredito(NotaCreditoCompra notaCredito) {
        NotaCreditoCompra nota = new NotaCreditoCompra();
        nota.setId(notaCredito.getId());
        nota.setIdMotivoAnulacion(notaCredito.getIdMotivoAnulacion());
        nota.setObservacionAnulacion(notaCredito.getObservacionAnulacion());
        BodyResponse notaCredResp = serviceNotaCredito.anularNotaCreditoCompra(nota);

        if (notaCredResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de crédito Anulada!"));
            notaCreditoCompraFilter = new NotaCreditoCompra();
            buscar();
        }

    }

    /**
     * Método para eliminar la nc de compra
     * @param id 
     */
    public void eliminarNotaCredito(Integer id) {
        boolean respuesta = serviceNotaCreditoCompra.deleteNotaCreditoCompra(id);
        if (respuesta == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de Crédito eliminada!"));
            notaCreditoCompraFilter = new NotaCreditoCompra();
            buscar();
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al eliminar la Nota de Crédito!"));
        }
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleNotaCreditoURL(Integer id) {

        return String.format("nota-credito-compra-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    @PostConstruct
    public void init() {
        try {
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.adapter = new NotaCreditoCompraAdapter();
            this.notaCreditoCompraFilter = new NotaCreditoCompra();
            this.monedaAdapter = new MonedaAdapter();
            this.moneda = new Moneda();
            this.motivoEmisionInternoAdapter = new MotivoEmisionInternoAdapter();
            this.motivoEmisionInterno = new MotivoEmisionInterno();

            this.serviceNotaCredito = new NotaCreditoService();
            getMonedas();
            getMotivoEmisionInternoList();
            buscar();

            this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
            this.motivoAnulacion = new MotivoAnulacion();

            motivoAnulacion.setActivo('S');
            motivoAnulacion.setCodigoTipoMotivoAnulacion("NOTA_CREDITO_COMPRA");
            adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
            this.serviceNotaCreditoCompra = new NotaCreditoCompraService();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
