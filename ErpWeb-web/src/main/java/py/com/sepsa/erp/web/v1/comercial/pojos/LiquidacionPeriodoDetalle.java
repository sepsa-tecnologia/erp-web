package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para listar detalle de liquidaciones por periodo.
 *
 * @author alext
 */
public class LiquidacionPeriodoDetalle {

    /**
     * Identificador de Historico Liquidación.
     */
    private Integer idHistoricoLiquidacion;
    /**
     * Identificador de número de línea.
     */
    private Integer nroLinea;
    /**
     * Identificador de tarifa.
     */
    private Integer idTarifa;
    /**
     * Identificador del cliente.
     */
    private Integer idCliente;
    /**
     * Cliente.
     */
    private String cliente;
    /**
     * Relación del cliente.
     */
    private String clienteRel;
    /**
     * Identificador de relación con cliente.
     */
    private Integer idGrupoClienteRel;
    /**
     * Monto de la tarifa.
     */
    private Integer montoTarifa;
    /**
     * Monto del descuento.
     */
    private Integer montoDescuento;
    /**
     * Monto total.
     */
    private Integer montoTotal;
    /**
     * Año
     */
    private String ano;
    /**
     * Mes
     */
    private String mes;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getClienteRel() {
        return clienteRel;
    }

    public void setClienteRel(String clienteRel) {
        this.clienteRel = clienteRel;
    }

    public Integer getIdGrupoClienteRel() {
        return idGrupoClienteRel;
    }

    public void setIdGrupoClienteRel(Integer idGrupoClienteRel) {
        this.idGrupoClienteRel = idGrupoClienteRel;
    }

    public Integer getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(Integer montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public Integer getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(Integer montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public Integer getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Integer montoTotal) {
        this.montoTotal = montoTotal;
    }
    
    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }
    //</editor-fold>

    /**
     * Constructor sin parámetros.
     */
    public LiquidacionPeriodoDetalle() {
        
    }

    public LiquidacionPeriodoDetalle(String ano, String mes) {
        this.ano = ano;
        this.mes = mes;
    }
    
    /**
     * Constructor con parámetros.
     * 
     * @param idHistoricoLiquidacion
     * @param nroLinea
     * @param idTarifa
     * @param idCliente
     * @param cliente
     * @param clienteRel
     * @param idGrupoClienteRel
     * @param montoTarifa
     * @param montoDescuento
     * @param montoTotal
     * @param ano
     * @param mes 
     */
    public LiquidacionPeriodoDetalle(Integer idHistoricoLiquidacion, Integer nroLinea,
            Integer idTarifa, Integer idCliente, String cliente, String clienteRel, Integer idGrupoClienteRel,
            Integer montoTarifa, Integer montoDescuento, Integer montoTotal, String ano, String mes) {

        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.nroLinea = nroLinea;
        this.idTarifa = idTarifa;
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.clienteRel = clienteRel;
        this.idGrupoClienteRel = idGrupoClienteRel;
        this.montoTarifa = montoTarifa;
        this.montoDescuento = montoDescuento;
        this.montoTotal = montoTotal;
        this.ano = ano;
        this.mes = mes;
    }

}
