package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ServicioProducto;

/**
 * Filtro utilizado para servicio 
 *
 * @author Romina E. Núñez Rojas
 */
public class ServicioProductoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de servicio
     *
     * @param id
     * @return
     */
    public ServicioProductoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción del servicio
     *
     * @param descripcion
     * @return
     */
    public ServicioProductoFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Agrega el filtro de la código del servicio
     *
     * @param codigo
     * @return
     */
    public ServicioProductoFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador del producto
     *
     * @param idProducto 
     * @return
     */
    public ServicioProductoFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de código de producto
     *
     * @param codigoProducto
     * @return
     */
    public ServicioProductoFilter codigoProducto(String codigoProducto) {
        if (codigoProducto != null) {
            params.put("codigoProducto", codigoProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro de producto
     *
     * @param codigo
     * @return
     */
    public ServicioProductoFilter producto(String producto) {
        if (producto != null) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param servicioProducto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ServicioProducto servicioProducto, Integer page, Integer pageSize) {
        ServicioProductoFilter filter = new ServicioProductoFilter();

        filter
                .id(servicioProducto.getId())
                .descripcion(servicioProducto.getDescripcion())
                .codigo(servicioProducto.getCodigo())
                .idProducto(servicioProducto.getIdProducto())
                .codigoProducto(servicioProducto.getCodigoProducto())
                .producto(servicioProducto.getProducto())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ServicioProductoFilter
     */
    public ServicioProductoFilter() {
        super();
    }
}
