package py.com.sepsa.erp.web.v1.facturacion.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.filters.DocumentoProcesamientoFilter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.AutofacturaProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Servicio para Factura Procesamiento
 *
 * @author Romina N
 */
public class FacturaProcesamientoService extends APIErpFacturacion {

    /**
     * Método que obtiene el listado de menu
     *
     * @param fp  objeto
     * @param page
     * @param pageSize
     * @return
     */
    public FacturaProcesamientoAdapter getFacturaProcesamientoList(DocumentoProcesamiento fp, Integer page,
            Integer pageSize) {

        FacturaProcesamientoAdapter lista = new FacturaProcesamientoAdapter();

        Map params = DocumentoProcesamientoFilter.build(fp, page, pageSize);

        HttpURLConnection conn = GET(Resource.FACTURA_PROCESAMIENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    FacturaProcesamientoAdapter.class);

            lista = (FacturaProcesamientoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
     /**
     * Método que obtiene el listado de autofacturaProcesamiento
     *
     * @param fp  objeto
     * @param page
     * @param pageSize
     * @return
     */
    public AutofacturaProcesamientoAdapter getAutofacturaProcesamientoList(DocumentoProcesamiento fp, Integer page,
            Integer pageSize) {

        AutofacturaProcesamientoAdapter lista = new AutofacturaProcesamientoAdapter();

        Map params = DocumentoProcesamientoFilter.build(fp, page, pageSize);

        HttpURLConnection conn = GET(Resource.AUTOFACTURA_PROCESAMIENTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    AutofacturaProcesamientoAdapter.class);

            lista = (AutofacturaProcesamientoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de la clase
     */
    public FacturaProcesamientoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        FACTURA_PROCESAMIENTO("Factura Procesamiento", "factura-procesamiento"),
        AUTOFACTURA_PROCESAMIENTO("Autofactura Procesamiento", "autofactura-procesamiento");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
