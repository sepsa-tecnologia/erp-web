
package py.com.sepsa.erp.api.response;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Encapsula las respuestas del servicio
 * @author Daniel F. Escauriza Arza
 */
public class ResponseWrapper {
    
    /**
     * Bandera que indica si el servicio se ejecuto con éxito
     */
    private boolean success;
    
    /**
     * Código de respuesta
     */
    private ResponseCode responseCode;
    
    /**
     * Datos asociados a la respuesta
     */
    private JsonElement payload;

    /**
     * Verifica si el servicio se ejecuto con éxito
     * @return Si el servicio se ejecuto con éxito
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setea si el servicio se ejecuto con éxito
     * @param success Bandera que indica si el servicio se ejecuto con éxito
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Obtiene el código de respuesta
     * @return Código de respuesta
     */
    public ResponseCode getResponseCode() {
        return responseCode;
    }

    /**
     * Setea el código de respuesta
     * @param responseCode Código de respuesta
     */
    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Obtiene los datos asociados a la respuesta
     * @return Datos asociados a la respuesta
     */
    public JsonElement getPayload() {
        return payload;
    }

    /**
     * Setea los datos asociados a la respuesta
     * @param payload Datos asociados a la respuesta
     */
    public void setPayload(JsonElement payload) {
        this.payload = payload;
    }

    /**
     * Convierte la respuesta encapsulada en formato JSON - String
     * @return Respuesta encapsulada en formato JSON - String
     */
    public String toJson() {
        JsonObject status = new JsonObject();
        
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        status.addProperty("timeZone", now.getTimeZone().toZoneId().toString());
        status.addProperty("stamp", sdf.format(now.getTime()));
        status.addProperty("success", success);
        
        if(responseCode != null) {
            status.addProperty("description", responseCode.getDescription());
        }
        
        JsonObject json = new JsonObject();
        json.addProperty("success", success);
        json.addProperty("raw", false);
        json.add("status", status);
        
        if(payload != null) {
            json.add("payload", payload);
        }
        
        return json.toString();
    }
    
    /**
     * Constructor de ResponseWrapper
     * @param success Bandera que indica si el servicio se ejecuto correctamente
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public ResponseWrapper(boolean success, ResponseCode responseCode, 
            JsonElement payload) {
        this.success = success;
        this.responseCode = responseCode;
        this.payload = payload;
    }
    
}
