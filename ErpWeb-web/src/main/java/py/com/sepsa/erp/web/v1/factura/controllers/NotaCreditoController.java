package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosNotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoProcesamientoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.DocumentoProcesamiento;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.facturacion.remote.SepsaSiediMonitorService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.notificacion.pojos.ArchivoAdjunto;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.notificacion.pojos.TipoNotificaciones;
import py.com.sepsa.erp.web.v1.notificacion.remote.NotificacionService;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@SessionScoped
@Named("notaCredito")
public class NotaCreditoController implements Serializable {

    /**
     * Adaptador para la lista de nota de credito
     */
    private NotaCreditoPojoAdapter adapterNotaCredito;
    /**
     * POJO NotaCredito
     */
    private NotaCreditoPojo notaCreditoFilter;
    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean create;

    /**
     * Dato bandera de prueba para la vista de crear NC
     */
    private boolean show;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFactura;
    /**
     * POJO Factura
     */
    private Factura facturaFilter;
    /**
     * Dato de control
     */
    private boolean showDatatable;
    /**
     * Lista de prueba
     */
    private List<Integer> listaPrueba = new ArrayList<>();
    /**
     * Service Nota de Crédito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * POJO Datos Nota de Crédito
     */
    private DatosNotaCredito datoNotaCredito;
    /**
     * POJO Nota de Crédito
     */
    private NotaCredito notaCreditoCreate;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Lista Detalle de N.C.
     */
    private List<NotaCreditoDetalle> listaDetalle = new ArrayList<>();
    /**
     * Dato Linea
     */
    private Integer linea;
    /**
     * Lista seleccionada de Nota de Crédito
     */
    private List<Factura> listSelectedFactura = new ArrayList<>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Direccion
     */
    private String direccion;
    /**
     * RUC
     */
    private String ruc;
    /**
     * Telefono
     */
    private String telefono;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     *
     * Adaptador para la lista de motivoEmisionInterno
     *
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;
    /**
     *
     * POJO de MotivoEmisionInterno
     *
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Pojo de procesamiento de archivo
     */
    private DocumentoProcesamiento procesamientoFilter;
    /**
     * Adaptador de la lista de procesamiento de archivo
     */
    private NotaCreditoProcesamientoAdapter adapterProcesamiento;
    /**
     * POJO NotaCredito
     */
    private NotaCreditoPojo notaCreditoReenvio;
    /**
     * Email Reenvio
     */
    private String emailReenvio;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    private Boolean addEmail;
    
    @Inject
    private SessionData session;

    private Integer diasFiltro;
    
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the adapterMotivoAnulacion
     */
    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    public void setNotaCreditoReenvio(NotaCreditoPojo notaCreditoReenvio) {
        this.notaCreditoReenvio = notaCreditoReenvio;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public void setEmailReenvio(String emailReenvio) {
        this.emailReenvio = emailReenvio;
    }

    public void setAddEmail(Boolean addEmail) {
        this.addEmail = addEmail;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public String getEmailReenvio() {
        return emailReenvio;
    }

    public Boolean getAddEmail() {
        return addEmail;
    }

    public NotaCreditoPojo getNotaCreditoReenvio() {
        return notaCreditoReenvio;
    }

    public void setProcesamientoFilter(DocumentoProcesamiento procesamientoFilter) {
        this.procesamientoFilter = procesamientoFilter;
    }

    public void setAdapterProcesamiento(NotaCreditoProcesamientoAdapter adapterProcesamiento) {
        this.adapterProcesamiento = adapterProcesamiento;
    }

    public NotaCreditoProcesamientoAdapter getAdapterProcesamiento() {
        return adapterProcesamiento;
    }

    public DocumentoProcesamiento getProcesamientoFilter() {
        return procesamientoFilter;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    /**
     * @param adapterMotivoAnulacion the adapterMotivoAnulacion to set
     */
    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    /**
     * @return the motivoAnulacion
     */
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    /**
     * @param motivoAnulacion the motivoAnulacion to set
     */
    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    /**
     * @return the adapterCliente
     */
    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {
        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;
    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {
        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;
    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {
        return motivoEmisionInternoFilter;
    }

    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {
        return motivoEmisionInternoAdapterList;
    }

    /**
     * @param adapterCliente the adapterCliente to set
     */
    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListSelectedFactura(List<Factura> listSelectedFactura) {
        this.listSelectedFactura = listSelectedFactura;
    }

    public List<Factura> getListSelectedFactura() {
        return listSelectedFactura;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListaDetalle(List<NotaCreditoDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<NotaCreditoDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setNotaCreditoCreate(NotaCredito notaCreditoCreate) {
        this.notaCreditoCreate = notaCreditoCreate;
    }

    public NotaCredito getNotaCreditoCreate() {
        return notaCreditoCreate;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

    public void setDatoNotaCredito(DatosNotaCredito datoNotaCredito) {
        this.datoNotaCredito = datoNotaCredito;
    }

    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    public DatosNotaCredito getDatoNotaCredito() {
        return datoNotaCredito;
    }

    public void setShowDatatable(boolean showDatatable) {
        this.showDatatable = showDatatable;
    }

    public boolean isShowDatatable() {
        return showDatatable;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public FacturaAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setListaPrueba(List<Integer> listaPrueba) {
        this.listaPrueba = listaPrueba;
    }

    public List<Integer> getListaPrueba() {
        return listaPrueba;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public boolean isCreate() {
        return create;
    }

    public void setNotaCreditoFilter(NotaCreditoPojo notaCreditoFilter) {
        this.notaCreditoFilter = notaCreditoFilter;
    }

    public void setAdapterNotaCredito(NotaCreditoPojoAdapter adapterNotaCredito) {
        this.adapterNotaCredito = adapterNotaCredito;
    }

    public NotaCreditoPojo getNotaCreditoFilter() {
        return notaCreditoFilter;
    }

    public NotaCreditoPojoAdapter getAdapterNotaCredito() {
        return adapterNotaCredito;
    }

    public SessionData getSession() {
        return session;
    }

    public void setSession(SessionData session) {
        this.session = session;
    }

    public Integer getDiasFiltro() {
        return diasFiltro;
    }

    public void setDiasFiltro(Integer diasFiltro) {
        this.diasFiltro = diasFiltro;
    }
     
    
//</editor-fold>

    /**
     * Método para llenar el adapter
     */
    public void filterNotaCredito() {
        if (validacionesFiltrarNotaCredito()) {
            getAdapterNotaCredito().setFirstResult(0);
            this.adapterNotaCredito = adapterNotaCredito.fillData(notaCreditoFilter);
        }
    }
    
    public boolean validacionesFiltrarNotaCredito() {
        boolean save = true;
        if (notaCreditoFilter.getIdCliente() == null && notaCreditoFilter.getFechaDesde() == null && notaCreditoFilter.getFechaHasta() == null) {
            FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error", "Se debe elegir al menos un cliente o un rango de fechas"));
            save = false;
        }else{
            if(notaCreditoFilter.getIdCliente() == null) {
                if (notaCreditoFilter.getFechaDesde() != null && notaCreditoFilter.getFechaHasta() == null) {
                    FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", "Se debe indicar la fecha hasta"));
                    save = false;
                }else if (notaCreditoFilter.getFechaDesde() == null && notaCreditoFilter.getFechaHasta() != null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Se debe indicar la fecha desde"));
                    save = false;
                }

                if (notaCreditoFilter.getFechaDesde() != null && notaCreditoFilter.getFechaHasta() != null) {
                    Instant from = notaCreditoFilter.getFechaDesde().toInstant();
                    Instant to = notaCreditoFilter.getFechaHasta().toInstant();
                    notaCreditoFilter.setListadoPojo(true);

                    long days = ChronoUnit.DAYS.between(from, to);

                    Integer maxDias = 31;
                    if (diasFiltro != null) {
                        maxDias = diasFiltro;
                    }

                    if (days > maxDias) {

                        FacesContext.getCurrentInstance().addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                        "Fecha Incorrecta", String.format("El rango de fecha no puede ser más de %d días", maxDias)));
                        save = false;
                    }
                }
            }
        }
        return save;
    }

    public void clear() {
        notaCreditoFilter = new NotaCreditoPojo();
        cliente = new Cliente();
        asignarFiltrosFecha();
        filterNotaCredito();
    }

    /**
     * Método para agregar detalle
     *
     * @param event
     */
    public void onCheckFactura(SelectEvent event) {
        if (show == false) {
            show = true;
        }
        NotaCreditoDetalle notaCreditoDetalle = new NotaCreditoDetalle();

        if (listaDetalle.isEmpty()) {
            notaCreditoDetalle.setNroLinea(linea);
        } else {
            linea++;
            notaCreditoDetalle.setNroLinea(linea);
        }

        notaCreditoDetalle.setIdFactura(((Factura) event.getObject()).getId());
        String nroFactura = ((Factura) event.getObject()).getNroFactura();
        notaCreditoDetalle.setDescripcion("BONIFICACION DE LA FACTURA NRO." + nroFactura);
        notaCreditoDetalle.setPorcentajeIva(10);
        notaCreditoDetalle.setMontoImponible(((Factura) event.getObject()).getMontoImponibleTotal());

        BigDecimal ivaValueDiez = new BigDecimal("0.10");
        BigDecimal resultDiez = notaCreditoDetalle.getMontoImponible().multiply(ivaValueDiez);
        BigDecimal montoTotalDiez = notaCreditoDetalle.getMontoImponible().add(resultDiez);

        notaCreditoDetalle.setMontoIva(resultDiez);
        notaCreditoDetalle.setMontoTotal(montoTotalDiez);

        listaDetalle.add(notaCreditoDetalle);

        calcularTotales();
    }

    /**
     * Método invocado para deseleccionar liquidacion
     *
     * @param event
     */
    public void onUncheckFactura(UnselectEvent event) {

        BigDecimal subTotal = new BigDecimal("0");
        NotaCreditoDetalle info = null;

        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdFactura(), ((Factura) event.getObject()).getId())) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        BigDecimal monto = notaCreditoCreate.getMontoTotalNotaCredito();
        BigDecimal totalNotaCredito = monto.subtract(subTotal);
        notaCreditoCreate.setMontoTotalNotaCredito(totalNotaCredito);

        calcularTotales();

    }

    /**
     * Método para calcular los montos
     *
     * @param nroLinea
     */
    public void calcularMontos(Integer nroLinea) {
        NotaCreditoDetalle info = null;
        for (NotaCreditoDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        BigDecimal montoImponible = info.getMontoImponible();

        if (info.getPorcentajeIva() == 0) {
            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoTotal(montoImponible);
        }

        if (info.getPorcentajeIva() == 5) {
            BigDecimal ivaValueCinco = new BigDecimal("0.05");
            BigDecimal resultCinco = montoImponible.multiply(ivaValueCinco);
            BigDecimal montoTotalCinco = montoImponible.add(resultCinco);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoTotalCinco);

        }

        if (info.getPorcentajeIva() == 10) {
            BigDecimal ivaValueDiez = new BigDecimal("0.10");
            BigDecimal resultDiez = montoImponible.multiply(ivaValueDiez);
            BigDecimal montoTotalDiez = montoImponible.add(resultDiez);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoTotalDiez);

        }

        calcularTotales();

    }

    /**
     * Método para cálculo de monto total
     */
    public void calcularTotales() {
        BigDecimal acumTotales = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            acumTotales = acumTotales.add(listaDetalle.get(i).getMontoTotal());
        }

        notaCreditoCreate.setMontoTotalNotaCredito(acumTotales);

        obtenerMontoTotalLetras();

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        notaCreditoCreate.setMontoIva5(montoIva5Acumulador);
        notaCreditoCreate.setMontoImponible5(montoImponible5Acumulador);
        notaCreditoCreate.setMontoTotal5(montoTotal5Acumulador);
        notaCreditoCreate.setMontoIva10(montoIva10Acumulador);
        notaCreditoCreate.setMontoImponible10(montoImponible10Acumulador);
        notaCreditoCreate.setMontoTotal10(montoTotal10Acumulador);
        notaCreditoCreate.setMontoTotalExento(montoExcentoAcumulador);
        notaCreditoCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        notaCreditoCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {
        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setIdMoneda(notaCreditoCreate.getIdMoneda());
        montoLetrasFilter.setMonto(notaCreditoCreate.getMontoTotalNotaCredito());

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        adapterCliente = adapterCliente.fillData(cliente);

        return adapterCliente.getData();
    }

    public void onItemSelectCliente(SelectEvent event) {
        this.notaCreditoFilter.setIdCliente(((Cliente) event.getObject()).getIdCliente());
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @param id
     * @return
     */
    public String detalleNotaCreditoURL(Integer id) {

        return String.format("nota-credito-detalle-list"
                + "?faces-redirect=true"
                + "&id=%d", id);
    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Date dateUtil = (Date) event.getObject();
        if (datoNotaCredito.getFecha().compareTo(dateUtil) > 0) {
            notaCreditoCreate.setFecha(datoNotaCredito.getFecha());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha anterior!"));
        }

    }

    /**
     * Método para crear Nota de Crédito
     */
    public void createNotaCredito() {
        calcularTotalesGenerales();

        notaCreditoCreate.setAnulado("N");
        notaCreditoCreate.setDigital("N");
        notaCreditoCreate.setImpreso("N");
        notaCreditoCreate.setEntregado("N");

        // se elimino la línea de asignación de detalle de NC
        Integer idNotaCredito = null;

        if (idNotaCredito != null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de Crédito creada correctamente!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al crear la Nota de Crédito!"));
        }
    }

    /**
     * Método para filtrar la factura
     */
    public void filterFactura() {
        facturaFilter.setCobrado("N");
        facturaFilter.setAnulado("N");

        adapterFactura = adapterFactura.fillData(facturaFilter);
    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        telefono = "";
        ruc = "";
        direccion = "";
        notaCreditoCreate = new NotaCredito();
        listSelectedFactura = new ArrayList<>();
        listaDetalle = new ArrayList<>();
        montoLetrasFilter = new MontoLetras();
        datoNotaCredito = new DatosNotaCredito();
    }

    /**
     *
     * Método para obtener motivoEmisionInternos
     *
     */
    public void getMotivoEmisionInterno() {

        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));

    }

    /**
     * Método para anular nota de crédito
     *
     * @param notaCredito
     */
    public void anularNotaCredito(NotaCreditoPojo notaCredito) {
        NotaCredito nota = new NotaCredito();
        nota.setId(notaCredito.getId());
        nota.setIdMotivoAnulacion(notaCredito.getIdMotivoAnulacion());
        nota.setObservacionAnulacion(notaCredito.getObservacionAnulacion());
        nota.setIgnorarPeriodoAnulacion(ignorarPeriodoAnulacion);
        BodyResponse notaCredResp = serviceNotaCredito.anularNotaCredito(nota);

        if (notaCredResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de Crédito Anulada!"));
            notaCreditoFilter = new NotaCreditoPojo();
            filterNotaCredito();
        }

    }

    /**
     *
     * @param notaCredito
     */
    public void regenerarNC(NotaCreditoPojo notaCredito) {
        NotaCredito nc = new NotaCredito();
        nc.setId(notaCredito.getId());
        nc.setEntregado(notaCredito.getEntregado());
        nc.setImpreso(notaCredito.getImpreso());
        nc.setArchivoEdi(notaCredito.getArchivoEdi());
        nc.setArchivoSet(notaCredito.getArchivoSet());
        nc.setGeneradoEdi(notaCredito.getGeneradoEdi());
        nc.setGeneradoSet("N");

        BodyResponse notaCredResp = serviceNotaCredito.regenerarNC(nc);

        if (notaCredResp.getSuccess() == true) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nota de crédito regenerada!"));
            notaCreditoFilter = new NotaCreditoPojo();
            filterNotaCredito();
        }

    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionPeriodoAnulacionNC() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("IGNORAR_PERIODO_ANULACION_NC");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ignorarPeriodoAnulacion = false;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.ignorarPeriodoAnulacion = true;
        } else {
            this.ignorarPeriodoAnulacion = false;
        }

    }

    /**
     * Metodo para obtener los doc asociados
     *
     * @param idProcesamiento
     */
    public void verProcesamiento(Integer idProcesamiento) {
        WebLogger.get().debug("PROCESAMIENTO" + idProcesamiento);
        this.procesamientoFilter = new DocumentoProcesamiento();
        this.adapterProcesamiento = new NotaCreditoProcesamientoAdapter();
        if (idProcesamiento != null) {
            this.procesamientoFilter.setIdProcesamiento(idProcesamiento);
            this.adapterProcesamiento = adapterProcesamiento.fillData(procesamientoFilter);
            this.procesamientoFilter = this.adapterProcesamiento.getData().get(0);
        }
    }

    public void guardarNcAReenviar(Integer id, String nro, String email, String digital, String cdc, String cliente) {
        this.notaCreditoReenvio = new NotaCreditoPojo();
        this.notaCreditoReenvio.setId(id);
        this.notaCreditoReenvio.setNroNotaCredito(nro);
        this.notaCreditoReenvio.setDigital(digital);
        this.notaCreditoReenvio.setCdc(cdc);
        this.notaCreditoReenvio.setRazonSocial(cliente);
        this.emailReenvio = null;
        this.emailReenvio = email;

        if (emailReenvio != null) {
            addEmail = true;
        } else {
            addEmail = false;
        }
    }

    public void validarCorreo(String email) {
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El email ingresado es inválido");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void reenviar() {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String cliente = notaCreditoReenvio.getRazonSocial();
        String mailTo = emailReenvio;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su Documento Fiscal en formato PDF";
        String subjet = "Nota de Crédito N° " + notaCreditoReenvio.getNroNotaCredito();
        String url = "nota-credito/consulta/" + notaCreditoReenvio.getId();
        byte[] data = fileServiceClient.download(url);
        String fileName = notaCreditoReenvio.getNroNotaCredito() + ".pdf";

        if (notaCreditoReenvio.getDigital().equalsIgnoreCase("S")) {
            reenviarFactElectronica();
        } else if (notaCreditoReenvio.getDigital().equalsIgnoreCase("N") && !mailSender.equalsIgnoreCase("N") && !passSender.equalsIgnoreCase("N")) {
            reenviarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, notaCreditoReenvio.getNroNotaCredito(), host, port);
        } else {
            reenviarViaNotificacionSepsa(mensaje, subjet, mailTo, data, fileName);
        }

    }

    public void reenviarFactElectronica() {
        SepsaSiediMonitorService serviceSiediMonitor = new SepsaSiediMonitorService();
        BodyResponse<Factura> respuestaFact = new BodyResponse<>();
        respuestaFact = serviceSiediMonitor.reenviarNotificacion(notaCreditoReenvio.getCdc());

        if (respuestaFact.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la NC al dirección confirmada"));
        }

    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }

    /**
     * Método para reenviar documento por correo
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     * @param from
     * @param passFrom
     * @param cliente
     * @param nroDoc
     */
    public void reenviarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port) {
        try {
            String mensaje = MailUtils.getMailHtml(nroDoc, cliente,null);
            MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host, port);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la NC al dirección confirmada"));
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para reenviar documento via sepsa notificación
     *
     * @param msn
     * @param subject
     * @param to
     * @param file
     * @param fileName
     */
    public void reenviarViaNotificacionSepsa(String msn, String subject, String to, byte[] file, String fileName) {
        Notificacion notificacion = new Notificacion();
        TipoNotificaciones tipoNotificacion = new TipoNotificaciones();
        ArchivoAdjunto archivo = new ArchivoAdjunto();
        NotificacionService servicioNoti = new NotificacionService();
        List<ArchivoAdjunto> notificationAttachments = new ArrayList<>();

        BodyResponse<Notificacion> respuestaNoti = new BodyResponse<>();

        try {
            tipoNotificacion.setCode("MAIL_ALERTAS");

            archivo.setContentType("application/pdf");
            archivo.setFileName(fileName);
            archivo.setData(file);
            notificationAttachments.add(archivo);

            notificacion.setSubject(subject);
            notificacion.setMessage(msn);
            notificacion.setTo(to);
            notificacion.setState("P");
            notificacion.setObservation("--");
            notificacion.setNotificationType(tipoNotificacion);
            notificacion.setNotificationAttachments(notificationAttachments);

            respuestaNoti = servicioNoti.createNotificacion(notificacion);

            if (respuestaNoti.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha reenviado la  NC al dirección confirmada"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener fechas desde y hasta para filtro
     */
    public void asignarFiltrosFecha() {
        Calendar today = Calendar.getInstance();
        notaCreditoFilter.setFechaHasta(today.getTime());

        today.add(Calendar.DATE, -10);
        notaCreditoFilter.setFechaDesde(today.getTime());

    }
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.notaCreditoFilter = new NotaCreditoPojo();
        this.adapterNotaCredito = new NotaCreditoPojoAdapter();
        asignarFiltrosFecha();
        filterNotaCredito();

        create = false;
        show = false;
        listaPrueba.add(1);

        this.adapterFactura = new FacturaAdapter();
        this.facturaFilter = new Factura();
        this.serviceNotaCredito = new NotaCreditoService();
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();
        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.cliente = new Cliente();
        this.adapterCliente = new ClientListAdapter();

        this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
        this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());

        getMotivoEmisionInterno();

        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();

        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("NOTA_CREDITO");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
        this.ignorarPeriodoAnulacion = false;
        obtenerConfiguracionPeriodoAnulacionNC();
        this.notaCreditoReenvio = new NotaCreditoPojo();
        this.emailReenvio = null;
        this.addEmail = false;
        this.fileServiceClient = new FileServiceClient();
    }

}
