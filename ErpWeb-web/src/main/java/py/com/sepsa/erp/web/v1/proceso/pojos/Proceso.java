/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.pojos;

/**
 *
 * @author Jonathan
 */
public class Proceso {
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Código de tipo notificacion
     */
    private String codigoTipoNotificacion;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Identificador de frecuencia de ejecución
     */
    private Integer idFrecuenciaEjecucion;
    
    /**
     * Identificador de tipo de proceso
     */
    private Integer idTipoProceso;
    
    /**
     * Tipo proceso
     */
    private TipoProceso tipoProceso;
    
    /**
     * Frecuencia de ejecución
     */
    private FrecuenciaEjecucion frecuenciaEjecucion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Integer getIdFrecuenciaEjecucion() {
        return idFrecuenciaEjecucion;
    }

    public void setIdFrecuenciaEjecucion(Integer idFrecuenciaEjecucion) {
        this.idFrecuenciaEjecucion = idFrecuenciaEjecucion;
    }

    public Integer getIdTipoProceso() {
        return idTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        this.idTipoProceso = idTipoProceso;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(TipoProceso tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public FrecuenciaEjecucion getFrecuenciaEjecucion() {
        return frecuenciaEjecucion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setFrecuenciaEjecucion(FrecuenciaEjecucion frecuenciaEjecucion) {
        this.frecuenciaEjecucion = frecuenciaEjecucion;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }
}
