package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCobroListAdapter;
import py.com.sepsa.erp.web.v1.factura.filters.CobroFilter;
import py.com.sepsa.erp.web.v1.factura.filters.CobroPojoFilter;
import py.com.sepsa.erp.web.v1.factura.filters.TipoCobroFilter;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroPojo;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.TalonarioPojoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de cobro
 *
 * @author Romina Núñez
 */
public class CobroService extends APIErpFacturacion {

    /**
     * Obtiene la lista de cobros
     *
     * @param cobro Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public CobroAdapter getCobroList(Cobro cobro, Integer page,
            Integer pageSize) {

        CobroAdapter lista = new CobroAdapter();

        Map params = CobroFilter.build(cobro, page, pageSize);

        HttpURLConnection conn = GET(Resource.COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CobroAdapter.class);

            lista = (CobroAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para editar cobro
     *
     * @param cobro
     * @return
     */
    public BodyResponse anularCobro(Cobro cobro) {

        BodyResponse response = new BodyResponse();

        String ANULAR_URL = "cobro/anular/" + cobro.getId();

        HttpURLConnection conn = PUT(ANULAR_URL, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(cobro));
            response = BodyResponse.createInstance(conn, Cobro.class);

        }
        return response;
    }

    /**
     * Método para editar cobro
     *
     * @param cobro
     * @return
     */
    public Cobro editCobro(Cobro cobro) {

        Cobro cobroEdit = new Cobro();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.COBRO.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(cobro));

            response = BodyResponse.createInstance(conn, Cobro.class);

            cobroEdit = ((Cobro) response.getPayload());

        }
        return cobroEdit;
    }

    /**
     *
     * @param params
     * @return
     */
    public DatosCobro getDatoCobro(Map params) {

        DatosCobro cobro = new DatosCobro();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = GET(Resource.COBRO_INICIAL.url,
                ContentType.JSON, params);

        if (conn != null) {

            response = BodyResponse.createInstance(conn, DatosCobro.class);

            cobro = ((DatosCobro) response.getPayload());

        }
        return cobro;
    }

    /**
     * Obtiene la lista de facturas recibidas
     *
     * @param factura Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public CobroTalonarioAdapter getCobroTalonarioList(TalonarioPojo talonario, Integer page,
            Integer pageSize) {

        CobroTalonarioAdapter lista = new CobroTalonarioAdapter();

        Map params = TalonarioPojoFilter.build(talonario, page, pageSize);

        HttpURLConnection conn = GET(Resource.COBRO_TALONARIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CobroTalonarioAdapter.class);

            lista = (CobroTalonarioAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear Cobro
     *
     * @param cobro
     * @return
     */
    public BodyResponse<Cobro> setCobro(Cobro cobro) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.COBRO.url, ContentType.JSON);

        if (conn != null) {

            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(cobro));

            response = BodyResponse.createInstance(conn, Cobro.class);

        }
        return response;
    }

    public byte[] descargaMasiva(List<Integer> cobros) {

        byte[] result = null;

        Cobro cobroParam = new Cobro();

        cobroParam.setIdCobros(cobros);

        HttpURLConnection conn = POST(Resource.DESCARGA_MASIVA.url, ContentType.JSON);

        if (conn != null) {
            
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(cobroParam));
            
            BodyByteArrayResponse response = BodyByteArrayResponse.createInstance(conn);

            result = response.getPayload();

            conn.disconnect();

        }
        return result;
    }

    /**
     * Obtiene la lista de tipo cobro
     *
     * @param tipoDoc
     * @param page
     * @param pageSize
     * @return
     */
    public TipoCobroListAdapter getTipoCobroList(TipoCobro tipoDoc, Integer page,
            Integer pageSize) {

        TipoCobroListAdapter lista = new TipoCobroListAdapter();

        Map params = TipoCobroFilter.build(tipoDoc, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoCobroListAdapter.class);

            lista = (TipoCobroListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de cobros
     *
     * @param cobro Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public CobroPojoAdapter getCobroPojoList(CobroPojo cobro, Integer page,
            Integer pageSize) {

        CobroPojoAdapter lista = new CobroPojoAdapter();
        cobro.setListadoPojo(true);

        Map params = CobroPojoFilter.build(cobro, page, pageSize);

        HttpURLConnection conn = GET(Resource.COBRO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    CobroPojoAdapter.class);

            lista = (CobroPojoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Constructor de FacturacionService
     */
    public CobroService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        COBRO("Cobro", "cobro"),
        COBRO_TALONARIO("Cobro  Talonario", "cobro/talonario"),
        COBRO_INICIAL("datos de cobro", "cobro/datos-crear"),
        DESCARGA_MASIVA("Descarga masiva de cobros", "cobro/descarga-masiva"),
        TIPO_COBRO("Lista tipo cobro", "tipo-cobro");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
