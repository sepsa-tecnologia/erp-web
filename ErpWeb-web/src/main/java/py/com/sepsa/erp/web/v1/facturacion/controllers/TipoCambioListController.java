/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para listar tipo cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("tipoCambioList")
public class TipoCambioListController implements Serializable {

    /**
     * Adaptador para la lista de talonario
     */
    private TipoCambioAdapter tipoCambioAdapterList;
    /**
     * POJO de TipoCambio
     */
    private TipoCambio tipoCambioFilter;
    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter monedaAdapterList;
    /**
     * POJO de Moneda
     */
    private Moneda monedaFilter;
    /**
     * Cliente para el servicio de tipo de cambio
     */
    private TipoCambioService TipoCambioClient;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoCambioAdapter getTipoCambioAdapterList() {
        return tipoCambioAdapterList;
    }

    public void setTipoCambioAdapterList(TipoCambioAdapter tipoCambioAdapterList) {
        this.tipoCambioAdapterList = tipoCambioAdapterList;
    }

    public TipoCambio getTipoCambioFilter() {
        return tipoCambioFilter;
    }

    public void setTipoCambioFilter(TipoCambio tipoCambioFilter) {
        this.tipoCambioFilter = tipoCambioFilter;
    }

    public MonedaAdapter getMonedaAdapterList() {
        return monedaAdapterList;
    }

    public void setMonedaAdapterList(MonedaAdapter monedaAdapterList) {
        this.monedaAdapterList = monedaAdapterList;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener tipo cambio
     */
    public void getTipoCambioList() {
        getTipoCambioAdapterList().setFirstResult(0);
        this.setTipoCambioAdapterList(getTipoCambioAdapterList().fillData(getTipoCambioFilter()));
    }

    /**
     * Método para obtener tipo cambio
     */
    public void getMonedaList() {
        this.setMonedaAdapterList(getMonedaAdapterList().fillData(getMonedaFilter()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.tipoCambioFilter = new TipoCambio();
        getTipoCambioList();
    }

    /**
     * Método para cambiar el estado
     *
     * @param tC
     */
    public void onChangeState(TipoCambio tC) {
        switch (tC.getActivo()) {
            case "S":
                tC.setActivo("N");
                break;
            case "N":
                tC.setActivo("S");
                break;
            default:
                break;
        }

        Integer idTC = TipoCambioClient.editTipoCambio(tC);

        if (idTC != null) {
            getTipoCambioList();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Cambio de estado correctamente!"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error", "Error al cambiar el estado!"));
        }
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.TipoCambioClient = new TipoCambioService();
            this.tipoCambioAdapterList = new TipoCambioAdapter();
            this.tipoCambioFilter = new TipoCambio();
            this.monedaAdapterList = new MonedaAdapter();
            this.monedaFilter = new Moneda();
            getTipoCambioList();
            getMonedaList();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
