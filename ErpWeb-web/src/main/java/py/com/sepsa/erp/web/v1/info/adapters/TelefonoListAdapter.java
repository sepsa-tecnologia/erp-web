/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.remote.TelefonoServiceClient;

/**
 *
 * @author Romina Núñez
 */

public class TelefonoListAdapter extends DataListAdapter<Telefono>  {
    
     /**
     * Cliente para los servicios de clientes
     */
    private final TelefonoServiceClient telefonoService;

    /**
     * Constructor de TipoDatoPersonaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TelefonoListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.telefonoService = new TelefonoServiceClient();
    }

    /**
     * Constructor de TipoDatoListAdapter
     */
    public TelefonoListAdapter() {
        super();
        this.telefonoService = new TelefonoServiceClient();
    }

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TelefonoListAdapter fillData(Telefono searchData) {
        return telefonoService.getTelefonoList(searchData, getFirstResult(),getPageSize());
    }
    

}