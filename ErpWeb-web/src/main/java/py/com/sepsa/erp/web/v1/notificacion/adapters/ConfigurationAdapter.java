/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Configuration;
import py.com.sepsa.erp.web.v1.notificacion.remote.ConfigurationService;

/**
 * Adapter para configuration
 *
 * @author Sergio D. Riveros Vazquez
 */
public class ConfigurationAdapter extends DataListAdapter<Configuration> {

      /**
     * Cliente para los servicios de Configuration value
     */
    private final ConfigurationService ConfigurationClient;

    @Override
    public ConfigurationAdapter fillData(Configuration searchData) {
        return ConfigurationClient.getConfigurationList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de configuration value
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ConfigurationAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.ConfigurationClient = new ConfigurationService();
    }

    /**
     * Constructor de ConfigurationAdapter
     */
    public ConfigurationAdapter() {
        super();
        this.ConfigurationClient = new ConfigurationService();
    }
}
