
package py.com.sepsa.erp.web.v1.system.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.system.pojos.Menu;
import py.com.sepsa.erp.web.v1.system.remote.MenuServiceClient;

/**
 * Adaptador para la lista de menu
 * @author Cristina Insfrán
 */
public class MenuListAdapter extends DataListAdapter<Menu>{
     /**
     * Cliente para los servicios de Menu
     */
    private final MenuServiceClient menuClient;
   
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MenuListAdapter fillData(Menu searchData) {
     
        return menuClient.getMenuList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de MenuListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MenuListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.menuClient = new MenuServiceClient();
    }

    /**
     * Constructor de MenuListAdapter
     */
    public MenuListAdapter() {
        super();
        this.menuClient = new MenuServiceClient();
    }
}
