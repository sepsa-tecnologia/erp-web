package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("facturaLiquidacionDetalle")
public class FacturaLiquidacionDetalleController implements Serializable {

    /**
     * Dato de Liquidacion
     */
    private String idHistoricoLiquidacion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public String getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(String idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    
   
//</editor-fold>


    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idHistoricoLiquidacion = params.get("idHistoricoLiquidacion");
        

    }

}
