package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.LiquidacionPeriodoDetalleAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.LiquidacionPeriodoDetalle;

/**
 * Controlador para lista de detalle de liquidaciones por periodo.
 *
 * @author alext
 */
@ViewScoped
@Named("liquidacionPeriodoDetalle")
public class LiquidacionPeriodoDetalleController implements Serializable {

    private String cliente;
    
    /**
     * Adaptador.
     */
    private LiquidacionPeriodoDetalleAdapter adapter;
    /**
     * Objeto.
     */
    private LiquidacionPeriodoDetalle searchData;


    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public LiquidacionPeriodoDetalleAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(LiquidacionPeriodoDetalleAdapter adapter) {
        this.adapter = adapter;
    }

    public LiquidacionPeriodoDetalle getSearchData() {
        return searchData;
    }

    public void setSearchData(LiquidacionPeriodoDetalle searchData) {
        this.searchData = searchData;
    }
    
    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    //</editor-fold>

    /**
     * Método para generar la lista.
     */
    public void search(Integer idHistoricoLiquidacion, String mes, String ano, String cliente) {
        searchData.setIdHistoricoLiquidacion(idHistoricoLiquidacion);
        searchData.setMes(mes);
        searchData.setAno(ano);
        this.cliente = cliente;
        adapter = new LiquidacionPeriodoDetalleAdapter();
        adapter = adapter.fillData(searchData);
    }

    /**
     * Inicializa los datos del controlador.
     */
    public LiquidacionPeriodoDetalleController() {
        adapter = new LiquidacionPeriodoDetalleAdapter();
        searchData = new LiquidacionPeriodoDetalle();
    }
}
