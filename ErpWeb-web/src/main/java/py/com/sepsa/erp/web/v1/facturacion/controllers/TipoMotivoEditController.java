package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoMotivoService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;

/**
 * Controlador para edición de TipoMotivo
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("tipoMotivoEdit")
public class TipoMotivoEditController implements Serializable {

    /**
     * Cliente para el servicio de motivoEmision.
     */
    private final TipoMotivoService service;
    /**
     * Datos del motivoEmision
     */
    private TipoMotivo tipoMotivo;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoMotivo getTipoMotivo() {
        return tipoMotivo;
    }

    public void setTipoMotivo(TipoMotivo tipoMotivo) {
        this.tipoMotivo = tipoMotivo;
    }

    //</editor-fold>
    /**
     * Método para editar MotivoEmision
     */
    public void edit() {
        BodyResponse<TipoMotivo> response = service.editTipoMotivo(tipoMotivo);

        if (response.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new 
        FacesMessage(FacesMessage.SEVERITY_INFO, "Info", 
                "Tipo de Motivo editado correctamente!"));
        }

    }

    /**
     * Inicializa los datos
     */
    private void init(int id) {
        this.tipoMotivo = service.get(id);
    }

    /**
     * Constructor
     */
    public TipoMotivoEditController() {
        this.service = new TipoMotivoService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
