package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Descuento;
import py.com.sepsa.erp.web.v1.comercial.remote.DescuentoService;

/**
 * Adaptador para la lista de Descuento
 *
 * @author Sergio D. Riveros Vazquez
 */
public class DescuentoAdapter extends DataListAdapter<Descuento> {

    /**
     * Cliente para los servicios de Descuento
     */
    private final DescuentoService serviceDescuento;

    /**
     * Constructor de DescuentoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public DescuentoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceDescuento = new DescuentoService();
    }

    /**
     * Constructor de DescuentoAdapter
     */
    public DescuentoAdapter() {
        super();
        this.serviceDescuento = new DescuentoService();
    }

    @Override
    public DescuentoAdapter fillData(Descuento searchData) {
        return serviceDescuento.getDescuentoList(searchData, getFirstResult(), getPageSize());
    }
}
