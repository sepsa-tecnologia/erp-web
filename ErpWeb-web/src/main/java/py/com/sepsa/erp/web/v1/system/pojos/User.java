package py.com.sepsa.erp.web.v1.system.pojos;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;

/**
 * POJO para los datos de usuario
 *
 * @author Daniel F. Escauriza Arza
 */
public class User {

    /**
     * Identificador de la persona física
     */
    private Integer idPersonaFisica;
    /**
     * Identificador del usuario
     */
    private Integer id;
    /**
     * Identificador del usuario
     */
    private String user;
    /**
     * Nombre del usuario
     */
    private String nombre;
    /**
     * Apellido del usuario
     */
    private String apellido;

    private Profiles perfiles;
    /**
     * Lista de perfiles del usuario
     */
    private List<String> profiles;
    /**
     * Usuario empresas
     */
    private List<UsuarioEmpresa> usuarioEmpresas;
    /**
     * Codigo tipo empresa
     */
    private String codigoTipoEmpresa;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    private String moduloInventario;
    
    private Permisos permisos;

    public Permisos getPermisos() {
        return permisos;
    }

    public void setPermisos(Permisos permisos) {
        this.permisos = permisos;
    }

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    /**
     * Obtiene el identificador del usuario
     *
     * @return Identificador del usuario
     */
    public Integer getId() {
        return id;
    }

    public void setModuloInventario(String moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public String getModuloInventario() {
        return moduloInventario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    /**
     * Setea el identificador del usuario
     *
     * @param id Identificador del usuario
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene el usuario
     *
     * @return Usuario
     */
    public String getUser() {
        return user;
    }

    /**
     * Setea el usuario
     *
     * @param user Usuario
     */
    public void setUser(String user) {
        this.user = user;
    }

    public void setPerfiles(Profiles perfiles) {
        this.perfiles = perfiles;
    }

    public Profiles getPerfiles() {
        return perfiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     *
     * @param usuarioEmpresas
     */
    public void setUsuarioEmpresas(List<UsuarioEmpresa> usuarioEmpresas) {
        this.usuarioEmpresas = usuarioEmpresas;
    }

    /**
     *
     * @return usuarioEmpresas
     */
    public List<UsuarioEmpresa> getUsuarioEmpresas() {
        return usuarioEmpresas;
    }

//</editor-fold>
    /**
     * Crea una instancia de User
     *
     * @param claims Datos del JWT
     * @return Instancia de User
     */
    public static User createInstance(Usuario userInfo) {

        Integer idUsuario = userInfo.getId();
        Integer idPersonaFisica = userInfo.getIdPersonaFisica();
        Integer idEmpresa = userInfo.getUsuarioEmpresas().get(0).getEmpresa().getId();

        String codTipoEmpresa = userInfo.getUsuarioEmpresas().get(0).getEmpresa().getTipoEmpresa().getCodigo();
        String moduloInventario = userInfo.getUsuarioEmpresas().get(0).getEmpresa().getModuloInventario();
        String usuario = userInfo.getUsuario();
        List<UsuarioPerfilRelacionado> profiles = new ArrayList<>();
        profiles = userInfo.getUsuarioPerfiles();

        Permisos permisos = null;
        List<String> perfiles = new ArrayList<>();
        
        for (UsuarioPerfilRelacionado profile : profiles) {
            if (profile.getEstado().getCodigo().equals("ACTIVO")) {
                perfiles.add(profile.getPerfil().getCodigo());
            }
            if(profile.getPerfil().getPermisos() != null && permisos == null){
                permisos = new Gson().fromJson(profile.getPerfil().getPermisos(), Permisos.class);
            }
        }
        permisos = validarPermisos(permisos);

        return new User(idUsuario, usuario, perfiles, userInfo.getUsuarioEmpresas(), idEmpresa, codTipoEmpresa, moduloInventario, idPersonaFisica, permisos);

    }

    public static Permisos validarPermisos(Permisos permisos){
        if(permisos == null){
            permisos = new Permisos();
        }
        if(permisos.getListarFactura() == null){
            permisos.setListarFactura(new PermisoListarFactura());
        }
        if(permisos.getListarNotaCredito()== null){
            permisos.setListarNotaCredito(new PermisoListarNotaCredito());
        }
        if(permisos.getListarNotaRemision()== null){
            permisos.setListarNotaRemision(new PermisoListarNotaRemision());
        }
        if(permisos.getListarNotaDebito()== null){
            permisos.setListarNotaDebito(new PermisoListarNotaDebito());
        }
        if(permisos.getListarCobro()== null){
            permisos.setListarCobro(new PermisoListarCobro());
        }
        return permisos;
    }
    
    
    /**
     * Constructor de User
     *
     * @param id Identificador del usuario
     * @param user Usuario
     * @param profiles Lista de perfiles del usuario
     */
    public User(Integer id, String user, List<String> profiles, List<UsuarioEmpresa> usuarioEmpresas, Integer idEmpresa, String codigoTipoEmpresa, String moduloInventario, Integer idPersonaFisica, Permisos permisos) {
        this.id = id;
        this.user = user;
        this.profiles = profiles;
        this.perfiles = new Profiles(profiles);
        this.usuarioEmpresas = usuarioEmpresas;
        this.idEmpresa = idEmpresa;
        this.codigoTipoEmpresa = codigoTipoEmpresa;
        this.moduloInventario = moduloInventario;
        this.idPersonaFisica = idPersonaFisica;
        this.permisos = permisos;
    }

    /**
     * Constructor de User
     */
    public User() {
    }

}
