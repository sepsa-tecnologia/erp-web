package py.com.sepsa.erp.web.v1.factura.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Talonario;

/**
 * POJO para Nota de Crédito
 *
 * @author Romina Núñez
 */
public class NotaCreditoPojo {

    /**
     * Identificador del tipo de dato
     */
    private Integer id;
    /**
     * Identificador del cliente
     */
    private Integer idEmpresa;
    /**
     * Identificador de la moneda
     */
    private Integer idMoneda;
    /**
     * Identificador de persona/cliente
     */
    private Integer idPersona;
    /**
     * Cod Moneda
     */
    private String codigoMoneda;
    /**
     * Número Nota de Crédito
     */
    private String nroNotaCredito;
    /**
     * Número Nota de Crédito
     */
    private String cdc;
    /**
     * Número Nota de Crédito
     */
    private String timbrado;
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Fecha desde
     */
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    /**
     * Razón Social
     */
    private String razonSocial;
    /**
     * Razón Social
     */
    private String ruc;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Digital
     */
    private String digital;
    /**
     * Anulado
     */
    private String anulado;
    /**
     * Impuesto
     */
    private String impreso;
    /**
     * Entregado
     */
    private String entregado;
    /**
     * Monto IVA 5%
     */
    private BigDecimal montoIva5;
    /**
     * Monto Imponible 5%
     */
    private BigDecimal montoImponible5;
    /**
     * Monto Total 5%
     */
    private BigDecimal montoTotal5;
    /**
     * Monto IVA 10%
     */
    private BigDecimal montoIva10;
    /**
     * Monto Imponible 10%
     */
    private BigDecimal montoImponible10;
    /**
     * Monto Total 10%
     */
    private BigDecimal montoTotal10;
    /**
     * Monto Total Exento
     */
    private BigDecimal montoTotalExento;
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Monto total nota de crédito
     */
    private BigDecimal montoTotalNotaCredito;
    /**
     * Monto total nota de crédito
     */
    private BigDecimal montoTotalGuaranies;

    /**
     * Identificador del talonario
     */
    private Integer idTalonario;
    /**
     * Moneda
     */
    private String moneda;

    /**
     * Direccion
     */
    private Talonario talonario;
    /**
     * RUC
     */
    private Empresa empresa;

    /**
     * Identificador del tipo de dato
     */
    private Integer idMotivoAnulacion;
    /**
     * Descripción Motivo Emision Interno
     */
    private String motivoEmisionInterno;
    /**
     * Motivo Anulación
     */
    private String observacionAnulacion;
    /**
     * Detalles
     */
    private List<NotaCreditoDetalle> notaCreditoDetalles = new ArrayList<>();
    /**
     * Identificador de emisión del motivo
     */
    private Integer idMotivoEmisionInterno;
    /**
     * Número Nota de Crédito
     */
    private Date fechaVencimientoTimbrado;
    /**
     * Listado de detalles de facturaCompra
     */
    private List<NotaCreditoDetalle> notaCreditoCompraDetalles;
    /**
     * Archivo Set
     */
    private String archivoSet;
    /**
     * Nro casa
     */
    private String nroCasa;
    /**
     * Punto de expedicion
     */
    private String puntoExpedicion;
    /**
     * Establecimiento
     */
    private String establecimiento;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * telefono
     */
    private String telefono;
    /**
     * direccion
     */
    private String direccion;
    /**
     * email
     */
    private String email;
    /**
     * Archivo Edi
     */
    private String archivoEdi;
    /**
     * Generado Set
     */
    private String generadoSet;
    /**
     * Generado Edi
     */
    private String generadoEdi;
    /**
     * Codigo de Estado
     */
    private String codigoEstado;
    /**
     * Id factura
     */
    private Integer idFactura;
    /**
     * Ignorar periodo de anulación
     */
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;
    /**
     * Dato para filtro de listado
     */
    private Boolean listadoPojo;
            /**
     * Clave de pago
     */
    private Integer idNaturalezaCliente;
    /*
    * Datos Adicionales
    */
    private String observacion;
    
    private String nroFacturas;
    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the idMotivoEmisionInterno
     */
    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setListadoPojo(Boolean listadoPojo) {
        this.listadoPojo = listadoPojo;
    }

    public Boolean getListadoPojo() {
        return listadoPojo;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public void setGeneradoEdi(String generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public String getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoSet(String generadoSet) {
        this.generadoSet = generadoSet;
    }

    public void setArchivoEdi(String archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public String getGeneradoSet() {
        return generadoSet;
    }

    public String getArchivoEdi() {
        return archivoEdi;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    /**
     *
     * @return the archivo set
     */
    public String getArchivoSet() {
        return archivoSet;
    }

    /**
     *
     * @param archivoSet
     */
    public void setArchivoSet(String archivoSet) {
        this.archivoSet = archivoSet;
    }

    public void setNotaCreditoCompraDetalles(List<NotaCreditoDetalle> notaCreditoCompraDetalles) {
        this.notaCreditoCompraDetalles = notaCreditoCompraDetalles;
    }

    public List<NotaCreditoDetalle> getNotaCreditoCompraDetalles() {
        return notaCreditoCompraDetalles;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    /**
     * @param idMotivoEmisionInterno the idMotivoEmisionInterno to set
     */
    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    /**
     * @return the idMotivoAnulacion
     */
    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    /**
     * @param idMotivoAnulacion the idMotivoAnulacion to set
     */
    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    /**
     * @return the observacionAnulacion
     */
    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    /**
     * @param observacionAnulacion the observacionAnulacion to set
     */
    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Integer getId() {
        return id;
    }

    public String getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(String motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getAnulado() {
        return anulado;
    }

    public void setAnulado(String anulado) {
        this.anulado = anulado;
    }

    public String getImpreso() {
        return impreso;
    }

    public void setImpreso(String impreso) {
        this.impreso = impreso;
    }

    public String getEntregado() {
        return entregado;
    }

    public void setEntregado(String entregado) {
        this.entregado = entregado;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public Talonario getTalonario() {
        return talonario;
    }

    public void setTalonario(Talonario talonario) {
        this.talonario = talonario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public List<NotaCreditoDetalle> getNotaCreditoDetalles() {
        return notaCreditoDetalles;
    }

    public void setNotaCreditoDetalles(List<NotaCreditoDetalle> notaCreditoDetalles) {
        this.notaCreditoDetalles = notaCreditoDetalles;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    public String getNroFacturas() {
        return nroFacturas;
    }

    public void setNroFacturas(String nroFacturas) {
        this.nroFacturas = nroFacturas;
    }
    
    //</editor-fold>
    /**
     * Constructor por defecto
     */
    public NotaCreditoPojo() {
    }

}
