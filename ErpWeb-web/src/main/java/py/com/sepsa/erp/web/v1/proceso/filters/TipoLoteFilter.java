/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.proceso.pojos.TipoLote;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 * Filter para tipo cambio
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoLoteFilter extends Filter {

    /**
     * Agrega el filtro de identificador
     *
     * @param id
     * @return
     */
    public TipoLoteFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de descripcion
     *
     * @param descripcion
     * @return
     */
    public TipoLoteFilter descripcion(String descripcion) {
        if (descripcion != null) {
            params.put("descripcion", descripcion);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de codigo
     *
     * @param codigo
     * @return
     */
    public TipoLoteFilter codigo(String codigo) {
        if (codigo != null) {
            params.put("codigo", codigo);
        }
        return this;
    }


    /**
     * Construye el mapa de parametros
     *
     * @param tipoLote datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(TipoLote tipoLote, Integer page, Integer pageSize) {
        TipoLoteFilter filter = new TipoLoteFilter();

        filter
                .id(tipoLote.getId())
                .descripcion(tipoLote.getDescripcion())
                .codigo(tipoLote.getCodigo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public TipoLoteFilter() {
        super();
    }

}
