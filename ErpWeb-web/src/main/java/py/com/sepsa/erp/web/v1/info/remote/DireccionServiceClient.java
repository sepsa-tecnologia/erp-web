package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.filters.DireccionFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Servicio para la dirección
 *
 * @author Cristina Insfrán
 */
public class DireccionServiceClient extends APIErpCore {

    /**
     * Método para crear la dirección
     *
     * @param direccion
     * @return
     */
    public BodyResponse setDireccion(Direccion direccion) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.DIRECCION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(direccion));

            response = BodyResponse.createInstance(conn, Direccion.class);

        }
        return response;
    }

    /**
     * Método para obtener el listado de direccion
     *
     * @param direccion
     * @param page
     * @param pageSize
     * @return
     */
    public DireccionAdapter getDireccionList(Direccion direccion, Integer page,
            Integer pageSize) {

        DireccionAdapter lista = new DireccionAdapter();

        Map params = DireccionFilter.build(direccion, page, pageSize);

        HttpURLConnection conn = GET(Resource.DIRECCION.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    DireccionAdapter.class);

            lista = (DireccionAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para editar direccion
     *
     * @param direccion
     * @return
     */
    public BodyResponse editDireccion(Direccion direccion) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.DIRECCION.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(direccion));

            response = BodyResponse.createInstance(conn, Direccion.class);


        }
        return response;
    }

    /**
     * Constructor de DireccionServiceClient
     */
    public DireccionServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        DIRECCION("Servicio para direccion", "direccion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
