package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetalles;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Facturas
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("crearOCEmitida")
public class OrdenCompraEmitidaCrearController implements Serializable {

    /**
     * DATOS DE PRUEBA
     */
    /**
     * Objeto Cliente
     */
    private Cliente cliente;
    /**
     * Objeto Cliente
     */
    private Local localProveedor;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFactura;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFilterPrueba;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterClientePrueba2;
    /**
     * Variable de control
     */
    private boolean show;
    /**
     * Linea
     */
    private Integer linea;
    /**
     * ***DATOS REALES****
     */
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFacturaCancelada;
    /**
     * POJO para Factura
     */
    private Factura facturaFilterCancelada;
    /**
     * Servicio Orden de Compra
     */
    private OrdenDeCompraService serviceOrdenCompra;
    /**
     * POJO Factura para crear
     */
    private Factura facturaCreate;
    /**
     * Dato del Cliente
     */
    private String ruc;
    /**
     * Dato del Cliente
     */
    private String direccion;
    /**
     * Dato del Cliente
     */
    private String telefono;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Adapter Historico Liquidación
     */
    private HistoricoLiquidacionAdapter adapterLiquidacion;
    /**
     * Historico Liquidación
     */
    private HistoricoLiquidacion historicoLiquidacionFilter;
    /**
     * Bandera
     */
    private boolean showDetalleCreate;
    /**
     * Lista Detalle
     */
    private List<OrdenCompraDetalles> listaDetalle;
    /**
     * Lista Selected Liquidación
     */
    private List<HistoricoLiquidacion> listSelectedLiquidacion = new ArrayList<>();
    /**
     * Mapa de Datos de Liquidación y nro Linea
     */
    private Map<Integer, Integer> liquidacionDetalle = new HashMap<Integer, Integer>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Producto Adaptador
     */
    private ProductoAdapter adapterProducto;
    /**
     * Service Factura
     */
    private FacturacionService serviceFactura;
    /**
     * Adaptador parala lista de cliente
     */
    private ClientListAdapter adapter;
    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Identificador de Orden de compra
     */
    private String idOC;
    /**
     * Adaptador para la lista de orden de compra
     */
    private OrdenDeCompraAdapter adapterOrdenCompra;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompraCreate;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Codigo de moneda
     */
    private String codMoneda;
    /**
     * Habilitacion de panel de cotizacion
     */
    private boolean habilitarPanelCotizacion;
    /**
     *
     */
    private TipoCambio tipoCambio;
    /**
     *
     */
    private TipoCambioService serviceTipoCambio;
    /**
     *
     */
    private boolean archivoEdi;

    /**
     * Objeto producto
     */
    private Producto producto;
    /**
     *
     */
    private Integer nroLineaProducto;
    /**
     * Objeto producto
     */
    private Producto productoNuevo;
    /**
     *
     */
    private LocalListAdapter localOrigenAdapter;
    /**
     *
     */
    private LocalListAdapter localDestinoAdapter;
    /**
     *
     */
    private Local localOrigen;
    /**
     *
     */
    private Local localDestino;
    /**
     * Adaptador parala lista de cliente
     */
    private ClientListAdapter adapterDestino;
    /**
     *
     */
    private Cliente clienteDestino;

    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * POJO Producto Precio para parámetro
     */
    private ProductoPrecio parametroProductoPrecio;
    /**
     * Unidad Mínima de Venta
     */
    private BigDecimal umv;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    
    public Local getLocalProveedor() {
        return localProveedor;
    }

    public void setLocalProveedor(Local localProveedor) {
        this.localProveedor = localProveedor;
    }
    
    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setParametroProductoPrecio(ProductoPrecio parametroProductoPrecio) {
        this.parametroProductoPrecio = parametroProductoPrecio;
    }

    public ProductoPrecio getParametroProductoPrecio() {
        return parametroProductoPrecio;
    }

    public void setClienteDestino(Cliente clienteDestino) {
        this.clienteDestino = clienteDestino;
    }

    public void setAdapterDestino(ClientListAdapter adapterDestino) {
        this.adapterDestino = adapterDestino;
    }

    public Cliente getClienteDestino() {
        return clienteDestino;
    }

    public ClientListAdapter getAdapterDestino() {
        return adapterDestino;
    }

    public void setArchivoEdi(boolean archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public LocalListAdapter getLocalOrigenAdapter() {
        return localOrigenAdapter;
    }

    public void setLocalOrigenAdapter(LocalListAdapter localOrigenAdapter) {
        this.localOrigenAdapter = localOrigenAdapter;
    }

    public LocalListAdapter getLocalDestinoAdapter() {
        return localDestinoAdapter;
    }

    public void setLocalDestinoAdapter(LocalListAdapter localDestinoAdapter) {
        this.localDestinoAdapter = localDestinoAdapter;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }

    public boolean isArchivoEdi() {
        return archivoEdi;
    }

    public void setProductoNuevo(Producto productoNuevo) {
        this.productoNuevo = productoNuevo;
    }

    public Producto getProductoNuevo() {
        return productoNuevo;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setOrdenDeCompraCreate(OrdenDeCompra ordenDeCompraCreate) {
        this.ordenDeCompraCreate = ordenDeCompraCreate;
    }

    public OrdenDeCompra getOrdenDeCompraCreate() {
        return ordenDeCompraCreate;
    }

    public void setServiceTipoCambio(TipoCambioService serviceTipoCambio) {
        this.serviceTipoCambio = serviceTipoCambio;
    }

    public TipoCambioService getServiceTipoCambio() {
        return serviceTipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setHabilitarPanelCotizacion(boolean habilitarPanelCotizacion) {
        this.habilitarPanelCotizacion = habilitarPanelCotizacion;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public boolean isHabilitarPanelCotizacion() {
        return habilitarPanelCotizacion;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public OrdenDeCompraAdapter getAdapterOrdenCompra() {
        return adapterOrdenCompra;
    }

    public void setAdapterOrdenCompra(OrdenDeCompraAdapter adapterOrdenCompra) {
        this.adapterOrdenCompra = adapterOrdenCompra;
    }

    public void setIdOC(String idOC) {
        this.idOC = idOC;
    }

    public String getIdOC() {
        return idOC;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapter(ClientListAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientListAdapter getAdapter() {
        return adapter;
    }

    public void setClienteFactura(Cliente clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public Cliente getClienteFactura() {
        return clienteFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListaDetalle(List<OrdenCompraDetalles> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<OrdenCompraDetalles> getListaDetalle() {
        return listaDetalle;
    }

    public Map<Integer, Integer> getLiquidacionDetalle() {
        return liquidacionDetalle;
    }

    public void setLiquidacionDetalle(Map<Integer, Integer> liquidacionDetalle) {
        this.liquidacionDetalle = liquidacionDetalle;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListSelectedLiquidacion(List<HistoricoLiquidacion> listSelectedLiquidacion) {
        this.listSelectedLiquidacion = listSelectedLiquidacion;
    }

    public List<HistoricoLiquidacion> getListSelectedLiquidacion() {
        return listSelectedLiquidacion;
    }

    public void setShowDetalleCreate(boolean showDetalleCreate) {
        this.showDetalleCreate = showDetalleCreate;
    }

    public boolean isShowDetalleCreate() {
        return showDetalleCreate;
    }

    public void setHistoricoLiquidacionFilter(HistoricoLiquidacion historicoLiquidacionFilter) {
        this.historicoLiquidacionFilter = historicoLiquidacionFilter;
    }

    public void setAdapterLiquidacion(HistoricoLiquidacionAdapter adapterLiquidacion) {
        this.adapterLiquidacion = adapterLiquidacion;
    }

    public HistoricoLiquidacion getHistoricoLiquidacionFilter() {
        return historicoLiquidacionFilter;
    }

    public HistoricoLiquidacionAdapter getAdapterLiquidacion() {
        return adapterLiquidacion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setFacturaCreate(Factura facturaCreate) {
        this.facturaCreate = facturaCreate;
    }

    public Factura getFacturaCreate() {
        return facturaCreate;
    }

    public void setFacturaFilterCancelada(Factura facturaFilterCancelada) {
        this.facturaFilterCancelada = facturaFilterCancelada;
    }

    public void setAdapterFacturaCancelada(FacturaAdapter adapterFacturaCancelada) {
        this.adapterFacturaCancelada = adapterFacturaCancelada;
    }

    public FacturaAdapter getAdapterFacturaCancelada() {
        return adapterFacturaCancelada;
    }

    public Factura getFacturaFilterCancelada() {
        return facturaFilterCancelada;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setAdapterFactura(FacturaAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Cliente getClienteFilterPrueba() {
        return clienteFilterPrueba;
    }

    public void setClienteFilterPrueba(Cliente clienteFilterPrueba) {
        this.clienteFilterPrueba = clienteFilterPrueba;
    }

    public void setAdapterClientePrueba2(ClientListAdapter adapterClientePrueba2) {
        this.adapterClientePrueba2 = adapterClientePrueba2;
    }

    public ClientListAdapter getAdapterClientePrueba2() {
        return adapterClientePrueba2;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public void setServiceOrdenCompra(OrdenDeCompraService serviceOrdenCompra) {
        this.serviceOrdenCompra = serviceOrdenCompra;
    }

    public OrdenDeCompraService getServiceOrdenCompra() {
        return serviceOrdenCompra;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

//</editor-fold>
    /**
     * Método para obtener el cliente
     *
     * @param event
     */
    public void onItemSelectClienteOrigen(SelectEvent event) {
        ordenDeCompraCreate.setIdClienteDestino(((Local) event.getObject()).getIdPersona());
        ordenDeCompraCreate.setIdLocalDestino(((Local) event.getObject()).getId());
        Cliente cliente = new Cliente();    
        cliente.setIdCliente(((Local) event.getObject()).getIdPersona());
       
        adapter = adapter.fillData(cliente);
        
        if (((Cliente) event.getObject()).getIdCanalVenta() == null) {
            parametroProductoPrecio.setIdCanalVenta(0);
        } else {
            parametroProductoPrecio.setIdCanalVenta(((Cliente) event.getObject()).getIdCanalVenta());
        }
        
    }

    /**
     * Método para obtener el cliente
     *
     * @param event
     */
    public void onItemSelectClienteDestino(SelectEvent event) {
        ordenDeCompraCreate.setIdClienteDestino(((Cliente) event.getObject()).getIdCliente());

    }

    /**
     * Método para crear la factura
     */
    public void crearFactura() {
        boolean saveFactura = true;

        if (listaDetalle.isEmpty() || listaDetalle == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe agregar al menos un detalle de factura!"));
            saveFactura = false;
        } else {
            for (int i = 0; i < listaDetalle.size(); i++) {
                int dn = i + 1;

                if (listaDetalle.get(i).getPrecioUnitarioConIva() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el precio unitario con IVA en el detalle N° " + dn));
                    saveFactura = false;
                }

                if (listaDetalle.get(i).getCantidadConfirmada() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la cantidad en el detalle N° " + dn));
                    saveFactura = false;
                }

            }
        }

        if (facturaCreate.getRazonSocial().trim().isEmpty() || facturaCreate.getRazonSocial() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la razón social del cliente!"));
            saveFactura = false;
        }

        if (facturaCreate.getRuc().trim().isEmpty() || facturaCreate.getRuc() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el ruc del cliente!"));
            saveFactura = false;
        }

        calcularTotalesGenerales();

        facturaCreate.setAnulado("N");
        facturaCreate.setCobrado("N");
        facturaCreate.setImpreso("N");
        facturaCreate.setEntregado("N");
        facturaCreate.setDigital("N");

        for (int i = 0; i < listaDetalle.size(); i++) {
            listaDetalle.get(i).setNroLinea(i + 1);
        }

    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        this.ordenDeCompraCreate = new OrdenDeCompra();
        this.ordenDeCompraCreate.setArchivoEdi("S");
        this.showDetalleCreate = false;
        this.listaDetalle = new ArrayList<>();
        this.cliente = new Cliente();
        this.localDestino = new Local();

        PF.current().ajax().update(":list-oc-create-form:form-data:clienteOrigen");
        PF.current().ajax().update(":list-oc-create-form:form-data:localDestino");
    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {

        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setCodigoMoneda(obtenerCodMoneda());
        montoLetrasFilter.setMonto(facturaCreate.getMontoTotalFactura());

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
        facturaCreate.setTotalLetras(montoLetrasFilter.getTotalLetras());
    }

    /**
     * Método para mostrar Datatable de Detalle al crear
     */
    public void showDetalle() {
        showDetalleCreate = true;

        addDetalle();
    }

    /**
     * Método para agregar detalle
     */
    public void addDetalle() {
        OrdenCompraDetalles detalle = new OrdenCompraDetalles();

        if (listaDetalle.isEmpty()) {
            detalle.setNroLinea(linea);
        } else {
            linea++;
            detalle.setNroLinea(linea);
        }
        detalle.setCantidad(BigDecimal.ONE);
        detalle.setPorcentajeIva(10);
        detalle.setMontoImponible(BigDecimal.valueOf(0));
        detalle.setMontoIva(BigDecimal.valueOf(0));
        detalle.setMontoTotal(BigDecimal.valueOf(0));

        listaDetalle.add(detalle);
    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        ordenDeCompraCreate.setMontoIva5(montoIva5Acumulador);
        ordenDeCompraCreate.setMontoImponible5(montoImponible5Acumulador);
        ordenDeCompraCreate.setMontoTotal5(montoTotal5Acumulador);
        ordenDeCompraCreate.setMontoIva10(montoIva10Acumulador);
        ordenDeCompraCreate.setMontoImponible10(montoImponible10Acumulador);
        ordenDeCompraCreate.setMontoTotal10(montoTotal10Acumulador);
        ordenDeCompraCreate.setMontoTotalExento(montoExcentoAcumulador);
        ordenDeCompraCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        ordenDeCompraCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos(Integer nroLinea) {
        OrdenCompraDetalles info = null;

        for (OrdenCompraDetalles info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();

        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        BigDecimal iva = new BigDecimal(info.getPorcentajeIva());
        BigDecimal ceroPorc = new BigDecimal("0");
        BigDecimal cincoPorc = new BigDecimal("5");
        BigDecimal diezPorc = new BigDecimal("10");
        BigDecimal prueba = new BigDecimal("0");

        if (iva.compareTo(ceroPorc) == 0) {
            BigDecimal resultCero = new BigDecimal("0");
            info.setPrecioUnitarioSinIva(precioUnitarioConIVA);
            info.setMontoIva(resultCero);
            info.setMontoImponible(montoPrecioXCantidad);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (iva.compareTo(cincoPorc) == 0) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (iva.compareTo(diezPorc) == 0) {
            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal precioUnitarioSinIVARedondeado = precioUnitarioSinIVA.setScale(0, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
            BigDecimal montoImponibleRedondeado = montoImponible.setScale(0, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoPrecioXCantidad.subtract(montoImponibleRedondeado);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVARedondeado);
            info.setMontoImponible(montoImponibleRedondeado);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);
            info.setCantidadConfirmada(cantidad);
            info.setCantidadSolicitada(cantidad);

        }
        if (info.getMontoTotal().compareTo(prueba) == 0) {
            WebLogger.get().debug("EL MONTO TOTAL ES 0");
        } else {
            calcularTotal();
        }

    }

    /**
     * Método para calcular el total de la factura
     */
    public void calcularTotal() {
        BigDecimal totalOC = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            BigDecimal montoTotal = listaDetalle.get(i).getMontoTotal();
            totalOC = totalOC.add(montoTotal);
        }

        ordenDeCompraCreate.setMontoTotalOrdenCompra(totalOC);
    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param rowKey
     */
    public void deleteDetalle(Integer nroLinea) {
        OrdenCompraDetalles info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (OrdenCompraDetalles info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        BigDecimal monto = facturaCreate.getMontoTotalFactura();
        BigDecimal totalFactura = monto.subtract(subTotal);
        facturaCreate.setMontoTotalFactura(totalFactura);

    }

    /**
     * Método para filtrar el producto
     */
    public void filterProducto() {
        producto = new Producto();
        adapterProducto = adapterProducto.fillData(producto);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);
        cliente.setComprador("N");
        cliente.setProveedor("S");

        adapter = adapter.fillData(cliente);

        return adapter.getData();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQueryClienteDestino(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        adapterDestino = adapterDestino.fillData(cliente);

        return adapterDestino.getData();
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        cliente.setComprador("N");
        cliente.setProveedor("S");
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterClienteDestino() {
        this.adapterDestino = adapterDestino.fillData(clienteDestino);
    }

    /**
     * Método para obtener Moneda
     */
    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }

    public String obtenerCodMoneda() {
        Moneda mf = new Moneda();
        MonedaAdapter am = new MonedaAdapter();
        mf.setId(facturaCreate.getIdMoneda());
        am = am.fillData(mf);

        return am.getData().get(0).getCodigo();
    }

    /**
     * Método para calcular el precio unitario con gs de referencia
     *
     * @param facturaDetalle
     * @param precioCompraMoneda
     */
    public void calcularCambioCompra(FacturaDetalle facturaDetalle, BigDecimal precioCompraMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.divide(precioCompraMoneda, 2, RoundingMode.HALF_UP);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para calcular el precio unitario a gs
     *
     * @param facturaDetalle
     * @param precioVentaMoneda
     */
    public void calcularCambioVenta(FacturaDetalle facturaDetalle, BigDecimal precioVentaMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.multiply(precioVentaMoneda);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para crear OC
     */
    public void crearOC() {
        boolean crear = true;
        Calendar today = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
        String id = sdf.format(new Date());
        ordenDeCompraCreate.setNroOrdenCompra("GM-" + id);
        ordenDeCompraCreate.setRecibido("N");
        ordenDeCompraCreate.setCodigoEstado("ENVIADO");
        ordenDeCompraCreate.setAnulado("N");
        ordenDeCompraCreate.setArchivoEdi("S");
        ordenDeCompraCreate.setGeneradoEdi("N");
        ordenDeCompraCreate.setFechaRecepcion(today.getTime());

        ordenDeCompraCreate.setOrdenCompraDetalles(listaDetalle);

        calcularTotalesGenerales();
        if (ordenDeCompraCreate.getIdClienteOrigen() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicarse el cliente!"));
            crear = false;
        }
        if (ordenDeCompraCreate.getIdLocalOrigen() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicarse la sucursal!"));
            crear = false;
        }
        if (crear == true) {
            BodyResponse<OrdenDeCompra> respuestaDeOC = serviceOrdenCompra.createOrdenCompra(ordenDeCompraCreate);
            if (respuestaDeOC.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Orden de Compra creada correctamente!"));
                clearForm();
            }
        }

    }

    /**
     * Método para obtener estado
     *
     * @param cod
     * @return
     */
    public Integer obtenerEstado(String cod) {
        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoOC = new Estado();
        estadoOC.setCodigo(cod);
        estadoOC.setCodigoTipoEstado("ORDEN_COMPRA");
        adapterEstado = adapterEstado.fillData(estadoOC);
        return adapterEstado.getData().get(0).getId();
    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        producto = new Producto();
        producto.setDescripcion(query);
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea;
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        productoNuevo = new Producto();
        productoNuevo = (Producto) event.getObject();
        if (((Producto) event.getObject()).getUmv() != null) {
            umv = ((Producto) event.getObject()).getUmv();
        } else {
            umv = BigDecimal.valueOf(1);
        }

    }

    /**
     * Método para agregar el producto con validaciones
     */
    public void agregarProducto() {
        Calendar today = Calendar.getInstance();
        boolean save = true;
        OrdenCompraDetalles info = null;
        ProductoService serviceProductoPrecio = new ProductoService();

        for (OrdenCompraDetalles info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (OrdenCompraDetalles info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), productoNuevo.getId())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }

        if (save == true) {

            if (parametroProductoPrecio.getIdMoneda() == null) {
                parametroProductoPrecio.setIdMoneda(0);
            }
            parametroProductoPrecio.setIdProducto(productoNuevo.getId());
            parametroProductoPrecio.setFecha(today.getTime());

            if (ordenDeCompraCreate.getIdClienteOrigen() != null) {
                parametroProductoPrecio.setIdCliente(ordenDeCompraCreate.getIdClienteOrigen());
            } else {
                parametroProductoPrecio.setIdCanalVenta(0);
                parametroProductoPrecio.setIdCliente(0);
            }

            ProductoPrecio prodRespuesta = new ProductoPrecio();
            WebLogger.get().debug("PRIMERA");
            try
            {
                prodRespuesta = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);
            } catch (Exception e) {
                WebLogger.get().debug("SEGUNDA");
            }
           
            if (prodRespuesta.getPrecio() == null) {
                parametroProductoPrecio.setIdCliente(0);
                parametroProductoPrecio.setIdCanalVenta(0);
                ProductoPrecio prodRespuesta2 = new ProductoPrecio();
                WebLogger.get().debug("SEGUNDA");
                prodRespuesta2 = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);
                if (prodRespuesta2 == null) {
                    info.setIdProducto(productoNuevo.getId());
                    info.setProducto(productoNuevo);
                    info.setCantidad(umv);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El producto seleccionado no tiene un precio general asociado activo"));
                } else {
                    info.setIdProducto(productoNuevo.getId());
                    info.setProducto(productoNuevo);
                    info.setCantidad(umv);
                    info.setPrecioUnitarioConIva(prodRespuesta2.getPrecio());
                    calcularMontos(nroLineaProducto);
                }

            } else {
                info.setIdProducto(productoNuevo.getId());
                info.setProducto(productoNuevo);
                info.setCantidad(umv);
                info.setPrecioUnitarioConIva(prodRespuesta.getPrecio());
                calcularMontos(nroLineaProducto);
            }

        }
    }

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
        ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
        configuracionValorFilter.setCodigoConfiguracion("PRODUCTO_PRECIO_MONEDA");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        MonedaAdapter adapterMonedaDefecto = new MonedaAdapter();
        Moneda monedadefecto = new Moneda();
        monedadefecto.setCodigo(adapterConfigValor.getData().get(0).getValor());
        adapterMonedaDefecto = adapterMonedaDefecto.fillData(monedadefecto);
        if (!adapterMonedaDefecto.getData().isEmpty()) {
            parametroProductoPrecio.setIdMoneda(adapterMonedaDefecto.getData().get(0).getId());
        }
    }

    /**
     * Método para filtrar los locales de origen
     */
    public void filterLocalOrigen() {
        localOrigen.setLocalExterno("N");
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
    }

    /**
     * Método para filtrar los locales destino
     */
    public void filterLocalDestino() {
        localDestino.setLocalExterno("S");
        localDestinoAdapter = localDestinoAdapter.fillData(localDestino);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryOrigen(String query) {
        Local localOrigen = new Local();
        localOrigen.setLocalExterno("S");
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQuerySucursal(String query) {
        Local localDestino = new Local();
        localDestino.setDescripcion(query);
        localDestino.setLocalExterno("N");
        localDestinoAdapter = localDestinoAdapter.fillData(localDestino);
        return localDestinoAdapter.getData();
    }

    /**
     * Selecciona local
     *
     * @param event
     */
    public void onItemSelectLocalDestinoFilter(SelectEvent event) {
        ordenDeCompraCreate.setIdLocalOrigen(((Local) event.getObject()).getId());
        ordenDeCompraCreate.setIdClienteOrigen(((Local) event.getObject()).getCliente().getIdCliente());
    }

    /**
     * Selecciona local
     *
     *
     *
     * public void onItemSelectLocalDestinoFilter(SelectEvent event) {
     * ordenDeCompraCreate.setIdLocalDestino(((Local)
     * event.getObject()).getId()); }
     *
     */
    public void onChange() {
        String archivo = ordenDeCompraCreate.getArchivoEdi();
        WebLogger.get().debug("archivo edi: " + archivo);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        /**
         * DATOS REALES
         */
        this.codMoneda = null;
        this.digital = "N";
        this.facturaCreate = new Factura();

        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();

        this.adapterFactura = new FacturaAdapter();

        this.adapterFacturaCancelada = new FacturaAdapter();
        this.adapterProducto = new ProductoAdapter();

        this.ruc = "--";
        this.direccion = "--";
        this.telefono = "--";

        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();

        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();

        //OC
        this.serviceOrdenCompra = new OrdenDeCompraService();
        this.ordenDeCompraCreate = new OrdenDeCompra();
        this.ordenDeCompraCreate.setArchivoEdi("S");
        this.archivoEdi = false;
        filterProducto();
        this.nroLineaProducto = null;
        this.localDestinoAdapter = new LocalListAdapter();
        this.localOrigenAdapter = new LocalListAdapter();
        this.localOrigen = new Local();
        this.localDestino = new Local();
        this.clienteDestino = new Cliente();
        this.adapterDestino = new ClientListAdapter();
        this.cliente = new Cliente();
        this.adapter = new ClientListAdapter();
        this.parametroProductoPrecio = new ProductoPrecio();
        obtenerConfiguracion();
        filterCliente();
        filterClienteDestino();
        filterLocalOrigen();
        filterLocalDestino();

    }

}
