/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.usuario.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioEmpresaListAdapter;
import py.com.sepsa.erp.web.v1.usuario.adapters.UsuarioEmpresaRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.usuario.filters.UsuarioEmpresaFilter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioEmpresa;

/**
 * Cliente para Usuario-Empresa
 *
 * @author Sergio D. Riveros Vazquez
 */
public class UsuarioEmpresaServiceClient extends APIErpCore {

    /**
     * Obtiene la lista de usuario-empresa
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioEmpresaListAdapter getUsuarioEmpresaList(UsuarioEmpresa dato, Integer page,
            Integer pageSize) {

        UsuarioEmpresaListAdapter lista = new UsuarioEmpresaListAdapter();

        Map params = UsuarioEmpresaFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_EMPRESA.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioEmpresaListAdapter.class);

            lista = (UsuarioEmpresaListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Obtiene la lista de usuario empresa
     *
     * @param dato Objeto dato
     * @param page
     * @param pageSize
     * @return
     */
    public UsuarioEmpresaRelacionadoAdapter getUsuarioEmpresaRelacionadoList(UsuarioEmpresa dato, Integer page,
            Integer pageSize) {

        UsuarioEmpresaRelacionadoAdapter lista = new UsuarioEmpresaRelacionadoAdapter();

        Map params = UsuarioEmpresaFilter.build(dato, page, pageSize);

        HttpURLConnection conn = GET(Resource.USUARIO_EMPRESA_RELACIONADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    UsuarioEmpresaRelacionadoAdapter.class);

            lista = (UsuarioEmpresaRelacionadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para editar el Usuario Empresa editado/creado
     *
     * @param usuarioEmpresa
     * @return
     */
    public UsuarioEmpresa editAsociacionUsuarioEmpresa(UsuarioEmpresa usuarioEmpresa) {
        UsuarioEmpresa usuarioLocalEdit = new UsuarioEmpresa();
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.USUARIO_EMPRESA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuarioEmpresa));
            response = BodyResponse.createInstance(conn, UsuarioEmpresa.class);
            usuarioLocalEdit = ((UsuarioEmpresa) response.getPayload());
        }
        return usuarioLocalEdit;
    }

    /**
     * Asocia las empresas a un usuario
     *
     * @param usuarioEmpresa
     * @return
     */
    public Boolean asociarMasivamenteUsuarioEmpresa(UsuarioEmpresa usuarioEmpresa) {

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = PUT(Resource.ASOCIAR_USUARIO_EMPRESA.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuarioEmpresa));

            response = BodyResponse.createInstance(conn, UsuarioEmpresa.class);

        }
        return response.getSuccess();
    }

    /**
     * Metodo para obtener un usuario-empresa segun su idUsuario
     *
     * @param idUsuario
     * @return Usuario
     */
    public UsuarioEmpresa get(Integer idUsuario) {
        UsuarioEmpresa data = new UsuarioEmpresa(idUsuario);
        UsuarioEmpresaListAdapter list = getUsuarioEmpresaList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para editar el Usuario Empresa relacionado editado/creado
     *
     * @param usuarioEmpresaRelacionado
     * @return
     */
    public BodyResponse<UsuarioEmpresa> editAsociacionMasivaUsuarioEmpresaRelacionado(UsuarioEmpresa usuarioEmpresaRelacionado) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.ASOCIAR_USUARIO_EMPRESA.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(usuarioEmpresaRelacionado));
            response = BodyResponse.createInstance(conn, UsuarioEmpresa.class);
        }
        return response;
    }

    /**
     * Constructor
     */
    public UsuarioEmpresaServiceClient() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        USUARIO_EMPRESA("Servicio Usuario Empresa", "usuario-empresa"),
        USUARIO_EMPRESA_RELACIONADO("Servicio Usuario Empresa Relacionado", "usuario-empresa/relacionado"),
        ASOCIAR_USUARIO_EMPRESA("Servicio Usuario Empresa Asociar Masivamente", "usuario-empresa/asociar-masivo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
