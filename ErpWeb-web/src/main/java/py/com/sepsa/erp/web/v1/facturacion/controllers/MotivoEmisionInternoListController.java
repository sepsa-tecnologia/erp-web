/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista listar motivo emision interno
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionInternoList")
public class MotivoEmisionInternoListController implements Serializable {

    /**
     * Adaptador para la lista de motivoEmisionInterno
     */
    private MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList;
    /**
     * POJO de MotivoEmisionInterno
     */
    private MotivoEmisionInterno motivoEmisionInternoFilter;
    /**
     * Adaptador para la lista de tipo Documento
     */
    private MotivoEmisionAdapter motivoEmisionAdapterList;
    /**
     * POJO de tipo documento
     */
    private MotivoEmision motivoEmisionFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoEmisionInternoAdapter getMotivoEmisionInternoAdapterList() {

        return motivoEmisionInternoAdapterList;

    }

    public void setMotivoEmisionInternoAdapterList(MotivoEmisionInternoAdapter motivoEmisionInternoAdapterList) {

        this.motivoEmisionInternoAdapterList = motivoEmisionInternoAdapterList;

    }

    public MotivoEmisionInterno getMotivoEmisionInternoFilter() {

        return motivoEmisionInternoFilter;

    }

    public void setMotivoEmisionInternoFilter(MotivoEmisionInterno motivoEmisionInternoFilter) {

        this.motivoEmisionInternoFilter = motivoEmisionInternoFilter;

    }

    public MotivoEmisionAdapter getMotivoEmisionAdapterList() {
        return motivoEmisionAdapterList;
    }

    public void setMotivoEmisionAdapterList(MotivoEmisionAdapter motivoEmisionAdapterList) {
        this.motivoEmisionAdapterList = motivoEmisionAdapterList;
    }

    public MotivoEmision getMotivoEmisionFilter() {
        return motivoEmisionFilter;
    }

    public void setMotivoEmisionFilter(MotivoEmision motivoEmisionFilter) {
        this.motivoEmisionFilter = motivoEmisionFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener motivoEmisionInternos
     */
    public void getMotivoEmisionInterno() {
        getMotivoEmisionAdapterList().setFirstResult(0);
        this.setMotivoEmisionInternoAdapterList(getMotivoEmisionInternoAdapterList().fillData(getMotivoEmisionInternoFilter()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {

        this.motivoEmisionInternoFilter = new MotivoEmisionInterno();
        getMotivoEmisionInterno();

    }

    /**
     * Método para obtener tipo documento
     */
    public void getMotivoEmision() {
        this.setMotivoEmisionAdapterList(getMotivoEmisionAdapterList().fillData(getMotivoEmisionFilter()));
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("motivo-emision-interno-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct

    public void init() {
        try {
            this.setMotivoEmisionInternoFilter(new MotivoEmisionInterno());
            this.setMotivoEmisionInternoAdapterList(new MotivoEmisionInternoAdapter());
            this.motivoEmisionAdapterList = new MotivoEmisionAdapter();
            this.motivoEmisionFilter = new MotivoEmision();
            getMotivoEmision();
            getMotivoEmisionInterno();

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
