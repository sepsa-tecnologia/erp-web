package py.com.sepsa.erp.web.v1.system.pojos;

import py.com.sepsa.erp.web.v1.info.pojos.Empresa;

/**
 * POJO para los datos de usuario
 *
 * @author Romina Núñez
 */
public class UsuarioEmpresa {

    /**
     * Identificador de la empresa
     */
    private Integer idEmpresa;
    /**
     * Identificador del usuario
     */
    private Integer idUsuario;
    /**
     * Identificador
     */
    private Integer id;
    /**
     * Dato Activo
     */
    private String activo;
    /**
     * POJO Empresa
     */
    private Empresa empresa;
    /**
     * Codigo Tipo Empresa
     */
    private String codigoTipoEmpresa;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public String getActivo() {
        return activo;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public Integer getId() {
        return id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

//</editor-fold>
    /**
     * Constructor de User
     */
    public UsuarioEmpresa() {
    }

}
