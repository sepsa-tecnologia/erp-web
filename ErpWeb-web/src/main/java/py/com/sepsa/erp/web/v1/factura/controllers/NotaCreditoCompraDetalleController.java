package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.pojos.NotaCreditoCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("notaCreditoCompraDetalle")
public class NotaCreditoCompraDetalleController implements Serializable {

    /**
     * Servicio Nota de Credito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * POJO Nota de Crédito
     */
    private NotaCreditoCompra notaCreditoDetalle;
    /**
     * Dato de Nota de Crédito
     */
    private String idNotaCredito;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public String getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setNotaCreditoDetalle(NotaCreditoCompra notaCreditoDetalle) {
        this.notaCreditoDetalle = notaCreditoDetalle;
    }

    public NotaCreditoCompra getNotaCreditoDetalle() {
        return notaCreditoDetalle;
    }
    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    public void setIdNotaCredito(String idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

//</editor-fold>
    /**
     * Método para obtener el detalle de la nota de crédito
     */
    public void obtenerNotaCreditoDetalle() {
        notaCreditoDetalle.setId(Integer.parseInt(idNotaCredito));

        notaCreditoDetalle = serviceNotaCredito.getNotaCreditoCompraDetalle(notaCreditoDetalle);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idNotaCredito = params.get("id");

        this.serviceNotaCredito = new NotaCreditoService();
        this.notaCreditoDetalle = new NotaCreditoCompra();

        obtenerNotaCreditoDetalle();
    }

}
