package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioDetalle;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro para lista de inventario
 *
 * @author Romina Núñez
 */
public class InventarioDetalleFilter extends Filter {

    /**
     * Agrega el filtro por id de inventario
     *
     * @param id
     * @return
     */
    public InventarioDetalleFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idDeposito
     *
     * @param idDeposito
     * @return
     */
    public InventarioDetalleFilter idInventario(Integer idInventario) {
        if (idInventario != null) {
            params.put("idInventario", idInventario);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoDeposito
     *
     * @param codigoDeposito
     * @return
     */
    public InventarioDetalleFilter codigoDeposito(String codigoDeposito) {
        if (codigoDeposito != null && codigoDeposito.trim().isEmpty()) {
            params.put("codigoDeposito", codigoDeposito);
        }
        return this;
    }

    /**
     * Agrega el filtro por deposito
     *
     * @param deposito
     * @return
     */
    public InventarioDetalleFilter deposito(String deposito) {
        if (deposito != null && deposito.trim().isEmpty()) {
            params.put("deposito", deposito);
        }
        return this;
    }

    /**
     * Agrega el filtro por idProducto
     *
     * @param idProducto
     * @return
     */
    public InventarioDetalleFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }

    /**
     * Agrega el filtro por producto
     *
     * @param producto
     * @return
     */
    public InventarioDetalleFilter producto(String producto) {
        if (producto != null && producto.trim().isEmpty()) {
            params.put("producto", producto);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoGtin
     *
     * @param codigoGtin
     * @return
     */
    public InventarioDetalleFilter codigoGtin(String codigoGtin) {
        if (codigoGtin != null && codigoGtin.trim().isEmpty()) {
            params.put("codigoGtin", codigoGtin);
        }
        return this;
    }

    /**
     * Agrega el filtro por codigoInterno
     *
     * @param codigoInterno
     * @return
     */
    public InventarioDetalleFilter codigoInterno(String codigoInterno) {
        if (codigoInterno != null && codigoInterno.trim().isEmpty()) {
            params.put("codigoInterno", codigoInterno);
        }
        return this;
    }

    /**
     * Agrega el filtro por idEstado
     *
     * @param idEstado
     * @return
     */
    public InventarioDetalleFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por estado
     *
     * @param estado
     * @return
     */
    public InventarioDetalleFilter estado(String estado) {
        if (estado != null && estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agregar el filtro de producto
     *
     * @param codigoEstado
     * @return
     */
    public InventarioDetalleFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro por cantidad
     *
     * @param cantidad
     * @return
     */
    public InventarioDetalleFilter cantidad(Integer cantidad) {
        if (cantidad != null) {
            params.put("cantidad", cantidad);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public InventarioDetalleFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param tieneFechaVencimiento
     * @return
     */
    public InventarioDetalleFilter tieneFechaVencimiento(Boolean tieneFechaVencimiento) {
        if (tieneFechaVencimiento != null) {
            params.put("tieneFechaVencimiento", tieneFechaVencimiento);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public InventarioDetalleFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaVencimiento);

                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param inventario datos del filtro
     * @param page página consultada
     * @param pageSize tamaño de la página consultada
     * @return
     */
    public static Map build(InventarioDetalle inventario, Integer page,
            Integer pageSize) {

        InventarioDetalleFilter filter = new InventarioDetalleFilter();

        filter
                .id(inventario.getId())
                .idInventario(inventario.getIdInventario())
                .idProducto(inventario.getIdProducto())
                .producto(inventario.getProducto())
                .codigoGtin(inventario.getCodigoGtin())
                .codigoEstado(inventario.getCodigoEstado())
                .codigoInterno(inventario.getCodigoInterno())
                .fechaVencimiento(inventario.getFechaVencimiento())
                .cantidad(inventario.getCantidad())
                .listadoPojo(inventario.getListadoPojo())
                .tieneFechaVencimiento(inventario.getTieneFechaVencimiento())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public InventarioDetalleFilter() {
        super();
    }

}
