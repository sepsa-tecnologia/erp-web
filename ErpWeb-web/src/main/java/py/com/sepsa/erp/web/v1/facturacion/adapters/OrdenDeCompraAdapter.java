/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;

/**
 * Adapter para orden de compra
 *
 * @author Sergio D. Riveros Vazquez
 */
public class OrdenDeCompraAdapter extends DataListAdapter<OrdenDeCompra> {

    /**
     * Cliente para el servicio de OrdenDeCompra
     */
    private final OrdenDeCompraService serviceOrdenDeCompra;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public OrdenDeCompraAdapter fillData(OrdenDeCompra searchData) {

        return serviceOrdenDeCompra.getOrdenDeCompraList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de OrdenDeCompraAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public OrdenDeCompraAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceOrdenDeCompra = new OrdenDeCompraService();
    }

    /**
     * Constructor de OrdenDeCompraAdapter
     */
    public OrdenDeCompraAdapter() {
        super();
        this.serviceOrdenDeCompra = new OrdenDeCompraService();
    }
}
