package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoAnulacionAdapterList;
import py.com.sepsa.erp.web.v1.facturacion.adapters.RetencionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoAnulacion;
import py.com.sepsa.erp.web.v1.facturacion.pojos.Retencion;
import py.com.sepsa.erp.web.v1.facturacion.remote.RetencionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para lista de retención
 *
 * @author alext , Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("listarRetencion")
public class RetencionListController implements Serializable {

    /**
     * Adaptador de la lista de retención
     */
    private RetencionAdapter adapter;
    /**
     * Pojo de retención
     */
    private Retencion searchData;
    /**
     * Pojo de cliente
     */
    private Cliente cliente;
    /**
     * Adaptador de estado.
     */
    private EstadoAdapter estadoAdapter;
    /**
     * Objeto estado.
     */
    private Estado estadoFilter;
    /**
     * Adaptador de la lista de cliente
     */
    private ClientListAdapter clientAdapter;
    /**
     * Servicio de retencion
     */
    private RetencionService retencionService;
    /**
     * Adaptador de la lista de motivo anulación
     */
    private MotivoAnulacionAdapterList adapterMotivoAnulacion;
    /**
     * Objeto motivo anulación
     */
    private MotivoAnulacion motivoAnulacion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    /**
     * @return the adapterMotivoAnulacion
     */
    public MotivoAnulacionAdapterList getAdapterMotivoAnulacion() {
        return adapterMotivoAnulacion;
    }

    /**
     * @param adapterMotivoAnulacion the adapterMotivoAnulacion to set
     */
    public void setAdapterMotivoAnulacion(MotivoAnulacionAdapterList adapterMotivoAnulacion) {
        this.adapterMotivoAnulacion = adapterMotivoAnulacion;
    }

    /**
     * @return the motivoAnulacion
     */
    public MotivoAnulacion getMotivoAnulacion() {
        return motivoAnulacion;
    }

    /**
     * @param motivoAnulacion the motivoAnulacion to set
     */
    public void setMotivoAnulacion(MotivoAnulacion motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    /**
     * @return the retencionService
     */
    public RetencionService getRetencionService() {
        return retencionService;
    }

    /**
     * @param retencionService the retencionService to set
     */
    public void setRetencionService(RetencionService retencionService) {
        this.retencionService = retencionService;
    }

    public RetencionAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RetencionAdapter adapter) {
        this.adapter = adapter;
    }

    public Retencion getSearchData() {
        return searchData;
    }

    public void setSearchData(Retencion searchData) {
        this.searchData = searchData;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public EstadoAdapter getEstadoAdapter() {
        return estadoAdapter;
    }

    public void setEstadoAdapter(EstadoAdapter estadoAdapter) {
        this.estadoAdapter = estadoAdapter;
    }

    public Estado getEstadoFilter() {
        return estadoFilter;
    }

    public void setEstadoFilter(Estado estadoFilter) {
        this.estadoFilter = estadoFilter;
    }

    //</editor-fold>
    /**
     * Método para filtrar retención
     */
    public void filterRetencion() {
        getAdapter().setFirstResult(0);
        adapter = adapter.fillData(searchData);
    }

    /**
     * Método para limpiar los campos del filtro
     */
    public void clear() {
        cliente = new Cliente();
        searchData = new Retencion();
        filterRetencion();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        this.searchData.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Método para anular la retencion
     *
     * @param retencion
     */
    public void anularRetencion(Retencion retencion) {
        try {
            Retencion ret = new Retencion();
            ret.setId(retencion.getId());
            ret.setIdMotivoAnulacion(retencion.getIdMotivoAnulacion());
            ret.setObservacionAnulacion(retencion.getObservacionAnulacion());
            BodyResponse response = retencionService.anularRetencion(ret);
            if (response.getSuccess() == true) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Retencion Anulada!"));
                searchData = new Retencion();
                filterRetencion();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para filtrar estados de retencion.
     */
    public void listarEstados() {
        this.estadoAdapter = estadoAdapter.fillData(estadoFilter);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.adapter = new RetencionAdapter();
        this.searchData = new Retencion();
        this.cliente = new Cliente();
        this.clientAdapter = new ClientListAdapter();
        this.retencionService = new RetencionService();

        this.estadoAdapter = new EstadoAdapter();
        this.estadoFilter = new Estado();
        this.estadoFilter.setCodigoTipoEstado("RETENCION");

        this.adapterMotivoAnulacion = new MotivoAnulacionAdapterList();
        this.motivoAnulacion = new MotivoAnulacion();

        motivoAnulacion.setActivo('S');
        motivoAnulacion.setCodigoTipoMotivoAnulacion("RETENCION");
        adapterMotivoAnulacion = adapterMotivoAnulacion.fillData(motivoAnulacion);
        listarEstados();
        filterRetencion();
    }

}
