
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.comercial.remote.ProductoContratoService;

/**
 * Adaptador para la lista de producto
 * @author Romina Núñez
 */
public class ProductoContratoAdapter extends DataListAdapter<Producto>{
    
    /**
     * Cliente para el servicio producto
     */
    private final ProductoContratoService productoService;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoContratoAdapter fillData(Producto searchData) {
     
        return productoService.getProductoContratoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ProductoContratoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoContratoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoContratoService();
    }

    /**
     * Constructor de ProductoContratoAdapter
     */
    public ProductoContratoAdapter() {
        super();
        this.productoService= new ProductoContratoService();
    }
}
