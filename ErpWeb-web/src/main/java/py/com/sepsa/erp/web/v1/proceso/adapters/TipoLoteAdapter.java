/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.proceso.pojos.TipoLote;
import py.com.sepsa.erp.web.v1.proceso.remote.TipoLoteService;

/**
 *
 * @author Antonella Lucero
 */
public class TipoLoteAdapter extends DataListAdapter<TipoLote> {

    /**
     * Cliente para el servicio de TipoCambio
     */
    private final TipoLoteService serviceTipoLote;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return TipoCambioAdapter
     */
    @Override
    public TipoLoteAdapter fillData(TipoLote searchData) {

        return serviceTipoLote.getTipoLote(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoCambioAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoLoteAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceTipoLote = new TipoLoteService();
    }

    /**
     * Constructor de TipoLoteAdapter
     */
    public TipoLoteAdapter() {
        super();
        this.serviceTipoLote = new TipoLoteService();
    }
}
