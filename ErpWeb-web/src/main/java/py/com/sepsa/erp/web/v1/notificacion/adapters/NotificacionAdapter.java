/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;
import py.com.sepsa.erp.web.v1.notificacion.remote.NotificacionService;

/**
 * Adapter para Notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotificacionAdapter extends DataListAdapter<Notificacion> {

    /**
     * Cliente para los servicios de Notificacion
     */
    private final NotificacionService notificacionClient;

    @Override
    public NotificacionAdapter fillData(Notificacion searchData) {
        return notificacionClient.getNotificacionList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de Notificacion
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotificacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.notificacionClient = new NotificacionService();
    }

    /**
     * Constructor de notificacionAdapter
     */
    public NotificacionAdapter() {
        super();
        this.notificacionClient = new NotificacionService();
    }

}
