package py.com.sepsa.erp.web.v1.tipoEtiqueta.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEtiqueta;
import py.com.sepsa.erp.web.v1.info.remote.TipoEtiquetaService;

@ViewScoped
@Named("crearTipoEtiqueta")
public class TipoEtiquetaCrearController implements Serializable {

    /**
     * Cliente para el servicio de etiqueta.
     */
    private final TipoEtiquetaService service;

    /**
     * Datos del talonario
     */
    private TipoEtiqueta tipoEtiqueta;

    /**
     * Getter y Setters.
     */
    public TipoEtiqueta getTipoEtiqueta() {
        return tipoEtiqueta;
    }

    public void setTipoEtiqueta(TipoEtiqueta tipoEtiqueta) {    
        this.tipoEtiqueta = tipoEtiqueta;
    }

    /**
     * Método para crear y validar nueva etiqueta.
     */
    public void create() {
        tipoEtiqueta = service.create(tipoEtiqueta);
        if (tipoEtiqueta != null && tipoEtiqueta.getId() != null) {
            init();
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, "Tipo Etiqueta creada con éxito!", "Tipo Etiqueta creada con éxito!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear tipo etiqueta!", "Error al crear tipo etiqueta!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.tipoEtiqueta = new TipoEtiqueta();
    }

    /**
     * Método para reiniciar el formulario
     *
     * public void clear() { init();
    }
     */
    /**
     * Constructor de BookNewController
     */
    public TipoEtiquetaCrearController() {
        init();
        this.service = new TipoEtiquetaService();
    }

}
