package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetallePojo;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;

/**
 * Adaptador de la lista de factura
 *
 * @author Romina Núñez
 */
public class FacturaDetallePojoAdapter extends DataListAdapter<FacturaDetallePojo> {

    /**
     * Cliente para el servicio de facturacion
     */
    private final FacturacionService serviceFactura;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public FacturaDetallePojoAdapter fillData(FacturaDetallePojo searchData) {

        return serviceFactura.getFacturaDetallePojoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de FacturaAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public FacturaDetallePojoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceFactura = new FacturacionService();
    }

    /**
     * Constructor de FacturaAdapter
     */
    public FacturaDetallePojoAdapter() {
        super();
        this.serviceFactura = new FacturacionService();
    }
}
