package py.com.sepsa.erp.web.v1.test.controllers;

import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.test.pojos.Test;
import py.com.sepsa.erp.web.v1.test.remote.TestServiceClient;

/**
 * Controlador para crear un test
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("testNew")
public class TestNewController implements Serializable {
   
    /**
     * Cliente para el servicio de test
     */
    private final TestServiceClient serviceClient;
    
    /**
     * Adaptador para la lista de mensajes del servidor
     */
    private MessageListAdapter messageAdapter;
    
    /**
     * Datos test
     */
    private Test test;

    /**
     * Obtiene el adaptador de la lista de mensajes provenientes del servidor
     * @return Adaptador de la lista de mensajes provenientes del servidor
     */
    public MessageListAdapter getMessageAdapter() {
        return messageAdapter;
    }

    /**
     * Setea el adaptador de la lista de mensajes provenientes del servidor
     * @param messageAdapter Adaptador de la lista de mensajes provenientes 
     * del servidor
     */
    public void setMessageAdapter(MessageListAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    /**
     * Obtiene los datos test
     * @return Datos test
     */
    public Test getTest() {
        return test;
    }

    /**
     * Setea los datos test
     * @param test Datos test
     */
    public void setTest(Test test) {
        this.test = test;
    }
    
    /**
     * Método para crear un test
     */
    public void create() {
        messageAdapter = serviceClient.createTest(test);
        
        if(messageAdapter != null 
                && messageAdapter.getMessageList() != null 
                && !messageAdapter.getMessageList().isEmpty()
                && messageAdapter
                        .getMessageList()
                        .get(0)
                        .getType()
                        .equalsIgnoreCase("OK")) {
            initData();
        }
    }
    
    /**
     * Inicializa los datos
     */
    private void initData() {
        this.test = new Test(null, null, null);
    }
    
    /**
     * Método para reiniciar el formulario
     */
    public void clear() {
        initData();
        messageAdapter = new MessageListAdapter();
    }
    
    /** 
     * Constructor de TestNewController
     */
    public TestNewController() {
        initData();
        this.serviceClient = new TestServiceClient();
    }
}