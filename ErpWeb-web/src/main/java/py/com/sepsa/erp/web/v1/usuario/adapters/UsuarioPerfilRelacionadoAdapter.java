
package py.com.sepsa.erp.web.v1.usuario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.UsuarioPerfilRelacionado;
import py.com.sepsa.erp.web.v1.usuario.remote.UsuarioPerfilRelacionadoService;

/**
 * Adaptador para la lista de perfiles de usuario relacionados
 * @author Romina Núñez
 */
public class UsuarioPerfilRelacionadoAdapter extends DataListAdapter<UsuarioPerfilRelacionado>{
    
     /**
     * Cliente para los servicios de usuario perfil relacionado
     */
    private final UsuarioPerfilRelacionadoService usuarioPerfilRelacionado;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public UsuarioPerfilRelacionadoAdapter fillData(UsuarioPerfilRelacionado searchData) {
        return usuarioPerfilRelacionado.getUsuarioPerfilRelacionadoList(searchData, getFirstResult(), getPageSize());

    }

    /**
     * Constructor de UsuarioPerfilRelacionadoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public UsuarioPerfilRelacionadoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.usuarioPerfilRelacionado = new UsuarioPerfilRelacionadoService();
    }

    /**
     * Constructor de UsuarioPerfilListAdapter
     */
    public UsuarioPerfilRelacionadoAdapter() {
        super();
        this.usuarioPerfilRelacionado = new UsuarioPerfilRelacionadoService();
    }

}
