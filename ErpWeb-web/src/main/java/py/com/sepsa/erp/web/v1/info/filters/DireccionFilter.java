
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;

/**
 * Filtro utilizado para dato persona
 * @author Romina Núñez
 */
public class DireccionFilter extends Filter{
    
     
     /**
     * Agrega el filtro de identificador de la direccion
     *
     * @param id Identificador 
     * @return
     */
    public DireccionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

     /**
     * Construye el mapa de parametros
     *
     * @param direccion  datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(Direccion direccion, Integer page, Integer pageSize) {
        DireccionFilter filter = new DireccionFilter();

        filter
                .id(direccion.getId())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DireccionFilter
     */
    public DireccionFilter() {
        super();
    }
}
