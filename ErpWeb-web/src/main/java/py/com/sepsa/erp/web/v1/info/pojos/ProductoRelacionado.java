/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

import java.math.BigDecimal;

/**
 *
 * @author Williams Vera
 */
public class ProductoRelacionado {
    
    /**
     * Identificador de Producto Relacionado
     */
    private Integer id;
    /**
     * Identificador de Producto
     */
    private Integer idProducto;
    /**
     * Identificador de producto relacionado
     */
    private Integer idProductoRelacionado;
    /**
     * Activo
     */
    private String activo;
    /**
     * Porcentaje relacionado
     */
    private BigDecimal porcentajeRelacionado;

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProductoRelacionado() {
        return idProductoRelacionado;
    }

    public void setIdProductoRelacionado(Integer idProductoRelacionado) {
        this.idProductoRelacionado = idProductoRelacionado;
    }

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    /**
     * Constructor
     */
    public ProductoRelacionado() {
    }

}
