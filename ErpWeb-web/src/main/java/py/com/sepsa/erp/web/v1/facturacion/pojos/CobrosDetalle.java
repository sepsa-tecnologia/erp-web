package py.com.sepsa.erp.web.v1.facturacion.pojos;

import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 *
 * @author alext
 */
public class CobrosDetalle {
    
    /**
     * Identificador del detalle
     */
    private Integer id;
    
    /**
     * Identificador de cobro
     */
    private Integer idCobro;
    
    /**
     * Número de línea
     */
    private Integer nroLinea;

    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Monto a ser cobrado
     */
    private Integer montoCobro;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Identificador de concepto de cobro
     */
    private Integer idConceptoCobro;
    
    /**
     * Identificador de tipo de cobro
     */
    private Integer idTipoCobro;
    
    /**
     * Parámetro estado
     */
    private Estado estado;
    
    /**
     * Parámetro conceptoCobro
     */
    private ConceptoCobro conceptoCobro;
    
    /**
     * Parámetro factura
     */
    private Factura factura;
    
    /**
     * Parámetro tipoCobro
     */
    private TipoCobro tipoCobro;

    //<editor-fold defaultstate="collapsed" desc="***Get y Set***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCobro() {
        return idCobro;
    }

    public void setIdCobro(Integer idCobro) {
        this.idCobro = idCobro;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(Integer montoCobro) {
        this.montoCobro = montoCobro;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdConceptoCobro() {
        return idConceptoCobro;
    }

    public void setIdConceptoCobro(Integer idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public Integer getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(Integer idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }
    
     public ConceptoCobro getConceptoCobro() {
        return conceptoCobro;
    }

    public void setConceptoCobro(ConceptoCobro conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }
    
    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public CobrosDetalle() {
    }

}
