package py.com.sepsa.erp.web.v1.system.pojos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.Serializable;
import java.util.Map;
/**
 * Clase para el manejo de la información del usuario
 * @author Jonathan D. Bernal Fernández
 */
public class UserInfo implements Serializable {
    /**
     * Genera una instancia desde un JsonObject
     * @param json JsonObject
     * @return UserInfo
     */
    public static UserInfo generate(JsonObject json) {
        UserInfo user = new UserInfo();
        for (Map.Entry<String, JsonElement> entry : json.entrySet()) {
            JsonElement value = entry.getValue();
            if(value == null || value.isJsonNull()) {
                continue;
            }
            switch(entry.getKey()) {
                case "id":
                    user.setId(value.getAsInt());
                    break;
                case "usuario":
                    user.setUsuario(value.getAsString());
                    break;
                case "idPersonaFisica":
                    user.setIdPersonaFisica(value.getAsInt());
                    break;
                case "nombre":
                    user.setNombre(value.getAsString());
                    break;
                case "apellido":
                    user.setApellido(value.getAsString());
                    break;
            }
        }
        return user;
    }
    /**
     * Identificador del usuario
     */
    private Integer id;
    /**
     * Usuario
     */
    private String usuario;
    /**
     * Identificador de persona física
     */
    private Integer idPersonaFisica;
    /**
     * Nombre 
     */
    private String nombre;
    /**
     * Apellido
     */
    private String apellido;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }
    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}