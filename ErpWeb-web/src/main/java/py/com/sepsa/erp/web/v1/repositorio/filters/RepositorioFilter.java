/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;

/**
 * Filtro para Repositorio
 *
 * @author Romina Núñez
 */
public class RepositorioFilter extends Filter {

    /**
     * Agrega el parametro para el filtro id
     *
     * @param id
     * @return
     */
    public RepositorioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro idTipoRepositorio
     *
     * @param idTipoRepositorio
     * @return
     */
    public RepositorioFilter idTipoRepositorio(Integer idTipoRepositorio) {
        if (idTipoRepositorio != null) {
            params.put("idTipoRepositorio", idTipoRepositorio);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro tipoRepositorio
     *
     * @param tipoRepositorio
     * @return
     */
    public RepositorioFilter tipoRepositorio(String tipoRepositorio) {
        if (tipoRepositorio != null && !tipoRepositorio.trim().isEmpty()) {
            params.put("tipoRepositorio", tipoRepositorio);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro idCliente
     *
     * @param idCliente
     * @return
     */
    public RepositorioFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el parámetro para el filtro cliente
     *
     * @param cliente
     * @return
     */
    public RepositorioFilter cliente(String cliente) {
        if (cliente != null && !cliente.trim().isEmpty()) {
            params.put("cliente", cliente);
        }
        return this;
    }

    /**
     * Agrega el parámetro para el filtro por empresa
     *
     * @param empresa
     * @return
     */
    public RepositorioFilter empresa(Empresa empresa) {
        if (empresa != null) {
            params.put("empresa", empresa);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro claveAcceso
     *
     * @param claveAcceso
     * @return
     */
    public RepositorioFilter claveAcceso(String claveAcceso) {
        if (claveAcceso != null && !claveAcceso.trim().isEmpty()) {
            params.put("claveAcceso", claveAcceso);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro claveSecreta
     *
     * @param claveSecreta
     * @return
     */
    public RepositorioFilter claveSecreta(String claveSecreta) {
        if (claveSecreta != null && !claveSecreta.trim().isEmpty()) {
            params.put("claveSecreta", claveSecreta);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro region
     *
     * @param region
     * @return
     */
    public RepositorioFilter region(String region) {
        if (region != null && !region.trim().isEmpty()) {
            params.put("region", region);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro bucket
     *
     * @param bucket
     * @return
     */
    public RepositorioFilter bucket(String bucket) {
        if (bucket != null && !bucket.trim().isEmpty()) {
            params.put("bucket", bucket);
        }
        return this;
    }

    /**
     * Agrega el parametro para el filtro activo
     *
     * @param activo
     * @return
     */
    public RepositorioFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param repositorio datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Repositorio repositorio, Integer page, Integer pageSize) {
        RepositorioFilter filter = new RepositorioFilter();

        filter
                .id(repositorio.getId())
                .idTipoRepositorio(repositorio.getIdTipoRepositorio())
                .idCliente(repositorio.getIdCliente())
                .empresa(repositorio.getEmpresa())
                .cliente(repositorio.getCliente())
                .claveAcceso(repositorio.getClaveAcceso())
                .claveSecreta(repositorio.getClaveSecreta())
                .activo(repositorio.getActivo())
                .region(repositorio.getRegion())
                .bucket(repositorio.getBucket())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de RepositorioFilter
     */
    public RepositorioFilter() {
        super();
    }
}
