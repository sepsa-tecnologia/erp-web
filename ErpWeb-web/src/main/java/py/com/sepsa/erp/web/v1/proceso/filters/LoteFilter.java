/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.proceso.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.proceso.pojos.Lote;
import py.com.sepsa.erp.web.v1.filters.Filter;

/**
 *
 * @author Antonella Lucero
 */
public class LoteFilter extends Filter {
    /**
     * Agrega el filtro de identificador de ProcesamientoLote
     *
     * @param id
     * @return
     */
    public LoteFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

     /**
     * Agregar el filtro de fecha
     *
     * @param fechaInsercion
     * @return
     */
    public LoteFilter fechaInsercion(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercion", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaDesde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public LoteFilter fechaInsercionDesde(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
     /**
     * Agregar el filtro de fechaHasta
     *
     * @param fechaInsercionHasta
     * @return
     */
    public LoteFilter fechaInsercionHasta(Date fechaInsercion) {
        if (fechaInsercion != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercion);
                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de empresa
     *
     * @param idEmpresa
     * @return
     */
    public LoteFilter idEmpresa(Integer idEmpresa) {
        if (idEmpresa != null) {
            params.put("idEmpresa", idEmpresa);
        }
        return this;
    }

    /**
     * Agregar el filtro de estado
     *
     * @param idEstado
     * @return
     */
    public LoteFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de idTipoLote
     *
     * @param idTipoLote
     * @return
     */
    public LoteFilter idTipoLote(Integer idTipoLote) {
        if (idTipoLote != null) {
            params.put("idTipoLote", idTipoLote);
        }
        return this;
    }
    
    /**
     * Agregar el filtro de digital
     *
     * @return
     */
    public LoteFilter codigoTipoLote(String codigoTipoLote) {
        if (codigoTipoLote != null && !codigoTipoLote.trim().isEmpty()) {
            params.put("codigoTipoLote", codigoTipoLote);
        }
        return this;
    }
    
    public LoteFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param lote datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Lote lote, Integer page, Integer pageSize) {
        LoteFilter filter = new LoteFilter();

        filter
                .id(lote.getId())
                .fechaInsercion(lote.getFechaInsercion())
                .fechaInsercionDesde(lote.getFechaInsercionDesde())
                .fechaInsercionHasta(lote.getFechaInsercionHasta())
                .idEstado(lote.getIdEstado())
                .idEmpresa(lote.getIdEmpresa())
                .idTipoLote(lote.getIdTipoLote())
                .codigoTipoLote(lote.getCodigoTipoLote())
                .listadoPojo(lote.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ProcesamientoLoteFilter
     */
    public LoteFilter() {
        super();
    }
}
