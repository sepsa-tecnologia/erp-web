/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.MotivoService;

/**
 * Adapter para motivo emision
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoAdapter extends DataListAdapter<Motivo> {

    /**
     * Cliente remoto para tipo motivoEmision
     */
    private final MotivoService motivoClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public MotivoAdapter fillData(Motivo searchData) {

        return motivoClient.getMotivo(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoDocumentoSAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public MotivoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.motivoClient = new MotivoService();
    }

    /**
     * Constructor de TipoCambioAdapter
     */
    public MotivoAdapter() {
        super();
        this.motivoClient = new MotivoService();
    }
}
