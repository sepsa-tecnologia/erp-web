package py.com.sepsa.erp.web.v1.usuario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.ChangePass;
import py.com.sepsa.erp.web.v1.system.pojos.Message;
import py.com.sepsa.erp.web.v1.usuario.remote.UserServiceClient;

/**
 * Controlador para cambiar contraseña
 *
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("changePass")
public class ChangePassController implements Serializable {

    /**
     * Datos de la sesión
     */
    @Inject
    private SessionData session;

    /**
     * Cliente para el servicio de usuarios
     */
    private final UserServiceClient serviceClient;

    /**
     * Adaptador para la lista de mensajes del servidor
     */
    private MessageListAdapter messageAdapter;

    /**
     * Datos para cambiar contraseña
     */
    private ChangePass changePass;
    /**
     * Confirmacion de la nueva contraseña
     */
    private String confirNewPassword;

    /**
     * Obtiene el adaptador de la lista de mensajes provenientes del servidor
     *
     * @return Adaptador de la lista de mensajes provenientes del servidor
     */
    public MessageListAdapter getMessageAdapter() {
        return messageAdapter;
    }

    /**
     * Setea el adaptador de la lista de mensajes provenientes del servidor
     *
     * @param messageAdapter Adaptador de la lista de mensajes provenientes del
     * servidor
     */
    public void setMessageAdapter(MessageListAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    /**
     * Obtiene los datos para cambiar contraseña
     *
     * @return Datos para cambiar contraseña
     */
    public ChangePass getChangePass() {
        return changePass;
    }

    /**
     * Setea los datos para cambiar contraseña
     *
     * @param changePass Datos para cambiar contraseña
     */
    public void setChangePass(ChangePass changePass) {
        this.changePass = changePass;
    }

    /**
     * @return the confirNewPassword
     */
    public String getConfirNewPassword() {
        return confirNewPassword;
    }

    /**
     * @param confirNewPassword the confirNewPassword to set
     */
    public void setConfirNewPassword(String confirNewPassword) {
        this.confirNewPassword = confirNewPassword;
    }

    /**
     * Método para cambiar la contraseña del usuario logueado
     */
    public void change() {

        messageAdapter = new MessageListAdapter();
        Message msj = new Message();
        List<Message> msjs = new ArrayList<>();

        if (!changePass.getNuevaContrasena().equals(this.confirNewPassword)) {

            msj = new Message();
            msj.setMessage("No coincide la"
                    + " nueva contraseña con la verificación! ");
            msjs.add(msj);
            messageAdapter.setMessageList(msjs);

        } else {
            messageAdapter = serviceClient.changePass(changePass);

            if (messageAdapter != null
                    && messageAdapter.getMessageList() != null
                    && !messageAdapter.getMessageList().isEmpty()
                    && messageAdapter
                            .getMessageList()
                            .get(0)
                            .getType()
                            .equalsIgnoreCase("OK")) {
                initData();
            } else {
                msj = new Message("Cambio realizado con exito!", null, "OK");
                msjs.add(msj);
                messageAdapter.setMessageList(msjs);
            }
        }
    }
    
    /**
     * Método para cambiar la contraseña del usuario
     */

    /**
     * Inicializa los datos
     */
    @PostConstruct
    private void initData() {
        this.confirNewPassword="";
        this.changePass = new ChangePass(session.getUser().getUser(), null,
                null);
    }

    /**
     * Método para reiniciar el formulario
     */
    public void clear() {
        initData();
        messageAdapter = new MessageListAdapter();
    }

    /**
     * Constructor de ChangePassController
     */
    public ChangePassController() {
        this.serviceClient = new UserServiceClient();
    }

}
