package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.info.adapters.ContactoEmailAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ContactoEmailFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoEmail;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio de contacto email
 *
 * @author Romina Núñez
 */
public class ContactoEmailService extends APIErpCore {

    /**
     * Obtiene la lista de contacto
     *
     * @param contactoEmail
     * @param page
     * @param pageSize
     * @return
     */
    public ContactoEmailAdapter getContactoEmailList(ContactoEmail contactoEmail, Integer page,
            Integer pageSize) {

        ContactoEmailAdapter lista = new ContactoEmailAdapter();

        Map params = ContactoEmailFilter.build(contactoEmail, page, pageSize);

        HttpURLConnection conn = GET(Resource.CONTACTO_EMAIL.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ContactoEmailAdapter.class);

            lista = (ContactoEmailAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;

    }

    /**
     * Método para crear contacto email
     *
     * @param contactoEmail
     * @return
     */
    public ContactoEmail createContactoEmail(ContactoEmail contactoEmail) {

        ContactoEmail cemail = new ContactoEmail();

        BodyResponse response = new BodyResponse();

        HttpURLConnection conn = POST(Resource.CONTACTO_EMAIL.url, ContentType.JSON);
        if (conn != null) {

            Gson gson = new Gson();
            this.addBody(conn, gson.toJson(contactoEmail));

            response = BodyResponse.createInstance(conn, ContactoEmail.class);

            cemail = ((ContactoEmail) response.getPayload());

        }
        return cemail;
    }

    /**
     * Constructor de ContactoEmailService
     */
    public ContactoEmailService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpCore
     */
    public enum Resource {

        //Servicios
        CONTACTO_EMAIL("Servicio Contacto Email", "contacto-email");
        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
