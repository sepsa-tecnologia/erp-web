/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.notificacion.filters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.notificacion.pojos.Notificacion;

/**
 * Filter para notificacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class NotificacionFilter extends Filter {

    /**
     * Agrega el filtro de identificador de notificacion
     *
     * @param id
     * @return
     */
    public NotificacionFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de tipoNotificacion de notificacion
     *
     * @param idTipoNotificacion
     * @return
     */
    public NotificacionFilter idTipoNotificacion(Integer idTipoNotificacion) {
        if (idTipoNotificacion != null) {
            params.put("notificationTypeId", idTipoNotificacion);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha de insercion desde
     *
     * @param fromInsertDate
     * @return
     */
    public NotificacionFilter fromInsertDate(Date fromInsertDate) {
        
        if (fromInsertDate != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fromInsertDate);
                params.put("fromInsertDate", date);
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha de insercion hasta
     *
     * @param toInsertDate
     * @return
     */
    public NotificacionFilter toInsertDate(Date toInsertDate) {
        
        if (toInsertDate != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(toInsertDate);
                params.put("toInsertDate", date);
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha de envio desde
     *
     * @param fromSendDate
     * @return
     */
    public NotificacionFilter fromSendDate(Date fromSendDate) {
        
        if (fromSendDate != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(fromSendDate);
                params.put("fromSendDate", date);
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha de envio hasta
     *
     * @param toSendDate
     * @return
     */
    public NotificacionFilter toSendDate(Date toSendDate) {
        
        if (toSendDate != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                
                String date = simpleDateFormat.format(toSendDate);
                params.put("toSendDate", date);
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * Filtro por asunto.
     *
     * @param subject
     * @return
     */
    public NotificacionFilter subject(String subject) {
        if (subject != null) {
            params.put("subject", subject);
        }
        return this;
    }

    /**
     * Filtro por Destino.
     *
     * @param to
     * @return
     */
    public NotificacionFilter to(String to) {
        if (to != null) {
            params.put("to", to);
        }
        return this;
    }

    /**
     * Filtro por Estado.
     *
     * @param state
     * @return
     */
    public NotificacionFilter state(String state) {
        if (state != null) {
            params.put("state", state);
        }
        return this;
    }

    /**
     * Filtro por Mensaje.
     *
     * @param message
     * @return
     */
    public NotificacionFilter message(String message) {
        if (message != null) {
            params.put("message", message);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param notificacion datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(Notificacion notificacion, Integer page, Integer pageSize) {
        NotificacionFilter filter = new NotificacionFilter();
        
        filter
                .id(notificacion.getId())
                .idTipoNotificacion(notificacion.getNotificationType().getId())
                .fromInsertDate(notificacion.getFromInsertDate())
                .fromSendDate(notificacion.getFromSendDate())
                .toInsertDate(notificacion.getToInsertDate())
                .toSendDate(notificacion.getToSendDate())
                .to(notificacion.getTo())
                .subject(notificacion.getSubject())
                .state(notificacion.getState())
                .message(notificacion.getMessage())
                .page(page)
                .pageSize(pageSize);
        
        return filter.getParams();
    }

    /**
     * Constructor de NotificacionFilter
     */
    public NotificacionFilter() {
        super();
    }
}
