/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoRelacionadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TipoProductoAdapter;
import py.com.sepsa.erp.web.v1.info.filters.ProductoFilter;
import py.com.sepsa.erp.web.v1.info.filters.ProductoPrecioFilter;
import py.com.sepsa.erp.web.v1.info.filters.ProductoRelacionadoFilter;
import py.com.sepsa.erp.web.v1.info.filters.TipoProductoFilter;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.ProductoRelacionado;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProducto;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.pojos.Attach;
import py.com.sepsa.erp.web.v1.producto.adapters.ProductoPrecioListAdapter;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;
import py.com.sepsa.erp.web.v1.remote.APIErpCore;

/**
 * Cliente para el servicio producto info
 *
 * @author Sergio D. Riveros Vazquez, Romina Núñez
 */
public class ProductoService extends APIErpCore {
    
    /**
     * Obtiene la lista de productos precios.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoRelacionadoAdapter getProductoRelacionadoList(ProductoRelacionado producto, Integer page,
            Integer pageSize) {

        ProductoRelacionadoAdapter lista = new ProductoRelacionadoAdapter();

        Map params = ProductoRelacionadoFilter.build(producto, page, pageSize);

        HttpURLConnection conn = GET(Resource.PRODUCTO_RELACIONADO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoRelacionadoAdapter.class);

            lista = (ProductoRelacionadoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }
    
    
    public TipoProductoAdapter getTipoProductoList(TipoProducto param, Integer page,
            Integer pageSize) {

        TipoProductoAdapter lista = new TipoProductoAdapter();

        Map params = TipoProductoFilter.build(param, page, pageSize);

        HttpURLConnection conn = GET(Resource.TIPO_PRODUCTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    TipoProductoAdapter.class);

            lista = (TipoProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de productos precios.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoPrecioListAdapter getProductoPrecioList(ProductoPrecio producto, Integer page,
            Integer pageSize) {

        ProductoPrecioListAdapter lista = new ProductoPrecioListAdapter();

        Map params = ProductoPrecioFilter.build(producto, page, pageSize);

        HttpURLConnection conn = GET(Resource.PRODUCTO_PRECIO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoPrecioListAdapter.class);

            lista = (ProductoPrecioListAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Obtiene la lista de productos.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoAdapter getProductoList(Producto producto, Integer page,
            Integer pageSize) {

        ProductoAdapter lista = new ProductoAdapter();

        Map params = ProductoFilter.build(producto, page, pageSize);

        HttpURLConnection conn = GET(Resource.PRODUCTO.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoAdapter.class);

            lista = (ProductoAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear producto
     *
     * @param producto
     * @return
     */
    public BodyResponse<Producto> setProducto(Producto producto) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.PRODUCTO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(producto));
            response = BodyResponse.createInstance(conn, Producto.class);
        }
        return response;
    }

    /**
     * Método para crear producto precio
     *
     * @param producto
     * @return
     */
    public BodyResponse<ProductoPrecio> setProductoPrecio(ProductoPrecio producto) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.PRODUCTO_PRECIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(producto));
            response = BodyResponse.createInstance(conn, ProductoPrecio.class);
        }
        return response;
    }

    /**
     * Método para crear/editar producto precio
     *
     * @param producto
     * @return
     */
    public BodyResponse<ProductoPrecio> putProductoPrecio(ProductoPrecio producto) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.PRODUCTO_PRECIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(producto));
            response = BodyResponse.createInstance(conn, ProductoPrecio.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public Producto get(Integer id) {
        Producto data = new Producto(id);
        ProductoAdapter list = getProductoList(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Método para editar Producto
     *
     * @param producto
     * @return
     */
    public BodyResponse<Producto> editProducto(Producto producto) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.PRODUCTO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(producto));
            response = BodyResponse.createInstance(conn, Producto.class);
        }
        return response;
    }

    /**
     * Método para crear producto
     *
     * @param producto
     * @return
     */
    public byte[] sendFile(Attach cargaProducto) {

        byte[] result = null;

        HttpURLConnection conn = POST(Resource.CARGA_MASIVA.url, ContentType.MULTIPART);

        if (conn != null) {
            try {
                OutputStream os = conn.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(os), true);
                addFormFile(writer, os, "uploadedFile", cargaProducto);
                BodyByteArrayResponse response = BodyByteArrayResponse
                        .createInstance(conn, writer);

                result = response.getPayload();

                conn.disconnect();

            } catch (IOException ex) {
                WebLogger.get().fatal(ex);
            }
        }

        return result;
    }

    /**
     * Obtiene la lista de productos precios.
     *
     * @param producto
     * @param page
     * @param pageSize
     * @return
     */
    public ProductoPrecio consultaProductoPrecio(ProductoPrecio producto) {

        ProductoPrecio respuesta = new ProductoPrecio();

        Map params = new HashMap();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String fecha_ = format.format(producto.getFecha());
        String service = String.format(Resource.PRODUCTO_PRECIO_CONSULTAR.url, fecha_, producto.getIdProducto(), producto.getIdMoneda(), producto.getIdCanalVenta(), producto.getIdCliente());

        HttpURLConnection conn = GET(service, ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    ProductoPrecio.class);

            respuesta = (ProductoPrecio) response.getPayload();

            conn.disconnect();
        }
        return respuesta;
    }

    /**
     * Constructor de ProductoService
     */
    public ProductoService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APISepsaCore
     */
    public enum Resource {

        //Servicio
        PRODUCTO("Listado de productos", "producto"),
        PRODUCTO_PRECIO_CONSULTAR("Listado de productos", "producto-precio/consultar/%s/%d/%d/%d/%d"),
        CARGA_MASIVA("Carga Masiva de productos", "producto/crear-masivo"),
        PRODUCTO_PRECIO("Listado de producto precio", "producto-precio"),
        PRODUCTO_RELACIONADO("Listado de productos relacionados", "producto-relacionado"),
        TIPO_PRODUCTO("Listado de tipos de producto", "tipo-producto");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
