package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.util.Date;

/**
 * Pojo para reporte de nc de venta
 *
 * @author Alexander Triana
 */
public class ReporteComprobanteNotaCreditoVentaParam {

    /**
     * Indicador de persona
     */
    private Boolean anual;

    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     *Identificador de Cliente 
     */
    private Integer idCliente;

    /**
     * Get y Set
     */
    public Boolean getAnual() {
        return anual;
    }

    public void setAnual(Boolean anual) {
        this.anual = anual;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * Constructor de la clase
     */
    public ReporteComprobanteNotaCreditoVentaParam() {
    }

}
