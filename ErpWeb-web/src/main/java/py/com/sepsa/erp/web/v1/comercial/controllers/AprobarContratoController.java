package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.remote.ContratoService;

/**
 * Controlador para Contrato
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("contratoAprobar")
public class AprobarContratoController implements Serializable {

    /**
     * Adaptador para la lista de contratos
     */
    private ContratoAdapter adapterContratoAprobar;
    /**
     * Objeto Contrato
     */
    private Contrato contratoAprobarFilter;
    /**
     * Servicio Contrato
     */
    private ContratoService serviceContrato;

    public ContratoAdapter getAdapterContratoAprobar() {
        return adapterContratoAprobar;
    }

    public void setServiceContrato(ContratoService serviceContrato) {
        this.serviceContrato = serviceContrato;
    }

    public ContratoService getServiceContrato() {
        return serviceContrato;
    }

    public Contrato getContratoAprobarFilter() {
        return contratoAprobarFilter;
    }

    public void setAdapterContratoAprobar(ContratoAdapter adapterContratoAprobar) {
        this.adapterContratoAprobar = adapterContratoAprobar;
    }

    public void setContratoAprobarFilter(Contrato contratoAprobarFilter) {
        this.contratoAprobarFilter = contratoAprobarFilter;
    }


    public void filterContratoAprobar() {
        adapterContratoAprobar.setFirstResult(0);
        contratoAprobarFilter.setEstado("P");
        this.adapterContratoAprobar = adapterContratoAprobar.fillData(contratoAprobarFilter);

    }
    
    /**
     * Método para aprobar el contrato
     * @param contrato 
     */
    public void aprobarContrato(Contrato contrato){
        Contrato contratoAprobar = new  Contrato();
        
        contratoAprobar.setEstado("P");
        contratoAprobar.setId(contrato.getId());
        
        contratoAprobar = serviceContrato.aprobarContrato(contratoAprobar);
        
        filterContratoAprobar();
        
        if(contratoAprobar!=null){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contrato aprobado!"));    
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error en la aprobación!")); 
        }
    }

    
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.contratoAprobarFilter = new Contrato();
        this.adapterContratoAprobar = new ContratoAdapter();
        this.serviceContrato = new ContratoService();
        filterContratoAprobar();

    }
}
