package py.com.sepsa.erp.web.v1.comercial.controllers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClienteProductoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClienteTipoNegocioAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ConceptoFacturaAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ContratoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ProductoContratoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ServicioProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaRolAsociadoAdapter;
import py.com.sepsa.erp.web.v1.montoMinimo.adapters.MontoMinimoAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteProducto;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClienteTipoNegocio;
import py.com.sepsa.erp.web.v1.comercial.pojos.ConceptoFactura;
import py.com.sepsa.erp.web.v1.comercial.pojos.Contrato;
import py.com.sepsa.erp.web.v1.comercial.pojos.ContratoServicio;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.comercial.pojos.ServicioProducto;
import py.com.sepsa.erp.web.v1.comercial.remote.ClientServiceClient;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaRol;
import py.com.sepsa.erp.web.v1.montoMinimo.pojos.MontoMinimo;
import py.com.sepsa.erp.web.v1.comercial.remote.ContratoService;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.CanalVentaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PaisAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.CanalVenta;
import py.com.sepsa.erp.web.v1.info.pojos.Pais;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador para Contrato
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("contrato")
public class ContratoController implements Serializable {

    /**
     * Servicio Contrato
     */
    private ContratoService serviceContrato;
    /**
     * Servicio Contrato
     */
    private FacturacionService serviceFactura;
    /**
     * Lista de Facturas a actualizar
     */
    private List<FacturaPojo> selectedFacturas;
    /**
     * POJO para Factura
     */
    private FacturaPojo facturaFilter;
    private Integer diasFiltro;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaPojoAdapter adapterFactura;
    /**
     * Lista de servicios seleccionados
     */
    private List<ServicioProducto> listSelectedServicios = new ArrayList<>();

    /**
     * Lista de servicios seleccionados
     */
    private List<ContratoServicio> servicios = new ArrayList<>();
    /**
     * Adaptador para la lista de cliente tipo negocio
     */
    private ClienteTipoNegocioAdapter adaptadorClienteTipoNegocio;
    /**
     * Objeto cliente tipo negocio
     */
    private ClienteTipoNegocio clienteTipoNegocioFilter;
    /**
     * Dato del Cliente
     */
    private String idCliente;
    /**
     * Adaptador para la lista de contratos
     */
    private ContratoAdapter adapter;
    /**
     * Objeto Contrato
     */
    private Contrato contratoFilter;
    /**
     * Adaptador para la lista de asociación cliente-producto
     */
    private ClienteProductoAdapter adapterClienteProducto;
    /**
     * Objeto ClienteProducto
     */
    private ClienteProducto clienteProductoFilter;
    /**
     * Adaptador para la lista de roles
     */
    private PersonaRolAsociadoAdapter adapterPersonaRol;
    /**
     * Objeto PersonaRol
     */
    private PersonaRol personaRolFilter;
    /**
     * Objeto Contrato
     */
    private Contrato contratoCreate;
    /**
     * Adaptador para la lista de concepto factura
     */
    private ConceptoFacturaAdapter adapterConceptoFactura;
    /**
     * Objeto concepto factura
     */
    private ConceptoFactura conceptoFacturaFilter;
    /**
     * Adaptador para la lista de producto
     */
    private ProductoContratoAdapter adapterProductoContrato;
    /**
     * Objeto Producto
     */
    private Producto productoContratoFilter;
    /**
     * Adaptador para la lista de servicios
     */
    private ServicioProductoAdapter adapterServicio;
    /**
     * Objeto Servicio
     */
    private ServicioProducto servicioFilter;
    /**
     * Adaptador para la lista de contratos
     */
    private ContratoAdapter adapterContratoAprobar;
    /**
     * Objeto Contrato
     */
    private Contrato contratoAprobarFilter;
    /**
     * Adaptador para la lista de monto minimo
     */
    private MontoMinimoAdapter adapterMontoMinimo;
    /**
     * POJO Monto Minimo
     */
    private MontoMinimo montoMinimoFilter;
    /**
     * Identificador de monto mínimo seleccionado
     */
    private Integer idMontoMinimo = null;
    /**
     * Fecha Vto Acuerdo
     */
    private Date fechaVtoAcuerdo;
    /**
     * Monto
     */
    private BigDecimal monto;
    /**
     * Adaptador para la lista de cliente
     */
    private ClientListAdapter adapterCliente;
    /**
     * POJO Cliente
     */
    private Cliente clienteFilter;
    /**
     * Dato para habilitación de panel
     */
    private boolean habilitarEdicionCliente;
    /**
     * Cliente para los servicios de cliente
     */
    private ClientServiceClient serviceClient;
    /**
     * Pojo de canal de ventas
     */
    private CanalVenta canalVenta;
    /**
     * Adaptador de canal de ventas
     */
    private CanalVentaListAdapter canalVentaAdapter;
    /**
     * Titulo
     */
    private String titulo;
    /**
     * Descripcion
     */
    private String descripcion;
    
    /**
     * Código de pais
     */
    private String codigoPais;

    private PaisAdapter paisAdapter;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public String getIdCliente() {
        return idCliente;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setServiceClient(ClientServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    public ClientServiceClient getServiceClient() {
        return serviceClient;
    }

    public void setHabilitarEdicionCliente(boolean habilitarEdicionCliente) {
        this.habilitarEdicionCliente = habilitarEdicionCliente;
    }

    public boolean isHabilitarEdicionCliente() {
        return habilitarEdicionCliente;
    }

    public void setClienteFilter(Cliente clienteFilter) {
        this.clienteFilter = clienteFilter;
    }

    public Cliente getClienteFilter() {
        return clienteFilter;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public void setFechaVtoAcuerdo(Date fechaVtoAcuerdo) {
        this.fechaVtoAcuerdo = fechaVtoAcuerdo;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public Date getFechaVtoAcuerdo() {
        return fechaVtoAcuerdo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setServicios(List<ContratoServicio> servicios) {
        this.servicios = servicios;
    }

    public List<ContratoServicio> getServicios() {
        return servicios;
    }

    public void setMontoMinimoFilter(MontoMinimo montoMinimoFilter) {
        this.montoMinimoFilter = montoMinimoFilter;
    }

    public void setAdapterMontoMinimo(MontoMinimoAdapter adapterMontoMinimo) {
        this.adapterMontoMinimo = adapterMontoMinimo;
    }

    public MontoMinimo getMontoMinimoFilter() {
        return montoMinimoFilter;
    }

    public MontoMinimoAdapter getAdapterMontoMinimo() {
        return adapterMontoMinimo;
    }

    public void setContratoAprobarFilter(Contrato contratoAprobarFilter) {
        this.contratoAprobarFilter = contratoAprobarFilter;
    }

    public void setListSelectedServicios(List<ServicioProducto> listSelectedServicios) {
        this.listSelectedServicios = listSelectedServicios;
    }

    public List<ServicioProducto> getListSelectedServicios() {
        return listSelectedServicios;
    }

    public void setAdapterContratoAprobar(ContratoAdapter adapterContratoAprobar) {
        this.adapterContratoAprobar = adapterContratoAprobar;
    }

    public Contrato getContratoAprobarFilter() {
        return contratoAprobarFilter;
    }

    public ContratoAdapter getAdapterContratoAprobar() {
        return adapterContratoAprobar;
    }

    public void setServiceContrato(ContratoService serviceContrato) {
        this.serviceContrato = serviceContrato;
    }

    public ContratoService getServiceContrato() {
        return serviceContrato;
    }

    public void setServicioFilter(ServicioProducto servicioFilter) {
        this.servicioFilter = servicioFilter;
    }

    public void setAdapterServicio(ServicioProductoAdapter adapterServicio) {
        this.adapterServicio = adapterServicio;
    }

    public ServicioProducto getServicioFilter() {
        return servicioFilter;
    }

    public ServicioProductoAdapter getAdapterServicio() {
        return adapterServicio;
    }

    public void setProductoContratoFilter(Producto productoContratoFilter) {
        this.productoContratoFilter = productoContratoFilter;
    }

    public void setAdapterProductoContrato(ProductoContratoAdapter adapterProductoContrato) {
        this.adapterProductoContrato = adapterProductoContrato;
    }

    public Producto getProductoContratoFilter() {
        return productoContratoFilter;
    }

    public ProductoContratoAdapter getAdapterProductoContrato() {
        return adapterProductoContrato;
    }

    public void setContratoCreate(Contrato contratoCreate) {
        this.contratoCreate = contratoCreate;
    }

    public Contrato getContratoCreate() {
        return contratoCreate;
    }

    public void setClienteTipoNegocioFilter(ClienteTipoNegocio clienteTipoNegocioFilter) {
        this.clienteTipoNegocioFilter = clienteTipoNegocioFilter;
    }

    public void setAdaptadorClienteTipoNegocio(ClienteTipoNegocioAdapter adaptadorClienteTipoNegocio) {
        this.adaptadorClienteTipoNegocio = adaptadorClienteTipoNegocio;
    }

    public ClienteTipoNegocio getClienteTipoNegocioFilter() {
        return clienteTipoNegocioFilter;
    }

    public ClienteTipoNegocioAdapter getAdaptadorClienteTipoNegocio() {
        return adaptadorClienteTipoNegocio;
    }

    public void setClienteProductoFilter(ClienteProducto clienteProductoFilter) {
        this.clienteProductoFilter = clienteProductoFilter;
    }

    public void setAdapterClienteProducto(ClienteProductoAdapter adapterClienteProducto) {
        this.adapterClienteProducto = adapterClienteProducto;
    }

    public ClienteProducto getClienteProductoFilter() {
        return clienteProductoFilter;
    }

    public ClienteProductoAdapter getAdapterClienteProducto() {
        return adapterClienteProducto;
    }

    public void setContratoFilter(Contrato contratoFilter) {
        this.contratoFilter = contratoFilter;
    }

    public Contrato getContratoFilter() {
        return contratoFilter;
    }

    public void setAdapter(ContratoAdapter adapter) {
        this.adapter = adapter;
    }

    public ContratoAdapter getAdapter() {
        return adapter;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public PersonaRol getPersonaRolFilter() {
        return personaRolFilter;
    }

    public void setPersonaRolFilter(PersonaRol personaRolFilter) {
        this.personaRolFilter = personaRolFilter;
    }

    public void setAdapterPersonaRol(PersonaRolAsociadoAdapter adapterPersonaRol) {
        this.adapterPersonaRol = adapterPersonaRol;
    }

    public PersonaRolAsociadoAdapter getAdapterPersonaRol() {
        return adapterPersonaRol;
    }

    public ConceptoFactura getConceptoFacturaFilter() {
        return conceptoFacturaFilter;
    }

    public void setConceptoFacturaFilter(ConceptoFactura conceptoFacturaFilter) {
        this.conceptoFacturaFilter = conceptoFacturaFilter;
    }

    public void setAdapterConceptoFactura(ConceptoFacturaAdapter adapterConceptoFactura) {
        this.adapterConceptoFactura = adapterConceptoFactura;
    }

    public ConceptoFacturaAdapter getAdapterConceptoFactura() {
        return adapterConceptoFactura;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public List<FacturaPojo> getSelectedFacturas() {
        return selectedFacturas;
    }

    public void setSelectedFacturas(List<FacturaPojo> selectedFacturas) {
        this.selectedFacturas = selectedFacturas;
    }

    public FacturaPojo getFacturaFilter() {
        return facturaFilter;
    }

    public void setFacturaFilter(FacturaPojo facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Integer getDiasFiltro() {
        return diasFiltro;
    }

    public void setDiasFiltro(Integer diasFiltro) {
        this.diasFiltro = diasFiltro;
    }

    public FacturaPojoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setAdapterFactura(FacturaPojoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }
    
    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public CanalVentaListAdapter getCanalVentaAdapter() {
        return canalVentaAdapter;
    }

    public void setCanalVentaAdapter(CanalVentaListAdapter canalVentaAdapter) {
        this.canalVentaAdapter = canalVentaAdapter;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public PaisAdapter getPaisAdapter() {
        return paisAdapter;
    }

    public void setPaisAdapter(PaisAdapter paisAdapter) {
        this.paisAdapter = paisAdapter;
    }
        
    //</editor-fold>
    /**
     * Método para filtrar el contrato
     */
    public void filterContrato() {
        contratoFilter.setIdCliente(Integer.parseInt(idCliente));
        adapter.setFirstResult(0);
        this.adapter = adapter.fillData(contratoFilter);

    }

    public void habilitarEdicionClientePanel() {
        this.habilitarEdicionCliente = true;
    }

    /**
     * Método para filtrar el producto
     */
    public void filterClienteProducto() {
        clienteProductoFilter.setIdCliente(Integer.parseInt(idCliente));
        adapterClienteProducto.setFirstResult(0);
        this.adapterClienteProducto = adapterClienteProducto.fillData(clienteProductoFilter);

    }

    /**
     * Método para filtrar tipo negocio
     */
    public void filterClienteTipoNegocio() {
        clienteTipoNegocioFilter.setIdCliente(Integer.parseInt(idCliente));
        adaptadorClienteTipoNegocio.setFirstResult(0);
        this.adaptadorClienteTipoNegocio = adaptadorClienteTipoNegocio.fillData(clienteTipoNegocioFilter);

    }

    /**
     * Método para filtrar cliente rol
     */
    public void filterClienteRol() {
        personaRolFilter.setIdPersona(Integer.parseInt(idCliente));
        adapterPersonaRol.setFirstResult(0);
        this.adapterPersonaRol = adapterPersonaRol.fillData(personaRolFilter);

    }

    /**
     * Método para filtrar el concepto factura
     */
    public void filterConceptoFactura() {
        adapterConceptoFactura.setFirstResult(0);
        this.adapterConceptoFactura = adapterConceptoFactura.fillData(conceptoFacturaFilter);

    }

    /**
     * Método para filtrar producto
     */
    public void filterProducto() {
        adapterProductoContrato.setFirstResult(0);
        this.adapterProductoContrato = adapterProductoContrato.fillData(productoContratoFilter);

        contratoCreate.setIdProducto(adapterProductoContrato.getData().get(0).getId());
        filterServicioProducto();
    }

    /**
     * Método para filtrar servicio producto
     */
    public void filterServicioProducto() {
        adapterServicio.setFirstResult(0);
        servicioFilter.setIdProducto(contratoCreate.getIdProducto());
        this.adapterServicio = adapterServicio.fillData(servicioFilter);

    }

    /**
     * Método para filtrar contratos con estado P
     */
    public void filterContratoAprobar() {
        adapterContratoAprobar.setFirstResult(0);
        contratoAprobarFilter.setEstado("P");
        this.adapterContratoAprobar = adapterContratoAprobar.fillData(contratoAprobarFilter);

    }

    /**
     * Método para filtrar monto mínimo
     */
    public void filterMontoMinimo() {
        montoMinimoFilter.setEstado("A");
        adapterMontoMinimo = adapterMontoMinimo.fillData(montoMinimoFilter);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String contratoDetalleURL(Integer id) {
        return String.format("contrato-detalle"
                + "?faces-redirect=true"
                + "&id=%d"
                + "&idCliente=%s", id, idCliente);
    }

    /**
     * Método para asignar el monto mínimo
     *
     * @param idServicio
     */
    public void asignarMontoMinimo(Integer idServicio) {

        for (ContratoServicio info0 : servicios) {
            if (Objects.equals(info0.getIdServicio(), idServicio)) {
                info0.setIdMontoMinimo(idMontoMinimo);
                break;
            }
        }
    }

    /**
     * Método para asignar el monto mínimo
     *
     * @param idServicio
     */
    public void asignarMonto(Integer idServicio) {

        for (ContratoServicio info0 : servicios) {
            if (Objects.equals(info0.getIdServicio(), idServicio)) {
                info0.setMontoAcuerdo(monto);
                break;
            }
        }
    }

    /**
     * Método para asignar el monto mínimo
     *
     * @param idServicio
     */
    public void asignarFechaVto(Integer idServicio) {

        for (ContratoServicio info0 : servicios) {
            if (Objects.equals(info0.getIdServicio(), idServicio)) {
                info0.setFechaVencimientoAcuerdo(fechaVtoAcuerdo);
                break;
            }
        }
    }

    /**
     * Método invocado al seleccionar el servicio
     *
     * @param event
     */
    public void onCheckServicio(SelectEvent event) {
        Integer idServicio = (((ServicioProducto) event.getObject()).getId());
        String servicio = (((ServicioProducto) event.getObject()).getDescripcion());

        ContratoServicio contratoServicioNuevo = new ContratoServicio();

        contratoServicioNuevo.setIdServicio(idServicio);
        contratoServicioNuevo.setServicio(servicio);

        servicios.add(contratoServicioNuevo);
    }

    /**
     * Método invocado para deseleccionar el servicio
     *
     * @param event
     */
    public void onUncheckServicio(UnselectEvent event) {
        Integer idServicio = (((ServicioProducto) event.getObject()).getId());
        ContratoServicio info = null;

        for (ContratoServicio info0 : servicios) {
            if (Objects.equals(info0.getIdServicio(), idServicio)) {
                info = info0;
                servicios.remove(info);
                break;
            }
        }

    }

    /**
     * Método para crear contrato
     */
    public void crearContrato() {
        Integer id;

        contratoCreate.setIdCliente(Integer.parseInt(idCliente));
        contratoCreate.setServicios(servicios);
        contratoCreate.setEstado("P");
        if (!contratoCreate.getServicios().isEmpty() && contratoCreate.getServicios() != null) {
            id = serviceContrato.createContrato(contratoCreate);
            if (id != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contrato creado correctamente!"));
                filterContrato();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al crear el contrato!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe seleccionar al menos un servicio!"));
        }

    }

    public List<MontoMinimo> getMontosMinimos(Integer idServicio) {
        montoMinimoFilter.setIdServicio(idServicio);
        filterMontoMinimo();
        return adapterMontoMinimo.getData();
    }

    /**
     * Método para inactivar descuento
     *
     * @param id
     */
    public void inactivarContrato(Integer id) {
        Contrato contratoInactivado = new Contrato();

        contratoInactivado.setId(id);

        contratoInactivado = serviceContrato.inactivarContrato(contratoInactivado);

        if (contratoInactivado.getEstado().equals("I")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contrato inactivado correctamente!"));
            filterContrato();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al inactivar el Contrato!"));
        }
    }

    /**
     * Método para filtrar el Cliente
     */
    public void filterCliente() {
        clienteFilter.setIdCliente(Integer.parseInt(idCliente));
        adapterCliente = adapterCliente.fillData(clienteFilter);
        clienteFilter = adapterCliente.getData().get(0);
        
        if (clienteFilter.getComprador().equalsIgnoreCase("S")) {
            this.titulo = "EDITAR CLIENTE";
            this.descripcion = "Página para Editar Cliente";
        } else {
            this.titulo = "EDITAR PROVEEDOR";
            this.descripcion = "Página para Editar Proveedor";
        }
        this.canalVenta = this.clienteFilter.getCanalVenta();
        
    }

    public void editarContrato() {
        clienteFilter.setCodigoPais(codigoPais);
        BodyResponse<Cliente> respuestaOC = serviceClient.putCliente(clienteFilter);
        if (respuestaOC.getSuccess()) {
            crearActualizacionMasivaFactura();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Edicion Correcta!"));
            habilitarEdicionCliente = false;
            
        } else {
            for (Mensaje mensaje : respuestaOC.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                "Error", mensaje.getDescripcion()));
            }
        }
    }
     /**
     * Método para filtrar las facturas
     * @param factura
     * @return 
     */
    public List<FacturaPojo> filterfacturas(FacturaPojo factura) {
        Instant from = factura.getFechaDesde().toInstant();
        Instant to = factura.getFechaHasta().toInstant();
        factura.setListadoPojo(true);
        long days = ChronoUnit.DAYS.between(from, to);
      
        Integer maxDias = 31;
        if (diasFiltro != null){
            maxDias = diasFiltro;
        } 
        
        if (days > maxDias) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Fecha Incorrecta", String.format("Fecha Incorrecta", "El rango de fecha no puede ser más de %d días",maxDias)));
        } else {
            this.adapterFactura.setFirstResult(0);
            this.adapterFactura = adapterFactura.fillData(factura);
        }
        return this.adapterFactura.getData();
    }
    public void crearActualizacionMasivaFactura() {
        Date fechaActual = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date fechaDesde = cal.getTime();
        facturaFilter.setIdCliente(clienteFilter.getIdCliente());
        facturaFilter.setFechaDesde(fechaDesde);
        facturaFilter.setFechaHasta(fechaActual);
        facturaFilter.setCodigoEstado("RECHAZADO");
        selectedFacturas = filterfacturas(facturaFilter);
        try {
            BodyResponse<FacturaPojo> respuestaConf=null;
            for (FacturaPojo ins : selectedFacturas) {
                ins.setRuc(clienteFilter.getNroDocumento());
                System.out.println("Cliente "+ins.getIdCliente()+" ruc "+ins.getRuc());
                respuestaConf = serviceFactura.editarFactura(ins);
            }
            if (respuestaConf.getSuccess()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Actualización enviada correctamente"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Ocurrió un error"));
            }
        }catch (Exception e) {
            WebLogger.get().error("Error al enviar actualizacion masivamente");
            FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", e.getMessage()));
            WebLogger.get().error(e.getMessage());
        }
    }

    /* Método para obtener canales de ventas filtrados
     *
     * @param query
     * @return
     */
    public List<CanalVenta> completeQuery(String query) {
        CanalVenta canalVenta = new CanalVenta();
        canalVenta.setDescripcion(query);
        canalVentaAdapter = canalVentaAdapter.fillData(canalVenta);
        return canalVentaAdapter.getData();
    }

    /**
     * Selecciona el canal de ventas
     *
     * @param event
     */
    public void onItemSelectCanalVentaFilter(SelectEvent event) {
        clienteFilter.setIdCanalVenta(((CanalVenta) event.getObject()).getId());
    }

    /**
     * Selecciona la naturaleza del cliente
     *
     * @param event
     */
    public void onItemSelectNaturaleza(SelectEvent e) { 
        Integer idNaturalezaCliente = (Integer) e.getObject();
        clienteFilter.setIdNaturalezaCliente(idNaturalezaCliente);
        if(idNaturalezaCliente != 3) {
            clienteFilter.setCodigoPais("PRY");
        } 
    } 
    
    public void filterPais() {
        Pais pais = new Pais();
        this.paisAdapter = paisAdapter.fillData(pais); 
        this.codigoPais = clienteFilter.getCodigoPais();
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        this.canalVenta = new CanalVenta();
        this.canalVentaAdapter = new CanalVentaListAdapter();

        this.habilitarEdicionCliente = false;
        this.adapter = new ContratoAdapter();
        this.contratoFilter = new Contrato();

        this.adapterClienteProducto = new ClienteProductoAdapter();
        this.clienteProductoFilter = new ClienteProducto();

        this.adaptadorClienteTipoNegocio = new ClienteTipoNegocioAdapter();
        this.clienteTipoNegocioFilter = new ClienteTipoNegocio();

        this.adapterPersonaRol = new PersonaRolAsociadoAdapter();
        this.personaRolFilter = new PersonaRol();

        this.contratoCreate = new Contrato();

        this.conceptoFacturaFilter = new ConceptoFactura();
        this.adapterConceptoFactura = new ConceptoFacturaAdapter();

        this.adapterServicio = new ServicioProductoAdapter();
        this.servicioFilter = new ServicioProducto();

        this.productoContratoFilter = new Producto();
        this.adapterProductoContrato = new ProductoContratoAdapter();

        this.serviceContrato = new ContratoService();

        this.contratoAprobarFilter = new Contrato();
        this.adapterContratoAprobar = new ContratoAdapter();

        this.montoMinimoFilter = new MontoMinimo();
        this.adapterMontoMinimo = new MontoMinimoAdapter();

        this.adapterCliente = new ClientListAdapter();
        this.clienteFilter = new Cliente();

        this.serviceClient = new ClientServiceClient();
        this.facturaFilter = new FacturaPojo();

        this.titulo = null;
        this.descripcion = null;
        this.paisAdapter = new PaisAdapter(0,1000);
        
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idCliente = params.get("idCliente");

        filterContrato();
        filterContratoAprobar();
        filterCliente();
        filterPais();

    }
}
