/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.inventario.remote.TipoOperacionService;

/**
 * Adapter para tipo operacion
 *
 * @author Sergio D. Riveros Vazquez
 */
public class TipoOperacionAdapter extends DataListAdapter<TipoOperacion> {

    /**
     * Cliente remoto para tipo tipoOperacion
     */
    private final TipoOperacionService tipoOperacionClient;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoOperacionAdapter fillData(TipoOperacion searchData) {

        return tipoOperacionClient.getTipoOperacion(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoOperacionAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoOperacionClient = new TipoOperacionService();
    }

    /**
     * Constructor
     */
    public TipoOperacionAdapter() {
        super();
        this.tipoOperacionClient = new TipoOperacionService();
    }
}
