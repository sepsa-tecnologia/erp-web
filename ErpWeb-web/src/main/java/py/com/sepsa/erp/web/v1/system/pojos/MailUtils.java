/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.system.pojos;

import java.util.*;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Clase utilizada para el manejo de envíos de mails
 *
 * @author Daniel Federico Escauriza Arza
 */
public class MailUtils {

    /**
     * Sesión utilizada para el envío de mail
     */
//    private static final Session SESSION;

    /**
     * Inicializa los valores para el envío de mail
     */
//    static {
//        Properties props = new Properties();
//        props.setProperty("mail.smtp.host", "smtp.gmail.com");
//        props.setProperty("mail.smtp.port", String.valueOf(587));
//        props.setProperty("mail.smtp.user", "rnunez@sepsa.com.py");
//        props.setProperty("mail.smtp.auth", "true");
//        props.setProperty("mail.smtp.starttls.enable", "true");
//        SESSION = Session.getDefaultInstance(props, null);
//    }

       public static String getCustomMailHtml(String fileName, String cliente, String empresa, String emailEmpresa, String telefonoEmpresa) {
        if (empresa == null || empresa.isEmpty()){
            empresa = "SEPSA";
        }
        if (emailEmpresa == null || emailEmpresa.isEmpty()){
            emailEmpresa = "--";
        }
        if (telefonoEmpresa == null || telefonoEmpresa.isEmpty()){
            telefonoEmpresa = "--";
        }
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "    <head>\n"
                + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "        <title>A Simple Responsive HTML Email</title>\n"
                + "        <style type=\"text/css\">\n"
                + "            body {margin-top: 5px; padding: 0; min-width: 100%!important; font-family: -apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,\"Noto Sans\",sans-serif,\"Apple Color Emoji\",\"Segoe UI Emoji\",\"Segoe UI Symbol\",\"Noto Color Emoji\";}\n"
                + "            .content {width: 100%; max-width: 600px;}  \n"
                + "            @media only screen and (min-device-width: 601px) {\n"
                + "                .content {width: 600px !important;}\n"
                + "            }\n"
                + "            .header {padding: 0px 0px;}\n"
                + "            .text-white {\n"
                + "                color: #FFFFFF;\n"
                + "                font-size: 1.75rem;\n"
                + "                font-weight: 500;\n"
                + "            }\n"
                + "            .text-center {\n"
                + "                text-align: center;\n"
                + "            }\n"
                + "            .text-left {\n"
                + "                text-align: left;\n"
                + "            }\n"
                + "            .h5, .h4, .h3, .h2, .h1 {\n"
                + "                margin-bottom: .5rem;\n"
                + "                line-height: 1.2;\n"
                + "            }\n"
                + "            .rounded {\n"
                + "                border-radius: .25rem !important;\n"
                + "            }\n"
                + "            .table {\n"
                + "                width: 100%;\n"
                + "                margin-bottom: 1rem;\n"
                + "                color: #212529;\n"
                + "                border-collapse: collapse;\n"
                + "            }\n"
                + "            *, ::after, ::before {\n"
                + "                box-sizing: border-box;\n"
                + "            }\n"
                + "            .table-striped tbody tr:nth-of-type(odd) {\n"
                + "                background-color: rgba(0, 0, 0, 0.05);\n"
                + "            }\n"
                + "            .table-dark.table-striped tbody tr:nth-of-type(odd) {\n"
                + "                background-color: rgba(255, 255, 255, 0.05);\n"
                + "            }\n"
                + "            .text-danger {\n"
                + "                color: #dc3545 ;\n"
                + "            }\n"
                + "            .table thead th {\n"
                + "                vertical-align: bottom;\n"
                + "                border-bottom: 2px solid #dee2e6;\n"
                + "            }\n"
                + "            .table td, .table th {\n"
                + "                border-top: 1px solid #dee2e6;\n"
                + "            }\n"
                + "            .table-sm td, .table-sm th {\n"
                + "                padding: .3rem;\n"
                + "            }\n"
                + "            .footer-row {\n"
                + "                border-top: 2px solid #dee2e6;\n"
                + "                text-align: center;\n"
                + "                padding: 10px 10px 10px 10px;\n"
                + "            }\n"
                + "            a {\n"
                + "                color: #007bff;\n"
                + "                text-decoration: none;\n"
                + "                background-color: transparent;\n"
                + "                \n"
                + "            }\n"
                + "            a:hover {\n"
                + "                text-decoration: underline;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body yahoo bgcolor=\"#f6f8f1\">\n"
                + "        <!--[if (gte mso 9)|(IE)]>\n"
                + "        <table width=\"100%\" bgcolor=\"#f6f8f1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "            <tr>\n"
                + "                <td>\n"
                + "                    <![endif]-->\n"
                + "                    <table class=\"content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n"
                + "                        <tr>\n"
                + "                            <td class=\"header rounded\" bgcolor=\"#fab400\">\n"
                + "                                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"90\" style=\"padding: 0 0 0 0;\">\n"
                + "                                            <h3 class=\"text-white\">Notificación de Documento | "+ empresa +"</h3>\n"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                </table>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                        <tr>\n"
                + "                            <td>\n"
                + "                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"30\" style=\"padding: 10px 10px 10px 10px;\">\n"
                + "                                            <h5 class=\"h5\" style=\"font-size:15px;\">Estimado(a) Cliente: " + cliente + "</h5>\n"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"30\" style=\"padding: 10px 10px 10px 10px;\">\n"
                + "                                            <h5 class=\"h5\" style=\"font-size:15px;\">Adjunto encontrará su Documento Tributario N° " + fileName + " en formato PDF.</h5>\n"
    
                + "                                             <h5 class=\"h5\" style=\"font-size:13px;\">Atentos saludos,</h5>"
                + "                                             <p style=\"font-size:13px; margin:2px; padding-left:50px;\">"+empresa+"</p>"
                + "                                             <p style=\"font-size:13px; margin:2px; padding-left:50px;\">"+telefonoEmpresa+"</p>"
                + "                                             <p style=\"font-size:13px; margin:2px; padding-left:50px;\">"+emailEmpresa+"</p>"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                </table>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                        <tr>\n"
                + "                            <td>\n"
                + "                                <hr/>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "\n"
                + "                        <tr>\n"
                + "                            <td class=\"footer-row\">\n"
                + "                                <small><a href=\"www.sepsa.com.py\">&copy; 2022 SEPSA</a></small>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                    </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "    </body>\n"
                + "</html>";
    }
    
    
    /**
     * Prepara el HTML para el email de alerta a enviar
     *
     * @param localNombre Nombre del local con alertas
     * @param tablaHtml Tabla en HTML del resumen de las alertas
     * @return
     */
    public static String getMailHtml(String fileName, String cliente, String empresa) {
        if (empresa == null || empresa.isEmpty()){
            empresa = "SEPSA";
        }
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "    <head>\n"
                + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "        <title>A Simple Responsive HTML Email</title>\n"
                + "        <style type=\"text/css\">\n"
                + "            body {margin-top: 5px; padding: 0; min-width: 100%!important; font-family: -apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,\"Noto Sans\",sans-serif,\"Apple Color Emoji\",\"Segoe UI Emoji\",\"Segoe UI Symbol\",\"Noto Color Emoji\";}\n"
                + "            .content {width: 100%; max-width: 600px;}  \n"
                + "            @media only screen and (min-device-width: 601px) {\n"
                + "                .content {width: 600px !important;}\n"
                + "            }\n"
                + "            .header {padding: 0px 0px;}\n"
                + "            .text-white {\n"
                + "                color: #FFFFFF;\n"
                + "                font-size: 1.75rem;\n"
                + "                font-weight: 500;\n"
                + "            }\n"
                + "            .text-center {\n"
                + "                text-align: center;\n"
                + "            }\n"
                + "            .text-left {\n"
                + "                text-align: left;\n"
                + "            }\n"
                + "            .h5, .h4, .h3, .h2, .h1 {\n"
                + "                margin-bottom: .5rem;\n"
                + "                line-height: 1.2;\n"
                + "            }\n"
                + "            .rounded {\n"
                + "                border-radius: .25rem !important;\n"
                + "            }\n"
                + "            .table {\n"
                + "                width: 100%;\n"
                + "                margin-bottom: 1rem;\n"
                + "                color: #212529;\n"
                + "                border-collapse: collapse;\n"
                + "            }\n"
                + "            *, ::after, ::before {\n"
                + "                box-sizing: border-box;\n"
                + "            }\n"
                + "            .table-striped tbody tr:nth-of-type(odd) {\n"
                + "                background-color: rgba(0, 0, 0, 0.05);\n"
                + "            }\n"
                + "            .table-dark.table-striped tbody tr:nth-of-type(odd) {\n"
                + "                background-color: rgba(255, 255, 255, 0.05);\n"
                + "            }\n"
                + "            .text-danger {\n"
                + "                color: #dc3545 ;\n"
                + "            }\n"
                + "            .table thead th {\n"
                + "                vertical-align: bottom;\n"
                + "                border-bottom: 2px solid #dee2e6;\n"
                + "            }\n"
                + "            .table td, .table th {\n"
                + "                border-top: 1px solid #dee2e6;\n"
                + "            }\n"
                + "            .table-sm td, .table-sm th {\n"
                + "                padding: .3rem;\n"
                + "            }\n"
                + "            .footer-row {\n"
                + "                border-top: 2px solid #dee2e6;\n"
                + "                text-align: center;\n"
                + "                padding: 10px 10px 10px 10px;\n"
                + "            }\n"
                + "            a {\n"
                + "                color: #007bff;\n"
                + "                text-decoration: none;\n"
                + "                background-color: transparent;\n"
                + "                \n"
                + "            }\n"
                + "            a:hover {\n"
                + "                text-decoration: underline;\n"
                + "            }\n"
                + "        </style>\n"
                + "    </head>\n"
                + "    <body yahoo bgcolor=\"#f6f8f1\">\n"
                + "        <!--[if (gte mso 9)|(IE)]>\n"
                + "        <table width=\"100%\" bgcolor=\"#f6f8f1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "            <tr>\n"
                + "                <td>\n"
                + "                    <![endif]-->\n"
                + "                    <table class=\"content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n"
                + "                        <tr>\n"
                + "                            <td class=\"header rounded\" bgcolor=\"#fab400\">\n"
                + "                                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"90\" style=\"padding: 0 0 0 0;\">\n"
                + "                                            <h3 class=\"text-white\">Notificación de Documento | "+ empresa +"</h3>\n"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                </table>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                        <tr>\n"
                + "                            <td>\n"
                + "                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"30\" style=\"padding: 10px 10px 10px 10px;\">\n"
                + "                                            <h5 class=\"h5\" style=\"font-size:15px;\">Estimado(a) Cliente: " + cliente + "</h5>\n"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                    <tr>\n"
                + "                                        <td height=\"30\" style=\"padding: 10px 10px 10px 10px;\">\n"
                + "                                            <h5 class=\"h5\" style=\"font-size:15px;\">Adjunto encontrará su Documento Tributario N° " + fileName + " en formato PDF.</h5>\n"
                + "                                             <h5 class=\"h5\" style=\"font-size:15px;\">Atentos saludos</h5>"
                + "                                        </td>\n"
                + "                                    </tr>\n"
                + "                                </table>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                        <tr>\n"
                + "                            <td>\n"
                + "                                <hr/>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "\n"
                + "                        <tr>\n"
                + "                            <td class=\"footer-row\">\n"
                + "                                <small><a href=\"www.sepsa.com.py\">&copy; 2022 SEPSA</a></small>\n"
                + "                            </td>\n"
                + "                        </tr>\n"
                + "                    </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "    </body>\n"
                + "</html>";
    }

    /**
     * Envía mensajes vía mail
     *
     * @param msn Mensaje del mail
     * @param subject Asunto del mail
     * @param to Destinatario del mensaje
     * @param file Bytes del archivo a adjuntar
     * @param fileName Nombre del archivo a adjuntar
     * @param from
     * @param passFrom
     * @param host
     * @param port
     * @return bool
     */
    public static Boolean sendMail(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom,String host, String port) {
        try {
            Properties props = new Properties();
            if(host.equalsIgnoreCase("N")){
                 props.setProperty("mail.smtp.host", "smtp.gmail.com");
                
            }
            else {
                props.setProperty("mail.smtp.host", host);
            }
            if(port.equalsIgnoreCase("N")){
                props.setProperty("mail.smtp.port", String.valueOf(587));
            }
            else {
                props.setProperty("mail.smtp.port", port);
            }
            
            props.setProperty("mail.smtp.user", from);
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(props, null);
            
            javax.mail.Message mMessage = new MimeMessage(session);
            mMessage.setSubject(subject);

            mMessage.setFrom(new InternetAddress(from));

            //Texto del correo
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setContent(msn, "text/html");

            //Archivo adjunto del correo
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();
            ByteArrayDataSource bds = new ByteArrayDataSource(file, "application/pdf");
            messageBodyPart.setDataHandler(new DataHandler(bds));
            messageBodyPart.setFileName(fileName);

            multipart.addBodyPart(textBodyPart);
            multipart.addBodyPart(messageBodyPart);

            mMessage.setContent(multipart);
            StringTokenizer token = new StringTokenizer(to, ";");

            while (token.hasMoreTokens()) {
                mMessage.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(token.nextToken()));
            }

            Transport t = session.getTransport("smtp");
            t.connect(from, passFrom);
            t.sendMessage(mMessage, mMessage.getAllRecipients());
            t.close();
            return true;
        } catch (MessagingException ex) {
            WebLogger.get().fatal(ex);
            return false;
        }
    }
}
