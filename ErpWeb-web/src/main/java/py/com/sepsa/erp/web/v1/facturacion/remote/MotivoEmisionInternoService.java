/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionInternoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.MotivoEmisionInternoFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmisionInterno;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio Motivo Emision Interno
 *
 * @author Sergio D. Riveros Vazquez
 */
public class MotivoEmisionInternoService extends APIErpFacturacion {

    /**
     *
     * Obtiene la lista
     *
     *
     *
     * @param motivoEmisionInterno Objeto
     *
     * @param page
     *
     * @param pageSize
     *
     * @return
     *
     */
    public MotivoEmisionInternoAdapter getMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno, Integer page,
            Integer pageSize) {

        MotivoEmisionInternoAdapter lista = new MotivoEmisionInternoAdapter();

        Map params = MotivoEmisionInternoFilter.build(motivoEmisionInterno, page, pageSize);

        HttpURLConnection conn = GET(Resource.MOTIVO_EMISION_INTERNO.url,
                ContentType.JSON, params);

        if (conn != null) {

            BodyResponse response = BodyResponse.createInstance(conn,
                    MotivoEmisionInternoAdapter.class);

            lista = (MotivoEmisionInternoAdapter) response.getPayload();

            conn.disconnect();

        }

        return lista;

    }

    /**
     * Método para crear
     *
     * @param motivoEmisionInterno
     * @return
     */
    public BodyResponse<MotivoEmisionInterno> setMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MOTIVO_EMISION_INTERNO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoEmisionInterno));
            response = BodyResponse.createInstance(conn, MotivoEmisionInterno.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param motivoEmisionInterno
     * @return
     */
    public BodyResponse<MotivoEmisionInterno> editMotivoEmisionInterno(MotivoEmisionInterno motivoEmisionInterno) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.MOTIVO_EMISION_INTERNO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(motivoEmisionInterno));
            response = BodyResponse.createInstance(conn, MotivoEmisionInterno.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public MotivoEmisionInterno get(Integer id) {
        MotivoEmisionInterno data = new MotivoEmisionInterno(id);
        MotivoEmisionInternoAdapter list = getMotivoEmisionInterno(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     *
     * Constructor de clase
     *
     */
    public MotivoEmisionInternoService() {

        super();

    }

    /**
     *
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     *
     */
    public enum Resource {

        //Servicios
        MOTIVO_EMISION_INTERNO("Motivo Emision Interno", "motivo-emision-interno");

        /**
         *
         * Nombre del recurso
         *
         */
        private String resource;

        /**
         *
         * URL del recurso
         *
         */
        private String url;

        /**
         *
         * Obtiene el nombre del recurso
         *
         *
         *
         * @return Nombre del recurso
         *
         */
        public String getResource() {

            return resource;

        }

        /**
         *
         * Setea el nombre del recurso
         *
         *
         *
         * @param resource Nombre del recurso
         *
         */
        public void setResource(String resource) {

            this.resource = resource;

        }

        /**
         *
         * Obtiene la URL del servicio
         *
         *
         *
         * @return URL del servicio
         *
         */
        public String getUrl() {

            return url;

        }

        /**
         *
         * Setea la URL del servicio
         *
         *
         *
         * @param url URL del servicio
         *
         */
        public void setUrl(String url) {

            this.url = url;

        }

        /**
         *
         * Constructor de Resource
         *
         *
         *
         * @param resource Nombre del recurso
         *
         * @param url URL del servicio o recurso
         *
         */
        Resource(String resource, String url) {

            this.resource = resource;

            this.url = url;

        }

    }

}
