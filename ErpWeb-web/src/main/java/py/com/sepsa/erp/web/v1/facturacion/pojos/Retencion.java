package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;

/**
 * Pojo para listado de retención
 *
 * @author alext
 */
public class Retencion {

    /**
     * Identificador de retención
     */
    private Integer id;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Identificador de la persona
     */
    private Integer idPersona;
    /**
     * Cliente
     */
    private Cliente cliente;
    /**
     * Nro de retención
     */
    private String nroRetencion;
    /**
     * Parámetro fechaInsercion
     */
    private Date fechaInsercion;
    /**
     * Parámetro fecha
     */
    private Date fecha;
    /**
     * Parámetro fechaDesde
     */
    private Date fechaDesde;
    /**
     * Parámetro fechaHasta
     */
    private Date fechaHasta;
    /**
     * Parámetro montoImponibleTotal
     */
    private BigDecimal montoImponibleTotal;
    /**
     * Parámetro montoIvaTotal
     */
    private BigDecimal montoIvaTotal;
    /**
     * Parámetro montoTotal
     */
    private BigDecimal montoTotal;
    /**
     * Parámetro porcentajeRetencion
     */
    private Integer porcentajeRetencion;
    /**
     * Parámetro montoRetenidoTotal
     */
    private BigDecimal montoRetenidoTotal;
    /**
     * Estado de la retención
     */
    private Estado estado;
    /**
     * Detalle
     */
    private List<RetencionDetalle> retencionDetalles;
    /**
     * Detalle
     */
    private List<RetencionDetalle> retencionCompraDetalles;
    /**
     * Identificador del estado
     */
    private Integer idEstado;
    /**
     * Código de estado
     */
    private String codigoEstado;
    /**
     * Identificador de
     */
    private Integer idMotivoAnulacion;
    /**
     * Motivo anulación
     */
    private String observacionAnulacion;
    /**
     * Id factura
     */
    private Integer idFactura;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    /**
     * @return the retencionCompraDetalles
     */
    public List<RetencionDetalle> getRetencionCompraDetalles() {
        return retencionCompraDetalles;
    }

    /**
     * @param retencionCompraDetalles the retencionCompraDetalles to set
     */
    public void setRetencionCompraDetalles(List<RetencionDetalle> retencionCompraDetalles) {
        this.retencionCompraDetalles = retencionCompraDetalles;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the idMotivoAnulacion
     */
    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    /**
     * @param idMotivoAnulacion the idMotivoAnulacion to set
     */
    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    /**
     * @return the observacionAnulacion
     */
    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    /**
     * @param observacionAnulacion the observacionAnulacion to set
     */
    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<RetencionDetalle> getRetencionDetalles() {
        return retencionDetalles;
    }

    public void setRetencionDetalles(List<RetencionDetalle> retencionDetalles) {
        this.retencionDetalles = retencionDetalles;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    //</editor-fold>
    public Retencion() {
    }

}
