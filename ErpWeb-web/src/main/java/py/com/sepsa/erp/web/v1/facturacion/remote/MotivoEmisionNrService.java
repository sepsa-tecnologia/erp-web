/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionNrAdapter;
import py.com.sepsa.erp.web.v1.facturacion.filters.MotivoEmisionFilter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.remote.APIErpFacturacion;

/**
 * Cliente para el servicio de Motivo Emision Nota Remision
 *
 * @author Williams Vera
 */
public class MotivoEmisionNrService extends APIErpFacturacion {

    /**
     * Obtiene la lista
     *
     * @param motivoEmision Objeto
     * @param page
     * @param pageSize
     * @return
     */
    public MotivoEmisionNrAdapter getMotivoEmisionNr(MotivoEmision motivoEmision, Integer page,
            Integer pageSize) {

        MotivoEmisionNrAdapter lista = new MotivoEmisionNrAdapter();

        Map params = MotivoEmisionFilter.build(motivoEmision, page, pageSize);

        HttpURLConnection conn = GET(Resource.MOTIVO_EMISION_NR.url,
                ContentType.JSON, params);

        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MotivoEmisionNrAdapter.class);

            lista = (MotivoEmisionNrAdapter) response.getPayload();

            conn.disconnect();
        }
        return lista;
    }

    /**
     * Método para crear
     *
     * @param motivoEmision
     * @return
     */
    public BodyResponse<MotivoEmision> setMotivoEmision(MotivoEmision motivoEmision) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.MOTIVO_EMISION_NR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(motivoEmision));
            response = BodyResponse.createInstance(conn, MotivoEmision.class);
        }
        return response;
    }

    /**
     * Método para editar
     *
     * @param motivoEmision
     * @return
     */
    public BodyResponse<MotivoEmision> editMotivoEmision(MotivoEmision motivoEmision) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.MOTIVO_EMISION_NR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(motivoEmision));
            response = BodyResponse.createInstance(conn, MotivoEmision.class);
        }
        return response;
    }

    /**
     *
     * @param id
     * @return
     */
    public MotivoEmision get(Integer id) {
        MotivoEmision data = new MotivoEmision(id);
        MotivoEmisionNrAdapter list = getMotivoEmisionNr(data, 0, 1);
        data = list != null
                && list.getData() != null
                && !list.getData().isEmpty()
                ? list.getData().get(0)
                : null;

        return data;
    }

    /**
     * Constructor de clase
     */
    public MotivoEmisionNrService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del ApiSepsaFacturacion
     */
    public enum Resource {

        //Servicios
        MOTIVO_EMISION_NR("Motivo Emision Nota Remision", "motivo-emision-nr");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
