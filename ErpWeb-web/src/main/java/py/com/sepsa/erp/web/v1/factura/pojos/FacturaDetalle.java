package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;

/**
 * POJO para Factura
 *
 * @author Romina Núñez
 */
public class FacturaDetalle {

    /**
     * Identificador de Factura Detalle
     */
    private Integer id;
    /**
     * Identificador de fcatura
     */
    private Integer idFactura;
    /**
     * Identificador de factura compra
     */
    private Integer idFacturaCompra;
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    /**
     * Número de línea
     */
    private Integer nroLinea;
    /**
     * Descripción
     */
    private String nroCtaContable;
    /**
     * Descripción
     */
    private String descripcion;
    /**
     * Codigo Interno Producto
     */
    private String codigoInternoProducto;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * Monto IVA
     */
    private BigDecimal montoIva;
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    /**
     * Precio Unitario
     */
    private BigDecimal precioUnitarioConIva;
        /**
     * Precio Unitario
     */
    private BigDecimal precioUnitarioConIvaOc;
    /**
     * Precio Unitario
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoParticularUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoParticularUnitarioAux;
    /**
     * Precio Unitario
     */
    private BigDecimal descuentoGlobalUnitario;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoParticular;
    /**
     * Precio Unitario
     */
    private BigDecimal montoDescuentoGlobal;
    /**
     * Precio Unitario
     */
    private BigDecimal montoExentoGravado;
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    /**
     * Identificador del producto
     */
    private Producto producto;
    /**
     * Porcentaje IVA
     */
    private BigDecimal porcentajeGravada;
    /**
     * Cantidad
     */
    private BigDecimal cantidadOrdenCompra;
    /**
     * Cantidad
     */
    private BigDecimal cantidadFacturada;
    /**
     * Cantidad
     */
    private BigDecimal cantidadRecibida;
    /**
     * Porcentaje IVA
     */
    private Integer idEstadoInventario;
        /**
     * Porcentaje IVA
     */
    private String codigoEstadoInventario;
    /**
     * Porcentaje IVA
     */
    private Integer idDepositoLogistico;
    /**
     *
     */
    private Date fechaVencimiento;
    
    private String codDncpNivelGeneral;
    
    private String codDncpNivelEspecifico;
 
    private String lote;
    
    private String fechaVencimientoLote;
    
    private BigDecimal totalSinDescuento;
    
    private Integer idTipoProducto;

    /**
     * @return the producto
     */
    public BigDecimal getDescuentoParticularUnitarioAux() {
        return descuentoParticularUnitarioAux;
    }
    
    public void setDescuentoParticularUnitarioAux(BigDecimal descuentoParticularUnitarioAux) {
        this.descuentoParticularUnitarioAux = descuentoParticularUnitarioAux;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setPrecioUnitarioConIvaOc(BigDecimal precioUnitarioConIvaOc) {
        this.precioUnitarioConIvaOc = precioUnitarioConIvaOc;
    }

    public BigDecimal getPrecioUnitarioConIvaOc() {
        return precioUnitarioConIvaOc;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public BigDecimal getCantidadOrdenCompra() {
        return cantidadOrdenCompra;
    }

    public void setCantidadOrdenCompra(BigDecimal cantidadOrdenCompra) {
        this.cantidadOrdenCompra = cantidadOrdenCompra;
    }

    public BigDecimal getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(BigDecimal cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public BigDecimal getCantidadRecibida() {
        return cantidadRecibida;
    }

    public void setCantidadRecibida(BigDecimal cantidadRecibida) {
        this.cantidadRecibida = cantidadRecibida;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public void setNroCtaContable(String nroCtaContable) {
        this.nroCtaContable = nroCtaContable;
    }

    public String getNroCtaContable() {
        return nroCtaContable;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public BigDecimal getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(BigDecimal porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }
  
    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public BigDecimal getTotalSinDescuento() {
        return totalSinDescuento;
    }

    public void setTotalSinDescuento(BigDecimal totalSinDescuento) {
        this.totalSinDescuento = totalSinDescuento;
    }


    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(String fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getCodigoInternoProducto() {
        return codigoInternoProducto;
    }

    public void setCodigoInternoProducto(String codigoInternoProducto) {
        this.codigoInternoProducto = codigoInternoProducto;
    }

//</editor-fold>

    /**
     * Constructor de la clase
     */
    public FacturaDetalle() {
    }

}
