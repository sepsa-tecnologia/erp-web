
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO de Email
 * @author Cristina Insfrán
 */
public class TipoEmail {

    /**
     * Identificador del email
     */
    private Integer id;
    /**
     * Descripción del email
     */
    private String descripcion;
    /**
     * Código 
     */
    private String codigo;
    
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    
    /**
     * Constructor de la clase
     */
    public TipoEmail(){
        
    }
    
    /**
     * Constructor de la clase
     * @param id
     * @param descripcion
     */
    public TipoEmail(Integer id,String descripcion){
        this.id = id;
        this.descripcion = descripcion;
    }
}
