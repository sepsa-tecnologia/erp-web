/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoProducto;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;

/**
 * Adapter para tipo producto
 *
 * @author Williams Vera
 */
public class TipoProductoAdapter extends DataListAdapter<TipoProducto> {

    /**
     * Cliente para el servicio tipo producto
     */
    private final ProductoService productoService;

    /**
     * Método para cargar la lista de datos
     *
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoProductoAdapter fillData(TipoProducto searchData) {

        return productoService.getTipoProductoList(searchData, getFirstResult(), getPageSize());
    }

    /**
     * Constructor de TipoProductoAdapter
     *
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoService();
    }

    /**
     * Constructor de TipoEmpresaAdapter
     */
    public TipoProductoAdapter() {
        super();
        this.productoService = new ProductoService();
    }
}
