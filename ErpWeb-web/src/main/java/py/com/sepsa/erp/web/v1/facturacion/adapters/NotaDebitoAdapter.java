/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaDebito;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaDebitoService;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoAdapter extends DataListAdapter<NotaDebito> {
    

    private final NotaDebitoService serviceNotaDebito;
    
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public NotaDebitoAdapter fillData(NotaDebito searchData) {
     
        return serviceNotaDebito.getNotaDebitoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de NotaDebitoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public NotaDebitoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceNotaDebito = new NotaDebitoService();
    }

    /**
     * Constructor de NotaCreditoAdapter
     */
    public NotaDebitoAdapter() {
        super();
        this.serviceNotaDebito = new NotaDebitoService();
    }
}
