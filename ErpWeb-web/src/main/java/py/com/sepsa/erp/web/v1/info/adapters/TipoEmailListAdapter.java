
package py.com.sepsa.erp.web.v1.info.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.TipoEmail;
import py.com.sepsa.erp.web.v1.info.remote.TipoEmailServiceClient;

/**
 * Adaptador de la lista Tipo Email
 * @author Cristina Insfrán
 */
public class TipoEmailListAdapter extends DataListAdapter<TipoEmail> {
     /**
     * Cliente para el servicio de tipo email
     */
    private final TipoEmailServiceClient tipoEmailClient;
 
    /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public TipoEmailListAdapter fillData(TipoEmail searchData) {
     
        return tipoEmailClient.getTipoEmailList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de TipoEmailListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public TipoEmailListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.tipoEmailClient = new TipoEmailServiceClient();
    }

    /**
     * Constructor de TipoEmailListAdapter
     */
    public TipoEmailListAdapter() {
        super();
        this.tipoEmailClient = new TipoEmailServiceClient();
    }
}
