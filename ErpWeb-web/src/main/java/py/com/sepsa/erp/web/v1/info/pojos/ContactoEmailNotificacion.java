package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * POJO para contacto email
 *
 * @author Romina Núñez
 */
public class ContactoEmailNotificacion {

    /**
     * Identificador del contacto
     */
    private Integer idContacto;
    /**
     * Nombre del contacto
     */
    private String contacto;
    /**
     * Identificador del Email
     */
    private Integer idEmail;
    /**
     * Email del contacto
     */
    private String email;
    /**
     * Identificador de notificación
     */
    private Integer idTipoNotificacion;
    /**
     * Tipo de Notificación
     */
    private String tipoNotificacion;
    /**
     * Estado
     */
    private String estado;

    /**
     * @return the idEmail
     */
    public Integer getIdEmail() {
        return idEmail;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getEmail() {
        return email;
    }

    public String getContacto() {
        return contacto;
    }

    /**
     * @param idEmail the idEmail to set
     */
    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    /**
     * @return the idContacto
     */
    public Integer getIdContacto() {
        return idContacto;
    }

    /**
     * @param idContacto the idContacto to set
     */
    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Constructor
     */
    public ContactoEmailNotificacion() {

    }
    
    /**
     * Constructor con parámetros
     * @param idContacto
     * @param contacto
     * @param idEmail
     * @param email
     * @param idTipoNotificacion
     * @param tipoNotificacion
     * @param estado 
     */
    public ContactoEmailNotificacion(Integer idContacto, String contacto, Integer idEmail, String email, Integer idTipoNotificacion, String tipoNotificacion, String estado) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idEmail = idEmail;
        this.email = email;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.estado = estado;
    }

    public ContactoEmailNotificacion(Integer idTipoNotificacion, String tipoNotificacion, String estado) {
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.estado = estado;
    }
    
    
    
    
    
}
