package py.com.sepsa.erp.web.v1.factura.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroPojo;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filtro utilizado para el servicio de Factura
 *
 * @author Williams Vera
 */
public class CobroPojoFilter extends Filter {

    /**
     * Agrega el filtro de identificador de cobro
     *
     * @param id
     * @return
     */
    public CobroPojoFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public CobroPojoFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de cliente
     *
     * @param idCliente
     * @return
     */
    public CobroPojoFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de moneda
     *
     * @param idMoneda
     * @return
     */
    public CobroPojoFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de moneda
     *
     * @param moneda
     * @return
     */
    public CobroPojoFilter moneda(String moneda) {
        if (moneda != null && !moneda.trim().isEmpty()) {
            params.put("moneda", moneda);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha
     *
     * @param fecha
     * @return
     */
    public CobroPojoFilter fecha(Date fecha) {
        if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {

                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha vencimiento
     *
     * @param fechaVencimiento
     * @return
     */
    public CobroPojoFilter fechaVencimiento(Date fechaVencimiento) {
        if (fechaVencimiento != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = simpleDateFormat.format(fechaVencimiento);
                params.put("fechaVencimiento", URLEncoder.encode(date, "UTF-8"));

            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de razón social
     *
     * @param razonSocial
     * @return
     */
    public CobroPojoFilter razonSocial(String razonSocial) {
        if (razonSocial != null && !razonSocial.trim().isEmpty()) {
            params.put("razonSocial", razonSocial);
        }
        return this;
    }

    /**
     * Agrega el filtro de monto cobro
     *
     * @param montoCobro
     * @return
     */
    public CobroPojoFilter montoCobro(BigDecimal montoCobro) {
        if (montoCobro != null) {
            params.put("montoCobro", montoCobro);
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaDesde
     * @return
     */
    public CobroPojoFilter fechaDesde(Date fechaDesde) {
        if (fechaDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaDesde);

                params.put("fechaDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha hasta
     *
     * @param fechaHasta
     * @return
     */
    public CobroPojoFilter fechaHasta(Date fechaHasta) {
        if (fechaHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaHasta);
                params.put("fechaHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de digital
     *
     * @param digital
     * @return
     */
    public CobroPojoFilter digital(String digital) {
        if (digital != null && !digital.trim().isEmpty()) {
            params.put("digital", digital);
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador de lugar de cobro
     *
     * @param idLugarCobro
     * @return
     */
    public CobroPojoFilter idLugarCobro(Integer idLugarCobro) {
        if (idLugarCobro != null) {
            params.put("idLugarCobro", idLugarCobro);
        }
        return this;
    }

    /**
     * Agregar el filtro de nro recibo
     *
     * @param nroRecibo
     * @return
     */
    public CobroPojoFilter nroRecibo(String nroRecibo) {
        if (nroRecibo != null && !nroRecibo.trim().isEmpty()) {
            params.put("nroRecibo", nroRecibo);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de identificador de talonario
     *
     * @param idTalonario
     * @return
     */
    public CobroPojoFilter idTalonario(Integer idTalonario) {
        if (idTalonario != null) {
            params.put("idTalonario", idTalonario);
        }
        return this;
    }

    /**
     * Agrega el filtro de tiene retencion, true o false
     *
     * @param tieneRetencion
     * @return
     */
    public CobroPojoFilter tieneRetencion(String tieneRetencion) {
        if (tieneRetencion != null && !tieneRetencion.trim().isEmpty()) {
            params.put("tieneRetencion", tieneRetencion.trim());
        }
        return this;
    }

    /**
     * Agregar el filtro de anhoMes
     *
     * @param anhoMes
     * @return
     */
    public CobroPojoFilter anhoMes(String anhoMes) {
        if (anhoMes != null && !anhoMes.trim().isEmpty()) {
            params.put("anhoMes", anhoMes);
        }
        return this;
    }

    /**
     * Agregar el filtro de estado
     *
     * @param idEstado
     * @return
     */
    public CobroPojoFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param cobro datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(CobroPojo cobro, Integer page, Integer pageSize) {
        CobroPojoFilter filter = new CobroPojoFilter();

        filter
                .id(cobro.getId())
                .listadoPojo(cobro.getListadoPojo())
                .idCliente(cobro.getIdCliente())
                .idLugarCobro(cobro.getIdLugarCobro())
                .digital(cobro.getDigital())
                .fechaDesde(cobro.getFechaDesde())
                .fechaHasta(cobro.getFechaHasta())
                .razonSocial(cobro.getRazonSocial())
                .montoCobro(cobro.getMontoCobro())
                .nroRecibo(cobro.getNroRecibo())
                .idTalonario(cobro.getIdTalonario())
                .idEstado(cobro.getIdEstado())              
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de DocumentEdiFilter
     */
    public CobroPojoFilter() {
        super();
    }
}
