/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.inventario.pojos.DevolucionDetalle;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Filter para operacion de inventario
 *
 * @author Romina Núñez
 */
public class DevolucionDetalleFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public DevolucionDetalleFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por idUsuario
     *
     * @param idUsuario
     * @return
     */
    public DevolucionDetalleFilter idDevolucion(Integer idDevolucion) {
        if (idDevolucion != null) {
            params.put("idDevolucion", idDevolucion);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador de estado
     *
     * @param idEstado
     * @return
     */
    public DevolucionDetalleFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro Estado
     *
     * @param estado
     * @return
     */
    public DevolucionDetalleFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado);
        }
        return this;
    }

    /**
     * Agrega el filtro codigoEstado
     *
     * @param codigoEstado
     * @return
     */
    public DevolucionDetalleFilter codigoEstado(String codigoEstado) {
        if (codigoEstado != null && !codigoEstado.trim().isEmpty()) {
            params.put("codigoEstado", codigoEstado);
        }
        return this;
    }

    /**
     * Agrega el filtro listadoPojo
     *
     * @param listadoPojo
     * @return
     */
    public DevolucionDetalleFilter listadoPojo(String listadoPojo) {
        if (listadoPojo != null && !listadoPojo.trim().isEmpty()) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }
    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionDesde
     * @return
     */
    public DevolucionDetalleFilter fechaInsercionDesde(Date fechaInsercionDesde) {
        if (fechaInsercionDesde != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionDesde);

                params.put("fechaInsercionDesde", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Agregar el filtro de fecha desde
     *
     * @param fechaInsercionHasta
     * @return
     */
    public DevolucionDetalleFilter fechaInsercionHasta(Date fechaInsercionHasta) {
        if (fechaInsercionHasta != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fechaInsercionHasta);

                params.put("fechaInsercionHasta", URLEncoder.encode(date, "UTF-8"));
            } catch (Exception e) {
                WebLogger.get().error("Exception: " + e);
            }
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param devolucionDetalle datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(DevolucionDetalle devolucionDetalle, Integer page, Integer pageSize) {
        DevolucionDetalleFilter filter = new DevolucionDetalleFilter();

        filter
                .id(devolucionDetalle.getId())
                .idDevolucion(devolucionDetalle.getIdDevolucion())
                .listadoPojo(devolucionDetalle.getListadoPojo())
                .page(page)
                .pageSize(pageSize);
        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public DevolucionDetalleFilter() {
        super();
    }

}
