/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventarioDetalle;
import py.com.sepsa.erp.web.v1.inventario.remote.ControlInventarioService;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("editarControlInventario")
public class ControlInventarioEditController implements Serializable {

    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioAdapter adapterControlInventario;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioFilter;
    /**
     * Adaptador para la lista de control de inventario
     */
    private ControlInventarioDetalleAdapter adapterControlInventarioDetalle;
    /**
     * Pojo Control Inventario
     */
    private ControlInventarioDetalle controlInventarioDetalleFilter;
    /**
     * Dato bandera
     */
    private Boolean show;
    /**
     * Dato bandera
     */
    private Boolean showFilter;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioEdit;
    /**
     * Lista Detalle
     */
    private List<ControlInventarioDetalle> listaDetalle;
    /**
     * Service control inventario
     */
    private ControlInventarioService service;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public void setControlInventarioDetalleFilter(ControlInventarioDetalle controlInventarioDetalleFilter) {
        this.controlInventarioDetalleFilter = controlInventarioDetalleFilter;
    }
    
    public void setService(ControlInventarioService service) {
        this.service = service;
    }
    
    public ControlInventarioService getService() {
        return service;
    }
    
    public void setListaDetalle(List<ControlInventarioDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }
    
    public List<ControlInventarioDetalle> getListaDetalle() {
        return listaDetalle;
    }
    
    public void setControlInventarioEdit(ControlInventario controlInventarioEdit) {
        this.controlInventarioEdit = controlInventarioEdit;
    }
    
    public ControlInventario getControlInventarioEdit() {
        return controlInventarioEdit;
    }
    
    public void setShowFilter(Boolean showFilter) {
        this.showFilter = showFilter;
    }
    
    public Boolean getShowFilter() {
        return showFilter;
    }
    
    public void setShow(Boolean show) {
        this.show = show;
    }
    
    public Boolean getShow() {
        return show;
    }
    
    public void setAdapterControlInventarioDetalle(ControlInventarioDetalleAdapter adapterControlInventarioDetalle) {
        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle;
    }
    
    public ControlInventarioDetalle getControlInventarioDetalleFilter() {
        return controlInventarioDetalleFilter;
    }
    
    public ControlInventarioDetalleAdapter getAdapterControlInventarioDetalle() {
        return adapterControlInventarioDetalle;
    }
    
    public void setControlInventarioFilter(ControlInventario controlInventarioFilter) {
        this.controlInventarioFilter = controlInventarioFilter;
    }
    
    public void setAdapterControlInventario(ControlInventarioAdapter adapterControlInventario) {
        this.adapterControlInventario = adapterControlInventario;
    }
    
    public ControlInventario getControlInventarioFilter() {
        return controlInventarioFilter;
    }
    
    public ControlInventarioAdapter getAdapterControlInventario() {
        return adapterControlInventario;
    }

    //</editor-fold>
    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.controlInventarioFilter = new ControlInventario();
        filterControlInventario();
    }
    
    public void filterControlInventario() {
        getAdapterControlInventario().setFirstResult(0);
        this.controlInventarioFilter.setListadoPojo(true);
        if (this.controlInventarioFilter.getCodigo() != null && !this.controlInventarioFilter.getCodigo().trim().isEmpty()) {
            this.adapterControlInventario = adapterControlInventario.fillData(controlInventarioFilter);
            if (!this.adapterControlInventario.getData().isEmpty()) {
                this.show = true;
                this.showFilter = false;
                this.controlInventarioEdit = this.adapterControlInventario.getData().get(0);
                filterControlInventarioDetalle(controlInventarioEdit.getId());
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Advertencia", "No hay registros relacionados al N° ingresado!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Advertencia", "Debe ingresar el N° de Control!"));
        }
        
    }

    /**
     * Método para crear
     */
    public void edit() {
        controlInventarioEdit.setControlInventarioDetalles(listaDetalle);
        
        BodyResponse<ControlInventario> respuestaDep = service.editDeposito(controlInventarioEdit);
        
        if (respuestaDep.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Control de Inventario completado correctamente!"));
            
            init();
        }
        
    }

    /**
     * Método para filtrar los detalles de control de inventario
     *
     * @param idControl
     */
    public void filterControlInventarioDetalle(Integer idControl) {
        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();
        
        this.controlInventarioDetalleFilter.setListadoPojo(Boolean.TRUE);
        this.controlInventarioDetalleFilter.setIdControlInventario(idControl);
        
        this.adapterControlInventarioDetalle = adapterControlInventarioDetalle.fillData(controlInventarioDetalleFilter);
        this.listaDetalle = this.adapterControlInventarioDetalle.getData();
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        
        this.adapterControlInventario = new ControlInventarioAdapter();
        this.controlInventarioFilter = new ControlInventario();
        
        this.adapterControlInventarioDetalle = new ControlInventarioDetalleAdapter();
        this.controlInventarioDetalleFilter = new ControlInventarioDetalle();
        
        this.show = false;
        this.showFilter = true;
        
        this.controlInventarioEdit = new ControlInventario();
        this.listaDetalle = new ArrayList<>();
        this.service = new ControlInventarioService();
    }

    /**
     * Constructor
     */
    public ControlInventarioEditController() {
        init();
    }
    
}
