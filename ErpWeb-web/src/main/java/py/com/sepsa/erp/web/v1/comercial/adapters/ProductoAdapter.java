
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Producto;
import py.com.sepsa.erp.web.v1.comercial.remote.ProductoService;

/**
 * Adaptador para la lista de tipo negocio
 * @author Cristina Insfrán
 */
public class ProductoAdapter extends DataListAdapter<Producto>{
    
    /**
     * Cliente para el servicio producto
     */
    private final ProductoService productoService;
    
     /**
     * Método para cargar la lista de datos
     * @param searchData Datos de búsqueda
     * @return ProductListAdapter
     */
    @Override
    public ProductoAdapter fillData(Producto searchData) {
     
        return productoService.getProductoList(searchData,getFirstResult(),getPageSize());
    }
    
    /**
     * Constructor de ProductoAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ProductoAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.productoService = new ProductoService();
    }

    /**
     * Constructor de ProductoAdapter
     */
    public ProductoAdapter() {
        super();
        this.productoService= new ProductoService();
    }
}
