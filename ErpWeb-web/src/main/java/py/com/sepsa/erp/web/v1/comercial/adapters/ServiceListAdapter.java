
package py.com.sepsa.erp.web.v1.comercial.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Servicio;
import py.com.sepsa.erp.web.v1.comercial.remote.ServicioServiceClient;

/**
 * Adaptador de la lista de Servicio
 * @author Cristina Insfrán
 */
public class ServiceListAdapter extends DataListAdapter<Servicio>{

     private final ServicioServiceClient serviceClient;
    /**
     * 
     * @param searchData
     * @return 
     */
    @Override
    public ServiceListAdapter fillData(Servicio searchData) {
       return serviceClient.getClientList(searchData,getFirstResult(),getPageSize());
    }
    
     /**
     * Constructor de ClientListAdapter
     * @param page Página de la lista
     * @param pageSize Tamaño de la página
     */
    public ServiceListAdapter(Integer page, Integer pageSize) {
        super(page, pageSize);
        this.serviceClient = new ServicioServiceClient();
    }

    /**
     * Constructor de ClientListAdapter
     */
    public ServiceListAdapter() {
        super();
        this.serviceClient = new ServicioServiceClient();
    }
    
    
}
