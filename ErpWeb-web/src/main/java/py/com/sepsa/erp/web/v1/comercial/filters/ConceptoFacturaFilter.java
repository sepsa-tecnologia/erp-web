package py.com.sepsa.erp.web.v1.comercial.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.comercial.pojos.ConceptoFactura;

/**
 * Filtro utilizado para el servicio de Concepto Factura
 *
 * @author Romina E. Núñez Rojas
 */
public class ConceptoFacturaFilter extends Filter {

    /**
     * Agrega el filtro de identificador de concepto factura
     *
     * @param id
     * @return
     */
    public ConceptoFacturaFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de concepto factura
     *
     * @param descripcion
     * @return
     */
    public ConceptoFacturaFilter descripcion(String descripcion) {
        if (descripcion != null && !descripcion.trim().isEmpty()) {
            params.put("descripcion", descripcion);
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param conceptoFactura datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ConceptoFactura conceptoFactura, Integer page, Integer pageSize) {
        ConceptoFacturaFilter filter = new ConceptoFacturaFilter();

        filter
                .id(conceptoFactura.getId())
                .descripcion(conceptoFactura.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ConceptoFacturaFilter
     */
    public ConceptoFacturaFilter() {
        super();
    }
}
