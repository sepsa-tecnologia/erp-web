package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.ContactoTelefonoNotificacion;

/**
 * Filtro utilizado para el servicio de contacto email
 *
 * @author Romina Núñez
 */
public class ContactoTelefonoNotificacionFilter extends Filter {

    /**
     * Agrega el filtro de identificador del Contacto
     *
     * @param idContacto Identificador del Contacto
     * @return
     */
    public ContactoTelefonoNotificacionFilter idContacto(Integer idContacto) {
        if (idContacto != null) {
            params.put("idContacto", idContacto);
        }
        return this;
    }

    /**
     * Agrega el filtro de la descripción de contacto
     *
     * @param contacto
     * @return
     */
    public ContactoTelefonoNotificacionFilter contacto(String contacto) {
        if (contacto != null && !contacto.trim().isEmpty()) {
            params.put("contacto", contacto.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del telefono
     *
     * @param idTelefono
     * @return
     */
    public ContactoTelefonoNotificacionFilter idTelefono(Integer idTelefono) {
        if (idTelefono != null) {
            params.put("idTelefono", idTelefono);
        }
        return this;
    }

    /**
     * Agrega el filtro de la prefijo
     *
     * @param prefijo
     * @return
     */
    public ContactoTelefonoNotificacionFilter prefijo(String prefijo) {
        if (prefijo != null && !prefijo.trim().isEmpty()) {
            params.put("prefijo", prefijo.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro numero
     *
     * @param numero
     * @return
     */
    public ContactoTelefonoNotificacionFilter numero(String numero) {
        if (numero != null && !numero.trim().isEmpty()) {
            params.put("numero", numero.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro de identificador del telefono
     *
     * @param idTipoNotificacion
     * @return
     */
    public ContactoTelefonoNotificacionFilter idTipoNotificacion(Integer idTipoNotificacion) {
        if (idTipoNotificacion != null) {
            params.put("idTipoNotificacion", idTipoNotificacion);
        }
        return this;
    }

    /**
     * Agrega el filtro tipo notificación
     *
     * @param tipoNotificacion
     * @return
     */
    public ContactoTelefonoNotificacionFilter tipoNotificacion(String tipoNotificacion) {
        if (tipoNotificacion != null && !tipoNotificacion.trim().isEmpty()) {
            params.put("tipoNotificacion", tipoNotificacion.trim());
        }
        return this;
    }

    /**
     * Agrega el filtro estado
     *
     * @param estado
     * @return
     */
    public ContactoTelefonoNotificacionFilter estado(String estado) {
        if (estado != null && !estado.trim().isEmpty()) {
            params.put("estado", estado.trim());
        }
        return this;
    }

    /**
     * Construye el mapa de parametros
     *
     * @param contactoTelefonoNotificacion 
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ContactoTelefonoNotificacion contactoTelefonoNotificacion, Integer page, Integer pageSize) {
        ContactoTelefonoNotificacionFilter filter = new ContactoTelefonoNotificacionFilter();

        filter
                .idContacto(contactoTelefonoNotificacion.getIdContacto())
                .contacto(contactoTelefonoNotificacion.getContacto())
                .idTelefono(contactoTelefonoNotificacion.getIdTelefono())
                .prefijo(contactoTelefonoNotificacion.getPrefijo())
                .numero(contactoTelefonoNotificacion.getNumero())
                .idTipoNotificacion(contactoTelefonoNotificacion.getIdTipoNotificacion())
                .tipoNotificacion(contactoTelefonoNotificacion.getTipoNotificacion())
                .estado(contactoTelefonoNotificacion.getEstado())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de ContactoTelefonoNotificacionFilter
     */
    public ContactoTelefonoNotificacionFilter() {
        super();
    }
}
