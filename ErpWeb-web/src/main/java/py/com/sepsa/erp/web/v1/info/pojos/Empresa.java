/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para Empresa
 *
 * @author Romina Núñez, Sergio D. Riveros Vazquez
 */
public class Empresa {

    /**
     * Identificador de Empresa
     */
    private Integer id;
    /**
     * Descripción de la empresa
     */
    private String descripcion;
    /**
     * Código
     */
    private String codigo;
    /**
     * Activo
     */
    private String activo;
    /**
     * Identificador de tipo Empresa
     */
    private Integer idTipoEmpresa;
    /**
     * Tipo Empresa
     */
    private TipoEmpresa tipoEmpresa;
    /**
     * Modulo inventario
     */
    private String moduloInventario;
    /**
     * Facturador electrónico
     */
    private String facturadorElectronico;
    /**
     * Identificador de empresa en el SistemaAdmin
     */
    private Integer idClienteSepsa; 
    /**
     * RUC
     */
    private String ruc;

    public Integer getId() {
        return id;
    }

    public void setModuloInventario(String moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public String getModuloInventario() {
        return moduloInventario;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    public Integer getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setIdTipoEmpresa(Integer idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setFacturadorElectronico(String facturadorElectronico) {
        this.facturadorElectronico = facturadorElectronico;
    }

    public String getFacturadorElectronico() {
        return facturadorElectronico;
    }
    
    public Empresa() {
    }

}
