
package py.com.sepsa.erp.web.v1.montoMinimo.remote;

import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.montoMinimo.adapters.MontoMinimoAdapter;
import py.com.sepsa.erp.web.v1.montoMinimo.filters.MontoMinimoFilter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.montoMinimo.pojos.MontoMinimo;
import py.com.sepsa.erp.web.v1.remote.APIErpComercial;

/**
 * Cliente para el servicio monto mínimo 
 * @author Romina E. Núñez Rojas
 */
public class MontoMinimoService extends APIErpComercial{
   
    /**
     * Obtiene la lista
     *
     * @param 
     * @param page
     * @param pageSize
     * @return
     */
    public MontoMinimoAdapter getMontoMinimoList(MontoMinimo montoMinimo, Integer page, Integer pageSize) {
        
        MontoMinimoAdapter lista = new MontoMinimoAdapter();

            Map params = MontoMinimoFilter.build(montoMinimo, page, pageSize);
       
        HttpURLConnection conn = GET(Resource.MONTO_MINIMO.url, 
                ContentType.JSON, params);
 
        if (conn != null) {
            BodyResponse response = BodyResponse.createInstance(conn,
                    MontoMinimoAdapter.class);

            lista = (MontoMinimoAdapter) response.getPayload();
           
            conn.disconnect();
        }
        return lista;
    }
     /**
     * Constructor de MontoMinimoService
     */
    public MontoMinimoService() {
        super();
    }
    
    
     /**
     * Recursos de conexión o servicios del APIErpComercial
     */
    public enum Resource {

        //Servicio
        MONTO_MINIMO("MontoMinimo","monto-minimo");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
