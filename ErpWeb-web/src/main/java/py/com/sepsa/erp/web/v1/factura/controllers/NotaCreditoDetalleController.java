package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCredito;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.NotaCreditoDetalleListPojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.NotaCreditoService;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;

/**
 * Controlador para Detalles
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("notaCreditoDetalle")
public class NotaCreditoDetalleController implements Serializable {

    /**
     * Servicio Nota de Credito
     */
    private NotaCreditoService serviceNotaCredito;
    /**
     * POJO Nota de Crédito
     */
    private NotaCreditoPojo notaCreditoDetalle;
    /**
     * Dato de Nota de Crédito
     */
    private String idNotaCredito;
    /**
     * Cliente para el servicio de descarga de archivo
     */
    private FileServiceClient fileServiceClient;
    private NotaCreditoDetalleListPojoAdapter adapterDetalle;
    private NotaCreditoDetallePojo detallePojoFilter;
    private Map parametros;

    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;

    private Boolean siediApiKude = false;
    
    @Inject
    private SessionData session;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    
    public String getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setDetallePojoFilter(NotaCreditoDetallePojo detallePojoFilter) {
        this.detallePojoFilter = detallePojoFilter;
    }

    public void setAdapterDetalle(NotaCreditoDetalleListPojoAdapter adapterDetalle) {
        this.adapterDetalle = adapterDetalle;
    }

    public NotaCreditoDetallePojo getDetallePojoFilter() {
        return detallePojoFilter;
    }

    public NotaCreditoDetalleListPojoAdapter getAdapterDetalle() {
        return adapterDetalle;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setNotaCreditoDetalle(NotaCreditoPojo notaCreditoDetalle) {
        this.notaCreditoDetalle = notaCreditoDetalle;
    }

    public NotaCreditoPojo getNotaCreditoDetalle() {
        return notaCreditoDetalle;
    }

    public NotaCreditoService getServiceNotaCredito() {
        return serviceNotaCredito;
    }

    public void setIdNotaCredito(String idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public void setServiceNotaCredito(NotaCreditoService serviceNotaCredito) {
        this.serviceNotaCredito = serviceNotaCredito;
    }

//</editor-fold>
    /**
     * Método para obtener el detalle de la nota de crédito
     */
    public void obtenerNotaCreditoDetalle() {
        notaCreditoDetalle.setId(Integer.parseInt(idNotaCredito));
        notaCreditoDetalle.setListadoPojo(Boolean.TRUE);

        notaCreditoDetalle = serviceNotaCredito.getNotaCreditoDetalle(notaCreditoDetalle);
        
        detallePojoFilter.setListadoPojo(Boolean.TRUE);
        detallePojoFilter.setIdNotaCredito(notaCreditoDetalle.getId());
        adapterDetalle = adapterDetalle.fillData(detallePojoFilter);
    }

    /**
     * Método para obtener la dirección a dirigirse
     *
     * @return
     */
    public String notaCreditoURL() {
        return String.format("nota-credito-list"
                + "?faces-redirect=true");
    }

    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download(Boolean ticket, Boolean xml) {
        String contentType = ("application/pdf");
        String fileName = notaCreditoDetalle.getNroNotaCredito() + ".pdf";
        String url = "nota-credito/consulta/" + idNotaCredito;
        if (notaCreditoDetalle.getDigital().equalsIgnoreCase("S")){
            parametros.put("siediApi", siediApiKude);
            if (xml){
                fileName = notaCreditoDetalle.getNroNotaCredito() + ".xml";
                url = "nota-credito/consulta-xml/" + idNotaCredito;
            } else {
                 parametros.put("ticket", ticket);
            } 
        }
        byte[] data = fileServiceClient.downloadNotaCredito(url,parametros);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "nota-credito/consulta/" + idNotaCredito;
        if (notaCreditoDetalle.getDigital().equalsIgnoreCase("S")){
            parametros.put("siediApi", siediApiKude);
            parametros.put("ticket", false); 
        }
        byte[] data = fileServiceClient.downloadNotaCredito(url,parametros);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try ( FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }
    
    /**
     * Obtiene el valor de configuracion siediApi
     */
    public void obtenerConfigSiediApi() {
        configuracionValorFilter = new ConfiguracionValor();
        adapterConfigValor = new ConfiguracionValorListAdapter();
        configuracionValorFilter.setCodigoConfiguracion("ARCHIVOS_DTE_SIEDI_API");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            siediApiKude = false;
        } else {
            siediApiKude = adapterConfigValor.getData().get(0).getValor().equalsIgnoreCase("S"); 
        }
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();

        idNotaCredito = params.get("id");

        this.serviceNotaCredito = new NotaCreditoService();
        this.notaCreditoDetalle = new NotaCreditoPojo();
        this.fileServiceClient = new FileServiceClient();
        
        this.adapterDetalle = new NotaCreditoDetalleListPojoAdapter();
        this.detallePojoFilter = new NotaCreditoDetallePojo();
        this.parametros = new HashMap();
        obtenerNotaCreditoDetalle();
        obtenerConfigSiediApi();
    }

}
