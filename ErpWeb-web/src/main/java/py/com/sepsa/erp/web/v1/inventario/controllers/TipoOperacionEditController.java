package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.pojos.TipoOperacion;
import py.com.sepsa.erp.web.v1.inventario.remote.TipoOperacionService;

/**
 * Controlador para edición de Tipo de Operación
 *
 * @author Alexander Triana
 */
@ViewScoped
@Named("tipoOperacionEdit")
public class TipoOperacionEditController implements Serializable {

    /**
     * Cliente para edición de tipo de operación
     */
    private final TipoOperacionService service;

    /**
     * Pojo de tipoOperacion
     */
    private TipoOperacion tipoOperacion;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(TipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    //</editor-fold>

    /**
     * Método para editar Tipo de Operación
     */
    public void edit() {
        BodyResponse<TipoOperacion> response = service.
                editTipoOperacion(tipoOperacion);

        if (response.getSuccess()) {
            FacesContext.getCurrentInstance().
                    addMessage(null, 
                            new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                    "Tipo de Operacion editada correctamente!"));
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    private void init(int id) {
        this.tipoOperacion = service.get(id);
    }

    /**
     * Constructor de la clase
     *
     * @param service
     */
    public TipoOperacionEditController() {
        this.service = new TipoOperacionService();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        init(id);
    }

}
