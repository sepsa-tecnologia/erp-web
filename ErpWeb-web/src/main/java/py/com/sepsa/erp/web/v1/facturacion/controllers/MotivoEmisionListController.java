/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.MotivoEmisionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.MotivoEmision;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista Motivo Emision
 *
 * @author Sergio D. Riveros Vazquez
 */
@ViewScoped
@Named("motivoEmisionList")
public class MotivoEmisionListController implements Serializable {

    /**
     * Adaptador para la lista de motivoEmision
     */
    private MotivoEmisionAdapter motivoEmisionAdapterList;
    /**
     * POJO de MotivoEmision
     */
    private MotivoEmision motivoEmisionFilter;
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public MotivoEmisionAdapter getMotivoEmisionAdapterList() {
        return motivoEmisionAdapterList;
    }

    public void setMotivoEmisionAdapterList(MotivoEmisionAdapter motivoEmisionAdapterList) {
        this.motivoEmisionAdapterList = motivoEmisionAdapterList;
    }

    public MotivoEmision getMotivoEmisionFilter() {
        return motivoEmisionFilter;
    }

    public void setMotivoEmisionFilter(MotivoEmision motivoEmisionFilter) {
        this.motivoEmisionFilter = motivoEmisionFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener motivoEmisions
     */
    public void getMotivoEmision() {
        getMotivoEmisionAdapterList().setFirstResult(0);
        this.setMotivoEmisionAdapterList(getMotivoEmisionAdapterList().fillData(getMotivoEmisionFilter()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.motivoEmisionFilter = new MotivoEmision();
        getMotivoEmision();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("motivo-emision-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.motivoEmisionFilter = new MotivoEmision();
            this.motivoEmisionAdapterList = new MotivoEmisionAdapter();
            getMotivoEmision();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
