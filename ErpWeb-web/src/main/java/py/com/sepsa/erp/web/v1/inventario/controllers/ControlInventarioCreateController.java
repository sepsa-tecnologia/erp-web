/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.InventarioResumenAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.pojos.InventarioResumen;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.ControlInventarioService;
import py.com.sepsa.erp.web.v1.usuario.adapters.UserListAdapter;
import py.com.sepsa.erp.web.v1.usuario.pojos.Usuario;

/**
 * Controlador para la vista motivo
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("crearControlInventario")
public class ControlInventarioCreateController implements Serializable {

    /**
     * Adaptador para la lista de motivos
     */
    private MotivoAdapter motivoAdapterList;

    /**
     * POJO de MotivoEmisionInterno
     */
    private Motivo motivoFilter;
    /**
     * Pojo Control Inventario
     */
    private ControlInventario controlInventarioCreate;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterEncargado;
    /**
     * POJO Encargado
     */
    private Usuario encargado;
    /**
     * Adaptador para el listado de Encargados
     */
    private UserListAdapter adapterSupervisor;
    /**
     * POJO Encargado
     */
    private Usuario supervisor;
    /**
     * Service control inventario
     */
    private ControlInventarioService service;
    /**
     * Objeto depositoLogistico
     */
    private DepositoLogistico deposito;

    /**
     * Adaptador de la lista de depositoLogistico
     */
    private DepositoLogisticoAdapter depositoAdapter;
    /**
     * Adaptador para la lista de inventario
     */
    private InventarioResumenAdapter inventarioResumenAdapter;

    /**
     * Pojo de inventario
     */
    private InventarioResumen inventarioResumen;
            /**
     * Lista para cobro
     */
    private List<InventarioResumen> selectedProductos;
        /**
     * Pojo de inventario
     */
    private InventarioResumen inventarioTest;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public MotivoAdapter getMotivoAdapterList() {
        return motivoAdapterList;
    }

    public void setInventarioTest(InventarioResumen inventarioTest) {
        this.inventarioTest = inventarioTest;
    }

    public InventarioResumen getInventarioTest() {
        return inventarioTest;
    }

    public void setSelectedProductos(List<InventarioResumen> selectedProductos) {
        this.selectedProductos = selectedProductos;
    }

    public List<InventarioResumen> getSelectedProductos() {
        return selectedProductos;
    }

    public void setInventarioResumenAdapter(InventarioResumenAdapter inventarioResumenAdapter) {
        this.inventarioResumenAdapter = inventarioResumenAdapter;
    }

    public void setInventarioResumen(InventarioResumen inventarioResumen) {
        this.inventarioResumen = inventarioResumen;
    }

    public InventarioResumenAdapter getInventarioResumenAdapter() {
        return inventarioResumenAdapter;
    }

    public InventarioResumen getInventarioResumen() {
        return inventarioResumen;
    }

    public void setDepositoAdapter(DepositoLogisticoAdapter depositoAdapter) {
        this.depositoAdapter = depositoAdapter;
    }

    public void setDeposito(DepositoLogistico deposito) {
        this.deposito = deposito;
    }

    public DepositoLogisticoAdapter getDepositoAdapter() {
        return depositoAdapter;
    }

    public DepositoLogistico getDeposito() {
        return deposito;
    }

    public void setService(ControlInventarioService service) {
        this.service = service;
    }

    public ControlInventarioService getService() {
        return service;
    }

    public void setSupervisor(Usuario supervisor) {
        this.supervisor = supervisor;
    }

    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    public void setAdapterSupervisor(UserListAdapter adapterSupervisor) {
        this.adapterSupervisor = adapterSupervisor;
    }

    public void setAdapterEncargado(UserListAdapter adapterEncargado) {
        this.adapterEncargado = adapterEncargado;
    }

    public Usuario getSupervisor() {
        return supervisor;
    }

    public Usuario getEncargado() {
        return encargado;
    }

    public UserListAdapter getAdapterSupervisor() {
        return adapterSupervisor;
    }

    public UserListAdapter getAdapterEncargado() {
        return adapterEncargado;
    }

    public void setControlInventarioCreate(ControlInventario controlInventarioCreate) {
        this.controlInventarioCreate = controlInventarioCreate;
    }

    public ControlInventario getControlInventarioCreate() {
        return controlInventarioCreate;
    }

    public void setMotivoAdapterList(MotivoAdapter motivoAdapterList) {
        this.motivoAdapterList = motivoAdapterList;
    }

    public Motivo getMotivoFilter() {
        return motivoFilter;
    }

    public void setMotivoFilter(Motivo motivoFilter) {
        this.motivoFilter = motivoFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener motivoEmisions
     */
    public void getMotivos() {
        getMotivoAdapterList().setFirstResult(0);
        motivoFilter.setCodigoTipoMotivo("CONTROL_INVENTARIO");
        this.motivoAdapterList = this.motivoAdapterList.fillData(motivoFilter);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioEncargado(String query) {

        Usuario encargado = new Usuario();
        encargado.setTienePersonaFisica(true);
        encargado.setNombrePersonaFisica(query);

        adapterEncargado = adapterEncargado.fillData(encargado);

        return adapterEncargado.getData();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Usuario> completeQueryUsuarioSupervisor(String query) {

        Usuario supervisor = new Usuario();
        supervisor.setTienePersonaFisica(true);
        supervisor.setNombrePersonaFisica(query);

        adapterSupervisor = adapterSupervisor.fillData(supervisor);

        return adapterSupervisor.getData();
    }

    /**
     * Autocomplete para depositoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        DepositoLogistico deposito = new DepositoLogistico();
        deposito.setDescripcion(query);
        deposito.setListadoPojo(true);
        depositoAdapter = depositoAdapter.fillData(deposito);

        return depositoAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectEncargadoFilter(SelectEvent event) {
        encargado.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioCreate.setIdUsuarioEncargado(encargado.getId());
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectSupervisorFilter(SelectEvent event) {
        supervisor.setId(((Usuario) event.getObject()).getId());
        this.controlInventarioCreate.setIdUsuarioSupervisor(supervisor.getId());
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        controlInventarioCreate.setIdDepositoLogistico(((DepositoLogistico) event.getObject()).getId());
    }

    /**
     * Método para crear
     */
    public void create() {
        this.controlInventarioCreate.setCodigoEstado("PENDIENTE");
        BodyResponse<ControlInventario> respuestaDep = service.setControlInventario(controlInventarioCreate);

        if (respuestaDep.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Control de Inventario creado correctamente!"));

            clear();
        }

    }

    /**
     * Método para limpiar formulario
     */
    public void clear() {
        this.controlInventarioCreate = new ControlInventario();
        this.deposito = new DepositoLogistico();
        this.supervisor = new Usuario();
        this.encargado = new Usuario();
        PF.current().ajax().update(":list-control-inventario-form:form-data:deposito");
        PF.current().ajax().update(":list-control-inventario-form:form-data:supervisor");
        PF.current().ajax().update(":list-control-inventario-form:form-data:encargado");
    }

    public void filterResumenInventario() {
        getInventarioResumenAdapter().setFirstResult(0);
        this.inventarioResumen.setListadoPojo(Boolean.TRUE);
        this.inventarioResumenAdapter = this.inventarioResumenAdapter.fillData(inventarioResumen);
    }

    /**
     * Inicializa los datos
     */
    private void init() {
        this.motivoFilter = new Motivo();
        this.motivoAdapterList = new MotivoAdapter();

        this.controlInventarioCreate = new ControlInventario();
        this.service = new ControlInventarioService();

        this.adapterEncargado = new UserListAdapter();
        this.adapterSupervisor = new UserListAdapter();
        this.encargado = new Usuario();
        this.supervisor = new Usuario();

        this.deposito = new DepositoLogistico();
        this.deposito.setListadoPojo(Boolean.TRUE);
        this.depositoAdapter = new DepositoLogisticoAdapter();

        this.inventarioResumen = new InventarioResumen();
        this.inventarioResumenAdapter = new InventarioResumenAdapter();
        
        this.selectedProductos = new ArrayList<>();
        this.inventarioTest = new InventarioResumen();

        filterResumenInventario();
        getMotivos();
    }

    /**
     * Constructor
     */
    public ControlInventarioCreateController() {
        init();
    }

}
