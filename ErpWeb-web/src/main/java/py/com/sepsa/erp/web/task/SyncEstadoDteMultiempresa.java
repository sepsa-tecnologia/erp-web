/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.task;

import py.com.sepsa.erp.syncdte.SyncEstadoDteRunnable;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorUtils;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.utils.automation.AbstractTask;
import py.com.sepsa.utils.automation.Task;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Romina Núñez
 */
public class SyncEstadoDteMultiempresa extends AbstractTask {

    /**
     * Identificador de empresa
     */
    private final Integer idEmpresa;

    public SyncEstadoDteMultiempresa(Proceso proceo, Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
        this.task = new Task(proceo.getId(),
                String.format("%s - IdEmpresa:%d", proceo.getCodigo(), idEmpresa),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getFrecuencia()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getHora()),
                Units.execute(() -> proceo.getFrecuenciaEjecucion().getMinuto()));
    }

    @Override
    public void task() {
        try {

            Empresa efilter = new Empresa();
            efilter.setActivo("S");
            efilter.setFacturadorElectronico("S");
            EmpresaAdapter eadapter = new EmpresaAdapter();
            eadapter.setPageSize(1000);
            eadapter = eadapter.fillData(efilter);

            for (Empresa empresa : eadapter.getData()) {
                generar(empresa.getId());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    public static void generar(Integer idEmpresa) {
        try {
            String apiToken = ConfiguracionValorUtils.getApiToken(idEmpresa);
            String user = ConfiguracionValorUtils.getUser(idEmpresa);
            String pass = ConfiguracionValorUtils.getPass(idEmpresa);
            SyncEstadoDteRunnable sedr = new SyncEstadoDteRunnable(apiToken, user, pass, idEmpresa);
            sedr.run();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
