
package py.com.sepsa.erp.web.v1.factura.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para cheque
 * @author Cristina Insfrán
 */
public class Cheque {

   
    /**
     * Identificador de entidad financiera
     */
    private Integer idEntidadFinanciera;
    
    /**
     * Identificador de deposito
     */
    private Integer idDeposito;
    
    /**
     * Nro cheque
     */
    private String nroCheque;
    
    /**
     * Fecha de emisión
     */
    private Date fechaEmision;
    
    /**
     * Fecha de pago
     */
    private Date fechaPago;
    
    /**
     * Monto cheque
     */
    private BigDecimal montoCheque; 
    
    /**
     * Bandera para omitir validacion de datos de cheque
     */
    private String omitirValidacionCheque;
    
    
    public String getOmitirValidacionCheque() {
        return omitirValidacionCheque;
    }

    public void setOmitirValidacionCheque(String omitirValidacionCheque) {
        this.omitirValidacionCheque = omitirValidacionCheque;
    }
    
     /**
     * @return the idEntidadFinanciera
     */
    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    /**
     * @param idEntidadFinanciera the idEntidadFinanciera to set
     */
    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    /**
     * @return the idDeposito
     */
    public Integer getIdDeposito() {
        return idDeposito;
    }

    /**
     * @param idDeposito the idDeposito to set
     */
    public void setIdDeposito(Integer idDeposito) {
        this.idDeposito = idDeposito;
    }

    /**
     * @return the nroCheque
     */
    public String getNroCheque() {
        return nroCheque;
    }

    /**
     * @param nroCheque the nroCheque to set
     */
    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    /**
     * @return the fechaEmision
     */
    public Date getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the montoCheque
     */
    public BigDecimal getMontoCheque() {
        return montoCheque;
    }

    /**
     * @param montoCheque the montoCheque to set
     */
    public void setMontoCheque(BigDecimal montoCheque) {
        this.montoCheque = montoCheque;
    }
     
    /**
     * Constructor
     */
    public Cheque(){
        
    }
}
