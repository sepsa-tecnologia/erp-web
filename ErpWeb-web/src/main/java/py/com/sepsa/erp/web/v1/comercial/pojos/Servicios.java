package py.com.sepsa.erp.web.v1.comercial.pojos;

/**
 * Pojo para servicios.
 *
 * @author alext
 */
public class Servicios {

    /**
     * Identificador del servicio.
     */
    private Integer id;
    /**
     * Descripción del servicio.
     */
    private String descripcion;
    /**
     * Código de servicio.
     */
    private String codigo;
    /**
     * Identificador de producto.
     */
    private Integer idProducto;
    /**
     * Código de producto.
     */
    private String codigoProducto;
    /**
     * Producto.
     */
    private Producto producto;

    /**
     * Getter y Setter
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * Constructor
     */
    public Servicios() {
    }

    /**
     * Constructor.
     *
     * @param id
     * @param descripcion
     * @param codigo
     * @param idProducto
     * @param codigoProducto
     */
    public Servicios(Integer id, String descripcion, String codigo, Integer idProducto, String codigoProducto) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.idProducto = idProducto;
        this.codigoProducto = codigoProducto;
    }
}
