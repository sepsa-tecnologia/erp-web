package py.com.sepsa.erp.web.v1.http.body.pojos;


import java.util.List;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * POJO para estatus de la respuesta
 *
 * @author Daniel F. Escauriza Arza
 */
public class BodyResponseStatus {

    /**
     * Código de la respuesta
     */
    private Integer code;
    /**
     * Fecha en la cual se proceso la solicitud
     */
    private String stamp;
    /**
     * Descripción del proceso de la solicitud
     */
    private String description;
    /**
     * Mensajes
     */
    private List<Mensaje> mensajes;

    /**
     * Obtiene el código de la respuesta
     *
     * @return Código de la respuesta
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Setea el código de la respuesta
     *
     * @param code Código de la respuesta
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Obtiene la fecha en la cual se proceso la solicitud
     *
     * @return Fecha en la cual se proceso la solicitud
     */
    public String getStamp() {
        return stamp;
    }

    /**
     * Setea la fecha en la cual se proceso la solicitud
     *
     * @param stamp Fecha en la cual se proceso la solicitud
     */
    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the description
     */
    public String getDescripcion() {
        return description;
    }

    /**
     * @param descripcion the description to set
     */
    public void setDescripcion(String descripcion) {
        this.description = descripcion;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    /**
     * Constructor de BodyResponseStatus
     *
     * @param code Código de la respuesta
     * @param stamp Fecha en la cual se proceso la solicitud
     */
    public BodyResponseStatus(Integer code, String stamp) {
        this.code = code;
        this.stamp = stamp;
    }

    public BodyResponseStatus(String stamp, String descripcion) {
        this.stamp = stamp;
        this.description = descripcion;
    }

    /**
     * Constructor de BodyResponseStatus
     */
    public BodyResponseStatus() {
    }
}
