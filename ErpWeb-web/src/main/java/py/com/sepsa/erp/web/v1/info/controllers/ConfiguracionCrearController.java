package py.com.sepsa.erp.web.v1.info.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Configuracion;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.remote.ConfiguracionValorServiceClient;

/**
 * Controlador para Configuración
 *
 * @author Romina Nuñez
 */
@ViewScoped
@Named("crearConfig")
public class ConfiguracionCrearController implements Serializable {

    /**
     * POJO Configuración
     */
    private Configuracion configuracionNuevo;
    /**
     * Llamada al servicio de Configuración Valor
     */
    private ConfiguracionValorServiceClient serviceConfiguracionValor;

    //<editor-fold defaultstate="collapsed" desc="getters & setters">
    public Configuracion getConfiguracionNuevo() {
        return configuracionNuevo;
    }
    
    public void setConfiguracionNuevo(Configuracion configuracionNuevo) {
        this.configuracionNuevo = configuracionNuevo;
    }
    
    public ConfiguracionValorServiceClient getServiceConfiguracionValor() {
        return serviceConfiguracionValor;
    }
    
    public void setServiceConfiguracionValor(ConfiguracionValorServiceClient serviceConfiguracionValor) {
        this.serviceConfiguracionValor = serviceConfiguracionValor;
    }
//</editor-fold>


    public void createConfiguracion() {
        Integer id = null;
        boolean save = true;

        if (configuracionNuevo.getDescripcion().trim().isEmpty() || configuracionNuevo.getDescripcion() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar una descripción para la configuración!"));
            save = false;
        }
        if (configuracionNuevo.getCodigo().trim().isEmpty() || configuracionNuevo.getCodigo() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar un código para la configuración!"));
            save = false;
        }

        if (save != false) {
            id = serviceConfiguracionValor.createConfiguracion(configuracionNuevo);

            if (id != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Configuración creada correctamente!"));
                configuracionNuevo = new Configuracion();
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un problema al intentar crear la configuración!"));
            }
        }

    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        this.configuracionNuevo = new Configuracion();

        this.serviceConfiguracionValor = new ConfiguracionValorServiceClient();

    }
}
