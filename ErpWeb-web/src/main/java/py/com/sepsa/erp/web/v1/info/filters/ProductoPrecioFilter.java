
package py.com.sepsa.erp.web.v1.info.filters;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;

/**
 * Filter para producto precio
 * @author Cristina Insfrán
 */
public class ProductoPrecioFilter extends Filter{
   
     /**
     * Agrega el filtro de identificador 
     *
     * @param id
     * @return
     */
    public ProductoPrecioFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
      /**
     * Agrega el filtro de identificador estado
     *
     * @param idEstado
     * @return
     */
    public ProductoPrecioFilter idEstado(Integer idEstado) {
        if (idEstado != null) {
            params.put("idEstado", idEstado);
        }
        return this;
    }
    
    
     /**
     * Agrega el filtro de identificador de producto
     *
     * @param idProducto
     * @return
     */
    public ProductoPrecioFilter idProducto(Integer idProducto) {
        if (idProducto != null) {
            params.put("idProducto", idProducto);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de identificador de producto
     *
     * @param idMoneda
     * @return
     */
    public ProductoPrecioFilter idMoneda(Integer idMoneda) {
        if (idMoneda != null) {
            params.put("idMoneda", idMoneda);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de idCanalVenta
     *
     * @param idCanalVenta
     * @return
     */
    public ProductoPrecioFilter idCanalVenta(Integer idCanalVenta) {
        if (idCanalVenta != null) {
            params.put("idCanalVenta", idCanalVenta);
        }
        return this;
    }
    
         /**
     * Agrega el filtro de idCliente
     *
     * @param idCliente
     * @return
     */
    public ProductoPrecioFilter idCliente(Integer idCliente) {
        if (idCliente != null) {
            params.put("idCliente", idCliente);
        }
        return this;
    }
    
     /**
     * Agrega el filtro de activo del producto
     *
     * @param activo
     * @return
     */
    public ProductoPrecioFilter activo(String activo) {
        if (activo != null && !activo.trim().isEmpty()) {
            params.put("activo", activo);
        }
        return this;
    }
    
       /**
     * Agrega el filtro de producto descripción
     *
     * @param productoDescripcion
     * @return
     */
    public ProductoPrecioFilter productoDescripcion(String productoDescripcion) {
        if (productoDescripcion != null && !productoDescripcion.trim().isEmpty()) {
            params.put("productoDescripcion", productoDescripcion);
        }
        return this;
    }
    
       /**
     * Agrega el filtro de canal venta descripción
     *
     * @param canalVentaDescripcion
     * @return
     */
    public ProductoPrecioFilter canalVentaDescripcion(String canalVentaDescripcion) {
        if (canalVentaDescripcion != null && !canalVentaDescripcion.trim().isEmpty()) {
            params.put("canalVentaDescripcion", canalVentaDescripcion);
        }
        return this;
    }
    
    
       /**
     * Agrega el filtro de precio
     *
     * @param precio
     * @return
     */
    public ProductoPrecioFilter precio(BigDecimal precio) {
        if (precio != null) {
            params.put("precio", precio);
        }
        return this;
    }
    
        /**
     * Agrega el filtro de fehca
     *
     * @param fecha
     * @return
     */
    public ProductoPrecioFilter fecha(Date fecha) {
         if (fecha != null) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = simpleDateFormat.format(fecha);
                params.put("fecha", URLEncoder.encode(date, "UTF-8"));
               
            } catch (Exception e) {
            }
        }
        return this;
    }
    
    
     /**
     * Construye el mapa de parametros
     *
     * @param producto datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ProductoPrecio producto, Integer page, Integer pageSize) {
        ProductoPrecioFilter filter = new ProductoPrecioFilter();

        filter
                .id(producto.getId())
                .idProducto(producto.getIdProducto())
                .idMoneda(producto.getIdMoneda())
                .idCanalVenta(producto.getIdCanalVenta())
                .idCliente(producto.getIdCliente())
                .activo(producto.getActivo())
                .productoDescripcion(producto.getProductoDescripcion())
                .canalVentaDescripcion(producto.getCanalVentaDescripcion())
                .fecha(producto.getFecha())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor
     */
    public ProductoPrecioFilter() {
        super();
    }
}
