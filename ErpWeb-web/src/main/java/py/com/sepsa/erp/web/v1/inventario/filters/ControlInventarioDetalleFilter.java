/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventarioDetalle;

/**
 * Filter para Control Inventario Detalle
 *
 * @author Romina Núñez
 */
public class ControlInventarioDetalleFilter extends Filter {

    /**
     * Agrega el filtro por identificador
     *
     * @param id
     * @return
     */
    public ControlInventarioDetalleFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }

    /**
     * Agrega el filtro por identificador
     *
     * @param idControlInventario
     * @return
     */
    public ControlInventarioDetalleFilter idControlInventario(Integer idControlInventario) {
        if (idControlInventario != null) {
            params.put("idControlInventario", idControlInventario);
        }
        return this;
    }

    /**
     * Agrega el filtro listado pojo
     *
     * @param listadoPojo
     * @return
     */
    public ControlInventarioDetalleFilter listadoPojo(Boolean listadoPojo) {
        if (listadoPojo != null) {
            params.put("listadoPojo", listadoPojo);
        }
        return this;
    }

    /**
     * Construye el mapa de parámetros
     *
     * @param searchData datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return
     */
    public static Map build(ControlInventarioDetalle searchData, Integer page, Integer pageSize) {
        ControlInventarioDetalleFilter filter = new ControlInventarioDetalleFilter();

        filter
                .id(searchData.getId())
                .idControlInventario(searchData.getIdControlInventario())
                .listadoPojo(searchData.getListadoPojo())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de la clase
     */
    public ControlInventarioDetalleFilter() {
        super();
    }

}
