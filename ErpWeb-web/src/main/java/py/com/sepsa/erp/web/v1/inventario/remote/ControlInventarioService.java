package py.com.sepsa.erp.web.v1.inventario.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.HttpURLConnection;
import java.util.Map;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyByteArrayResponse;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.ControlInventarioDetalleAdapter;
import py.com.sepsa.erp.web.v1.inventario.filters.ControlInventarioDetalleFilter;
import py.com.sepsa.erp.web.v1.inventario.filters.ControlInventarioFilter;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventario;
import py.com.sepsa.erp.web.v1.inventario.pojos.ControlInventarioDetalle;
import py.com.sepsa.erp.web.v1.remote.APIErpInventario;

/**
 * Servicio para listar inventario
 *
 * @author Romina Núñez
 */
public class ControlInventarioService extends APIErpInventario {

    /**
     * Método para obtener la lista de inventario
     *
     * @param ci
     * @param page
     * @param pageSize
     * @return
     */
    public ControlInventarioAdapter getControlInventario(ControlInventario ci, Integer page,
            Integer pageSize) {
        ControlInventarioAdapter lista = new ControlInventarioAdapter();
        try {
            Map params = ControlInventarioFilter.build(ci, page, pageSize);
            HttpURLConnection conn = GET(Resource.CONTROL_INVENTARIO.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        ControlInventarioAdapter.class);
                lista = (ControlInventarioAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return lista;
    }

    /**
     * Método para crear inventario
     *
     * @param cinventario
     * @return
     */
    public BodyResponse<ControlInventario> setControlInventario(ControlInventario cinventario) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = POST(Resource.CONTROL_INVENTARIO.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
            this.addBody(conn, gson.toJson(cinventario));
            response = BodyResponse.createInstance(conn, ControlInventario.class);
        }
        return response;
    }

    /**
     * Método para obtener la lista de inventario
     *
     * @param ci
     * @param page
     * @param pageSize
     * @return
     */
    public ControlInventarioDetalleAdapter getControlInventarioDetalleList(ControlInventarioDetalle ci, Integer page,
            Integer pageSize) {

        ControlInventarioDetalleAdapter lista = new ControlInventarioDetalleAdapter();
        try {
            Map params = ControlInventarioDetalleFilter.build(ci, page, pageSize);
            HttpURLConnection conn = GET(Resource.CONTROL_INVENTARIO_DETALLE.url,
                    ContentType.JSON, params);
            if (conn != null) {
                BodyResponse response = BodyResponse.createInstance(conn,
                        ControlInventarioDetalleAdapter.class);
                lista = (ControlInventarioDetalleAdapter) response.getPayload();
                conn.disconnect();
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return lista;
    }

    /**
     * Método para descargar un archivo desde el servidor
     *
     * @param id Clave asociada al archivo
     * @return Contenido descargado
     */
    public byte[] download(String url) {

        HttpURLConnection conn = GET(String.format(url),
                ContentType.MULTIPART, null);

        byte[] result = null;

        if (conn != null) {

            BodyByteArrayResponse response = BodyByteArrayResponse
                    .createInstance(conn);

            result = response.getPayload();

            conn.disconnect();
        }

        return result;
    }
    
    /**
     * Método para editar
     *
     * @param deposito
     * @return
     */
    public BodyResponse<ControlInventario> editDeposito(ControlInventario deposito) {
        BodyResponse response = new BodyResponse();
        HttpURLConnection conn = PUT(Resource.CONTROL_INVENTARIO_COMPLETAR.url, ContentType.JSON);
        if (conn != null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ssZ").create();
            this.addBody(conn, gson.toJson(deposito));
            response = BodyResponse.createInstance(conn, ControlInventario.class);
        }
        return response;
    }

    /**
     * Constructor de la clase
     */
    public ControlInventarioService() {
        super();
    }

    /**
     * Recursos de conexión o servicios del APIErpFacturacion
     */
    public enum Resource {

        //Servicios
        CONTROL_INVENTARIO("Control Inventario", "control-inventario"),
        CONTROL_INVENTARIO_COMPLETAR("Completar Control Inventario", "control-inventario/completar"),
        CONTROL_INVENTARIO_DETALLE("Control Inventario Detalle", "control-inventario-detalle");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }

}
