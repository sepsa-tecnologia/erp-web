 package py.com.sepsa.erp.web.v1.factura.controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import py.com.sepsa.erp.web.task.GeneracionDteFacturaMultiempresa;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientPojoAdapter;
import py.com.sepsa.erp.web.v1.comercial.adapters.MonedaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.HistoricoLiquidacionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.DireccionAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaTelefonoAdapter;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.web.v1.comercial.pojos.Moneda;
import py.com.sepsa.erp.web.v1.factura.pojos.Cheque;
import py.com.sepsa.erp.web.v1.factura.pojos.Cobro;
import py.com.sepsa.erp.web.v1.factura.pojos.CobroDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.Cuota;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.DatosFactura;
import py.com.sepsa.erp.web.v1.factura.pojos.EntidadFinanciera;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetalle;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDncp;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaNotificacion;
import py.com.sepsa.erp.web.v1.factura.pojos.HistoricoLiquidacion;
import py.com.sepsa.erp.web.v1.factura.pojos.LugarCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.MontoLetras;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoCobro;
import py.com.sepsa.erp.web.v1.factura.pojos.TipoTransaccion;
import py.com.sepsa.erp.web.v1.facturacion.adapters.CobroTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.EntidadFinancieraListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaTalonarioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.LugarCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.OrdenDeCompraAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCambioAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoCobroListAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoTransaccionAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenCompraDetalles;
import py.com.sepsa.erp.web.v1.facturacion.pojos.OrdenDeCompra;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoCambio;
import py.com.sepsa.erp.web.v1.facturacion.remote.CobroService;
import py.com.sepsa.erp.web.v1.info.pojos.Direccion;
import py.com.sepsa.erp.web.v1.info.pojos.Persona;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaTelefono;
import py.com.sepsa.erp.web.v1.facturacion.remote.FacturacionService;
import py.com.sepsa.erp.web.v1.facturacion.remote.FileServiceClient;
import py.com.sepsa.erp.web.v1.facturacion.remote.MontoLetrasService;
import py.com.sepsa.erp.web.v1.facturacion.remote.OrdenDeCompraService;
import py.com.sepsa.erp.web.v1.facturacion.remote.TipoCambioService;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.ConfiguracionValorListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.LocalListAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.PersonaEmailAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ReferenciaGeograficaAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.TelefonoListAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Local;
import py.com.sepsa.erp.web.v1.info.pojos.PersonaEmail;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.info.pojos.ReferenciaGeografica;
import py.com.sepsa.erp.web.v1.info.pojos.Telefono;
import py.com.sepsa.erp.web.v1.info.remote.ProductoService;
import py.com.sepsa.erp.web.v1.producto.pojo.ProductoPrecio;
import py.com.sepsa.erp.web.v1.system.controllers.SessionData;
import py.com.sepsa.erp.web.v1.system.pojos.MailUtils;
import py.com.sepsa.erp.web.v1.test.pojos.Mensaje;

/**
 * Controlador alternativo para crear facturas
 *
 * @author Williams Vera
 */
@ViewScoped
@Named("invoiceCreate")
public class FacturaCreateController implements Serializable {
    /**
     * Objeto Cliente
     */
    private ClientePojo cliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFactura;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterCliente;
    /**
     * Objeto Cliente
     */
    private Cliente clienteFilterPrueba;
    /**
     * Adaptador para la lista de clientes
     */
    private ClientListAdapter adapterClientePrueba2;
    /**
     * Variable de control
     */
    private boolean show;
    /**
     * Linea
     */
    private Integer linea;
    /**
     * ***DATOS REALES****
     */
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private Factura facturaFilter;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaAdapter adapterFacturaCancelada;
    /**
     * POJO para Factura
     */
    private Factura facturaFilterCancelada;
    /**
     * POJO DatosFactura
     */
    private DatosFactura datoFactura;
    /**
     * Servicio Facturacion
     */
    private FacturacionService serviceFacturacion;
    /**
     * POJO Factura para crear
     */
    private Factura facturaCreate;
    /**
     * Dato del Cliente
     */
    private String ruc;
    /**
     * Dato del Cliente
     */
    private String direccion;
    /**
     * Dato del Cliente
     */
    private String telefono;
    /**
     * Adaptador para la lista de persona
     */
    private PersonaListAdapter personaAdapter;
    /**
     * POJO Persona
     */
    private Persona personaFilter;
    /**
     * Adaptador para la lista de Direccion
     */
    private DireccionAdapter direccionAdapter;
    /**
     * POJO Dirección
     */
    private Direccion direccionFilter;
    /**
     * Adaptador para la lista de telefonos
     */
    private PersonaTelefonoAdapter adapterPersonaTelefono;
    /**
     * Persona Telefono
     */
    private PersonaTelefono personaTelefono;
    /**
     * Adapter Historico Liquidación
     */
    private HistoricoLiquidacionAdapter adapterLiquidacion;
    /**
     * Historico Liquidación
     */
    private HistoricoLiquidacion historicoLiquidacionFilter;
    /**
     * Bandera
     */
    private boolean showDetalleCreate;
    /**
     * Lista Detalle
     */
    private List<FacturaDetalle> listaDetalle;
    /**
     * Lista Selected Liquidación
     */
    private List<HistoricoLiquidacion> listSelectedLiquidacion = new ArrayList<>();
    /**
     * Mapa de Datos de Liquidación y nro Linea
     */
    private Map<Integer, Integer> liquidacionDetalle = new HashMap<Integer, Integer>();
    /**
     * POJO Monto Letras
     */
    private MontoLetras montoLetrasFilter;
    /**
     * Servicio Monto Letras
     */
    private MontoLetrasService serviceMontoLetras;
    /**
     * Producto Adaptador
     */
    private ProductoAdapter adapterProducto;
    /**
     * POJO Producto
     */
    private Producto producto;
    /**
     * POJO Producto
     */
    private Producto productoNuevo;
    /**
     * Service Factura
     */
    private FacturacionService serviceFactura;
    /**
     * Adaptador parala lista de cliente
     */
    private ClientPojoAdapter adapter;
    /**
     * Adaptador para la lista de moneda
     */
    private MonedaAdapter adapterMoneda;
    /**
     * POJO Moneda
     */
    private Moneda monedaFilter;
    /**
     * Identificador de Orden de compra
     */
    private String idOC;
    /**
     * Adaptador para la lista de orden de compra
     */
    private OrdenDeCompraAdapter adapterOrdenCompra;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompra;
    /**
     * Dato para digital
     */
    private String digital;
    /**
     * Dato para codigo de moneda
     */
    private String codMoneda;
    /**
     * Dato para habilitar panel de cotizacion
     */
    private boolean habilitarPanelCotizacion;
    /**
     * POJO Tipo Cambio
     */
    private TipoCambio tipoCambio;
    /**
     * Service tipo cambio
     */
    private TipoCambioService serviceTipoCambio;
    /**
     * Dato de nro de linea que se relaciona al producto
     */
    private Integer nroLineaProducto;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Descripción de producto
     */
    private String descripcion;
    
    /**
     * Lote de producto
     */
    private String loteProducto;
    
    /**
     * Vencimiento de lote de producto
     */
    private String fechaVencimientoLote;
    /**
     * Porcentaje
     */
    private Integer porcentaje;
    /**
     * Unidad Mínima de Venta
     */
    private BigDecimal umv;
    /**
     * POJO Producto Precio para parámetro
     */
    private ProductoPrecio parametroProductoPrecio;
    /**
     * Dato bandera para la parte de cuotas
     */
    private boolean plazo;
    /**
     * Dato bandera para la parte de cuotas
     */
    private boolean cuota;
    /**
     * Listado de detalles
     */
    private List<Cuota> detalleCuotas;
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    /**
     * Objeto Referencia Geografica
     */
    private ReferenciaGeografica referenciaGeoDepartamento;
    /**
     * Objeto Referencia Geografica Distrito
     */
    private ReferenciaGeografica referenciaGeoDistrito;
    /**
     * Objeto Referencia ciudad
     */
    private ReferenciaGeografica referenciaGeoCiudad;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeo;
    /**
     * Adaptador para la lista de configuracion valor
     */
    private ConfiguracionValorListAdapter adapterConfigValor;
    /**
     * POJO Configuración Valor
     */
    private ConfiguracionValor configuracionValorFilter;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showTalonarioPopup;
    /**
     * Adapter Factura Talonario
     */
    private FacturaTalonarioAdapter adapterFacturaTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo facturaTalonario;
    /**
     * Factura Talonario
     */
    private TalonarioPojo talonario;
    /**
     * Datos del usuario
     */
    @Inject
    private SessionData session;
    /**
     * Cliente para el servicio de descarga de archivos
     */
    private FileServiceClient fileServiceClient;
    /**
     * Bandera para panel de elección de talonario
     */
    private boolean showDescargaPopup;
    /**
     * Dato a ser utilizado para la descarga
     */
    private Integer idFacturaCreada;
    /**
     * Dato a ser utilizado para la descarga
     */
    private String nroFacturaCreada;
    /**
     * Gravada
     */
    private String gravada;
    /**
     * Dato Bandera
     */
    private String ctaCtble;
    /**
     * Nro Cta Ctble
     */
    private String nroCtaCtble;
    /**
     * Dato de numeración dinámica
     */
    private Boolean numeracionDinamica;
    /**
     * Servicio Orden de Compra
     */
    private OrdenDeCompraService serviceOrdenCompra;
    /**
     * POJO de Orden de Compra
     */
    private OrdenDeCompra ordenDeCompraCreate;
    /**
     * Bandera para generar OC
     */
    private String generarOc;
    /**
     * Bandera para modulo de inventario
     */
    private String moduloInventario;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoDistrito;
    /**
     * Adaptador de la Referencia geográfica
     */
    private ReferenciaGeograficaAdapter adapterRefGeoCiudad;
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    /**
     * Bandera para agregar campos de dirección
     */
    private String tieneDireccion;
    /**
     * Bandera para agregar campos de licitacion
     */
    private String licitacion;
    /**
     * Factura Dncp
     */
    private FacturaDncp facturaDncp;
    /*
     * Bandera para agregar campo de Orden de Compra
     */
    private String tieneOC;
    /**
     * Número de orden de compra
     */
    private String nroOC;
    private String notificar;
    private Boolean notificado;
    /**
     * Adapter de TipoTransaccion
     */
    private TipoTransaccionAdapter tipoTransaccionAdapter;
    private TipoCambioAdapter adapterTipoCambio;
    /**
     * ID de tipoCambio seleccionado
     */
    private Integer idTipoCambioActual;
    /**
     * Adaptador de locales 
     */
    private LocalListAdapter adapterLocal;
    
    private String multiplesLocales;
    
    private Integer idLocal;
    
    private String siediApi;
    
    private Integer tipoDescuento;
    
    private List<FacturaNotificacion> emailList;
    
    private String emailsNotificar;
    
    /**
     * Adaptador de la lista de tipo cobro
     */
    private TipoCobroListAdapter adapterTipoCobro;
    /**
     * Pojo tipo cobro
     */
    private TipoCobro tipoCobro;
    /**
     * Lista para tipo de cobro
     */
    private List<SelectItem> listTipoCobro = new ArrayList<>();
    /**
     * Pojo CobroDetalle
     */
    private CobroDetalle cobroDetalle;
    /**
     * Adaptador para la lista de entidad financiera
     */
    private EntidadFinancieraListAdapter adapterEntidadFinanciera;
    /**
     * Objeto Entidad Financiera
     */
    private EntidadFinanciera entidadFinanciera;
    
    /**
     * Pojo Cheque
     */
    private Cheque cheque;
    /**
     * Datos de cobro
     */
    private DatosCobro datoCobro;
    /**
     * Pojo de Cobro
     */
    private Cobro cobro;
        /**
     * Cobro Talonario
     */
    private TalonarioPojo cobroTalonario;
    /**
     * Adapter Cobro Talonario
     */
    private CobroTalonarioAdapter adapterCobroTalonario;
    /**
     * Cliente remoto para servicio de cobro
     */
    private CobroService cobroClient;
    /**
     * Talonario de recibo digital
     */
    private String reciboDigital;
    /**
     * Lista de detalles del cobro
     */
    private List<CobroDetalle> cobroDetalles;
    
    private BigDecimal montoCobroTotal = BigDecimal.ZERO;
    
    private BigDecimal montoCobroTotalGs = BigDecimal.ZERO;
    
    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">

    public Cobro getCobro() {
        return cobro;
    }

    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    
    public BigDecimal getMontoCobroTotal() {
        return montoCobroTotal;
    }

    public void setMontoCobroTotal(BigDecimal montoCobroTotal) {
        this.montoCobroTotal = montoCobroTotal;
    }

    public BigDecimal getMontoCobroTotalGs() {
        return montoCobroTotalGs;
    }

    public void setMontoCobroTotalGs(BigDecimal montoCobroTotalGs) {
        this.montoCobroTotalGs = montoCobroTotalGs;
    }

    
    
    public List<CobroDetalle> getCobroDetalles() {
        return cobroDetalles;
    }

    public void setCobroDetalles(List<CobroDetalle> cobroDetalles) {
        this.cobroDetalles = cobroDetalles;
    }

    public EntidadFinancieraListAdapter getAdapterEntidadFinanciera() {
        return adapterEntidadFinanciera;
    }

    public void setAdapterEntidadFinanciera(EntidadFinancieraListAdapter adapterEntidadFinanciera) {
        this.adapterEntidadFinanciera = adapterEntidadFinanciera;
    }

    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }
    

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public CobroDetalle getCobroDetalle() {
        return cobroDetalle;
    }

    public void setCobroDetalle(CobroDetalle cobroDetalle) {
        this.cobroDetalle = cobroDetalle;
    }

    public TipoCobro getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(TipoCobro tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    public List<SelectItem> getListTipoCobro() {
        return listTipoCobro;
    }

    public void setListTipoCobro(List<SelectItem> listTipoCobro) {
        this.listTipoCobro = listTipoCobro;
    }

    public TipoCobroListAdapter getAdapterTipoCobro() {
        return adapterTipoCobro;
    }

    public void setAdapterTipoCobro(TipoCobroListAdapter adapterTipoCobro) {
        this.adapterTipoCobro = adapterTipoCobro;
    }
    
    public String getEmailsNotificar() {
        return emailsNotificar;
    }

    public void setEmailsNotificar(String emailsNotificar) {
        this.emailsNotificar = emailsNotificar;
    }
    
    public List<FacturaNotificacion> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<FacturaNotificacion> emailList) {
        this.emailList = emailList;
    }

    public Integer getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(Integer tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }
    
    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public LocalListAdapter getAdapterLocal() {
        return adapterLocal;
    }

    public void setAdapterLocal(LocalListAdapter adapterLocal) {
        this.adapterLocal = adapterLocal;
    }

    public String getMultiplesLocales() {
        return multiplesLocales;
    }

    public void setMultiplesLocales(String multiplesLocales) {
        this.multiplesLocales = multiplesLocales;
    }

    public Integer getIdTipoCambioActual() {
        return idTipoCambioActual;
    }

    public void setIdTipoCambioActual(Integer idTipoCambioActual) {
        this.idTipoCambioActual = idTipoCambioActual;
    }

    public TipoCambioAdapter getAdapterTipoCambio() {
        return adapterTipoCambio;
    }

    public void setAdapterTipoCambio(TipoCambioAdapter adapterTipoCambio) {
        this.adapterTipoCambio = adapterTipoCambio;
    }
    
    
    public void setServiceFactura(FacturacionService serviceFactura) {
        this.serviceFactura = serviceFactura;
    }

    public void setTieneDireccion(String tieneDireccion) {
        this.tieneDireccion = tieneDireccion;
    }

    public String getTieneDireccion() {
        return tieneDireccion;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setAdapterRefGeoDistrito(ReferenciaGeograficaAdapter adapterRefGeoDistrito) {
        this.adapterRefGeoDistrito = adapterRefGeoDistrito;
    }

    public void setAdapterRefGeoCiudad(ReferenciaGeograficaAdapter adapterRefGeoCiudad) {
        this.adapterRefGeoCiudad = adapterRefGeoCiudad;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoDistrito() {
        return adapterRefGeoDistrito;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeoCiudad() {
        return adapterRefGeoCiudad;
    }

    public void setModuloInventario(String moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public String getModuloInventario() {
        return moduloInventario;
    }

    public void setGenerarOc(String generarOc) {
        this.generarOc = generarOc;
    }

    public String getGenerarOc() {
        return generarOc;
    }

    public void setServiceOrdenCompra(OrdenDeCompraService serviceOrdenCompra) {
        this.serviceOrdenCompra = serviceOrdenCompra;
    }

    public OrdenDeCompraService getServiceOrdenCompra() {
        return serviceOrdenCompra;
    }

    public void setOrdenDeCompraCreate(OrdenDeCompra ordenDeCompraCreate) {
        this.ordenDeCompraCreate = ordenDeCompraCreate;
    }

    public OrdenDeCompra getOrdenDeCompraCreate() {
        return ordenDeCompraCreate;
    }

    public Boolean getNumeracionDinamica() {
        return numeracionDinamica;
    }

    public void setNumeracionDinamica(Boolean numeracionDinamica) {
        this.numeracionDinamica = numeracionDinamica;
    }

    public void setNroCtaCtble(String nroCtaCtble) {
        this.nroCtaCtble = nroCtaCtble;
    }

    public String getNroCtaCtble() {
        return nroCtaCtble;
    }

    public void setCtaCtble(String ctaCtble) {
        this.ctaCtble = ctaCtble;
    }

    public String getCtaCtble() {
        return ctaCtble;
    }

    public void setGravada(String gravada) {
        this.gravada = gravada;
    }

    public String getGravada() {
        return gravada;
    }

    public void setNroFacturaCreada(String nroFacturaCreada) {
        this.nroFacturaCreada = nroFacturaCreada;
    }

    public String getNroFacturaCreada() {
        return nroFacturaCreada;
    }

    public void setIdFacturaCreada(Integer idFacturaCreada) {
        this.idFacturaCreada = idFacturaCreada;
    }

    public Integer getIdFacturaCreada() {
        return idFacturaCreada;
    }

    public void setShowDescargaPopup(boolean showDescargaPopup) {
        this.showDescargaPopup = showDescargaPopup;
    }

    public boolean isShowDescargaPopup() {
        return showDescargaPopup;
    }

    public void setFileServiceClient(FileServiceClient fileServiceClient) {
        this.fileServiceClient = fileServiceClient;
    }

    public FileServiceClient getFileServiceClient() {
        return fileServiceClient;
    }

    public void setTalonario(TalonarioPojo talonario) {
        this.talonario = talonario;
    }

    public TalonarioPojo getTalonario() {
        return talonario;
    }

    public FacturaTalonarioAdapter getAdapterFacturaTalonario() {
        return adapterFacturaTalonario;
    }

    public void setAdapterFacturaTalonario(FacturaTalonarioAdapter adapterFacturaTalonario) {
        this.adapterFacturaTalonario = adapterFacturaTalonario;
    }

    public void setFacturaTalonario(TalonarioPojo facturaTalonario) {
        this.facturaTalonario = facturaTalonario;
    }

    public TalonarioPojo getFacturaTalonario() {
        return facturaTalonario;
    }

    public void setShowTalonarioPopup(boolean showTalonarioPopup) {
        this.showTalonarioPopup = showTalonarioPopup;
    }

    public boolean isShowTalonarioPopup() {
        return showTalonarioPopup;
    }

    public void setConfiguracionValorFilter(ConfiguracionValor configuracionValorFilter) {
        this.configuracionValorFilter = configuracionValorFilter;
    }

    public void setAdapterConfigValor(ConfiguracionValorListAdapter adapterConfigValor) {
        this.adapterConfigValor = adapterConfigValor;
    }

    public ConfiguracionValor getConfiguracionValorFilter() {
        return configuracionValorFilter;
    }

    public ConfiguracionValorListAdapter getAdapterConfigValor() {
        return adapterConfigValor;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setCuota(boolean cuota) {
        this.cuota = cuota;
    }

    public boolean isCuota() {
        return cuota;
    }

    public void setDetalleCuotas(List<Cuota> detalleCuotas) {
        this.detalleCuotas = detalleCuotas;
    }

    public List<Cuota> getDetalleCuotas() {
        return detalleCuotas;
    }

    public void setPlazo(boolean plazo) {
        this.plazo = plazo;
    }

    public boolean isPlazo() {
        return plazo;
    }

    public void setReferenciaGeoDistrito(ReferenciaGeografica referenciaGeoDistrito) {
        this.referenciaGeoDistrito = referenciaGeoDistrito;
    }

    public void setReferenciaGeoDepartamento(ReferenciaGeografica referenciaGeoDepartamento) {
        this.referenciaGeoDepartamento = referenciaGeoDepartamento;
    }

    public void setReferenciaGeoCiudad(ReferenciaGeografica referenciaGeoCiudad) {
        this.referenciaGeoCiudad = referenciaGeoCiudad;
    }

    public void setAdapterRefGeo(ReferenciaGeograficaAdapter adapterRefGeo) {
        this.adapterRefGeo = adapterRefGeo;
    }

    public ReferenciaGeografica getReferenciaGeoDistrito() {
        return referenciaGeoDistrito;
    }

    public ReferenciaGeografica getReferenciaGeoDepartamento() {
        return referenciaGeoDepartamento;
    }

    public ReferenciaGeografica getReferenciaGeoCiudad() {
        return referenciaGeoCiudad;
    }

    public ReferenciaGeograficaAdapter getAdapterRefGeo() {
        return adapterRefGeo;
    }

    public void setParametroProductoPrecio(ProductoPrecio parametroProductoPrecio) {
        this.parametroProductoPrecio = parametroProductoPrecio;
    }

    public ProductoPrecio getParametroProductoPrecio() {
        return parametroProductoPrecio;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setProductoNuevo(Producto productoNuevo) {
        this.productoNuevo = productoNuevo;
    }

    public Producto getProductoNuevo() {
        return productoNuevo;
    }

    public void setNroLineaProducto(Integer nroLineaProducto) {
        this.nroLineaProducto = nroLineaProducto;
    }

    public Integer getNroLineaProducto() {
        return nroLineaProducto;
    }

    public void setServiceTipoCambio(TipoCambioService serviceTipoCambio) {
        this.serviceTipoCambio = serviceTipoCambio;
    }

    public TipoCambioService getServiceTipoCambio() {
        return serviceTipoCambio;
    }

    public void setTipoCambio(TipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public TipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setHabilitarPanelCotizacion(boolean habilitarPanelCotizacion) {
        this.habilitarPanelCotizacion = habilitarPanelCotizacion;
    }

    public void setCodMoneda(String codMoneda) {
        this.codMoneda = codMoneda;
    }

    public boolean isHabilitarPanelCotizacion() {
        return habilitarPanelCotizacion;
    }

    public String getCodMoneda() {
        return codMoneda;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getDigital() {
        return digital;
    }

    public OrdenDeCompraAdapter getAdapterOrdenCompra() {
        return adapterOrdenCompra;
    }

    public OrdenDeCompra getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void setAdapterOrdenCompra(OrdenDeCompraAdapter adapterOrdenCompra) {
        this.adapterOrdenCompra = adapterOrdenCompra;
    }

    public void setOrdenDeCompra(OrdenDeCompra ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    public void setIdOC(String idOC) {
        this.idOC = idOC;
    }

    public String getIdOC() {
        return idOC;
    }

    public void setMonedaFilter(Moneda monedaFilter) {
        this.monedaFilter = monedaFilter;
    }

    public void setAdapterMoneda(MonedaAdapter adapterMoneda) {
        this.adapterMoneda = adapterMoneda;
    }

    public Moneda getMonedaFilter() {
        return monedaFilter;
    }

    public MonedaAdapter getAdapterMoneda() {
        return adapterMoneda;
    }

    public void setAdapter(ClientPojoAdapter adapter) {
        this.adapter = adapter;
    }

    public ClientPojoAdapter getAdapter() {
        return adapter;
    }

    public void setClienteFactura(Cliente clienteFactura) {
        this.clienteFactura = clienteFactura;
    }

    public Cliente getClienteFactura() {
        return clienteFactura;
    }

    public FacturacionService getServiceFactura() {
        return serviceFactura;
    }

    public void setAdapterProducto(ProductoAdapter adapterProducto) {
        this.adapterProducto = adapterProducto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public ProductoAdapter getAdapterProducto() {
        return adapterProducto;
    }

    public void setServiceMontoLetras(MontoLetrasService serviceMontoLetras) {
        this.serviceMontoLetras = serviceMontoLetras;
    }

    public void setMontoLetrasFilter(MontoLetras montoLetrasFilter) {
        this.montoLetrasFilter = montoLetrasFilter;
    }

    public MontoLetrasService getServiceMontoLetras() {
        return serviceMontoLetras;
    }

    public MontoLetras getMontoLetrasFilter() {
        return montoLetrasFilter;
    }

    public void setListaDetalle(List<FacturaDetalle> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public List<FacturaDetalle> getListaDetalle() {
        return listaDetalle;
    }

    public Map<Integer, Integer> getLiquidacionDetalle() {
        return liquidacionDetalle;
    }

    public void setLiquidacionDetalle(Map<Integer, Integer> liquidacionDetalle) {
        this.liquidacionDetalle = liquidacionDetalle;
    }

    public void setLinea(Integer linea) {
        this.linea = linea;
    }

    public Integer getLinea() {
        return linea;
    }

    public void setListSelectedLiquidacion(List<HistoricoLiquidacion> listSelectedLiquidacion) {
        this.listSelectedLiquidacion = listSelectedLiquidacion;
    }

    public List<HistoricoLiquidacion> getListSelectedLiquidacion() {
        return listSelectedLiquidacion;
    }

    public void setShowDetalleCreate(boolean showDetalleCreate) {
        this.showDetalleCreate = showDetalleCreate;
    }

    public boolean isShowDetalleCreate() {
        return showDetalleCreate;
    }

    public void setHistoricoLiquidacionFilter(HistoricoLiquidacion historicoLiquidacionFilter) {
        this.historicoLiquidacionFilter = historicoLiquidacionFilter;
    }

    public void setAdapterLiquidacion(HistoricoLiquidacionAdapter adapterLiquidacion) {
        this.adapterLiquidacion = adapterLiquidacion;
    }

    public HistoricoLiquidacion getHistoricoLiquidacionFilter() {
        return historicoLiquidacionFilter;
    }

    public HistoricoLiquidacionAdapter getAdapterLiquidacion() {
        return adapterLiquidacion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public void setAdapterPersonaTelefono(PersonaTelefonoAdapter adapterPersonaTelefono) {
        this.adapterPersonaTelefono = adapterPersonaTelefono;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public PersonaTelefonoAdapter getAdapterPersonaTelefono() {
        return adapterPersonaTelefono;
    }

    public void setPersonaFilter(Persona personaFilter) {
        this.personaFilter = personaFilter;
    }

    public void setPersonaAdapter(PersonaListAdapter personaAdapter) {
        this.personaAdapter = personaAdapter;
    }

    public void setDireccionFilter(Direccion direccionFilter) {
        this.direccionFilter = direccionFilter;
    }

    public void setDireccionAdapter(DireccionAdapter direccionAdapter) {
        this.direccionAdapter = direccionAdapter;
    }

    public Persona getPersonaFilter() {
        return personaFilter;
    }

    public PersonaListAdapter getPersonaAdapter() {
        return personaAdapter;
    }

    public Direccion getDireccionFilter() {
        return direccionFilter;
    }

    public DireccionAdapter getDireccionAdapter() {
        return direccionAdapter;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setFacturaCreate(Factura facturaCreate) {
        this.facturaCreate = facturaCreate;
    }

    public Factura getFacturaCreate() {
        return facturaCreate;
    }

    public void setFacturaFilterCancelada(Factura facturaFilterCancelada) {
        this.facturaFilterCancelada = facturaFilterCancelada;
    }

    public void setAdapterFacturaCancelada(FacturaAdapter adapterFacturaCancelada) {
        this.adapterFacturaCancelada = adapterFacturaCancelada;
    }

    public Factura getFacturaFilterCancelada() {
        return facturaFilterCancelada;
    }

    public FacturaAdapter getAdapterFacturaCancelada() {
        return adapterFacturaCancelada;
    }

    public void setFacturaFilter(Factura facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public Factura getFacturaFilter() {
        return facturaFilter;
    }

    public void setAdapterFactura(FacturaAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public Cliente getClienteFilterPrueba() {
        return clienteFilterPrueba;
    }

    public void setClienteFilterPrueba(Cliente clienteFilterPrueba) {
        this.clienteFilterPrueba = clienteFilterPrueba;
    }

    public void setAdapterClientePrueba2(ClientListAdapter adapterClientePrueba2) {
        this.adapterClientePrueba2 = adapterClientePrueba2;
    }

    public ClientListAdapter getAdapterClientePrueba2() {
        return adapterClientePrueba2;
    }

    public void setAdapterCliente(ClientListAdapter adapterCliente) {
        this.adapterCliente = adapterCliente;
    }

    public void setServiceFacturacion(FacturacionService serviceFacturacion) {
        this.serviceFacturacion = serviceFacturacion;
    }

    public void setDatoFactura(DatosFactura datoFactura) {
        this.datoFactura = datoFactura;
    }

    public FacturacionService getServiceFacturacion() {
        return serviceFacturacion;
    }

    public DatosFactura getDatoFactura() {
        return datoFactura;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }

    public ClientListAdapter getAdapterCliente() {
        return adapterCliente;
    }

    public void setCliente(ClientePojo cliente) {
        this.cliente = cliente;
    }

    public ClientePojo getCliente() {
        return cliente;
    }

    public String getLicitacion() {
        return licitacion;
    }

    public void setLicitacion(String licitacion) {
        this.licitacion = licitacion;
    }

    public FacturaDncp getFacturaDncp() {
        return facturaDncp;
    }

    public void setFacturaDncp(FacturaDncp facturaDncp) {
        this.facturaDncp = facturaDncp;
    }

    public String getTieneOC() {
        return tieneOC;
    }

    public void setTieneOC(String tieneOC) {
        this.tieneOC = tieneOC;
    }
    
    public String getNroOC() {
        return nroOC;
    }

    public void setNroOC(String nroOC) {
        this.nroOC = nroOC;
    }

    public TipoTransaccionAdapter getTipoTransaccionAdapter() {
        return tipoTransaccionAdapter;
    }

    public void setTipoTransaccionAdapter(TipoTransaccionAdapter tipoTransaccionAdapter) {
        this.tipoTransaccionAdapter = tipoTransaccionAdapter;
    }

//</editor-fold>    
    
    /**
     * Método para obtener el cliente
     *
     * @param event
     */
    public void onItemSelectClienteFacturaCrear(SelectEvent event) {
        facturaCreate.setIdCliente(((ClientePojo) event.getObject()).getIdCliente());  
        String nroDoc = ((ClientePojo) event.getObject()).getNroDocumento();
        if (nroDoc != null){
            nroDoc = nroDoc.trim().replace(" ", "").replace(".","");
        }
        facturaCreate.setRuc(nroDoc);
        facturaCreate.setRazonSocial(((ClientePojo) event.getObject()).getRazonSocial());
        
        if (((ClientePojo) event.getObject()).getIdNaturalezaCliente() != null) {
            facturaCreate.setIdNaturalezaCliente(((ClientePojo) event.getObject()).getIdNaturalezaCliente());
            }

        parametroProductoPrecio.setIdCliente(facturaCreate.getIdCliente());
        idCliente = facturaCreate.getIdCliente();
        if (((ClientePojo) event.getObject()).getIdCanalVenta() == null) {
            parametroProductoPrecio.setIdCanalVenta(0);
        } else {
            parametroProductoPrecio.setIdCanalVenta(((ClientePojo) event.getObject()).getIdCanalVenta());
        }
        PF.current().executeScript("$('#modalCliente').modal('hide');");
        obtenerDatosFacturacion();
        
        obtenerMails(((ClientePojo) event.getObject()).getIdCliente());
        consultarLocales();
    }
    
    

    /**
     * Método para obtener la configuración
     */
    public void obtenerConfiguracion() {
        configuracionValorFilter.setCodigoConfiguracion("TIPO_TALONARIO_FACTURA_DIGITAL");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            digital = "N";
        } else {
            digital = adapterConfigValor.getData().get(0).getValor();
        }
        
        obtenerDatosTalonario();
    }

    /**
     * Método para obtener la configuración
     */
    public String obtenerConfiguracionGeneral(String codigo) {
        String descargar = null;
        ConfiguracionValor confVal = new ConfiguracionValor();
        ConfiguracionValorListAdapter a = new ConfiguracionValorListAdapter();

        confVal.setCodigoConfiguracion(codigo);
        confVal.setActivo("S");
        a = a.fillData(confVal);

        if (a.getData() == null || a.getData().isEmpty()) {
            descargar = "N";
        } else {
            descargar = a.getData().get(0).getValor();
        }

        return descargar;

    }

    /**
     * Método para obtener datos de factura
     */
    public void obtenerDatosFacturacion() {

        try {
            String emptyString = null;
            Integer emptyInteger = null;
            Map parametros = new HashMap();
            datoFactura = new DatosFactura();
            Calendar today = Calendar.getInstance();
            direccionFilter = new Direccion();
            referenciaGeoDepartamento = new ReferenciaGeografica();
            referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoCiudad = new ReferenciaGeografica();
            facturaCreate.setTelefono(emptyString);
            facturaCreate.setEmail(emptyString);
            facturaCreate.setDireccion(emptyString);
            facturaCreate.setIdDepartamento(emptyInteger);
            facturaCreate.setIdDistrito(emptyInteger);
            facturaCreate.setIdCiudad(emptyInteger);
            facturaCreate.setNroCasa(emptyString);

            parametros.put("digital", digital);
            if (idCliente != null) {
                parametros.put("idCliente", idCliente);
            }

            if (talonario.getId() != null) {
                parametros.put("id", talonario.getId());
            }

            datoFactura = serviceFacturacion.getDatosFactura(parametros);
            if (datoFactura != null) {
                facturaCreate.setFecha(today.getTime());
                facturaCreate.setFechaVencimiento(today.getTime());

                facturaCreate.setNroFactura(datoFactura.getNroFactura());

                //Validaciones
                if (datoFactura.getTelefono() != null) {
                    facturaCreate.setTelefono(datoFactura.getTelefono());
                }
                if (datoFactura.getEmail() != null) {
                    facturaCreate.setEmail(datoFactura.getEmail().trim());
                }
                if (datoFactura.getDireccion() != null) {
                    facturaCreate.setDireccion(datoFactura.getDireccion());
                }
                if (datoFactura.getIdDepartamento() != null) {
                    facturaCreate.setIdDepartamento(datoFactura.getIdDepartamento());
                    direccionFilter.setIdDepartamento(datoFactura.getIdDepartamento());
                    referenciaGeoDepartamento = obtenerReferencia(datoFactura.getIdDepartamento());
                    idDepartamento = direccionFilter.getIdDepartamento();
                    filterDistrito();
                }
                if (datoFactura.getIdDistrito() != null) {
                    facturaCreate.setIdDistrito(datoFactura.getIdDistrito());
                    direccionFilter.setIdDistrito(datoFactura.getIdDistrito());
                    referenciaGeoDistrito = obtenerReferencia(datoFactura.getIdDistrito());
                    idDistrito = direccionFilter.getIdDistrito();
                    filterCiudad();
                }
                if (datoFactura.getIdCiudad() != null) {
                    facturaCreate.setIdCiudad(datoFactura.getIdCiudad());
                    direccionFilter.setIdCiudad(datoFactura.getIdCiudad());
                    referenciaGeoCiudad = obtenerReferencia(datoFactura.getIdCiudad());
                    idCiudad = direccionFilter.getIdCiudad();
                }
                if (datoFactura.getNroCasa() != null) {
                    facturaCreate.setNroCasa(datoFactura.getNroCasa());
                }

                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario disponible"));
                facturaCreate.setFecha(today.getTime());
                facturaCreate.setFechaVencimiento(today.getTime());
            }
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Método para obtener datos de talonario
     */
    public void obtenerDatosTalonario() {
        facturaTalonario.setDigital(digital);
        facturaTalonario.setIdEncargado(session.getUser().getIdPersonaFisica());
        adapterFacturaTalonario = adapterFacturaTalonario.fillData(facturaTalonario);

        if (adapterFacturaTalonario.getData().size() > 1) {
            showTalonarioPopup = true;
            PF.current().ajax().update(":list-factura-create-form:form-data:pnl-pop-up");
        } else {
            if (!adapterFacturaTalonario.getData().isEmpty()) {
                talonario.setId(adapterFacturaTalonario.getData().get(0).getId());
            }
            obtenerDatosFacturacion();
        }
    }
    
    /**
     * Metodo para obtener datos de la factura creada
     * @param idFactura 
     */
    public void obtenerDatosFactura(Integer idFactura){
        Factura filtro = new Factura();
        filtro.setId(idFactura);
        adapterFactura = adapterFactura.fillData(filtro);
        facturaCreate = adapterFactura.getData().get(0);
        nroFacturaCreada = facturaCreate.getNroFactura();
     }

    /**
     * Método para filtrar los datos factura
     */
    public void filterDatosFactura() {
        Map parametros = new HashMap();
        DatosFactura df = new DatosFactura();

        df = serviceFacturacion.getDatosFactura(parametros);

        facturaCreate.setDiasCredito(df.getDiasCredito());
        facturaCreate.setFechaVencimiento(df.getFechaVencimiento());
    }

    /**
     * Método para crear la factura
     */
    public void crearFactura() {
        boolean saveFactura = true;
        if (facturaCreate.getIdCliente() == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el cliente!"));
            saveFactura = false;
        }

        if (tieneDireccion.equals("S")) {
            if (facturaCreate.getDireccion() == null || facturaCreate.getDireccion().trim().isEmpty()) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la dirección!"));
            }

            if (facturaCreate.getNroCasa() == null || facturaCreate.getNroCasa().trim().isEmpty()) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el N° Casa!"));
            }
            
            
            if (idOC != null ||(cliente != null && cliente.getIdNaturalezaCliente() != 3)){
                if (idDepartamento == null) {
                    saveFactura = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Departamento!"));
                } else {
                    facturaCreate.setIdDepartamento(idDepartamento);
                }

                if (idDistrito == null) {
                    saveFactura = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar el Distrito!"));
                } else {
                    facturaCreate.setIdDistrito(idDistrito);
                }

                if (idCiudad == null) {
                    saveFactura = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe indicar la Ciudad!"));
                } else {
                    facturaCreate.setIdCiudad(idCiudad);
                }
            } else {
                facturaCreate.setIdDepartamento(null);
                facturaCreate.setIdDistrito(null);
                facturaCreate.setIdCiudad(null);
            }

        } else {
            facturaCreate.setDireccion(null);
            facturaCreate.setNroCasa(null);
            facturaCreate.setIdDepartamento(null);
            facturaCreate.setIdDistrito(null);
            facturaCreate.setIdCiudad(null);
        }
        
        if ((cliente != null) && cliente.getIdNaturalezaCliente() == 3 && ((facturaCreate.getDireccion() == null) || (facturaCreate.getNroCasa() == null) )) {
            saveFactura = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar una dirección y Nro. de casa!"));
        }
        
        if(licitacion.equals("S")){
            if (facturaDncp.getModalidad() == null || facturaDncp.getModalidad().trim().isEmpty()) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la modalidad!"));
            }
            
            if (facturaDncp.getEntidad() == null || facturaDncp.getEntidad().trim().isEmpty()) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la entidad!"));
            }
            
            if (facturaDncp.getAnho() == null || facturaDncp.getAnho().trim().isEmpty()) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el Año!"));
            }
                     
            if (facturaDncp.getFechaEmision()== null) {
                saveFactura = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la fecha de emisión !"));
            }
            
            if (saveFactura){
                facturaCreate.setCompraPublica(licitacion);
                facturaCreate.setFacturaDncp(facturaDncp);
            }
        }
        else{
            facturaCreate.setCompraPublica(licitacion);
            facturaCreate.setFacturaDncp(null);
        }

        if (listaDetalle.isEmpty() || listaDetalle == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe agregar al menos un detalle de factura!"));
            saveFactura = false;
        } else {
            for (int i = 0; i < listaDetalle.size(); i++) {
                int dn = i + 1;

                if (listaDetalle.get(i).getPrecioUnitarioConIva() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el precio unitario con IVA en el detalle N° " + dn));
                    saveFactura = false;
                }

                if (listaDetalle.get(i).getCantidad() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar la cantidad en el detalle N° " + dn));
                    saveFactura = false;
                }
                
                if (licitacion.contains("S")){
                    if(listaDetalle.get(i).getCodDncpNivelGeneral() == null || listaDetalle.get(i).getCodDncpNivelGeneral().trim().isEmpty()){
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el código DNCP nivel general en el detalle N° " + dn));
                        saveFactura = false;
                    }
                    
                    if(listaDetalle.get(i).getCodDncpNivelEspecifico() == null || listaDetalle.get(i).getCodDncpNivelEspecifico().trim().isEmpty()){
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el código DNCP nivel específico en el detalle N° " + dn));
                        saveFactura = false;
                    }
                    
                }
                else{
                    listaDetalle.get(i).setCodDncpNivelGeneral(null);
                    listaDetalle.get(i).setCodDncpNivelEspecifico(null);
                }

            }
        }
        
        if (facturaCreate.idTipoFactura == 2) {
            if (cobroDetalles == null || cobroDetalles.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se debe ingresar el medio de pago"));
                saveFactura = false;
            } else {
                if (cobroDetalle != null && !cobroDetalles.isEmpty()){
                    BigDecimal temp ;
                    if (this.codMoneda.equalsIgnoreCase("PYG")) {
                            temp = facturaCreate.getMontoTotalFactura();
                            temp = temp.subtract(montoCobroTotal);
                        if (temp.compareTo(new BigDecimal("-1")) < 0){
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto total del cobro no puede ser mayor al total de la factura"));
                            saveFactura = false;
                        }
                    } else {
                            temp = facturaCreate.getMontoTotalGuaranies();
                            temp = temp.subtract(montoCobroTotalGs);
                        if (temp.compareTo(new BigDecimal("-1")) < 0){
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto total del cobro no puede ser mayor al total de la factura"));
                            saveFactura = false;
                        }
                    }
                }
            }
        }
        
        if(tieneOC.equals("S") && (nroOC == null || nroOC.trim().isEmpty())){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Debe ingresar el número de Orden de Compra " ));
            saveFactura = false;
        }
        
        if (facturaCreate.getEmail() != null) {
             facturaCreate.setEmail(
                facturaCreate.getEmail().replaceAll("\\s", "").isEmpty() ?
                null :
                facturaCreate.getEmail().replaceAll("\\s", "")
            );
        }
        
        if (emailList != null && !emailList.isEmpty()){
            for (FacturaNotificacion fn : emailList){
                if (fn.getEmail().trim().equalsIgnoreCase("")){
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No puede ingresar un Email vacío");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    saveFactura = false;
                    break;
                } else if (!validarFormatoCorreo(fn.getEmail())) {
                    saveFactura = false;
                }
            }
        }
        
        if (saveFactura == true) {
            calcularTotalesGenerales();
            facturaCreate.setIdTalonario(datoFactura.getIdTalonario());
            if (!emailList.isEmpty()){
               if (emailList.size()>1){
                   facturaCreate.setFacturaNotificaciones(emailList); 
                   facturaCreate.setEmail(null);
               } else {
                   facturaCreate.setEmail(emailList.get(0).getEmail());
               }
            }
            facturaCreate.setAnulado("N");
            facturaCreate.setImpreso("N");
            facturaCreate.setDigital(digital);
            facturaCreate.setNroFactura(datoFactura.getNroFactura());
            if ((digital != null && digital.equalsIgnoreCase("S"))
                    || notificar.equalsIgnoreCase("S")
                    || siediApi.equalsIgnoreCase("S")) {
                facturaCreate.setEntregado("P");
            } else {
                facturaCreate.setEntregado("N");
            }
            if (digital.equals("S")) {
                facturaCreate.setArchivoSet("S");
            } else {
                facturaCreate.setArchivoSet("N");
                facturaCreate.setIdTipoTransaccion(null);
            }

            for (int i = 0; i < listaDetalle.size(); i++) {
                listaDetalle.get(i).setNroLinea(i + 1);
            }
            facturaCreate.setFacturaDetalles(listaDetalle);
            
            if (facturaCreate.idTipoFactura == 2){
                facturaCreate.setCobrado("S");
                if (!cobroDetalles.isEmpty()){
                    generarCobro();
                }
            } 
            
            if (plazo == true) {
                facturaCreate.setCodigoTipoOperacionCredito("PLAZO");
                facturaCreate.setCantidadCuotas(null);
            }
            if (cuota == true) {
                facturaCreate.setCodigoTipoOperacionCredito("CUOTA");
                facturaCreate.setFacturaCuotas(detalleCuotas);
            }

            if ((moduloInventario.equalsIgnoreCase("S") && idOC == null ) || tieneOC.equalsIgnoreCase("S")) {
                facturaCreate.setOrdenCompra(generarOC(facturaCreate));
            }

            if (idOC != null && !idOC.trim().isEmpty()) {
                editarOC(facturaCreate);
            } else {
                WebLogger.get().debug("SE ESTA POR GENERAR LA FACTURA");
                generarFactura();
            }

        }

    }
    
    /**
     * Método para obtener todos los mails registrados del cliente
     * @param idCliente 
     */
    public void obtenerMails(Integer idCliente){
        emailList = new ArrayList();
        String emails = "";
        PersonaEmailAdapter adapterPersonaEmail = new PersonaEmailAdapter();
        List<PersonaEmail> listaEmails = new ArrayList();
        PersonaEmail personaEmail = new PersonaEmail();
        personaEmail.setIdPersona(idCliente);
        personaEmail.setActivo("S");
        adapterPersonaEmail = adapterPersonaEmail.fillData(personaEmail);
        listaEmails = adapterPersonaEmail.getData();
        
        if (!listaEmails.isEmpty()) {
            for (PersonaEmail pe : listaEmails) {
                FacturaNotificacion fn = new FacturaNotificacion();
                fn.setEmail(pe.getEmail().getEmail().replaceAll("\\s", ""));
                emails = emails.format("%s;%s",emails,pe.getEmail().getEmail().replaceAll("\\s", ""));
                fn.setIdTipoNotificacion(1);
               emailList.add(fn);
            }
        } 
        emailsNotificar = emails;
    }
    
    
    /**
     * Método para validar el formato y duplicados de emails
     * @param email 
     */
    public void validarCorreo(String email) {
        Integer count = 0;
        if (validarFormatoCorreo(email)){
            for (FacturaNotificacion fn : emailList){
                if (fn.getEmail().equalsIgnoreCase(email)) {
                    count =+1;
                }
                if (count > 1){
                    FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se han cargado emails duplicados");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    break;
                }
            }
        }
        actualizarVistaEmails();
    }
    
    
     public boolean validarFormatoCorreo(String email) {
         boolean addEmail = false;
         
        email = email.trim().replace(" ", "");
        // Patrón para validar el email
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+(\\.[a-zA-Z]{2,})?$");

        // El email a validar
        String correo = email;

        Matcher mather = pattern.matcher(correo);

        if (mather.find() == true) {
            addEmail = true;
            WebLogger.get().debug("El email ingresado es válido.");

        } else {
            addEmail = false;
            WebLogger.get().debug("El email ingresado es inválido.");
            String msg = String.format("%s no es un email válido", email);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        return addEmail;
    }
     
    
    public void addEmailNotificacion(){
        FacturaNotificacion fn = new FacturaNotificacion();
        fn.setIdTipoNotificacion(1); // TIPO NOTIFICAICION = APROBACION_DTE
        if(emailList != null){
            emailList.add(fn);
        }
    }
    
    public void deleteEmailNotificacion(FacturaNotificacion fn) {
        if(emailList != null && !emailList.isEmpty()){
            emailList.remove(fn);
        }
        actualizarVistaEmails();
    }
   
    /**
     * Actualiza la vista previa de emails separados por ;
     */
    public void actualizarVistaEmails(){
        String emails = "";
        if (emailList != null && !emailList.isEmpty()){
            for (FacturaNotificacion fn : emailList) {
                String email = fn.getEmail().replaceAll("\\s", "");
                emails = String.format("%s;%s",emails,email);
            }
        }
        emailsNotificar = emails;
    }
    
    public void generarFactura() {
        WebLogger.get().debug("GENERANDO FACTURA");
        BodyResponse<Factura> respuestaDeFactura = serviceFactura.createFactura(facturaCreate);

        if (respuestaDeFactura.getSuccess()) {

            Factura f = new Factura();
            f = (Factura) respuestaDeFactura.getPayload();
            idFacturaCreada = f.getId();
            nroFacturaCreada = f.getNroFactura();

            if (obtenerConfiguracionGeneral("DESCARGAR_FACTURA").equalsIgnoreCase("S")) {
                mostrarPanelDescarga();
            }
            
//            if (notificar.equalsIgnoreCase("S")) {
//                this.notificado = false;
//                notificar(facturaCreate.getEmail());
//                if (notificado){
//                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Se ha enviado la factura a la dirección confirmada"));
//                }
//                else {
//                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Ocurrió un error al enviar la notificación. Verifique la configuración."));
//                }
//            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Factura N° " + nroFacturaCreada + " creada correctamente!"));
            try {
                GeneracionDteFacturaMultiempresa.generar(f.getIdEmpresa(), idFacturaCreada);
            } catch (Exception e){
                WebLogger.get().fatal(e);
            }
            
            clearForm();
            
           
        }
    }

    /**
     * Método para editar la OC
     *
     * @param facturaGenerada factura creada
     */
    public void editarOC(Factura facturaGenerada) {
        this.adapterOrdenCompra = new OrdenDeCompraAdapter();
        this.ordenDeCompra = new OrdenDeCompra();
        this.ordenDeCompra.setId(Integer.parseInt(idOC));
        this.adapterOrdenCompra = this.adapterOrdenCompra.fillData(ordenDeCompra);
        OrdenDeCompra ocEdit = new OrdenDeCompra();
        ocEdit = adapterOrdenCompra.getData().get(0);

        for (FacturaDetalle facturaDetalle : facturaGenerada.getFacturaDetalles()) {
            boolean existe = false;
            for (OrdenCompraDetalles ordenCompraDetalle : ocEdit.getOrdenCompraDetalles()) {
                if (ordenCompraDetalle.getIdProducto().equals(facturaDetalle.getIdProducto())) {
                    existe = true;
                    ordenCompraDetalle.setCantidadConfirmada(facturaDetalle.getCantidad());
                }
            }

            if (existe == false) {
                OrdenCompraDetalles ocDetalle = new OrdenCompraDetalles();
                ocDetalle.setIdOrdenCompra(Integer.parseInt(idOC));
                ocDetalle.setIdProducto(facturaDetalle.getIdProducto());
                ocDetalle.setPrecioUnitarioConIva(facturaDetalle.getPrecioUnitarioConIva());
                ocDetalle.setCantidad(facturaDetalle.getCantidad());
                ocDetalle.setCantidadSolicitada(BigDecimal.ZERO);
                ocDetalle.setCantidadConfirmada(facturaDetalle.getCantidad());
                ocDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
                ocDetalle.setPrecioUnitarioSinIva(facturaDetalle.getPrecioUnitarioSinIva());
                ocDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
                ocDetalle.setMontoIva(facturaDetalle.getMontoIva());
                ocDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
                ocEdit.getOrdenCompraDetalles().add(ocDetalle);
            }
        }

        for (OrdenCompraDetalles ordenCompraDetalle : ocEdit.getOrdenCompraDetalles()) {
            boolean existe2 = false;
            for (FacturaDetalle facturaDetalle : facturaGenerada.getFacturaDetalles()) {
                if (ordenCompraDetalle.getIdProducto().equals(facturaDetalle.getIdProducto())) {
                    existe2 = true;
                }
            }

            if (existe2 == false) {
                ordenCompraDetalle.setCantidadConfirmada(BigDecimal.ZERO);
            }

        }

        BodyResponse<OrdenDeCompra> respuestaDeOc = serviceOrdenCompra.editarOrdenCompra(ocEdit);

        if (respuestaDeOc.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Oc ajustada"));
            generarFactura();
        }

    }

    /**
     * Generar OC
     *
     * @param facturaGenerada
     * @return
     */
    public OrdenDeCompra generarOC(Factura facturaGenerada) {
        WebLogger.get().debug("GENERAR ORDEN DE COMPRA");
        ordenDeCompraCreate.setIdClienteOrigen(facturaGenerada.getIdCliente());
        ordenDeCompraCreate.setFechaRecepcion(facturaGenerada.getFecha());
        if(tieneOC.equalsIgnoreCase("S")) {
            ordenDeCompraCreate.setNroOrdenCompra(nroOC);
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
            ordenDeCompraCreate.setNroOrdenCompra("AG-" + sdf.format(new Date()));
        }
        List<OrdenCompraDetalles> listaDetalleOC = new ArrayList<>();

        for (FacturaDetalle facturaDetalle : facturaGenerada.getFacturaDetalles()) {
            OrdenCompraDetalles ocDetalle = new OrdenCompraDetalles();
            ocDetalle.setIdProducto(facturaDetalle.getIdProducto());
            ocDetalle.setPrecioUnitarioConIva(facturaDetalle.getPrecioUnitarioConIva());
            ocDetalle.setCantidad(facturaDetalle.getCantidad());
            ocDetalle.setCantidadSolicitada(BigDecimal.ZERO);
            ocDetalle.setCantidadConfirmada(facturaDetalle.getCantidad());
            ocDetalle.setPorcentajeIva(facturaDetalle.getPorcentajeIva());
            ocDetalle.setPrecioUnitarioSinIva(facturaDetalle.getPrecioUnitarioSinIva());
            ocDetalle.setMontoImponible(facturaDetalle.getMontoImponible());
            ocDetalle.setMontoIva(facturaDetalle.getMontoIva());
            ocDetalle.setMontoTotal(facturaDetalle.getMontoTotal());
            listaDetalleOC.add(ocDetalle);
        }
        ordenDeCompraCreate.setRecibido("S");
        ordenDeCompraCreate.setArchivoEdi("N");
        ordenDeCompraCreate.setGeneradoEdi("N");
        ordenDeCompraCreate.setAnulado("N");
        ordenDeCompraCreate.setCodigoEstado("PENDIENTE_FACTURACION");
        ordenDeCompraCreate.setOrdenCompraDetalles(listaDetalleOC);
        ordenDeCompraCreate.setMontoIva5(facturaGenerada.getMontoIva5());
        ordenDeCompraCreate.setMontoImponible5(facturaGenerada.getMontoImponible5());
        ordenDeCompraCreate.setMontoTotal5(facturaGenerada.getMontoTotal5());
        ordenDeCompraCreate.setMontoIva10(facturaGenerada.getMontoIva10());
        ordenDeCompraCreate.setMontoImponible10(facturaGenerada.getMontoImponible10());
        ordenDeCompraCreate.setMontoTotal10(facturaGenerada.getMontoTotal10());
        ordenDeCompraCreate.setMontoTotalExento(facturaGenerada.getMontoTotalExento());
        ordenDeCompraCreate.setMontoImponibleTotal(facturaGenerada.getMontoImponibleTotal());
        ordenDeCompraCreate.setMontoIvaTotal(facturaGenerada.getMontoIvaTotal());
        ordenDeCompraCreate.setMontoTotalOrdenCompra(facturaGenerada.getMontoTotalFactura());
        WebLogger.get().debug("ORDEN DE COMPRA GENERADA");
        return ordenDeCompraCreate;

    }

    public void mostrarPanelDescarga() {
        PF.current().executeScript("$('#modalDescarga').modal('show');");

    }
    
    /**
     * Método para descargar el pdf de Nota de Crédito
     */
    public StreamedContent download() {
        String contentType = ("application/pdf");
        String fileName = nroFacturaCreada + ".pdf";
        String url = "factura/consulta/" + idFacturaCreada;
        Map param = new HashMap();
        if (this.digital.equalsIgnoreCase("S")){
             param.put("ticket", false);
             if (siediApi.equalsIgnoreCase("S")) {
                param.put("siediApi", true);
             } else {
                param.put("siediApi", false);
             }
        }
        byte[] data = fileServiceClient.downloadFactura(url,param);
        return new DefaultStreamedContent(new ByteArrayInputStream(data),
                contentType, fileName);
    }

    public void print() {
        String url = "factura/consulta/" + idFacturaCreada;
        byte[] data = fileServiceClient.download(url);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String id = sdf.format(new Date());
            File tmp = Files.createTempFile("sepsa_lite_", id).toFile();
            String fileId = tmp.getName().split(Pattern.quote("_"))[2];
            try ( FileOutputStream stream = new FileOutputStream(tmp)) {
                stream.write(data);
                stream.flush();
            }

            String stmt = String.format("hideStatus();printPdf('%s');", fileId);
            PF.current().executeScript(stmt);
        } catch (Throwable thr) {

        }

    }

    /**
     * Método para cambiar de estado a la OC
     */
    public void cambiarEstadoOC() {
        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoOC = new Estado();
        estadoOC.setCodigo("FACTURADO");
        estadoOC.setCodigoTipoEstado("ORDEN_COMPRA");
        adapterEstado = adapterEstado.fillData(estadoOC);

        OrdenDeCompraService service = new OrdenDeCompraService();
        OrdenDeCompra ordenDeCompraEdit = new OrdenDeCompra();

        ordenDeCompraEdit = adapterOrdenCompra.getData().get(0);
        ordenDeCompraEdit.setIdEstado(adapterEstado.getData().get(0).getId());

        BodyResponse<OrdenDeCompra> respuestaDeOC = service.editarOrdenCompra(ordenDeCompraEdit);

        if (respuestaDeOC.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "OC facturada correctamente"));
        } else {
            for (Mensaje mensaje : respuestaDeOC.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }

        }

    }

    public Integer obtenerEstadoCliente() {
        EstadoAdapter adapterEstado = new EstadoAdapter();
        Estado estadoCliente = new Estado();
        estadoCliente.setCodigo("ACTIVO");
        estadoCliente.setCodigoTipoEstado("CLIENTE");
        adapterEstado = adapterEstado.fillData(estadoCliente);

        return adapterEstado.getData().get(0).getId();
    }

    public void obtenerConfiguracionGravada() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("CLIENTE_GRAVADO");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.gravada = "N";
        } else {
            this.gravada = adaptercv.getData().get(0).getValor();
        }

    }

    public void obtenerConfiguracionNroCtaContable() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("PRODUCTO_NRO_CTA_CONTABLE");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.ctaCtble = "N";
        } else {
            this.ctaCtble = adaptercv.getData().get(0).getValor();
        }

    }

    /**
     * Método para obtener la configuración de numeracion de factura
     */
    public void obtenerConfiguracionNumeracionFactura() {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion("NUMERACION_DINAMICA_FACTURA");
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            this.numeracionDinamica = true;
        } else if (adaptercv.getData().get(0).getValor().equals("S")) {
            this.numeracionDinamica = true;
        } else {
            this.numeracionDinamica = false;
        }

        facturaCreate.setAsignarNroFactura(numeracionDinamica);

    }

    /**
     * Método para limpiar el formulario
     */
    public void clearForm() {
        this.codMoneda = null;
        this.facturaCreate = new Factura();
        this.facturaCreate.setIdTipoFactura(2);
        this.facturaCreate.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        this.facturaCreate.setPorcentajeDescuentoGlobal(new BigDecimal("0"));
        this.cliente = new ClientePojo();
        this.cliente.setListadoPojo(true);
        this.adapter = new ClientPojoAdapter();
        filterCliente();
        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();
        this.cobro = new Cobro();
        this.cobro.setSaldoCliente(BigDecimal.ZERO);
        this.cobro.setCodigoMoneda("PYG");
        this.cobroDetalle = new CobroDetalle();
        this.cobroDetalle.setCodigoTipoCobro("EFECTIVO");
        this.cobroDetalles = new ArrayList();
        this.montoCobroTotal = BigDecimal.ZERO;
        this.montoCobroTotalGs = BigDecimal.ZERO;
        this.codMoneda = "PYG";
        
        getTipoPago();
        
        this.adapterFactura = new FacturaAdapter();

        this.adapterFacturaCancelada = new FacturaAdapter();

        this.serviceFacturacion = new FacturacionService();
        this.adapterProducto = new ProductoAdapter();
        this.idCliente = null;

        this.ruc = "--";
        this.direccion = "--";
        this.telefono = "--";
        this.emailsNotificar ="";
        
        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("idOC") != null) {
            idOC = params.get("idOC");
            obtenerOC();
        }
        this.nroOC="";
        this.tieneOC="N";
        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();
        this.nroLineaProducto = null;
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.idProducto = null;
        this.descripcion = null;
        this.porcentaje = null;
        this.parametroProductoPrecio = new ProductoPrecio();
        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.detalleCuotas = new ArrayList<>();
        this.facturaCreate.setIdTipoFactura(2);
        this.ctaCtble = null;
        this.nroCtaCtble = null;
        plazo = false;
        cuota = false;
        this.showTalonarioPopup = false;
        obtenerConfiguracion();
        obtenerDatosFacturacion();
        obtenerConfiguracionGravada();
        obtenerConfiguracionNroCtaContable();
        obtenerConfiguracionNumeracionFactura();

        this.licitacion="N";
        this.facturaDncp = new FacturaDncp();
        
        obtenerTipoTransaccion();
        this.emailList = new ArrayList();

        PF.current().ajax().update(":list-factura-create-form:form-data:localDestino");

    }

    /**
     * Método para obtener el monto en letras
     */
    public void obtenerMontoTotalLetras() {

        montoLetrasFilter = new MontoLetras();
        montoLetrasFilter.setCodigoMoneda(obtenerCodMoneda());
        BigDecimal scaled = facturaCreate.getMontoTotalFactura().setScale(0, RoundingMode.HALF_UP);

        montoLetrasFilter.setMonto(scaled);

        montoLetrasFilter = serviceMontoLetras.getMontoLetras(montoLetrasFilter);
        facturaCreate.setTotalLetras(montoLetrasFilter.getTotalLetras());
    }

    /**
     * Método para mostrar Datatable de Detalle al crear
     */
    public void showDetalle() {
        showDetalleCreate = true;

        addDetalle();
    }

    /**
     * Método para agregar detalle
     */
    public void addDetalle() {
        try {
            FacturaDetalle detalle = new FacturaDetalle();
            Producto productoDetalle = new Producto();

            if (listaDetalle.isEmpty()) {
                detalle.setNroLinea(linea);
            } else {
                linea++;
                detalle.setNroLinea(linea);
            }

            productoDetalle.setDescripcion("--");
            detalle.setPorcentajeGravada(new BigDecimal(100));
            detalle.setDescripcion("--");
            detalle.setCantidad(BigDecimal.valueOf(1));
            detalle.setPorcentajeIva(10);
            detalle.setMontoImponible(BigDecimal.valueOf(0));
            detalle.setMontoIva(BigDecimal.valueOf(0));
            detalle.setMontoTotal(BigDecimal.valueOf(0));
            detalle.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
            detalle.setDescuentoParticularUnitarioAux(BigDecimal.valueOf(0));
            detalle.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoParticular(BigDecimal.valueOf(0));
            detalle.setProducto(productoDetalle);

            listaDetalle.add(detalle);
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para cáculo de totales
     */
    public void calcularTotalesGenerales() {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        if (this.gravada.equalsIgnoreCase("S")) {
            for (int i = 0; i < listaDetalle.size(); i++) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoExentoGravado());
            }
        }

        facturaCreate.setMontoIva5(montoIva5Acumulador);
        facturaCreate.setMontoImponible5(montoImponible5Acumulador);
        facturaCreate.setMontoTotal5(montoTotal5Acumulador);
        facturaCreate.setMontoIva10(montoIva10Acumulador);
        facturaCreate.setMontoImponible10(montoImponible10Acumulador);
        facturaCreate.setMontoTotal10(montoTotal10Acumulador);
        facturaCreate.setMontoTotalExento(montoExcentoAcumulador);
        facturaCreate.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        facturaCreate.setMontoIvaTotal(montoIvaTotalAcumulador);
        //facturaCreate.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        facturaCreate.setMontoTotalDescuentoParticular(new BigDecimal("0"));
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos1(Integer nroLinea) {
        FacturaDetalle info = null;

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }

        BigDecimal cantidad = info.getCantidad();
        BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
        BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

        if (info.getPorcentajeIva() == 0) {

            BigDecimal resultCero = new BigDecimal("0");
            info.setMontoIva(resultCero);
            info.setMontoIva(BigDecimal.ZERO);
            info.setPrecioUnitarioSinIva(precioUnitarioConIVA);
            info.setMontoImponible(precioUnitarioConIVA);
            info.setMontoTotal(info.getMontoImponible());
        }

        if (info.getPorcentajeIva() == 5) {

            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            BigDecimal cinco = new BigDecimal("5");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueCinco, 2, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueCinco, 2, RoundingMode.HALF_UP);
            BigDecimal resultCinco = montoImponible.divide(cinco, 2, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultCinco);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        if (info.getPorcentajeIva() == 10) {

            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            BigDecimal diez = new BigDecimal("10");
            BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(ivaValueDiez, 2, RoundingMode.HALF_UP);

            BigDecimal montoImponible = montoPrecioXCantidad.divide(ivaValueDiez, 2, RoundingMode.HALF_UP);
            BigDecimal resultDiez = montoImponible.divide(diez, 2, RoundingMode.HALF_UP);

            info.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            info.setMontoImponible(montoImponible);
            info.setMontoIva(resultDiez);
            info.setMontoTotal(montoPrecioXCantidad);

        }

        calcularTotal();

        if (facturaCreate.getCantidadCuotas() != null) {
            calcularCuotas();
        }

    }
    
    /**
     * Método para calcular el descuento de un item del detalle
     * @param param 
     */
    public void calcularDescuentoItem(Integer nroLinea, Integer param) {
        FacturaDetalle info = null;
        
        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        
        if (info != null) {
            BigDecimal descuentoItemAux = info.getDescuentoParticularUnitarioAux();
            BigDecimal descuentoItem = info.getDescuentoParticularUnitario();
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();

            if (param == 1) {
                if (info.getPrecioUnitarioConIva() != null && info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == 1) {
                    //valida que el porcentaje de descuento ingresado sea mayor o igual a 0 y menor que 100
                    if (descuentoItemAux != null && descuentoItem != null && ((descuentoItem.compareTo(BigDecimal.ZERO) == 1 && descuentoItem.compareTo(new BigDecimal("100")) == -1) || (descuentoItem.compareTo(BigDecimal.ZERO) == 0)) ) {                
                        BigDecimal porcentajeDescuento = descuentoItemAux.divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
                        montoDescuentoItem = info.getPrecioUnitarioConIva().multiply(porcentajeDescuento);
                        info.setMontoDescuentoParticular(montoDescuentoItem);
                        info.setDescuentoParticularUnitario(descuentoItemAux);
                    } else {
                        info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                        info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                        info.setMontoDescuentoParticular(BigDecimal.ZERO);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje de descuento debe ser entre 0 y 100."));
                    }
                }   
            } else if (param == 2) {
                //valida que el montoDescuento no sea nulo, menor a cero o mayor al precio unitario
                if (montoDescuentoItem != null && ((( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1 ) ||( montoDescuentoItem.compareTo(BigDecimal.ZERO) == 0 )) && (montoDescuentoItem.compareTo(info.getPrecioUnitarioConIva())== -1))){
                    BigDecimal aux = info.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                    descuentoItem = aux.divide(info.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                    info.setDescuentoParticularUnitario(descuentoItem);
                    info.setDescuentoParticularUnitarioAux(descuentoItem);
                } else {
                    info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                    info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
                    info.setMontoDescuentoParticular(BigDecimal.ZERO);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El monto de descuento no puede ser menor a 0 o mayor al precio unitario con IVA"));
                }
            }
        }
    }
    
    
    /**
     * Método que calcula el descuento global de la factura
     * @param param 1 = Calcular descuento segun porcentaje, 2 = Calcular según monto descuento
     */
    public void calcularDescuentos(Integer param){
        if (facturaCreate.getMontoTotalFactura() != null && facturaCreate.getMontoTotalFactura().compareTo(BigDecimal.ZERO) == 1){
            calcularTotal();
            if (param == 1){
                if (facturaCreate.getPorcentajeDescuentoGlobal()!= null) {
                    if(((facturaCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1 || facturaCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 0) &&( facturaCreate.getPorcentajeDescuentoGlobal().compareTo(new BigDecimal("100")) == -1 ||  facturaCreate.getPorcentajeDescuentoGlobal().compareTo(new BigDecimal("100")) == 0))) {
                        BigDecimal porcentajeDescuento = facturaCreate.getPorcentajeDescuentoGlobal().divide(new BigDecimal("100"),5,RoundingMode.HALF_UP);
                        facturaCreate.setMontoTotalDescuentoGlobal(facturaCreate.getSubTotalSinDescuento().multiply(porcentajeDescuento));
                        facturaCreate.setMontoTotalFactura(facturaCreate.getMontoTotalFactura().subtract(facturaCreate.getMontoTotalDescuentoGlobal()));
                            if (facturaCreate.getIdTipoCambio() != null) {
                                TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
                                TipoCambio tipoCambioFilter = new TipoCambio();
                                tipoCambioFilter.setId(facturaCreate.getIdTipoCambio());
                                adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);
                                BigDecimal totalGs = facturaCreate.getMontoTotalFactura().multiply(adapterTipoCambio.getData().get(0).getVenta());
                                facturaCreate.setMontoTotalGuaranies(totalGs);
                                obtenerMontoTotalLetras();
                            } else {

                                facturaCreate.setMontoTotalGuaranies(facturaCreate.getMontoTotalFactura());
                                obtenerMontoTotalLetras();
                            }
                    } else {
                          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El rango del porcentaje de descuento debe ser entre 0 y 100."));
                    }                  
                } else {
                    facturaCreate.setMontoTotalDescuentoGlobal(null);
                }
                
            } else if (param == 2) {
                if (facturaCreate.getMontoTotalDescuentoGlobal() != null && (facturaCreate.getMontoTotalDescuentoGlobal().compareTo(facturaCreate.getMontoTotalFactura()) == -1 || facturaCreate.getMontoTotalFactura().compareTo(facturaCreate.getMontoTotalFactura()) == 0)) {
                    BigDecimal aux = facturaCreate.getMontoTotalDescuentoGlobal().multiply(new BigDecimal("100"));
                    BigDecimal porcentaje = aux.divide(facturaCreate.getSubTotalSinDescuento(),5,RoundingMode.HALF_UP);
                    facturaCreate.setPorcentajeDescuentoGlobal(porcentaje);
                    facturaCreate.setMontoTotalFactura(facturaCreate.getMontoTotalFactura().subtract(facturaCreate.getMontoTotalDescuentoGlobal()));
                    if (facturaCreate.getIdTipoCambio() != null) {
                            TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
                            TipoCambio tipoCambioFilter = new TipoCambio();
                            tipoCambioFilter.setId(facturaCreate.getIdTipoCambio());
                            adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);
                            BigDecimal totalGs = facturaCreate.getMontoTotalFactura().multiply(adapterTipoCambio.getData().get(0).getVenta());
                            facturaCreate.setMontoTotalGuaranies(totalGs);
                            obtenerMontoTotalLetras();
                        } else {

                            facturaCreate.setMontoTotalGuaranies(facturaCreate.getMontoTotalFactura());
                            obtenerMontoTotalLetras();
                        }
                } else {
                    facturaCreate.setPorcentajeDescuentoGlobal(null);
                }
                
            }
        }
    }
    
    
    public void descuentoListener(Integer nroLinea, Integer param){
        calcularDescuentoItem(nroLinea,tipoDescuento);
        calcularMontos(nroLinea);
    }
    
    
    public FacturaDetalle obtenerDetalle(Integer nroLinea) {
        FacturaDetalle info = null;
        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                return info;
            }
        }

        return null;
    }

    /**
     * Método para Cálculo de Montos
     *
     * @param monto
     */
    public void calcularMontos(Integer nroLinea) {
        FacturaDetalle info = null;

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                break;
            }
        }
        if (info.getPrecioUnitarioConIva().compareTo(BigDecimal.ZERO) == -1){
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pueden ingresar montos menores a 0."));
        } else {
            // Se calcula el monto total del detalle
            BigDecimal cantidad = info.getCantidad();
            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
            BigDecimal descuentoParticularUnitario = info.getDescuentoParticularUnitarioAux();
            //Se aplica el descuento al item
            BigDecimal montoDescuentoItem = info.getMontoDescuentoParticular();
            //Se usa para calcular el descuento global
            BigDecimal montoPrecioXCantidadSinDescuento = precioUnitarioConIVA.multiply(cantidad);

            //Casos en que se hayan ingresado primero el monto/porcentaje descuento y luego el precioUnitarioIVA
           if( montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            } else if (descuentoParticularUnitario != null && descuentoParticularUnitario.compareTo(BigDecimal.ZERO) == 1) {
                calcularDescuentoItem(nroLinea,tipoDescuento);
                //Se obtiene el detalle con los montos de descuento actualizados
                info = obtenerDetalle(nroLinea);
                montoDescuentoItem = info.getMontoDescuentoParticular();
            }

            if (montoDescuentoItem != null && montoDescuentoItem.compareTo(BigDecimal.ZERO) == 1) { 
                montoDescuentoItem = info.getMontoDescuentoParticular();
                precioUnitarioConIVA = precioUnitarioConIVA.subtract(montoDescuentoItem);
            } else {
                info.setDescuentoGlobalUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitario(BigDecimal.ZERO);
                info.setDescuentoParticularUnitarioAux(BigDecimal.ZERO);
            }

            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

            // Se calcula el monto base
            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxB = info.getPorcentajeGravada().divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxC = auxA.multiply(auxB);
            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);

            // Se calcula el monto exento gravado
            BigDecimal auxD = new BigDecimal("100").subtract(info.getPorcentajeGravada());
            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
            // Integer aux1 = ((100 - info.getPorcentajeIva()) / 100);
            BigDecimal montoExentoGravado = montoBase.multiply(auxF);

            //Se calcula el monto imponible
            //Integer aux2 = info.getPorcentajeGravada() / 100;
            //BigDecimal auxF = new BigDecimal(info.getPorcentajeGravada()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
            BigDecimal montoImponible = montoBase.multiply(auxB);

            //Se calcula el monto iva
            // Integer aux3 = info.getPorcentajeIva() / 100;
            BigDecimal montoIva = montoImponible.multiply(auxA);

            //Se suman los montos para el total
            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);

            //Precio unitario
            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);

            info.setMontoIva(montoIva);
            info.setMontoImponible(montoImponible);
            info.setMontoExentoGravado(montoExentoGravado);
            info.setMontoTotal(total);
            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
            info.setTotalSinDescuento(montoPrecioXCantidadSinDescuento);

            calcularTotal();

            if (facturaCreate.getPorcentajeDescuentoGlobal()!= null && facturaCreate.getPorcentajeDescuentoGlobal().compareTo(BigDecimal.ZERO) == 1){
                calcularDescuentos(tipoDescuento);
            }

            if (facturaCreate.getCantidadCuotas() != null) {
                calcularCuotas();
            }
        }
    }

    /**
     * Método para calcular el total de la factura
     */
    public void calcularTotal() {
        BigDecimal totalFactura = new BigDecimal("0");
        BigDecimal totalSinDescuento = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            BigDecimal montoTotal = listaDetalle.get(i).getMontoTotal();
            totalFactura = totalFactura.add(montoTotal);
            totalSinDescuento = totalSinDescuento.add(listaDetalle.get(i).getTotalSinDescuento());
        }
        if (facturaCreate.getIdTipoCambio() != null) {
            facturaCreate.setMontoTotalFactura(totalFactura);
            TipoCambioAdapter adapterTipoCambio = new TipoCambioAdapter();
            TipoCambio tipoCambioFilter = new TipoCambio();
            tipoCambioFilter.setId(facturaCreate.getIdTipoCambio());
            adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);
            BigDecimal totalGs = totalFactura.multiply(adapterTipoCambio.getData().get(0).getVenta());
            facturaCreate.setMontoTotalGuaranies(totalGs);
            facturaCreate.setSubTotalSinDescuento(totalSinDescuento);
            obtenerMontoTotalLetras();
        } else {
            facturaCreate.setMontoTotalFactura(totalFactura);
            facturaCreate.setMontoTotalGuaranies(totalFactura);
            facturaCreate.setSubTotalSinDescuento(totalSinDescuento);
            obtenerMontoTotalLetras();
        }

    }

    /**
     * Método para eliminar el detalle del listado
     *
     * @param rowKey
     */
    public void deleteDetalle(Integer nroLinea) {
        FacturaDetalle info = null;
        BigDecimal subTotal = new BigDecimal("0");

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLinea)) {
                info = info0;
                subTotal = info.getMontoTotal();
                listaDetalle.remove(info);
                break;
            }
        }

        if (facturaCreate.getMontoTotalFactura() != null) {
            BigDecimal monto = facturaCreate.getMontoTotalFactura();
            BigDecimal totalFactura = monto.subtract(subTotal);
            facturaCreate.setMontoTotalFactura(totalFactura);
            obtenerMontoTotalLetras();
        }

    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelect(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        if(digital != null && digital.equals("S")) {
            today.add(Calendar.DAY_OF_MONTH, 5);
        }
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) < 0) {
            facturaCreate.setFecha(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha posterior!"));
        } else {
            calcularFechaVto();
        }

    }

    /**
     * Método para comparar la fecha
     *
     * @param event
     */
    public void onDateSelectVto(SelectEvent event) {
        Calendar today = Calendar.getInstance();
        Date dateUtil = (Date) event.getObject();
        if (today.getTime().compareTo(dateUtil) > 0) {
            facturaCreate.setFechaVencimiento(today.getTime());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede editar a una fecha anterior!"));
        } else {
            Calendar day1 = Calendar.getInstance();
            Calendar day2 = Calendar.getInstance();
            day1.setTime(today.getTime());
            day2.setTime(facturaCreate.getFechaVencimiento());

            int daysBetween = day2.get(Calendar.DAY_OF_YEAR) - day1.get(Calendar.DAY_OF_YEAR);

            facturaCreate.setDiasCredito(daysBetween);
        }

    }

    /**
     * Método para filtrar el producto
     */
    public void filterProducto() {
        producto = new Producto();
        adapterProducto = adapterProducto.fillData(producto);
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<ClientePojo> completeQuery(String query) {

        ClientePojo cliente = new ClientePojo();
        cliente.setListadoPojo(true);
        cliente.setIdEstado(obtenerEstadoCliente());
        cliente.setRazonSocial(query);

        adapter = adapter.fillData(cliente);

        return adapter.getData();
    }

    /**
     * Método para obtener la lista de cliente
     */
    public void filterCliente() {
        cliente.setListadoPojo(true);
        this.adapter = adapter.fillData(cliente);
    }

    /**
     * Método para obtener Moneda
     */
    public void filterMoneda() {
        this.adapterMoneda = adapterMoneda.fillData(monedaFilter);
    }
    
        /**
     * Método para obtener Moneda
     */
    public void obtenerTipoTransaccion() {
        TipoTransaccion tipoTransaccionFilter = new TipoTransaccion();
        this.tipoTransaccionAdapter = tipoTransaccionAdapter.fillData(tipoTransaccionFilter);
        
        configuracionValorFilter.setCodigoConfiguracion("TIPO_TRANSACCION");
        configuracionValorFilter.setActivo("S");
        adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

        if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
            this.facturaCreate.setIdTipoTransaccion(2);
        } else {
            Integer tipoTransaccion = Integer.parseInt(adapterConfigValor.getData().get(0).getValor());
            this.facturaCreate.setIdTipoTransaccion(tipoTransaccion);
            
        }
    }

    /**
     * Método para obtener orden de compra
     */
    public void obtenerOC() {
        this.adapterOrdenCompra = new OrdenDeCompraAdapter();
        this.ordenDeCompra = new OrdenDeCompra();
        ordenDeCompra.setId(Integer.parseInt(idOC));
        facturaCreate.setIdOrdenCompra(ordenDeCompra.getId());
        adapterOrdenCompra = adapterOrdenCompra.fillData(ordenDeCompra);
        showDetalleCreate = true;
        facturaCreate.setArchivoEdi(adapterOrdenCompra.getData().get(0).getArchivoEdi());

        if (facturaCreate.getArchivoEdi().equals("S")) {
            facturaCreate.setIdLocalOrigen(adapterOrdenCompra.getData().get(0).getIdLocalDestino());
            facturaCreate.setIdLocalDestino(adapterOrdenCompra.getData().get(0).getIdLocalOrigen());
        }

        for (OrdenCompraDetalles detalleOC : adapterOrdenCompra.getData().get(0).getOrdenCompraDetalles()) {
            FacturaDetalle detalle = new FacturaDetalle();

            if (listaDetalle.isEmpty()) {
                detalle.setNroLinea(linea);
            } else {
                linea++;
                detalle.setNroLinea(linea);
            }
            detalle.setIdProducto(detalleOC.getProducto().getId());
            detalle.setDescripcion(detalleOC.getProducto().getDescripcion());
            detalle.setPorcentajeIva(detalleOC.getProducto().getPorcentajeImpuesto());
            detalle.setCantidad(detalleOC.getCantidadConfirmada());
            detalle.setPorcentajeGravada(new BigDecimal(100));
            detalle.setPrecioUnitarioConIva(detalleOC.getPrecioUnitarioConIva());
            detalle.setMontoImponible(BigDecimal.valueOf(0));
            detalle.setMontoIva(BigDecimal.valueOf(0));
            detalle.setMontoTotal(BigDecimal.valueOf(0));
            detalle.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
            detalle.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
            detalle.setMontoDescuentoParticular(BigDecimal.valueOf(0));

            listaDetalle.add(detalle);

            calcularMontos(linea);
        }

        if (adapterOrdenCompra.getData().get(0).getLocalOrigen() != null) {
            if (adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente() != null) {
                facturaCreate.setIdCliente(adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente().getIdCliente());
                idCliente = facturaCreate.getIdCliente();
                facturaCreate.setRazonSocial(adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente().getRazonSocial());
                facturaCreate.setRuc(adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente().getNroDocumento());

                if (adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente().getIdCanalVenta() != null) {
                    parametroProductoPrecio.setIdCanalVenta(adapterOrdenCompra.getData().get(0).getLocalOrigen().getCliente().getIdCanalVenta());
                } else {
                    parametroProductoPrecio.setIdCanalVenta(0);
                }
            }
        } else {
            facturaCreate.setIdCliente(adapterOrdenCompra.getData().get(0).getClienteOrigen().getIdCliente());
            idCliente = facturaCreate.getIdCliente();
            facturaCreate.setRazonSocial(adapterOrdenCompra.getData().get(0).getClienteOrigen().getRazonSocial());
            facturaCreate.setRuc(adapterOrdenCompra.getData().get(0).getClienteOrigen().getNroDocumento());
            if (adapterOrdenCompra.getData().get(0).getClienteOrigen().getIdCanalVenta() != null) {
                parametroProductoPrecio.setIdCanalVenta(adapterOrdenCompra.getData().get(0).getClienteOrigen().getIdCanalVenta());
            } else {
                parametroProductoPrecio.setIdCanalVenta(0);
            }
        }
    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVto() {
        if (facturaCreate.getIdTipoFactura() == 2) {
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(facturaCreate.getFecha());
            cal.add(Calendar.DAY_OF_MONTH, facturaCreate.getDiasCredito());
            Date fechaVencimiento = cal.getTime();

            facturaCreate.setFechaVencimiento(fechaVencimiento);
        }

    }

    /**
     * Método que se dispara al cambiar los dias de crédito
     */
    public void calcularFechaVtoContado() {
        if (facturaCreate.getIdTipoFactura() == 2) {
            facturaCreate.setFechaVencimiento(facturaCreate.getFecha());
            facturaCreate.setDiasCredito(null);
        }

    }
  
    /**
     * Método que se dispara en el evento de cambio de moneda
     */
    public void onChangeMoneda() {
        Calendar now = Calendar.getInstance();
        Date date = now.getTime();

        this.adapterTipoCambio = new TipoCambioAdapter();
        TipoCambio tipoCambioFilter = new TipoCambio();

        tipoCambioFilter.setIdMoneda(facturaCreate.getIdMoneda());
        tipoCambioFilter.setActivo("S");
        tipoCambioFilter.setFechaInsercion(date);
        
        adapterTipoCambio = adapterTipoCambio.fillData(tipoCambioFilter);

        codMoneda = obtenerCodMoneda();

        if (adapterTipoCambio.getData().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No existe un registro de cotización actual para la moneda seleccionada!"));
            habilitarPanelCotizacion = true;
        } else {
            facturaCreate.setIdTipoCambio(adapterTipoCambio.getData().get(0).getId());
            tipoCambio = adapterTipoCambio.getData().get(0);
            facturaCreate.setCodigoMoneda(codMoneda);
            habilitarPanelCotizacion = true;
            if (!listaDetalle.isEmpty()) {
                if (codMoneda.equals("PYG")) {
                    for (FacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioVenta(facturaDetalle, adapterTipoCambio.getData().get(0).getVenta());
                    }
                } else {
                    for (FacturaDetalle facturaDetalle : listaDetalle) {
                        calcularCambioCompra(facturaDetalle, adapterTipoCambio.getData().get(0).getCompra());
                    }
                }
            }
        }
    }

    public void crearTipoCambio() {
        tipoCambio.setActivo("S");
        tipoCambio.setIdMoneda(facturaCreate.getIdMoneda());
        if (facturaCreate.getIdTipoCambio() != null){
            tipoCambio.setId(facturaCreate.getIdTipoCambio());
        } 
        BodyResponse<TipoCambio> respuestaDeTipoCambio = serviceTipoCambio.actualizarTipoCambio(tipoCambio);

        if (respuestaDeTipoCambio.getSuccess()) {
            habilitarPanelCotizacion = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Tipo de cambio creado correctamente!"));
            onChangeMoneda();
        } else {
            for (Mensaje mensaje : respuestaDeTipoCambio.getStatus().getMensajes()) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", mensaje.getDescripcion()));
            }

        }
    }

    public String obtenerCodMoneda() {
        Moneda mf = new Moneda();
        MonedaAdapter am = new MonedaAdapter();
        mf.setId(facturaCreate.getIdMoneda());
        am = am.fillData(mf);

        return am.getData().get(0).getCodigo();
    }

    /**
     * Método para calcular el precio unitario con gs de referencia
     *
     * @param facturaDetalle
     * @param precioCompraMoneda
     */
    public void calcularCambioCompra(FacturaDetalle facturaDetalle, BigDecimal precioCompraMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.divide(precioCompraMoneda, 5, RoundingMode.HALF_UP);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método para calcular el precio unitario a gs
     *
     * @param facturaDetalle
     * @param precioVentaMoneda
     */
    public void calcularCambioVenta(FacturaDetalle facturaDetalle, BigDecimal precioVentaMoneda) {
        BigDecimal precioUnitario = facturaDetalle.getPrecioUnitarioConIva();
        BigDecimal precioUnitarioCambio = precioUnitario.multiply(precioVentaMoneda);
        facturaDetalle.setPrecioUnitarioConIva(precioUnitarioCambio);

        calcularMontos(facturaDetalle.getNroLinea());
    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {

        producto = new Producto();
        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto.setPageSize(100);
        adapterProducto = adapterProducto.fillData(producto);
        producto = new Producto();

        return adapterProducto.getData();
    }

    public void guardarNroLinea(Integer nroLinea) {
        nroLineaProducto = nroLinea;
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            idProducto = ((Producto) event.getObject()).getId();
            descripcion = ((Producto) event.getObject()).getDescripcion();
            porcentaje = ((Producto) event.getObject()).getPorcentajeImpuesto();
            loteProducto = ((Producto) event.getObject()).getNroLote();
            Date fechaVencLote = ((Producto) event.getObject()).getFechaVencimientoLote();
            if (fechaVencLote != null ) {
                fechaVencimientoLote = sdf.format(((Producto) event.getObject()).getFechaVencimientoLote());
            } else {
                fechaVencimientoLote = null;
            }
            
            if (((Producto) event.getObject()).getUmv() != null) {
                umv = ((Producto) event.getObject()).getUmv();
            } else {
                umv = BigDecimal.valueOf(1);
            }

            if (((Producto) event.getObject()).getCuentaContable() != null) {
                nroCtaCtble = ((Producto) event.getObject()).getCuentaContable();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Método para agregar el producto con validaciones
     */
    public void agregarProducto() {
        Calendar today = Calendar.getInstance();
        boolean save = true;
        FacturaDetalle info = null;
        ProductoService serviceProductoPrecio = new ProductoService();

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getNroLinea(), nroLineaProducto)) {
                info = info0;
                break;
            }
        }

        for (FacturaDetalle info0 : listaDetalle) {
            if (Objects.equals(info0.getIdProducto(), idProducto)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El producto ya se encuentra en la lista de detalle, no puede repetirse"));
                save = false;
                break;
            }
        }

        if (save == true) {

            parametroProductoPrecio.setIdProducto(idProducto);
            parametroProductoPrecio.setIdMoneda(facturaCreate.getIdMoneda());
            parametroProductoPrecio.setFecha(today.getTime());
            if (facturaCreate.getIdCliente() != null) {
                parametroProductoPrecio.setIdCliente(facturaCreate.getIdCliente());
            } else {
                parametroProductoPrecio.setIdCanalVenta(0);
                parametroProductoPrecio.setIdCliente(0);
            }

            ProductoPrecio prodRespuesta = new ProductoPrecio();
            WebLogger.get().debug("PRIMERA");
            prodRespuesta = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);

            if (prodRespuesta == null) {
                parametroProductoPrecio.setIdCliente(0);
                parametroProductoPrecio.setIdCanalVenta(0);
                ProductoPrecio prodRespuesta2 = new ProductoPrecio();
                WebLogger.get().debug("SEGUNDA");
                prodRespuesta2 = serviceProductoPrecio.consultaProductoPrecio(parametroProductoPrecio);
                if (prodRespuesta2 == null) {
                    info.setDescripcion(descripcion);
                    info.setPorcentajeIva(porcentaje);
                    info.setCantidad(umv);
                    info.setNroCtaContable(nroCtaCtble);
                    info.setIdProducto(idProducto);
                    info.setLote(loteProducto);
                    info.setFechaVencimientoLote(fechaVencimientoLote);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El producto seleccionado no tiene un precio general asociado activo"));
                } else {
                    info.setDescripcion(prodRespuesta2.getProducto().getDescripcion());
                    info.setPrecioUnitarioConIva(prodRespuesta2.getPrecio());
                    info.setIdProducto(idProducto);
                    info.setPorcentajeIva(porcentaje);
                    info.setCantidad(umv);
                    info.setNroCtaContable(nroCtaCtble);
                    info.setLote(loteProducto);
                    info.setFechaVencimientoLote(fechaVencimientoLote);
                    calcularMontos(nroLineaProducto);
                }

            } else {
                info.setDescripcion(descripcion);
                info.setPorcentajeIva(porcentaje);
                info.setCantidad(umv);
                info.setIdProducto(idProducto);
                info.setNroCtaContable(nroCtaCtble);
                info.setPrecioUnitarioConIva(prodRespuesta.getPrecio());
                info.setLote(loteProducto);
                info.setFechaVencimientoLote(fechaVencimientoLote);
                calcularMontos(nroLineaProducto);
                
            }

        }
    }

    /**
     * Método para definir el tipo de operación a crédito
     */
    public void seleccionarPlazo() {
        plazo = true;
        cuota = false;
    }

    /**
     * Método para definir el tipo de operación a crédito
     */
    public void seleccionarCuota() {
        plazo = false;
        cuota = true;
    }

    /**
     * Método para mostrar panel de cuota
     */
    public void calcularCuotas() {
        if (facturaCreate.getMontoTotalFactura() != null) {
            detalleCuotas = new ArrayList<>();
            BigDecimal montoTotal = facturaCreate.getMontoTotalFactura();
            BigDecimal cantidadCuotas = new BigDecimal(facturaCreate.getCantidadCuotas());
            BigDecimal cuota = montoTotal.divide(cantidadCuotas, 2, RoundingMode.HALF_UP);
            for (int i = 0; i < facturaCreate.getCantidadCuotas(); i++) {
                Cuota nuevaCuota = new Cuota();
                nuevaCuota.setIdMoneda(facturaCreate.getIdMoneda());
                nuevaCuota.setMonto(cuota);
                detalleCuotas.add(nuevaCuota);
            }
        } else {
            WebLogger.get().debug("NO HAY MONTO DE FACTURA, SE CALCULA DESPUÉS");
        }

    }

    /* Método para obtener los departamentos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDpto(String query) {
        Integer empyInt = null;
        referenciaGeoDepartamento = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDepartamento(empyInt);
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica ref = new ReferenciaGeografica();
            ref.setDescripcion(query);
            ref.setIdTipoReferenciaGeografica(1);
            adapterRefGeo = adapterRefGeo.fillData(ref);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-factura-create-form:form-data:distrito");
        PF.current().ajax().update(":list-factura-create-form:form-data:ciudad");

        return datos;
    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryDistritos(String query) {
        Integer empyInt = null;
        referenciaGeoDistrito = null;
        referenciaGeoCiudad = null;
        direccionFilter.setIdDistrito(empyInt);
        direccionFilter.setIdCiudad(empyInt);

        List<ReferenciaGeografica> datos;
        if (query != null && !query.trim().isEmpty()) {
            ReferenciaGeografica referenciaGeoDistrito = new ReferenciaGeografica();
            referenciaGeoDistrito.setIdPadre(direccionFilter.getIdDepartamento());
            referenciaGeoDistrito.setDescripcion(query);
            adapterRefGeo = adapterRefGeo.fillData(referenciaGeoDistrito);
            datos = adapterRefGeo.getData();
        } else {
            datos = Collections.emptyList();
        }

        PF.current().ajax().update(":list-factura-create-form:form-data:ciudad");

        return datos;

    }

    /**
     * Método para obtener la lista de distritos
     *
     * @param query
     * @return
     */
    public List<ReferenciaGeografica> completeQueryCiudad(String query) {
        referenciaGeoCiudad = new ReferenciaGeografica();
        referenciaGeoCiudad.setDescripcion(query);
        referenciaGeoCiudad.setIdPadre(direccionFilter.getIdDistrito());

        adapterRefGeo = adapterRefGeo.fillData(referenciaGeoCiudad);

        return adapterRefGeo.getData();
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDptoFilter(SelectEvent event) {
        // referenciaGeoDistrito = new ReferenciaGeografica();
        // referenciaGeoCiudad = new ReferenciaGeografica();
        // adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdDepartamento(((ReferenciaGeografica) event.getObject()).getId());

    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectDistritoFilter(SelectEvent event) {
        //referenciaGeoCiudad = new ReferenciaGeografica();
        //adapterRefGeo = new ReferenciaGeograficaAdapter();
        direccionFilter.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdDistrito(((ReferenciaGeografica) event.getObject()).getId());
    }

    /**
     * Selecciona departamento
     *
     * @param event
     */
    public void onItemSelectCiudadFilter(SelectEvent event) {
        direccionFilter.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
        facturaCreate.setIdCiudad(((ReferenciaGeografica) event.getObject()).getId());
    }

    public ReferenciaGeografica obtenerReferencia(Integer id) {
        ReferenciaGeografica refe = new ReferenciaGeografica();
        ReferenciaGeograficaAdapter adapterRefGeo = new ReferenciaGeograficaAdapter();
        refe.setId(id);
        adapterRefGeo = adapterRefGeo.fillData(refe);
        return adapterRefGeo.getData().get(0);
    }

    /* Método para obtener los locales filtrados
     *
     * @param query
     * @return
     */
    public List<Local> completeQueryOrigen(String query) {
        Local localOrigen = new Local();
        LocalListAdapter localOrigenAdapter = new LocalListAdapter();
        localOrigen.setLocalExterno("N");
        localOrigen.setDescripcion(query);
        localOrigenAdapter = localOrigenAdapter.fillData(localOrigen);
        return localOrigenAdapter.getData();
    }

    /* Método para obtener locales
     *
     * @return
     */
    public void consultarLocales() {
        if (cliente != null && cliente.getIdCliente() != null) {
            Local localFilter = new Local();
            localFilter.setIdPersona(cliente.getIdCliente());
            adapterLocal.setPageSize(1000);
            this.adapterLocal = adapterLocal.fillData(localFilter); 
            if (!adapterLocal.getData().isEmpty() && adapterLocal.getData().size()>1){
                multiplesLocales = "S";
            } else {
                multiplesLocales = "N";
            }
        } 
    }
    
    
    public void obtenerDatosLocal() {
        Local local = null;
        if (idLocal != null){
            local = adapterLocal.getData().stream().filter(r -> r.getId() != null && r.getId().equals(idLocal)).findFirst().orElse(null);
        }
        
        if (local != null) {     
            DireccionAdapter adapterDireccion = new DireccionAdapter();
            Direccion direccionFilter1 = new Direccion();
            direccionFilter1.setId(local.getIdDireccion());
            adapterDireccion = adapterDireccion.fillData(direccionFilter1);
            if (adapterDireccion != null && !adapterDireccion.getData().isEmpty()){
                
                Direccion dir = adapterDireccion.getData().get(0);
                facturaCreate.setDireccion(dir.getDireccion());
                idDepartamento = dir.getIdDepartamento();
                filterGeneral();
                idDistrito = dir.getIdDistrito();
                filterCiudad();
                idCiudad = dir.getIdCiudad();
                facturaCreate.setNroCasa(dir.getNumero());   
                
            }

            if (local.getIdTelefono() != null) {
                TelefonoListAdapter telefonoAdapter = new TelefonoListAdapter();
                Telefono telefonoFilter = new Telefono();
                telefonoFilter.setId(local.getIdTelefono());
                telefonoAdapter = telefonoAdapter.fillData(telefonoFilter);
                if (!telefonoAdapter.getData().isEmpty()){
                    Telefono tel = telefonoAdapter.getData().get(0);
                    facturaCreate.setTelefono(tel.getPrefijo()+tel.getNumero());
                }

            }
        }
    }
    
    
    /**
     *
     * @param event
     */
    public void onItemSelectLocalOringenFilter(SelectEvent event) {
        this.ordenDeCompraCreate.setIdLocalDestino(((Local) event.getObject()).getId());
        this.ordenDeCompraCreate.setIdClienteDestino(((Local) event.getObject()).getIdPersona());
    }

    public void filterDepartamento() {
        WebLogger.get().debug("FILTRO DEPARTAMENTO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(1);
        this.adapterRefGeo.setPageSize(50);
        this.adapterRefGeo = this.adapterRefGeo.fillData(referenciaGeo);
    }

    public void filterDistrito() {
        WebLogger.get().debug("FILTRO DISTRITO");
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(2);
        referenciaGeo.setIdPadre(this.idDepartamento);
        this.adapterRefGeoDistrito.setPageSize(300);
        this.adapterRefGeoDistrito = this.adapterRefGeoDistrito.fillData(referenciaGeo);
    }

    public void filterCiudad() {
        WebLogger.get().debug("FILTRO CIUDAD");
        this.idCiudad = null;
        ReferenciaGeografica referenciaGeo = new ReferenciaGeografica();
        referenciaGeo.setIdTipoReferenciaGeografica(3);
        referenciaGeo.setIdPadre(this.idDistrito);
        this.adapterRefGeoCiudad.setPageSize(300);
        this.adapterRefGeoCiudad = this.adapterRefGeoCiudad.fillData(referenciaGeo);
    }

    public void filterGeneral() {
        this.idDistrito = null;
        this.idCiudad = null;
        filterDistrito();
        filterCiudad();
    }
    
        public String obtenerConfiguracion(String code) {
        ConfiguracionValor cvf = new ConfiguracionValor();
        ConfiguracionValorListAdapter adaptercv = new ConfiguracionValorListAdapter();
        cvf.setCodigoConfiguracion(code);
        cvf.setActivo("S");
        adaptercv = adaptercv.fillData(cvf);
        String value = null;

        if (adaptercv.getData() == null || adaptercv.getData().isEmpty()) {
            value = "N";
        } else {
            value = adaptercv.getData().get(0).getValor();
        }

        return value;

    }
    
    /**
     * Método para obtener la configuración de Notificación por email
     */
    public void obtenerConfiguracionNotificacion() {
        try {
            ConfiguracionValorListAdapter adapterConfigValor = new ConfiguracionValorListAdapter();
            ConfiguracionValor configuracionValorFilter = new ConfiguracionValor();
            configuracionValorFilter.setIdEmpresa(session.getUser().getIdEmpresa());
            configuracionValorFilter.setActivo("S");
            configuracionValorFilter.setCodigoConfiguracion("NOTIFICACION_FACTURA_EMAIL");
            adapterConfigValor = adapterConfigValor.fillData(configuracionValorFilter);

            if (adapterConfigValor.getData() == null || adapterConfigValor.getData().isEmpty()) {
                this.notificar = "N";
            } else {
                this.notificar = adapterConfigValor.getData().get(0).getValor();
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
    
    public void notificar(String emailDestino) {
        String mailSender = obtenerConfiguracion("MAIL_SENDER");
        String passSender = obtenerConfiguracion("PASS_MAIL_SENDER");
        String port = obtenerConfiguracion("PUERTO_CORREO_SALIENTE");
        String host = obtenerConfiguracion("SERVIDOR_CORREO_SALIENTE");
        String emailEmpresa = obtenerConfiguracion("EMAIL");
        String telefonoEmpresa = obtenerConfiguracion("TELEFONO");
        String cliente = this.cliente.getRazonSocial();
        String empresa = session.getNombreEmpresa();
        String mailTo = emailDestino;
        String mensaje = "Estimado(a) Cliente:" + cliente + "\n"
                + "Adjunto encontrará su recibo en formato PDF";
        
        String subjet = "Factura No. " + nroFacturaCreada;
        String url = "factura/consulta/" + idFacturaCreada;
        byte[] data = fileServiceClient.download(url);
       
        String fileName = nroFacturaCreada + ".pdf";

        obtenerDatosFactura(idFacturaCreada);
        notificarViaPropia(mensaje, subjet, mailTo, data, fileName, mailSender, passSender, cliente, nroFacturaCreada, host, port, empresa, emailEmpresa, telefonoEmpresa);

        
    }
    
     public void notificarViaPropia(String msn, String subject, String to, byte[] file, String fileName, String from, String passFrom, String cliente, String nroDoc, String host, String port, String empresa, String emailEmpresa, String telefonoEmpresa) {
        try {
            String mensaje = MailUtils.getCustomMailHtml(nroDoc, cliente, empresa, emailEmpresa, telefonoEmpresa);
            Boolean notificados = MailUtils.sendMail(mensaje, subject, to, file, fileName, from, passFrom, host,port);  
            if (notificados) {
                
                LocalDate fecha = LocalDate.now();
                // Convertir LocalDate a Date
                Date fechaHoy = Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
                facturaCreate.setFechaEntrega(fechaHoy);
                facturaCreate.setEntregado("C");
                facturaCreate.setId(idFacturaCreada);
                serviceFacturacion.editFactura(facturaCreate);
                this.notificado = true;
            } else {
                // actualizar campo "entregado" a P (Pendiente)
                facturaCreate.setEntregado("P");
                serviceFacturacion.editFactura(facturaCreate);
            }
            
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }
     
    public void getTipoPago() {
        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        for (int i = 0; i < adapterTipoCobro.getData().size(); i++) {
            listTipoCobro.add(new SelectItem(adapterTipoCobro.getData().get(i).
                    getId(), adapterTipoCobro.getData().get(i).getCodigo()));
        }
    }
     
     
    /**
     * Método para obtener el tipo de cobro
     *
     * @param idTipoCobro
     * @return
     */
    public String getCodTipoCobro(Integer idTipoCobro) {
        tipoCobro = new TipoCobro();
        tipoCobro.setId(idTipoCobro);
        adapterTipoCobro = adapterTipoCobro.fillData(tipoCobro);
        return adapterTipoCobro.getData().get(0).getCodigo();
    }
     

    public void codTipoCobro() {
        cobroDetalle.setCodigoTipoCobro(getCodTipoCobro(cobroDetalle.getIdTipoCobro()));
        if (cobroDetalle.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
            Calendar today = Calendar.getInstance();
            cheque.setOmitirValidacionCheque("S");
            cheque.setMontoCheque(BigDecimal.ZERO);
            cheque.setNroCheque("");
            cheque.setFechaEmision(today.getTime());
            cheque.setFechaPago(today.getTime());
        } else {
            cobroDetalle.setCheque(null);
        }

    }
    
    
        /**
     * Método autocomplete cliente
     *
     * @param query
     * @return
     */
    public List<EntidadFinanciera> completeQueryEntidad(String query) {

        EntidadFinanciera entidadFinanciera = new EntidadFinanciera();
        entidadFinanciera.setRazonSocial(query);
        adapterEntidadFinanciera.setPageSize(20);
        adapterEntidadFinanciera = adapterEntidadFinanciera.fillData(entidadFinanciera);

        return adapterEntidadFinanciera.getData();
    }
    
    
    public void onItemSelectEntidadFinanciera() {
        cobroDetalle.setIdEntidadFinanciera(entidadFinanciera.getIdEntidadFinanciera());
        cobroDetalle.setEntidadFinanciera(entidadFinanciera);
        if (cobroDetalle.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")) {
            cobroDetalle.getCheque().setIdEntidadFinanciera(entidadFinanciera.getIdEntidadFinanciera());
        }
        int a = cobroDetalle.getIdEntidadFinanciera();
        WebLogger.get().debug("id entidad financiera" + a);

    }
    
    /**
     * Genera el cobro que se asociará a la factura cuando es contado.
     */
    public void generarCobro() {
        this.adapterCobroTalonario = new CobroTalonarioAdapter();
        this.cobroTalonario = new TalonarioPojo();
        this.cobroClient = new CobroService();
        obtenerDatosTalonarioCobro();
        
        cobro.setNroFactura(facturaCreate.getNroFactura());
        cobro.setSaldoCliente(BigDecimal.ZERO);
        cobro.setTotalFactura(facturaCreate.getMontoTotalFactura());
        cobro.setMontoRetenidoTotal(BigDecimal.ZERO);
        cobro.setCodigoMoneda(facturaCreate.getCodigoMoneda());
        cobro.setIdCliente(idCliente);
        LugarCobro lugarCobro = new LugarCobro();
        LugarCobroListAdapter adapterLugarCobro = new LugarCobroListAdapter();
        adapterLugarCobro = adapterLugarCobro.fillData(lugarCobro);
        if (adapterLugarCobro != null) {
            cobro.setCodigoLugarCobro(adapterLugarCobro.getData().get(0).getCodigo());
        }
        Estado estadoDetalle = new Estado();
        estadoDetalle.setActivo("C");
        cobro.setEstado(estadoDetalle);
        cobro.setEnviado('N');
        cobro.setDigital(reciboDigital);
        cobro.setCodigoEstado("COBRADO");
        monedaFilter = new Moneda();
        monedaFilter.setId(facturaCreate.getIdMoneda());
        adapterMoneda = adapterMoneda.fillData(monedaFilter);
        if (!adapterMoneda.getData().isEmpty()) {
            monedaFilter = adapterMoneda.getData().get(0);
        } else {
            monedaFilter.setCodigo("PYG");
        }
        cobro.setMontoCobro(facturaCreate.getMontoTotalFactura());
        if (!monedaFilter.getCodigo().equalsIgnoreCase("PYG")) {
            cobroDetalle.setMontoCobro(facturaCreate.getMontoTotalFactura());
            cobroDetalle.setMontoCobroGuaranies(facturaCreate.getMontoTotalGuaranies());
            cobro.setIdTipoCambio(facturaCreate.getIdTipoCambio());
        } else {
            cobroDetalle.setMontoCobro(facturaCreate.getMontoTotalFactura());
        }    
        
        cobro.setCobroDetalles(cobroDetalles);
        facturaCreate.setCobro(cobro);
        facturaCreate.setSaldo(BigDecimal.ZERO);
        facturaCreate.setCobro(cobro);
    }
    
    /**
     * Obtiene datos de talonario tipo Cobro
     */
    public void obtenerDatosTalonarioCobro() {
        cobroTalonario.setDigital(reciboDigital);
        adapterCobroTalonario = adapterCobroTalonario.fillData(cobroTalonario);
        List<TalonarioPojo> talonarios = new ArrayList();
        if (!adapterCobroTalonario.getData().isEmpty()){
            if (adapterCobroTalonario.getData().size() > 1) {
                LocalDate fechaHoy = LocalDate.now();
                for(TalonarioPojo tp :adapterCobroTalonario.getData()){
                    if (tp.getFechaVencimiento() != null){
                        LocalDate fechaAComparar = tp.getFechaVencimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        if (fechaAComparar.isAfter(fechaHoy)) {
                            talonarios.add(tp);
                        }
                    }
                    else {
                        talonarios.add(tp);
                    }
                }
                if(talonarios.size()>1) {
                    adapterCobroTalonario.setData(talonarios);
                    showTalonarioPopup = true;
                    PF.current().ajax().update(":cobro-form:form-data:pnl-pop-up");
                }
                else if(!talonarios.isEmpty()) {
                    cobroTalonario.setId(adapterCobroTalonario.getData().get(0).getId());
                    obtenerDatosCobro();
                }
            } else {
                cobroTalonario.setId(adapterCobroTalonario.getData().get(0).getId());
                obtenerDatosCobro();
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario de cobro disponible"));
            PF.current().ajax().update(":cobro-form:form-data:pnl-pop-up :message-form:message");
        }
    }
    
    /**
     * Obtiene datos generales para el cobro
     */
    public void obtenerDatosCobro() {
        try {

            Map parametros = new HashMap();
            datoCobro = new DatosCobro();
            Calendar today = Calendar.getInstance();
            parametros.put("digital", reciboDigital);
            if (cobro.getIdCliente() != null) {
                parametros.put("idCliente", cobro.getIdCliente());
            }

            if (cobroTalonario.getId() != null) {
                parametros.put("id", cobroTalonario.getId());
            }

            datoCobro = cobroClient.getDatoCobro(parametros);

            if (datoCobro != null) {
                cobro.setFecha(today.getTime());
                cobro.setIdTalonario(datoCobro.getIdTalonario());
                cobro.setNroRecibo(datoCobro.getNroRecibo());
                showTalonarioPopup = false;
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encuentra talonario de cobro disponible"));
            }

        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
    
    /**
     * Método para borrar linea de detalle de cobro
     */
    public void delLine() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String nroLinea = (String) params.get("nro");  
        if (cobroDetalles.size() == 1) {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                    "Debe tener al menos un detalle!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        } else if (cobroDetalles.size() > 1) {
            for (int j = 0; j < cobroDetalles.size(); j++) {
                CobroDetalle cd = cobroDetalles.get(j);
                if (cd.getNroLinea() == Integer.parseInt(nroLinea)) {
                    montoCobroTotal = montoCobroTotal.subtract(cd.getMontoCobro());
                    cobro.setSaldoCliente(cobro.getSaldoCliente().add(cd.getMontoCobro()));
                    cobroDetalles.remove(cd);
                }
            }
        }
        
    }
    
     /**
     * Método invocado al cambiar el monto a cobrar
     *
     * @param event Evento producido al cambiar el monto a cobrar
     */
    public void onChangeChargeAmount(ValueChangeEvent event) {
        BigDecimal oldVal = new BigDecimal(event.getOldValue() == null ? "0"
                : event.getOldValue().toString());
        BigDecimal newVal = new BigDecimal(event.getNewValue() == null ? "0"
                : event.getNewValue().toString());

        cobro.setSaldoCliente(cobro.
                getSaldoCliente().add(oldVal));
        cobro.setSaldoCliente(cobro.
                getSaldoCliente().subtract(newVal));

        montoCobroTotal = montoCobroTotal.subtract(oldVal);
        montoCobroTotal = montoCobroTotal.add(newVal);
        if (!cobro.getCodigoMoneda().equalsIgnoreCase("PYG")) {
            BigDecimal oldValGs = oldVal.multiply(tipoCambio.getVenta());
            BigDecimal newValGs = newVal.multiply(tipoCambio.getVenta());

            montoCobroTotalGs = montoCobroTotalGs.subtract(oldValGs);
            montoCobroTotalGs = montoCobroTotalGs.add(newValGs);

        }

    }
    
    public void calcularTotalCobro(){
        montoCobroTotal = BigDecimal.ZERO;
        montoCobroTotalGs = BigDecimal.ZERO;
        for (CobroDetalle cd : cobroDetalles) {
            montoCobroTotal = montoCobroTotal.add(cd.getMontoCobro());
            cobro.setSaldoCliente(cobro.getSaldoCliente().add(cd.getMontoCobro()));
            if (!codMoneda.equalsIgnoreCase("PYG")) {
                BigDecimal montoGs = cd.getMontoCobro().multiply(tipoCambio.getVenta());    
                montoCobroTotalGs = montoCobroTotalGs.add(montoGs);
            }
        }
    }
    
    public void addCobroDetalle() {
        cobroDetalle.setNroLinea(cobroDetalles.size() + 1);
        cobroDetalle.setDescripcion(String.format("Pago de Factura Nro. %s ", facturaCreate.getNroFactura()));
        Estado estadoDetalle = new Estado();
        estadoDetalle.setActivo("C");
        cobro.setEstado(estadoDetalle);
        cobro.setCodigoEstado("COBRADO");
        cobroDetalle.setEstado(estadoDetalle);
        cobroDetalle.setCodigoEstado("COBRADO");
        cobroDetalle.setCodigoConceptoCobro("OTROS");
        
        if (cobroDetalle.getCodigoTipoCobro().equalsIgnoreCase("CHEQUE")){
            cobroDetalle.setCheque(cheque);
        }
        if (cobroDetalle.getMontoCobro() != null && cobroDetalle.getMontoCobro().compareTo(BigDecimal.ZERO)>0){
            cobroDetalles.add(cobroDetalle);
            calcularTotalCobro();
        } else {
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia",
                    "Debe ingresar el monto del cobro!");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
        
        cobroDetalle = new CobroDetalle();
        cobroDetalle.setCodigoTipoCobro("EFECTIVO");
        cheque = new Cheque();
    }
    
     /**
     * Método para agregar linea al detalle de cobro
     */
    public void addLine() {
        cobroDetalle = new CobroDetalle();
        cobroDetalle.setNroLinea(cobroDetalles.size() + 1);
        //cobroDetalle.setIdFactura(cobroDetalles().get(0).getIdFactura());
        cobroDetalle.setMontoCobro(BigDecimal.ZERO);
        cobroDetalle.setIdTipoCobro((Integer) listTipoCobro.get(0).getValue());
        cobroDetalle.setCodigoTipoCobro(listTipoCobro.get(0).getLabel());
        Estado estadoDetalle = new Estado();
        estadoDetalle.setActivo("C");
        cobro.setEstado(estadoDetalle);
        cobro.setCodigoEstado("COBRADO");

        cobroDetalle.setEstado(estadoDetalle);
        cobroDetalle.setCodigoEstado("COBRADO");
        cobroDetalle.setCodigoConceptoCobro("OTROS");
        cobroDetalle.setDescripcion(String.format("Pago de Factura Nro. %s ",facturaCreate.getNroFactura()));
        cobroDetalles.add(cobroDetalle); 
    }

    /**
     * Actualiza el monto en gs del detalle del cobro
     * @param event 
     */
    public void actualizarDetalleGs(AjaxBehaviorEvent event) {
        if (!cobro.getCodigoMoneda().equalsIgnoreCase("PYG")) {
            for (CobroDetalle cd : cobro.getCobroDetalles()) {
                cd.setMontoCobroGuaranies(cd.getMontoCobro().multiply(tipoCambio.getVenta()));
            }
        }
    }
    
    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {

        /**
         * DATOS REALES
         */
        this.codMoneda = null;
        this.facturaCreate = new Factura();
        this.facturaCreate.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        this.facturaCreate.setPorcentajeDescuentoGlobal(new BigDecimal("0"));

        this.cliente = new ClientePojo();
        this.cliente.setListadoPojo(true);
        this.adapter = new ClientPojoAdapter();
        filterCliente();
        this.listaDetalle = new ArrayList<>();
        this.adapterMoneda = new MonedaAdapter();
        this.monedaFilter = new Moneda();
        filterMoneda();
        this.facturaCreate.setCodigoMoneda(obtenerCodMoneda());
        
        this.adapterFactura = new FacturaAdapter();

        this.adapterFacturaCancelada = new FacturaAdapter();

        this.serviceFacturacion = new FacturacionService();
        this.adapterProducto = new ProductoAdapter();

        this.referenciaGeoDepartamento = new ReferenciaGeografica();
        this.referenciaGeoDistrito = new ReferenciaGeografica();
        this.referenciaGeoCiudad = new ReferenciaGeografica();
        this.adapterRefGeo = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoDistrito = new ReferenciaGeograficaAdapter();
        this.adapterRefGeoCiudad = new ReferenciaGeograficaAdapter();

        this.plazo = false;
        this.cuota = false;
        this.idCliente = null;

        this.ruc = "--";
        this.direccion = "--";
        this.telefono = "--";

        this.direccionAdapter = new DireccionAdapter();
        this.personaAdapter = new PersonaListAdapter();
        this.adapterPersonaTelefono = new PersonaTelefonoAdapter();

        this.adapterLiquidacion = new HistoricoLiquidacionAdapter();
        this.historicoLiquidacionFilter = new HistoricoLiquidacion();

        this.showDetalleCreate = false;

        this.linea = 1;
        this.serviceMontoLetras = new MontoLetrasService();
        this.serviceFactura = new FacturacionService();

        this.habilitarPanelCotizacion = false;
        this.tipoCambio = new TipoCambio();
        this.serviceTipoCambio = new TipoCambioService();
        this.nroLineaProducto = null;
        this.producto = new Producto();
        this.adapterProducto = new ProductoAdapter();
        this.idProducto = null;
        this.descripcion = null;
        this.porcentaje = null;
        this.parametroProductoPrecio = new ProductoPrecio();
        this.direccionFilter = new Direccion();
        this.adapterConfigValor = new ConfiguracionValorListAdapter();
        this.configuracionValorFilter = new ConfiguracionValor();
        this.detalleCuotas = new ArrayList<>();
        this.facturaCreate.setIdTipoFactura(2);
        this.showTalonarioPopup = false;
        this.adapterFacturaTalonario = new FacturaTalonarioAdapter();
        this.facturaTalonario = new TalonarioPojo();
        this.talonario = new TalonarioPojo();
        this.showDescargaPopup = false;
        this.fileServiceClient = new FileServiceClient();
        this.idFacturaCreada = null;
        this.nroFacturaCreada = null;
        this.gravada = null;
        this.ctaCtble = null;
        this.nroCtaCtble = null;
        this.tipoTransaccionAdapter = new TipoTransaccionAdapter();
        

        this.serviceOrdenCompra = new OrdenDeCompraService();
        this.ordenDeCompraCreate = new OrdenDeCompra();

        this.generarOc = null;
        this.idOC = null;

        this.moduloInventario = session.getUser().getModuloInventario();

        if (moduloInventario == null) {
            moduloInventario = "N";
        }
        this.idDepartamento = null;
        this.idDistrito = null;
        this.idCiudad = null;

        this.tieneDireccion = "N";
        
        this.licitacion ="N";
        this.facturaDncp = new FacturaDncp();
        this.tieneOC = "N";
        this.nroOC="";
        this.notificar = "N";
        this.notificado = false;
        this.adapterTipoCambio = new TipoCambioAdapter();
        this.adapterLocal = new LocalListAdapter();
       
        obtenerConfiguracionNotificacion();

        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        if (params.get("idOC") != null) {
            idOC = params.get("idOC");
            obtenerOC();
        }

        obtenerConfiguracionNumeracionFactura();
        obtenerConfiguracion();
        obtenerConfiguracionGravada();
        obtenerConfiguracionNroCtaContable();

        filterDepartamento();
        filterDistrito();
        filterCiudad();
        obtenerTipoTransaccion();
        this.siediApi = obtenerConfiguracionGeneral("ARCHIVOS_DTE_SIEDI_API");
        this.tipoDescuento = 1;
        this.emailList = new ArrayList();
        this.adapterTipoCobro = new TipoCobroListAdapter();
        this.tipoCobro = new TipoCobro();
        this.cobro = new Cobro();
        this.cobroDetalle = new CobroDetalle();
        this.cobroDetalle.setCodigoTipoCobro("EFECTIVO");
        this.cobro.setSaldoCliente(BigDecimal.ZERO);
        this.cobro.setCodigoMoneda("PYG");
        this.codMoneda = "PYG";
        this.cobroDetalles = new ArrayList();
        this.adapterEntidadFinanciera = new EntidadFinancieraListAdapter();
        this.entidadFinanciera = new EntidadFinanciera();
        this.cheque = new Cheque();
        this.reciboDigital = obtenerConfiguracionGeneral("TIPO_TALONARIO_RECIBO_DIGITAL");
        
        getTipoPago(); 
        
    }

}
