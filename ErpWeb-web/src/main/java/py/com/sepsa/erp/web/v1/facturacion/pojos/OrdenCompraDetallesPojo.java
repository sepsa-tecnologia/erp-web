/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * pojo para detalles de orden de compra
 *
 * @author Romina Nuñez
 */
public class OrdenCompraDetallesPojo {

    /**
     * Identificador
     */
    private Integer id;
    /**
     * Identificador de orden de compra
     */
    private Integer idOrdenCompra;
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    /**
     * Descripción de producto
     */
    private String descripcion;
    /**
     * Código GTIN
     */
    private String codigoGtin;
    /**
     * Código interno
     */
    private String codigoInterno;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidadSolicitada;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidadConfirmada;
    /**
     * cantidad del producto asociado
     */
    private BigDecimal cantidadDisponible;
    /**
     *
     */
    private BigDecimal precioUnitarioConIva;
    /**
     *
     */
    private BigDecimal precioUnitarioSinIva;
    /**
     *
     */
    private BigDecimal montoIva;
    /**
     *
     */
    private BigDecimal montoImponible;
    /**
     *
     */
    private BigDecimal montoTotal;
    /**
     * Porcentaje IVA
     */
    private Integer porcentajeIva;
    /**
     * N° Línea
     */
    private Integer nroLinea;
    /**
     * Fecha de Vencimiento
     */
    private Date fechaVencimiento;

    //<editor-fold defaultstate="collapsed" desc="***GET Y SET***">
    public Integer getId() {
        return id;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public void setCantidadDisponible(BigDecimal cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public BigDecimal getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public BigDecimal getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(BigDecimal cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public BigDecimal getCantidadConfirmada() {
        return cantidadConfirmada;
    }

    public void setCantidadConfirmada(BigDecimal cantidadConfirmada) {
        this.cantidadConfirmada = cantidadConfirmada;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    //</editor-fold>
    public OrdenCompraDetallesPojo() {

    }

}
