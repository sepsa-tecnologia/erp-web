package py.com.sepsa.erp.web.v1.test.controllers;

import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import py.com.sepsa.erp.web.v1.adapters.MessageListAdapter;
import py.com.sepsa.erp.web.v1.test.pojos.Test;
import py.com.sepsa.erp.web.v1.test.remote.TestServiceClient;

/**
 * Controlador de edición
 * @author Daniel F. Escauriza Arza
 */
@ViewScoped
@Named("testEdit")
public class TestEditController implements Serializable {
   
    /**
     * Cliente para el servicio test
     */
    private final TestServiceClient serviceClient;
    
    /**
     * Adaptador de la lista de mensajes del servidor
     */
    private MessageListAdapter messageAdapter;
    
    /**
     * Datos test
     */
    private Test test;

    /**
     * Obtiene el adaptador de la lista de mensajes provenientes del servidor
     * @return Adaptador de la lista de mensajes provenientes del servidor
     */
    public MessageListAdapter getMessageAdapter() {
        return messageAdapter;
    }

    /**
     * Setea el adaptador de la lista de mensajes provenientes del servidor
     * @param messageAdapter Adaptador de la lista de mensajes provenientes 
     * del servidor
     */
    public void setMessageAdapter(MessageListAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    /**
     * Obtiene los datos test
     * @return Datos test
     */
    public Test getTest() {
        return test;
    }

    /**
     * Setea los datos test
     * @param test Datos test
     */
    public void setTest(Test test) {
        this.test = test;
    }
    
    /**
     * Método para editar
     */
    public void edit() {
        messageAdapter = serviceClient.editTest(test);
    }
    
    /**
     * Inicializa los datos
     * @param id Identificador del test
     */
    private void initTest(int id) {
        this.test = serviceClient.getTest(id);
    }
    
    /** 
     * Constructor de TestEditController
     */
    public TestEditController() {
        
        this.serviceClient = new TestServiceClient();
        
        //TODO: Conectar con el servicio
        /*
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context
                .getExternalContext()
                .getRequest();
        Integer id = new Integer((String) req.getParameter("id"));
        
        initTest(id);*/
        
    }
}