package py.com.sepsa.erp.web.v1.proceso.adapters;

import py.com.sepsa.erp.web.v1.adapters.DataListAdapter;
import py.com.sepsa.erp.web.v1.proceso.pojos.Proceso;
import py.com.sepsa.erp.web.v1.proceso.remote.ProcesoService;

/**
 * Adaptador para la lista de monto mínimo
 *
 * @author Romina Núñez
 */
public class ProcesoAdapter extends DataListAdapter<Proceso> {

    private final ProcesoService service;

    @Override
    public ProcesoAdapter fillData(Proceso searchData) {
        ProcesoAdapter adapter = service.listar(searchData, getFirstResult(), getPageSize());
        this.data = adapter.data;
        this.firstResult = adapter.firstResult;
        this.pageSize = adapter.pageSize;
        this.totalSize = adapter.totalSize;
        return this;
    }

    public ProcesoAdapter(String token, Integer page, Integer pageSize) {
        super(page, pageSize);
        this.service = new ProcesoService(token);
    }

    public ProcesoAdapter(String token) {
        super();
        this.service = new ProcesoService(token);
    }
}
