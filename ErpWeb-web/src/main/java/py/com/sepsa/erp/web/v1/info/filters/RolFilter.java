
package py.com.sepsa.erp.web.v1.info.filters;

import java.util.Map;
import py.com.sepsa.erp.web.v1.filters.Filter;
import py.com.sepsa.erp.web.v1.info.pojos.Rol;

/**
 * Filtro utilizado para el servicio de rol
 * @author Cristina Insfrán
 */
public class RolFilter extends Filter{
     /**
     * Agrega el filtro de identificador del rol
     *
     * @param id Identificador del rol
     * @return
     */
    public RolFilter id(Integer id) {
        if (id != null) {
            params.put("id", id);
        }
        return this;
    }
    
    /**
     * Agrega el filtro de la descripción
     * @param descripcion descripción del rol
     * @return 
     */
    public RolFilter descripcion(String descripcion){
         if(descripcion!= null){
           params.put("descripcion", descripcion);
         }
         return this;
    }
    
    /**
     * Construye el mapa de parametros
     *
     * @param rol datos del filtro
     * @param page página buscada en el resultado
     * @param pageSize tamaño de la pagina de resultado
     * @return 
     */
    public static Map build(Rol rol, Integer page, Integer pageSize) {
        RolFilter filter = new RolFilter();

        filter
                .id(rol.getId())
                .descripcion(rol.getDescripcion())
                .page(page)
                .pageSize(pageSize);

        return filter.getParams();
    }

    /**
     * Constructor de RolFilter
     */
    public RolFilter() {
        super();
    }
    
}
