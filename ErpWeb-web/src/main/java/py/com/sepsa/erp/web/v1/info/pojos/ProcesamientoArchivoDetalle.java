package py.com.sepsa.erp.web.v1.info.pojos;

/**
 * Pojo para detalles de procesamiento de archivos
 *
 * @author alext
 */
public class ProcesamientoArchivoDetalle {

    /**
     * Identificador de la clase
     */
    private Integer id;
    /**
     * Parámetro descripción
     */
    private String descripcion;
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamientoArchivo;

    //<editor-fold defaultstate="collapsed" desc="***GETTERS Y SETTERS***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }
    //</editor-fold>

    /**
     * Constructor de la clase
     */
    public ProcesamientoArchivoDetalle() {
    }

}
