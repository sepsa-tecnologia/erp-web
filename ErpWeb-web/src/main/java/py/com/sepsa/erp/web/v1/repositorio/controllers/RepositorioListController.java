/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.repositorio.controllers;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.comercial.adapters.ClientListAdapter;
import py.com.sepsa.erp.web.v1.comercial.pojos.Cliente;
import py.com.sepsa.erp.web.v1.info.adapters.EmpresaAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Empresa;
import py.com.sepsa.erp.web.v1.logger.WebLogger;
import py.com.sepsa.erp.web.v1.repositorio.adapters.RepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.adapters.TipoRepositorioAdapter;
import py.com.sepsa.erp.web.v1.repositorio.pojos.Repositorio;
import py.com.sepsa.erp.web.v1.repositorio.pojos.TipoRepositorio;

/**
 * Controlador para listar Repositorios
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("repositorioList")
public class RepositorioListController implements Serializable {

    /**
     * Adaptador para la lista de Repositorios
     */
    private RepositorioAdapter repositorioAdapterList;

    /**
     * Adaptador para la lista de Tipos de Repositorios
     */
    private TipoRepositorioAdapter tipoRepositorioListAdapter;

    /**
     * Pojo de tipo Repositorio
     */
    private TipoRepositorio tipoRepositorio;

    /**
     * POJO de Repositorio
     */
    private Repositorio repositorioFilter;

    /**
     * Adaptador de la lista de clientes
     */
    private ClientListAdapter clientAdapter;

    /**
     * Datos del cliente
     */
    private Cliente cliente;

    /**
     * Objeto Empresa para autocomplete
     */
    private Empresa empresaAutocomplete;

    /**
     * Adaptador de la lista de clientes
     */
    private EmpresaAdapter empresaAdapter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public RepositorioAdapter getRepositorioAdapterList() {
        return repositorioAdapterList;
    }

    public void setRepositorioAdapterList(RepositorioAdapter repositorioAdapterList) {
        this.repositorioAdapterList = repositorioAdapterList;
    }

    public Repositorio getRepositorioFilter() {
        return repositorioFilter;
    }

    public void setRepositorioFilter(Repositorio repositorioFilter) {
        this.repositorioFilter = repositorioFilter;
    }

    public ClientListAdapter getClientAdapter() {
        return clientAdapter;
    }

    public void setClientAdapter(ClientListAdapter clientAdapter) {
        this.clientAdapter = clientAdapter;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the tipoRepositorioListAdapter
     */
    public TipoRepositorioAdapter getTipoRepositorioListAdapter() {
        return tipoRepositorioListAdapter;
    }

    /**
     * @param tipoRepositorioListAdapter the tipoRepositorioListAdapter to set
     */
    public void setTipoRepositorioListAdapter(TipoRepositorioAdapter tipoRepositorioListAdapter) {
        this.tipoRepositorioListAdapter = tipoRepositorioListAdapter;
    }

    public TipoRepositorio getTipoRepositorio() {
        return tipoRepositorio;
    }

    public void setTipoRepositorio(TipoRepositorio tipoRepositorio) {
        this.tipoRepositorio = tipoRepositorio;
    }

    public Empresa getEmpresaAutocomplete() {
        return empresaAutocomplete;
    }

    public void setEmpresaAutocomplete(Empresa empresaAutocomplete) {
        this.empresaAutocomplete = empresaAutocomplete;
    }

    public EmpresaAdapter getEmpresaAdapter() {
        return empresaAdapter;
    }

    public void setEmpresaAdapter(EmpresaAdapter empresaAdapter) {
        this.empresaAdapter = empresaAdapter;
    }

    //</editor-fold>
    /**
     * Método para obtener las Retenciones
     */
    public void getRepositorios() {
        this.setRepositorioAdapterList(getRepositorioAdapterList().fillData(getRepositorioFilter()));
    }

    /**
     * Método para obtener los Tipo de Retenciones
     */
    public void getTipoRepositorios() {
        this.setTipoRepositorioListAdapter(getTipoRepositorioListAdapter().fillData(getTipoRepositorio()));
    }

    /**
     * Método autocomplete para obtener empresas
     *
     * @param query
     * @return
     */
    public List<Empresa> completeEmpresaQuery(String query) {
        empresaAutocomplete = new Empresa();
        empresaAutocomplete.setDescripcion(query);
        empresaAdapter = empresaAdapter.fillData(empresaAutocomplete);
        return empresaAdapter.getData();
    }

    /**
     * Selecciona una Empresa de la lista
     *
     * @param event
     */
    public void onItemSelectEmpresa(SelectEvent event) {
        repositorioFilter.setIdEmpresa(((Empresa) event.getObject()).getId());
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.tipoRepositorio = new TipoRepositorio();
        this.cliente = new Cliente();
        this.setRepositorioFilter(new Repositorio());
        getRepositorios();
        getTipoRepositorios();
    }

    /* Método para obtener los clientes filtrados
     *
     * @param query
     * @return
     */
    public List<Cliente> completeQuery(String query) {

        Cliente cliente = new Cliente();
        cliente.setRazonSocial(query);

        clientAdapter = clientAdapter.fillData(cliente);

        return clientAdapter.getData();
    }

    /**
     * Selecciona cliente
     *
     * @param event
     */
    public void onItemSelectClienteFilter(SelectEvent event) {
        cliente.setIdCliente(((Cliente) event.getObject()).getIdCliente());
        repositorioFilter.setIdCliente(cliente.getIdCliente());
    }

    /**
     * Metodo para redirigir a la vista Editar Repositorio
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("repositorio-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.setRepositorioFilter(new Repositorio());
            this.setRepositorioAdapterList(new RepositorioAdapter());
            this.clientAdapter = new ClientListAdapter();
            this.cliente = new Cliente();
            this.tipoRepositorioListAdapter = new TipoRepositorioAdapter();
            this.tipoRepositorio = new TipoRepositorio();
            this.empresaAdapter = new EmpresaAdapter();
            this.empresaAutocomplete = new Empresa();
            getRepositorios();
            getTipoRepositorios();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

}
