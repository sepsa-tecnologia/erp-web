package py.com.sepsa.erp.web.v1.facturacion.controllers;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import py.com.sepsa.erp.web.v1.facturacion.adapters.TipoMotivoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.pojos.TipoMotivo;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista de Tipo Motivo
 *
 * @author Alexander Triana.
 */
@ViewScoped
@Named("tipoMotivoList")
public class TipoMotivoListController implements Serializable {

    /**
     * Adaptador para la lista de tipoMotivo
     */
    private TipoMotivoAdapter tipoMotivoAdapter;
    /**
     * POJO de TipoMotivo
     */
    private TipoMotivo tipoMotivoFilter;

    //<editor-fold defaultstate="collapsed" desc="/**GETTERS & SETTERS**/">
    public TipoMotivoAdapter getTipoMotivoAdapter() {
        return tipoMotivoAdapter;
    }

    public void setTipoMotivoAdapter(TipoMotivoAdapter tipoMotivoAdapter) {
        this.tipoMotivoAdapter = tipoMotivoAdapter;
    }

    public TipoMotivo getTipoMotivoFilter() {
        return tipoMotivoFilter;
    }

    public void setTipoMotivoFilter(TipoMotivo tipoMotivoFilter) {
        this.tipoMotivoFilter = tipoMotivoFilter;
    }

    //</editor-fold>
    /**
     * Método para obtener tipoMotivo
     */
    public void getTipoMotivo () {
        getTipoMotivoAdapter().setFirstResult(0);
        this.setTipoMotivoAdapter(getTipoMotivoAdapter().fillData(getTipoMotivoFilter()));
    }

    /**
     * Método para limpiar el filtro
     */
    public void clear() {
        this.tipoMotivoFilter = new TipoMotivo();
        getTipoMotivo();
    }

    /**
     * Metodo para redirigir a la vista Editar
     *
     * @param id
     * @return
     */
    public String editUrl(Integer id) {
        return String.format("tipo-motivo-edit?faces-redirect=true&id=%d", id);
    }

    /**
     * Inicializa los datos del controlador
     */
    @PostConstruct
    public void init() {
        try {
            this.tipoMotivoAdapter = new TipoMotivoAdapter();
            this.tipoMotivoFilter = new TipoMotivo();
            getTipoMotivo();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }
}
