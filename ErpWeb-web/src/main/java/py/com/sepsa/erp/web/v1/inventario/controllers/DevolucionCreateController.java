/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.web.v1.inventario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PF;
import org.primefaces.event.SelectEvent;
import py.com.sepsa.erp.web.v1.factura.pojos.Factura;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaDetallePojo;
import py.com.sepsa.erp.web.v1.factura.pojos.FacturaPojo;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaDetallePojoAdapter;
import py.com.sepsa.erp.web.v1.facturacion.adapters.FacturaPojoAdapter;
import py.com.sepsa.erp.web.v1.info.adapters.ProductoAdapter;
import py.com.sepsa.erp.web.v1.http.body.pojos.BodyResponse;
import py.com.sepsa.erp.web.v1.info.adapters.EstadoAdapter;
import py.com.sepsa.erp.web.v1.info.pojos.Estado;
import py.com.sepsa.erp.web.v1.info.pojos.Producto;
import py.com.sepsa.erp.web.v1.inventario.adapters.DepositoLogisticoAdapter;
import py.com.sepsa.erp.web.v1.inventario.adapters.MotivoAdapter;
import py.com.sepsa.erp.web.v1.inventario.pojos.DepositoLogistico;
import py.com.sepsa.erp.web.v1.inventario.pojos.Devolucion;
import py.com.sepsa.erp.web.v1.inventario.pojos.DevolucionDetalle;
import py.com.sepsa.erp.web.v1.inventario.pojos.Motivo;
import py.com.sepsa.erp.web.v1.inventario.remote.DevolucionService;
import py.com.sepsa.erp.web.v1.logger.WebLogger;

/**
 * Controlador para la vista crear operaciones o Ajuste de inventario
 *
 * @author Romina Núñez
 */
@ViewScoped
@Named("devolucionCreate")
public class DevolucionCreateController implements Serializable {

    /**
     * Adapter para motivo de operacion
     */
    private MotivoAdapter adapterMotivo;
    /**
     * POJO de motivo
     */
    private Motivo motivo;
    /**
     * Adapter para Estado
     */
    private EstadoAdapter adapterEstado;
    /**
     * Pojo para estado
     */
    private Estado estado;
    /**
     * Objeto depositoDestinoLogistico
     */
    private DepositoLogistico depositoDestino;
    /**
     * Adaptador de la lista de depositoDestinoLogistico
     */
    private DepositoLogisticoAdapter depositoDestinoAdapter;
    private String tieneFactura;
    private Boolean ingresaNroFactura;
    private Devolucion devolucionCreate;
    private Integer idDepositoDestino;
    private String nroFactura;
    private String codigoMotivo;
    private Date fechaVtoAux;
    private Producto producto;

    /**
     * Cliente para el servicio de operacion
     */
    private final DevolucionService service;
    /**
     * Adaptador para la lista de facturas
     */
    private FacturaPojoAdapter adapterFactura;
    /**
     * POJO para Factura
     */
    private FacturaPojo facturaFilter;
    /**
     * POJO para Factura
     */
    private FacturaPojo facturaSelected;

    public MotivoAdapter getAdapterMotivo() {
        return adapterMotivo;
    }

    public void setFacturaSelected(FacturaPojo facturaSelected) {
        this.facturaSelected = facturaSelected;
    }

    public FacturaPojo getFacturaSelected() {
        return facturaSelected;
    }

    public void setFacturaFilter(FacturaPojo facturaFilter) {
        this.facturaFilter = facturaFilter;
    }

    public void setAdapterFactura(FacturaPojoAdapter adapterFactura) {
        this.adapterFactura = adapterFactura;
    }

    public FacturaPojo getFacturaFilter() {
        return facturaFilter;
    }

    public FacturaPojoAdapter getAdapterFactura() {
        return adapterFactura;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setFechaVtoAux(Date fechaVtoAux) {
        this.fechaVtoAux = fechaVtoAux;
    }

    public Date getFechaVtoAux() {
        return fechaVtoAux;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setAdapterMotivo(MotivoAdapter adapterMotivo) {
        this.adapterMotivo = adapterMotivo;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public EstadoAdapter getAdapterEstado() {
        return adapterEstado;
    }

    public void setAdapterEstado(EstadoAdapter adapterEstado) {
        this.adapterEstado = adapterEstado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public DepositoLogistico getDepositoDestino() {
        return depositoDestino;
    }

    public void setDepositoDestino(DepositoLogistico depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public DepositoLogisticoAdapter getDepositoDestinoAdapter() {
        return depositoDestinoAdapter;
    }

    public void setDepositoDestinoAdapter(DepositoLogisticoAdapter depositoDestinoAdapter) {
        this.depositoDestinoAdapter = depositoDestinoAdapter;
    }

    public String getTieneFactura() {
        return tieneFactura;
    }

    public void setTieneFactura(String tieneFactura) {
        this.tieneFactura = tieneFactura;
    }

    public Boolean getIngresaNroFactura() {
        return ingresaNroFactura;
    }

    public void setIngresaNroFactura(Boolean ingresaNroFactura) {
        this.ingresaNroFactura = ingresaNroFactura;
    }

    public Devolucion getDevolucionCreate() {
        return devolucionCreate;
    }

    public void setDevolucionCreate(Devolucion devolucionCreate) {
        this.devolucionCreate = devolucionCreate;
    }

    public DevolucionService getService() {
        return service;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public List<DevolucionDetalle> getListaDetalles() {
        return listaDetalles;
    }

    public void setListaDetalles(List<DevolucionDetalle> listaDetalles) {
        this.listaDetalles = listaDetalles;
    }

    /**
     * Detalles de Devolución
     */
    private List<DevolucionDetalle> listaDetalles;

    /**
     * Metodo para crear Operacion
     */
    public void create() {
        añadirDetalles();
        devolucionCreate.setCodigoEstado("CONFIRMADO");
        BodyResponse<Devolucion> respuesta = service.createDevolucion(devolucionCreate);
        if (respuesta.getSuccess()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO,
                            "Info", "Devolución creada correctamente!"));
            init();
            PF.current().ajax().update(":data-form:form-data:deposito-destino");
        }
    }

    /**
     * Método para obtener el listado de motivos para operacion
     */
    public void filterMotivoOperacion() {
        this.motivo.setCodigoTipoMotivo("DEVOLUCION");
        adapterMotivo = adapterMotivo.fillData(motivo);
    }

    /**
     * Método para obtener el listado de estados
     */
    public void getEstadoList() {
        estado.setCodigoTipoEstado("DEVOLUCION");
        adapterEstado = adapterEstado.fillData(estado);
    }

    public void consultarFactura() {
        if (this.nroFactura != null && !this.nroFactura.trim().isEmpty()) {
            facturaFilter = new FacturaPojo();
            facturaFilter.setListadoPojo(true);
            facturaFilter.setNroFactura(nroFactura);
            adapterFactura = adapterFactura.fillData(facturaFilter);

            if (adapterFactura.getData().size() == 1) {
                agregarDatosDeFacturaDetalle(adapterFactura.getData().get(0).getId());
            } else if (adapterFactura.getData().size() > 1) {
                PF.current().executeScript("$('#modalFactura').modal('show');");
                PF.current().ajax().update(":data-form:form-data:modalFactura:data-list-factura");
            } else {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN,
                                "Info", "No hay datos disponibles asociados al N° ingresado"));
            }

        }
    }

    public void agregarFactura() {
        agregarDatosDeFacturaDetalle(facturaSelected.getId());
    }

    /**
     * Método para agregar datos de factura
     *
     * @param idFactura
     */
    public void agregarDatosDeFacturaDetalle(Integer idFactura) {
        this.listaDetalles = new ArrayList<>();
        this.devolucionCreate.setIdFactura(idFactura);
        FacturaDetallePojoAdapter adapterDetalle = new FacturaDetallePojoAdapter();
        FacturaDetallePojo facturaDetalleFilter = new FacturaDetallePojo();

        facturaDetalleFilter.setIdFactura(idFactura);
        facturaDetalleFilter.setListadoPojo(true);
        adapterDetalle = adapterDetalle.fillData(facturaDetalleFilter);

        for (FacturaDetallePojo fd : adapterDetalle.getData()) {
            DevolucionDetalle devolucionDetalle = new DevolucionDetalle();
            devolucionDetalle.setIdFacturaDetalle(fd.getId());
            devolucionDetalle.setProducto(fd.getDescripcion());
            devolucionDetalle.setIdProducto(fd.getIdProducto());
            devolucionDetalle.setFechaVencimiento(null);
            devolucionDetalle.setCantidadFacturada(fd.getCantidad().intValue());
            devolucionDetalle.setCantidadDevolucion(0);
            listaDetalles.add(devolucionDetalle);
        }
    }

    /**
     * Ingresar nroDocumento
     */
    public void ingresarNroDocumento() {
        if (tieneFactura.equals("S")) {
            ingresaNroFactura = true;
        } else {
            nroFactura = null;
            listaDetalles = new ArrayList<>();
            ingresaNroFactura = false;
        }
    }

    /**
     * Autocomplete para depositoDestinoLogistico
     *
     * @param query
     * @return
     */
    public List<DepositoLogistico> completeQueryDeposito(String query) {
        depositoDestino = new DepositoLogistico();
        depositoDestino.setDescripcion(query);
        depositoDestino.setListadoPojo(true);
        depositoDestinoAdapter = depositoDestinoAdapter.fillData(depositoDestino);

        return depositoDestinoAdapter.getData();
    }

    /**
     * Método para seleccionar el depósito
     */
    public void onItemSelectDeposito(SelectEvent event) {
        this.idDepositoDestino = ((DepositoLogistico) event.getObject()).getId();
    }

    /**
     * Metodo para agregar un detalle a la lista de detalles.
     */
    public void añadirDetalle() {
        try {
            DevolucionDetalle devolucionDetalle = new DevolucionDetalle();
            devolucionDetalle.setProducto(producto.getDescripcion());
            devolucionDetalle.setIdProducto(producto.getId());
            devolucionDetalle.setFechaVencimiento(fechaVtoAux);
            devolucionDetalle.setCantidadFacturada(0);
            devolucionDetalle.setCantidadDevolucion(0);
            listaDetalles.add(devolucionDetalle);
            fechaVtoAux = null;
            producto = new Producto();
            PF.current().ajax().update(":data-form:form-data:producto");
            PF.current().ajax().update(":data-form:form-data:fechaVto");
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }

    }

    /**
     * Metodo para eliminar un detalle de la lista de detalles.
     *
     * @param opD
     */
    public void deleteDetalle(DevolucionDetalle dd) {
        this.listaDetalles.remove(dd);
    }

    /**
     * Metodo para añadir la lista de detalles al objeto de devolución a enviar
     */
    public void añadirDetalles() {
        for (DevolucionDetalle dd : listaDetalles) {
            dd.setIdDepositoLogistico(this.idDepositoDestino);
            dd.setCodigoMotivo(codigoMotivo);
            dd.setCodigoEstadoInventario("CUARENTENA");
        }
        this.devolucionCreate.setDevolucionDetalles(listaDetalles);
    }

    /**
     * Método autocomplete Productos
     *
     * @param query
     * @return
     */
    public List<Producto> completeQueryProducto(String query) {
        Producto producto = new Producto();
        ProductoAdapter adapterProducto = new ProductoAdapter();

        producto.setDescripcion(query);
        producto.setActivo("S");
        adapterProducto = adapterProducto.fillData(producto);

        return adapterProducto.getData();
    }

    /**
     * Método para seleccionar el producto
     */
    public void onItemSelectProducto(SelectEvent event) {
        producto.setId(((Producto) event.getObject()).getId());
        producto.setDescripcion(((Producto) event.getObject()).getDescripcion());
    }

    /**
     * Metodo para inicializar los datos
     */
    private void init() {
        try {
            this.motivo = new Motivo();
            this.adapterMotivo = new MotivoAdapter();
            this.estado = new Estado();
            this.adapterEstado = new EstadoAdapter();
            this.depositoDestino = new DepositoLogistico();
            this.depositoDestino.setListadoPojo(true);
            this.depositoDestinoAdapter = new DepositoLogisticoAdapter();
            this.tieneFactura = "N";
            this.ingresaNroFactura = false;
            this.filterMotivoOperacion();
            this.getEstadoList();
            this.idDepositoDestino = null;
            this.listaDetalles = new ArrayList<>();
            this.nroFactura = null;
            this.codigoMotivo = null;
            this.fechaVtoAux = null;
            this.producto = new Producto();
            this.adapterFactura = new FacturaPojoAdapter();
            this.facturaFilter = new FacturaPojo();
            this.facturaSelected = new FacturaPojo();

            this.devolucionCreate = new Devolucion();
        } catch (Exception e) {
            WebLogger.get().fatal(e);
        }
    }

    /**
     * Constructor
     *
     */
    public DevolucionCreateController() {
        init();
        this.service = new DevolucionService();
    }

}
