// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:

var placeSearch, autocomplete;
var componentForm = {
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')));
    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    autocomplete.setTypes([]);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    var address = place.address_components;
    if (typeof address !== "undefined") {
        var i = address.length - 1;
        //var parametros = new Array();
        while (true) {
            if (i >= 0) {
                //var addressType = address[i].types[0];
                //if (componentForm[addressType]) {
                //    var val = address[i][componentForm[addressType]];
                //    parametros.push({name: addressType, value: val});
                //}
            } else {
                var location = place.geometry.location;
                PF('geoMap').reverseGeocode(location.lat(), location.lng());
                //send_address(parametros);
                break;
            }
            i--;
        }
    }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        var geolocation = {
            lat: -25.2939747,
            lng: -57.5883479
        };

        var circle = new google.maps.Circle({
            center: geolocation,
            radius: 1000
        });
        autocomplete.setBounds(circle.getBounds());
    }
}