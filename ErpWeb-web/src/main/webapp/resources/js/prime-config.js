PrimeFaces.locales["es"] = {
    closeText: 'Cerrar', 
    prevText: '',
    nextText: '', 
    currentText: 'Actual',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 
        'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 
        'Noviembre', 'Diciembre'],
    monthNamesShort: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 
        'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 
        'Viernes', 'Sábado'],
    dayNamesShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    weekHeader: 'Semanas',
    /*dateFormat: 'dd/mm/yyyy',*/
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};

